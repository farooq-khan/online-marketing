<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddConcessionColumnToOtherFeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('other_fees', function (Blueprint $table) {
            $table->string('concession')->after('amount')->nullable();
            $table->string('paid_amount')->after('concession')->nullable();
            $table->string('unpaid_amount')->after('paid_amount')->nullable();
            $table->dateTime('fee_month')->after('unpaid_amount')->nullable();
            $table->string('receipt')->after('fee_month')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('other_fees', function (Blueprint $table) {
            $table->dropColumn('concession');
            $table->dropColumn('paid_amount');
            $table->dropColumn('unpaid_amount');
            $table->dropColumn('fee_month');
            $table->dropColumn('receipt');
        });
    }
}
