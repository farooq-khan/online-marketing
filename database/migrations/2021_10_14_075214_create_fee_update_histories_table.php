<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeeUpdateHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fee_update_histories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('fee_id')->nullable();
            $table->double('paid_amount',15,4)->nullable();
            $table->double('concession',15,4)->nullable();
            $table->string('concession_reason')->nullable();
            $table->string('receipt_no')->nullable();
            $table->double('unpaid_amount',15,4)->nullable();
            $table->date('submitted_date')->nullable();
            $table->integer('user_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fee_update_histories');
    }
}
