<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPayscaleIdColumnToTeachersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('teachers', function (Blueprint $table) {
            $table->integer('designation_id')->after('designation')->nullable();
            $table->integer('department_id')->after('designation_id')->nullable();
            $table->integer('payscale_id')->after('department_id')->nullable();
            $table->dropColumn('designation');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('teachers', function (Blueprint $table) {
            $table->dropColumn('designation_id');
            $table->dropColumn('department_id');
            $table->dropColumn('payscale_id');
            $table->string('designation')->after('experience')->nullable();
        });
    }
}
