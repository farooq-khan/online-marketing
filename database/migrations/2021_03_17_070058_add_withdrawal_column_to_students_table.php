<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddWithdrawalColumnToStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('students', function (Blueprint $table) {
            $table->string('primary_withdrawal_no')->after('promotion_history')->nullable();
            $table->string('middle_withdrawal_no')->after('primary_withdrawal_no')->nullable();
            $table->string('high_withdrawal_no')->after('middle_withdrawal_no')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('students', function (Blueprint $table) {
            $table->dropColumn('primary_withdrawal_no');
            $table->dropColumn('middle_withdrawal_no');
            $table->dropColumn('high_withdrawal_no');
        });
    }
}
