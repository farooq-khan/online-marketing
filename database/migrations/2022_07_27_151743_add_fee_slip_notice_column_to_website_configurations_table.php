<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFeeSlipNoticeColumnToWebsiteConfigurationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('website_configurations', function (Blueprint $table) {
            $table->text('fee_slip_notice')->after('banner3')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('website_configurations', function (Blueprint $table) {
            $table->dropColumn('fee_slip_notice');
        });
    }
}
