<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDeductionColumnToTeacherSalariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('teacher_salaries', function (Blueprint $table) {
            $table->string('deduction')->after('paid_amount')->nullable();
            $table->string('reason')->after('deduction')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('teacher_salaries', function (Blueprint $table) {
            $table->dropColumn('deduction');
            $table->dropColumn('reason');
        });
    }
}
