<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddQuoteColumnToWebsiteConfigurationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('website_configurations', function (Blueprint $table) {
            $table->string('quote')->after('id')->nullable();
            $table->text('about_us')->after('quote')->nullable();
            $table->string('principal_name',40)->after('about_us')->nullable();
            $table->text('principal_description')->after('principal_name')->nullable();
            $table->string('principal_photo')->after('principal_description')->nullable();
            $table->text('why_choose_us')->after('principal_photo')->nullable();
            $table->text('books_and_library')->after('why_choose_us')->nullable();
            $table->text('quality_teachers')->after('books_and_library')->nullable();
            $table->text('great_certification')->after('quality_teachers')->nullable();
            $table->text('teacher_section_description')->after('great_certification')->nullable();
            $table->text('image_gallery_description')->after('teacher_section_description')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('website_configurations', function (Blueprint $table) {
            $table->dropColumn('quote');
            $table->dropColumn('about_us');
            $table->dropColumn('principal_name');
            $table->dropColumn('principal_description');
            $table->dropColumn('principal_photo');
            $table->dropColumn('why_choose_us');
            $table->dropColumn('books_and_library');
            $table->dropColumn('quality_teachers');
            $table->dropColumn('great_certification');
            $table->dropColumn('teacher_section_description');
            $table->dropColumn('image_gallery_description');
        });
    }
}
