<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLibraryBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('library_books', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title')->nullable();
            $table->string('author')->nullable();
            $table->string('edition')->nullable();
            $table->string('price')->nullable();
            $table->string('publisher')->nullable();
            $table->string('book_no')->nullable();
            $table->string('book_fund')->nullable();
            $table->string('pages')->nullable();
            $table->string('subject')->nullable();
            $table->string('user_id')->nullable();
            $table->string('total_quantity')->nullable();
            $table->string('remaining_quantity')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('library_books');
    }
}
