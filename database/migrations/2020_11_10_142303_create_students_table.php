<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->nullable();
            $table->string('std_phone')->nullable();
            $table->string('roll_no')->nullable();
            $table->date('dob')->nullable();
            $table->string('guardian')->nullable();
            $table->string('guardian_cnic')->nullable();
            $table->string('guardian_phone')->nullable();
            $table->string('address')->nullable();
            $table->date('date_of_admission')->nullable();
            $table->string('gender')->nullable();
            $table->string('class_id')->nullable();
            $table->string('discount')->nullable();
            $table->string('image')->nullable();
            $table->integer('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
