<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSubmittedDateColumnToTeacherSalariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('teacher_salaries', function (Blueprint $table) {
            $table->date('submitted_date')->after('salary_month')->nullable();
            $table->string('remark')->after('submitted_date')->nullable();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('teacher_salaries', function (Blueprint $table) {
            $table->dropColumn('submitted_date');
            $table->dropColumn('remark');
        });
    }
}
