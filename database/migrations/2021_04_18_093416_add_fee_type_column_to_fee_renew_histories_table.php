<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFeeTypeColumnToFeeRenewHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fee_renew_histories', function (Blueprint $table) {
            $table->string('fee_type')->after('class_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fee_renew_histories', function (Blueprint $table) {
            $table->dropColumn('fee_type');
        });
    }
}
