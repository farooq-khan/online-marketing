<div class="modal fade" id="addTeacherAttendanceCommon" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Submit Teacher Attendance</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      
      <form id="add-teacher-attendance-common" method="POST" enctype="multipart/form-data">
      <div class="modal-body ">

          <div class="form-row">
             <div class="form-group col-md-6">
                <label for="date">Attendance Date</label>
                <input type="date" name="Attendance_date" id="date" class="form-control" />
            </div>

          </div>
          
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
      </form>
    </div>
  </div>
</div>