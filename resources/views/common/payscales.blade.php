@extends('user.layouts.layout')

@section('title','Online Marketing')

@section('content')
<style type="text/css">
   
</style>

<div class="row page-title-row">
    <div class="col-md col-sm col  page-title-col align-self-center">
        <h1 class="page-title">All Payscales</h1>
    </div>
    <div class="col-sm col-auto ml-auto text-right page-action-button">
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addPayscale">+Add Payscale</button>
    </div>      
</div>

<div class="card">
      <div class="card-body">
      <div class="table-responsive">
      <table class="table table-striped nowrap table-payscales">
      <thead>
      <tr>
          <th>S.No</th>
          <th>Name</th>
          <th>Action</th>
      </tr>
      </thead>
      </table>
      </div>
  </div>
</div>


<!--Add Student Modal -->
<div class="modal fade" id="addPayscale" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add Payscale</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form id="add-payscale-form" method="POST" enctype="multipart/form-data">
        {{csrf_field()}}
        <div class="modal-body ">

          <div class="form-row row-cols-lg-3 row-cols-sm-2 row-cols-2">
            <div class="form-group col">
                <label for="name">Payscale</label>
                <input type="text" name="name" id="name" class="form-control" placeholder="i.e BPS 5"/>
            </div>
          </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Add</button>
      </div>
      </form>
    </div>
  </div>
</div>
@endsection

@section('javascript')
<script type="text/javascript">
  $(document).ready(function(){
  $('#add-payscale-form').on('submit',function(e){
    e.preventDefault();

    var name = $('#name').val();
    var guardian = $('#guardian').val();
    if(name == null || name == '')
    {
      toastr.error('Sorry!', 'Payscale Name Cannot Be Null!!!',{"positionClass": "toast-bottom-right"});
      return false;
    }
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
    });
       $.ajax({
          url: "{{ route('add-payscale') }}",
          dataType: 'json',
          method: 'post',
          data: new FormData(this),
          contentType: false,
          cache: false,
          processData:false,
          beforeSend: function(){
            $('.save-btn').addClass('disabled');
            $('.save-btn').attr('disabled', true);
            $('#loader_modal').modal({
              backdrop: 'static',
              keyboard: false
            });
            $('#loader_modal').modal('show');
          },
          success: function(result){
            $('.save-btn').attr('disabled', true);
            $('.save-btn').removeAttr('disabled');
            if(result.success == true){
              toastr.success('Success!', 'Payscale Added Successfully!!!',{"positionClass": "toast-bottom-right"});
              $('.table-payscales').DataTable().ajax.reload();
              $('#add-payscale-form')[0].reset();
              $('#addPayscale').modal('hide');
              $('#loader_modal').modal('hide');
              
            }
            else{
              toastr.error('Error!', result.errormsg,{"positionClass": "toast-bottom-right"});
              $('#loader_modal').modal('hide');
            }

          },
          error: function (request, status, error) {
                $('#loader_modal').modal('hide');
                $('.save-btn').removeClass('disabled');
                $('.save-btn').removeAttr('disabled');
                json = $.parseJSON(request.responseText);
                $.each(json.errors, function(key, value){
                      $('input[name="'+key+'"]').after('<span class="invalid-feedback" role="alert"><strong>'+value+'</strong>');
                     $('input[name="'+key+'"]').addClass('is-invalid');
                     $('textarea[name="'+key+'"]').after('<span class="invalid-feedback" role="alert"><strong>'+value+'</strong>');
                     $('textarea[name="'+key+'"]').addClass('is-invalid');


                });
            }
        });
  });

  var table2 =  $('.table-payscales').DataTable({
    processing: true,
    searching: true,
    "language": {
    processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw" style="color:#13436c;"></i><span class="sr-only">Loading...</span> '},
    ordering: false,
    pageLength: {{50}},
    serverSide: true,
    autoWidth: false,
    "lengthMenu": [50,100,150,200],
    
    scrollX:true,
    scrollY : '90vh',
    scrollCollapse: true,
    ajax: 
    {
      beforeSend: function(){
        $('#loader_modal').modal({
          backdrop: 'static',
          keyboard: false
        });
        $('#loader_modal').modal('show');
      },
      url: "{!! route('get-payscales') !!}",
       data: function(data) { } ,
    },
    columns: [
        { data: 'empty_col', name: 'empty_col' },
        { data: 'name', name: 'name' }, 
        { data: 'action', name: 'action' }, 
    ],
    initComplete: function () {
    // Enable THEAD scroll bars
    $('.dataTables_scrollHead').css('overflow', 'auto');
    // Sync THEAD scrolling with TBODY
      $('.dataTables_scrollHead').on('scroll', function () {
          $('.dataTables_scrollBody').scrollLeft($(this).scrollLeft());
      });
    },
    drawCallback: function(){
      $('#loader_modal').modal('hide');
    },
  });

  table2.on( 'draw.dt', function () {
    var PageInfo = $('.table-payscales').DataTable().page.info();
         table2.column(0, { page: 'current' }).nodes().each( function (cell, i) {
            cell.innerHTML = i + 1 + PageInfo.start;
        } );
  } );

  $(document).on('click','.delete_designation',function(){
      var id = $(this).data('id');

        swal({
            title: "Are You Sure!",
            html: true,
            text: "<b>You want to delete this payscale !!!</b>",
            type: "info",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes!",
            cancelButtonText: "No!",
            closeOnConfirm: true,
            closeOnCancel: false
          },
            function(isConfirm) {
            if (isConfirm){
             $.ajaxSetup({
              headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
              }
            });
            $.ajax({
              url: "{{ route('delete-payscale') }}",
              method: 'post',
              data: {id:id},
              beforeSend: function(){
                $('#loader_modal').modal({
                  backdrop: 'static',
                  keyboard: false
                });
                $('#loader_modal').modal('show');
              },
              success: function (response) {

                if(response.success == true)
                {
                  toastr.success('Success!', 'Record Deleted Successfully!!!',{"positionClass": "toast-bottom-right"});
                  $('.table-payscales').DataTable().ajax.reload();
                  $('#loader_modal').modal('hide');
                }
                else if(response.success == false)
                {
                  toastr.info('Sorry!', 'Cannot delete payscale as it is already bond to other record(s)!!!',{"positionClass": "toast-bottom-right"});
                  $('.table-payscales').DataTable().ajax.reload();
                  $('#loader_modal').modal('hide');
                }
                else if(response.success == false)
                {
                  toastr.error('Error!', 'Record does not exist!!!',{"positionClass": "toast-bottom-right"});
                  $('.table-payscales').DataTable().ajax.reload();
                  $('#loader_modal').modal('hide');
                }
               
              },
              error: function(request, status, error){
                $('#loader_modal').modal('hide');
              }
            });
            }
            else{
              swal("Cancelled", "", "error");
              
            }
          });
  });
});
</script>
@stop