@extends('user.layouts.layout')

@section('title','Online Marketing')

@section('content')
<style type="text/css">
   
</style>

<div class="row page-title-row">
    <div class="col-md col-sm col  page-title-col align-self-center">
        <h1 class="page-title">All Departments</h1>
    </div>
    <div class="col-sm col-auto ml-auto text-right page-action-button">
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addDepartment">+Add Department</button>
    </div>      
</div>

<div class="card">
      <div class="card-body">
      <div class="table-responsive">
      <table class="table table-striped nowrap table-departments">
      <thead>
      <tr>
          <th>S.No</th>
          <th>Name</th>
          <th>Action</th>
      </tr>
      </thead>
      </table>
      </div>
  </div>
</div>


<!--Add Student Modal -->
<div class="modal fade" id="addDepartment" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add Department</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form id="add-department-form" method="POST" enctype="multipart/form-data">
        {{csrf_field()}}
        <div class="modal-body ">

          <div class="form-row row-cols-lg-3 row-cols-sm-2 row-cols-2">
            <div class="form-group col">
                <label for="name">Department Name</label>
                <input type="text" name="name" id="name" class="form-control" placeholder="Department Name"/>
            </div>
          </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Add</button>
      </div>
      </form>
    </div>
  </div>
</div>
@endsection

@section('javascript')
<script type="text/javascript">
  $(document).ready(function(){
  $('#add-department-form').on('submit',function(e){
    e.preventDefault();

    var name = $('#name').val();
    var guardian = $('#guardian').val();
    if(name == null || name == '')
    {
      toastr.error('Sorry!', 'Department Name Cannot Be Null!!!',{"positionClass": "toast-bottom-right"});
      return false;
    }
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
    });
       $.ajax({
          url: "{{ route('add-department') }}",
          dataType: 'json',
          method: 'post',
          data: new FormData(this),
          contentType: false,
          cache: false,
          processData:false,
          beforeSend: function(){
            $('.save-btn').addClass('disabled');
            $('.save-btn').attr('disabled', true);
            $('#loader_modal').modal({
              backdrop: 'static',
              keyboard: false
            });
            $('#loader_modal').modal('show');
          },
          success: function(result){
            $('.save-btn').attr('disabled', true);
            $('.save-btn').removeAttr('disabled');
            if(result.success == true){
              toastr.success('Success!', 'Department Added Successfully!!!',{"positionClass": "toast-bottom-right"});
              $('.table-departments').DataTable().ajax.reload();
              /*setTimeout(function(){
                window.location.reload();
              }, 2000);*/

              $('#add-department-form')[0].reset();
              $('#addDepartment').modal('hide');
              $('#loader_modal').modal('hide');
              
            }
            else{
              toastr.error('Error!', result.errormsg,{"positionClass": "toast-bottom-right"});
              $('#loader_modal').modal('hide');
            }

          },
          error: function (request, status, error) {
                /*$('.form-control').removeClass('is-invalid');
                $('.form-control').next().remove();*/
                $('#loader_modal').modal('hide');
                $('.save-btn').removeClass('disabled');
                $('.save-btn').removeAttr('disabled');
                json = $.parseJSON(request.responseText);
                $.each(json.errors, function(key, value){
                      $('input[name="'+key+'"]').after('<span class="invalid-feedback" role="alert"><strong>'+value+'</strong>');
                     $('input[name="'+key+'"]').addClass('is-invalid');
                     $('textarea[name="'+key+'"]').after('<span class="invalid-feedback" role="alert"><strong>'+value+'</strong>');
                     $('textarea[name="'+key+'"]').addClass('is-invalid');


                });
            }
        });
  });

  var table2 =  $('.table-departments').DataTable({
    processing: true,
    searching: true,
    "language": {
    processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw" style="color:#13436c;"></i><span class="sr-only">Loading...</span> '},
    ordering: false,
    pageLength: {{50}},
    serverSide: true,
    autoWidth: false,
    "lengthMenu": [50,100,150,200],
    
    scrollX:true,
    scrollY : '90vh',
    scrollCollapse: true,
    ajax: 
    {
      beforeSend: function(){
        $('#loader_modal').modal({
          backdrop: 'static',
          keyboard: false
        });
        $('#loader_modal').modal('show');
      },
      url: "{!! route('get-departments') !!}",
       data: function(data) { } ,
    },
    columns: [
        { data: 'empty_col', name: 'empty_col' },
        { data: 'name', name: 'name' }, 
        { data: 'action', name: 'action' }, 
    ],
    initComplete: function () {
    // Enable THEAD scroll bars
    $('.dataTables_scrollHead').css('overflow', 'auto');
    // Sync THEAD scrolling with TBODY
      $('.dataTables_scrollHead').on('scroll', function () {
          $('.dataTables_scrollBody').scrollLeft($(this).scrollLeft());
      });
    },
    drawCallback: function(){
      $('#loader_modal').modal('hide');
    },
  });

  table2.on( 'draw.dt', function () {
    var PageInfo = $('.table-departments').DataTable().page.info();
         table2.column(0, { page: 'current' }).nodes().each( function (cell, i) {
            cell.innerHTML = i + 1 + PageInfo.start;
        } );
  } );

    $(document).on('click','.suspend_teacher',function(){
      var id = $(this).data('id');

        swal({
            title: "Are You Sure!",
            html: true,
            text: "<b>You want to suspend this teacher !!!</b>",
            type: "info",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes!",
            cancelButtonText: "No!",
            closeOnConfirm: true,
            closeOnCancel: false
          },
            function(isConfirm) {
            if (isConfirm){
             $.ajaxSetup({
              headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
              }
            });
            $.ajax({
              url: "{{ route('suspend-teacher') }}",
              method: 'post',
              data: {id:id},
              beforeSend: function(){
                $('#loader_modal').modal({
                  backdrop: 'static',
                  keyboard: false
                });
                $('#loader_modal').modal('show');
              },
              success: function (response) {
                if(response.success == true)
                {
                  toastr.success('Success!', 'Teacher Suspended Successfully!!!',{"positionClass": "toast-bottom-right"});
                  $('.table-departments').DataTable().ajax.reload();
                  $('#loader_modal').modal('hide');
                }
               
              },
              error: function(request, status, error){
                $('#loader_modal').modal('hide');
              }
            });
            }
            else{
              swal("Cancelled", "", "error");
              
            }
          });
  });

    $(document).on('change','.teacher_status',function(){
    $('.table-departments').DataTable().ajax.reload();
    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
  });

    $(document).on('click','#reset',function(){
    $('.teacher_status').val('');
    $('.table-departments').DataTable().ajax.reload();
    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
  });

  $(document).on('click','.activate_teacher',function(){
      var id = $(this).data('id');

        swal({
            title: "Are You Sure!",
            html: true,
            text: "<b>You want to activate this teacher !!!</b>",
            type: "info",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes!",
            cancelButtonText: "No!",
            closeOnConfirm: true,
            closeOnCancel: false
          },
            function(isConfirm) {
            if (isConfirm){
             $.ajaxSetup({
              headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
              }
            });
            $.ajax({
              url: "{{ route('activate-teacher') }}",
              method: 'post',
              data: {id:id},
              beforeSend: function(){
                $('#loader_modal').modal({
                  backdrop: 'static',
                  keyboard: false
                });
                $('#loader_modal').modal('show');
              },
              success: function (response) {
                if(response.success == true)
                {
                  toastr.success('Success!', 'Teacher Activated Successfully!!!',{"positionClass": "toast-bottom-right"});
                  $('.table-departments').DataTable().ajax.reload();
                  $('#loader_modal').modal('hide');
                }
               
              },
              error: function(request, status, error){
                $('#loader_modal').modal('hide');
              }
            });
            }
            else{
              swal("Cancelled", "", "error");
              
            }
          });
  });

  $('#image').on('change', function() {
        let size = this.files[0].size; // this is in bytes
        if (size > 1000000) {
        // do something. Prevent form submit. Show message, etc.
          toastr.error('Sorry!', 'Student Photo Size Must Be Less Than 1MB!!!',{"positionClass": "toast-bottom-right"});

        }
        else
        {
          $('.update_image').removeAttr('disabled');
        }
    });

  $(document).on('click','.delete_department',function(){
      var id = $(this).data('id');

        swal({
            title: "Are You Sure!",
            html: true,
            text: "<b>You want to delete this department !!!</b>",
            type: "info",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes!",
            cancelButtonText: "No!",
            closeOnConfirm: true,
            closeOnCancel: false
          },
            function(isConfirm) {
            if (isConfirm){
             $.ajaxSetup({
              headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
              }
            });
            $.ajax({
              url: "{{ route('delete-department') }}",
              method: 'post',
              data: {id:id},
              beforeSend: function(){
                $('#loader_modal').modal({
                  backdrop: 'static',
                  keyboard: false
                });
                $('#loader_modal').modal('show');
              },
              success: function (response) {

                if(response.success == true)
                {
                  toastr.success('Success!', 'Record Deleted Successfully!!!',{"positionClass": "toast-bottom-right"});
                  $('.table-departments').DataTable().ajax.reload();
                  $('#loader_modal').modal('hide');
                }
                else if(response.success == false)
                {
                  toastr.info('Sorry!', 'Cannot delete department as it is already bond to other record(s)!!!',{"positionClass": "toast-bottom-right"});
                  $('.table-departments').DataTable().ajax.reload();
                  $('#loader_modal').modal('hide');
                }
                else if(response.success == false)
                {
                  toastr.error('Error!', 'Record does not exist!!!',{"positionClass": "toast-bottom-right"});
                  $('.table-departments').DataTable().ajax.reload();
                  $('#loader_modal').modal('hide');
                }
               
              },
              error: function(request, status, error){
                $('#loader_modal').modal('hide');
              }
            });
            }
            else{
              swal("Cancelled", "", "error");
              
            }
          });
  });

  $('.charge_salary').on('click',function(e){
    if($('#fee_month').val() == ''){
      toastr.info('Sorry!', 'Please select salary month!!!',{"positionClass": "toast-bottom-right"});
      return false;
    }
    var selected_fees = [];
    $("input.check:checked").each(function() {
      selected_fees.push($(this).val());
    });

    if(selected_fees == ''){
            toastr.error('Error!', 'Select Teacher(s) First.',{"positionClass": "toast-bottom-right"});
        return false;
        }else{
        $('.fees_selected').val(selected_fees);
        var ids = $('.fees_selected').val();
        }
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
      }
    });
    $.ajax({
      url: "{{ route('charge-teachers-salary') }}",
      method: 'get',
      data: {salary_month: $('#fee_month').val(),ids: ids},
      beforeSend: function(){
        $('#loader_modal').modal({
          backdrop: 'static',
          keyboard: false
        });
        $('#loader_modal').modal('show');
      },
      success: function (response) {

        if(response.success == true)
        {
          toastr.success('Success!', 'Operation done Successfully!!!',{"positionClass": "toast-bottom-right"});
          $('.table-departments').DataTable().ajax.reload();
          $('#loader_modal').modal('hide');
        }
        else if(response.success == false)
        {
          toastr.info('Sorry!', 'something went wrong!!!',{"positionClass": "toast-bottom-right"});
          $('.table-departments').DataTable().ajax.reload();
          $('#loader_modal').modal('hide');
        }
       
      },
      error: function(request, status, error){
        $('#loader_modal').modal('hide');
      }
    });
  })
});
</script>
@stop