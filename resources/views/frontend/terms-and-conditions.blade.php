{{-- @extends('layouts.app') --}}
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- Meta Tags for SEO -->
    <title>Terms and Conditions | School Management System | All-in-One Educational Platform</title>

    <!-- Meta Tags for SEO -->
    <meta name="keywords" content="Terms and Conditions, school management software, school management system, education management, online school platform, student management, teacher management, school admin system, academic platform">
    <meta name="description" content="Read the Terms and Conditions of our all-in-one School Management System. Learn how we provide a secure, efficient, and comprehensive platform for managing school administration, students, and educational operations.">
    
    <!-- Open Graph Meta Tags for social media -->
    <meta property="og:title" content="Terms and Conditions | School Management System | All-in-One Educational Platform">
    <meta property="og:description" content="Explore the Terms and Conditions of our School Management System. Understand how we ensure secure data handling, efficient management, and streamlined educational operations through our platform.">
    <meta property="og:image" content="{{asset('public/assets/img/page-bg.jpg')}}"> <!-- Replace with an actual image URL -->
    <meta property="og:url" content="{{ route('terms-and-conditions')}}">
    
    <!-- Twitter Card Meta Tags -->
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="Terms and Conditions | School Management System | All-in-One Educational Platform">
    <meta name="twitter:description" content="Read our Terms and Conditions to learn how we manage data, ensure privacy, and provide educational tools for seamless school administration.">
    <meta name="twitter:image" content="{{asset('public/assets/img/page-bg.jpg')}}"> <!-- Replace with an actual image URL -->
    
    <link href="https://fonts.googleapis.com/css2?family=Karla:ital,wght@0,200..800;1,200..800&family=Montserrat:ital,wght@0,100..900;1,100..900&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">
    <link href="{{asset('public/css/bootstrap.css')}}" rel="stylesheet" />
    <link href="{{asset('public/css/frontend.css?')}}{{time()}}" rel="stylesheet" />
    @include('layouts.google-analytics')
</head>
<body>
  @include('frontend.header')

  <div class="container  py-5">
<!-- Terms and Conditions Content -->
    <h1 class="text-center">Terms and Conditions</h1>
    <p class="text-center"><em>Last updated: December 24, 2024</em></p>

    <div class="mt-4">
        <h4>1. Introduction</h4>
        <p>Welcome to the School Management System! By accessing and using our platform, you agree to comply with the following terms and conditions. Please read them carefully before using our services.</p>
    </div>

    <div class="mt-4">
        <h4>2. Use of Our Service</h4>
        <p>You must use the School Management System in accordance with these Terms and Conditions. You may not use the platform for any illegal, unauthorized, or harmful purpose. You agree not to engage in any activity that may harm the system or its users.</p>
    </div>

    <div class="mt-4">
        <h4>3. User Responsibilities</h4>
        <p>As a user of our platform, you are responsible for maintaining the confidentiality of your account information. You must promptly notify us if you believe your account has been compromised. You are also responsible for the accuracy of the data you input into the system.</p>
    </div>

    <div class="mt-4">
        <h4>4. Privacy Policy</h4>
        <p>We are committed to protecting your privacy. Please review our <a class="text-link" href="{{ route('privacy-policy') }}">Privacy Policy</a> for information on how we collect, use, and protect your personal data.</p>
    </div>

    <div class="mt-4">
        <h4>5. Data Protection</h4>
        <p>We take the security of your data seriously and implement appropriate measures to protect your personal information. However, we cannot guarantee the security of data transmitted over the internet. By using our platform, you acknowledge and accept the risks associated with online data transmission.</p>
    </div>

    <div class="mt-4">
        <h4>6. Limitation of Liability</h4>
        <p>Our liability is limited to the fullest extent permitted by law. We are not responsible for any damages, losses, or expenses resulting from your use of the platform, including but not limited to data loss, system failures, or any form of disruption.</p>
    </div>

    <div class="mt-4">
        <h4>7. Modifications to the Terms</h4>
        <p>We reserve the right to modify these Terms and Conditions at any time. We will notify you of any changes by updating the "Effective Date" at the top of this page. Continued use of the platform after any changes constitutes acceptance of the modified terms.</p>
    </div>

    <div class="mt-4">
        <h4>8. Termination</h4>
        <p>We reserve the right to suspend or terminate your access to the platform if we believe you have violated these Terms and Conditions or engaged in unlawful activities. Upon termination, your rights to access the platform will be revoked.</p>
    </div>

    <div class="mt-4">
        <h4>9. Governing Law</h4>
        <p>These Terms and Conditions will be governed by and construed in accordance with the laws of [Your Jurisdiction]. Any disputes arising under these Terms will be subject to the exclusive jurisdiction of the courts in [Your Jurisdiction].</p>
    </div>

    <div class="mt-4">
        <h4>10. Contact Us</h4>
        <p>If you have any questions or concerns regarding these Terms and Conditions, please contact us at:</p>
        <ul>
            <li><strong>School Management System</strong></li>
            <li>Email: <strong><a href="mailto:info@forschoolmanagement.com">info@forschoolmanagement.com</a></strong></li>
            <li>Phone: <strong>+92-307-5943188</strong></li>
        </ul>
    </div>
</div>
    
  @include('frontend.footer')

</body>
</html>
