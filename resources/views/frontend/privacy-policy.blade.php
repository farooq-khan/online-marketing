{{-- @extends('layouts.app') --}}
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Privacy Policy | School Management System | All-in-One Educational Platform</title>

    <!-- Meta Tags for SEO -->
    <meta name="keywords" content="Privacy Policy, school management software, school management system, student information system, online education, school administration, school management platform, academic management, teacher and student portal, secure school data management">
    <meta name="description" content="Read the privacy policy of our all-in-one School Management System. Learn how we protect your data, manage student records, track academic progress, and ensure privacy for teachers, students, and administrators.">
    
    <!-- Open Graph Meta Tags for social media -->
    <meta property="og:title" content="Privacy Policy | School Management System | All-in-One Educational Platform">
    <meta property="og:description" content="Discover the privacy policy of our School Management System. We prioritize the security and privacy of your data while providing comprehensive features for school administration, student management, and educational success.">
    <meta property="og:image" content="{{asset('public/assets/img/page-bg.jpg')}}"> <!-- Replace with an actual image URL -->
    <meta property="og:url" content="{{ route('privacy-policy')}}"">
    
    <!-- Twitter Card Meta Tags -->
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="Privacy Policy | School Management System | All-in-One Educational Platform">
    <meta name="twitter:description" content="Learn about our commitment to data protection in our School Management System. Read our privacy policy outlining data security measures and user rights.">
    <meta name="twitter:image" content="{{asset('public/assets/img/page-bg.jpg')}}"> <!-- Replace with an actual image URL -->
    
    <link href="https://fonts.googleapis.com/css2?family=Karla:ital,wght@0,200..800;1,200..800&family=Montserrat:ital,wght@0,100..900;1,100..900&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">
    <link href="{{asset('public/css/bootstrap.css')}}" rel="stylesheet" />
    <link href="{{asset('public/css/frontend.css?')}}{{time()}}" rel="stylesheet" />
    @include('layouts.google-analytics')
</head>
<body>
  @include('frontend.header')

  <div class="container  py-5">

<!-- Privacy Policy Page Content -->
    <h1 class="text-center">Privacy Policy</h1>
    <p class="text-center"><em>Last updated: December 24, 2024</em></p>

    <div class="mt-4">
        <h4>1. Information We Collect</h4>
        <p>We collect the following types of personal data:</p>
        <ul>
            <li><strong>Personal Information:</strong> When you register on our website, we may collect your name, email address, phone number, and other necessary contact details.</li>
            <li><strong>User-Generated Content:</strong> Information you input while using the platform, such as student data, grades, assignments, and class materials.</li>
            <li><strong>Technical Data:</strong> This includes IP addresses, browser types, operating systems, pages viewed, and time spent on pages, which we collect through cookies and tracking technologies to improve our services.</li>
        </ul>
    </div>

    <div class="mt-4">
        <h4>2. How We Use Your Information</h4>
        <p>We use the collected information in the following ways:</p>
        <ul>
            <li>To provide, maintain, and improve our Service.</li>
            <li>To communicate with you, including customer support, service updates, and promotional information.</li>
            <li>To process payments, if applicable.</li>
            <li>To enhance the user experience and optimize website functionality.</li>
            <li>To comply with legal requirements and protect the security of the website.</li>
        </ul>
    </div>

    <div class="mt-4">
        <h4>3. How We Share Your Information</h4>
        <p>We may share your personal information in the following situations:</p>
        <ul>
            <li><strong>Service Providers:</strong> We may share your data with third-party vendors who help us operate and improve the Service (e.g., payment processors, hosting providers).</li>
            <li><strong>Legal Requirements:</strong> We may disclose your information if required by law, or if we believe that such action is necessary to comply with legal processes, protect our rights, or prevent fraud.</li>
            <li><strong>Business Transfers:</strong> If our company is involved in a merger, acquisition, or sale of assets, your information may be transferred as part of that transaction.</li>
        </ul>
    </div>

    <div class="mt-4">
        <h4>4. Data Retention</h4>
        <p>We retain personal information only for as long as necessary to fulfill the purposes outlined in this Privacy Policy unless a longer retention period is required or permitted by law. After the retention period ends, your information will be securely deleted or anonymized.</p>
    </div>

    <div class="mt-4">
        <h4>5. Security of Your Information</h4>
        <p>We take reasonable precautions to protect your personal data from unauthorized access, alteration, or destruction. However, no data transmission over the internet or method of electronic storage can be guaranteed to be 100% secure. While we strive to protect your personal data, we cannot ensure its absolute security.</p>
    </div>

    <div class="mt-4">
        <h4>6. Cookies and Tracking Technologies</h4>
        <p>We use cookies and other tracking technologies to collect data about your interactions with our Service. Cookies help us improve your experience by remembering your preferences and optimizing our website. You can control cookie settings through your browser, but please note that disabling cookies may affect the functionality of our Service.</p>
    </div>

    <div class="mt-4">
        <h4>7. Your Data Protection Rights</h4>
        <p>Depending on your location, you may have the following rights regarding your personal data:</p>
        <ul>
            <li>The right to access and obtain a copy of your data.</li>
            <li>The right to rectify inaccurate or incomplete information.</li>
            <li>The right to request the deletion of your data, subject to certain conditions.</li>
            <li>The right to restrict or object to the processing of your data.</li>
            <li>The right to withdraw consent at any time (if processing is based on consent).</li>
        </ul>
        <p>To exercise these rights, please contact us at <strong><a href="mailto:info@forschoolmanagement.com">info@forschoolmanagement.com</a></strong>.</p>
    </div>

    <div class="mt-4">
        <h4>8. Third-Party Websites</h4>
        <p>Our Service may contain links to third-party websites or services that are not operated by us. We are not responsible for the privacy practices of these third-party sites. We recommend reviewing their privacy policies before submitting any personal information.</p>
    </div>

    <div class="mt-4">
        <h4>9. Changes to This Privacy Policy</h4>
        <p>We may update this Privacy Policy from time to time. When we make changes, we will update the "Effective Date" at the top of the policy. We encourage you to periodically review this Privacy Policy for any updates.</p>
    </div>

    <div class="mt-4">
        <h4>10. Contact Us</h4>
        <p>If you have any questions or concerns about this Privacy Policy or our data practices, please contact us at:</p>
        <ul>
            <li><strong>School Management System</strong></li>
            <li>Email: <strong><a href="mailto:info@forschoolmanagement.com">info@forschoolmanagement.com</a></strong></li>
            <li>Phone: <strong>+92-307-5943188</strong></li>
        </ul>
    </div>
  </div>
    
  @include('frontend.footer')
</body>
</html>
