{{-- @extends('layouts.app') --}}
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Contact Us | School Management System | Get in Touch with Us</title>

    <meta name="description" content="Have questions or need support? Contact the team behind the School Management System today. We're here to assist with product inquiries, technical support, and more.">

    <meta name="keywords" content="contact school management system, get in touch with school software, support for school management, customer service, contact form, technical support, education software support">

    <meta property="og:title" content="Contact Us | School Management System | All-in-One Educational Platform">
    <meta property="og:description" content="Reach out to the School Management System team for product inquiries, support, or technical assistance. We're here to help with all your school management needs.">
    <meta property="og:image" content="{{asset('public/assets/img/page-bg.jpg')}}"> <!-- Replace with an actual contact-related image -->
    <meta property="og:url" content="{{ route('contact-us')}}"> <!-- URL of your Contact Us page -->

    <meta name="twitter:title" content="Contact Us | School Management System | All-in-One Educational Platform">
    <meta name="twitter:description" content="Contact us for any inquiries, support, or assistance related to the School Management System. Our team is ready to help you.">
    <meta name="twitter:image" content="{{asset('public/assets/img/page-bg.jpg')}}"> <!-- Replace with an actual image -->
    <meta name="twitter:card" content="summary_large_image">


    <link href="https://fonts.googleapis.com/css2?family=Karla:ital,wght@0,200..800;1,200..800&family=Montserrat:ital,wght@0,100..900;1,100..900&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">
    <link href="{{asset('public/css/bootstrap.css')}}" rel="stylesheet" />
    <link href="{{asset('public/css/frontend.css?')}}{{time()}}" rel="stylesheet" />
    <!-- Google tag (gtag.js) -->
    @include('layouts.google-analytics')
</head>
<body>
  @include('frontend.header')

  <section class="faqs-banner d-flex align-items-center" style='background-image: url("{{asset('public/assets/img/contact-us-bg.png')}}"); background-size:cover; background-color: #f5f7fa;
}'>
    <div class="container py-5 my-md-5">
      <div class="col-12 px-0">
          <h1 class="mb-2">Contact us</h1>
          <p class="mb-0">Feel free to contact us and we will be happy to help you!</p>
        </div>
       </div>
  </section>

    <div class="container contact-us-form py-5 my-lg-3">
      <div class="row">
        <div class="col-lg-8 col-md-10 col-12 mx-auto">
          <form id="contactForm" class="p-md-5 p-4">
              @csrf
              {{-- <h3 class="mb-2 text-center">Drop Us a Message</h3>
              <p class="mb-0 text-center">We would love to hear from you!</p> --}}
              <div class="row">
                  <div class="col-sm-6 form-group">
                        <input type="text" name="txtName" class="form-control" placeholder="Your Name *">
                    </div>
                    <div class="col-sm-6 form-group">
                        <input type="text" name="txtEmail" class="form-control" placeholder="Your Email *">
                    </div>
                    <div class="col-12 form-group">
                        <input type="text" name="txtPhone" class="form-control" placeholder="Your Phone Number *">
                    </div>
                    <div class="col-12 form-group">
                        <textarea name="txtMsg" class="form-control" rows="4" placeholder="Your Message *"></textarea>
                    </div>
                </div>
                <div class="contact-button">
                    <button type="submit" class="btn btn-primary" id="submitContactForm">Send Message</button>
                </div>
                <div class="alert alert-success alert-dismissible d-none mt-4 mb-0">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <strong>Thank you!</strong> We have received your email. We will contact you <strong>ASAP!</strong>
                </div>
           </form>
        </div>
        <div class="col-md-4 col-12 d-none">
            <!-- Phones -->
            <div class="mb-3">
                <h3 class="h6 mb-2">Call us directly</h3>
                <ul class="list-unstyled mb-0">
                  <li><a href="https://wa.me/03075943188?text=Hello,%20I%20need%20assistance%20with%20your%20School%20Management%20Software." class="text-hover stretched-link">+92-307-5943188</a></li>
                </ul>
            </div>
            <!-- Working hours -->
            <div class="mb-3">
                <h3 class="h6 mb-2">Working hours</h3>
                <ul class="list-unstyled mb-0">
                  <li>Mon - Fri  10:00 - 10:00</li>
                  <li>Sut - Sun  10:00 - 10:00</li>
                </ul>
            </div>
            <!-- Location -->
            <div class="mb-3">
                <h3 class="h6 mb-2">Location</h3>
                <ul class="list-unstyled mb-0">
                  <li>Saddar, Rawalpindi, Punjab 46000</li>
                </ul>
            </div>
        </div>
      </div>
    </div>

  <div class="contact-form py-5 d-none">
    <div class="container my-lg-4">
      {{-- <div class="contact-image">
          <img src="https://image.ibb.co/kUagtU/rocket_contact.png" alt="rocket_contact">
      </div> --}}
      <form id="contactForm" class="col-lg-8 col-md-9 col-sm-11 col-12 mt-4 mx-auto pt-md-3 px-0">
        @csrf
        <h3 class="mb-2 text-center">Drop Us a Message</h3>
        <p class="mb-0 text-center">We would love to hear from you!</p>
        <div class="row mt-4 pt-md-2">
              <div class="col-md-6 form-group">
                    <input type="text" name="txtName" class="form-control" placeholder="Your Name *">
                </div>
                <div class="col-md-6 form-group">
                    <input type="text" name="txtEmail" class="form-control" placeholder="Your Email *">
                </div>
                <div class="col-md-6 form-group">
                    <input type="text" name="txtPhone" class="form-control" placeholder="Your Phone Number *">
                </div>
                <div class="col-12 form-group">
                    <textarea name="txtMsg" class="form-control" placeholder="Your Message *" style="width: 100%; height: 150px;"></textarea>
                </div>
            </div>
            <div class="col-12">
                <button type="submit" class="btn btn-primary" id="submitContactForm">Send Message</button>
            </div>
            <div class="col-12 mt-3">
              <div class="alert alert-success alert-dismissible d-none">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <strong>Thank you!</strong> We have received your email. We will contact you <strong>ASAP!</strong>
              </div>
          </div>
        </div>
    </form>
    </div>
  </div>
    
  @include('frontend.footer')
  <script type="text/javascript">
    $(document).ready(function () {
        $('#contactForm').on('submit', function (e) {
            e.preventDefault();

            let formData = $(this).serialize();

            $.ajax({
                url: "{{ route('contact.submit') }}",
                type: "POST",
                data: formData,
                beforeSend: function(){
                  $('#submitContactForm').attr('disabled', true);
                  $('#submitContactForm').html('Please wait ...');
                  $('.alert-success').addClass('d-none');
                },
                success: function (response) {
                    if (response.success) {
                        $('#submitContactForm').attr('disabled', false);
                        $('#submitContactForm').html('Send Message');
                        $('#contactForm')[0].reset();
                        $('.alert-success').removeClass('d-none');
                    } else {
                    }
                },
                error: function (xhr) {
                  $('#submitContactForm').attr('disabled', false);
                  $('#submitContactForm').html('Send Message');
                    let errors = xhr.responseJSON.errors;
                    let errorMessages = '';
                    $.each(errors, function (field, messages) {
                        var input = $('[name="' + field + '"]');
                        var errorList = $('<ul class="text-danger form-errors mb-0 pl-0 text-end"></ul>');
                        $.each(messages, function (index, message) {
                            errorList.append($('<span></span>').text(message));
                        });
                        input.after(errorList);
                    });
                    return;
                }
            });
        });
    });
  </script>
</body>
</html>
