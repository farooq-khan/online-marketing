{{-- @extends('layouts.app') --}}
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- Meta Tags for SEO -->
    <title>Pricing Plans | School Management System | Flexible & Affordable Options</title>
<meta name="description" content="Discover our flexible pricing plans for the School Management System. Choose from a free trial, monthly, or yearly plan, all packed with powerful features for schools.">
<meta name="keywords" content="school management system pricing, school software plans, affordable school software, trial school software, monthly school system, yearly school management">
<meta property="og:title" content="Pricing Plans | School Management System | Flexible & Affordable Options">
<meta property="og:description" content="Choose the perfect pricing plan for your school. Explore our trial, monthly, and yearly options with comprehensive features to streamline school operations.">
<meta property="og:image" content="{{asset('public/assets/img/pricing-plans.png')}}">
<meta property="og:type" content="website">
<meta property="og:url" content="{{ route('pricing-plans')}}">
<meta name="author" content="Your School Management System">
<meta name="robots" content="index, follow">


    <title>FAQ | School Management System | All-in-One Educational Platform</title>
    <meta name="description" content="Find answers to the most frequently asked questions about our School Management System. Get details on features, pricing, and more.">
    <meta name="keywords" content="school management system, FAQs, school software, education management, support, school administration">
    <meta property="og:title" content="FAQ | School Management System | All-in-One Educational Platform">
    <meta property="og:description" content="Find answers to common questions about our School Management System. Learn more about features, pricing, and support.">
    <meta property="og:image" content="{{asset('public/assets/img/page-bg.jpg')}}">
    
    
    <link href="https://fonts.googleapis.com/css2?family=Karla:ital,wght@0,200..800;1,200..800&family=Montserrat:ital,wght@0,100..900;1,100..900&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700;800;900&display=swap" rel="stylesheet">

    {{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css"> --}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.7.2/css/all.min.css">

    <link href="{{asset('public/css/bootstrap.css')}}" rel="stylesheet" />
    <link href="{{asset('public/css/frontend.css?')}}{{time()}}" rel="stylesheet" />
    @include('layouts.google-analytics')
</head>
<body>
  @include('frontend.header')

  <section class="faqs-banner d-flex align-items-center" style='background-image: url("{{asset('public/assets/img/banner-for-school-management-software.jpg')}}"); background-size:cover'>
    <div class="container py-5 my-md-5">
        <div class="col-12 col-lg-10 mx-auto px-0 text-center text-white">
          <h1 class="mb-2">School Management Software Pricing Plans</h1>
          <p class="mb-4">Flexible plans to suit your school's needs</p>
          <button class="btn btn-primary py-2">Get Started Today</button>
        </div>
    </div>
  </section>

    <!-- Pricing Plans -->
    <div class="container py-5 my-4">
        <div class="plan-header pb-4 mb-3 text-center">
            <h2 class="font-weight-bold mb-2 pb-1">Affordable Plans for Every School</h2>
            <p class="font-weight-bold mb-0">Choose a plan that fits your school’s needs and budget.</p>
        </div>
        <div class="row mx-n2 pricing-plan-row small">
            <!-- Basic Plan -->
            <div class="col-md-4 pricing-plan px-2 d-flex">
                <div class="card-body flex-grow-1">
                        <h4>Free Plan</h4>
                        <p class="font-italic font-weight-semibold plan-sub-title text-italic">"Experience the power of our system with no cost for 4 months. Perfect for schools starting their digital journey."</p>
                        <h5 class="card-title text-center font-weight-medium">4-Month Trial</h5>
                        <div class="font-weight-bold text-left mb-2">Features Included:</div>
                        <ul class="list-unstyled text-left">
                            <li><strong>Students:</strong> Manage profiles, academic records, attendance, and progress.</li>
                            <li><strong>Teachers:</strong> Maintain teacher profiles and qualifications.</li>
                            <li><strong>Attendance:</strong> Track daily attendance for students and staff.</li>
                            <li><strong>Fee Management:</strong> Billing, invoicing, and payment tracking.</li>
                            <li><strong>Results:</strong> Grade management, report cards, and performance analysis.</li>
                            <li><strong>Subjects:</strong> Manage subjects, curriculum, and syllabus coverage.</li>
                            <li><strong>Exams:</strong> Schedule exams, track results, and analyze performance.</li>
                            <li><strong>Classes:</strong> Organize class schedules, teacher assignments, and classroom allocation.</li>
                            <li><strong>Certificates:</strong> Generate certificates for achievements and completions.</li>
                            <li><strong>Book & Library:</strong> Manage library inventory, check-ins, check-outs, and fines.</li>
                            <li><strong>Expenses:</strong> Track school expenditures and budget planning.</li>
                            <li><strong>Accounts:</strong> Manage financial accounts, income, and reporting.</li>
                            <li><strong>Configuration:</strong> Customize user roles, permissions, and settings.</li>
                        </ul>
                        <div class="font-weight-bold text-left">Limitations:</div>
                        <p class=" text-left">Basic support only.</p>
                        <a href="#" class="btn btn-outline-primary btn-block py-2">Get Started</a>
                </div>
            </div>
            <div class="col-md-4 pricing-plan px-2 d-flex best-plan-col">
                <div class="card-body flex-grow-1"> 
                    <span class="best-plan position-absolute">Best Plan</span>
                        <h4>Monthly Plan</h4>
                        <p class="font-italic font-weight-semibold plan-sub-title text-italic">"Enjoy full access to all features with a flexible monthly payment option. Ideal for small to medium schools."</p>
                        <h5 class="card-title text-center font-weight-medium">1,000 PKR</h5>
                        <div class="font-weight-bold text-left mb-2">Features Included:</div>
                        <ul class="list-unstyled text-left">
                            <li><strong>Students:</strong> Manage profiles, academic records, attendance, and progress.</li>
                            <li><strong>Teachers:</strong> Maintain teacher profiles and qualifications.</li>
                            <li><strong>Attendance:</strong> Track daily attendance for students and staff.</li>
                            <li><strong>Fee Management:</strong> Billing, invoicing, and payment tracking.</li>
                            <li><strong>Results:</strong> Grade management, report cards, and performance analysis.</li>
                            <li><strong>Subjects:</strong> Manage subjects, curriculum, and syllabus coverage.</li>
                            <li><strong>Exams:</strong> Schedule exams, track results, and analyze performance.</li>
                            <li><strong>Classes:</strong> Organize class schedules, teacher assignments, and classroom allocation.</li>
                            <li><strong>Certificates:</strong> Generate certificates for achievements and completions.</li>
                            <li><strong>Book & Library:</strong> Manage library inventory, check-ins, check-outs, and fines.</li>
                            <li><strong>Expenses:</strong> Track school expenditures and budget planning.</li>
                            <li><strong>Accounts:</strong> Manage financial accounts, income, and reporting.</li>
                            <li><strong>Configuration:</strong> Customize user roles, permissions, and settings.</li>
                        </ul>
                        <div class="font-weight-bold text-left">Limitations:</div>
                        <p class=" text-left">Basic support only.</p>
                        <a href="#" class="btn btn-primary btn-block py-2">Get Started</a>
                </div>
            </div>

            <!-- Standard Plan -->
            <div class="col-md-4 pricing-plan px-2 d-flex">
                <div class="card-body flex-grow-1">
                        <h4>Yearly Plan</h4>
                        <p class="font-italic font-weight-semibold plan-sub-title text-italic">"Save more with our yearly plan while managing your school efficiently. Best value for long-term users!"</p>
                        <h5 class="card-title text-center font-weight-medium">9,000 PKR</h5>
                        <div class="font-weight-bold text-left mb-2">Features Included:</div>
                        <ul class="list-unstyled text-left">
                            <li><strong>Students:</strong> Manage profiles, academic records, attendance, and progress.</li>
                            <li><strong>Teachers:</strong> Maintain teacher profiles and qualifications.</li>
                            <li><strong>Attendance:</strong> Track daily attendance for students and staff.</li>
                            <li><strong>Fee Management:</strong> Billing, invoicing, and payment tracking.</li>
                            <li><strong>Results:</strong> Grade management, report cards, and performance analysis.</li>
                            <li><strong>Subjects:</strong> Manage subjects, curriculum, and syllabus coverage.</li>
                            <li><strong>Exams:</strong> Schedule exams, track results, and analyze performance.</li>
                            <li><strong>Classes:</strong> Organize class schedules, teacher assignments, and classroom allocation.</li>
                            <li><strong>Certificates:</strong> Generate certificates for achievements and completions.</li>
                            <li><strong>Book & Library:</strong> Manage library inventory, check-ins, check-outs, and fines.</li>
                            <li><strong>Expenses:</strong> Track school expenditures and budget planning.</li>
                            <li><strong>Accounts:</strong> Manage financial accounts, income, and reporting.</li>
                            <li><strong>Configuration:</strong> Customize user roles, permissions, and settings.</li>
                        </ul>
                        <div class="font-weight-bold text-left">Limitations:</div>
                        <p class=" text-left">Basic support only.</p>
                        <a href="#" class="btn btn-outline-primary btn-block py-2">Get Started</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('frontend.footer')
</body>
</html>
