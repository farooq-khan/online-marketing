<header class="py-md-2 py-1 position-sticky">
    <div class="container">
      <div class="align-items-center row">
        <div class="col-auto d-lg-block d-none logo">
          {{-- <a href="{{ route('fronted-home')}}" class="font-weight-bold">School Management System</a> --}}
          <a href="{{ route('fronted-home')}}" class="font-weight-bold">
          {{-- <img src="{{asset('public/assets/img/school-management-software-logo.png')}}" alt=""> --}}
          <img src="{{asset('public/assets/img/school-management-software-logo-2.png')}}" alt="school management software logo" class="img-fluid">
        </a>
        </div>
        <nav class="col navbar navbar-expand-lg main-menu">
            <a class="navbar-brand d-lg-none py-0" href="{{ route('fronted-home')}}">          
                <img src="{{asset('public/assets/img/school-management-software-logo-2.png')}}" alt="school management software logo" class="img-fluid">
            </a>
            @if (!Auth::check())
                <a href="{{ route('get-started') }}" class="btn btn-outline-primary d-lg-none px-sm-3 px-2 ml-auto">Register</a>
                <a  href="{{ route('login') }}" class="btn btn-primary ml-1 px-sm-3 px-2 d-lg-none">Login</a>
            @else
                <a  href="{{ route('dashboard') }}" class="btn btn-primary d-lg-none px-sm-3 px-2 ml-auto">View Dashboard</a>
            @endif
            <button class="d-lg-none fa fa-bars navbar-toggler ml-2" type="button" data-toggle="collapse" data-target="#collapsibleNavId" aria-controls="collapsibleNavId"
                aria-expanded="false" aria-label="Toggle navigation"></button>
            <div class="collapse navbar-collapse" id="collapsibleNavId">
                <ul class="navbar-nav mx-auto mt-2 mt-lg-0">
                    <li class="nav-item">
                        <a class="nav-link" href="/#about-us" title="About us">About us</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('features')}}" title="Features">Features</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('frequestly-asked-questions')}}" title="Frequestly asked Questions">FAQs</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('pricing-plans')}}" title="Pricing Plans">Pricing</a>
                    </li>
                    {{-- <li class="nav-item">
                        <a class="nav-link" href="#our-clients">Our clients</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#testimonials">Testimonials</a>
                    </li> --}}
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('contact-us') }}" title="Contact us">Contact us</a>
                    </li>
                </ul>
            </div>
        </nav>
        <div class="col-auto top-buttons text-right d-lg-block d-none">
          @if (!Auth::check())
            <a  href="{{ route('get-started') }}" class="btn btn-outline-primary">Register</a>
            <a  href="{{ route('login') }}" class="btn btn-primary">Login</a>
          @else
            <a  href="{{ route('dashboard') }}" class="btn btn-primary">View Dashboard</a>
          @endif
        </div>
      </div>
    </div>
</header>