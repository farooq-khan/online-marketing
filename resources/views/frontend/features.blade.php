{{-- @extends('layouts.app') --}}
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- Meta Tags for SEO -->
    <title>Features | School Management System | All-in-One Educational Platform</title>

    <meta name="description" content="Explore the powerful features of our School Management System. From attendance tracking and grade management to scheduling and communication tools, our system simplifies school operations.">
    <meta name="keywords" content="school management software, education management system, school features, grade management, attendance tracking, school scheduling, communication tools, student information system, teacher management, school dashboard">

    <meta property="og:title" content="Features | School Management System | All-in-One Educational Platform">
    <meta property="og:description" content="Discover the features of our School Management System. Manage attendance, grades, schedules, and communication all in one platform for improved school operations.">
    <meta property="og:image" content="{{asset('public/assets/img/page-bg.jpg')}}"> <!-- Change to an appropriate image for the page -->
    <meta property="og:url" content="{{ route('features')}}"> <!-- URL of the Features page -->

    <meta name="twitter:title" content="Features | School Management System | All-in-One Educational Platform">
    <meta name="twitter:description" content="Learn about the key features of our School Management System. Simplify your school operations with powerful tools for attendance, grades, scheduling, and more.">
    <meta name="twitter:image" content="{{asset('public/assets/img/page-bg.jpg')}}"> <!-- Change to an appropriate image for the page -->
    <meta name="twitter:card" content="summary_large_image">
    
    <link href="https://fonts.googleapis.com/css2?family=Karla:ital,wght@0,200..800;1,200..800&family=Montserrat:ital,wght@0,100..900;1,100..900&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">
    <link href="{{asset('public/css/bootstrap.css')}}" rel="stylesheet" />
    <link href="{{asset('public/css/frontend.css?')}}{{time()}}" rel="stylesheet" />
    @include('layouts.google-analytics')
</head>
<body>
  @include('frontend.header')

  <section class="features py-lg-4 py-1 text-center" id="features">
    <div class="container py-5">
      <div class="row">
        <div class="col-xl-10 col-lg-11 col-12 mx-auto feature-col pb-2">
          <h1 class="section-title">Features</h1>
          <p class="sub-heading mb-0">Explore our comprehensive suite of tools designed to streamline school management. From student and teacher administration to fee handling and exam scheduling, our features ensure efficient and effective school operations. Discover how our software enhances productivity and simplifies daily tasks.</p>
        </div>
      </div>
      <div class="mx-lg-n3 mx-n2 row row-cols-1 row-cols-lg-3 row-cols-md-3 row-cols-sm-2 justify-content-center">
        <div class="col feature-col mt-lg-4 mt-3 d-flex pt-5 px-lg-3 px-2">
          <div class="card flex-fill px-3 pb-3 px-lg-4 pb-lg-4">
            <i class="fas fa-2x fa-users mb-3"></i>
            <h4 class="mb-2 pb-1">Students</h4>
            <p class="mb-0">Manages student profiles, academic records, attendance, and progress</p>
          </div>
        </div>
        <div class="col feature-col mt-lg-4 mt-3 d-flex pt-5 px-lg-3 px-2">
          <div class="card flex-fill px-3 pb-3 px-lg-4 pb-lg-4">
            <i class="fas fa-2x fa-chalkboard-teacher mb-3"></i>
            <h4 class="mb-2 pb-1">Teachers</h4>
            <p class="mb-0">Maintains teacher profiles, qualifications, and class assignments</p>
          </div>
        </div>
        <div class="col feature-col mt-lg-4 mt-3 d-flex pt-5 px-lg-3 px-2">
          <div class="card flex-fill px-3 pb-3 px-lg-4 pb-lg-4">
            <i class="fas fa-2x fa-clock mb-3"></i>
            <h4 class="mb-2 pb-1">Attendance</h4>
            <p class="mb-0">Tracks and records daily attendance for students and staff.</p>
          </div>
        </div>
        <div class="col feature-col mt-lg-4 mt-3 d-flex pt-5 px-lg-3 px-2">
          <div class="card flex-fill px-3 pb-3 px-lg-4 pb-lg-4">
            <i class="fas fa-2x fa-money-check-alt mb-3"></i>
            <h4 class="mb-2 pb-1">Fee</h4>
            <p class="mb-0">Handles billing, invoicing, fee collection, and payment tracking.</p>
          </div>
        </div>
        <div class="col feature-col mt-lg-4 mt-3 d-flex pt-5 px-lg-3 px-2">
          <div class="card flex-fill px-3 pb-3 px-lg-4 pb-lg-4">
            <i class="fas fa-2x fa-poll mb-3"></i>
            <h4 class="mb-2 pb-1">Results</h4>
            <p class="mb-0">Manages grades, generates report cards, and analyzes student performance.</p>
          </div>
        </div>
        <div class="col feature-col mt-lg-4 mt-3 d-flex pt-5 px-lg-3 px-2">
          <div class="card flex-fill px-3 pb-3 px-lg-4 pb-lg-4">
            <i class="fas fa-2x fa-school mb-3"></i>
            <h4 class="mb-2 pb-1">Classes</h4>
            <p class="mb-0">Organizes class schedules, teacher assignments, and classroom allocations.</p>
          </div>
        </div>
        <div class="col feature-col mt-lg-4 mt-3 d-flex pt-5 px-lg-3 px-2">
          <div class="card flex-fill px-3 pb-3 px-lg-4 pb-lg-4">
            <i class="fas fa-2x fa-book mb-3"></i>
            <h4 class="mb-2 pb-1">Subjects</h4>
            <p class="mb-0">Manages subjects, curriculum, and syllabus coverage.</p>
          </div>
        </div>
        <div class="col feature-col mt-lg-4 mt-3 d-flex pt-5 px-lg-3 px-2">
          <div class="card flex-fill px-3 pb-3 px-lg-4 pb-lg-4">
            <i class="fas fa-2x fa-pager mb-3"></i>
            <h4 class="mb-2 pb-1">Exams</h4>
            <p class="mb-0">Schedules and administers exams, tracks results, and analyzes performance.</p>
          </div>
        </div>
        <div class="col feature-col mt-lg-4 mt-3 d-flex pt-5 px-lg-3 px-2">
          <div class="card flex-fill px-3 pb-3 px-lg-4 pb-lg-4">
            <i class="fas fa-2x fa-certificate mb-3"></i>
            <h4 class="mb-2 pb-1">Certificates</h4>
            <p class="mb-0">Generates and issues certificates for achievements and completions.</p>
          </div>
        </div>
        <div class="col feature-col mt-lg-4 mt-3 d-flex pt-5 px-lg-3 px-2">
          <div class="card flex-fill px-3 pb-3 px-lg-4 pb-lg-4">
            <i class="fas fa-2x fa-hand-holding-usd mb-3"></i>
            <h4 class="mb-2 pb-1">Expenses</h4>
            <p class="mb-0">Tracks school expenditures, generates expense reports, and aids budget planning.</p>
          </div>
        </div>
        <div class="col feature-col mt-lg-4 mt-3 d-flex pt-5 px-lg-3 px-2">
          <div class="card flex-fill px-3 pb-3 px-lg-4 pb-lg-4">
            <i class="fas fa-2x fa-book mb-3"></i>
            <h4 class="mb-2 pb-1">Book &amp; Library</h4>
            <p class="mb-0">Manages library inventory, check-ins, check-outs, and fines.</p>
          </div>
        </div>
        <div class="col feature-col mt-lg-4 mt-3 d-flex pt-5 px-lg-3 px-2">
          <div class="card flex-fill px-3 pb-3 px-lg-4 pb-lg-4">
            <i class="fas fa-2x fa-cogs mb-3"></i>
            <h4 class="mb-2 pb-1">Configuration</h4>
            <p class="mb-0">Customizes software settings, user roles, and permissions.</p>
          </div>
        </div>
        <div class="col feature-col mt-lg-4 mt-3 d-flex pt-5 px-lg-3 px-2">
          <div class="card flex-fill px-3 pb-3 px-lg-4 pb-lg-4">
            <i class="fas fa-2x fa-users mb-3"></i>
            <h4 class="mb-2 pb-1">Accounts</h4>
            <p class="mb-0">Manages financial accounts, income, expenses, and reporting.</p>
          </div>
        </div>
      </div>
    </div>
  </section>
    
  @include('frontend.footer')
</body>
</html>
