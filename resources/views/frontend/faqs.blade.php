{{-- @extends('layouts.app') --}}
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- Meta Tags for SEO -->
    <title>FAQ | School Management System | All-in-One Educational Platform</title>
    <meta name="description" content="Find answers to the most frequently asked questions about our School Management System. Get details on features, pricing, and more.">
    <meta name="keywords" content="school management system, FAQs, school software, education management, support, school administration">
    <meta property="og:title" content="FAQ | School Management System | All-in-One Educational Platform">
    <meta property="og:description" content="Find answers to common questions about our School Management System. Learn more about features, pricing, and support.">
    <meta property="og:image" content="{{asset('public/assets/img/page-bg.jpg')}}">
    
    
    <link href="https://fonts.googleapis.com/css2?family=Karla:ital,wght@0,200..800;1,200..800&family=Montserrat:ital,wght@0,100..900;1,100..900&display=swap" rel="stylesheet">
    {{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css"> --}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.7.2/css/all.min.css">

    <link href="{{asset('public/css/bootstrap.css')}}" rel="stylesheet" />
    <link href="{{asset('public/css/frontend.css?')}}{{time()}}" rel="stylesheet" />
    @include('layouts.google-analytics')
</head>
<body>
  @include('frontend.header')

  <section class="faqs-banner d-flex align-items-center" style='background-image: url("{{asset('public/assets/img/frequestly-ask-questions.jpg')}}"); background-size:cover'>
    <div class="container py-5 my-md-5">
      <div class="col-12 px-0 text-center text-white">
          <h1 class="mb-2">Frequestly Asked Questions</h1>
          <p class="mb-0">Everything You Need to Know About Our School Management Software</p>
        </div>
       </div>
  </section>

  <main class="container faqs-section py-5 my-lg-5 d-flex flex-column">
        <div class="faqs-header text-center">
            <h1 class="mb-3">FAQs for Managing Your School with Ease</h1>
            <p class="mb-0 sub-heading">Find answers to common questions about using our School Management Software to streamline administration, enhance communication, and simplify school operations.</p>
        </div>
        <article class="faq-section">
            <h3 class="mb-0">General Questions</h3>
            <div class="faq">
                <h4>1. What is a School Management Software?</h4>
                <p clas>School Management Software is a digital platform designed to streamline administrative and academic processes in educational institutions.</p>
            </div>
            <div class="faq">
                <h4>2. Who can use this software?</h4>
                <p>This software is designed for school administrators, teachers, parents, and students, each with tailored functionality.</p>
            </div>
        </article>

        <article class="faq-section">
            <h3 class="mb-0">Features and Functionality</h3>
            <div class="faq">
                <h4>3. What features does the software offer?</h4>
                <p>The software offers features like student management, attendance tracking, fee management, timetable scheduling, and more.</p>
            </div>
            <div class="faq">
                <h4>4. Can the software handle multiple schools or campuses?</h4>
                <p>Yes, it supports multi-campus management, allowing administrators to oversee multiple schools from one platform.</p>
            </div>
            <div class="faq">
                <h4>5. Is the software customizable?</h4>
                <p>Yes, it can be tailored to meet your institution's specific needs, such as adding custom fields or integrations.</p>
            </div>
        </article>

        <article class="faq-section">
            <h3 class="mb-0">Technical Questions</h3>
            <div class="faq">
                <h4>6. Is the software cloud-based?</h4>
                <p>Yes, the software is cloud-based, ensuring accessibility from anywhere with an internet connection.</p>
            </div>
            <div class="faq">
                <h4>7. What are the system requirements?</h4>
                <p>The software works on any device with a modern web browsers, requiring minimal hardware.</p>
            </div>
            <div class="faq">
                <h4>8. Is data stored securely?</h4>
                <p>Yes, we use industry-standard encryption and regular backups to ensure data security.</p>
            </div>
        </article>

        <article class="faq-section">
            <h3 class="mb-0">Support and Training</h3>
            <div class="faq">
                <h4>9. What kind of support is available?</h4>
                <p>We provide 24/7 customer support via email, chat, and phone, along with a knowledge base and tutorials.</p>
            </div>
            <div class="faq">
                <h4>10. Is training provided for staff?</h4>
                <p>Yes, we offer comprehensive training sessions for administrators and teachers.</p>
            </div>
        </article>

        <article class="faq-section">
            <h3 class="mb-0">Pricing and Licensing</h3>
            <div class="faq">
                <h4>11. What is the pricing structure?</h4>
                <p>Our pricing model includes subscription plans based on the size and needs of your institution. <a href="{{ route('contact-us') }}" class="text-link text-underline font-weight-medium">Contact us</a> or <a style="color:#25D366" class="text-link text-underline font-weight-medium" href="https://wa.me/03075943188?text=Hello,%20I%20need%20assistance%20with%20your%20School%20Management%20Software.">Chat with Us on WhatsApp</a> for details.</p>
            </div>
            <div class="faq">
                <h4>12. Are there any hidden costs?</h4>
                <p>No, all costs are transparently outlined, including optional add-ons and integrations.</p>
            </div>
            <div class="faq">
                <h4>13. Can we get a free trial?</h4>
                <p>Yes, we offer a 30-day free trial for you to explore the software's features.</p>
            </div>
        </article>
    </main>
    
  @include('frontend.footer')
</body>
</html>
