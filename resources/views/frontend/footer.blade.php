<div class="text-center contact-us-details" style="background-color: #f0f4f7;">
    <div class="container">
      <div class="row py-md-0 py-3">
        <div class="col-md col-12 p-md-4 p-3 position-relative">
            <em style="color:#25D366;font-size: 1.5rem;border-color: #25D366;" class="fab fa-whatsapp"></em>
            <a href="https://wa.me/03075943188?text=Hello,%20I%20need%20assistance%20with%20your%20School%20Management%20Software." class="text-hover stretched-link">+92-307-5943188</a>
        </div>
        <div class="col-md col-12 p-md-4 p-3 position-relative">
            <em class="fa fa-envelope"></em>
            <a href="mailto:info@forschoolmanagement.com" class="text-break text-hover stretched-link">info@forschoolmanagement.com</a>
        </div>
        <div class="col-md col-12 p-md-4 p-3">
            <em class="fa fa-map"></em>
            <span>Saddar, Rawalpindi, Punjab 46000</span>
        </div>
      </div>
    </div>
  </div>
<footer class="footer pt-5" style="background-color: black;">
    <div class="container pb-4">
      <div class="row justify-content-center">
        <div class="col-md-12 text-center">
          <a class="footer-logo d-inline-block mb-4 pb-md-3 pb-2" href="{{ route('fronted-home')}}" title="School Management System">
            <img src="{{asset('public/assets/img/school-management-software-logo-white-2.png')}}" alt="school management software logo" class="img-fluid">
          </a>
  
          <ul class="d-flex flex-wrap justify-content-center list-unstyled mb-4 pb-md-3 pb-2 footer-menu" style="gap: 12px 30px;">
            <li><a href="{{ route('fronted-home')}}" title="Home">Home</a></li>
            <li><a href="/#about-us" title="About us">About us</a></li>
            <li><a href="{{ route('features')}}" title="Feature">Feature</a></li>
            <li><a href="{{ route('frequestly-asked-questions')}}" title="Frequestly asked Questions">FAQs</a></li>
            <li><a href="{{ route('pricing-plans')}}" title="Pricing Plans">Pricing</a></li>
            <li><a href="{{ route('contact-us') }}" title="Contact us">Contact us</a></li>
            <li><a href="{{ route('privacy-policy') }}" title="Privacy policy">Privacy policy</a></li>
            <li><a href="{{ route('terms-and-conditions') }}" title="Terms and Conditions">Terms and Conditions</a></li>
          </ul>
          <ul class="social-link mb-0 list-unstyled d-flex flex-wrap justify-content-center">
            <li><a href="https://www.facebook.com/forschoolmanagementsystem" target="_blank" title=" School Management System on Facebook" class="facebook fa-facebook fab"></a></li>
            <li><a href="https://www.linkedin.com/company/school-management-system-pakistan" target="_blank" title="School Management System on LinkedIn" class="linkedin fab fa-linkedin"></a></li>
          </ul>
        </div>
      </div>
    </div>
    <div class="py-3 py-md-4 mt-4 text-center copyrights" style="border-top: 1px solid #232323;">
      <div class="">
        <p class="mb-0">© <script>document.write(new Date().getFullYear());</script>. Your <a href="{{ route('fronted-home')}}" class="text-hover" title="School Management System">School Management System</a>. All Rights Reserved</p>
      </div>
    </div>
  </footer>
  <a href="https://wa.me/03075943188?text=Hello,%20I%20need%20assistance%20with%20your%20School%20Management%20Software." class="whatsapp-floating-button fab">
    <svg viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="width: 100%; height: 100%; fill: rgb(255, 255, 255); stroke: none;"><path d="M19.11 17.205c-.372 0-1.088 1.39-1.518 1.39a.63.63 0 0 1-.315-.1c-.802-.402-1.504-.817-2.163-1.447-.545-.516-1.146-1.29-1.46-1.963a.426.426 0 0 1-.073-.215c0-.33.99-.945.99-1.49 0-.143-.73-2.09-.832-2.335-.143-.372-.214-.487-.6-.487-.187 0-.36-.043-.53-.043-.302 0-.53.115-.746.315-.688.645-1.032 1.318-1.06 2.264v.114c-.015.99.472 1.977 1.017 2.78 1.23 1.82 2.506 3.41 4.554 4.34.616.287 2.035.888 2.722.888.817 0 2.15-.515 2.478-1.318.13-.33.244-.73.244-1.088 0-.058 0-.144-.03-.215-.1-.172-2.434-1.39-2.678-1.39zm-2.908 7.593c-1.747 0-3.48-.53-4.942-1.49L7.793 24.41l1.132-3.337a8.955 8.955 0 0 1-1.72-5.272c0-4.955 4.04-8.995 8.997-8.995S25.2 10.845 25.2 15.8c0 4.958-4.04 8.998-8.998 8.998zm0-19.798c-5.96 0-10.8 4.842-10.8 10.8 0 1.964.53 3.898 1.546 5.574L5 27.176l5.974-1.92a10.807 10.807 0 0 0 16.03-9.455c0-5.958-4.842-10.8-10.802-10.8z"></path></svg>
  </a>
  <!-- Jquery needed -->
<script src="{{asset('public/frontend/js/jquery.min.js')}}"></script>
<script src="{{asset('public/js/bootstrap.min.js')}}" crossorigin="anonymous"></script>
   
<script>
       $(window).scroll(function() {
           if ($(document).scrollTop() > 50) {
               $('.nav').addClass('affix');
               console.log("OK");
           } else {
               $('.nav').removeClass('affix');
           }
       });
$(document).ready(function(){
    // Add smooth scrolling to all links
    $("a.scroll_tags").on('click', function(event) {
    if (this.hash !== "") {
        event.preventDefault();
        var hash = this.hash;
        $('html, body').animate({
        scrollTop: $(hash).offset().top
        }, 500, function(){
        window.location.hash = hash;
        });
    } 
    });
});
</script>