{{-- @extends('layouts.app') --}}
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>404 | Page Not Found | School Management System</title>

<!-- Meta Description -->
<meta name="description" content="Oops! The page you're looking for doesn't exist on the School Management System. Return to the homepage or contact support for assistance.">

<!-- Keywords -->
<meta name="keywords" content="404 page not found, error page, school management system, page missing, broken link">

<!-- Open Graph Title -->
<meta property="og:title" content="404 | Page Not Found | School Management System">

<!-- Open Graph Description -->
<meta property="og:description" content="Oops! The page you're looking for doesn't exist on the School Management System. Return to the homepage or contact support for assistance.">

<!-- Open Graph URL -->
<meta property="og:url" content="{{ url()->current() }}">

<!-- Open Graph Type -->
<meta property="og:type" content="website">

<!-- Twitter Card -->
<meta name="twitter:card" content="summary">
<meta name="twitter:title" content="404 | Page Not Found | School Management System">
<meta name="twitter:description" content="Oops! The page you're looking for doesn't exist on the School Management System. Return to the homepage or contact support for assistance.">


<!-- Canonical URL -->
<link rel="canonical" href="{{ url()->current() }}">



    <!-- Robots -->
    <meta name="robots" content="noindex, nofollow">
    <link href="{{asset('public/css/bootstrap.css')}}" rel="stylesheet" />
    <link href="{{asset('public/css/custom.css?')}}{{time()}}" rel="stylesheet" />
    <style>
      .authincation{
	background:#fff;
	display:flex;
	min-height: 100vh;}
    .error-page .error-text {
    font-size: 150px;
    line-height: 1;
    font-weight: 700;
    margin-bottom: 0;
}
.error-page h4, .error-page .h4 {
    font-size: 44px;
    margin-bottom: 0;
    line-height: 1.2;
}
.error-page p {
    font-size: 18px;
    color: #c2c2c2;
    margin-bottom: 30px;
}
@media(max-width:767px) {
    .error-page .error-text {
    font-size: 80px;
}
.error-page h4, .error-page .h4 {
        font-size: 27px;
    }
}

    </style>
@include('layouts.google-analytics')
</head>
<body class="p-0" style="background-color: #eee;">
        
    <div class="align-items-center authincation h-100" style='background-image: url("{{asset('public/assets/img/404-bg.jpg')}}"); background-size:cover; background-repeat:no-repeat; background-size:cover;'>
        <div class="container h-100">
            <div class="row h-100 align-items-center text-md-left text-center">
                <div class="col-md-6 col-sm-12">
                    <div class="form-input-content  error-page">
						<h1 class="error-text text-link">400</h1>
						<h4 class="text-link">Bad Request</h4>
						<p>Sorry, we are under maintenance!</p>
                        <a class="btn btn-primary py-2 px-3" href="{{ route('fronted-home')}}">Back to Home</a>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12">
					<img class="w-100 move1" src="{{asset('public/assets/img/404.svg')}}" alt="">
    
				</div>
            </div>
        </div>
    </div>
</body>
</html>
