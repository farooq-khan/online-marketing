{{-- @extends('layouts.app') --}}
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login</title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100..900;1,100..900&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700;900&display=swap" rel="stylesheet">
    <link href="{{asset('public/css/bootstrap.css')}}" rel="stylesheet" />
    <link href="{{asset('public/css/school.css')}}" rel="stylesheet" />
</head>
<body>
    <header class="header position-absolute py-3">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-auto logo">
                    <img src="{{asset('public/assets/img/school-placeholder-logo.png')}}" class="img-fluid" alt="School">
                </div>
                    <nav class="col navbar navbar-expand-sm py-0">
                        <a class="navbar-brand d-lg-none" href="#"><img src="{{asset('public/assets/img/school-placeholder-logo.png')}}" class="img-fluid" alt="School"></a>
                        <button class="navbar-toggler d-lg-none" type="button" data-toggle="collapse" data-target="#collapsibleNavId" aria-controls="collapsibleNavId"
                            aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        <div class="collapse navbar-collapse" id="collapsibleNavId">
                            <ul class="ml-auto mr-0 mt-2 mt-lg-0 navbar-nav">
                                <li class="nav-item active">
                                    <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">About</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">Admission Enquiry</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">Fee submission</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">Contact</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">Dashboard</a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </header>
    {{-- banner section --}}
    <div class="banner position-relative text-white align-items-center d-flex" style="background-image: url('{{ asset('public/assets/img/banner-for-school-management-software.jpg') }}');">
        <div class="container ">
            {{-- <img src="{{asset('public/assets/img/banner-for-school-management-software.jpg')}}" class="img-fluid w-100"> --}}
            {{-- <img src="{{asset('public/uploads/website/banners/'.@$configuration->banner1)}}" class="img-fluid w-100"> --}}
            <h1 class="mb-0">{{@$configuration->quote}}</h1>
        </div>
    </div>

    {{-- about us section --}}
    <div class="container ">
    <div class="row mt-5 pt-3">
        <div class="col-lg-7 col-md-7 col-12 mb-md-0 mb-4">
            <h2 class="section-heading mb-3">About Us</h2>
            <p class="mb-0 text-muted">{!! @$configuration->about_us !!}</p>
        </div>
        <div class="col-lg-5 col-md-5 col-12">
            <img src="{{asset('public/uploads/website/banners/'.@$configuration->principal_photo)}}" class="img-fluid" style="max-height: 212px">
            <h5 class="mt-2">{{@$configuration->principal_name}}</h5>
            <p class="text-muted mb-0">{{@$configuration->principal_description}}</p>
        </div>
    </div>

    {{-- why choose us section --}} 
    <div class="row mt-5 pt-3">
        <div class="col-12 text-center mt-4 mb-4">
            <h3 class="text-center section-heading pb-3 underline-center">Why Choose Us</h3>
            <p class="mb-0 text-muted">{!! @$configuration->why_choose_us !!}</p>
        </div>

        <div class="col-4">
            <div class="card">
                <div class="card-body text-center">
                    <h4>Books & Library</h4>
                    <p class="mb-0 text-muted">{{@$configuration->books_and_library}}</p>
                </div>
            </div>
        </div>

        <div class="col-4"> 
            <div class="card">
                <div class="card-body text-center">
                    <h4>Quality Teachers</h4>
                    <p class="mb-0 text-muted">{{@$configuration->quality_teachers}}</p>
                </div>
            </div>
        </div>

        <div class="col-4">
            <div class="card">
                <div class="card-body text-center">
                    <h4>Great Certfication</h4>
                    <p class="mb-0 text-muted">{{@$configuration->great_certification}}</p>
                </div>
            </div>
        </div>

        {{-- counter section --}}
        <div class="col-12 mt-4">
            <div class="row" style="background-image: url({{asset('public/assets/img/background.jpg')}}); min-height: 120px; align-items: center;">
            @foreach($labels as $label)
                <div class="col-3 text-center">
                    <h4>{{$label->value}}</h4>
                    <h6>{{$label->label}}</h6>
                </div>
            @endforeach
            </div>
        </div>

         {{-- our experts teachers section --}}
        <div class="col-12 text-center mt-4 mb-4">
            <h4 class="text-center"><span style="border-bottom: 2px solid blue;">Our Experts Teachers</span></h4>
            <p class="mb-0 text-muted">{!! @$configuration->teacher_section_description !!}</p>
        </div>

        @foreach($teachers as $teacher)
        <div class="col-3">
            <div class="card">
                <div class="card-body">
                    <img src="{{asset('public/uploads/website/banners/'.@$teacher->photo)}}" class="img-fluid">
                    <h4 class="mt-2">{{@$teacher->name}}</h4>
                    <p class="text-muted">{{@$teacher->description}}</p>
                </div>
            </div>
        </div>
        @endforeach

        {{-- our experts teachers section --}}
        <div class="col-12 text-center mt-4 mb-4">
            <h4 class="text-center"><span style="border-bottom: 2px solid blue;">Image Gallery</span></h4>
            <p class="mb-0 text-muted">{!! @$configuration->image_gallery_description !!}</p>
        </div>

        @foreach($galleries as $gallery)
        <div class="col-3">
            <div class="card">
                <div class="card-body">
                    <img src="{{asset('public/uploads/website/banners/'.@$gallery->photo)}}" class="img-fluid">
                </div>
            </div>
        </div>
        @endforeach
    </div>

<script>
    
</script>
</body>
</html>

