<style type="text/css">
  html
{
  background: white !important;
}

</style>
@extends('layouts.app')

@section('title','School Mangement System')

@section('content')
<style type="text/css">
.invalid-feedback {
     font-size: 100%;
}
.disabled:disabled{
  opacity:0.5;
  cursor: not-allowed;
}
#page-header
{
    background-color: #515f6d;
}
.offer-course-header
{
  color: #3f9ce8!important;
  margin: 0;
}

ul {
  font-family: "Roboto";
  font-size: 13px;
  line-height: 1.5em;
  margin: 5px 0 15px;
  padding: 0;
}
ul li {
  list-style: none;
  position: relative;
  padding: 0 0 0 20px;
  margin-bottom: 5px;
}
ul.circle-checkmark li::before {
  content: "";
  position: absolute;
  left: 0;
  top: 2px;
  border: solid 8px #ff6600;
  border-radius: 8px;
  -moz-border-radius: 8px;
  -webkit-border-radius: 8px;
}
ul.circle-checkmark li::after {
  content: "";
  position: absolute;
  left: 0.6%;
  top: 5px;
  width: 3px;
  height: 8px;
  border: solid #fff;
  border-width: 0 2px 2px 0;
  -webkit-transform: rotate(45deg);
  -moz-transform: rotate(45deg);
  -o-transform: rotate(45deg);
  transform: rotate(45deg);
}

</style>
<!-- Main Container -->
            <main id="main-container" style="margin-top: 44px;">
                <!-- Hero -->
                <div class="bg-gd-primary overflow-hidden">
                    <div class="hero bg-black-op-25" style="height: 500px !important;">
                            <div id="demo" class="carousel slide" data-ride="carousel" style="width: 100%;height: 100%;">

                              <!-- Indicators -->
                              <ul class="carousel-indicators">
                                <li data-target="#demo" data-slide-to="0" class="active"></li>
                                <li data-target="#demo" data-slide-to="1"></li>
                                <li data-target="#demo" data-slide-to="2"></li>
                              </ul>

                              <!-- The slideshow -->
                              <div class="carousel-inner">
                                @if($conf == null)
                                <div class="carousel-item active">
                                  <img src="https://media.istockphoto.com/photos/pakistan-monument-islamabad-picture-id535695503?k=6&m=535695503&s=612x612&w=0&h=uP8aDK4xlfjk3kEiyr9wwUiuh80UwAiICweFpiBDosk=" alt="Los Angeles" style="width: 100%;background-size: cover;background-position: center;max-height: 500px;">
                                </div>
                                <div class="carousel-item">
                                  <img src="https://cdn.pixabay.com/photo/2015/12/01/20/28/road-1072823__340.jpg" alt="Chicago" style="width: 100%;background-size: cover;background-position: center;max-height: 500px;">
                                </div>
                                <div class="carousel-item">
                                  <img src="https://www.itl.cat/pngfile/big/30-303191_background-images-for-editing-editing-pictures-background-background.jpg" alt="New York" style="width: 100%;background-size: cover;background-position: center;max-height: 500px;">
                                </div>
                                @else
                                <div class="carousel-item active">
                                  <img src="{{asset('public/uploads/website/banners/'.$conf->banner1)}}" alt="Los Angeles" style="width: 100%;background-size: cover;background-position: center;max-height: 500px;">
                                </div>

                                <div class="carousel-item">
                                  <img src="{{asset('public/uploads/website/banners/'.$conf->banner2)}}" alt="Los Angeles" style="width: 100%;background-size: cover;background-position: center;max-height: 500px;">
                                </div>

                                <div class="carousel-item">
                                  <img src="{{asset('public/uploads/website/banners/'.$conf->banner3)}}" alt="Los Angeles" style="width: 100%;background-size: cover;background-position: center;max-height: 500px;">
                                </div>
                                @endif
                              </div>

                              <!-- Left and right controls -->
                              <a class="carousel-control-prev" href="#demo" data-slide="prev">
                                <span class="carousel-control-prev-icon"></span>
                              </a>
                              <a class="carousel-control-next" href="#demo" data-slide="next">
                                <span class="carousel-control-next-icon"></span>
                              </a>

                            </div>
                        {{-- </div> --}}
                    </div>
                </div>
                <div class="custom-container p-4" style="margin: 0 20%;">
                 <div class="row bg-white" style="margin-top: -75px">
                    <div class="col text-left bg-white shadow" style="padding: 20px 30px;border-radius: 5px;">
                      <h2>Tech4U</h2>
                      <div>
                        <p>
                          
                          Tech4U institute aims to achieve a distinct recognition and credibility in international community as a center of excellence. The institute is striving to be known for its creativity, innovation, public service, knowledge dissemination, and communication. It focuses on maintaining values of integrity, diversity, merit and quality in all spheres of life to ensure socio-economic, technological as well as industrial growth and development in the advancing world of today.
                          Tech4U institute is a private institution comprising of 1 campus, spread over 1.5 kanals of land, and offers coeducation in disciplines including, Basic IT, Office, Graphics Designing, Web Development, CIT, 2D/3D Animations, C/C++, PHP/MySQL, and Android Development.
                          Founded in 2010 by Afzal khan as a small private sector institution, known as a MC Computer Academy with only 5 computers and very few students, the institute has always worked on the principles to help students be adept with Science and Technology. Today, the institute has offered continuous support to more than 50 students in career development, especially in the fields of Computer Science, and Information Technology. Tech4U institute is efficiently pursuing the principles of academic excellence, teaching, research and discovery, and is endeavoring in educating and grooming students predominately in the disciplines of engineering, architecture, and computer making them capable of taking the future responsibilities and meeting the regional and global challenges faced by the society with strength of leadership, courage and passion.
                          The institute is working on adopting a personalized approach with close relationship between the faculty and the students, emphasizing on student-centered learning, improving the capacity of its faculty, developing education programs, integrating theory and practice for better results. It is also encouraging creativity, learning, and individual enterprise. The institute is also promoting international perspectives in the curriculum, among students and faculty.
                          The key to our long-term stability in the market is the quality we provide; we have top market professionals in our trainer list who are expert in their respective fields and provide hands-on practical training to the students. Our trainers have divers experience and currently they are also working in the market. One of the most important factors which we feel lucky about is that our trainers have also been a professional teacher in some of top organizations of the country which help them convey the right thing to the right person at the right time.
                          <h4 class="mb-0">Our Motto</h4>
                          Preparing youth for 4th Industrial Revolution
                          <h4 class="mb-0">Our Vision</h4>
                          To grow a community of technologically empowered, technically sound and litterally enlightened youth with a thematic vision to let them contribute in fourth induarial revolution.
                          <h4 class="mb-0">Our Philosphy</h4>
                          <img src="{{asset('public/assets/img/our-philosphy.png')}}" style="margin-top: -55px;">
                        </p>
                      </div>
                    </div>
                    
                </div>

                </div>
                <!-- END Hero -->
              </main>
            <!-- END Main Container -->
@endsection

@section('javascript')
<!-- Jquery needed -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    

<!-- Function used to shrink nav bar removing paddings and adding black background -->
    <script>
        $(window).scroll(function() {
            if ($(document).scrollTop() > 50) {
                $('.nav').addClass('affix');
                console.log("OK");
            } else {
                $('.nav').removeClass('affix');
            }
        });
    </script>
@stop

