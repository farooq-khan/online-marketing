{{-- @extends('layouts.app') --}}
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>Register | School Management System | All-in-One Educational Platform</title>

    <!-- Meta Description -->
    <meta name="description" content="Create your account on the School Management System. Register as a student, teacher, or administrator to manage your profile and access educational resources.">

    <!-- Keywords -->
    <meta name="keywords" content="school management system registration, create account, register, student signup, teacher signup, educational resources">

    <!-- Open Graph Title -->
    <meta property="og:title" content="Register | School Management System | All-in-One Educational Platform">

    <!-- Open Graph Description -->
    <meta property="og:description" content="Create your account on the School Management System. Register as a student, teacher, or administrator to access a comprehensive suite of tools and resources.">

    <!-- Open Graph URL -->
    <meta property="og:url" content="{{ route('get-started') }}">

    <!-- Open Graph Image -->
    <meta property="og:image" content="{{asset('public/assets/img/register-to-school-management-system.png')}}">

    <!-- Open Graph Type -->
    <meta property="og:type" content="website">


    <meta name="twitter:card" content="summary">
    <meta name="twitter:title" content="Register | School Management System | All-in-One Educational Platform">
    <meta name="twitter:description" content="Sign up to the School Management System and gain access to personalized educational tools and resources.">
    <meta name="twitter:image" content="{{asset('public/assets/img/register-to-school-management-system.png')}}">

    <link rel="canonical" href="{{ route('get-started') }}">




    <!-- Robots -->
    <meta name="robots" content="noindex, nofollow">
    <link href="{{asset('public/css/bootstrap.css')}}" rel="stylesheet" />
    <link href="{{asset('public/frontend/css/toastr.min.css')}}" rel="stylesheet" />
    <link href="{{asset('public/css/custom.css?')}}{{time()}}" rel="stylesheet" />
<style>
    .form-control {
    height: 2.75rem;
}
.btn-lg {
  font-size: 1.125rem
}
.card { max-width: 600px; z-index: 1;
  position: relative;}
section.signup:after {
    content: '';
    background: rgb(0 0 0 / 50%);
    height: 100%;
    width: 100%;
    position: fixed;
    z-index: 0;
    left: 0; top: 0
}
section.signup {
  background: url("{{asset('public/assets/img/page-bg.jpg')}}");
  background-size: cover;
}

@media(max-width:576px){
  .h2, h2 {
    font-size: 1.5rem;
}
}
</style>

@include('layouts.google-analytics')
</head>
<body class="p-0" style="background-color: #eee;">
<!-- Sign up form -->
<section class="align-items-center d-flex min-vh-100 px-3 signup">
  <div class="card mx-auto w-100 my-4">
    <div class="signup-form card-body p-sm-5 p-4">
          <h2 class="mb-2 text-center">Welcome to <br>School Management System</h2>
          <p class="mb-4 pb-sm-2 text-center">Get started by filling following data</p>
          <form class="register-form add-users-form" id="register-form">
            @csrf
            <div class="form-row">
              <div class="form-group mb-sm-4 col-12">
                <input type="text" name="school_name" class="form-control" placeholder="School name ...">
              </div>
              <div class="form-group mb-sm-4 col-12 col-sm-6">
                <input type="text" name="school_address" class="form-control" placeholder="School address ..." autocomplete="nope">
              </div> 
              <div class="form-group mb-sm-4 col-6">
                <input type="text" name="name" class="form-control" placeholder="Your name ...">
              </div>
              <div class="form-group mb-sm-4 col-6">
                <input type="text" name="phone_number" class="form-control" placeholder="Phone number ..." autocomplete="nope">
              </div>
              <div class="form-group mb-sm-4 col-12 col-sm-6">
                <input type="email" name="email" class="form-control" placeholder="Email address ...">
              </div>
              <div class="form-group mb-sm-4 col-6">
                <input type="password" name="password" class="form-control" placeholder="Password ..." id="password">
              </div>
              <div class="form-group mb-sm-4 col-6">
                <input type="password" name="confirm_password" class="form-control" placeholder="Confirm password ..." id="confirm_password">
              </div>
              <div class="form-group mb-sm-4 col-12">
                <div class="custom-control custom-checkbox">
                  <input type="checkbox" class="custom-control-input" id="agree-term">
                  <label class="custom-control-label font-weight-normal" for="agree-term">I agree all statements in  <a href="{{ route('terms-and-conditions') }}" title="Terms and Conditions" class="term-service text-link">Terms and Conditions</a></label>
                </div>
              </div>
              <div class="form-group form-button mb-sm-4 col-12">
                <input type="submit" name="signup" id="signup" class="btn btn-primary btn-lg w-100 form-submit save-btn" value="Get started">
            </div>
            <div class="col-12 text-center">
              <p class="mb-0 me-2">Already have an account? <a href="{{ route('login') }}" class="text-link" style="text-decoration: underline">Login</a> here.</p>
            </div>
              {{-- <button class="btn-primary btn-sm mt-2 save-btn">Get started</button> --}}
            </div>
          </form>
    </div>
  </div>
</section>

        <!-- Sing in  Form -->
<script src="{{asset('public/frontend/js/jquery.min.js')}}"></script>
<script src="{{asset('public/js/bootstrap.min.js')}}" crossorigin="anonymous"></script>
<script src="{{asset('public/frontend/js/toastr.min.js')}}"></script>

<script type="text/javascript">
$(document).ready(function(){
  $(document).on('click', '.save-btn', function(e){
    $('.register-form').submit();
  });
  $(document).on('submit', '.register-form', function(e){
      e.preventDefault();

      if($('#agree-term').prop("checked") == false)
      {
        toastr.error('Alert!', 'Please accept terms of service',{"positionClass": "toast-bottom-right"});
        return false;
      }

      var new_password= $("#password").val();
      var confirm_new_password= $("#confirm_password").val();
      if(new_password != confirm_new_password)
      {
          toastr.error('Alert!', 'Passwords does not match !!',{"positionClass": "toast-bottom-right"});
          return false;
      }     
      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
          }
      });
       $.ajax({
          url: "{{ route('get-started') }}",
          method: 'post',
          data: $('.add-users-form').serialize(),
          beforeSend: function(){
            $('#loader_modal').modal({
              backdrop: 'static',
              keyboard: false
            });
            $("#loader_modal").modal('show');
            $('.save-btn').val('Please wait...');
            $('.save-btn').addClass('disabled');
            $('.save-btn').attr('disabled', true);
          },
          success: function(result){
            $("#loader_modal").modal('hide');
            $('.save-btn').val('add');
            $('.save-btn').attr('disabled', true);
            $('.save-btn').removeAttr('disabled');
            if(result.success === true){
              $('.modal').modal('hide');
              toastr.success('Success!', 'Account created successfuly',{"positionClass": "toast-bottom-right"});
              $('.add-users-form')[0].reset();
              setTimeout(function(){ window.location.href = "{{route('login')}}"; }, 3000);
            }
            else if (result.success === false) {
              toastr.success('Success!', result.message ,{"positionClass": "toast-bottom-right"});

            }


          },
          error: function (request, status, error) {
                $("#loader_modal").modal('hide');
                $('.save-btn').val('add');
                $('.save-btn').removeClass('disabled');
                $('.save-btn').removeAttr('disabled');
                $('.form-control').removeClass('is-invalid');
                $('.form-control').next().remove();
                json = $.parseJSON(request.responseText);
                $.each(json.errors, function(key, value){
                    $('input[name="'+key+'"]').after('<span class="invalid-feedback" role="alert"><strong>'+value+'</strong>');
                     $('input[name="'+key+'"]').addClass('is-invalid');

                     $('select[name="'+key+'"]').after('<span class="invalid-feedback" role="alert"><strong>'+value+'</strong>');
                     $('select[name="'+key+'"]').addClass('is-invalid');
                });

            }
        });
    });
});
</script>
</body>
</html>

