<style type="text/css">
  html
{
  background: white !important;
}

</style>
@extends('layouts.app')

@section('title','School Mangement System')

@section('content')
<style type="text/css">
.invalid-feedback {
     font-size: 100%;
}
.disabled:disabled{
  opacity:0.5;
  cursor: not-allowed;
}
#page-header
{
    background-color: #515f6d;
}
.offer-course-header
{
  color: #3f9ce8!important;
  margin: 0;
}

ul {
  font-family: "Roboto";
  font-size: 13px;
  line-height: 1.5em;
  margin: 5px 0 15px;
  padding: 0;
}
ul li {
  list-style: none;
  position: relative;
  padding: 0 0 0 20px;
  margin-bottom: 5px;
}
ul.circle-checkmark li::before {
  content: "";
  position: absolute;
  left: 0;
  top: 2px;
  border: solid 8px #ff6600;
  border-radius: 8px;
  -moz-border-radius: 8px;
  -webkit-border-radius: 8px;
}
ul.circle-checkmark li::after {
  content: "";
  position: absolute;
  left: 0.6%;
  top: 5px;
  width: 3px;
  height: 8px;
  border: solid #fff;
  border-width: 0 2px 2px 0;
  -webkit-transform: rotate(45deg);
  -moz-transform: rotate(45deg);
  -o-transform: rotate(45deg);
  transform: rotate(45deg);
}

</style>
<!-- Main Container -->
            <main id="main-container" style="margin-top: 44px;">
                <!-- Hero -->
                <div class="bg-gd-primary overflow-hidden">
                    <div class="hero bg-black-op-25" style="height: 500px !important;">
                            <div id="demo" class="carousel slide" data-ride="carousel" style="width: 100%;height: 100%;">

                              <!-- Indicators -->
                              <ul class="carousel-indicators">
                                <li data-target="#demo" data-slide-to="0" class="active"></li>
                                <li data-target="#demo" data-slide-to="1"></li>
                                <li data-target="#demo" data-slide-to="2"></li>
                              </ul>

                              <!-- The slideshow -->
                              <div class="carousel-inner">
                                @if($conf == null)
                                <div class="carousel-item active">
                                  <img src="https://media.istockphoto.com/photos/pakistan-monument-islamabad-picture-id535695503?k=6&m=535695503&s=612x612&w=0&h=uP8aDK4xlfjk3kEiyr9wwUiuh80UwAiICweFpiBDosk=" alt="Los Angeles" style="width: 100%;background-size: cover;background-position: center;max-height: 500px;">
                                </div>
                                <div class="carousel-item">
                                  <img src="https://cdn.pixabay.com/photo/2015/12/01/20/28/road-1072823__340.jpg" alt="Chicago" style="width: 100%;background-size: cover;background-position: center;max-height: 500px;">
                                </div>
                                <div class="carousel-item">
                                  <img src="https://www.itl.cat/pngfile/big/30-303191_background-images-for-editing-editing-pictures-background-background.jpg" alt="New York" style="width: 100%;background-size: cover;background-position: center;max-height: 500px;">
                                </div>
                                @else
                                <div class="carousel-item active">
                                  <img src="{{asset('public/uploads/website/banners/'.$conf->banner1)}}" alt="Los Angeles" style="width: 100%;background-size: cover;background-position: center;max-height: 500px;">
                                </div>

                                <div class="carousel-item">
                                  <img src="{{asset('public/uploads/website/banners/'.$conf->banner2)}}" alt="Los Angeles" style="width: 100%;background-size: cover;background-position: center;max-height: 500px;">
                                </div>

                                <div class="carousel-item">
                                  <img src="{{asset('public/uploads/website/banners/'.$conf->banner3)}}" alt="Los Angeles" style="width: 100%;background-size: cover;background-position: center;max-height: 500px;">
                                </div>
                                @endif
                                
                              </div>

                              <!-- Left and right controls -->
                              <a class="carousel-control-prev" href="#demo" data-slide="prev">
                                <span class="carousel-control-prev-icon"></span>
                              </a>
                              <a class="carousel-control-next" href="#demo" data-slide="next">
                                <span class="carousel-control-next-icon"></span>
                              </a>

                            </div>
                        {{-- </div> --}}
                    </div>
                </div>
                <div class="custom-container p-4" style="margin: 0 20%;">
                <div class="row bg-white" style="margin-top: -75px">
                    <div class="col text-left bg-white shadow" style="padding: 20px 30px;border-radius: 5px;">
                      <div style="">
                      <form id="apply-online-form" method="POST">
                      @csrf
                      <div class="modal-body ">
                          <div class="form-row next_subject">
                            <div class="form-group col-md-6">
                                <label for="fullname">Full Name*</label>
                                <input type="text" name="fullname" id="fullname" class="form-control" placeholder="Enter Full Name..." required="required" />
                            </div>

                             <div class="form-group col-md-6">
                                <label for="guardian">Father Name*</label>
                                <input type="text" name="guardian" id="guardian" class="form-control" placeholder="Enter Full Name..." required="required" />
                            </div>

                            <div class="form-group col-md-6">
                                <label for="email">Email Address*</label>
                                <input type="email" name="email" id="email" class="form-control" placeholder="example@gmail.com" required="required" />
                            </div>

                            <div class="form-group col-md-6">
                                <label for="phone">Phone Number*</label>
                                <input type="number" name="phone" id="phone" class="form-control" placeholder="0900 0000000" required="required" />
                            </div>

                            <div class="form-group col-md-12">
                                <label for="email">Course*</label>
                               {{-- <textarea name="message" id="email" placeholder="Enter Message..." class="form-control" rows="10"></textarea> --}}
                               <select class="form-control" name="course" required="required" >
                                 <option selected="true">Select Course</option>
                                 @foreach($courses as $course)
                                  <option value="{{$course->id}}">{{$course->name}}</option>
                                 @endforeach
                               </select>
                            </div>

                          </div>
                      </div>
                      <div class="modal-footer">
                        <button type="submit" class="btn btn-primary save-btn">Submit To Register</button>
                      </div>
                      </form>
                      </div>
                      
                    </div>
                    
                </div>

                </div>
                <!-- END Hero -->
              </main>
            <!-- END Main Container -->
@endsection

@section('javascript')
<!-- Jquery needed -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    

<!-- Function used to shrink nav bar removing paddings and adding black background -->
    <script>
        $(window).scroll(function() {
            if ($(document).scrollTop() > 50) {
                $('.nav').addClass('affix');
                console.log("OK");
            } else {
                $('.nav').removeClass('affix');
            }
        });

    $('#apply-online-form').on('submit',function(e){
    e.preventDefault();
      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
          }
      });
       $.ajax({
          url: "{{ route('apply-online-for-course') }}",
          dataType: 'json',
          type: 'post',
          data: new FormData(this),
          contentType: false,
          cache: false,
          processData:false,
          beforeSend: function(){
            $('.save-btn').addClass('disabled');
            $('.save-btn').attr('disabled', true);
            $('#loader_modal').modal({
              backdrop: 'static',
              keyboard: false
            });
            $('#loader_modal').modal('show');
          },
          success: function(result){
            $('.save-btn').attr('disabled', true);
            $('.save-btn').removeAttr('disabled');
            if(result.success == true){
              toastr.success('Success!', 'Your Form Submitted Successfully!!!',{"positionClass": "toast-bottom-right"});
              $('#loader_modal').modal('hide');
              $('#apply-online-form')[0].reset();
            }

          },
          error: function (request, status, error) {
                /*$('.form-control').removeClass('is-invalid');
                $('.form-control').next().remove();*/
                $('#loader_modal').modal('hide');
                $('.save-btn').removeClass('disabled');
                $('.save-btn').removeAttr('disabled');
                json = $.parseJSON(request.responseText);
                $.each(json.errors, function(key, value){
                      $('input[name="'+key+'"]').after('<span class="invalid-feedback" role="alert"><strong>'+value+'</strong>');
                     $('input[name="'+key+'"]').addClass('is-invalid');
                     $('textarea[name="'+key+'"]').after('<span class="invalid-feedback" role="alert"><strong>'+value+'</strong>');
                     $('textarea[name="'+key+'"]').addClass('is-invalid');


                });
            }
        });
  });
    </script>
@stop

