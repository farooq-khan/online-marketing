  <!DOCTYPE html>
  <html>
  <head>
    <meta charset="utf-8"/>
    <title>Invoice</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- <link href="//db.onlinewebfonts.com/c/c77b24ed6e38fde787d3c573f1f4265d?family=Clarendon+Blk+BT" rel="stylesheet" type="text/css"/> -->
    <style type="text/css">
      /*@import url(//db.onlinewebfonts.com/c/c77b24ed6e38fde787d3c573f1f4265d?family=Clarendon+Blk+BT);*/

      /*@font-face {font-family: "Clarendon Blk BT"; src: url("//db.onlinewebfonts.com/t/c77b24ed6e38fde787d3c573f1f4265d.eot"); src: url("//db.onlinewebfonts.com/t/c77b24ed6e38fde787d3c573f1f4265d.eot?#iefix") format("embedded-opentype"), url("//db.onlinewebfonts.com/t/c77b24ed6e38fde787d3c573f1f4265d.woff2") format("woff2"), url("//db.onlinewebfonts.com/t/c77b24ed6e38fde787d3c573f1f4265d.woff") format("woff"), url("//db.onlinewebfonts.com/t/c77b24ed6e38fde787d3c573f1f4265d.ttf") format("truetype"), url("//db.onlinewebfonts.com/t/c77b24ed6e38fde787d3c573f1f4265d.svg#Clarendon Blk BT") format("svg"); }*/
      body{
        font-family: 'Times';
      }
      @page {size: auto;   /* auto is the initial value */
        margin: 5mm;}
    	  table { font-size: 10px; }
        .pay_slip{
          border-collapse: collapse;
        }
      .pay_slip tr td{
        border: 1px solid black;
        padding: 5px 2px;

      }
      .pay_slip tr th{
        border: 1px solid black;
        padding: 5px 2px;

      }
  
    </style>

  </head>
@php
  use Carbon\Carbon;
@endphp
  <body style="">
    <table style="width: 100%">
        <tr>
          @foreach($teachers as $teacher)
          <td width="48%" style="width: 45%">
            <div style="width: 100%">
              <table style="width: 100%">
                <tr>
                  <td style="width: 30%;text-align: right;">
                    <img src="{{asset('public/uploads/school/images/'.@$school->logo)}}" width="60" style="margin-bottom: 0px;">
                  </td>
                  <td style="width: 70%;text-align: center;align-items: center;">
                    <h2>{{@$school->name}}</h2>
                    <h4>Pay Slip For The Month Of {{Carbon::parse($teacher->salary_month)->format('F-Y')}}</h4>
                  </td>
                </tr>
              </table>
              <hr style="height: 2px;background-color: black;width: 100%%;">  
              <table style="width: 100%%">
                <tr>
                  <td style="width: 30%"><b>Teacher Name</b></td>
                  <td style="width: 10%;text-align: center;">:</td>
                  <td style="width: 30%">{{@$teacher->teacher->name}}</td>
                  <td rowspan="4" style="width: 30%;padding: 0;" align="right" valign="top">
                    @if(@$teacher->teacher->image != null && file_exists(public_path().'/uploads/teachers/images/'.$teacher->teacher->image))
                    <img src="{{asset('public/uploads/teachers/images/'.$teacher->teacher->image)}}" class="teacher__img profileimg" style="width: 60px;height: 60px;">
                    @else
                    <img src="{{asset('public/assets/img/'.@$teacher->teacher->gender.'.jpg')}}" class="teacher__img profileimg" style="width: 60px;height: 60px;">

                    @endif
                  </td>
                </tr>
                 <tr>
                  <td style="width: 30%"><b>Guardian Name</b></td>
                  <td style="width: 10%;text-align: center;">:</td>
                  <td style="width: 30%">{{@$teacher->teacher->guardian}}</td>
                  <!-- <td></td> -->
                </tr>
                 <tr>
                  <td style="width: 30%">Designation</td>
                  <td style="width: 10%;text-align: center;">:</td>
                  <td style="width: 30%">{{@$teacher->teacher->designation}}</td>
                  <!-- <td></td> -->
                </tr>
                 <tr>
                  <td style="width: 30%">Contact</td>
                  <td style="width: 10%;text-align: center;">:</td>
                  <td style="width: 30%">{{@$teacher->teacher->phone}}</td>
                  <!-- <td></td> -->
                </tr>
                 <tr>
                  <td style="width: 30%">Address</td>
                  <td style="width: 10%;text-align: center;">:</td>
                  <td style="width: 30%" colspan="2">{{@$teacher->teacher->address}}</td>
                  <!-- <td></td> -->
                </tr>
                 <tr>
                  <td style="width: 30%"><b>Issue Date</b></td>
                  <td style="width: 10%;text-align: center;">:</td>
                  <td style="width: 30%">{{Carbon::now()->format('Y-M-D')}}</td>
                  <td></td>
                </tr>

              </table>
              <table style="width: 100%%;margin-top: 10px;" class="pay_slip">
                <thead style="background-color: black;color: white;">
                  <tr>
                    <th style="width: 20%">S.No:</th>
                    <th style="width: 40%">Description</th>
                    <th style="width: 40%">Amount / Date</th>
                  </tr>
                </thead>
                <tr>
                  <td>1</td>
                  <td>Salary Amount</td>
                  <td>Rs: {{number_format($teacher->total_amount,2,'.',',')}}/-</td>
                </tr>
                <tr>
                  <td>2</td>
                  <td>Deduction Amount</td>
                  <td>Rs: {{number_format($teacher->deduction,2,'.',',')}}/-</td>
                </tr>
                <tr>
                  <td>3</td>
                  <td>Bonus Amount</td>
                  <td>Rs: {{number_format($teacher->bonus_amount,2,'.',',')}}/-</td>
                </tr>
                <tr>
                  <td>4</td>
                  <td>Total Payable Amount</td>
                  <td>Rs: {{number_format($teacher->total_amount - $teacher->deduction + $teacher->bonus_amount,2,'.',',')}}/-</td>
                </tr>
                <tr>
                  <td>5</td>
                  <td>Total Paid Amount</td>
                  <td>Rs: {{number_format($teacher->paid_amount,2,'.',',')}}/-</td>
                </tr>
                <tr>
                  <td>6</td>
                  <td>Total Unpaid Amount</td>
                  <td>Rs: {{number_format($teacher->unpaid_amount,2,'.',',')}}/-</td>
                </tr>
                <tr>
                  <td>7</td>
                  <td>Salary Month</td>
                  <td>{{$teacher->salary_month}}</td>
                </tr>
                <tr>
                  <td>8</td>
                  <td>Payment Date</td>
                  <td>{{$teacher->submitted_date}}</td>
                </tr>
              </table>
              <p style="width: 98%;font-size: 12px;">
                Dear & Respected teacher kindly carefully check your Pay slip and incase
                Of any issue you can contact school administration. Thank You,
              </p>
              <p style="width: 100%;font-size: 12px;">
                <table style="width: 100%">
                  <tr>
                    <td style="width: 76%;" align="right">Principal:</td>
                    <td></td>
                  </tr>
                </table>
              
              </p>
            </div>
          </td>
          @if($loop->iteration % 2 == 0)
          </tr>
          <tr>
          @elseif($teachers->count() == 1)
          <td width="4%"></td>
          <td width="48%"></td>
          @else
          <td width="4%"></td>
          @endif
          @endforeach
        </tr>
    </table>
    
  </body>
</html>