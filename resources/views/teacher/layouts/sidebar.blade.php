<div id="layoutSidenav_nav">
                <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
                    <div class="sb-sidenav-menu">
                        <div class="nav">
                            <div class="row w-100">
                                <div class="col-12 d-flex justify-content-center align-items-center mt-4">
                                @if(Auth::user()->logo != null && file_exists( public_path() . '/uploads/users/logos/'.Auth::user()->logo))                          
                                    <img src="{{asset('public/uploads/users/logos/'.Auth::user()->logo)}}" width="50%" style="border-radius: 50%;" height="130px">
                                @else
                                    <img src="{{asset('public/assets/img/user-avatar.png')}}" width="50%">
                                @endif
                                </div>
                                <div class="col-12 d-flex justify-content-center align-items-center text-white mt-1">
                                <h5>{{Auth::user()->name}}</h5>
                                </div>
                            </div>

                            <a class="nav-link" href="{{route('all-attendance')}}">
                                <div class="sb-nav-link-icon"><i class="fas fa-clock text-white"></i></div>
                                Attendance
                            </a>

                            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#resultLayouts" aria-expanded="false" aria-controls="resultLayouts">
                                <div class="sb-nav-link-icon"><i class="fas fa-poll text-white"></i></div>
                                Results
                                <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                            </a>
                            <div class="collapse" id="resultLayouts" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                                <nav class="sb-sidenav-menu-nested nav">
                                    <a class="nav-link" href="{{route('all-result')}}">Exam Result</a>
                                    <a class="nav-link" href="{{route('all-test-result')}}">Test(s) Result</a>
                                </nav>
                            </div>
                            
                        </div>
                    </div>
                    <div class="sb-sidenav-footer">
                        <div class="small">Logged in as:</div>
                        {{Auth::user()->name}}
                    </div>
                </nav>
            </div>