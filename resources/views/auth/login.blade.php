{{-- @extends('layouts.app') --}}
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login | School Management System | All-in-One Educational Platform</title>
    <!-- Meta Description -->
    <meta name="description" content="Secure login to access your account on the School Management System. Manage your profile, track progress, and access educational resources.">

    <!-- Keywords -->
    <meta name="keywords" content="school management system login, student login, teacher login, secure login, account access, education system">
    <!-- Open Graph Title -->
    <meta property="og:title" content="Login | School Management System | All-in-One Educational Platform">

    <!-- Open Graph Description -->
    <meta property="og:description" content="Secure login to access your account on the School Management System. Manage your profile, track progress, and access educational resources.">

    <!-- Open Graph URL -->
    <meta property="og:url" content="{{ route('login') }}">

    <!-- Open Graph Image -->
    <meta property="og:image" content="{{asset('public/assets/img/login-to-school-management-system.png')}}">
    

    <!-- Open Graph Type -->
    <meta property="og:type" content="website">

    <meta name="twitter:card" content="summary">
    <meta name="twitter:title" content="Login | School Management System | All-in-One Educational Platform">
    <meta name="twitter:description" content="Secure login to access your account on the School Management System. Manage your profile, track progress, and access educational resources.">
    <meta name="twitter:image" content="{{asset('public/assets/img/login-to-school-management-system.png')}}">
    <link rel="canonical" href="{{ route('login') }}">


    <!-- Robots -->
    <meta name="robots" content="noindex, nofollow">
    <link href="{{asset('public/css/bootstrap.css')}}" rel="stylesheet" />
    <link href="{{asset('public/css/custom.css?')}}{{time()}}" rel="stylesheet" />
    <style>
        .form-control {
        height: 2.75rem;
    }
    .btn-lg {
      font-size: 1.125rem
    }
    .card { max-width: 560px; z-index: 1;
      position: relative;}
    section.signup:after {
        content: '';
        background: rgb(0 0 0 / 50%);
        height: 100%;
        width: 100%;
        position: absolute;
        z-index: 0;
        left: 0; top: 0
    }
    section.signup {
      background: url("{{asset('public/assets/img/page-bg.jpg')}}");
      background-size: cover;
    }
    @media(max-width:576px){
    .h2, h2 {
        font-size: 1.85rem;
        }
    }
    </style>
@include('layouts.google-analytics')
</head>
<body class="p-0" style="background-color: #eee;">
    
    @if (session('msg'))
    <div class="alert alert-success">
        {{ session('msg') }}
    </div>
@endif

        <section class="align-items-center d-flex min-vh-100 px-3 signup">
            <div class="card mx-auto w-100">
                <div class="card-body p-sm-5 p-4">
                    <h2 class="mb-4 pb-sm-2 text-center">Login to continue</h2>
                    <form method="POST" action="{{ route('login') }}" class="m-sm-0 m-2">
                        @csrf
                        <div class="form-group mb-4">
                            <input class="form-control" type="text" name="email" id="email" placeholder="Your Email"/>
                        </div>
                        <div class="form-group mb-4">
                            <input class="form-control" type="password" name="password" id="pass" placeholder="Password"/>
                        </div>
                        <div class="row form-group mb-4">
                            <div class="col">
                                <div class="custom-control custom-checkbox mr-sm-2">
                                <input type="checkbox" class="custom-control-input" id="remembe_rme">
                                <label class="custom-control-label font-weight-normal" for="remembe_rme">Remember me</label>
                                </div>
                            </div>
                            <div class="col text-right">
                                <a href="{{ route('password.request') }}">Forgot password?</a>
                            </div>
                            </div>
                        <div class="form-group form-button mb-4">
                            <input type="submit" name="signup" id="signup" class="btn btn-lg btn-primary form-submit w-100" value="Login"/>
                        </div>
                        <div class="text-center">
                            <p class="mb-0 me-2">Don't have an account? <a href="{{ route('get-started') }}" style="text-decoration: underline" class="text-link">Sign up</a> here.</p>
                        </div>
                    </form>
                </div>
            </div>
        </section>

<script type="text/javascript">
$(document).ready(function(){
       // alert('hi');
@if(Session::has('suspend'))
  toastr.info('Sorry!', "Either Your Account Has Disabled or Invalid Credentials !!!",{"positionClass": "toast-bottom-right"});
  @php
   Session()->forget('suspend');
  @endphp
@endif
});
</script>
</body>
</html>

