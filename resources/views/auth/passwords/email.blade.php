{{-- @extends('layouts.app') --}}
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login | School Management System | All-in-One Educational Platform</title>
    <!-- Meta Description -->
    <meta name="description" content="Secure login to access your account on the School Management System. Manage your profile, track progress, and access educational resources.">

    <!-- Keywords -->
    <meta name="keywords" content="school management system login, student login, teacher login, secure login, account access, education system">
    <!-- Open Graph Title -->
    <meta property="og:title" content="Login | School Management System | All-in-One Educational Platform">

    <!-- Open Graph Description -->
    <meta property="og:description" content="Secure login to access your account on the School Management System. Manage your profile, track progress, and access educational resources.">

    <!-- Open Graph URL -->
    <meta property="og:url" content="{{ route('login') }}">

    <!-- Open Graph Image -->
    <meta property="og:image" content="{{asset('public/assets/img/login-to-school-management-system.png')}}">
    

    <!-- Open Graph Type -->
    <meta property="og:type" content="website">

    <meta name="twitter:card" content="summary">
    <meta name="twitter:title" content="Login | School Management System | All-in-One Educational Platform">
    <meta name="twitter:description" content="Secure login to access your account on the School Management System. Manage your profile, track progress, and access educational resources.">
    <meta name="twitter:image" content="{{asset('public/assets/img/login-to-school-management-system.png')}}">
    <link rel="canonical" href="{{ route('login') }}">


    <!-- Robots -->
    <meta name="robots" content="noindex, nofollow">
    <link href="{{asset('public/css/bootstrap.css')}}" rel="stylesheet" />
    <link href="{{asset('public/css/custom.css?')}}{{time()}}" rel="stylesheet" />
    <style>
        .form-control {
        height: 2.75rem;
    }
    .btn-lg {
      font-size: 1.125rem
    }
    .card { max-width: 560px; z-index: 1;
      position: relative;}
    section.signup:after {
        content: '';
        background: rgb(0 0 0 / 50%);
        height: 100%;
        width: 100%;
        position: absolute;
        z-index: 0;
        left: 0; top: 0
    }
    section.signup {
      background: url("{{asset('public/assets/img/page-bg.jpg')}}");
      background-size: cover;
    }
    @media(max-width:576px){
    .h2, h2 {
        font-size: 1.85rem;
        }
    }
    </style>
@include('layouts.google-analytics')
</head>
<body class="p-0" style="background-color: #eee;">
    @if (session('status'))
    <div class="alert alert-success" role="alert">
        {{ session('status') }}
    </div>
@endif
@if (session('error'))
    <div class="alert alert-error" role="alert">
        {{ session('error') }}
    </div>
    @endif
    <section class="align-items-center d-flex min-vh-100 px-3 signup">
        <div class="card mx-auto w-100">
            <div class="card-body p-sm-5 p-4">
                <h2 class="mb-4 pb-sm-2 text-center">Reset Password</h2>
                <form method="POST" action="{{ route('send-reset-link') }}" class="m-sm-0 m-2" data-gtm-form-interact-id="0">
                    @csrf
                    <div class="form-group mb-4">
                        <label for="email">{{ __('E-Mail Address') }}</label>
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group  mb-0">
                        <button type="submit" class="btn btn-lg btn-primary form-submit w-100">
                            {{ __('Send Password Reset Link') }}
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </section>

<script type="text/javascript">
    $(document).ready(function(){
           // alert('hi');
    @if(Session::has('suspend'))
      toastr.info('Sorry!', "Either Your Account Has Disabled or Invalid Credentials !!!",{"positionClass": "toast-bottom-right"});
      @php
       Session()->forget('suspend');
      @endphp
    @endif
    });
</script>
</body>
</html>

    
