@extends('user.layouts.layout')

@section('title','Single Student')

@section('content')
<style type="text/css">
   .student-image
   {
    padding-top: 100%;
    background-position: center;
    background-size: cover;
   }

   .student_image
   {
    box-shadow: 0px 0px 15px 0px rgba(0,0,0,0.5);

   }

/*   .dataTables_scrollHeadInner, .dataTables_scrollHeadInner table
   {
    width: 100% !important;
   }*/
</style>
<?php 
  use Carbon\Carbon;
?>

                    <div class=>
                      <!-- Nav tabs -->
                    <ul class="nav nav-tabs mt-5 custom-tabs" role="tablist">
                      <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#personalInformation">Personal Information</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#studentFamily">Student Family</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#studentFeeHistory">Student Fee History</a>
                      </li>

                      <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#otherFeeHistory">Student Other Fee History</a>
                      </li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                      <div id="personalInformation" class="tab-pane active"><br>
                        <div class="row">
                          <div class="col-lg-4 col-md-4 col-sm-12 mb-2 pl-2 pr-2 student_image">
                            <div class="student-image bg-white" style="background-image: url('{{asset('public/uploads/students/images/'.$student->image)}}')">
                              
                            </div>
                          </div>
                          <div class="col-lg-4 col-md-4 col-sm-12 pl-2 mb-2 pr-2">
                            <div class="bg-white h-100 p-4">
                              <table style="width: 100%;font-size: 18px">
                                <tr>
                                  <td>Admission No. :</td>
                                  <td>
                                    <span class="m-l-15" id="roll_no"  data-fieldvalue="{{$student->roll_no}}">
                                    {{$student->roll_no != null ? $student->roll_no : '--'}}
                                  </span>
                                  
                                  </td>
                                </tr>

                                <tr>
                                  <td>Name :</td>
                                  <td>
                                    <span class="m-l-15 inputDoubleClick" id="roll_no"  data-fieldvalue="{{$student->name}}">
                                    {{$student->name != null ? $student->name : '--'}}
                                  </span>
                                    <input type="text" autocomplete="nope" name="name" class="fieldFocus d-none form-control" value="{{(@$student->name!=null)?$student->name:''}}">
                                  </td>
                                </tr>

                                <tr>
                                  <td>Class :</td>
                                  <td>{{$student->student_class != null ? $student->student_class->class_name : '--'}}</td>
                                </tr>

                                <tr>
                                  <td>Gender :</td>
                                  <td>
                                    <span class="m-l-15 inputDoubleClick" id="gender"  data-fieldvalue="{{$student->gender}}">
                                      {{$student->gender == 'male' ? 'Male' : ($student->gender == 'female' ? 'Female' : 'Other')}}
                                    </span>
                                    <select class="fieldFocus d-none form-control" name="gender">
                                      <option value="">---Select Gender---</option>
                                      <option value="male" {{$student->gender == 'male' ? 'selected' : ''}}>Male</option>
                                      <option value="female" {{$student->gender == 'female' ? 'selected' : ''}}>Female</option>
                                    </select>
                                    </td>
                                </tr>

                                <tr>
                                  <td> Date of Birth :</td>
                                  <td>
                                    <span class="m-l-15 inputDoubleClick" id="dob"  data-fieldvalue="{{$student->dob}}">
                                    {{$student->dob != null ? $student->dob : '--'}}
                                  </span>
                                    <input type="date" autocomplete="nope" name="dob" class="fieldFocus d-none form-control" value="{{(@$student->dob!=null)?$student->dob:''}}">
                                   
                                  </td>
                                </tr>

                                <tr>
                                  <td>Date of Admission</td>
                                  <td>
                                    <!-- {{$student->date_of_admission}} -->
                                    <span class="m-l-15 inputDoubleClick" id="date_of_admission"  data-fieldvalue="{{$student->date_of_admission}}">
                                    {{$student->date_of_admission != null ? $student->date_of_admission : '--'}}
                                  </span>
                                    <input type="date" autocomplete="nope" name="date_of_admission" class="fieldFocus d-none form-control" value="{{(@$student->date_of_admission!=null)?$student->date_of_admission:''}}">
                                  </td>
                                </tr>

                                <tr>
                                  <td>Address:</td>
                                  <td>
                                    <!-- {{$student->address}} -->
                                     <span class="m-l-15 inputDoubleClick" id="address"  data-fieldvalue="{{$student->address}}">
                                    {{$student->address != null ? $student->address : '--'}}
                                  </span>
                                    <input type="text" autocomplete="nope" name="address" class="fieldFocus d-none form-control" value="{{(@$student->address!=null)?$student->address:''}}">
                                  </td>
                                </tr>

                                <tr>
                                  <td>Phone :</td>
                                  <td>
                                    <span class="m-l-15 inputDoubleClick" id="roll_no"  data-fieldvalue="{{$student->std_phone}}">
                                    {{$student->std_phone != null ? $student->std_phone : '--'}}
                                  </span>
                                    <input type="number" autocomplete="nope" name="std_phone" class="fieldFocus d-none form-control" value="{{(@$student->std_phone!=null)?$student->std_phone:''}}">
                                    
                                  </td>
                                </tr>

                                <tr>
                                  <td>Fee(Inc Discount) :</td>
                                  <td>
                                    @if($student->discount > 0)
                                    {{$student->student_class != null ? $student->student_class->fee * (100-$student->discount)/100 : 0}}
                                    @else
                                    {{$student->student_class != null ? $student->student_class->fee : 0}}
                                    @endif
                                  </td>
                                </tr>

                                <tr>
                                  <td>Discount :</td>
                                  <td>
                                    <span class="m-l-15 inputDoubleClick" id="discount"  data-fieldvalue="{{$student->discount}}">
                                    {{$student->discount != null ? $student->discount : '--'}} %
                                  </span>
                                    <input type="number" autocomplete="nope" name="discount" class="fieldFocus d-none form-control" value="{{(@$student->discount!=null)?$student->discount:''}}">
                                    
                                     </td>
                                </tr>

                                <tr>
                                  <td>Status :</td>
                                  <td>
                                    <span class="m-l-15" id="status"  data-fieldvalue="{{$student->status}}">
                                    @if($student->status == 1)
                                    <span>Active</span>
                                    @else
                                    <span style="color: red;font-style: italic;"><b>Suspended</b></span>
                                    @endif
                                  </span>
                                     </td>
                                </tr>
                                <tr>
                                  <td valign="top">Promotion : </td>
                                  <td>{{$std_cl}}</td>
                                </tr>
                              </table>
                            </div>
                          </div>

                          <div class="col-lg-4 col-md-4 col-sm-12 pl-2 mb-2 pr-2">
                            <div class="bg-white h-100 p-4">
                              <table style="width: 100%;font-size: 18px">
                                <tr>
                                  <td>Parent/Guardian Name :</td>
                                  <td>
                                    <span class="m-l-15 inputDoubleClick" id="guardian"  data-fieldvalue="{{$student->guardian}}">
                                    {{$student->guardian != null ? $student->guardian : '--'}}
                                  </span>
                                    <input type="text" autocomplete="nope" name="guardian" class="fieldFocus d-none form-control" value="{{(@$student->guardian!=null)?$student->guardian:''}}">
                                    
                                  </td>
                                </tr>

                                <tr>
                                  <td>Parent/Guardian Cnic :</td>
                                  <td>
                                    <span class="m-l-15 inputDoubleClick" id="guardian_cnic"  data-fieldvalue="{{$student->guardian_cnic}}">
                                    {{$student->guardian_cnic != null ? $student->guardian_cnic : '--'}}
                                  </span>
                                    <input type="text" autocomplete="nope" name="guardian_cnic" class="fieldFocus d-none form-control" value="{{(@$student->guardian_cnic!=null)?$student->guardian_cnic:''}}">
                                    </td>
                                </tr>

                                <tr>
                                  <td>Parent/Guardian Phone No. :</td>
                                  <td>
                                    <span class="m-l-15 inputDoubleClick" id="guardian_phone"  data-fieldvalue="{{$student->guardian_phone}}">
                                    {{$student->guardian_phone != null ? $student->guardian_phone : '--'}}
                                  </span>
                                    <input type="text" autocomplete="nope" name="guardian_phone" class="fieldFocus d-none form-control" value="{{(@$student->guardian_phone!=null)?$student->guardian_phone:''}}">
                                   
                                  </td>
                                </tr>
                              </table>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div id="studentFamily" class="tab-pane fade"><br>
                                                <div class="row">
                            <div class="col-8">
                                <h1 class="page-title">Student Family</h1>
                            </div>
                            
                    </div>

                     <div class="row p-0 mt-0">
                        <div class="col-12 mt-0">
                        <div class="card p-4">
                        <div class="table-responsive">
                        <table class="table table-striped nowrap table-family" style="width: 100%">
                        <thead>
                        <tr>
                            <th>Action</th>
                            <th>Family Member</th>
                            <th>Father/Guardian</th>
                            <th>Class</th>
                            <th>Relation</th>
                        </tr>
                        </thead>
                        </table>
                        </div>
                    </div>
                </div>
                    </div>
                      </div>
                      <div id="studentFeeHistory" class="tab-pane fade"><br>
                      <div class="row">
                  <div class="col-8">
                      <h1 class="page-title">Student Fee History</h1>
                  </div>                           
              </div>

                      <div class="row mb-0">
                                <div class="col-lg-12 col-md-12 title-col">
                                  <div class="d-sm-flex justify-content-center align-items-center">
                                    <div class="col mb-2">
                                      <select class="form-control form-control student_classes_select state-tags" name="student_classes_select" >
                                        <option value="">-- All Classes --</option>
                                         @foreach($classes as $class)
                                          <option value="{{$class->id}}"> {{$class->class_name}} </option>
                                          @endforeach
                                      </select>
                                    </div>

                                     <div class="col mb-2">
                                      <select class="form-control form-control discount_student state-tags" name="discount_student" >
                                        <option value="">-- Discount --</option>
                                          <option value="discount"> Discounted Students </option>
                                          <option value="non-discount"> Non Discounted Students </option>
                                      </select>
                                    </div>

                                     <div class="col mb-2">
                                      <select class="form-control form-control status_student state-tags" name="status" >
                                        <option value="">-- Status --</option>
                                          <option value="1"> Paid </option>
                                          <option value="2"> Partial Paid </option>
                                      </select>
                                    </div>

                                     <div class="col mb-2">
                                        <input type="date" name="from_date" id="from_date" class="form-control" placeholder="Search by year ..." />
                                    </div>

                                    <div class="col mb-2">
                                        <input type="date" name="to_date" id="to_date" class="form-control" placeholder="Search by year ..." />
                                    </div>
                              
                                    <div class="col-lg-2 col-md-2 col">
                                      <div class="input-group-append">
                                        <button class="btn btn-primary" type="reset" id="reset" style="padding: 5px 30px;">Reset</button>  
                                      </div>
                                    </div>
                                    {{-- <div class="col-lg-1 col-md-1"></div> --}}

                                  </div>
                                </div>
                              </div>

                      <div class="row p-0 mt-0">
                              <div class="selected-item catalogue-btn-group mt-4 mt-sm-3 ml-3 d-none">
                    
                                            <a href="javascript:void(0)" class="actionicon viewIcon download_invoice" data-id="1" title="Download Invoice"><i class="fas fa-download"></i></a>

                                        </div>
                                <div class="col-12 mt-4">
                                <div class="card p-4">
                                <div class="table-responsive">
                                <table class="table table-striped nowrap table-students-fee">
                                <thead>
                                <tr>
                                     <th class="noVis">
                                              <div class="custom-control custom-checkbox custom-checkbox1 d-inline-block">
                                                <input type="checkbox" class="custom-control-input check-all1" name="check_all" id="check-all">
                                                <label class="custom-control-label" for="check-all"></label>
                                              </div>
                                            </th>
                                    <th>Admission No.</th>
                                    <th>Fee Slip</th>
                                    <th>Name</th>
                                    <th>Class</th>
                                    <th>Gender</th>
                                    <th>Father/Guardian Name</th>
                                    <th>Fee(Inc Discount)</th>
                                    <th>Paid Amount</th>
                                    <th>Unpaid Amount</th>
                                    <th>Discount</th>
                                    <th>Fee Month</th>
                                    <th>Submitted Date</th>
                                    <th>Status</th>
                                    
                                </tr>
                                </thead>
                                </table>
                                </div>
                                </div>
                                </div>
                            </div>

                      <div class="row">
                  <div class="col-8">
                      <h1 class="page-title">Student Fee History By Year</h1>
                  </div>                           
                    </div>

                  <div class="row mb-0">
                            <div class="col-lg-12 col-md-12 title-col">
                              <div class="d-sm-flex justify-content-left align-items-center row">
                            
                                <div class="col-2">
                                    <input type="number" name="year" id="year" class="form-control" value="{{carbon::now()->format('Y')}}" placeholder="Search by year ..." />
                                </div>
                          
                                <div class="col-lg-2 col-md-2 col-4">
                                  <div class="input-group-append">
                                    <button class="btn btn-primary" type="button" id="apply_year" style="padding: 5px 30px;">Apply</button>  
                                  </div>
                                </div>

                                <div class="col-2 pull-right ml-4">
                                  <div class="input-group-append">
                                    <button class="btn btn-primary" type="button" id="export_attendance" style="padding: 5px 30px;">Export</button>  
                                  </div>
                                </div>
                                {{-- <div class="col-lg-1 col-md-1"></div> --}}

                              </div>
                            </div>
                          </div>

                  <div class="row p-0 mt-0">
                <div class="col-12 mt-4">
                <div class="card p-4">
                <div class="table-responsive">
                <table class="table table-striped nowrap table-students-fee-month">
                <thead>
                <tr>
                    <th>Month</th>
                    <th>Submitted Date</th>
                    <th>Submitted To</th>
                    <th>Total Amount</th>
                    <th>Paid Amount</th>
                    <th>Unpaid Amount</th>
                    <th>Status</th>
                    <th>Remark</th>
                </tr>
                </thead>
                </table>
                </div>
                </div>
                </div>
            </div>
                      </div>

                      <div id="otherFeeHistory" class="tab-pane fade"><br>
                      <div class="row">
                            <div class="col-8">
                                <h1 class="page-title">Other Fee</h1>
                            </div>
                    </div>

                      <div class="row mb-0">
                      <div class="col-lg-12 col-md-12 title-col">
                        <div class="d-sm-flex justify-content-left align-items-center">
                          <div class="col-lg-3 col-md-3 mb-2">
                            <select class="form-control form-control student_classes_select_other_fee state-tags" name="warehouse_id" >
                              <option value="">-- All Classes --</option>
                               @foreach($classes as $class)
                                <option value="{{$class->id}}"> {{$class->class_name}} </option>
                                @endforeach
                            </select>
                          </div>

                           <div class="col-lg-3 col-md-3">
                            <select class="form-control" id="fee_type_select" name="fee_type_id" style="" required="true">
                              <option value="">--Select Fee Type--</option>
                              @foreach($fee_types as $type)
                              <option value="{{$type->id}}"> {{$type->title}} </option>
                              @endforeach
                          </select>
                          </div>

                          <div class="form-group col-md-3">
                              <label for="roll_no" style="visibility: hidden;margin-bottom: 0;"><i class="zmdi zmdi-account material-icons-name pr-2"> Student </i></label>
                              <select class="form-control" id="student_filter" name="student_filter" required="true" style="display: block !important;width: 100%;" disabled="true">
                                <option value="" disabled="true">--Select Student--</option>
                                <option value="{{$student->id}}" selected=""> {{$student->name}}</option>
                              </select>
                          </div>
                    
                          <div class="col-lg-2 col-md-2">
                            <div class="input-group-append">
                              <button class="btn btn-primary" type="reset" id="reset" style="padding: 5px 30px;">Reset</button>  
                            </div>
                          </div>
                          <div class="col-lg-1 col-md-1"></div>

                        </div>
                      </div>
                    </div>

                    <div class="row p-0 mt-4">
                        <div class="col-12 mt-4">
                    
                        <div class="card p-4">
                          <div class="d-sm-flex justify-content-between">
                          <div class="delete-selected-item mb-4 d-none">
                            <a href="javascript:void(0);" class="btn selected-item-btn btn-sm delete_class deleteIcon" data-type="quotation" title="Delete Class" >
                            <i class="zmdi zmdi-delete" style="font-size: 25px;"></i></a>
                          </div>
                        </div>
                        <div class="table-responsive">
                        <table class="table table-striped nowrap table-fee-type">
                        <thead>
                        <tr>
                            {{-- <th>Action</th> --}}
                            <th>Admission No.</th>
                            <th>Name</th>
                            <th>Class</th>
                            <th>Fee Type</th>
                            <th>Amount</th>
                            <th>Remark</th>
                            <th>Submitted Date</th>
                        </tr>
                        </thead>
                        <tfoot>
                          <tr>
                            <th colspan="4" align="right">Total Amount</th>
                            <th></th>
                            <th></th>
                            <th></th>
                          </tr>
                        </tfoot>
                        </table>
                        </div>
                    </div>
                </div>
                    </div>
                      </div>
                    </div>
                    </div>


<!--Add Student Modal -->
<div class="modal fade" id="addStudentFamily" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add Student Family</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form id="add-student-family-form" method="POST" enctype="multipart/form-data">
        <input type="hidden" name="std_id" value="{{$student->id}}">
      <div class="modal-body ">

          <div class="form-row">
            <div class="form-group col-md-6">
                <label for="student_class"><i class="zmdi zmdi-library pr-2"></i>Class</label>
                <select class="form-control" id="student_class" name="student_class">
                  <option value="">--Select Class--</option>
                  @foreach($classes as $class)
                  <option value="{{$class->id}}"> {{$class->class_name}} </option>
                  @endforeach
                </select>
              </div>

               <div class="form-group col-md-6">
                <label for="name"><i class="zmdi zmdi-account material-icons-name pr-2"> Select Father/Guardian</i></label>
                <select class="form-control selecting-students-guardian state-tags" id="student_guardian" name="member" disabled="true">
          
                </select>
            </div>

            <hr>

            <div class="form-group col-md-6">
                <label for="name"><i class="zmdi zmdi-account material-icons-name pr-2"> Select Family Member</i></label>
                <select class="form-control selecting-students state-tags" name="member" disabled="true">
          
                </select>
            </div>
            <div class="form-group col-md-6">
                <label for="relation"><i class="zmdi zmdi-account pr-2"></i> Relation</label>
                <input type="text" name="relation" id="relation" class="form-control" placeholder="Enter Student Relation..."/>
            </div>
          </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save</button>
      </div>
      </form>
    </div>
  </div>
</div>

<!--Add Student Modal -->
<div class="modal fade" id="updateImage" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Update Student Image</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form id="add-student-photo" method="POST" enctype="multipart/form-data">
        <input type="hidden" name="std_id" value="{{$student->id}}">
      <div class="modal-body ">

          <div class="form-row">
            <div class="form-group col-md-12">
                <label for="relation"><i class="zmdi zmdi-account pr-2"></i> Upload Photo</label>
                <input type="file" name="image" id="image" class="form-control" accept="image/*"/>
            </div>
          </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary update_image" disabled="true">Update</button>
      </div>
      </form>
    </div>
  </div>
</div>
@endsection

@section('javascript')
<script type="text/javascript">
$(document).ready(function(){
  $('.custom-tabs .nav-item a').on('click',function(e){
    // alert('hi');
    $('.entriestable').DataTable().ajax.reload();
    // $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
  });
  var std_id = "{{$student->id}}";
   // to make fields double click editable

    var table2 =  $('.table-family').DataTable({
    processing: true,
    searching: false,
    "language": {
    processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw" style="color:#13436c;"></i><span class="sr-only">Loading...</span> '},
    ordering: false,
    pageLength: {{50}},
    serverSide: true,
    autoWidth: false,
    "lengthMenu": [50,100,150,200],
    
    scrollX:true,
    scrollY : '90vh',
    scrollCollapse: true,
    ajax: 
    {
      beforeSend: function(){
        $('#loader_modal').modal({
          backdrop: 'static',
          keyboard: false
        });
        $('#loader_modal').modal('show');
      },
      url: "{!! route('get-student-family') !!}",
       data: function(data) { data.id = std_id } ,
    },
    columns: [
        { data: 'checkbox', name: 'checkbox' }, 
        { data: 'name', name: 'name' }, 
        { data: 'father', name: 'father' }, 
        { data: 'std_class', name: 'std_class' }, 
        { data: 'relation', name: 'relation' }, 
    ],
    initComplete: function () {
    // Enable THEAD scroll bars
    $('.dataTables_scrollHead').css('overflow', 'auto');
    // Sync THEAD scrolling with TBODY
      $('.dataTables_scrollHead').on('scroll', function () {
          $('.dataTables_scrollBody').scrollLeft($(this).scrollLeft());
      });
    },
    drawCallback: function(){
      $('#loader_modal').modal('hide');
    },
  });
      var student_id = "{{$student->id}}";
    var table2 =  $('.table-students-fee').DataTable({
    processing: true,
    searching: false,
    "language": {
    processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw" style="color:#13436c;"></i><span class="sr-only">Loading...</span> '},
    ordering: false,
    pageLength: {{50}},
    serverSide: true,
    autoWidth: false,
    "lengthMenu": [50,100,150,200],
    
    scrollX:true,
    scrollY : '90vh',
    scrollCollapse: true,
    ajax: 
    {
      beforeSend: function(){
        $('#loader_modal').modal({
          backdrop: 'static',
          keyboard: false
        });
        $('#loader_modal').modal('show');
      },
      url: "{!! route('get-single-student-fee') !!}",
       data: function(data) { data.student_classes_select = $('.student_classes_select option:selected').val(),data.discount_student = $('.discount_student option:selected').val(),data.status_student = $('.status_student option:selected').val(), data.from_date = $('#from_date').val(), data.to_date = $('#to_date').val(), data.student_id = student_id } ,
    },
    columns: [
        { data: 'checkbox', name: 'checkbox' }, 
        { data: 'roll_no', name: 'roll_no' }, 
        { data: 'invoice_no', name: 'invoice_no' }, 
        { data: 'name', name: 'name' }, 
        { data: 'class', name: 'class' }, 
        { data: 'gender', name: 'gender' }, 
        { data: 'guardian', name: 'guardian' },  
        { data: 'fee', name: 'fee' }, 
        { data: 'paid_amount', name: 'paid_amount' }, 
        { data: 'unpaid_amount', name: 'unpaid_amount' }, 
        { data: 'discount', name: 'discount' }, 
        { data: 'fee_month', name: 'fee_month' }, 
        { data: 'submitted_date', name: 'submitted_date' }, 
        { data: 'status', name: 'status' }, 
    ],
    initComplete: function () {
    // Enable THEAD scroll bars
    $('.dataTables_scrollHead').css('overflow', 'auto');
    // Sync THEAD scrolling with TBODY
      $('.dataTables_scrollHead').on('scroll', function () {
          $('.dataTables_scrollBody').scrollLeft($(this).scrollLeft());
      });
    },
    drawCallback: function(){
      $('#loader_modal').modal('hide');
    },
  });

        $(document).on('change','.student_classes_select',function(){
    $('.table-students-fee').DataTable().ajax.reload();
    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
  });

  $(document).on('change','.discount_student',function(){
    $('.table-students-fee').DataTable().ajax.reload();
    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
  });

   $(document).on('change','.status_student',function(){
    $('.table-students-fee').DataTable().ajax.reload();
    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
  });

   $(document).on('change','#from_date',function(){
    $('.table-students-fee').DataTable().ajax.reload();
    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
  });

   $(document).on('change','#to_date',function(){
    $('.table-students-fee').DataTable().ajax.reload();
    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
  });

  $(document).on('click','#reset',function(){
    $('.student_classes_select').val('');
    $('.discount_student').val('');
    $('.status_student').val('');
    $('#from_date').val('');
    $('#to_date').val('');
    $('.table-students-fee').DataTable().ajax.reload();
    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
  });

    var table2 =  $('.table-students-fee-month').DataTable({
    processing: true,
    searching: false,
    "language": {
    processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw" style="color:#13436c;"></i><span class="sr-only">Loading...</span> '},
    ordering: false,
    pageLength: {{50}},
    serverSide: true,
    autoWidth: false,
    "lengthMenu": [50,100,150,200],
    
    scrollX:true,
    scrollY : '90vh',
    scrollCollapse: true,
    ajax: 
    {
      beforeSend: function(){
        $('#loader_modal').modal({
          backdrop: 'static',
          keyboard: false
        });
        $('#loader_modal').modal('show');
      },
      url: "{!! route('get-single-student-fee-month') !!}",
       data: function(data) { data.student_id = student_id, data.year = $('#year').val() } ,
    },
    columns: [ 
        { data: 'month', name: 'month' }, 
        { data: 'submitted_date', name: 'submitted_date' }, 
        { data: 'submitted_to', name: 'submitted_to' }, 
        { data: 'total_amount', name: 'total_amount' }, 
        { data: 'paid_amount', name: 'paid_amount' }, 
        { data: 'unpaid_amount', name: 'unpaid_amount' }, 
        { data: 'status', name: 'status' }, 
        { data: 'remark', name: 'remark' }, 
    ],
    initComplete: function () {
    // Enable THEAD scroll bars
    $('.dataTables_scrollHead').css('overflow', 'auto');
    // Sync THEAD scrolling with TBODY
      $('.dataTables_scrollHead').on('scroll', function () {
          $('.dataTables_scrollBody').scrollLeft($(this).scrollLeft());
      });
    },
    drawCallback: function(){
      $('#loader_modal').modal('hide');
    },
  });

  $(document).on('click','#apply_year',function(){
    $('.table-students-fee-month').DataTable().ajax.reload();
    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
  });

  var table2 =  $('.table-fee-type').DataTable({
    processing: true,
    searching: true,
    "language": {
    processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw" style="color:#13436c;"></i><span class="sr-only">Loading...</span> '},
    ordering: false,
    pageLength: {{50}},
    serverSide: true,
    autoWidth: false,
    "lengthMenu": [50,100,150,200],
    
    scrollX:true,
    scrollY : '90vh',
    scrollCollapse: true,
    ajax: 
    {
      beforeSend: function(){
        $('#loader_modal').modal({
          backdrop: 'static',
          keyboard: false
        });
        $('#loader_modal').modal('show');
      },
      url: "{!! route('get-other-fee') !!}",
      data: function(data) { data.student_classes_select = $('.student_classes_select option:selected').val(),data.fee_type_select = $('#fee_type_select option:selected').val(), data.student_id = $('#student_filter option:selected').val() } ,
    },
    columns: [
        // { data: 'checkbox', name: 'checkbox' },  
        { data: 'roll_no', name: 'roll_no' },  
        { data: 'name', name: 'name' }, 
        { data: 'class', name: 'class' }, 
        { data: 'fee_type', name: 'fee_type' }, 
        { data: 'amount', name: 'amount' }, 
        { data: 'remark', name: 'remark' }, 
        { data: 'submitted_date', name: 'submitted_date' }, 
    ],
    initComplete: function () {
    // Enable THEAD scroll bars
    $('.dataTables_scrollHead').css('overflow', 'auto');
    // Sync THEAD scrolling with TBODY
      $('.dataTables_scrollHead').on('scroll', function () {
          $('.dataTables_scrollBody').scrollLeft($(this).scrollLeft());
      });
    },
    drawCallback: function(){
      $('#loader_modal').modal('hide');
    },
    footerCallback: function ( row, data, start, end, display ) {
        var api              = this.api();
        var json             = api.ajax.json();
        var total_amount     = json.total_amount;
        total_amount     = parseFloat(total_amount).toFixed(2);
        total_amount     = total_amount.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ",");
        $( api.column( 4 ).footer() ).html(total_amount); 
      },
  });

  $(document).on('change','.student_classes_select_other_fee',function(){
    $('.table-fee-type').DataTable().ajax.reload();
    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
  });

      $(document).on('change','#fee_type_select',function(){
    $('.table-fee-type').DataTable().ajax.reload();
    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
  });
});
</script>
@stop

