@extends('user.layouts.layout')

@section('title','Teachers Attendance')

@section('content')
<style type="text/css">
   
</style>


                     <div class="row page-title-row">
                            <div class="col-md col-sm col page-title-col align-self-center">
                                <h1 class="page-title">Teachers Attendance History</h1>
                            </div>
                            <div class="col-sm col-auto ml-auto text-right page-action-button">
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addTeacherAttendance">+Submit Attendance</button>
                            </div>
                            
                    </div>

                        
                    <div class="card">
                        <div class="card-body">
                          <div class="selected-item catalogue-btn-group mb-3 d-none">
                            <a href="javascript:void(0)" class="btn btn-outline-primary download_invoice" data-id="1" title="Download Invoice">Download Invoice</a>
                        </div>
                          <div class="row align-items-end form-row">
                            <div class="col-lg-3 col-md-4 col-md-5 col form-group">
                              <label for="date">Attendance Date</label>
                              <input type="date" name="Attendance_date" id="attendance_date" class="form-control" />
                            </div>
                            <div class="col-auto form-group">
                                <input type="button" class="btn btn-primary" id="reset" style="padding: 5px 30px;" value="Reset" />  
                            </div>
                          </div>
                        <table class="table table-striped nowrap table-teachers-attendance">
                        <thead>
                        <tr>
                          <th>S.No</th>                       
                          <th>Attendance Date</th>
                          <th>Present</th>
                          <th>Absent</th>
                          <th>Leave</th>
                          <th>Total</th>
                          <th>Action</th>                       
                            
                        </tr>
                        </thead>
                        </table>
                      </div>
                    </div>

<!--Add Student Modal -->
<div class="modal fade" id="addTeacherAttendance" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Submit Teacher Attendance</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      
      <form id="add-teacher-attendance" method="POST" enctype="multipart/form-data">
      <div class="modal-body ">

          <div class="form-row">
             <div class="form-group col-md-6">
                <label for="date">Attendance Date</label>
                <input type="date" name="Attendance_date" id="date" class="form-control" />
            </div>

          </div>
          
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
      </form>
    </div>
  </div>

  <input type="hidden" name="fees_selected" class="fees_selected">
</div>
@endsection

@section('javascript')
<script type="text/javascript">
    var table2 =  $('.table-teachers-attendance').DataTable({
    processing: true,
    searching: true,
    "language": {
    processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw" style="color:#13436c;"></i><span class="sr-only">Loading...</span> '},
    ordering: false,
    pageLength: {{50}},
    serverSide: true,
    autoWidth: false,
    "lengthMenu": [50,100,150,200],
    
    scrollX:true,
    scrollY : '90vh',
    scrollCollapse: true,
    ajax: 
    {
      beforeSend: function(){
        $('#loader_modal').modal({
          backdrop: 'static',
          keyboard: false
        });
        $('#loader_modal').modal('show');
      },
      url: "{!! route('get-teachers-all-attendance') !!}",
       data: function(data) {data.attendance_date = $('#attendance_date').val()} ,
    },
    columns: [
        { data: 'empty_col', name: 'empty_col' },   
        { data: 'date', name: 'date' },  
        { data: 'present', name: 'present' },  
        { data: 'absent', name: 'absent' },  
        { data: 'leave', name: 'leave' },  
        { data: 'total', name: 'total' },  
        { data: 'action', name: 'action' },   
    ],
    initComplete: function () {
    // Enable THEAD scroll bars
    $('.dataTables_scrollHead').css('overflow', 'auto');
    // Sync THEAD scrolling with TBODY
      $('.dataTables_scrollHead').on('scroll', function () {
          $('.dataTables_scrollBody').scrollLeft($(this).scrollLeft());
      });
    },
    drawCallback: function(){
      $('#loader_modal').modal('hide');
    },
  });
    table2.on( 'draw.dt', function () {
    var PageInfo = $('.table-teachers-attendance').DataTable().page.info();
         table2.column(0, { page: 'current' }).nodes().each( function (cell, i) {
            cell.innerHTML = i + 1 + PageInfo.start;
        } );
    } );
    $('#add-teacher-attendance').on('submit',function(e){
    e.preventDefault();
    var attendance_date = $('#date').val();
    var btn = 'click';

     if(attendance_date == null || attendance_date == '')
    {
      toastr.info('Sorry!', 'Select Date First!!!',{"positionClass": "toast-bottom-right"});
      return false;
    }

    window.location.href = "{{url('user/submit-teacher-attendance')}}/"+attendance_date+"/"+btn;
  });

    $(document).on('change','.select_class_id',function(){
    $('.table-students-attendance').DataTable().ajax.reload();
    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
    });

    $(document).on('change','#attendance_date',function(){
    $('.table-teachers-attendance').DataTable().ajax.reload();
    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
    });
    @if(Session::has('attendance_exists'))
      toastr.info('Sorry!', "Attendance already submitted for the given date!!!",{"positionClass": "toast-bottom-right"});
      @php
       Session()->forget('attendance_exists');
      @endphp
    @endif

    $(document).on('click','#reset',function(){
      $('#attendance_date').val('');
    $('.table-teachers-attendance').DataTable().ajax.reload();
    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
    });

    $(document).on('click','.delete_student_permanant',function(){
      var id = $(this).data('id');
      var attendance_date = $(this).data('att');

        swal({
            title: "Are You Sure!",
            html: true,
            text: "<b>You want to delete this attendance !!!</b>",
            type: "info",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes!",
            cancelButtonText: "No!",
            closeOnConfirm: true,
            closeOnCancel: false
          },
            function(isConfirm) {
            if (isConfirm){
             $.ajaxSetup({
              headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
              }
            });
            $.ajax({
              url: "{{ route('delete-teacher-attendance-permanent') }}",
              method: 'post',
              data: {attendance_date:attendance_date},
              beforeSend: function(){
                $('#loader_modal').modal({
                  backdrop: 'static',
                  keyboard: false
                });
                $('#loader_modal').modal('show');
              },
              success: function (response) {
                if(response.success == false)
                {
                  toastr.warning('Sorry!', 'Something went wrong!!!',{"positionClass": "toast-bottom-right"});
                  $('#loader_modal').modal('hide');
                }

                if(response.success == true)
                {
                  toastr.success('Success!', 'Attendance Deleted Successfully!!!',{"positionClass": "toast-bottom-right"});
                  $('.table-teachers-attendance').DataTable().ajax.reload();
                  $('#loader_modal').modal('hide');
                }
               
              },
              error: function(request, status, error){
                $('#loader_modal').modal('hide');
              }
            });
            }
            else{
              swal("Cancelled", "", "error");
              
            }
          });
  });
</script>
@stop

