@extends('user.layouts.layout')

@section('title','Teachers Attendance')

@section('content')
<style type="text/css">
   
</style>


                    <div class="row page-title-row">
                            <div class="col-md col-sm col page-title-col align-self-center">
                                <h1 class="page-title">Submit Attendance For Teachers</h1>
                            </div>

                            <div class="col-auto ml-auto text-right page-action-button">
                                <a class="btn btn-primary text-white" href="{{route('teachers-all-attendance')}}">Submit</a>
                            </div>
                            
                    </div>

                    <div class="card">
                        <div class="card-body">
                          <div class="selected-item catalogue-btn-group mb-3 d-none">
                            <a href="javascript:void(0)" class="btn btn-outline-primary download_invoice" data-id="1" title="Download Invoice">Download Invoice</a>
                        </div>
                            <div class="mb-3">
                                <button class="btn btn-outline-primary mark_all_present mr-1" data-type="present">Mark Present All</button>
                                <button class="btn btn-outline-primary mark_all_present" data-type="early">Mark Early All</button>
                            </div>
                        <div class="table-responsive">
                        <table class="table table-striped nowrap table-students-attendance">
                        <thead>
                        <tr>
                          <th>Name</th>                          
                          <th>Father/Guardian Name</th>
                          <th>Present</th>
                          <th>Absent</th>
                          <th>Leave</th>
                          <th>Timing</th>
                          <th>Action</th>
                            <!-- <th>Uniform</th> -->
                        </tr>
                        </thead>
                        <tfoot>
                          <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                          </tr>
                        </tfoot>
                        </table>
                        </div>
                    </div>
                </div>

@endsection

@section('javascript')
<script type="text/javascript">
$(document).ready(function(){

  var table2 =  $('.table-students-attendance').DataTable({
    processing: true,
    searching: true,
    "language": {
    processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw" style="color:#13436c;"></i><span class="sr-only">Loading...</span> '},
    ordering: false,
    pageLength: {{50}},
    serverSide: true,
    autoWidth: false,
    "lengthMenu": [50,100,150,200],
    
    scrollX:true,
    scrollY : '90vh',
    scrollCollapse: true,
    ajax: 
    {
      beforeSend: function(){
        $('#loader_modal').modal({
          backdrop: 'static',
          keyboard: false
        });
        $('#loader_modal').modal('show');
      },
      url: "{!! route('get-teachers-for-attendance') !!}",
       data: function(data) {data.attendance_date = "{{$attendance_date}}"} ,
    },
    columns: [
      { data: 'name', name: 'name' },  
      { data: 'guardian', name: 'guardian' },  
      { data: 'present', name: 'present' },  
      { data: 'absent', name: 'absent' },  
      { data: 'leave', name: 'leave' },  
      { data: 'timing', name: 'timing' },  
      { data: 'action', name: 'action' },  
        // { data: 'uniform', name: 'uniform' },  
    ],
    initComplete: function () {
    // Enable THEAD scroll bars
    $('.dataTables_scrollHead').css('overflow', 'auto');
    // Sync THEAD scrolling with TBODY
      $('.dataTables_scrollHead').on('scroll', function () {
          $('.dataTables_scrollBody').scrollLeft($(this).scrollLeft());
      });
    },
    drawCallback: function(){
      $('#loader_modal').modal('hide');
    },
    footerCallback: function ( row, data, start, end, display ) {
        var api              = this.api();
        var json             = api.ajax.json();
        var total_present     = json.total_present;

        total_present     = parseFloat(total_present).toFixed(2);
        total_present     = total_present.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ",");

        var total_absent     = json.total_absent;

        total_absent     = parseFloat(total_absent).toFixed(2);
        total_absent     = total_absent.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ",");

        var total_leave     = json.total_leave;

        total_leave     = parseFloat(total_leave).toFixed(2);
        total_leave     = total_leave.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ",");

        $( api.column( 3 ).footer() ).html(total_present);
        $( api.column( 4 ).footer() ).html(total_absent);
        $( api.column( 5 ).footer() ).html(total_leave);
      },
  });

      $(document).on('click','.condition',function(){
        var id = $(this).data('id');
        var title = $(this).data('title');
        var attendance_date = "{{$attendance_date}}";
        var thisPointer = $(this);
        if($(this).prop("checked") == true)
        {
          var attendance = 1;
        }
        else if($(this).prop("checked") == false)
        {
            var attendance = 0;
        }

        // alert('checked '+attendance+' id '+id+' Title '+title);

        updateTeacherAttendance(attendance,title, id, attendance_date);
        
    });

    function updateTeacherAttendance(attendance,title, id, attendance_date){
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
      });
      $.ajax({
        method: "post",

        url: "{{ route('update-teachers-attendacne') }}",
        dataType: 'json',
        data: {id:id,attendance: attendance, title: title, attendance_date:attendance_date},
        beforeSend: function(){
          $('#loader_modal').modal({
            backdrop: 'static',
            keyboard: false
          });
          $('#loader_modal').modal('show');
        },
        success: function(data)
        {
            if(data.success == true)
            {
              toastr.success('Success!', 'Information Updated Successfully!!!',{"positionClass": "toast-bottom-right"});
            }
            if(data.success == false)
            {
              toastr.error('Sorry!', 'Cannot Update Information!!!',{"positionClass": "toast-bottom-right"});
            }
          $("#loader_modal").modal('hide');
        },

      });
    }

    $('.mark_all_present').on('click',function(e){
        var date = "{{@$attendance_date}}";
        var type = $(this).data('type');
        $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
      });
      $.ajax({
        method: "get",

        url: "{{ route('mark-all-teacher-present') }}",
        dataType: 'json',
        data: {date: date,type:type},
        beforeSend: function(){
          $('#loader_modal').modal({
            backdrop: 'static',
            keyboard: false
          });
          $('#loader_modal').modal('show');
        },
        success: function(data)
        {
            if(data.success == true)
            {
              toastr.success('Success!', 'Information Updated Successfully!!!',{"positionClass": "toast-bottom-right"});
              $('.table-students-attendance').DataTable().ajax.reload();
            }
            if(data.success == false)
            {
              toastr.error('Sorry!', 'Cannot Update Information!!!',{"positionClass": "toast-bottom-right"});
            }
          $("#loader_modal").modal('hide');
        },

      });
    });

});
</script>
@stop

