@extends('user.layouts.layout')

@section('title','Attendence')

@section('content')
<div class="bg-white">
<form id="add-students-attendance" method="POST" enctype="multipart/form-data">
      <div class="modal-body ">

          <div class="form-row">
             <div class="form-group col-sm-6 col-12">
                <label for="all_classes">All Classes</label>
                <select class="form-control form-control class_id state-tags" name="warehouse_id" id="all_classes" {{Auth::user()->role == 'teacher' ? 'disabled' : ''}}>
                  <option value="">-- All Classes --</option>
                  @foreach($classes as $class)
                       @if(Auth::user()->role == 'teacher')
                          <option value="{{$class->id}}" {{Auth::user()->id == $class->teacher_id ? 'selected' : ''}}> {{$class->class_name}} </option>
                       @else
                        <option value="{{$class->id}}"> {{$class->class_name}} </option>
                       @endif
                  @endforeach
                </select>
            </div>

             <div class="form-group col-sm-6 col-12">
                <label for="date">Attendance Date</label>
                <input type="date" name="Attendance_date" id="date" class="form-control" />
            </div>

          </div>
          
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
      </form>
</div>
@endsection

@section('javascript')
<script type="text/javascript">
$(document).ready(function(){
  $('#add-students-attendance').on('submit',function(e){
    e.preventDefault();

    var class_id = $('.class_id').val();
    var attendance_date = $('#date').val();
    var btn = 'click';

    if(class_id == null || class_id == '')
    {
      toastr.info('Sorry!', 'Select Class First!!!',{"positionClass": "toast-bottom-right"});
      return false;
    }

     if(attendance_date == null || attendance_date == '')
    {
      toastr.info('Sorry!', 'Select Date First!!!',{"positionClass": "toast-bottom-right"});
      return false;
    }

    window.location.href = "{{url('user/submit-attendance')}}/"+class_id+"/"+attendance_date+"/"+btn;
  });
});
</script>
@stop

