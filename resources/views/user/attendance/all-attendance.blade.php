@extends('user.layouts.layout')

@section('title','Online Marketing')

@section('content')
<style type="text/css">
   
</style>


                        <div class="row page-title-row">
                            <div class="col-md col-sm col-12 page-title-col">
                                <h1 class="page-title">Students Attendance History</h1>
                            </div>
                            <div class="col-sm-auto ml-auto mt-sm-0 mt-3 text-right page-action-button">
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addStudentFee">+Submit Attendance</button>
                            </div>
                        </div>

                        <div class="selected-item catalogue-btn-group mt-4 mt-sm-3 ml-3 d-none">
                            <a href="javascript:void(0)" class="actionicon viewIcon download_invoice" data-id="1" title="Download Invoice"><i class="zmdi zmdi-download"></i></a>
                        </div>
                        <div class="card">
                            <div class="card-body">
                              <div class="form-row align-items-end pb-1">
                                <div class="form-group col-sm col-6">
                                  <label for="">Classes</label>
                                  <select class="form-control form-control select_class_id state-tags" name="warehouse_id" {{Auth::user()->role == 'teacher' ? 'disabled' : ''}}>
                                    <option value="">All Classes</option>
                                     @foreach($classes as $class)
                                     @if(Auth::user()->role == 'teacher')
                                      <option value="{{$class->id}}" {{Auth::user()->id == $class->teacher_id ? 'selected' : ''}}> {{$class->class_name}} </option>
                                     @else
                                      <option value="{{$class->id}}"> {{$class->class_name}} </option>
                                     @endif
                                      @endforeach
                                  </select>
                                </div>
                                <div class="form-group col-sm col-6">
                                    <label for="date">Attendance Date</label>
                                    <input type="date" name="Attendance_date" id="attendance_date" class="form-control" />
                                </div>
                                <div class="form-group col-sm-auto col-12 text-right">
                                    <button class="btn btn-outline-primary px-md-4 px-3" type="reset" id="reset">Reset</button>  
                                </div>
                              </div>
                              <table class="table table-striped nowrap table-students-attendance">
                                <thead>
                                <tr>
                                    <th>Action</th>
                                    <th>Class</th>                          
                                    <th>Attendance Date</th>
                                    <th>Present</th>
                                    <th>Absent</th>
                                    <th>Leave</th>
                                    <th>Late Students</th>
                                    <th>Without uniform</th>
                                    <th>Total</th> 
                                </tr>
                                </thead>
                              </table>
                            </div>
                        </div>


<!--Add Student Modal -->
<div class="modal fade" id="addStudentFee" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Submit Attendance</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      
      <form id="add-students-attendance" method="POST" enctype="multipart/form-data">
      <div class="modal-body ">

          <div class="form-row">
             <div class="form-group col-sm-6 col-12">
                <label for="all_classes">All Classes</label>
                <select class="form-control form-control class_id state-tags" name="warehouse_id" id="all_classes" {{Auth::user()->role == 'teacher' ? 'disabled' : ''}}>
                  <option value="">-- All Classes --</option>
                  @foreach($classes as $class)
                       @if(Auth::user()->role == 'teacher')
                          <option value="{{$class->id}}" {{Auth::user()->id == $class->teacher_id ? 'selected' : ''}}> {{$class->class_name}} </option>
                       @else
                        <option value="{{$class->id}}"> {{$class->class_name}} </option>
                       @endif
                  @endforeach
                </select>
            </div>

             <div class="form-group col-sm-6 col-12">
                <label for="date">Attendance Date</label>
                <input type="date" name="Attendance_date" id="date" class="form-control" />
            </div>

          </div>
          
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
      </form>
    </div>
  </div>

  <input type="hidden" name="fees_selected" class="fees_selected">
</div>
@endsection

@section('javascript')
<script type="text/javascript">
    var table2 =  $('.table-students-attendance').DataTable({
    processing: true,
    searching: false,
    "language": {
    processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw" style="color:#13436c;"></i><span class="sr-only">Loading...</span> '},
    ordering: false,
    pageLength: {{50}},
    serverSide: true,
    autoWidth: false,
    "lengthMenu": [50,100,150,200],
    
    scrollX:true,
    scrollY : '90vh',
    scrollCollapse: true,
    ajax: 
    {
      beforeSend: function(){
        $('#loader_modal').modal({
          backdrop: 'static',
          keyboard: false
        });
        $('#loader_modal').modal('show');
      },
      url: "{!! route('get-students-all-attendance') !!}",
       data: function(data) {data.class_id = $('.select_class_id').val(), data.attendance_date = $('#attendance_date').val()} ,
    },
    columns: [
        { data: 'action', name: 'action' }, 
        { data: 'class', name: 'class' },  
        { data: 'date', name: 'date' },  
        { data: 'present', name: 'present' },  
        { data: 'absent', name: 'absent' },  
        { data: 'leave', name: 'leave' },  
        { data: 'late', name: 'late' },  
        { data: 'without_uniform', name: 'without_uniform' }, 
        { data: 'total', name: 'total' },  
         
    ],
    initComplete: function () {
    // Enable THEAD scroll bars
    $('.dataTables_scrollHead').css('overflow', 'auto');
    // Sync THEAD scrolling with TBODY
      $('.dataTables_scrollHead').on('scroll', function () {
          $('.dataTables_scrollBody').scrollLeft($(this).scrollLeft());
      });
    },
    drawCallback: function(){
      $('#loader_modal').modal('hide');
    },
  });
    $('#add-students-attendance').on('submit',function(e){
    e.preventDefault();

    var class_id = $('.class_id').val();
    var attendance_date = $('#date').val();
    var btn = 'click';

    if(class_id == null || class_id == '')
    {
      toastr.info('Sorry!', 'Select Class First!!!',{"positionClass": "toast-bottom-right"});
      return false;
    }

     if(attendance_date == null || attendance_date == '')
    {
      toastr.info('Sorry!', 'Select Date First!!!',{"positionClass": "toast-bottom-right"});
      return false;
    }

    window.location.href = "{{url('user/submit-attendance')}}/"+class_id+"/"+attendance_date+"/"+btn;
  });

    $(document).on('change','.select_class_id',function(){
    $('.table-students-attendance').DataTable().ajax.reload();
    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
    });

    $(document).on('change','#attendance_date',function(){
    $('.table-students-attendance').DataTable().ajax.reload();
    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
    });
    @if(Session::has('attendance_exists'))
      toastr.info('Sorry!', "Attendance already submitted for this class",{"positionClass": "toast-bottom-right"});
      @php
       Session()->forget('attendance_exists');
      @endphp
    @endif

    $(document).on('click','.delete_student_permanant',function(){
      var id = $(this).data('id');
      var class_id = $(this).data('class');
      var attendance_date = $(this).data('att');

        swal({
            title: "Are You Sure!",
            html: true,
            text: "<b>You want to delete this attendance !!!</b>",
            type: "info",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes!",
            cancelButtonText: "No!",
            closeOnConfirm: true,
            closeOnCancel: false
          },
            function(isConfirm) {
            if (isConfirm){
             $.ajaxSetup({
              headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
              }
            });
            $.ajax({
              url: "{{ route('delete-attendance-permanent') }}",
              method: 'post',
              data: {class_id:class_id,attendance_date:attendance_date},
              beforeSend: function(){
                $('#loader_modal').modal({
                  backdrop: 'static',
                  keyboard: false
                });
                $('#loader_modal').modal('show');
              },
              success: function (response) {
                if(response.success == false)
                {
                  toastr.warning('Sorry!', 'Something went wrong!!!',{"positionClass": "toast-bottom-right"});
                  $('#loader_modal').modal('hide');
                }

                if(response.success == true)
                {
                  toastr.success('Success!', 'Attendance Deleted Successfully!!!',{"positionClass": "toast-bottom-right"});
                  $('.table-students-attendance').DataTable().ajax.reload();
                  $('#loader_modal').modal('hide');
                }
               
              },
              error: function(request, status, error){
                $('#loader_modal').modal('hide');
              }
            });
            }
            else{
              swal("Cancelled", "", "error");
              
            }
          });
  });
</script>
@stop

