@extends('user.layouts.layout')

@section('title','Single Teacher Attendance')

@section('content')
<style type="text/css">
   .student-image
   {
    padding-top: 100%;
    background-position: center;
    background-size: cover;
   }

   .student_image
   {
    box-shadow: 0px 0px 15px 0px rgba(0,0,0,0.5);

   }
</style>
<?php 
  use Carbon\Carbon;
?>

    <div>
      <div class="row bg-white mt-4 p-2">
        <div class="col-1">
           @if(@$teacher->image != null && file_exists(public_path().'/uploads/teachers/images/'.$teacher->image))
          <img src="{{asset('public/uploads/teachers/images/'.$teacher->image)}}" style="width: 115px;height: 115px;">
          @else
          <img src="{{asset('public/assets/img/user-avatar.png')}}" style="width: 115px;height: 115px;">

          @endif
        </div>
        <div class="col">
          <h4>{{$teacher->name}}</h4>
          <div class="row">
            <div class="col-2">
              <h6>
                <i class="zmdi zmdi-male mr-2"></i>Guardian : {{$teacher->guardian}} 
              </h6>
            </div>
            <div class="col-2">
              <h6>
                <i class="zmdi zmdi-book-photo mr-2"></i>Education : {{$teacher->education}} 
              </h6>
            </div>

            <div class="col-2">
              <h6>
                <i class="zmdi zmdi-account-circle mr-2"></i>Gender : {{$teacher->gender == 'male' ? 'Male' : ($teacher->gender == 'female' ? 'Female' : 'Other')}}
              </h6>
            </div>
             <div class="col-2">
              <h6>
                <i class="zmdi zmdi-email mr-2"></i>Email : {{$teacher->email != null ? $teacher->email : '--'}}
              </h6>
            </div>
            <div class="col-4"></div>
          </div>

          <div class="row">
            <div class="col-2">
              <h6>
                <i class="zmdi zmdi-map mr-2"></i>Address : {{$teacher->address}}
              </h6>
            </div>
            <div class="col-2">
              <h6>
                <i class="zmdi zmdi-phone mr-2"></i>Phone : {{$teacher->phone}}
              </h6>
            </div>

            <div class="col-2">
              <h6>
                <i class="zmdi zmdi-money mr-2"></i>Salary : {{number_format($teacher->salary,2,'.',',')}}
              </h6>
            </div>
            <div class="col-2">
              <h6>
                <i class="zmdi zmdi-nature mr-2"></i>Status : 
                {{-- {{number_format($teacher->salary,2,'.',',')}} --}}
                <span class="m-l-15" id="status"  data-fieldvalue="{{$teacher->status}}">
                                    @if($teacher->status == 1)
                                    <span>Active</span>
                                    @else
                                    <span style="color: red;font-style: italic;"><b>Suspended</b></span>
                                    @endif
                                  </span>
              </h6>
            </div>
            <div class="col-4"></div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-8">
            <h1 class="page-title">Teacher Attendance History By Year</h1>
        </div>                           
      </div>

      <div class="row mb-0">
          <div class="col-lg-12 col-md-12 title-col">
            <div class="d-sm-flex justify-content-left align-items-center">
          
              <div class="col-2">
                  <input type="number" name="year" id="year" class="form-control" value="{{carbon::now()->format('Y')}}" placeholder="Search by year ..." />
              </div>
        
              <div class="col-lg-2 col-md-2">
                <div class="input-group-append">
                  <button class="btn btn-primary" type="button" id="apply_year" style="padding: 5px 30px;">Apply</button>  
                </div>
              </div>

              <div class="col-lg-2 col-md-2">
                <div class="input-group-append">
                  <button class="btn btn-primary" type="button" id="export_attendance" style="padding: 5px 30px;">Export</button>  
                </div>
              </div>

              <div class="col-lg-1 col-md-1 d-flex">
                <div class="input-group-append mr-1" style="width: 20px;height: 20px;border-radius: 50%;background-color: green;">
                    
                </div>
                <span><b>Present</b></span>
              </div>

              <div class="col-lg-1 col-md-1 d-flex">
                <div class="input-group-append mr-1" style="width: 20px;height: 20px;border-radius: 50%;background-color: red;">
                    
                </div>
                <span><b>Absent</b></span>
              </div>

              <div class="col-lg-1 col-md-1 d-flex">
                <div class="input-group-append mr-1" style="width: 20px;height: 20px;border-radius: 50%;background-color: yellow;">
                    
                </div>
                <span><b>Leave</b></span>
              </div>
              {{-- <div class="col-lg-1 col-md-1"></div> --}}

            </div>
          </div>
        </div>

      <div class="row p-0 mt-0">
                <div class="col-12 mt-4">
                <div class="card p-4">
                <div class="table-responsive">
                <table class="table table-striped nowrap table-teachers-fee-month">
                <thead>
                <tr>
                    <th>Month</th>
                    @for($i = 1; $i < 32; $i++)
                    @php $value = str_pad($i,2,"0",STR_PAD_LEFT); @endphp
                    <th>{{$value}}</th>
                    @endfor
                    <th>Total Present</th>
                    <th>Total Absent</th>
                    <th>Total Leave</th>
                    <th>Total Late</th>
                    <th>Total</th>
                </tr>
                </thead>
                </table>
                </div>
                <div class="row" style="font-weight: bold;">
                  <div class="col-12">Total Attendance : <span id="total"></span></div>
                  <div class="col-12">Present Total : <span id="present_total"></span></div>
                  <div class="col-12">Absent Total : <span id="absent_total"></span></div>
                  <div class="col-12">Leave Total : <span id="leave_total"></span></div>
                  <div class="col-12">Late Total : <span id="late_total"></span></div>
                </div>
                </div>
                </div>
            </div>
    </div>
          <input type="hidden" name="salary_selected" class="salary_selected">

>
@endsection

@section('javascript')
<script type="text/javascript">
    var teacher_id = "{{$teacher->id}}";
  $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
      }
      });
  var table2 =  $('.table-teachers-fee-month').DataTable({
    processing: true,
    searching: false,
    "language": {
    processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw" style="color:#13436c;"></i><span class="sr-only">Loading...</span> '},
    ordering: false,
    pageLength: {{50}},
    serverSide: true,
    autoWidth: false,
    "lengthMenu": [50,100,150,200],
    
    scrollX:true,
    scrollY : '90vh',
    scrollCollapse: true,
    ajax: 
    {
      beforeSend: function(){
        $('#loader_modal').modal({
          backdrop: 'static',
          keyboard: false
        });
        $('#loader_modal').modal('show');
      },
      url: "{!! route('get-single-teacher-attendance') !!}",
       data: function(data) { data.teacher_id = teacher_id, data.year = $('#year').val() } ,
       method: 'post'
    },
    columns: [
        { data: 'month', name: 'month' },  
        @for ($i=1; $i < 32; $i++)
        { data: 'month_{{$i}}', name: 'month_{{$i}}' },  
        @endfor
        { data: 'present_total', name: 'present_total' }, 
        { data: 'absent_total', name: 'absent_total' }, 
        { data: 'leave_total', name: 'leave_total' }, 
        { data: 'late', name: 'late' }, 
        { data: 'total', name: 'total' }, 

    ],
    initComplete: function () {
    // Enable THEAD scroll bars
    $('.dataTables_scrollHead').css('overflow', 'auto');
    // Sync THEAD scrolling with TBODY
      $('.dataTables_scrollHead').on('scroll', function () {
          $('.dataTables_scrollBody').scrollLeft($(this).scrollLeft());
      });
    },
    drawCallback: function(){
      $('#loader_modal').modal('hide');

       var api = this.api()
        var json = api.ajax.json();
        
        // var unit_title = json.title;
        var month_total = json.month_total;
        var month_total_present = json.month_total_present;
        var month_total_absent = json.month_total_absent;
        var month_total_leave = json.month_total_leave;
        var month_total_late = json.month_total_late;

          $('#total').html(month_total);
          $('#present_total').html(month_total_present);
          $('#absent_total').html(month_total_absent);
          $('#leave_total').html(month_total_leave);
          $('#late_total').html(month_total_late);
    },
  });

    $(document).on('click','#apply_year',function(){
    $('.table-teachers-fee-month').DataTable().ajax.reload();
    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
  });

    $(document).on('click', '#export_attendance', function(){ 
    var attendance_year = $('#year').val();
    // alert('student id '+student_id+' year '+attendance_year);
     var url = "{{url('user/teacher-attendance-invoice-print')}}"+"/"+teacher_id+"/"+attendance_year;
      window.open(url, 'Teacher Attendance Print', 'width=1200,height=600,scrollbars=yes');
  });

</script>
@stop

