@extends('user.layouts.layout')

@section('title','Single Student')

@section('content')
<style type="text/css">
   .student-image
   {
    padding-top: 100%;
    background-position: center;
    background-size: cover;
   }

   .student_image
   {
    box-shadow: 0px 0px 15px 0px rgba(0,0,0,0.5);

   }
</style>
<?php 
  use Carbon\Carbon;
?>


             <div class="row page-title-row">
                  <div class="col-12">
                      <h1 class="page-title">Student Attendance History By Year</h1>
                  </div>                           
              </div>

           


              <div class="card">
                <div class="card-body">
                  <div class="justify-content-left align-items-center form-row">
                      
                    <div class="form-group col-lg-3 col-md-3 col-sm-4 col-6">
                        <input type="number" name="year" id="year" class="form-control" value="{{carbon::now()->format('Y')}}" placeholder="Search by year ..." />
                    </div>
              
                    <div class="form-group col-auto">
                        <button class="btn btn-primary px-lg-4 px-3" type="button" id="apply_year" style="">Apply</button>  
                    </div>

                    <div class="form-group col-auto">
                        <button class="btn btn-primary px-lg-4 px-3" type="button" id="export_attendance" style="">Export</button>  
                    </div>
                    
                    <div class="col-auto ml-auto">
                      <div class="form-row">
                        <div class="form-group col-auto d-flex">
                          <div class="mr-1" style="min-width: 20px;height: 20px;border-radius: 50%;background-color: green;">
                          </div>
                          <span><b>Present</b></span>
                        </div>
                    <div class="form-group col-auto d-flex">
                      <div class="mr-1" style="min-width: 20px;height: 20px;border-radius: 50%;background-color: red;">
                      </div>
                      <span><b>Absent</b></span>
                    </div>

                    <div class="form-group col-auto d-flex">
                      <div class="mr-1" style="min-width: 20px;height: 20px;border-radius: 50%;background-color: yellow;">
                      </div>
                      <span><b>Leave</b></span>
                    </div>
                    {{-- <div class="col-lg-1 col-md-1"></div> --}}

                  </div> 
                </div>
                </div>
                
                <table class="table table-striped nowrap table-students-fee-month">
                <thead>
                <tr>
                    <th>Month</th>
                    @for($i = 1; $i < 32; $i++)
                    @php $value = str_pad($i,2,"0",STR_PAD_LEFT); @endphp
                    <th>{{$value}}</th>
                    @endfor
                    <th>Total Present</th>
                    <th>Total Absent</th>
                    <th>Total Leave</th>
                    <th>Total Late</th>
                    <th>Without Uniform total</th>
                    <th>Total</th>
                </tr>
                </thead>
                </table>
                
                <div class="row" style="font-weight: bold;">
                  <div class="col-12">Total Attendance : <span id="total"></span></div>
                  <div class="col-12">Present Total : <span id="present_total"></span></div>
                  <div class="col-12">Absent Total : <span id="absent_total"></span></div>
                  <div class="col-12">Leave Total : <span id="leave_total"></span></div>
                  <div class="col-12">Late Total : <span id="late_total"></span></div>
                  <div class="col-12">Without Uniform Total : <span id="without_uniform_total"></span></div>
                </div>
              </div>
            </div>
    

@endsection

@section('javascript')
<script type="text/javascript">
  $(document).ready(function(){
  // $('.js-example-basic-single').select2();
  var student_id = "{{$student->id}}";

    $(document).on('change','.student_classes_select',function(){
    $('.table-students-fee').DataTable().ajax.reload();
    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
  });

  $(document).on('change','.discount_student',function(){
    $('.table-students-fee').DataTable().ajax.reload();
    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
  });

   $(document).on('change','.status_student',function(){
    $('.table-students-fee').DataTable().ajax.reload();
    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
  });

   $(document).on('change','#from_date',function(){
    $('.table-students-fee').DataTable().ajax.reload();
    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
  });

   $(document).on('change','#to_date',function(){
    $('.table-students-fee').DataTable().ajax.reload();
    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
  });

  $(document).on('click','#reset',function(){
    $('.student_classes_select').val('');
    $('.discount_student').val('');
    $('.status_student').val('');
    $('#from_date').val('');
    $('#to_date').val('');
    $('.table-students-fee').DataTable().ajax.reload();
    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
  });

      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
      });

  var table2 =  $('.table-students-fee-month').DataTable({
    processing: true,
    searching: false,
    "language": {
    processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw" style="color:#13436c;"></i><span class="sr-only">Loading...</span> '},
    ordering: false,
    pageLength: {{50}},
    serverSide: true,
    autoWidth: false,
    "lengthMenu": [50,100,150,200],
    
    scrollX:true,
    scrollY : '90vh',
    scrollCollapse: true,
    ajax: 
    {
      beforeSend: function(){
        $('#loader_modal').modal({
          backdrop: 'static',
          keyboard: false
        });
        $('#loader_modal').modal('show');
      },
      url: "{!! route('get-single-student-attendance') !!}",
       data: function(data) { data.student_id = student_id, data.year = $('#year').val(),data.class_id = "{{$class_id}}" } ,
       method: 'post'
    },
    columns: [
        { data: 'month', name: 'month' },  
        @for ($i=1; $i < 32; $i++)
        { data: 'month_{{$i}}', name: 'month_{{$i}}' },  
        @endfor
        { data: 'present_total', name: 'present_total' }, 
        { data: 'absent_total', name: 'absent_total' }, 
        { data: 'leave_total', name: 'leave_total' }, 
        { data: 'late', name: 'late' }, 
        { data: 'without_uniform', name: 'without_uniform' }, 
        { data: 'total', name: 'total' }, 

    ],
    initComplete: function () {
    // Enable THEAD scroll bars
    $('.dataTables_scrollHead').css('overflow', 'auto');
    // Sync THEAD scrolling with TBODY
      $('.dataTables_scrollHead').on('scroll', function () {
          $('.dataTables_scrollBody').scrollLeft($(this).scrollLeft());
      });
    },
    drawCallback: function(){
      $('#loader_modal').modal('hide');

       var api = this.api()
        var json = api.ajax.json();
        
        // var unit_title = json.title;
        var month_total = json.month_total;
        var month_total_present = json.month_total_present;
        var month_total_absent = json.month_total_absent;
        var month_total_leave = json.month_total_leave;
        var month_total_late = json.month_total_late;
        var month_total_without_uniform = json.month_total_without_uniform;

          $('#total').html(month_total);
          $('#present_total').html(month_total_present);
          $('#absent_total').html(month_total_absent);
          $('#leave_total').html(month_total_leave);
          $('#late_total').html(month_total_late);
          $('#without_uniform_total').html(month_total_without_uniform);
    },
  });

  $(document).on('click','#apply_year',function(){
    $('.table-students-fee-month').DataTable().ajax.reload();
    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
  });

  $(document).on('click', '#export_attendance', function(){ 
    var attendance_year = $('#year').val();
    // alert('student id '+student_id+' year '+attendance_year);
     var url = "{{url('user/attendance-invoice-print')}}"+"/"+student_id+"/"+attendance_year;
      window.open(url, 'Teacher Attendance Print', 'width=1200,height=600,scrollbars=yes');
  });

  });
</script>
@stop

