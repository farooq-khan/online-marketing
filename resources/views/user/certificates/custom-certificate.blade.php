@extends('user.layouts.layout')

@section('title','Online Marketing')

@section('content')
<style type="text/css">
   
</style>


        <div class="row page-title-row">
            <div class="col-md col-sm col-12 page-title-col align-self-center">
                <h1 class="page-title">Students Certificates</h1>
            </div>   
          </div>

        <div class="card">
          <div class="card-body">
              <form action="{{route('custom-certificate-print')}}" method="POST" id="edit-version-form" enctype="multipart/form-data">
                  @csrf
                  <div class="row">
                      <div class="form-group col-md-6 col">
                          <label>Certificate Name</label>
                          <input type="text" name="title" id="name" class="form-control"  placeholder="i.e Character Certificate" value="">
                          <div class="alert alert-warning mt-1 alert-title-title d-none" role="alert">
                              Kindly Enter Title!
                          </div>
                      </div>
                      <div class="form-group col-12">
                          <label for="inputEmail3" class="control-label">Detail</label>
                          <textarea type="text" name="detail" class="font-weight-bold form-control-lg form-control summernote ticket-description" id="detail"  placeholder="Description"></textarea>
                      </div>
                      <div class="col-12 text-right">
                          <button type="submit" class="btn btn-primary submit-btn download_invoice">Print</button>
                      </div>
                  </div>
              </form>
          </div>
        </div>



@endsection

@section('javascript')
<!-- include summernote css/js -->
<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.16/dist/summernote.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    $('.summernote').summernote();
  // $(document).on('click', '.download_invoice', function(){
  //   var name = $('#name').val();
  //   var detail = $('#detail').val();
  //   var url = "{{url('user/custom-certificate-print')}}"+"/"+name+"/"+encodeURIComponent(detail);
  //   window.open(url, 'Orders Receivable Print', 'width=1200,height=600,scrollbars=yes');
  // });
});
</script>
@stop

