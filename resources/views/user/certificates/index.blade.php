@extends('user.layouts.layout')

@section('title','Online Marketing')

@section('content')
<style type="text/css">
   
</style>
                    <div class="row page-title-row">
                            <div class="col-md col-sm col-12 page-title-col align-self-center">
                                <h1 class="page-title">Students Certificates</h1>
                            </div>
                            
                    </div>

                    {{-- <div class="row mb-0">
                      <div class="col-lg-12 col-md-12 title-col">
                        <div class="d-sm-flex justify-content-center align-items-center">
                          <div class="col-lg-3 col-md-3">
                            <select class="form-control form-control student_classes_select state-tags" name="warehouse_id" >
                              <option value="">-- All Classes --</option>
                               @foreach($classes as $class)
                                <option value="{{$class->id}}"> {{$class->class_name}} </option>
                                @endforeach
                            </select>
                          </div>
                    
                          <div class="col-lg-2 col-md-2">
                            <div class="input-group-append">
                              <button class="btn btn-primary" type="reset" id="reset" style="padding: 5px 30px;">Reset</button>  
                            </div>
                          </div>
                          <div class="col-lg-4 col-md-4"></div>

                        </div>
                      </div>
                    </div> --}}

                    <div class="card">
                        <div class="card-body">
                          <div class="selected-item catalogue-btn-group d-none mb-2">
                            <a href="javascript:void(0)" class="btn btn-outline-primary download_invoice mr-1 mb-1" data-id="1" title="Download Invoice">  
                              School Leaving Certificate
                            </a>
                            <a href="javascript:void(0)" class="btn btn-outline-primary download_invoice mr-1 mb-1" data-id="2" title="Download Invoice">  
                              Character Certificate
                            </a>
                            <a href="javascript:void(0)" class="btn btn-outline-primary download_invoice mr-1 mb-1" data-id="3" title="Download Invoice">
                              Sport Certificate
                            </a>
                            <select class="sport form-control d-inline-block mb-1" name="sport" style="max-width: 200px;">
                              <option value="">Select Sport</option>
                              <option value="cricket">Cricket</option>
                              <option value="football">Football</option>
                              <option value="basketball">Basketball</option>
                              <option value="volleyball">Volleyball</option>
                              <option value="hockey">Hockey</option>
                            </select>
                        </div>
                          <div class="row align-items-end form-row">
                            <div class="col-lg-3 col-md-4 col-md-5 col form-group">
                                  <select class="form-control form-control student_classes_select state-tags" name="warehouse_id" >
                                    <option value="">-- All Classes --</option>
                                    @foreach($classes as $class)
                                      <option value="{{$class->id}}"> {{$class->class_name}} </option>
                                      @endforeach
                                  </select>
                              </div>
                              <div class="col-auto form-group">
                                <button class="btn btn-primary px-3" type="reset" id="reset">Reset</button> 
                              </div>
                          </div>
                        <table class="table table-striped nowrap table-students">
                        <thead>
                        <tr>
                            <th class="noVis">
                              <div class="custom-control custom-checkbox custom-checkbox1 d-inline-block">
                                <input type="checkbox" class="custom-control-input check-all1" name="check_all" id="check-all">
                                <label class="custom-control-label" for="check-all"></label>
                              </div>
                            </th>
                            <th>Admission No.</th>
                            <th>Name</th>
                            <th>Class</th>
                            <th>Gender</th>
                            <th>Father/Guardian Name</th>
                            <th>Father/Guardian Phone</th>
                            <th>Fee(Inc Discount)</th>
                            <th>Discount</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        </table>
                        </div>
                    </div>

   


          <input type="hidden" name="students_selected" class="students_selected">


@endsection

@section('javascript')
<script type="text/javascript">
$(document).ready(function(){
  var table2 =  $('.table-students').DataTable({
    processing: true,
    searching: true,
    "language": {
    processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw" style="color:#13436c;"></i><span class="sr-only">Loading...</span> '},
    ordering: false,
    pageLength: {{50}},
    serverSide: true,
    autoWidth: false,
    "lengthMenu": [50,100,150,200],
    
    scrollX:true,
    scrollY : '90vh',
    scrollCollapse: true,
    ajax: 
    {
      beforeSend: function(){
        $('#loader_modal').modal({
          backdrop: 'static',
          keyboard: false
        });
        $('#loader_modal').modal('show');
      },
      url: "{!! route('get-students-for-certificates') !!}",
       data: function(data) { data.student_classes_select = $('.student_classes_select option:selected').val(),data.discount_student = $('.discount_student option:selected').val() } ,
    },
    columns: [
        { data: 'checkbox', name: 'checkbox' }, 
        { data: 'roll_no', name: 'roll_no' }, 
        { data: 'name', name: 'name' }, 
        { data: 'class', name: 'class' }, 
        { data: 'gender', name: 'gender' }, 
        { data: 'guardian', name: 'guardian' }, 
        { data: 'guardian_phone', name: 'guardian_phone' }, 
        { data: 'fee', name: 'fee' }, 
        { data: 'discount', name: 'discount' }, 
        { data: 'action', name: 'action' }, 
    ],
    initComplete: function () {
    // Enable THEAD scroll bars
    $('.dataTables_scrollHead').css('overflow', 'auto');
    // Sync THEAD scrolling with TBODY
      $('.dataTables_scrollHead').on('scroll', function () {
          $('.dataTables_scrollBody').scrollLeft($(this).scrollLeft());
      });
    },
    drawCallback: function(){
      $('#loader_modal').modal('hide');
    },
  });


  $(document).on('change','.student_classes_select',function(){
    $('.table-students').DataTable().ajax.reload();
    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
  });

  $(document).on('change','.discount_student',function(){
    $('.table-students').DataTable().ajax.reload();
    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
  });

  $(document).on('click','#reset',function(){
    $('.student_classes_select').val('');
    $('.discount_student').val('');
    $('.table-students').DataTable().ajax.reload();
    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
  });

  $(document).on('click', '.check-all1', function () {
    if(this.checked == true){
    $('.check').prop('checked', true);
    $('.check').parents('tr').addClass('selected');
    var cb_length = $( ".check:checked" ).length;
    if(cb_length > 0)
      {
        $('.selected-item').removeClass('d-none');
      }
    }
    else
    {
      $('.check').prop('checked', false);
      $('.check').parents('tr').removeClass('selected');
      $('.selected-item').addClass('d-none');
      
    }
    });

   $(document).on('click', '.check', function () {
    // $(this).removeClass('d-none');
        var cb_length = $( ".check:checked" ).length;
        var st_pieces = $(this).parents('tr').attr('data-pieces');
        if(this.checked == true){
        $('.selected-item').removeClass('d-none');
        $(this).parents('tr').addClass('selected');
      }else{
        $(this).parents('tr').removeClass('selected');
        if(cb_length == 0){
         $('.selected-item').addClass('d-none');
        }
        
      }
    });

    $(document).on('click', '.download_invoice', function(){
      var id = $(this).data('id');
      if(id == 3)
      {

        if($('.sport').val() == '' || $('.sport').val() == null)
        {
          toastr.info('Sorry!', 'Select Sport First From Dropdown!!!',{"positionClass": "toast-bottom-right"});
          return false;
        }
      }
    var students_selected = [];
    $("input.check:checked").each(function() {
      students_selected.push($(this).val());
    });

    if(students_selected == ''){
            toastr.error('Error!', 'Select Salary First.',{"positionClass": "toast-bottom-right"});
        return false;
        }else{
        $('.students_selected').val(students_selected);

        var students_selected = $('.students_selected').val();

        var url = "{{url('user/certificate-print')}}"+"/"+students_selected+"/"+id+"/"+$('.sport').val();
        window.open(url, 'Orders Receivable Print', 'width=1200,height=600,scrollbars=yes');


       }

    });
});
</script>
@stop

