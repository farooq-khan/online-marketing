@extends('user.layouts.layout')

@section('title','Teachers Certificates')

@section('content')
<style type="text/css">
   
</style>


                    <div class="row page-title-row">
                      <div class="col-md col-sm col-12 page-title-col align-self-center">
                          <h1 class="page-title">Teachers Certificates</h1>
                      </div>   
                    </div>

                    <div class="card">
                      <div class="card-body">
                          <div class="selected-item catalogue-btn-group mb-3 d-none">
                            <a href="javascript:void(0)" class="btn btn-outline-primary download_invoice mr-2 mb-1" data-id="1" title="Download Invoice">
                              Experience Certificate</a>

                            <select class="subject form-control d-inline-block mb-1" name="subject[]" multiple="multiple" style="max-width: 200px;">
                              <option value="">--Select Subject(s)--</option>
                              @foreach($subjects as $subject)
                              <option value="{{$subject->name}}">{{$subject->name}}</option>
                              @endforeach
                            </select>

                        </div>
                        <table class="table table-striped nowrap table-teachers">
                        <thead>
                        <tr>
                            <th class="noVis">
                              <div class="custom-control custom-checkbox custom-checkbox1 d-inline-block">
                                <input type="checkbox" class="custom-control-input check-all1" name="check_all" id="check-all">
                                <label class="custom-control-label" for="check-all"></label>
                              </div>
                            </th>
                            <th>Name</th>
                            <th>Guardian</th>
                            <th>Phone</th>
                            <th>Education</th>
                            <th>Joining Date</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        </table>
                        </div>
                    </div>



          <input type="hidden" name="teachers_selected" class="teachers_selected">


@endsection

@section('javascript')
<script type="text/javascript">
$(document).ready(function(){
  $('.subject').select2();
  var table2 =  $('.table-teachers').DataTable({
    processing: true,
    searching: true,
    "language": {
    processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw" style="color:#13436c;"></i><span class="sr-only">Loading...</span> '},
    ordering: false,
    pageLength: {{50}},
    serverSide: true,
    autoWidth: false,
    "lengthMenu": [50,100,150,200],
    
    scrollX:true,
    scrollY : '90vh',
    scrollCollapse: true,
    ajax: 
    {
      beforeSend: function(){
        $('#loader_modal').modal({
          backdrop: 'static',
          keyboard: false
        });
        $('#loader_modal').modal('show');
      },
      url: "{!! route('get-teachers-for-certificates') !!}",
       data: function(data) { data.student_classes_select = $('.student_classes_select option:selected').val(),data.discount_student = $('.discount_student option:selected').val() } ,
    },
    columns: [
        { data: 'checkbox', name: 'checkbox' }, 
        { data: 'name', name: 'name' }, 
        { data: 'guardian', name: 'guardian' }, 
        { data: 'phone', name: 'phone' }, 
        { data: 'education', name: 'education' }, 
        { data: 'joining_date', name: 'joining_date' }, 
        { data: 'action', name: 'action' }, 
    ],
    initComplete: function () {
    // Enable THEAD scroll bars
    $('.dataTables_scrollHead').css('overflow', 'auto');
    // Sync THEAD scrolling with TBODY
      $('.dataTables_scrollHead').on('scroll', function () {
          $('.dataTables_scrollBody').scrollLeft($(this).scrollLeft());
      });
    },
    drawCallback: function(){
      $('#loader_modal').modal('hide');
    },
  });

  $(document).on('click', '.check-all1', function () {
    if(this.checked == true){
    $('.check').prop('checked', true);
    $('.check').parents('tr').addClass('selected');
    var cb_length = $( ".check:checked" ).length;
    if(cb_length > 0)
      {
        $('.selected-item').removeClass('d-none');
      }
    }
    else
    {
      $('.check').prop('checked', false);
      $('.check').parents('tr').removeClass('selected');
      $('.selected-item').addClass('d-none');
      
    }
    });

   $(document).on('click', '.check', function () {
    // $(this).removeClass('d-none');
        var cb_length = $( ".check:checked" ).length;
        var st_pieces = $(this).parents('tr').attr('data-pieces');
        if(this.checked == true){
        $('.selected-item').removeClass('d-none');
        $(this).parents('tr').addClass('selected');
      }else{
        $(this).parents('tr').removeClass('selected');
        if(cb_length == 0){
         $('.selected-item').addClass('d-none');
        }
        
      }
    });

    $(document).on('click', '.download_invoice', function(){
      var id = $(this).data('id');
    var teachers_selected = [];
    $("input.check:checked").each(function() {
      teachers_selected.push($(this).val());
    });

    if($('.subject').val() == '' || $('.subject').val() == null)
        {
          toastr.info('Sorry!', 'Select Subject First From Dropdown!!!',{"positionClass": "toast-bottom-right"});
          return false;
        }

    if(teachers_selected == ''){
            toastr.error('Error!', 'Select Salary First.',{"positionClass": "toast-bottom-right"});
        return false;
        }else{
        $('.teachers_selected').val(teachers_selected);

        var teachers_selected = $('.teachers_selected').val();

        var url = "{{url('user/teacher-certificate-print')}}"+"/"+teachers_selected+"/"+$('.subject').val();
        window.open(url, 'Orders Receivable Print', 'width=1200,height=600,scrollbars=yes');


       }

    });
});
</script>
@stop

