@extends('user.layouts.layout')

@section('title','All Expenses Types')

@section('content')
<div class="bg-white">
	<form id="add-expense-type-form" method="POST">
        @csrf
      <div class="modal-body ">
          <div class="row mb-0 form-row row-cols-2">

            <div class="form-group col">
                <label for="description">Expense Category</label>
                <select class="form-control" name="expense_category" id="expense_category">
                  <option> -- Please Select Expense Category --</option>
                  <option value="personal">Personal</option>
                  <option value="school">School</option>
                  <option value="other">Other</option>
                </select>
            </div>

            <div class="form-group col">
                <label for="description">Type</label>
                <select class="form-control expense_type common-field" name="expense_type" disabled="true">
                  <option disabled="true"> -- Please Select Expense Type --</option>
                </select>
            </div>

            <div class="form-group col">
                <label for="amount">Total Amount</label>
                <input type="number" name="amount" id="amount" class="form-control common-field" placeholder="Enter Total Amount ..." required="true" disabled="true" />
            </div>

            <div class="form-group col">
                <label for="paid_amount">Paid Amount</label>
                <input type="number" name="paid_amount" id="paid_amount" class="form-control common-field" placeholder="Enter Paid Amount ..." required="true" disabled="true" />
            </div>

            <div class="form-group col">
                <label for="description">Description</label>
                <input type="text" name="description" id="description" class="form-control common-field" placeholder="Expense Description ..." disabled="true" />
            </div>

            <div class="form-group col">
                <label for="expense_date">Purchase Date</label>
                <input type="date" name="expense_date" id="expense_date" class="form-control common-field" required="true" />
            </div>
            <div class="form-group col">
                <label for="payment_date">Payment Date</label>
                <input type="date" name="payment_date" id="payment_date" class="form-control common-field" required="true" />
            </div>

            
          </div>
          
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary save-btn">Add</button>
      </div>
      </form>
</div>
@endsection

@section('javascript')
<script type="text/javascript">
	$(document).ready(function(){
		$('#add-expense-type-form').on('submit',function(e){
    e.preventDefault();

     e.preventDefault();
      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
          }
      });
       $.ajax({
          url: "{{ route('add-expense') }}",
          dataType: 'json',
          type: 'post',
          data: new FormData(this),
          contentType: false,
          cache: false,
          processData:false,
          beforeSend: function(){
            $('.save-btn').addClass('disabled');
            $('.save-btn').attr('disabled', true);
            $('#loader_modal').modal({
              backdrop: 'static',
              keyboard: false
            });
            $('#loader_modal').modal('show');
          },
          success: function(result){
            $('.save-btn').attr('disabled', true);
            $('.save-btn').removeAttr('disabled');
            if(result.success == true){
              toastr.success('Success!', 'Expense Added Successfully!!!',{"positionClass": "toast-bottom-right"});
              $('#add-expense-type-form')[0].reset();
              $('#loader_modal').modal('hide');
            }else{
              toastr.error('Error!', result.errormsg,{"positionClass": "toast-bottom-right"});
              $('#loader_modal').modal('hide');
            }

          },
          error: function (request, status, error) {
                /*$('.form-control').removeClass('is-invalid');
                $('.form-control').next().remove();*/
                $('#loader_modal').modal('hide');
                $('.save-btn').removeClass('disabled');
                $('.save-btn').removeAttr('disabled');
                json = $.parseJSON(request.responseText);
                $.each(json.errors, function(key, value){
                      $('input[name="'+key+'"]').after('<span class="invalid-feedback" role="alert"><strong>'+value+'</strong>');
                     $('input[name="'+key+'"]').addClass('is-invalid');
                     $('textarea[name="'+key+'"]').after('<span class="invalid-feedback" role="alert"><strong>'+value+'</strong>');
                     $('textarea[name="'+key+'"]').addClass('is-invalid');


                });
            }
        });
  });
	});
</script>
@endsection