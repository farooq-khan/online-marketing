@extends('user.layouts.layout')

@section('title','All Expenses Types')

@section('content')
<style type="text/css">
   
</style>


                    <div class="row page-title-row">
                            <div class="col-md col-sm col page-title-col align-self-center">
                                <h1 class="page-title">All Expenses Types</h1>
                            </div>
                            <div class="col-sm col-auto mt-sm-0 mt-3 ml-auto text-right page-action-button">
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addExpenseType">+Add Expense Type</button>
                            </div>    
                    </div>

                    <div class="mb-2" style="font-size: 18px;">
                      <span style="color: red;font-style: italic;">Note : </span> <span style="font-style: italic;padding-left: 5px;"> Double Click The Record For Updating</span>
                    </div>

                    <div class="card">
                        <div class="card-body">
                          <div class="d-sm-flex justify-content-between">
                          <div class="delete-selected-item mb-4 d-none">
                            <a href="javascript:void(0);" class="btn selected-item-btn btn-sm delete_class deleteIcon" data-type="quotation" title="Delete Class" >
                            <i class="zmdi zmdi-delete" style="font-size: 25px;"></i></a>
                          </div>
                        </div>
                        <div class="table-responsive">
                        <table class="table table-striped nowrap table-expense-type">
                        <thead>
                        <tr>
                          <th>Title</th>
                          <th>Type</th>
                          <th>Action</th>
                        </tr>
                        </thead>
                        </table>
                        </div>
                    </div>
                  </div>



<!--Add Student Modal -->
<div class="modal fade" id="addExpenseType" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add Expense Type</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form id="add-expense-type-form" method="POST">
        @csrf
      <div class="modal-body ">
          <div class="form-row">
            <div class="form-group col-md-6">
                <label for="title">Title</label>
                <input type="text" name="title" id="title" class="form-control" placeholder="Expense Type Title" required="true" />
            </div>

            <div class="form-group col-md-6">
                <label for="description">Type</label>
                <select class="form-control" name="expense_type">
                  <option disabled="true"> -- Please Select Expense Type --</option>
                  <option value="personal">Personal</option>
                  <option value="school">School</option>
                  <option value="other">Other</option>
                </select>
            </div>
          </div>
          
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary save-btn">Add</button>
      </div>
      </form>
    </div>
  </div>
</div>
@endsection

@section('javascript')
<script type="text/javascript">
$(document).ready(function(){
  var table2 =  $('.table-expense-type').DataTable({
    processing: true,
    searching: false,
    "language": {
    processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw" style="color:#13436c;"></i><span class="sr-only">Loading...</span> '},
    ordering: false,
    pageLength: {{50}},
    serverSide: true,
    autoWidth: false,
    "lengthMenu": [50,100,150,200],
    
    scrollX:true,
    scrollY : '90vh',
    scrollCollapse: true,
    ajax: 
    {
      beforeSend: function(){
        $('#loader_modal').modal({
          backdrop: 'static',
          keyboard: false
        });
        $('#loader_modal').modal('show');
      },
      url: "{!! route('get-expense-types') !!}",
    },
    columns: [
      { data: 'title', name: 'title' },  
      { data: 'type', name: 'type' }, 
      { data: 'checkbox', name: 'checkbox' },  
    ],
    initComplete: function () {
    // Enable THEAD scroll bars
    $('.dataTables_scrollHead').css('overflow', 'auto');
    // Sync THEAD scrolling with TBODY
      $('.dataTables_scrollHead').on('scroll', function () {
          $('.dataTables_scrollBody').scrollLeft($(this).scrollLeft());
      });
    },
    drawCallback: function(){
      $('#loader_modal').modal('hide');
    },
  });

  $('#add-expense-type-form').on('submit',function(e){
    e.preventDefault();

     e.preventDefault();
      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
          }
      });
       $.ajax({
          url: "{{ route('add-expense-type') }}",
          dataType: 'json',
          type: 'post',
          data: new FormData(this),
          contentType: false,
          cache: false,
          processData:false,
          beforeSend: function(){
            $('.save-btn').addClass('disabled');
            $('.save-btn').attr('disabled', true);
            $('#loader_modal').modal({
              backdrop: 'static',
              keyboard: false
            });
            $('#loader_modal').modal('show');
          },
          success: function(result){
            $('.save-btn').attr('disabled', true);
            $('.save-btn').removeAttr('disabled');
            if(result.success == true){
              toastr.success('Success!', 'Expense Type Added Successfully!!!',{"positionClass": "toast-bottom-right"});
              $('.table-expense-type').DataTable().ajax.reload();
              $('#add-expense-type-form')[0].reset();
              $('#addExpenseType').modal('hide');
            }else{
              toastr.error('Error!', result.errormsg,{"positionClass": "toast-bottom-right"});
              $('#loader_modal').modal('hide');
            }

          },
          error: function (request, status, error) {
                /*$('.form-control').removeClass('is-invalid');
                $('.form-control').next().remove();*/
                $('#loader_modal').modal('hide');
                $('.save-btn').removeClass('disabled');
                $('.save-btn').removeAttr('disabled');
                json = $.parseJSON(request.responseText);
                $.each(json.errors, function(key, value){
                      $('input[name="'+key+'"]').after('<span class="invalid-feedback" role="alert"><strong>'+value+'</strong>');
                     $('input[name="'+key+'"]').addClass('is-invalid');
                     $('textarea[name="'+key+'"]').after('<span class="invalid-feedback" role="alert"><strong>'+value+'</strong>');
                     $('textarea[name="'+key+'"]').addClass('is-invalid');


                });
            }
        });
  });

  $(document).on('click','.delete_fee_type',function(){
      var id = $(this).data('id');

        swal({
            title: "Alert!",
            html: true,
            text: "<b>Are you sure!!! You want to delete this expense type !!!</b>",
            type: "info",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes!",
            cancelButtonText: "No!",
            closeOnConfirm: true,
            closeOnCancel: false
          },
            function(isConfirm) {
            if (isConfirm){
             $.ajaxSetup({
              headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
              }
            });
            $.ajax({
              url: "{{ route('delete-expense-type') }}",
              method: 'post',
              data: {id:id},
              beforeSend: function(){
                $('#loader_modal').modal({
                  backdrop: 'static',
                  keyboard: false
                });
                $('#loader_modal').modal('show');
              },
              success: function (response) {
                if(response.success == false)
                {
                  toastr.warning('Sorry!', 'Cannot delete this expense type, as it is already bond to be submitted!!!',{"positionClass": "toast-bottom-right"});
                  $('#loader_modal').modal('hide');
                }

                if(response.success == true)
                {
                  toastr.success('Success!', 'Expense Type Deleted Successfully!!!',{"positionClass": "toast-bottom-right"});
                  $('.table-expense-type').DataTable().ajax.reload();
                  $('#loader_modal').modal('hide');
                }
               
              },
              error: function(request, status, error){
                $('#loader_modal').modal('hide');
              }
            });
            }
            else{
              swal("Cancelled", "", "error");
              
            }
          });
  });

    $(document).on("dblclick",".inputDoubleClick",function(){
    $(this).addClass('d-none');
    $(this).next().removeClass('d-none');
    $(this).next().addClass('active');
    $(this).next().focus();
  });

    $(document).on('keypress keyup focusout',".fieldFocus",function(e) {
    // alert('hi');
    var id = $(this).data('id');
    if (e.keyCode === 27 && $(this).hasClass('active')) {
      var fieldvalue = $(this).prev().data('fieldvalue');
      var thisPointer = $(this);
      thisPointer.addClass('d-none');
      thisPointer.val(fieldvalue);
      thisPointer.removeClass('active');
      thisPointer.prev().removeClass('d-none');
    }

    var name = $(this).attr('name');
    // alert(name);

    if( (e.keyCode === 13 || e.which === 0) && $(this).val().length > 0 && $(this).hasClass('active')){
      var str = $(this).val();
      if(($(this).val().length < 2 ||  !str.replace(/\s/g, '').length))
      {
        swal({ html:true, title:'Alert !!!', text:'<b>Please Enter At Least 2 Characters (this string may only contain white spaces)!!!</b>'});
        return false;
      }
      else
      {
        $(this).removeClass('active');
        var fieldvalue = $(this).prev().data('fieldvalue');
        var new_value = $(this).val();
        // alert(new_value);
        if(fieldvalue == new_value)
        {
          var thisPointer = $(this);
          thisPointer.addClass('d-none');
          thisPointer.prev().removeClass('d-none');
          $(this).prev().html(fieldvalue);
        }
        else
        {
          var thisPointer = $(this);
          thisPointer.addClass('d-none');
          thisPointer.prev().removeClass('d-none');
          if(new_value != '')
          {
            $(this).prev().html(new_value);
          }
          $(this).prev().data('fieldvalue', new_value);
          saveData(thisPointer,thisPointer.attr('name'), thisPointer.val(),fieldvalue,id);
        }
      }
    }

  });

      function saveData(thisPointer,field_name,field_value,new_select_value,id){

      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
      });
      $.ajax({
        method: "post",

        url: "{{ url('user/update-expense-type-data') }}",
        dataType: 'json',
        data: 'id='+id+'&'+field_name+'='+encodeURIComponent(field_value)+'&'+'new_select_value'+'='+encodeURIComponent(new_select_value),
        beforeSend: function(){
          $('#loader_modal').modal({
            backdrop: 'static',
            keyboard: false
          });
          $('#loader_modal').modal('show');
        },
        success: function(data)
        {
          $("#loader_modal").modal('hide');
          if(data.success == true)
          {
            
            toastr.success('Success!', 'Information updated successfully.',{"positionClass": "toast-bottom-right"});
            return true;
          }

        },

      });
    }
});
</script>
@stop

