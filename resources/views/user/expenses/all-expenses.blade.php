@extends('user.layouts.layout')

@section('title','All Expenses Types')

@section('content')
<style type="text/css">
   
</style>
                    <div class="row page-title-row">
                        <div class="col-md col-sm col page-title-col align-self-center">
                            <h1 class="page-title">All Expenses</h1>
                        </div>
                        <div class="col-sm col-auto mt-sm-0 mt-3 ml-auto text-right page-action-button">
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addExpense">+Add Expense</button>
                        </div>
                    </div>

                    <div class="mb-2">
                      <span style="color: red;font-style: italic;">Note : </span> <span style="font-style: italic;padding-left: 5px;"> Double Click The Record For Updating (Updatable Fields Are Description, Amount and Date)</span>
                    </div>

                    <div class="card card-border-top mb-4">
                      <div class="card-header">
                        <h6 class="mb-0">Filters</h6>
                      </div>
                      <div class="card-body pb-2">
                        <div class="row mb-0 form-row row-cols-lg-4 row-cols-md-3 row-cols-sm-2 row-cols-2">
                          <div class="form-group col">
                            <label for="from_date">From Purhase Date</label>
                              <input type="date" name="from_date" id="from_date" class="form-control" placeholder="Search by year ..." />
                          </div>

                          <div class="form-group col">
                            <label for="to_date">To Purchase Date</label>
                              <input type="date" name="to_date" id="to_date" class="form-control" placeholder="Search by year ..." />
                          </div>

                          <div class="form-group col">
                            <label for="from_payment_date">From Payment Date</label>
                              <input type="date" name="from_payment_date" id="from_payment_date" class="form-control" placeholder="Search by year ..." />
                          </div>

                          <div class="form-group col">
                            <label for="to_payment_date">To Payment Date</label>
                              <input type="date" name="to_payment_date" id="to_payment_date" class="form-control" placeholder="Search by year ..." />
                          </div>

                          <div class="form-group col">
                              <label for="expense_category_filter">Expense Category</label>
                              <select class="form-control" name="expense_category" id="expense_category_filter">
                                <option value=""> -- Please Select Expense Category --</option>
                                <option value="personal">Personal</option>
                                <option value="school">School</option>
                                <option value="other">Other</option>
                              </select>
                          </div>

                          <div class="form-group col">
                              <label for="expense_type_filter">Expense Type</label>
                              <select class="form-control" name="expense_type_filter" id="expense_type_filter">
                                <option value=""> -- Please Select Expense Type --</option>
                               @foreach($expenses as $exp)
                                <option value="{{$exp->id}}">{{$exp->title}}</option>
                               @endforeach
                              </select>
                          </div>
                          <div class="align-self-end col form-group ml-auto text-right">
                              <button class="btn btn-primary px-3" type="reset" id="reset">Reset</button>  
                          </div>

                        </div>
                      </div>
                    </div>


                    <div class="card">
                        <div class="card-body">
                          <div class="d-sm-flex justify-content-between">
                          <div class="delete-selected-item mb-4 d-none">
                            <a href="javascript:void(0);" class="btn selected-item-btn btn-sm delete_class deleteIcon" data-type="quotation" title="Delete Class" >
                            <i class="zmdi zmdi-delete" style="font-size: 25px;"></i></a>
                          </div>
                        </div>
                        <div class="table-responsive">
                        <table class="table table-striped nowrap table-expense-type">
                        <thead>
                        <tr>
                            <th>S.No</th>
                            <th>Title</th>
                            <th>Type</th>
                            <th>Description</th>
                            <th>Purchase Date</th>
                            <th>Total Amount</th>
                            <th>Paid Amount</th>
                            <th>Unpaid Amount</th>
                            <th>Payment Date</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tfoot>
                          <tr>
                            <th colspan="6" align="right">Total</th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                          </tr>
                        </tfoot>
                        </table>
                        </div>
                    </div>
                </div>



<!--Add Student Modal -->
<div class="modal fade" id="addExpense" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add Expense</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form id="add-expense-type-form" method="POST">
        @csrf
      <div class="modal-body ">
          <div class="row mb-0 form-row row-cols-2">

            <div class="form-group col">
                <label for="description">Expense Category</label>
                <select class="form-control" name="expense_category" id="expense_category">
                  <option> -- Please Select Expense Category --</option>
                  <option value="personal">Personal</option>
                  <option value="school">School</option>
                  <option value="other">Other</option>
                </select>
            </div>

            <div class="form-group col">
                <label for="description">Type</label>
                <select class="form-control expense_type common-field" name="expense_type" disabled="true">
                  <option disabled="true"> -- Please Select Expense Type --</option>
                </select>
            </div>

            <div class="form-group col">
                <label for="amount">Total Amount</label>
                <input type="number" name="amount" id="amount" class="form-control common-field" placeholder="Enter Total Amount ..." required="true" disabled="true" />
            </div>

            <div class="form-group col">
                <label for="paid_amount">Paid Amount</label>
                <input type="number" name="paid_amount" id="paid_amount" class="form-control common-field" placeholder="Enter Paid Amount ..." required="true" disabled="true" />
            </div>

            <div class="form-group col">
                <label for="description">Description</label>
                <input type="text" name="description" id="description" class="form-control common-field" placeholder="Expense Description ..." disabled="true" />
            </div>

            <div class="form-group col">
                <label for="expense_date">Purchase Date</label>
                <input type="date" name="expense_date" id="expense_date" class="form-control common-field" required="true" />
            </div>
            <div class="form-group col">
                <label for="payment_date">Payment Date</label>
                <input type="date" name="payment_date" id="payment_date" class="form-control common-field" required="true" />
            </div>

            
          </div>
          
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary save-btn">Add</button>
      </div>
      </form>
    </div>
  </div>
</div>
@endsection

@section('javascript')
<script type="text/javascript">
$(document).ready(function(){
  var table2 =  $('.table-expense-type').DataTable({
    processing: true,
    searching: true,
    "language": {
    processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw" style="color:#13436c;"></i><span class="sr-only">Loading...</span> '},
    ordering: false,
    pageLength: {{50}},
    serverSide: true,
    autoWidth: false,
    "lengthMenu": [50,100,150,200],
    
    scrollX:true,
    scrollY : '90vh',
    scrollCollapse: true,
    ajax: 
    {
      beforeSend: function(){
        $('#loader_modal').modal({
          backdrop: 'static',
          keyboard: false
        });
        $('#loader_modal').modal('show');
      },
      url: "{!! route('get-expense') !!}",
      data: function(data) { data.category_id = $('#expense_category_filter option:selected').val(), data.expense_type = $('#expense_type_filter option:selected').val(), data.from_date = $('#from_date').val(), data.to_date = $('#to_date').val(), data.from_payment_date = $('#from_payment_date').val(), data.to_payment_date = $('#to_payment_date').val() }
    },
    columns: [
        { data: 'empty_col', name: 'empty_col' },  
        { data: 'title', name: 'title' },  
        { data: 'type', name: 'type' }, 
        { data: 'description', name: 'description' }, 
        { data: 'expense_date', name: 'expense_date' }, 
        { data: 'amount', name: 'amount' }, 
        { data: 'paid_amount', name: 'paid_amount' }, 
        { data: 'remaining_amount', name: 'remaining_amount' }, 
        { data: 'payment_date', name: 'payment_date' }, 
        { data: 'status', name: 'status' }, 
        { data: 'checkbox', name: 'checkbox' },  
    ],
    initComplete: function () {
    // Enable THEAD scroll bars
    $('.dataTables_scrollHead').css('overflow', 'auto');
    // Sync THEAD scrolling with TBODY
      $('.dataTables_scrollHead').on('scroll', function () {
          $('.dataTables_scrollBody').scrollLeft($(this).scrollLeft());
      });
    },
    drawCallback: function(){
      $('#loader_modal').modal('hide');
    },
      footerCallback: function ( row, data, start, end, display ) {
        var api              = this.api();
        var json             = api.ajax.json();
        var Unpaid = 0;
        var total_amount     = json.total_amount;
        Unpaid += total_amount;
        total_amount     = parseFloat(total_amount).toFixed(2);
        total_amount     = total_amount.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ",");

        var paid_amount     = json.paid_amount;
        Unpaid -= paid_amount;
        paid_amount     = parseFloat(paid_amount).toFixed(2);
        paid_amount     = paid_amount.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ",");

        Unpaid     = parseFloat(Unpaid).toFixed(2);
        Unpaid     = Unpaid.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ",");
        $( api.column( 6 ).footer() ).html(total_amount); 
        $( api.column( 7 ).footer() ).html(paid_amount); 
        $( api.column( 8 ).footer() ).html(Unpaid); 
      },
  });
  table2.on( 'draw.dt', function () {
    var PageInfo = $('.table-expense-type').DataTable().page.info();
         table2.column(0, { page: 'current' }).nodes().each( function (cell, i) {
            cell.innerHTML = i + 1 + PageInfo.start;
        } );
    } );

  $('#add-expense-type-form').on('submit',function(e){
    e.preventDefault();

     e.preventDefault();
      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
          }
      });
       $.ajax({
          url: "{{ route('add-expense') }}",
          dataType: 'json',
          type: 'post',
          data: new FormData(this),
          contentType: false,
          cache: false,
          processData:false,
          beforeSend: function(){
            $('.save-btn').addClass('disabled');
            $('.save-btn').attr('disabled', true);
            $('#loader_modal').modal({
              backdrop: 'static',
              keyboard: false
            });
            $('#loader_modal').modal('show');
          },
          success: function(result){
            $('.save-btn').attr('disabled', true);
            $('.save-btn').removeAttr('disabled');
            if(result.success == true){
              toastr.success('Success!', 'Expense Added Successfully!!!',{"positionClass": "toast-bottom-right"});
              $('.table-expense-type').DataTable().ajax.reload();
              $('#add-expense-type-form')[0].reset();
              $('#addExpense').modal('hide');
            }else{
              toastr.error('Error!', result.errormsg,{"positionClass": "toast-bottom-right"});
              $('#loader_modal').modal('hide');
            }

          },
          error: function (request, status, error) {
                /*$('.form-control').removeClass('is-invalid');
                $('.form-control').next().remove();*/
                $('#loader_modal').modal('hide');
                $('.save-btn').removeClass('disabled');
                $('.save-btn').removeAttr('disabled');
                json = $.parseJSON(request.responseText);
                $.each(json.errors, function(key, value){
                      $('input[name="'+key+'"]').after('<span class="invalid-feedback" role="alert"><strong>'+value+'</strong>');
                     $('input[name="'+key+'"]').addClass('is-invalid');
                     $('textarea[name="'+key+'"]').after('<span class="invalid-feedback" role="alert"><strong>'+value+'</strong>');
                     $('textarea[name="'+key+'"]').addClass('is-invalid');


                });
            }
        });
  });

  $(document).on('click','.delete_fee_type',function(){
      var id = $(this).data('id');

        swal({
            title: "Alert!",
            html: true,
            text: "<b>Are you sure!!! You want to delete this expense !!!</b>",
            type: "info",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes!",
            cancelButtonText: "No!",
            closeOnConfirm: true,
            closeOnCancel: false
          },
            function(isConfirm) {
            if (isConfirm){
             $.ajaxSetup({
              headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
              }
            });
            $.ajax({
              url: "{{ route('delete-expense') }}",
              method: 'post',
              data: {id:id},
              beforeSend: function(){
                $('#loader_modal').modal({
                  backdrop: 'static',
                  keyboard: false
                });
                $('#loader_modal').modal('show');
              },
              success: function (response) {

                if(response.success == true)
                {
                  toastr.success('Success!', 'Expense Deleted Successfully!!!',{"positionClass": "toast-bottom-right"});
                  $('.table-expense-type').DataTable().ajax.reload();
                  $('#loader_modal').modal('hide');
                }
               
              },
              error: function(request, status, error){
                $('#loader_modal').modal('hide');
              }
            });
            }
            else{
              swal("Cancelled", "", "error");
              
            }
          });
  });

    $(document).on("dblclick",".inputDoubleClick",function(){
    $(this).addClass('d-none');
    $(this).next().removeClass('d-none');
    $(this).next().addClass('active');
    $(this).next().focus();
  });

    $(document).on('keypress keyup focusout',".fieldFocus",function(e) {
    // alert('hi');
    var id = $(this).data('id');
    if (e.keyCode === 27 && $(this).hasClass('active')) {
      var fieldvalue = $(this).prev().data('fieldvalue');
      var thisPointer = $(this);
      thisPointer.addClass('d-none');
      thisPointer.val(fieldvalue);
      thisPointer.removeClass('active');
      thisPointer.prev().removeClass('d-none');
    }

    var name = $(this).attr('name');
    // alert(name);

    if( (e.keyCode === 13 || e.which === 0) && $(this).val().length > 0 && $(this).hasClass('active')){
      var str = $(this).val();
      if(($(this).val().length < 2 ||  !str.replace(/\s/g, '').length))
      {
        swal({ html:true, title:'Alert !!!', text:'<b>Please Enter At Least 2 Characters (this string may only contain white spaces)!!!</b>'});
        return false;
      }
      else
      {
        $(this).removeClass('active');
        var fieldvalue = $(this).prev().data('fieldvalue');
        var new_value = $(this).val();
        // alert(new_value);
        if(fieldvalue == new_value)
        {
          var thisPointer = $(this);
          thisPointer.addClass('d-none');
          thisPointer.prev().removeClass('d-none');
          $(this).prev().html(fieldvalue);
        }
        else
        {
          var thisPointer = $(this);
          thisPointer.addClass('d-none');
          thisPointer.prev().removeClass('d-none');
          if(new_value != '')
          {
            $(this).prev().html(new_value);
          }
          $(this).prev().data('fieldvalue', new_value);
          saveData(thisPointer,thisPointer.attr('name'), thisPointer.val(),fieldvalue,id);
        }
      }
    }

  });

      function saveData(thisPointer,field_name,field_value,new_select_value,id){

      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
      });
      $.ajax({
        method: "post",

        url: "{{ url('user/update-expense-data') }}",
        dataType: 'json',
        data: 'id='+id+'&'+field_name+'='+encodeURIComponent(field_value)+'&'+'new_select_value'+'='+encodeURIComponent(new_select_value),
        beforeSend: function(){
          $('#loader_modal').modal({
            backdrop: 'static',
            keyboard: false
          });
          $('#loader_modal').modal('show');
        },
        success: function(data)
        {
          $("#loader_modal").modal('hide');
          if(data.success == true)
          {
            
            toastr.success('Success!', 'Information updated successfully.',{"positionClass": "toast-bottom-right"});
            $('.table-expense-type').DataTable().ajax.reload();
            return true;
          }

        },

      });
    }

    $(document).on('change','#expense_category',function(){
    // alert($(this).val());
    var id = $(this).val();

    $.ajaxSetup({
              headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
              }
            });
            $.ajax({
              url: "{{ route('fetch-expense-category') }}",
              method: 'post',
              data: {id:id},
              beforeSend: function(){
                $('#loader_modal').modal({
                  backdrop: 'static',
                  keyboard: false
                });
                $('#loader_modal').modal('show');
              },
              success: function (response) {
                
                if(response.success == true)
                {
                  // alert(response.fee);
                  $('.expense_type').empty();
                  $('.expense_type').append(response.html);
                  $('.common-field').attr('disabled',false);
                  $('#loader_modal').modal('hide');

                }
               
              },
              error: function(request, status, error){
                $('#loader_modal').modal('hide');
              }
            });
    });

    $(document).on('change','#expense_category_filter',function(){
      $('.table-expense-type').DataTable().ajax.reload();
      $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
    });

    $(document).on('change','#expense_type_filter',function(){
      $('.table-expense-type').DataTable().ajax.reload();
      $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
    });

    $(document).on('change','#from_date',function(){
      $('.table-expense-type').DataTable().ajax.reload();
      $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
    });

    $(document).on('change','#to_date',function(){
      $('.table-expense-type').DataTable().ajax.reload();
      $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
    });
    $(document).on('change','#from_payment_date',function(){
      $('.table-expense-type').DataTable().ajax.reload();
      $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
    });

    $(document).on('change','#to_payment_date',function(){
      $('.table-expense-type').DataTable().ajax.reload();
      $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
    });

    $(document).on('click','#reset',function(){
      $('#expense_category_filter').val('');
      $('#expense_type_filter').val('');
      $('#from_date').val('');
      $('#to_date').val('');
      $('#from_payment_date').val('');
      $('#to_payment_date').val('');
      $('.table-expense-type').DataTable().ajax.reload();
      $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
    })
});
</script>
@stop

