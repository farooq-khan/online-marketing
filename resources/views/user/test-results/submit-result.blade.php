@extends('user.layouts.layout')

@section('title','All Result')

@section('content')
<style type="text/css">
   
</style>

      <div>
          <div class="row page-title-row">
              <div class="col-8">
                  <h1 class="page-title">Submit Students Test Result</h1>
              </div>
              
      </div>

      <div class="row p-0 mt-0">
          <div class="col-12 mt-0">
         <div class="row">
          <div class="col-8">
              <h4 class="mt-0">{{@$find_test->name}} Test Total Marks : {{$find_test->total_marks}}</h4>
          </div>
              
      </div>

      <div class="selected-item catalogue-btn-group mt-4 mt-sm-3 ml-3 d-none">
        <a href="javascript:void(0)" class="actionicon viewIcon download_invoice" data-id="1" title="Download Invoice"><i class="zmdi zmdi-download"></i></a>

        <button class="theme-btn-color mb-3 print_result"> Print Result</button> 
      </div>
      <div class="col-12 mt-4">
        <button class="theme-btn-color mb-3 export_btn"> Export Result</button>
          <button class="theme-btn-color mb-3" data-toggle="modal" data-target="#import-modal"> Import Result</button>
      </div>
          <div class="card p-4">
            <div class="table-responsive">
              <table class="table table-striped nowrap table-students ">
              <thead>
              <tr>
                  <th class="noVis">
                    <div class="custom-control custom-checkbox custom-checkbox1 d-inline-block">
                      <input type="checkbox" class="custom-control-input check-all1" name="check_all" id="check-all">
                      <label class="custom-control-label" for="check-all"></label>
                    </div>
                  </th>
                  <th>Action</th>
                  <th>Admission No.</th>
                  <th>Name</th>
                  <th>Class</th>
                  <th>Father/Guardian Name</th>
                  <th>Exam</th>
                  @foreach($class_sub as $sub)
                  <th>{{$sub->name}}</th>
                  @endforeach
              </tr>
              </thead>
              </table>
            </div>
      </div>
  </div>
      </div>
                    </div>

  <input type="hidden" name="test_selected" class="test_selected">
<!-- Modal for Import Items  -->
<div class="modal fade" id="import-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">BULK IMPORT</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <div class="modal-body">
        <div align="center">
          <label class="mt-2">
            <strong>Note : </strong>Please use the downloaded export file for upload only.<span class="text-danger"> Also Don't Upload Empty File.</span>
          </label>
          <form class="upload-excel-form" id="upload-excel-form" enctype="multipart/form-data">
            @csrf
              <input type="hidden" name="id" id="id" value="{{$id}}">
              <input type="hidden" name="exam_id" value="{{$exam_id}}">
              <input type="hidden" name="from_date" id="from_date" value="{{$test_date}}">
              <input type="hidden" name="to_date" id="to_date" value="{{$to_test_date}}">
            <input type="file" class="form-control" name="product_excel" id="product_excel" accept=".xls,.xlsx" required="" style="white-space: pre-wrap;"><br>
            <button class="btn btn-info product-upload-btn" type="submit">Upload</button>
          </form>
        </div>
      </div>

    </div>
  </div>
</div>

<!-- export form -->
<form id="export_complete_exam_result_form" action="{{route('export-complete-test-result')}}" method="post">
  @csrf
  <input type="hidden" name="id" id="id" value="{{$id}}">
  <input type="hidden" name="exam_id" value="{{$exam_id}}">
  <input type="hidden" name="test_date" id="test_date" value="{{$test_date}}">
  <input type="hidden" name="to_test_date" id="to_test_date" value="{{$to_test_date}}">

</form>
@endsection

@section('javascript')
<script type="text/javascript">
$(document).ready(function(){
  $(document).on('click', '.export_btn', function(e){
    $('#export_complete_exam_result_form').submit();
  });

  $('.upload-excel-form').on('submit',function(e){
    $('#import-modal').modal('hide');
    e.preventDefault();
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
      }
    });
    $.ajax({
      url: "{{ route('upload-test-result') }}",
      method: 'post',
      data: new FormData(this),
      cache: false,
      contentType: false,
      processData: false,
      beforeSend: function(){
        $('#loader_modal').modal({
          backdrop: 'static',
          keyboard: false
        });
        $("#loader_modal").data('bs.modal')._config.backdrop = 'static';
        $('#loader_modal').modal('show');
        $(".product-upload-btn").attr("disabled", true);
      },
      success: function(data){
        $('#upload-excel-form')[0].reset();
        $('#loader_modal').modal('hide');
        $('#import-modal').modal('hide');
      
        if (data.success)
        {
          $('.table-students').DataTable().ajax.reload();
          toastr.success('Success!', 'Information Updated Successfully.' ,{"positionClass": "toast-bottom-right"});
        }
      },
      error: function (request, status, error) {
          $('#loader_modal').modal('hide');
          $(".product-upload-btn").attr("disabled", false);
          $('.po-porducts-details').DataTable().ajax.reload();
          $('.table-purchase-order-history').DataTable().ajax.reload();
          json = $.parseJSON(request.responseText);
          $.each(json.errors, function(key, value){
            $('input[name="'+key+'[]"]').after('<span class="invalid-feedback" role="alert"><strong>'+value+'</strong>');
            $('input[name="'+key+'[]"]').addClass('is-invalid');
          });
        }
    });
  });

  var class_id = "{{$id}}";
  var exam_id = "{{$exam_id}}";
  var test_date = "{{$test_date}}";

        $(document).on('click', '.check-all1', function () {
        if(this.checked == true){
        $('.check').prop('checked', true);
        $('.check').parents('tr').addClass('selected');
        var cb_length = $( ".check:checked" ).length;
        if(cb_length > 0){
          $('.selected-item').removeClass('d-none');
        }
      }else{
        $('.check').prop('checked', false);
        $('.check').parents('tr').removeClass('selected');
        $('.selected-item').addClass('d-none');
        
      }
    });

   $(document).on('click', '.check', function () {
    // $(this).removeClass('d-none');
        var cb_length = $( ".check:checked" ).length;
        var st_pieces = $(this).parents('tr').attr('data-pieces');
        if(this.checked == true){
        $('.selected-item').removeClass('d-none');
        $(this).parents('tr').addClass('selected');
      }else{
        $(this).parents('tr').removeClass('selected');
        if(cb_length == 0){
         $('.selected-item').addClass('d-none');
        }
        
      }
    });

    var table2 =  $('.table-students').DataTable({
    processing: true,
    searching: true,
    "language": {
    processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw" style="color:#13436c;"></i><span class="sr-only">Loading...</span> '},
    ordering: false,
    pageLength: {{50}},
    serverSide: true,
    autoWidth: false,
    "lengthMenu": [50,100,150,200],
    
    scrollX:true,
    scrollY : '90vh',
    scrollCollapse: true,
    ajax: 
    {
      beforeSend: function(){
        $('#loader_modal').modal({
          backdrop: 'static',
          keyboard: false
        });
        $('#loader_modal').modal('show');
      },
      url: "{!! route('get-specific-class-test-result') !!}",
       data: function(data) { data.class_id = class_id, data.exam_id = exam_id, data.test_date = "{{$test_date}}", data.to_test_date = "{{$to_test_date}}", data.new_test = "{{$new_test}}" } ,
    },
    columns: [
        { data: 'checkbox', name: 'checkbox' }, 
        { data: 'action', name: 'action' }, 
        { data: 'roll_no', name: 'roll_no' }, 
        { data: 'name', name: 'name' }, 
        { data: 'class', name: 'class' }, 
        { data: 'guardian', name: 'guardian' }, 
        { data: 'exam', name: 'exam' }, 
        // Dynamic columns start
        @if($class_sub->count() > 0)
        @foreach($class_sub as $sub)
          { data: '{{$sub->name}}', name: '{{$sub->name}}'},
        @endforeach
        @endif
        
    ],
    initComplete: function () {
    // Enable THEAD scroll bars
    $('.dataTables_scrollHead').css('overflow', 'auto');
    // Sync THEAD scrolling with TBODY
      $('.dataTables_scrollHead').on('scroll', function () {
          $('.dataTables_scrollBody').scrollLeft($(this).scrollLeft());
      });
    },
    drawCallback: function(){
      $('#loader_modal').modal('hide');
    },
  });

      $(document).on("dblclick",".inputDoubleClick",function(){
    $(this).addClass('d-none');
    $(this).next().removeClass('d-none');
    $(this).next().addClass('active');
    $(this).next().focus();
  });

  $(document).on('keypress keyup focusout',".fieldFocus",function(e) {

    var subject_id = $(this).data('id');
    var student_id = $(this).data('student');
    // alert(student_id);
    if (e.keyCode === 27 && $(this).hasClass('active')) {
      var fieldvalue = $(this).prev().data('fieldvalue');
      var thisPointer = $(this);
      thisPointer.addClass('d-none');
      thisPointer.val(fieldvalue);
      thisPointer.removeClass('active');
      thisPointer.prev().removeClass('d-none');
    }

    var name = $(this).attr('name');
    // alert(name);

    if( (e.keyCode === 13 || e.which === 0) && $(this).val().length > 0 && $(this).hasClass('active')){
      var str = $(this).val();
      if(($(this).val().length < 1 ||  !str.replace(/\s/g, '').length))
      {
        swal({ html:true, title:'Alert !!!', text:'<b>Please Enter At Least 2 Characters (this string may only contain white spaces)!!!</b>'});
        return false;
      }
      else
      {
        $(this).removeClass('active');
        var fieldvalue = $(this).prev().data('fieldvalue');
        var new_value = $(this).val();
        // alert(new_value);
        if(fieldvalue == new_value)
        {
          var thisPointer = $(this);
          thisPointer.addClass('d-none');
          thisPointer.prev().removeClass('d-none');
          $(this).prev().html(fieldvalue);
        }
        else
        {
          var thisPointer = $(this);
          thisPointer.addClass('d-none');
          thisPointer.prev().removeClass('d-none');
          if(new_value != '')
          {
            $(this).prev().html(new_value);
          }
          $(this).prev().data('fieldvalue', new_value);
          saveStudentData(thisPointer,thisPointer.attr('name'), thisPointer.val(),exam_id,class_id,subject_id,student_id);
        }
      }
    }

  });

        function saveStudentData(thisPointer,field_name,field_value,exam_id,class_id,subject_id,student_id){
      // alert('hi');
      var id = 200000;
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
      });
      $.ajax({
        method: "post",

        url: "{{ url('user/update-student-test-result') }}",
        dataType: 'json',
        data: 'id='+id+'&'+field_name+'='+encodeURIComponent(field_value)+'&'+'exam_id'+'='+exam_id+'&'+'class_id'+'='+class_id+'&'+'subject_id'+'='+subject_id+'&'+'student_id'+'='+student_id+'&'+'test_date='+"{{$test_date}}"+'&'+'to_test_date='+"{{$to_test_date}}",
        beforeSend: function(){
          $('#loader_modal').modal({
            backdrop: 'static',
            keyboard: false
          });
          $('#loader_modal').modal('show');
        },
        success: function(data)
        {
          $("#loader_modal").modal('hide');
          if(data.success == true)
          {
            
            toastr.success('Success!', 'Information updated successfully.',{"positionClass": "toast-bottom-right"});
            return true;
          }

          if(data.score_greater == true)
          {
            toastr.info('Sorry!', 'Obtained marks cannot be greater than total marks and less than zero!!!',{"positionClass": "toast-bottom-right"});
            $('.table-students').DataTable().ajax.reload();
            return true;
          }

        },

      });
    }

  $(document).on('click', '.download_invoice', function(){
    var selected_fees = [];
    $("input.check:checked").each(function() {
      selected_fees.push($(this).val());
    });

    if(selected_fees == ''){
            toastr.error('Error!', 'Select Test First.',{"positionClass": "toast-bottom-right"});
        return false;
        }else{
        $('.test_selected').val(selected_fees);

        var std_ids = $('.test_selected').val();
        var url = "{{url('user/test-invoice-print')}}"+"/"+std_ids+"/"+class_id+"/"+exam_id+"/"+test_date;
        window.open(url, 'Test Print', 'width=1200,height=600,scrollbars=yes');
       }

    });

  $(document).on('click', '.print_result', function(){
    var selected_fees = [];
    $("input.check:checked").each(function() {
      selected_fees.push($(this).val());
    });

    if(selected_fees == ''){
            toastr.error('Error!', 'Select Test First.',{"positionClass": "toast-bottom-right"});
        return false;
        }else{
        $('.test_selected').val(selected_fees);

        var std_ids = $('.test_selected').val();
        var url = "{{url('user/test-invoice-print-full')}}"+"/"+std_ids+"/"+class_id+"/"+exam_id+"/"+test_date;
        window.open(url, 'Test Print', 'width=1200,height=600,scrollbars=yes');
       }

    });
});
</script>
@stop

