@extends('user.layouts.layout')

@section('title','All Test Results')

@section('content')
<style type="text/css">
   
</style>

    <div class="row page-title-row">
        <div class="col-md col-sm col page-title-col align-self-center">
            <h1 class="page-title">Students Test(s) Results</h1>
        </div>
        <div class="col-sm col-auto mt-sm-0 ml-auto text-right page-action-button">
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addResult">+Add Result</button>
        </div> 
    </div>

    <div class="card card-border-top mb-4">
      <div class="card-header">
        <h6 class="mb-0">Filters</h6>
      </div>
      <div class="card-body pb-2">
        <div class="row mb-0 form-row row-cols-xl-4 row-cols-md-4 row-cols-sm-2 row-cols-2">
          <div class="col form-group">
            <label for="to_date">Class</label>
            <select class="form-control form-control student_classes_select1 state-tags" name="warehouse_id" {{Auth::user()->role == 'teacher' ? 'disabled' : ''}}>
              <option value="">-- All Classes --</option>
               @foreach($classes as $class)
                <!-- <option value="{{$class->id}}"> {{$class->class_name}} </option> -->
                @if(Auth::user()->role == 'teacher')
                  <option value="{{$class->id}}" {{Auth::user()->id == $class->teacher_id ? 'selected' : ''}}> {{$class->class_name}} </option>
                @else
                  <option value="{{$class->id}}"> {{$class->class_name}} </option>
                @endif
                @endforeach
            </select>
          </div>
           <div class="col form-group">
            <label for="from_date">Exam</label>
            <select class="form-control form-control exam_id state-tags" name="exam_id2" >
              <option value="">-- All Exams --</option>
               @foreach($exams as $exam)
                <option value="{{$exam->id}}"> {{$exam->name}} </option>
                @endforeach
            </select>
          </div>
          <div class="col form-group">
            <label for="from_date">From Date</label>
            <input type="date" name="from_test_date" id="from_test_date" class="form-control" placeholder="Admission Date"/>
          </div>
          <div class="col form-group">
            <label for="to_date">To Date</label>
            <input type="date" name="to_test_datee" id="to_test_datee" class="form-control" placeholder="Admission Date"/>
          </div>
        </div>
      </div>
    </div>

    <div class="card">
      <div class="card-body">
        <table class="table table-striped nowrap table-students ">
        <thead>
        <tr>
          <th>Class</th>
          <th>Test Name</th>
          <th>Test Marks</th>
          <th>Start Date</th>
          <th>End Date</th>
          <th>Action</th>
        </tr>
        </thead>
        </table>
      </div>
    </div>


<!--Add Student Modal -->
<div class="modal fade" id="addResult" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add Result</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form id="add-exam-form" method="POST">
        @csrf
      <div class="modal-body pb-2">
          <div class="row mb-0 form-row row-cols-lg-4 row-cols-sm-2 row-cols-1">
              <div class="col form-group">
                <label for="">Class</label>
                <select class="form-control form-control student_classes_select state-tags" name="class_id" {{Auth::user()->role == 'teacher' ? 'disabled' : ''}}>
                  <option value="">-- All Classes --</option>
                    @foreach($classes as $class)
                    <!-- <option value="{{$class->id}}"> {{$class->class_name}} </option> -->@if(Auth::user()->role == 'teacher')
                      <option value="{{$class->id}}" {{Auth::user()->id == $class->teacher_id ? 'selected' : ''}}> {{$class->class_name}} </option>
                    @else
                      <option value="{{$class->id}}"> {{$class->class_name}} </option>
                    @endif
                    @endforeach
                </select>
              </div>
              <div class="col form-group">
                <label for="">Exam</label>
                <select class="form-control form-control student_exam_id state-tags" name="exam_id" >
                  <option value="">-- All Exams --</option>
                    @foreach($exams as $exam)
                    <option value="{{$exam->id}}"> {{$exam->name}} </option>
                    @endforeach
                </select>
              </div>
              <div class="col form-group">
                  <label for="test_date">From Test Date</label>
                  <input type="date" name="test_date" id="test_date" class="form-control" placeholder="Admission Date"/>
                </div>
                <div class="col form-group">
                  <label for="to_test_date">To Test Date</label>
                  <input type="date" name="to_test_date" id="to_test_date" class="form-control" placeholder="Admission Date"/>
              </div>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary save-btn add-exam">Add</button>
      </div>
      </form>
    </div>
  </div>
</div>
</div>

@endsection

@section('javascript')
<script type="text/javascript">
$(document).ready(function(){
  $(document).on('click', '.download_invoice', function(){

      var id = $(this).data('id');
      var url = "{{url('user/result-print')}}"+"/"+id;
      window.open(url, 'Orders Receivable Print', 'width=1200,height=600,scrollbars=yes');
      $('#loader_modal').modal('hide');


    });

  $(document).on('click','.add-exam',function(e){
    var class_id = $('.student_classes_select').val();
    var exam_id = $('.student_exam_id').val();
    var test_date = $('#test_date').val();
    var to_test_date = $('#to_test_date').val();
    var new_test = true;

    window.location.href = "{{url('user/submit-test-result')}}/"+class_id+"/"+exam_id+"/"+test_date+"/"+to_test_date+"/"+new_test;
  });

    var table2 =  $('.table-students').DataTable({
    processing: true,
    searching: false,
    "language": {
    processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw" style="color:#13436c;"></i><span class="sr-only">Loading...</span> '},
    ordering: false,
    pageLength: {{50}},
    serverSide: true,
    autoWidth: false,
    "lengthMenu": [50,100,150,200],
    
    scrollX:true,
    scrollY : '90vh',
    scrollCollapse: true,
    ajax: 
    {
      beforeSend: function(){
        $('#loader_modal').modal({
          backdrop: 'static',
          keyboard: false
        });
        $('#loader_modal').modal('show');
      },
      url: "{!! route('get-all-test-results') !!}",
       data: function(data) { data.student_classes_select = $('.student_classes_select1 option:selected').val(), data.exam_id = $('.exam_id option:selected').val(), data.from_test_date = $('#from_test_date').val(), data.to_test_date = $('#to_test_datee').val() } ,
    },
    columns: [
      { data: 'class', name: 'class' }, 
      { data: 'test_name', name: 'test_name' }, 
      { data: 'total_marks', name: 'total_marks' }, 
      { data: 'test_date', name: 'test_date' }, 
      { data: 'to_test_date', name: 'to_test_date' }, 
      { data: 'checkbox', name: 'checkbox' }, 
        
    ],
    initComplete: function () {
    // Enable THEAD scroll bars
    $('.dataTables_scrollHead').css('overflow', 'auto');
    // Sync THEAD scrolling with TBODY
      $('.dataTables_scrollHead').on('scroll', function () {
          $('.dataTables_scrollBody').scrollLeft($(this).scrollLeft());
      });
    },
    drawCallback: function(){
      $('#loader_modal').modal('hide');
    },
  });

  $(document).on('change','.student_classes_select1',function(){
    $('.table-students').DataTable().ajax.reload();
    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
  });

  $(document).on('change','.exam_id',function(){
    $('.table-students').DataTable().ajax.reload();
    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
  });

  $(document).on('change','#from_test_date',function(){
    $('.table-students').DataTable().ajax.reload();
    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
  });

    $(document).on('change','#to_test_datee',function(){
    $('.table-students').DataTable().ajax.reload();
    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
  });
                $(document).on('click','.delete_student_permanant',function(){
      var id = $(this).data('id');

        swal({
            title: "Are You Sure!",
            html: true,
            text: "<b>You want to delete this record !!!</b>",
            type: "info",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes!",
            cancelButtonText: "No!",
            closeOnConfirm: true,
            closeOnCancel: false
          },
            function(isConfirm) {
            if (isConfirm){
             $.ajaxSetup({
              headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
              }
            });
            $.ajax({
              url: "{{ route('delete-student-test-result-permanent') }}",
              method: 'post',
              data: {id:id},
              beforeSend: function(){
                $('#loader_modal').modal({
                  backdrop: 'static',
                  keyboard: false
                });
                $('#loader_modal').modal('show');
              },
              success: function (response) {

                if(response.success == true)
                {
                  toastr.success('Success!', 'Record Deleted Successfully!!!',{"positionClass": "toast-bottom-right"});
                  $('.table-students').DataTable().ajax.reload();
                  $('#loader_modal').modal('hide');
                }
               
              },
              error: function(request, status, error){
                $('#loader_modal').modal('hide');
              }
            });
            }
            else{
              swal("Cancelled", "", "error");
              
            }
          });
  });
});
</script>
@stop

