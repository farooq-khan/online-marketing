@extends('user.layouts.layout')

@section('title','Single Student')

@section('content')
<style type="text/css">
   .student-image
   {
    padding-top: 100%;
    background-position: center;
    background-size: cover;
   }

   .student_image
   {
    box-shadow: 0px 0px 15px 0px rgba(0,0,0,0.5);

   }
</style>

                        <div class="row page-title-row">
                            <div class="col-12">
                                <h1 class="page-title">Student Test History</h1>
                            </div>                           
                        </div>
                        
                        <div class="card">
                          <div class="card-body">
                            <div class="selected-item catalogue-btn-group mb-3 d-none">
                              <a href="javascript:void(0)" class="btn- btn-primary download_invoice">Print</a>
                            </div>
                            <div class="row">
                              <div class="form-group col-lg-4 col-md-5 col-sm-5 col-12">
                                <label for="">Classes</label>
                                    <select class="form-control form-control student_classes_select state-tags" name="warehouse_id" >
                                      <option value="">All Classes</option>
                                       @foreach($student_classes as $class)
                                        <option value="{{$class->id}}" {{$class_id == $class->id ? 'selected' : ''}}> {{$class->class_name}} </option>
                                        @endforeach
                                    </select>
                                  </div>
                            </div>
                          <table class="table table-striped nowrap table-exam-history text-center nowrap">
                          <thead>
                          <tr>
                              <th class="noVis">
                                <div class="custom-control custom-checkbox custom-checkbox1 d-inline-block">
                                  <input type="checkbox" class="custom-control-input check-all1" name="check_all" id="check-all">
                                  <label class="custom-control-label" for="check-all"></label>
                                </div>
                              </th>
                              <th>Action</th>
                              <th class="important">Admission No.</th>
                              <th class="important">Name</th>
                              <th class="important">Class</th>
                              <th class="important">Father/Guardian Name</th>
                              <th class="important">Exam</th>
                              @foreach($class_sub as $sub)
                              <th class="important">{{$sub->name}}</th>
                              @endforeach
                              <th class="important">Total Marks</th>
                              <th class="important">Obtained Marks</th>
                              <th class="important">Week Start Date</th>
                              <th class="important">Week End Date</th>
                              <th class="important">Percentage</th>
                              <th class="important">Grade & Position</th>
                          </tr>
                          </thead>
                          </table>
                          </div>
                    </div>


<!--Add Student Modal -->
<div class="modal fade" id="addStudentFamily" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add Student Family</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form id="add-student-family-form" method="POST" enctype="multipart/form-data">
        <input type="hidden" name="std_id" value="{{$student->id}}">
      <div class="modal-body ">

          <div class="form-row">
            <div class="form-group col-md-6">
                <label for="student_class"><i class="zmdi zmdi-library pr-2"></i>Class</label>
                <select class="form-control" id="student_class" name="student_class">
                  <option value="">--Select Class--</option>
                  @foreach($classes as $class)
                  <option value="{{$class->id}}"> {{$class->class_name}} </option>
                  @endforeach
                </select>
              </div>

               <div class="form-group col-md-6">
                <label for="name"><i class="zmdi zmdi-account material-icons-name pr-2"> Select Father/Guardian</i></label>
                <select class="form-control selecting-students-guardian state-tags" id="student_guardian" name="member" disabled="true">
          
                </select>
            </div>

            <hr>

            <div class="form-group col-md-6">
                <label for="name"><i class="zmdi zmdi-account material-icons-name pr-2"> Select Family Member</i></label>
                <select class="form-control selecting-students state-tags" name="member" disabled="true">
          
                </select>
            </div>
            <div class="form-group col-md-6">
                <label for="relation"><i class="zmdi zmdi-account pr-2"></i> Relation</label>
                <input type="text" name="relation" id="relation" class="form-control" placeholder="Enter Student Relation..."/>
            </div>
          </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Add</button>
      </div>
      </form>
    </div>
  </div>
</div>

<!--Add Student Modal -->
<div class="modal fade" id="updateImage" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Update Student Image</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form id="add-student-photo" method="POST" enctype="multipart/form-data">
        <input type="hidden" name="std_id" value="{{$student->id}}">
      <div class="modal-body ">

          <div class="form-row">
            <div class="form-group col-md-12">
                <label for="relation"><i class="zmdi zmdi-account pr-2"></i> Upload Photo</label>
                <input type="file" name="image" id="image" class="form-control" accept="image/*"/>
            </div>
          </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Update</button>
      </div>
      </form>
    </div>
  </div>
</div>
  <input type="hidden" name="fees_selected" class="fees_selected">

@endsection

@section('javascript')
<script type="text/javascript">
$(document).ready(function(){
    $(document).on('click', '.check-all1', function () {
        if(this.checked == true){
        $('.check').prop('checked', true);
        $('.check').parents('tr').addClass('selected');
        var cb_length = $( ".check:checked" ).length;
        if(cb_length > 0){
          $('.selected-item').removeClass('d-none');
        }
      }else{
        $('.check').prop('checked', false);
        $('.check').parents('tr').removeClass('selected');
        $('.selected-item').addClass('d-none');
        
      }
    });

   $(document).on('click', '.check', function () {
        var cb_length = $( ".check:checked" ).length;
        var st_pieces = $(this).parents('tr').attr('data-pieces');
        if(this.checked == true){
        $('.selected-item').removeClass('d-none');
        $(this).parents('tr').addClass('selected');
      }else{
        $(this).parents('tr').removeClass('selected');
        if(cb_length == 0){
         $('.selected-item').addClass('d-none');
        }
        
      }
    });

    $(document).on('click', '.download_invoice', function(){

      var id = $(this).data('id');
      var url = "{{url('user/result-print')}}"+"/"+id;
      window.open(url, 'Orders Receivable Print', 'width=1200,height=600,scrollbars=yes');
      $('#loader_modal').modal('hide');


    });
    
  var std_id = "{{$id}}";
   // to make fields double click editable
  $(document).on("dblclick",".inputDoubleClick",function(){
    $(this).addClass('d-none');
    $(this).next().removeClass('d-none');
    $(this).next().addClass('active');
    $(this).next().focus();
  });

    $(document).on('keypress keyup focusout',".fieldFocus",function(e) {
    // alert('hi');
    if (e.keyCode === 27 && $(this).hasClass('active')) {
      var fieldvalue = $(this).prev().data('fieldvalue');
      var thisPointer = $(this);
      thisPointer.addClass('d-none');
      thisPointer.val(fieldvalue);
      thisPointer.removeClass('active');
      thisPointer.prev().removeClass('d-none');
    }

    var name = $(this).attr('name');
    // alert(name);

    if( (e.keyCode === 13 || e.which === 0) && $(this).val().length > 0 && $(this).hasClass('active')){
      var str = $(this).val();
      if(($(this).val().length < 2 ||  !str.replace(/\s/g, '').length))
      {
        swal({ html:true, title:'Alert !!!', text:'<b>Please Enter At Least 2 Characters (this string may only contain white spaces)!!!</b>'});
        return false;
      }
      else
      {
        $(this).removeClass('active');
        var fieldvalue = $(this).prev().data('fieldvalue');
        var new_value = $(this).val();
        // alert(new_value);
        if(fieldvalue == new_value)
        {
          var thisPointer = $(this);
          thisPointer.addClass('d-none');
          thisPointer.prev().removeClass('d-none');
          $(this).prev().html(fieldvalue);
        }
        else
        {
          var thisPointer = $(this);
          thisPointer.addClass('d-none');
          thisPointer.prev().removeClass('d-none');
          if(new_value != '')
          {
            $(this).prev().html(new_value);
          }
          $(this).prev().data('fieldvalue', new_value);
          saveStudentData(thisPointer,thisPointer.attr('name'), thisPointer.val(),fieldvalue);
        }
      }
    }

  });

      function saveStudentData(thisPointer,field_name,field_value,new_select_value){
      var id = "{{$student->id}}";
      
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
      });
      $.ajax({
        method: "post",

        url: "{{ url('user/update-student-data') }}",
        dataType: 'json',
        data: 'id='+id+'&'+field_name+'='+encodeURIComponent(field_value)+'&'+'new_select_value'+'='+encodeURIComponent(new_select_value),
        beforeSend: function(){
          $('#loader_modal').modal({
            backdrop: 'static',
            keyboard: false
          });
          $('#loader_modal').modal('show');
        },
        success: function(data)
        {
          $("#loader_modal").modal('hide');
          if(data.success == true)
          {
            
            toastr.success('Success!', 'Information updated successfully.',{"positionClass": "toast-bottom-right"});
            return true;
          }

        },

      });
    }

    var table2 =  $('.table-exam-history').DataTable({
    processing: true,
    searching: true,
    "language": {
    processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw" style="color:#13436c;"></i><span class="sr-only">Loading...</span> '},
    ordering: false,
    pageLength: {{50}},
    serverSide: true,
    autoWidth: false,
    "lengthMenu": [50,100,150,200],
    
    scrollX:true,
    scrollY : '90vh',
    scrollCollapse: true,
    dom: 'Bfrtip',
        buttons: [
            {
                extend: 'excelHtml5',
                exportOptions: {
                    columns: ['.important']
                }
            },
        ],
    ajax: 
    {
      beforeSend: function(){
        $('#loader_modal').modal({
          backdrop: 'static',
          keyboard: false
        });
        $('#loader_modal').modal('show');
      },
      url: "{!! route('get-specific-student-test-result') !!}",
       data: function(data) { data.std_id = std_id,data.student_classes_select = $('.student_classes_select option:selected').val() } ,
    },
    columns: [
        { data: 'checkbox', name: 'checkbox' }, 
        { data: 'action', name: 'action' }, 
        { data: 'roll_no', name: 'roll_no' }, 
        { data: 'name', name: 'name' }, 
        { data: 'class', name: 'class' }, 
        { data: 'guardian', name: 'guardian' }, 
        { data: 'exam', name: 'exam' }, 
        // Dynamic columns start
        @if($class_sub->count() > 0)
        @foreach($class_sub as $sub)
          { data: '{{$sub->name}}', name: '{{$sub->name}}'},
        @endforeach
        @endif
        { data: 'total_marks', name: 'total_marks' }, 
        { data: 'obtained_marks', name: 'obtained_marks' }, 
        { data: 'start_date', name: 'start_date' }, 
        { data: 'end_date', name: 'end_date' }, 
        { data: 'percentage', name: 'percentage' }, 
        { data: 'grade_and_position', name: 'grade_and_position' }, 
        
    ],
    initComplete: function () {
    // Enable THEAD scroll bars
    $('.dataTables_scrollHead').css('overflow', 'auto');
    // Sync THEAD scrolling with TBODY
      $('.dataTables_scrollHead').on('scroll', function () {
          $('.dataTables_scrollBody').scrollLeft($(this).scrollLeft());
      });
    },
    drawCallback: function(){
      $('#loader_modal').modal('hide');
    },
  });
  // $(document).on('change','.student_classes_select',function(){
  //   $('.table-exam-history').DataTable().ajax.reload();
  //   $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
  // });

    $(document).on('change','.student_classes_select',function(){
    // $('.table-exam-history').DataTable().ajax.reload();
    // $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
    var student_id = "{{$student->id}}";
    window.location.href = '{{url("user/student-test-history")}}'+'/'+student_id+'/'+$(this).val();
  });

    $(document).on('click', '.download_invoice', function(){
    var selected_fees = [];
    $("input.check:checked").each(function() {
      selected_fees.push($(this).val());
    });
        
      $('.fees_selected').val(selected_fees);

        var ids = $('.fees_selected').val();
        var std_id = "{{$student->id}}";
        var class_id = "{{$class_id}}";
        var url = "{{url('user/test-whole-invoice-print')}}"+"/"+ids+"/"+std_id+"/"+class_id;
        window.open(url, 'Orders Receivable Print', 'width=1200,height=600,scrollbars=yes');
        $('#loader_modal').modal('hide');
    });

      $(document).on('click', '.download_invoice2', function(){
        var ids = $(this).data('id');
        var std_id = "{{$student->id}}";
        var class_id = "{{$class_id}}";
        var url = "{{url('user/test-whole-invoice-print')}}"+"/"+ids+"/"+std_id+"/"+class_id;
        window.open(url, 'Orders Receivable Print', 'width=1200,height=600,scrollbars=yes');
        $('#loader_modal').modal('hide');
    });

});
</script>
@stop

