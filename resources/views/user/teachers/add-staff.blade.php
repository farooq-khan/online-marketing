@extends('user.layouts.layout')

@section('title','Online Marketing')

@section('content')
<div class="bg-white">
  <form id="add-teacher-form" method="POST" enctype="multipart/form-data">
        {{csrf_field()}}
      <div class="modal-body ">

          <div class="form-row row-cols-lg-3 row-cols-sm-2 row-cols-2">
            <div class="form-group col">
                <label for="name">Name</label>
                <input type="text" name="name" id="name" class="form-control" placeholder="Name"/>
            </div>
            <div class="form-group col">
                <label for="guardian">Father/Guardian Name</label>
                <input type="text" name="guardian" id="guardian" class="form-control" placeholder="Father/Guardian Name"/>
            </div>
            <div class="form-group col">
                <label for="guardian_relation">Father/Guardian Relation</label>
                <input type="text" name="guardian_relation" id="guardian_relation" class="form-control" placeholder="Relation"/>
            </div>
            <div class="form-group col">
                <label for="cnic">Cnic</label>
                <input type="text" name="cnic" id="cnic" class="form-control" placeholder="Cnic"/>
            </div>
            <div class="form-group col">
                <label for="phone">Phone No.</label>
                <input type="number" name="phone" id="phone" class="form-control" placeholder="Phone No."/>
            </div>
            <div class="form-group col">
                <label for="address">Address</label>
                <input type="text" name="address" id="address" class="form-control" placeholder="Address"/>
            </div>
            <div class="form-group col">
                <label for="gender">Gender</label>
                <select class="form-control" id="gender" name="gender">
                  <option value="">--Select Gender--</option>
                  <option value="male">Male</option>
                  <option value="female">Female</option>
                  <option value="other">Other</option>
                </select>
            </div>
           
            <div class="form-group col">
                <label for="email">Email</label>
                <input type="email" name="email" id="email" class="form-control" placeholder="Email" />
            </div>

            <div class="form-group col">
                <label for="education">Education</label>
                <input type="text" name="education" id="education" class="form-control" placeholder="Education"/>
            </div>

            <div class="form-group col">
                <label for="experience">Experience</label>
                <input type="text" name="experience" id="experience" class="form-control" placeholder="Experience"/>
            </div>
            <div class="form-group col">
                <label for="department">Department</label>
                <select class="form-control" id="department" name="department">
                  <option value="">--Select department--</option>
                  @foreach($departments as $department)
                    <option value="{{$department->id}}">{{$department->name}}</option>
                  @endforeach
                </select>
            </div>
            <div class="form-group col">
                <label for="designation">Designation</label>
                <select class="form-control" id="designation" name="designation">
                  <option value="">--Select Designation--</option>
                  @foreach($designations as $designation)
                    <option value="{{$designation->id}}">{{$designation->name}}</option>
                  @endforeach
                </select>
            </div>
            <div class="form-group col">
                <label for="payscale">Payscale</label>
                <select class="form-control" id="payscale" name="payscale">
                  <option value="">--Select payscale--</option>
                  @foreach($payscales as $payscale)
                    <option value="{{$payscale->id}}">{{$payscale->name}}</option>
                  @endforeach
                </select>
            </div>
            <div class="form-group col">
                <label for="joining_date">Joining Date</label>
                <input type="date" name="joining_date" id="joining_date" class="form-control" placeholder="Joining Date"/>
            </div>
            <div class="form-group col">
                <label for="salary">Salary</label>
                <input type="number" name="salary" id="salary" class="form-control" placeholder="Salary"/>
            </div>
             <div class="form-group col">
                <label for="image">Image</label>
                <input type="file" name="image" id="image" class="form-control" placeholder="Image" accept="image/*" />
            </div>
          </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Add</button>
      </div>
      </form>
</div>
@endsection

@section('javascript')
<script type="text/javascript">
  $(document).ready(function(){
    $('#add-teacher-form').on('submit',function(e){
    e.preventDefault();
      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
          }
      });
       $.ajax({
          url: "{{ route('add-teacher') }}",
          dataType: 'json',
          method: 'post',
          data: new FormData(this),
          contentType: false,
          cache: false,
          processData:false,
          beforeSend: function(){
            $('.save-btn').addClass('disabled');
            $('.save-btn').attr('disabled', true);
            $('#loader_modal').modal({
              backdrop: 'static',
              keyboard: false
            });
            $('#loader_modal').modal('show');
          },
          success: function(result){
            // alert('hi');
            $('.save-btn').attr('disabled', true);
            $('.save-btn').removeAttr('disabled');
            if(result.success == true){
              toastr.success('Success!', 'Staff Added Successfully!!!',{"positionClass": "toast-bottom-right"});
              $('#add-teacher-form')[0].reset();
              $('#loader_modal').modal('hide');
              
            }
            else{
              // toastr.error('Error!', result.errormsg,{"positionClass": "toast-bottom-right"});
              $('#loader_modal').modal('hide');
              $.each(result.errors, function(key, value){
                      $('input[name="'+key+'"]').after('<span class="invalid-feedback" role="alert"><strong>'+value+'</strong>');
                     $('input[name="'+key+'"]').addClass('is-invalid');
                     $('textarea[name="'+key+'"]').after('<span class="invalid-feedback" role="alert"><strong>'+value+'</strong>');
                     $('textarea[name="'+key+'"]').addClass('is-invalid');
                     $('select[name="'+key+'"]').after('<span class="invalid-feedback" role="alert"><strong>'+value+'</strong>');
                     $('select[name="'+key+'"]').addClass('is-invalid');


                });
            }

          },
          error: function (request, status, error) {
            alert('here');
            console.log(request);
            console.log(error);
                $('#loader_modal').modal('hide');
                $('.save-btn').removeClass('disabled');
                $('.save-btn').removeAttr('disabled');
                json = $.parseJSON(request.responseText);
                $.each(json.errors, function(key, value){
                      $('input[name="'+key+'"]').after('<span class="invalid-feedback" role="alert"><strong>'+value+'</strong>');
                     $('input[name="'+key+'"]').addClass('is-invalid');
                     $('textarea[name="'+key+'"]').after('<span class="invalid-feedback" role="alert"><strong>'+value+'</strong>');
                     $('textarea[name="'+key+'"]').addClass('is-invalid');


                });
            }
        });
  });
  });
</script>
@stop