@extends('user.layouts.layout')

@section('title','Online Marketing')

@section('content')
<style type="text/css">
   
</style>

<div class="row page-title-row">
    <div class="col-md col-sm col  page-title-col align-self-center">
        <h1 class="page-title">All Staff</h1>
    </div>
    <div class="col-sm col-auto ml-auto text-right page-action-button">
        {{-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addTeacher">+Add Staff</button> --}}

        <a type="button" href="{{route('add-staff')}}" class="btn btn-primary">+Add Staff</a>
    </div>      
</div>

<div class="card card-border-top mb-4">
<div class="card-header">
      <h6 class="mb-0">Filters</h6>
  </div>
<div class="card-body">
      
  <div class="row mb-0 form-row row-cols-lg-4 row-cols-md-4 row-cols-sm-2 row-cols-2">
    <div class="col form-group">
      <label for="">Department</label>
      <select class="form-control form-control department_select state-tags" >
        <option value="">-- All Departments --</option>
        @foreach($departments as $department)
          <option value="{{$department->id}}">{{$department->name}} </option>
          @endforeach
      </select>
    </div>

    <div class="col form-group">
      <label for="">Designation</label>
      <select class="form-control form-control designation_select state-tags" >
        <option value="">-- All Designations --</option>
        @foreach($designations as $designation)
          <option value="{{$designation->id}}">{{$designation->name}} </option>
          @endforeach
      </select>
    </div>

    <div class="col form-group">
      <label for="">Status</label>
      <select class="form-control form-control teacher_status state-tags" name="teacher_status" >
        <option value="">Select Status</option>
          <option value="1" selected="true"> Active </option>
          <option value="2"> Suspended </option>
      </select>
    </div>

    <div class="col form-group">
      <label for="">Search With Name</label>
      <input type="text" class="form-control name_search" placeholder="i.e XYZ">
    </div>


    <div class="col-lg col-md col-sm col ml-auto text-right mb-3 align-self-end">
      <!-- <label for=""style="visibility: hidden;"><b>Submitted From Date :</b></label> -->
      <div class=" mt-sm-4">
        <button class="btn btn-outline-primary" type="reset" id="reset">Reset</button>
        <button class="btn btn-primary search-btn ml-md-2 ml-1" type="button" id="search_btn">Search</button>
      </div>
    </div>
  </div>
</div>
</div>



                      <div class="selected-item catalogue-btn-group d-none">
                            <div class="row">
                              <div class="form-group col-2">
                                <label for="fee_month" style="visibility: hidden;"><b>Fee Month :</b></label>
                                <button class="theme-btn-color charge_salary btn btn-primary" style="display: block;" title="Charge Salary" data-id="2"> Charge Salary</button>
                              </div>
                              <div class="form-group col-2">
                                
                                <label for="fee_month"><b>Fee Month :</b></label>
                                  <input type="date" name="fee_month" id="fee_month" class="form-control" placeholder="Search by year ..." />
                              </div>
                            </div> 
                        </div>

                      <div class="card">
                            <div class="card-body">
                            <div class="table-responsive">
                            <table class="table table-striped nowrap table-teachers">
                            <thead>
                            <tr>
                                <th>S.No</th>
                                <th class="noVis">
                                  <div class="custom-control custom-checkbox custom-checkbox1 d-inline-block">
                                    <input type="checkbox" class="custom-control-input check-all1" name="check_all" id="check-all">
                                    <label class="custom-control-label" for="check-all"></label>
                                  </div>
                                </th>
                                <th>Name</th>
                                <th>Guardian</th>
                                <th>Guardian Relation</th>
                                <th>Phone</th>
                                <th>Education</th>
                                <th>Experience</th>
                                <th>Department</th>
                                <th>Designation</th>
                                <th>Payscale</th>
                                <th>Joining Date</th>
                                <th>Leaving Date</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            </table>
                            </div>
                        </div>
                    </div>
                      <input type="hidden" name="fees_selected" class="fees_selected">


<!--Add Student Modal -->
<div class="modal fade" id="addTeacher" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add Teacher</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form id="add-teacher-form" method="POST" enctype="multipart/form-data">
        {{csrf_field()}}
      <div class="modal-body ">

          <div class="form-row row-cols-lg-3 row-cols-sm-2 row-cols-2">
            <div class="form-group col">
                <label for="name">Teacher Name</label>
                <input type="text" name="name" id="name" class="form-control" placeholder="Teacher Name"/>
            </div>
            <div class="form-group col">
                <label for="guardian">Father/Guardian Name</label>
                <input type="text" name="guardian" id="guardian" class="form-control" placeholder="Father/Guardian Name"/>
            </div>
            <div class="form-group col">
                <label for="guardian_relation">Father/Guardian Relation</label>
                <input type="text" name="guardian_relation" id="guardian_relation" class="form-control" placeholder="Relation"/>
            </div>
            <div class="form-group col">
                <label for="cnic">Cnic</label>
                <input type="text" name="cnic" id="cnic" class="form-control" placeholder="Cnic"/>
            </div>
            <div class="form-group col">
                <label for="phone">Phone No.</label>
                <input type="number" name="phone" id="phone" class="form-control" placeholder="Phone No."/>
            </div>
            <div class="form-group col">
                <label for="address">Address</label>
                <input type="text" name="address" id="address" class="form-control" placeholder="Address"/>
            </div>
            <div class="form-group col">
                <label for="gender">Gender</label>
                <select class="form-control" id="gender" name="gender">
                  <option value="">--Select Gender--</option>
                  <option value="male">Male</option>
                  <option value="female">Female</option>
                  <option value="other">Other</option>
                </select>
            </div>
           
            <div class="form-group col">
                <label for="email">Email</label>
                <input type="email" name="email" id="email" class="form-control" placeholder="Email" required="required" />
            </div>

            <div class="form-group col">
                <label for="education">Education</label>
                <input type="text" name="education" id="education" class="form-control" placeholder="Education"/>
            </div>

            <div class="form-group col">
                <label for="experience">Experience</label>
                <input type="text" name="experience" id="experience" class="form-control" placeholder="Experience"/>
            </div>
            <div class="form-group col">
                <label for="designation">Designation</label>
                <input type="text" name="designation" id="designation" class="form-control" placeholder="Designation"/>
            </div>
            <div class="form-group col">
                <label for="joining_date">Joining Date</label>
                <input type="date" name="joining_date" id="joining_date" class="form-control" placeholder="Joining Date"/>
            </div>
            <div class="form-group col">
                <label for="salary">Salary</label>
                <input type="number" name="salary" id="salary" class="form-control" placeholder="Salary"/>
            </div>
             <div class="form-group col">
                <label for="image">Image</label>
                <input type="file" name="image" id="image" class="form-control" placeholder="Image" accept="image/*" />
            </div>
          </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Add</button>
      </div>
      </form>
    </div>
  </div>
</div>
@endsection

@section('javascript')
<script type="text/javascript">
  $(document).ready(function(){
    $('.search-btn').on('click',function(){
    $('.table-teachers').DataTable().ajax.reload();
  });

    $(document).on('click', '.check-all1', function () {
        if(this.checked == true){
        $('.check').prop('checked', true);
        $('.check').parents('tr').addClass('selected');
        var cb_length = $( ".check:checked" ).length;
        if(cb_length > 0){
          $('.selected-item').removeClass('d-none');
        }
      }else{
        $('.check').prop('checked', false);
        $('.check').parents('tr').removeClass('selected');
        $('.selected-item').addClass('d-none');
        
      }
    });

   $(document).on('click', '.check', function () {
    // $(this).removeClass('d-none');
        var cb_length = $( ".check:checked" ).length;
        var st_pieces = $(this).parents('tr').attr('data-pieces');
        if(this.checked == true){
        $('.selected-item').removeClass('d-none');
        $(this).parents('tr').addClass('selected');
      }else{
        $(this).parents('tr').removeClass('selected');
        if(cb_length == 0){
         $('.selected-item').addClass('d-none');
        }
        
      }
    });

   $(document).on('click', '.download_invoice2', function(){
    var selected_fees = [];
    $("input.check:checked").each(function() {
      selected_fees.push($(this).val());
    });

    if(selected_fees == ''){
            toastr.error('Error!', 'Select Teacher(s) First.',{"positionClass": "toast-bottom-right"});
        return false;
        }else{
        $('.fees_selected').val(selected_fees);

        var fees = $('.fees_selected').val();

        var url = "{{url('user/teacher-pay-slip')}}"+"/"+fees;
                    window.open(url, 'Orders Receivable Print', 'width=1200,height=600,scrollbars=yes');
                  $('#loader_modal').modal('hide');
        }
      });

        $('#add-teacher-form').on('submit',function(e){
    e.preventDefault();

    var name = $('#name').val();
    var guardian = $('#guardian').val();
    if(name == null || name == '')
    {
      toastr.error('Sorry!', 'Teacher Name Cannot Be Null!!!',{"positionClass": "toast-bottom-right"});
      return false;
    }

    if(guardian == null || guardian == '')
    {
      toastr.error('Sorry!', 'Father/Guardian Name Cannot Be Null!!!',{"positionClass": "toast-bottom-right"});
      return false;
    }

    var cnic = $('#cnic').val();
    if(cnic == null || cnic == '')
    {
      toastr.error('Sorry!', 'Cnic Cannot Be Null!!!',{"positionClass": "toast-bottom-right"});
      return false;
    }

    var phone = $('#phone').val();
    if(phone == null || phone == '')
    {
      toastr.error('Sorry!', 'Father/Guardian Phone No. Cannot Be Null!!!',{"positionClass": "toast-bottom-right"});
      return false;
    }

    var gender = $('#gender').val();
    if(gender == null || gender == '')
    {
      toastr.error('Sorry!', 'Gender Cannot Be Null!!!',{"positionClass": "toast-bottom-right"});
      return false;
    }

    var joining_date = $('#joining_date').val();
    if(joining_date == null || joining_date == '')
    {
      toastr.error('Sorry!', 'Joining Date Cannot Be Null!!!',{"positionClass": "toast-bottom-right"});
      return false;
    }

    var address = $('#address').val();
    if(address == null || address == '')
    {
      toastr.error('Sorry!', 'Address Cannot Be Null!!!',{"positionClass": "toast-bottom-right"});
      return false;
    }

    var education = $('#education').val();
    if(education == null || education == '')
    {
      toastr.error('Sorry!', 'Education Cannot Be Null!!!',{"positionClass": "toast-bottom-right"});
      return false;
    }

    var designation = $('#designation').val();
    if(designation == null || designation == '')
    {
      toastr.error('Sorry!', 'Designation Cannot Be Null!!!',{"positionClass": "toast-bottom-right"});
      return false;
    }

    var salary = $('#salary').val();
    if(salary == null || salary == '')
    {
      toastr.error('Sorry!', 'Salary Cannot Be Null!!!',{"positionClass": "toast-bottom-right"});
      return false;
    }

    var email = $('#email').val();
    if(email == null || email == '')
    {
      toastr.error('Sorry!', 'Email Cannot Be Null!!!',{"positionClass": "toast-bottom-right"});
      return false;
    }

    var image = $('#image').val();
    if(image == null || image == '')
    {
      toastr.error('Sorry!', 'Image Cannot Be Null!!!',{"positionClass": "toast-bottom-right"});
      return false;
    }


      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
          }
      });
       $.ajax({
          url: "{{ route('add-teacher') }}",
          dataType: 'json',
          method: 'post',
          data: new FormData(this),
          contentType: false,
          cache: false,
          processData:false,
          beforeSend: function(){
            $('.save-btn').addClass('disabled');
            $('.save-btn').attr('disabled', true);
            $('#loader_modal').modal({
              backdrop: 'static',
              keyboard: false
            });
            $('#loader_modal').modal('show');
          },
          success: function(result){
            $('.save-btn').attr('disabled', true);
            $('.save-btn').removeAttr('disabled');
            if(result.success == true){
              toastr.success('Success!', 'Teacher Added Successfully!!!',{"positionClass": "toast-bottom-right"});
              $('.table-teachers').DataTable().ajax.reload();
              /*setTimeout(function(){
                window.location.reload();
              }, 2000);*/

              $('#add-teacher-form')[0].reset();
              $('#addTeacher').modal('hide');
              $('#loader_modal').modal('hide');
              
            }
            else{
              toastr.error('Error!', result.errormsg,{"positionClass": "toast-bottom-right"});
              $('#loader_modal').modal('hide');
            }

          },
          error: function (request, status, error) {
                /*$('.form-control').removeClass('is-invalid');
                $('.form-control').next().remove();*/
                $('#loader_modal').modal('hide');
                $('.save-btn').removeClass('disabled');
                $('.save-btn').removeAttr('disabled');
                json = $.parseJSON(request.responseText);
                $.each(json.errors, function(key, value){
                      $('input[name="'+key+'"]').after('<span class="invalid-feedback" role="alert"><strong>'+value+'</strong>');
                     $('input[name="'+key+'"]').addClass('is-invalid');
                     $('textarea[name="'+key+'"]').after('<span class="invalid-feedback" role="alert"><strong>'+value+'</strong>');
                     $('textarea[name="'+key+'"]').addClass('is-invalid');


                });
            }
        });
  });

  var table2 =  $('.table-teachers').DataTable({
    processing: true,
    searching: true,
    "language": {
    processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw" style="color:#13436c;"></i><span class="sr-only">Loading...</span> '},
    ordering: false,
    pageLength: {{50}},
    serverSide: true,
    autoWidth: false,
    "lengthMenu": [50,100,150,200],
    
    scrollX:true,
    scrollY : '90vh',
    scrollCollapse: true,
    ajax: 
    {
      beforeSend: function(){
        $('#loader_modal').modal({
          backdrop: 'static',
          keyboard: false
        });
        $('#loader_modal').modal('show');
      },
      url: "{!! route('get-teachers') !!}",
       data: function(data) { data.teacher_status = $('.teacher_status option:selected').val(), data.department = $('.department_select option:selected').val(), data.designation = $('.designation_select option:selected').val(), data.search = $('.name_search').val() } ,
    },
    columns: [
        { data: 'empty_col', name: 'empty_col' }, 
        { data: 'checkbox', name: 'checkbox' }, 
        { data: 'name', name: 'name' }, 
        { data: 'guardian', name: 'guardian' }, 
        { data: 'guardian_relation', name: 'guardian_relation' }, 
        { data: 'phone', name: 'phone' }, 
        { data: 'education', name: 'education' }, 
        { data: 'experience', name: 'experience' }, 
        { data: 'department', name: 'department' }, 
        { data: 'designation', name: 'designation' }, 
        { data: 'payscale', name: 'payscale' }, 
        { data: 'joining_date', name: 'joining_date' }, 
        { data: 'leaving_date', name: 'leaving_date' }, 
        { data: 'action', name: 'action' }, 
    ],
    initComplete: function () {
    // Enable THEAD scroll bars
    $('.dataTables_scrollHead').css('overflow', 'auto');
    // Sync THEAD scrolling with TBODY
      $('.dataTables_scrollHead').on('scroll', function () {
          $('.dataTables_scrollBody').scrollLeft($(this).scrollLeft());
      });
    },
    drawCallback: function(){
      $('#loader_modal').modal('hide');
    },
  });

  table2.on( 'draw.dt', function () {
    var PageInfo = $('.table-teachers').DataTable().page.info();
         table2.column(0, { page: 'current' }).nodes().each( function (cell, i) {
            cell.innerHTML = i + 1 + PageInfo.start;
        } );
  } );

    $(document).on('click','.suspend_teacher',function(){
      var id = $(this).data('id');

        swal({
            title: "Are You Sure!",
            html: true,
            text: "<b>You want to suspend this teacher !!!</b>",
            type: "info",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes!",
            cancelButtonText: "No!",
            closeOnConfirm: true,
            closeOnCancel: false
          },
            function(isConfirm) {
            if (isConfirm){
             $.ajaxSetup({
              headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
              }
            });
            $.ajax({
              url: "{{ route('suspend-teacher') }}",
              method: 'post',
              data: {id:id},
              beforeSend: function(){
                $('#loader_modal').modal({
                  backdrop: 'static',
                  keyboard: false
                });
                $('#loader_modal').modal('show');
              },
              success: function (response) {
                if(response.success == true)
                {
                  toastr.success('Success!', 'Teacher Suspended Successfully!!!',{"positionClass": "toast-bottom-right"});
                  $('.table-teachers').DataTable().ajax.reload();
                  $('#loader_modal').modal('hide');
                }
               
              },
              error: function(request, status, error){
                $('#loader_modal').modal('hide');
              }
            });
            }
            else{
              swal("Cancelled", "", "error");
              
            }
          });
  });

    $(document).on('change','.teacher_status',function(){
    $('.table-teachers').DataTable().ajax.reload();
    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
  });

    $(document).on('click','#reset',function(){
    $('.teacher_status').val('');
    $('.department_select').val('');
    $('.designation_select').val('');
    $('.name_search').val('');
    $('.table-teachers').DataTable().ajax.reload();
    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
  });

  $(document).on('click','.activate_teacher',function(){
      var id = $(this).data('id');

        swal({
            title: "Are You Sure!",
            html: true,
            text: "<b>You want to activate this teacher !!!</b>",
            type: "info",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes!",
            cancelButtonText: "No!",
            closeOnConfirm: true,
            closeOnCancel: false
          },
            function(isConfirm) {
            if (isConfirm){
             $.ajaxSetup({
              headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
              }
            });
            $.ajax({
              url: "{{ route('activate-teacher') }}",
              method: 'post',
              data: {id:id},
              beforeSend: function(){
                $('#loader_modal').modal({
                  backdrop: 'static',
                  keyboard: false
                });
                $('#loader_modal').modal('show');
              },
              success: function (response) {
                if(response.success == true)
                {
                  toastr.success('Success!', 'Teacher Activated Successfully!!!',{"positionClass": "toast-bottom-right"});
                  $('.table-teachers').DataTable().ajax.reload();
                  $('#loader_modal').modal('hide');
                }
               
              },
              error: function(request, status, error){
                $('#loader_modal').modal('hide');
              }
            });
            }
            else{
              swal("Cancelled", "", "error");
              
            }
          });
  });

  $('#image').on('change', function() {
        let size = this.files[0].size; // this is in bytes
        if (size > 1000000) {
        // do something. Prevent form submit. Show message, etc.
          toastr.error('Sorry!', 'Student Photo Size Must Be Less Than 1MB!!!',{"positionClass": "toast-bottom-right"});

        }
        else
        {
          $('.update_image').removeAttr('disabled');
        }
    });

  $(document).on('click','.delete_student_permanant',function(){
      var id = $(this).data('id');

        swal({
            title: "Are You Sure!",
            html: true,
            text: "<b>You want to delete this teacher !!!</b>",
            type: "info",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes!",
            cancelButtonText: "No!",
            closeOnConfirm: true,
            closeOnCancel: false
          },
            function(isConfirm) {
            if (isConfirm){
             $.ajaxSetup({
              headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
              }
            });
            $.ajax({
              url: "{{ route('delete-teacher-permanent') }}",
              method: 'post',
              data: {id:id},
              beforeSend: function(){
                $('#loader_modal').modal({
                  backdrop: 'static',
                  keyboard: false
                });
                $('#loader_modal').modal('show');
              },
              success: function (response) {

                if(response.success == true)
                {
                  toastr.success('Success!', 'Record Deleted Successfully!!!',{"positionClass": "toast-bottom-right"});
                  $('.table-teachers').DataTable().ajax.reload();
                  $('#loader_modal').modal('hide');
                }
                else if(response.success == false)
                {
                  toastr.info('Sorry!', 'Cannot delete teacher as it is already bond to other record(s)!!!',{"positionClass": "toast-bottom-right"});
                  $('.table-teachers').DataTable().ajax.reload();
                  $('#loader_modal').modal('hide');
                }
                else if(response.success == false)
                {
                  toastr.error('Error!', 'Record does not exist!!!',{"positionClass": "toast-bottom-right"});
                  $('.table-teachers').DataTable().ajax.reload();
                  $('#loader_modal').modal('hide');
                }
               
              },
              error: function(request, status, error){
                $('#loader_modal').modal('hide');
              }
            });
            }
            else{
              swal("Cancelled", "", "error");
              
            }
          });
  });

  $('.charge_salary').on('click',function(e){
    if($('#fee_month').val() == ''){
      toastr.info('Sorry!', 'Please select salary month!!!',{"positionClass": "toast-bottom-right"});
      return false;
    }
    var selected_fees = [];
    $("input.check:checked").each(function() {
      selected_fees.push($(this).val());
    });

    if(selected_fees == ''){
            toastr.error('Error!', 'Select Teacher(s) First.',{"positionClass": "toast-bottom-right"});
        return false;
        }else{
        $('.fees_selected').val(selected_fees);
        var ids = $('.fees_selected').val();
        }
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
      }
    });
    $.ajax({
      url: "{{ route('charge-teachers-salary') }}",
      method: 'get',
      data: {salary_month: $('#fee_month').val(),ids: ids},
      beforeSend: function(){
        $('#loader_modal').modal({
          backdrop: 'static',
          keyboard: false
        });
        $('#loader_modal').modal('show');
      },
      success: function (response) {

        if(response.success == true)
        {
          toastr.success('Success!', 'Operation done Successfully!!!',{"positionClass": "toast-bottom-right"});
          $('.table-teachers').DataTable().ajax.reload();
          $('#loader_modal').modal('hide');
        }
        else if(response.success == false)
        {
          toastr.info('Sorry!', 'something went wrong!!!',{"positionClass": "toast-bottom-right"});
          $('.table-teachers').DataTable().ajax.reload();
          $('#loader_modal').modal('hide');
        }
       
      },
      error: function(request, status, error){
        $('#loader_modal').modal('hide');
      }
    });
  })
});
</script>
@stop

