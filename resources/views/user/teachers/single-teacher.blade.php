@extends('user.layouts.layout')

@section('title','Single Student')

@section('content')
<style type="text/css">
   .student-image
   {
    padding-top: 100%;
    background-position: center;
    background-size: cover;
   }

   .student_image
   {
    box-shadow: 0px 0px 15px 0px rgba(0,0,0,0.5);

   }
   h1,h3,h2,h4,h5,h6
   {
    color: black;
   }
   .teacher__img:hover{
    opacity: 0.5;
    cursor: pointer;
   }
</style>
<?php 
  use Carbon\Carbon;
?>

    <div>
      
      <div class="card mb-4">
        <div class="card-body">
          <div class="row mx-0 py-md-4 py-3 px-md-2 profile-header rounded-lg align-items-center mb-4">
            <div class="col-sm col-12 pb-1 profile-image-col mx-auto">
              <div class="overflow-hidden profile-image-border rounded-circle">
                @if(@$teacher->image != null && file_exists(public_path().'/uploads/teachers/images/'.$teacher->image))
              <img src="{{asset('public/uploads/teachers/images/'.$teacher->image)}}" class="teacher__img profileimg img-fluid">
              @else
              <img src="{{asset('public/assets/img/user-avatar.png')}}" class="teacher__img profileimg img-fluid">
    
              @endif
              <div style="display: none;">
                <form enctype="multipart/form-data" method="post" id="upload_form">
                  <input type="hidden" name="teacher_id" value="{{@$teacher->id}}">
              <input type="file" name="profileimg" onchange="validateImage()" id="profile" style="">
               <button id="submitbutton" type="submit">Upload</button>
              </form>
              </div>
              </div>
            </div>
            <div class="col-sm col-12 profile-name-col">
                  <span class="h5 mb-0 inputDoubleClick student-name" id="name"  data-fieldvalue="{{$teacher->name}}">
                    {{$teacher->name != null ? $teacher->name : '--'}} 
                  </span>
                  <input type="text" autocomplete="nope" name="name" class="fieldFocus d-none form-control" value="{{(@$teacher->name!=null)?$teacher->name:''}}">
            </div>   
          </div>
          <div class="row row-cols-xl-4 row-cols-md-4 row-cols-2">
            <div class="col mb-3 student-info-col px-sm-3 px-2">
              <h6 class="student-info-title">
                Guardian : </h6><p class="mb-0">
                <span class="m-l-15 inputDoubleClick" id="guardian"  data-fieldvalue="{{$teacher->guardian}}">
                {{$teacher->guardian != null ? $teacher->guardian : '--'}}
              </span>
            <input type="text" autocomplete="nope" name="guardian" class="fieldFocus d-none form-control" value="{{(@$teacher->guardian!=null)?$teacher->guardian:''}}">
              </p>
            </div>
            <div class="col mb-3 student-info-col px-sm-3 px-2">
              <h6 class="student-info-title">
                 Guardian Relation : </h6><p class="mb-0">
                <span class="m-l-15 inputDoubleClick" id="guardian_relation"  data-fieldvalue="{{$teacher->guardian_relation}}">
                {{$teacher->guardian_relation != null ? $teacher->guardian_relation : '--'}}
              </span>
            <input type="text" autocomplete="nope" name="guardian_relation" class="fieldFocus d-none form-control" value="{{(@$teacher->guardian_relation!=null)?$teacher->guardian_relation:''}}">
              </p>
            </div>
            <div class="col mb-3 student-info-col px-sm-3 px-2">
              <h6 class="student-info-title">
                Education : 
                {{-- {{$teacher->education}}  --}}</h6><p class="mb-0">
                <span class="m-l-15 inputDoubleClick" id="education"  data-fieldvalue="{{$teacher->education}}">
                {{$teacher->education != null ? $teacher->education : '--'}}
              </span>
            <input type="text" autocomplete="nope" name="education" class="fieldFocus d-none form-control" value="{{(@$teacher->education!=null)?$teacher->education:''}}">
              </p>
            </div>
            <div class="col mb-3 student-info-col px-sm-3 px-2">
              <h6 class="student-info-title">
                Experience : 
                {{-- {{$teacher->education}}  --}}</h6><p class="mb-0">
                <span class="m-l-15 inputDoubleClick" id="experience"  data-fieldvalue="{{$teacher->experience}}">
                {{$teacher->experience != null ? $teacher->experience : '--'}}
              </span>
            <input type="text" autocomplete="nope" name="experience" class="fieldFocus d-none form-control" value="{{(@$teacher->experience!=null)?$teacher->experience:''}}">
              </p>
            </div>
            <div class="col mb-3 student-info-col px-sm-3 px-2">
              <h6 class="student-info-title">
                Designation : 
                {{-- {{$teacher->education}}  --}}</h6><p class="mb-0">
                <span class="m-l-15 inputDoubleClick" id="education"  data-fieldvalue="{{@$teacher->designation->name}}">
                {{$teacher->designation != null ? $teacher->designation->name : '--'}}
              </span>
            {{-- <input type="text" autocomplete="nope" name="designation" class="fieldFocus d-none form-control" value="{{(@$teacher->designation!=null)?$teacher->designation:''}}"> --}}
              </p>
            </div>

            <div class="col mb-3 student-info-col px-sm-3 px-2">
              <h6 class="student-info-title">
                Gender : {{$teacher->gender == 'male' ? 'Male' : ($teacher->gender == 'female' ? 'Female' : 'Other')}}
              </h6>
            </div>
             <div class="col mb-3 student-info-col px-sm-3 px-2">
              <h6 class="student-info-title">
                Email : 
                {{-- {{$teacher->email != null ? $teacher->email : '--'}} --}}</h6><p class="mb-0">
                <span class="m-l-15 inputDoubleClick" id="email"  data-fieldvalue="{{$teacher->email}}">
                {{$teacher->email != null ? $teacher->email : '--'}}
              </span>
            <input type="email" autocomplete="nope" name="email" class="fieldFocus d-none form-control" value="{{(@$teacher->email!=null)?$teacher->email:''}}">
              </p>
            </div>

            <div class="col mb-3 student-info-col px-sm-3 px-2">
              <h6 class="student-info-title">
                Address : 
                {{-- {{$teacher->address}} --}}</h6><p class="mb-0">
                <span class="m-l-15 inputDoubleClick" id="address"  data-fieldvalue="{{$teacher->address}}">
                {{$teacher->address != null ? $teacher->address : '--'}}
              </span>
            <input type="text" autocomplete="nope" name="address" class="fieldFocus d-none form-control" value="{{(@$teacher->address!=null)?$teacher->address:''}}">
              </p>
            </div>
            <div class="col mb-3 student-info-col px-sm-3 px-2">
              <h6 class="student-info-title">
                Phone : 
                {{-- {{$teacher->phone}} --}}</h6><p class="mb-0">
                <span class="m-l-15 inputDoubleClick" id="phone"  data-fieldvalue="{{$teacher->phone}}">
                {{$teacher->phone != null ? $teacher->phone : '--'}}
              </span>
            <input type="number" autocomplete="nope" name="phone" class="fieldFocus d-none form-control" value="{{(@$teacher->phone!=null)?$teacher->phone:''}}">
              </p>
            </div>

            <div class="col mb-3 student-info-col px-sm-3 px-2">
              <h6 class="student-info-title">
                Salary : 
                {{-- {{number_format($teacher->salary,2,'.',',')}} --}}</h6><p class="mb-0">
                <span class="m-l-15 inputDoubleClick" id="salary"  data-fieldvalue="{{$teacher->salary}}">
                {{$teacher->salary != null ? number_format($teacher->salary,2,'.',',') : '--'}}
              </span>
            <input type="number" autocomplete="nope" name="salary" class="fieldFocus d-none form-control" value="{{(@$teacher->salary!=null)?$teacher->salary:''}}">
              </p>
            </div>

            <div class="col mb-3 student-info-col px-sm-3 px-2">
              <h6 class="student-info-title">
                Status : 
                {{-- {{number_format($teacher->salary,2,'.',',')}} --}}</h6><p class="mb-0">
                <span class="m-l-15" id="status"  data-fieldvalue="{{$teacher->status}}">
                                    @if($teacher->status == 1)
                                    <span>Active</span>
                                    @else
                                    <span style="color: red;font-style: italic;"><b>Suspended</b></span>
                                    @endif
                                  </span>
              </p>
            </div>
            <div class="col mb-3 student-info-col px-sm-3 px-2">
              <h6 class="student-info-title">
                Joining Date : 
                {{-- {{number_format($teacher->salary,2,'.',',')}} --}}</h6><p class="mb-0">
                <span class="m-l-15 inputDoubleClick" id="joining_date"  data-fieldvalue="{{$teacher->joining_date}}">
                {{$teacher->joining_date != null ? $teacher->joining_date : '--'}}
              </span>
            <input type="date" autocomplete="nope" name="joining_date" class="fieldFocus d-none form-control" value="{{(@$teacher->joining_date!=null)?$teacher->joining_date:''}}">
              </p>
            </div> 
            <div class="col mb-3 student-info-col px-sm-3 px-2">
              <h6 class="student-info-title">
                Leaving Date : 
                {{-- {{number_format($teacher->salary,2,'.',',')}} --}}</h6><p class="mb-0">
                <span class="m-l-15 inputDoubleClick" id="leaving_date"  data-fieldvalue="{{$teacher->leaving_date}}">
                {{$teacher->leaving_date != null ? $teacher->leaving_date : '--'}}
              </span>
            <input type="date" autocomplete="nope" name="leaving_date" class="fieldFocus d-none form-control" value="{{(@$teacher->leaving_date!=null)?$teacher->leaving_date:''}}">
            <i class="zmdi zmdi-delete clear_date" title="Clear Date" data-attr="leaving_date"></i>
              </p>
            </div>
          </div>
        </div>
      </div>

      <div class="pt-3 mb-4">
          <h1 class="page-title">Teacher Salary History</h1>                        
      </div>

      
          <div class="card">
          <div class="card-body">
                <div class="selected-item catalogue-btn-group d-none">
                  <a href="javascript:void(0)" class="btn btn-outline-primary download_invoice" data-id="1" title="Download Invoice">Download Invoice</a>
                </div>
                <div class="row align-items-end form-row">
                  <div class="col-lg-3 col-md-4 col-md-5 col form-group">
                        <input type="number" name="year" id="year" class="form-control" value="{{carbon::now()->format('Y')}}" placeholder="Search by year ..." />
                    </div>
                    <div class="col-auto form-group">
                        <button class="btn btn-primary px-4" type="button" id="apply_year">Apply</button>  

                    </div>
                </div>


          <table class="table table-striped nowrap table-teachers-salaries">
          <thead>
          <tr>
            <th class="noVis">
              <div class="custom-control custom-checkbox custom-checkbox1 d-inline-block">
                <input type="checkbox" class="custom-control-input check-all1" name="check_all" id="check-all">
                <label class="custom-control-label" for="check-all"></label>
              </div>
            </th>
            <th>Name</th>
            <th>Phone</th>
            <th>Salary</th>
            <th>Deduction</th>
            <th>Deduction Reason</th>
            <th>Bonus (%)</th>
            <th>Bonus Amount</th>
            <th>Bonus Reason</th>
            <th>Total Payable Amount</th>
            <th>Paid Amount</th>
            <th>Unpaid Amount</th>
            <th>Salary Month</th>
            <th>Submitted Date</th>
            <th>Remarks</th>
            <th>Status</th>
            <th>Action</th>
          </tr>
          </thead>
          </table>
          </div>
        </div>
          <input type="hidden" name="salary_selected" class="salary_selected">


@endsection

@section('javascript')
<script type="text/javascript">
  $('.profileimg').on('click',function(){
      $("#profile").click();
    });
    
    $('#profile').on('change',function(){
      $('#submitbutton').trigger('click');
    });
    $("#upload_form").on('submit',function(e){
      e.preventDefault();
       $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
    });
     $.ajax({
        url: "{{ route('update-profile-img-teacher') }}",
        method: 'post',
        data: new FormData(this), 
        contentType: false,       
        cache: false,             
        processData:false,
        beforeSend: function(){
          $('#loader_modal').modal('show');
        },
        success: function(result){
        
          if(result.success === true){
            
            toastr.success('Success!', 'Profile image updated successfully',{"positionClass": "toast-bottom-right"});
            $('#upload_form')[0].reset();
            setTimeout(function(){
          $('#loader_modal').modal('hide');

              window.location.reload();
            }, 2000);
            
          }
          
          
        },
        error: function (request, status, error) {
              $('.save-btn').val('add');
              $('.save-btn').removeClass('disabled');
              $('.save-btn').removeAttr('disabled');
              $('.form-control').removeClass('is-invalid');
              $('.form-control').next().remove();
              json = $.parseJSON(request.responseText);
              $.each(json.errors, function(key, value){
                  $('input[name="'+key+'"]').after('<span class="invalid-feedback" role="alert"><strong>'+value+'</strong>');
                   $('input[name="'+key+'"]').addClass('is-invalid');
              });
          }
      });
    });

    function validateImage() {
  console.log("validateImage called");
  var formData = new FormData();

  var file = document.getElementById("profile").files[0];

  formData.append("Filedata", file);
  var t = file.type.split('/').pop().toLowerCase();
  if (t != "jpeg" && t != "jpg" && t != "png" && t != "bmp" && t != "svg") {
    toastr.error('Uploaded file image must be jpeg, jpg, bmp, png or svg.','Error!' , {
                                "positionClass": "toast-bottom-right"
                            });
    document.getElementById("profile").value = '';
    return false;
  }
  if (file.size > 2048000) {
    toastr.error('Image size must be less than 2MB.','Error!' , {
                                "positionClass": "toast-bottom-right"
                            });
    document.getElementById("profile").value = '';
    return false;
  }
  return true;
}

    var table2 =  $('.table-teachers-salaries').DataTable({
    processing: true,
    searching: true,
    "language": {
    processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw" style="color:#13436c;"></i><span class="sr-only">Loading...</span> '},
    ordering: false,
    pageLength: {{50}},
    serverSide: true,
    autoWidth: false,
    "lengthMenu": [50,100,150,200],
    
    scrollX:true,
    scrollY : '90vh',
    scrollCollapse: true,
    dom: 'Bfrtip',
        buttons: [
            {
                extend: 'excelHtml5',
                exportOptions: {
                    columns: [2,3,4,5,6,7,8,9,10,11,12,13,14,15,16]
                }
            },
        ],
    ajax: 
    {
      beforeSend: function(){
        $('#loader_modal').modal({
          backdrop: 'static',
          keyboard: false
        });
        $('#loader_modal').modal('show');
      },
      url: "{!! route('get-teachers-salaries') !!}",
       data: function(data) {data.teacher_id = '{{$teacher->id}}', data.year = $('#year').val(),data.page = 'single-teacher'} ,
    },
    columns: [
        { data: 'checkbox', name: 'checkbox' }, 
        { data: 'name', name: 'name' }, 
        { data: 'phone', name: 'phone' }, 
        { data: 'salary', name: 'salary' }, 
        { data: 'deduction', name: 'deduction' }, 
        { data: 'reason', name: 'reason' }, 
        { data: 'bonus', name: 'bonus' }, 
        { data: 'bonus_amount', name: 'bonus_amount' }, 
        { data: 'bonus_description', name: 'bonus_description' }, 
        { data: 'total_payable_amount', name: 'total_payable_amount' }, 
        { data: 'paid_amount', name: 'paid_amount' }, 
        { data: 'unpaid_amount', name: 'unpaid_amount' }, 
        { data: 'salary_month', name: 'salary_month' }, 
        { data: 'submitted_date', name: 'submitted_date' }, 
        { data: 'remarks', name: 'remarks' }, 
        { data: 'status', name: 'status' },
        { data: 'action', name: 'action' }, 
    ],
    initComplete: function () {
    // Enable THEAD scroll bars
    $('.dataTables_scrollHead').css('overflow', 'auto');
    // Sync THEAD scrolling with TBODY
      $('.dataTables_scrollHead').on('scroll', function () {
          $('.dataTables_scrollBody').scrollLeft($(this).scrollLeft());
      });
    },
    drawCallback: function(){
      $('#loader_modal').modal('hide');
    },
  });

      $(document).on('click','#apply_year',function(){
    $('.table-teachers-salaries').DataTable().ajax.reload();
    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
  });

          $(document).on('click', '.check-all1', function () {
        if(this.checked == true){
        $('.check').prop('checked', true);
        $('.check').parents('tr').addClass('selected');
        var cb_length = $( ".check:checked" ).length;
        if(cb_length > 0){
          $('.selected-item').removeClass('d-none');
        }
      }else{
        $('.check').prop('checked', false);
        $('.check').parents('tr').removeClass('selected');
        $('.selected-item').addClass('d-none');
        
      }
    });

   $(document).on('click', '.check', function () {
    // $(this).removeClass('d-none');
        var cb_length = $( ".check:checked" ).length;
        var st_pieces = $(this).parents('tr').attr('data-pieces');
        if(this.checked == true){
        $('.selected-item').removeClass('d-none');
        $(this).parents('tr').addClass('selected');
      }else{
        $(this).parents('tr').removeClass('selected');
        if(cb_length == 0){
         $('.selected-item').addClass('d-none');
        }
        
      }
    });

    $(document).on('click', '.download_invoice', function(){
    var selected_fees = [];
    $("input.check:checked").each(function() {
      selected_fees.push($(this).val());
    });

    if(selected_fees == ''){
            toastr.error('Error!', 'Select Salary First.',{"positionClass": "toast-bottom-right"});
        return false;
        }else{
        $('.salary_selected').val(selected_fees);

        var selected_salary = $('.salary_selected').val();

        $.ajaxSetup({
              headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
              }
            });
            $.ajax({
              url: "{{ route('check-same-teacher') }}",
              method: 'post',
              data: {selected_salary:selected_salary},
              beforeSend: function(){
                $('#loader_modal').modal({
                  backdrop: 'static',
                  keyboard: false
                });
                $('#loader_modal').modal('show');
              },
              success: function (response) {
                if(response.teacher_salary > 1)
                {
                  toastr.info('Sorry!', 'Please select same teacher salaries record!!!',{"positionClass": "toast-bottom-right"});
                  $('#loader_modal').modal('hide');
                }
                else
                {
                   var url = "{{url('user/salary-invoice-print')}}"+"/"+selected_salary;
                    window.open(url, 'Orders Receivable Print', 'width=1200,height=600,scrollbars=yes');
                  $('#loader_modal').modal('hide');
                    
                }
               
              },
              error: function(request, status, error){
                // $('#loader_modal').modal('hide');
              }
            });
        // console.log(orders);
        // return false;
         // $('.export-account-received-form')[0].submit();
         // var url = "{{url('user/fee-invoice-print')}}"+"/"+fees;
          // window.open(url, 'Orders Receivable Print', 'width=1200,height=600,scrollbars=yes');
       }

    });

    $(document).on("dblclick",".inputDoubleClick",function(){
    $(this).addClass('d-none');
    $(this).next().removeClass('d-none');
    $(this).next().addClass('active');
    $(this).next().focus();
  });

    $(document).on('keypress keyup focusout',".fieldFocus",function(e) {
    // alert('hi');
    if (e.keyCode === 27 && $(this).hasClass('active')) {
      var fieldvalue = $(this).prev().data('fieldvalue');
      var thisPointer = $(this);
      thisPointer.addClass('d-none');
      thisPointer.val(fieldvalue);
      thisPointer.removeClass('active');
      thisPointer.prev().removeClass('d-none');
    }

    var name = $(this).attr('name');
    // alert(name);

    if( (e.keyCode === 13 || e.which === 0) && $(this).val().length > 0 && $(this).hasClass('active')){
      var str = $(this).val();
      if(($(this).val().length < 2 ||  !str.replace(/\s/g, '').length))
      {
        swal({ html:true, title:'Alert !!!', text:'<b>Please Enter At Least 2 Characters (this string may only contain white spaces)!!!</b>'});
        return false;
      }
      else
      {
        $(this).removeClass('active');
        var fieldvalue = $(this).prev().data('fieldvalue');
        var new_value = $(this).val();
        // alert(new_value);
        if(fieldvalue == new_value)
        {
          var thisPointer = $(this);
          thisPointer.addClass('d-none');
          thisPointer.prev().removeClass('d-none');
          $(this).prev().html(fieldvalue);
        }
        else
        {
          var thisPointer = $(this);
          thisPointer.addClass('d-none');
          thisPointer.prev().removeClass('d-none');
          if(new_value != '')
          {
            $(this).prev().html(new_value);
          }
          $(this).prev().data('fieldvalue', new_value);
          saveData(thisPointer,thisPointer.attr('name'), thisPointer.val(),fieldvalue);
        }
      }
    }

  });

    $(document).on('click','.clear_date',function(e){
      // alert($(this).data('attr'));
      if($(this).data('attr') == 'leaving_date'){
        $('#leaving_date').data('fieldvalue','');
        $('#leaving_date').html('--');
      }
      saveData($(this),$(this).data('attr'), null,null);
    })

      function saveData(thisPointer,field_name,field_value,new_select_value){
      var id = "{{$teacher->id}}";
      
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
      });
      $.ajax({
        method: "post",

        url: "{{ url('user/update-teacher-data') }}",
        dataType: 'json',
        data: 'id='+id+'&'+field_name+'='+encodeURIComponent(field_value)+'&'+'new_select_value'+'='+encodeURIComponent(new_select_value),
        beforeSend: function(){
          $('#loader_modal').modal({
            backdrop: 'static',
            keyboard: false
          });
          $('#loader_modal').modal('show');
        },
        success: function(data)
        {
          $("#loader_modal").modal('hide');
          if(data.success == true)
          {
            
            toastr.success('Success!', 'Information updated successfully.',{"positionClass": "toast-bottom-right"});
            return true;
          }

        },

      });
    }
</script>
@stop

