@extends('user.layouts.layout')

@section('title','Teacher Salaries')

@section('content')
<style type="text/css">
   
</style>


                      <div class="row page-title-row">
                          <div class="col-md col-sm col page-title-col align-self-center">
                              <h1 class="page-title">Teacher Salaries</h1>
                          </div>
                          <div class="col-sm col-auto ml-auto text-right page-action-button">
                              <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addTeacherSalary">+ Add Salary</button>
                          </div>
                      </div>

                        <div class="card card-border-top mb-4">
                          <div class="card-header">
                            <h6 class="mb-0">Filter</h6>
                          </div>
                          <div class="card-body">
                              <div class="row mb-0 form-row row-cols-xl-6 row-cols-lg-4 row-cols-md-3 row-cols-sm-3 row-cols-2">
                                <div class="form-group col">
                                  <label for="from_date">From Salary Month</label>
                                    <input type="date" name="from_date" id="from_date" class="form-control" placeholder="Search by year ..." />
                                </div>

                                <div class="form-group col">
                                  <label for="to_date">To Salary Month</label>
                                    <input type="date" name="to_date" id="to_date" class="form-control" placeholder="Search by year ..." />
                                </div>

                                <div class="form-group col">
                                  <label for="from_payment_date">From Submitted Date</label>
                                    <input type="date" name="from_payment_date" id="from_payment_date" class="form-control" placeholder="Search by year ..." />
                                </div>
                                <div class="form-group col">
                                  <label for="to_payment_date">To Submitted Date</label>
                                    <input type="date" name="to_payment_date" id="to_payment_date" class="form-control" placeholder="Search by year ..." />
                                </div>
                                <div class="form-group col">
                                  <label for="status">Status</label>
                                    <select class="status form-control">
                                      <option value="">-- Select Status --</option>
                                      <option value="0"> Unpaid</option>
                                      <option value="1"> Paid</option>
                                    </select>
                                </div>
                                <div class="form-group col align-self-end">
                                  <input type="button" class="btn btn-primary px-4" name="reset" id="reset" value="Reset" />
                                </div>
                            </div>
                          </div>
                        </div>

                      <div class="card">
                        <div class="card-body">
                          <div class="selected-item catalogue-btn-group mt-4 mt-sm-3 ml-3 d-none">
                            <button class="btn btn-outline-primary download_invoice2" title="Pay Slips" data-id="2">Generate Pay Slip</button>
                        </div>
                        <table class="table table-striped nowrap table-teachers-salaries">
                        <thead>
                        <tr>
                          <th class="noVis">
                              <div class="custom-control custom-checkbox custom-checkbox1 d-inline-block">
                                <input type="checkbox" class="custom-control-input check-all1" name="check_all" id="check-all">
                                <label class="custom-control-label" for="check-all"></label>
                              </div>
                            </th>
                            <th>Name</th>
                            <th>Phone</th>
                            <th>Salary</th>
                            <th>Deduction</th>
                            <th>Deduction Reason</th>
                            <th>B.A.D.E Amount</th>
                            <th>B.A.D.E Reason</th>
                            <th>Total Payable Amount</th>
                            <th>Paid Amount</th>
                            <th>Unpaid Amount</th>
                            <th>Salary Month</th>
                            <th>Submitted Date</th>
                            <th>Remarks</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tfoot>
                          <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                          </tr>
                        </tfoot>
                        </table>
                        </div>
                    </div>


<!--Add Student Modal -->
<div class="modal fade" id="addTeacherSalary" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg " role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add Teacher Salary</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form id="add-teacher-salary-form" method="POST" enctype="multipart/form-data">
      <div class="modal-body pb-md-2 pb-0">

          <div class="form-row row-cols-lg-3 row-cols-md-3 row-cols-sm-3 row-cols-2">
              <div class="form-group col">
                  <label for="name">Teacher Name</label>
                  <select class="form-control w-100" id="name" name="name" style="width:100%">
                    <option value="">Select Teacher</option>
                    @foreach($teachers as $tec)
                    <option value="{{$tec->id}}"> {{$tec->name}} </option>
                    @endforeach
                  </select>
              </div>
              <div class="form-group col">
                  <label for="phone">Phone</label>
                  <input type="text" name="phone" id="phone" class="form-control" placeholder="Teacher Phone" disabled="true" />
              </div>
              <div class="form-group col">
                  <label for="guardian">Father Name</label>
                  <input type="text" name="guardian" id="guardian" class="form-control" placeholder="Father Name" disabled="true" />
              </div>
              <div class="form-group col">
                  <label for="salary">Salary</label>
                  <input type="text" name="salary" id="salary" class="form-control" placeholder="Total Salary" disabled="true" />
              </div>
              <div class="form-group col">
                  <label for="paid_amount">Enter Amount To Pay</label>
                  <input type="text" name="paid_amount" id="paid_amount" class="form-control" placeholder="0.00" required="required" />
              </div>
              <div class="form-group col">
                  <label for="remaining_amount">Remaining Amount</label>
                  <input type="text" name="remaining_amount" id="remaining_amount" class="form-control" placeholder="0.00" disabled="true" />
              </div>
              <div class="form-group col">
                  <label for="deduction">Deduction</label>
                  <input type="number" name="deduction" id="deduction" class="form-control" placeholder="0.00" />
              </div>
              <div class="form-group col">
                  <label for="reason">Deduction Reason</label>
                  <input type="text" name="reason" id="reason" class="form-control" placeholder="0.00"/>
              </div>
              <div class="form-group col">
                  <label for="bonus_amount">Bonus Amount</label>
                  <input type="text" name="bonus_amount" id="bonus_amount" class="form-control" placeholder="0.00" />
              </div>
              <div class="form-group col">
                  <label for="bonus_description">Bonus Description</label>
                  <input type="text" name="bonus_description" id="bonus_description" class="form-control" placeholder="Enter Bonus Description..." />
              </div>
              <div class="form-group col">
                  <label for="salary_month">Salary Month</label>
                  <input type="date" name="salary_month" id="salary_month" class="form-control" required="required" />
              </div>
              <div class="form-group col">
                  <label for="submitted_date">Submitted Date</label>
                  <input type="date" name="submitted_date" id="submitted_date" class="form-control" required="required" />
              </div>
              <div class="form-group col">
                  <label for="remark">Remarks</label>
                  <input type="text" name="remark" id="remark" class="form-control" placeholder="Enter Remarks ..." />
              </div>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
      </form>
    </div>
  </div>

  <input type="hidden" name="fees_selected" class="fees_selected">
</div>
@endsection

@section('javascript')
<script type="text/javascript">
  $(document).ready(function(){
    $(document).on('click', '.check-all1', function () {
        if(this.checked == true){
        $('.check').prop('checked', true);
        $('.check').parents('tr').addClass('selected');
        var cb_length = $( ".check:checked" ).length;
        if(cb_length > 0){
          $('.selected-item').removeClass('d-none');
        }
      }else{
        $('.check').prop('checked', false);
        $('.check').parents('tr').removeClass('selected');
        $('.selected-item').addClass('d-none');
        
      }
    });

   $(document).on('click', '.check', function () {
    // $(this).removeClass('d-none');
        var cb_length = $( ".check:checked" ).length;
        var st_pieces = $(this).parents('tr').attr('data-pieces');
        if(this.checked == true){
        $('.selected-item').removeClass('d-none');
        $(this).parents('tr').addClass('selected');
      }else{
        $(this).parents('tr').removeClass('selected');
        if(cb_length == 0){
         $('.selected-item').addClass('d-none');
        }
        
      }
    });

   $(document).on('click', '.download_invoice2', function(){
    var selected_fees = [];
    $("input.check:checked").each(function() {
      selected_fees.push($(this).val());
    });

    if(selected_fees == ''){
            toastr.error('Error!', 'Select Teacher(s) First.',{"positionClass": "toast-bottom-right"});
        return false;
        }else{
        $('.fees_selected').val(selected_fees);

        var fees = $('.fees_selected').val();

        var url = "{{url('user/teacher-pay-slip')}}"+"/"+fees;
                    window.open(url, 'Orders Receivable Print', 'width=1200,height=600,scrollbars=yes');
                  $('#loader_modal').modal('hide');
        }
      });
   
      $('#name').select2();
        $('#add-teacher-form').on('submit',function(e){
    e.preventDefault();

    var name = $('#name').val();
    var guardian = $('#guardian').val();
    if(name == null || name == '')
    {
      toastr.error('Sorry!', 'Teacher Name Cannot Be Null!!!',{"positionClass": "toast-bottom-right"});
      return false;
    }

    if(guardian == null || guardian == '')
    {
      toastr.error('Sorry!', 'Father/Guardian Name Cannot Be Null!!!',{"positionClass": "toast-bottom-right"});
      return false;
    }

    var cnic = $('#cnic').val();
    if(cnic == null || cnic == '')
    {
      toastr.error('Sorry!', 'Cnic Cannot Be Null!!!',{"positionClass": "toast-bottom-right"});
      return false;
    }

    var phone = $('#phone').val();
    if(phone == null || phone == '')
    {
      toastr.error('Sorry!', 'Father/Guardian Phone No. Cannot Be Null!!!',{"positionClass": "toast-bottom-right"});
      return false;
    }

    var gender = $('#gender').val();
    if(gender == null || gender == '')
    {
      toastr.error('Sorry!', 'Gender Cannot Be Null!!!',{"positionClass": "toast-bottom-right"});
      return false;
    }

    var joining_date = $('#joining_date').val();
    if(joining_date == null || joining_date == '')
    {
      toastr.error('Sorry!', 'Joining Date Cannot Be Null!!!',{"positionClass": "toast-bottom-right"});
      return false;
    }

    var address = $('#address').val();
    if(address == null || address == '')
    {
      toastr.error('Sorry!', 'Address Cannot Be Null!!!',{"positionClass": "toast-bottom-right"});
      return false;
    }

    var education = $('#education').val();
    if(education == null || education == '')
    {
      toastr.error('Sorry!', 'Education Cannot Be Null!!!',{"positionClass": "toast-bottom-right"});
      return false;
    }

    var salary = $('#salary').val();
    if(salary == null || salary == '')
    {
      toastr.error('Sorry!', 'Salary Cannot Be Null!!!',{"positionClass": "toast-bottom-right"});
      return false;
    }

    var email = $('#email').val();
    if(email == null || email == '')
    {
      toastr.error('Sorry!', 'Email Cannot Be Null!!!',{"positionClass": "toast-bottom-right"});
      return false;
    }

    var image = $('#image').val();
    if(image == null || image == '')
    {
      toastr.error('Sorry!', 'Image Cannot Be Null!!!',{"positionClass": "toast-bottom-right"});
      return false;
    }


      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
          }
      });
       $.ajax({
          url: "{{ route('add-teacher') }}",
          dataType: 'json',
          type: 'post',
          data: new FormData(this),
          contentType: false,
          cache: false,
          processData:false,
          beforeSend: function(){
            $('.save-btn').addClass('disabled');
            $('.save-btn').attr('disabled', true);
            $('#loader_modal').modal({
              backdrop: 'static',
              keyboard: false
            });
            $('#loader_modal').modal('show');
          },
          success: function(result){
            $('.save-btn').attr('disabled', true);
            $('.save-btn').removeAttr('disabled');
            if(result.success == true){
              toastr.success('Success!', 'Teacher Added Successfully!!!',{"positionClass": "toast-bottom-right"});
              $('.table-students').DataTable().ajax.reload();
              /*setTimeout(function(){
                window.location.reload();
              }, 2000);*/

              $('#add-teacher-form')[0].reset();
              $('#addTeacher').modal('hide');
              $('#loader_modal').modal('hide');
              
            }
            else{
              toastr.error('Error!', result.errormsg,{"positionClass": "toast-bottom-right"});
              $('#loader_modal').modal('hide');
            }

          },
          error: function (request, status, error) {
                /*$('.form-control').removeClass('is-invalid');
                $('.form-control').next().remove();*/
                $('#loader_modal').modal('hide');
                $('.save-btn').removeClass('disabled');
                $('.save-btn').removeAttr('disabled');
                json = $.parseJSON(request.responseText);
                $.each(json.errors, function(key, value){
                      $('input[name="'+key+'"]').after('<span class="invalid-feedback" role="alert"><strong>'+value+'</strong>');
                     $('input[name="'+key+'"]').addClass('is-invalid');
                     $('textarea[name="'+key+'"]').after('<span class="invalid-feedback" role="alert"><strong>'+value+'</strong>');
                     $('textarea[name="'+key+'"]').addClass('is-invalid');


                });
            }
        });
  });

  var table2 =  $('.table-teachers-salaries').DataTable({
    processing: true,
    searching: true,
    "language": {
    processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw" style="color:#13436c;"></i><span class="sr-only">Loading...</span> '},
    ordering: false,
    pageLength: {{50}},
    serverSide: true,
    autoWidth: false,
    "lengthMenu": [50,100,150,200],
    
    scrollX:true,
    scrollY : '90vh',
    scrollCollapse: true,
    ajax: 
    {
      beforeSend: function(){
        $('#loader_modal').modal({
          backdrop: 'static',
          keyboard: false
        });
        $('#loader_modal').modal('show');
      },
      url: "{!! route('get-teachers-salaries') !!}",
       data: function(data) {data.page = 'all-teacher', data.from_date = $('#from_date').val(), data.to_date = $('#to_date').val(), data.from_payment_date = $('#from_payment_date').val(), data.to_payment_date = $('#to_payment_date').val(), data.status = $('.status').val()} ,
    },
    columns: [
        { data: 'checkbox', name: 'checkbox' }, 
        { data: 'name', name: 'name' }, 
        { data: 'phone', name: 'phone' }, 
        { data: 'salary', name: 'salary' }, 
        { data: 'deduction', name: 'deduction' }, 
        { data: 'reason', name: 'reason' },  
        { data: 'bonus_amount', name: 'bonus_amount' }, 
        { data: 'bonus_description', name: 'bonus_description' }, 
        { data: 'total_payable_amount', name: 'total_payable_amount' }, 
        { data: 'paid_amount', name: 'paid_amount' }, 
        { data: 'unpaid_amount', name: 'unpaid_amount' }, 
        { data: 'salary_month', name: 'salary_month' }, 
        { data: 'submitted_date', name: 'submitted_date' }, 
        { data: 'remarks', name: 'remarks' }, 
        { data: 'status', name: 'status' }, 
        { data: 'action', name: 'action' }, 
    ],
    initComplete: function () {
    // Enable THEAD scroll bars
    $('.dataTables_scrollHead').css('overflow', 'auto');
    // Sync THEAD scrolling with TBODY
      $('.dataTables_scrollHead').on('scroll', function () {
          $('.dataTables_scrollBody').scrollLeft($(this).scrollLeft());
      });
    },
    drawCallback: function(){
      $('#loader_modal').modal('hide');
    },
    footerCallback: function ( row, data, start, end, display ) {
        var api              = this.api();
        var json             = api.ajax.json();
        var payable = 0;
        var total_amount     = json.total_amount;
        payable += total_amount;
        total_amount     = parseFloat(total_amount).toFixed(2);
        total_amount     = total_amount.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ",");

        var paid_amount     = json.paid_amount;
        paid_amount     = parseFloat(paid_amount).toFixed(2);
        paid_amount     = paid_amount.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ",");

        var unpaid_amount     = json.unpaid_amount;
        unpaid_amount     = parseFloat(unpaid_amount).toFixed(2);
        unpaid_amount     = unpaid_amount.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ",");

        var Unpaid = (total_amount - paid_amount);

        var deduction     = json.deduction;
        payable -= deduction;
        deduction     = parseFloat(deduction).toFixed(2);
        deduction     = deduction.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ",");

        var bonus_amount     = json.bonus_amount;
        payable += bonus_amount;
        bonus_amount     = parseFloat(bonus_amount).toFixed(2);
        bonus_amount     = bonus_amount.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ",");

        payable     = parseFloat(payable).toFixed(2);
        payable     = payable.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ",");

        $( api.column( 4 ).footer() ).html(total_amount); 
        $( api.column( 5 ).footer() ).html(deduction); 
        $( api.column( 7 ).footer() ).html(bonus_amount); 
        $( api.column( 9 ).footer() ).html(payable); 
        $( api.column( 10 ).footer() ).html(paid_amount); 
        $( api.column( 11 ).footer() ).html(unpaid_amount); 
      }
  });
  $(document).on('change','#from_date',function(){
      $('.table-teachers-salaries').DataTable().ajax.reload();
      $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
    });

  $(document).on('change','.status',function(){
      $('.table-teachers-salaries').DataTable().ajax.reload();
      $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
    });

    $(document).on('change','#to_date',function(){
      $('.table-teachers-salaries').DataTable().ajax.reload();
      $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
    });
    $(document).on('change','#from_payment_date',function(){
      $('.table-teachers-salaries').DataTable().ajax.reload();
      $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
    });

    $(document).on('change','#to_payment_date',function(){
      $('.table-teachers-salaries').DataTable().ajax.reload();
      $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
    });
    $(document).on('click','#reset',function(){
      $('#from_date').val('');
      $('#to_date').val('');
      $('#from_payment_date').val('');
      $('#to_payment_date').val('');
      $('.status').val('');
      $('.table-teachers-salaries').DataTable().ajax.reload();
      $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
    })

    $(document).on('click','.suspend_teacher',function(){
      var id = $(this).data('id');

        swal({
            title: "Are You Sure!",
            html: true,
            text: "<b>You want to suspend this teacher !!!</b>",
            type: "info",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes!",
            cancelButtonText: "No!",
            closeOnConfirm: true,
            closeOnCancel: false
          },
            function(isConfirm) {
            if (isConfirm){
             $.ajaxSetup({
              headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
              }
            });
            $.ajax({
              url: "{{ route('suspend-teacher') }}",
              method: 'post',
              data: {id:id},
              beforeSend: function(){
                $('#loader_modal').modal({
                  backdrop: 'static',
                  keyboard: false
                });
                $('#loader_modal').modal('show');
              },
              success: function (response) {
                if(response.success == true)
                {
                  toastr.success('Success!', 'Teacher Suspended Successfully!!!',{"positionClass": "toast-bottom-right"});
                  $('.table-teachers').DataTable().ajax.reload();
                  $('#loader_modal').modal('hide');
                }
               
              },
              error: function(request, status, error){
                $('#loader_modal').modal('hide');
              }
            });
            }
            else{
              swal("Cancelled", "", "error");
              
            }
          });
  });

  $(document).on('change','#name',function(){
    // alert($(this).val());
    var id = $(this).val();

    $.ajaxSetup({
              headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
              }
            });
            $.ajax({
              url: "{{ route('fetch-teacher-detail') }}",
              method: 'post',
              data: {id:id},
              beforeSend: function(){
                $('#loader_modal').modal({
                  backdrop: 'static',
                  keyboard: false
                });
                $('#loader_modal').modal('show');
              },
              success: function (response) {
                
                if(response.success == true)
                {
                  // alert(response.fee);
                $('#loader_modal').modal('hide');
                // console.log(response.student);
                  $('#phone').val(response.teacher.phone);
                  $('#guardian').val(response.teacher.guardian);
                  $('#salary').val(response.teacher.salary);

                }
               
              },
              error: function(request, status, error){
                $('#loader_modal').modal('hide');
              }
            });
  });

    $('#paid_amount').on('focusout',function(){
    var paid = $('#paid_amount').val();

    var total = $('#salary').val();

    var diff = total - paid;

    $('#remaining_amount').val(diff);
  });

    $('#bonus').on('focusout',function(){
    var bonus = $('#bonus').val();

    var total = $('#salary').val();

    var diff = total * (bonus/100);

    $('#bonus_amount').val(diff);
  });

      $('#add-teacher-salary-form').on('submit',function(e){
    e.preventDefault();

    var paid_amount = $('#paid_amount').val();
    if(paid_amount == null || paid_amount == '')
    {
      toastr.error('Sorry!', 'Paid Amount Cannot Be Null!!!',{"positionClass": "toast-bottom-right"});
      return false;
    }

    var salary_month = $('#salary_month').val();
    if(salary_month == null || salary_month == '')
    {
      toastr.error('Sorry!', 'Salary Month Cannot Be Null!!!',{"positionClass": "toast-bottom-right"});
      return false;
    }

    var name = $('#name').val();
    if(name == null || name == '')
    {
      toastr.error('Sorry!', 'Teacher Name Cannot Be Null!!!',{"positionClass": "toast-bottom-right"});
      return false;
    }

      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
          }
      });
       $.ajax({
          url: "{{ route('add-teacher-salary') }}",
          dataType: 'json',
          type: 'post',
          data: new FormData(this),
          contentType: false,
          cache: false,
          processData:false,
          beforeSend: function(){
            $('.save-btn').addClass('disabled');
            $('.save-btn').attr('disabled', true);
            $('#loader_modal').modal({
              backdrop: 'static',
              keyboard: false
            });
            $('#loader_modal').modal('show');
          },
          success: function(result){
            $('.save-btn').attr('disabled', true);
            $('.save-btn').removeAttr('disabled');
            if(result.success == true){
              toastr.success('Success!', 'Teacher Salary Added Successfully!!!',{"positionClass": "toast-bottom-right"});
              $('.table-teachers-salaries').DataTable().ajax.reload();
              /*setTimeout(function(){
                window.location.reload();
              }, 2000);*/

              $('#add-teacher-salary-form')[0].reset();
              $('#addTeacherSalary').modal('hide');
              $('#loader_modal').modal('hide');

            }
            else if(result.paid == true)
            {
               toastr.warning('Warning!', 'This month salary already submitted!!!',{"positionClass": "toast-bottom-right"});
              $('.table-teachers-salaries').DataTable().ajax.reload();
              $('#add-teacher-salary-form')[0].reset();
              $('#addTeacherSalary').modal('hide');
              $('#loader_modal').modal('hide');

            }
            else{
              toastr.error('Error!', result.errormsg,{"positionClass": "toast-bottom-right"});
              $('#loader_modal').modal('hide');
            }

          },
          error: function (request, status, error) {
                /*$('.form-control').removeClass('is-invalid');
                $('.form-control').next().remove();*/
                $('#loader_modal').modal('hide');
                $('.save-btn').removeClass('disabled');
                $('.save-btn').removeAttr('disabled');
                json = $.parseJSON(request.responseText);
                $.each(json.errors, function(key, value){
                      $('input[name="'+key+'"]').after('<span class="invalid-feedback" role="alert"><strong>'+value+'</strong>');
                     $('input[name="'+key+'"]').addClass('is-invalid');
                     $('textarea[name="'+key+'"]').after('<span class="invalid-feedback" role="alert"><strong>'+value+'</strong>');
                     $('textarea[name="'+key+'"]').addClass('is-invalid');


                });
            }
        });
  });

    $(document).on('click','.delete_student_permanant',function(){
      var id = $(this).data('id');

        swal({
            title: "Are You Sure!",
            html: true,
            text: "<b>You want to delete this record !!!</b>",
            type: "info",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes!",
            cancelButtonText: "No!",
            closeOnConfirm: true,
            closeOnCancel: false
          },
            function(isConfirm) {
            if (isConfirm){
             $.ajaxSetup({
              headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
              }
            });
            $.ajax({
              url: "{{ route('delete-teacher-salary') }}",
              method: 'post',
              data: {id:id},
              beforeSend: function(){
                $('#loader_modal').modal({
                  backdrop: 'static',
                  keyboard: false
                });
                $('#loader_modal').modal('show');
              },
              success: function (response) {

                if(response.success == true)
                {
                  toastr.success('Success!', 'Record Deleted Successfully!!!',{"positionClass": "toast-bottom-right"});
                  $('.table-teachers-salaries').DataTable().ajax.reload();
                  $('#loader_modal').modal('hide');
                }
                else if(response.success == false)
                {
                  toastr.error('Error!', 'Something went wrong!!!',{"positionClass": "toast-bottom-right"});
                  $('.table-teachers-salaries').DataTable().ajax.reload();
                  $('#loader_modal').modal('hide');
                }
               
              },
              error: function(request, status, error){
                $('#loader_modal').modal('hide');
              }
            });
            }
            else{
              swal("Cancelled", "", "error");
              
            }
          });
  });

  $(document).on("dblclick",".inputDoubleClick",function(){
    $(this).addClass('d-none');
    $(this).next().removeClass('d-none');
    $(this).next().addClass('active');
    $(this).next().focus();
  });
    $(document).on('keypress keyup focusout',".fieldFocus",function(e) {
    // alert('hi');
    if (e.keyCode === 27 && $(this).hasClass('active')) {
      var fieldvalue = $(this).prev().data('fieldvalue');
      var thisPointer = $(this);
      thisPointer.addClass('d-none');
      thisPointer.val(fieldvalue);
      thisPointer.removeClass('active');
      thisPointer.prev().removeClass('d-none');
    }

    var name = $(this).attr('name');
    // alert(name);

    if( (e.keyCode === 13 || e.which === 0) && $(this).val().length > 0 && $(this).hasClass('active')){
      var str = $(this).val();
      if(($(this).val().length < 1 ||  !str.replace(/\s/g, '').length))
      {
        swal({ html:true, title:'Alert !!!', text:'<b>Please Enter At Least 1 Character (this string may only contain white spaces)!!!</b>'});
        return false;
      }
      if(true)
      {
        $(this).removeClass('active');
        var fieldvalue = $(this).prev().data('fieldvalue');
        var new_value = $(this).val();
        // alert(new_value);
        if(fieldvalue == new_value)
        {
          var thisPointer = $(this);
          thisPointer.addClass('d-none');
          thisPointer.prev().removeClass('d-none');
          $(this).prev().html(fieldvalue);
        }
        else
        {
          var thisPointer = $(this);
          thisPointer.addClass('d-none');
          thisPointer.prev().removeClass('d-none');
          if(new_value != '')
          {
            $(this).prev().html(new_value);
          }
          $(this).prev().data('fieldvalue', new_value);
          var id = $(this).data('id');
          saveStudentData(thisPointer,thisPointer.attr('name'), thisPointer.val(),fieldvalue,id);
        }
      }
    }

  });

      function saveStudentData(thisPointer,field_name,field_value,new_select_value,id){
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
      });
      $.ajax({
        method: "post",

        url: "{{ route('update-teacher-salary') }}",
        dataType: 'json',
        data: 'id='+id+'&'+field_name+'='+encodeURIComponent(field_value)+'&'+'new_select_value'+'='+encodeURIComponent(new_select_value),
        beforeSend: function(){
          $('#loader_modal').modal({
            backdrop: 'static',
            keyboard: false
          });
          $('#loader_modal').modal('show');
        },
        success: function(data)
        {
          $("#loader_modal").modal('hide');
          if(data.success == true)
          {
            
            toastr.success('Success!', 'Information updated successfully.',{"positionClass": "toast-bottom-right"});
            $('.table-teachers-salaries').DataTable().ajax.reload();

            return true;
          }

        },

      });
    }
});
</script>
@stop

