@extends('user.layouts.layout')

@section('title','Online Marketing')

@section('content')
<style type="text/css">
   
</style>

                    <div>
                        <div class="row page-title-row">
                            <div class="col-md col-sm col-12 page-title-col align-self-center">
                                <h1 class="page-title">Suspended Teachers</h1>
                            </div>
                            
                    </div>

                    <div class="row p-0 mt-0">
                        <div class="col-12 mt-4">
                        <div class="card p-4">
                        <div class="table-responsive">
                        <table class="table table-striped nowrap table-teachers">
                        <thead>
                        <tr>
                            <th>Action</th>
                            <th>Name</th>
                            <th>Guardian</th>
                            <th>Phone</th>
                            <th>Education</th>
                            <th>Joining Date</th>
                        </tr>
                        </thead>
                        </table>
                        </div>
                    </div>
                </div>
                    </div>
                    </div>


@endsection

@section('javascript')
<script type="text/javascript">
  $(document).ready(function(){

  var table2 =  $('.table-teachers').DataTable({
    processing: true,
    searching: true,
    "language": {
    processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw" style="color:#13436c;"></i><span class="sr-only">Loading...</span> '},
    ordering: false,
    pageLength: {{50}},
    serverSide: true,
    autoWidth: false,
    "lengthMenu": [50,100,150,200],
    
    scrollX:true,
    scrollY : '90vh',
    scrollCollapse: true,
    ajax: 
    {
      beforeSend: function(){
        $('#loader_modal').modal({
          backdrop: 'static',
          keyboard: false
        });
        $('#loader_modal').modal('show');
      },
      url: "{!! route('get-suspended-teachers') !!}",
       data: function(data) { } ,
    },
    columns: [
        { data: 'action', name: 'action' }, 
        { data: 'name', name: 'name' }, 
        { data: 'guardian', name: 'guardian' }, 
        { data: 'phone', name: 'phone' }, 
        { data: 'education', name: 'education' }, 
        { data: 'joining_date', name: 'joining_date' }, 
    ],
    initComplete: function () {
    // Enable THEAD scroll bars
    $('.dataTables_scrollHead').css('overflow', 'auto');
    // Sync THEAD scrolling with TBODY
      $('.dataTables_scrollHead').on('scroll', function () {
          $('.dataTables_scrollBody').scrollLeft($(this).scrollLeft());
      });
    },
    drawCallback: function(){
      $('#loader_modal').modal('hide');
    },
  });

    $(document).on('click','.activate_teacher',function(){
      var id = $(this).data('id');

        swal({
            title: "Are You Sure!",
            html: true,
            text: "<b>You want to activate this teacher !!!</b>",
            type: "info",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes!",
            cancelButtonText: "No!",
            closeOnConfirm: true,
            closeOnCancel: false
          },
            function(isConfirm) {
            if (isConfirm){
             $.ajaxSetup({
              headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
              }
            });
            $.ajax({
              url: "{{ route('activate-teacher') }}",
              method: 'post',
              data: {id:id},
              beforeSend: function(){
                $('#loader_modal').modal({
                  backdrop: 'static',
                  keyboard: false
                });
                $('#loader_modal').modal('show');
              },
              success: function (response) {
                if(response.success == true)
                {
                  toastr.success('Success!', 'Teacher Activated Successfully!!!',{"positionClass": "toast-bottom-right"});
                  $('.table-teachers').DataTable().ajax.reload();
                  $('#loader_modal').modal('hide');
                }
               
              },
              error: function(request, status, error){
                $('#loader_modal').modal('hide');
              }
            });
            }
            else{
              swal("Cancelled", "", "error");
              
            }
          });
  });
});
</script>
@stop

