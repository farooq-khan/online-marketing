@extends('user.layouts.layout')

@section('title','All Result')

@section('content')
<style type="text/css">
   
</style>

      <div class="row page-title-row">
          <div class="col-md col-sm col page-title-col align-self-center">
              <h1 class="page-title">Students Result</h1>
          </div>
          <div class="col-sm col-auto mt-sm-0 ml-auto text-right page-action-button">
              <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addResult">+Add Result</button>
          </div> 
      </div>

      <div class="card card-border-top mb-4">
        <div class="card-header">
              <h6 class="mb-0">Filters</h6>
        </div>
        <div class="card-body pb-2">
          <div class="row mb-0 form-row row-cols-xl-4 row-cols-md-4 row-cols-sm-2 row-cols-2">
            <div class="col form-group">
              <label for="to_date">Class</label>
              <select class="form-control form-control student_classes_select1 state-tags" name="warehouse_id" {{Auth::user()->role == 'teacher' ? 'disabled' : ''}}>
                <option value="">All Classes</option>
                 @foreach($classes as $class)
                  <!-- <option value="{{$class->id}}"> {{$class->class_name}} </option> -->
                  @if(Auth::user()->role == 'teacher')
                    <option value="{{$class->id}}" {{Auth::user()->id == $class->teacher_id ? 'selected' : ''}}> {{$class->class_name}} </option>
                  @else
                    <option value="{{$class->id}}"> {{$class->class_name}} </option>
                  @endif
                  @endforeach
              </select>
            </div>
             <div class="col form-group">
              <label for="from_date">Exam</label>
              <select class="form-control form-control exam_id state-tags" name="exam_id2" >
                <option value="">All Exams</option>
                 @foreach($exams as $exam)
                  <option value="{{$exam->id}}"> {{$exam->name}} </option>
                  @endforeach
              </select>
            </div>
            <div class="col form-group">
              <label for="from_date">From Date</label>
              <input type="date" name="from_date" id="from_date" class="form-control" placeholder=""/>
            </div>
            <div class="col form-group">
              <label for="to_date">To Date</label>
              <input type="date" name="to_date" id="to_date" class="form-control" placeholder=""/>
            </div>
          </div>
        </div>
      </div>

      
        <div class="card">
          <div class="card-body">
            <div class="selected-item catalogue-btn-group mt-4 mt-sm-3 ml-3 d-none">
              <a href="javascript:void(0)" class="actionicon viewIcon download_invoice_all" data-id="1" title="Download Invoice"><i class="zmdi zmdi-download"></i></a>
            </div>
            <div class="table-responsive">
              <table class="table table-striped nowrap table-students nowrap">
              <thead>
              <tr>
                  <th>S.No.</th>
                  <th>Class</th>
                  <th>Exam</th>
                  <th>From Date</th>
                  <th>To Date</th>
                  <th>Action</th>
              </tr>
              </thead>
              </table>
            </div>
        </div>
      </div>



<!--Add Student Modal -->
<div class="modal fade" id="addResult" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add Result</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form id="add-exam-form" method="POST">
        @csrf
      <div class="modal-body ">
         

          <div class="row">
          <div class="col-lg-6 col-md-6">
            <select class="form-control form-control student_classes_select state-tags" name="class_id" {{Auth::user()->role == 'teacher' ? 'disabled' : ''}}>
              <option value="">-- All Classes --</option>
               @foreach($classes as $class)
                <!-- <option value="{{$class->id}}"> {{$class->class_name}} </option> -->
                @if(Auth::user()->role == 'teacher')
                  <option value="{{$class->id}}" {{Auth::user()->id == $class->teacher_id ? 'selected' : ''}}> {{$class->class_name}} </option>
                @else
                  <option value="{{$class->id}}"> {{$class->class_name}} </option>
                @endif
                @endforeach
            </select>
          </div>

          
          <div class="col-lg-6 col-md-6">
            <select class="form-control form-control student_exam_id state-tags" name="exam_id" >
              <option value="">-- All Exams --</option>
               @foreach($exams as $exam)
                <option value="{{$exam->id}}"> {{$exam->name}} </option>
                @endforeach
            </select>
          </div>

          <div class="col-lg-6 col-md-6 mt-3">
            <input type="date" name="from_date" class="form-control from_date_exam" placeholder="Exam from year ..." required="">
          </div>
          <div class="col-lg-6 col-md-6 mt-3">
            <input type="date" name="to_date" class="form-control to_date_exam " placeholder="Exam to year ..." required="">
          </div>
          <div class="col-lg-6 col-md-6 mt-3">
            <input type="text" name="year" class="form-control year " placeholder="Exam year ..." required="">
          </div>
          </div>
          
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary save-btn add-exam">Add</button>
      </div>
      </form>
    </div>
  </div>
</div>

  <input type="hidden" name="fees_selected" class="fees_selected">


@endsection

@section('javascript')
<script type="text/javascript">
$(document).ready(function(){
  $(document).on('click', '.download_invoice', function(){

      var id = $(this).data('id');
      var url = "{{url('user/result-print')}}"+"/"+id;
      window.open(url, 'Orders Receivable Print', 'width=1200,height=600,scrollbars=yes');
      $('#loader_modal').modal('hide');


    });

  $(document).on('click','.add-exam',function(e){
    var class_id = $('.student_classes_select').val();
    var exam_id = $('.student_exam_id').val();
    var year = $('.year').val();
    var from_date_exam = $('.from_date_exam').val();
    var to_date_exam = $('.to_date_exam').val();
    var new_res = true;

    window.location.href = "{{url('user/submit-result')}}/"+class_id+"/"+exam_id+"/"+year+"/"+from_date_exam+"/"+to_date_exam+"/"+new_res;
  });

  $(document).on('click', '.check-all1', function () {
  if(this.checked == true){
  $('.check').prop('checked', true);
  $('.check').parents('tr').addClass('selected');
  var cb_length = $( ".check:checked" ).length;
  if(cb_length > 0){
    $('.selected-item').removeClass('d-none');
  }
}else{
  $('.check').prop('checked', false);
  $('.check').parents('tr').removeClass('selected');
  $('.selected-item').addClass('d-none');
  
}
});

   $(document).on('click', '.check', function () {
    // $(this).removeClass('d-none');
        var cb_length = $( ".check:checked" ).length;
        var st_pieces = $(this).parents('tr').attr('data-pieces');
        if(this.checked == true){
        $('.selected-item').removeClass('d-none');
        $(this).parents('tr').addClass('selected');
      }else{
        $(this).parents('tr').removeClass('selected');
        if(cb_length == 0){
         $('.selected-item').addClass('d-none');
        }
        
      }
    });

    var table2 =  $('.table-students').DataTable({
    processing: true,
    searching: true,
    "language": {
    processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw" style="color:#13436c;"></i><span class="sr-only">Loading...</span> '},
    ordering: false,
    pageLength: {{50}},
    serverSide: true,
    autoWidth: false,
    "lengthMenu": [50,100,150,200],
    
    scrollX:true,
    scrollY : '90vh',
    scrollCollapse: true,
    ajax: 
    {
      beforeSend: function(){
        $('#loader_modal').modal({
          backdrop: 'static',
          keyboard: false
        });
        $('#loader_modal').modal('show');
      },
      url: "{!! route('get-all-results') !!}",
       data: function(data) { data.student_classes_select = $('.student_classes_select1 option:selected').val(), data.exam_id = $('.exam_id option:selected').val(), data.from_date = $('#from_date').val(), data.to_date = $('#to_datee').val() } ,
    },
    columns: [
        { data: 'empty_col', name: 'empty_col' }, 
        { data: 'class', name: 'class' }, 
        { data: 'exam', name: 'exam' }, 
        { data: 'from_date', name: 'from_date' },         
        { data: 'to_date', name: 'to_date' },         
        { data: 'action', name: 'action' }, 
    ],
    initComplete: function () {
    // Enable THEAD scroll bars
    $('.dataTables_scrollHead').css('overflow', 'auto');
    // Sync THEAD scrolling with TBODY
      $('.dataTables_scrollHead').on('scroll', function () {
          $('.dataTables_scrollBody').scrollLeft($(this).scrollLeft());
      });
    },
    drawCallback: function(){
      $('#loader_modal').modal('hide');
    },
  });

  table2.on( 'draw.dt', function () {
    var PageInfo = $('.table-students').DataTable().page.info();
         table2.column(0, { page: 'current' }).nodes().each( function (cell, i) {
            cell.innerHTML = i + 1 + PageInfo.start;
        } );
    } );

  $(document).on('change','.student_classes_select1',function(){
    $('.table-students').DataTable().ajax.reload();
    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
  });

  $(document).on('change','.exam_id',function(){
    $('#check-all').prop('checked',false);
    $('.selected-item').addClass('d-none');
    $('.table-students').DataTable().ajax.reload();
    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
  });

  $(document).on('change','#from_date',function(){
    $('.table-students').DataTable().ajax.reload();
    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
  });

    $(document).on('change','#to_date',function(){
    $('.table-students').DataTable().ajax.reload();
    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
  });

      $(document).on('click', '.download_invoice_all', function(){
    var selected_fees = [];
    $("input.check:checked").each(function() {
      selected_fees.push($(this).val());
    });

    if(selected_fees == ''){
            toastr.error('Error!', 'Select Fees First.',{"positionClass": "toast-bottom-right"});
        return false;
        }else{
        $('.fees_selected').val(selected_fees);

        var std_ids = $('.fees_selected').val();
        var exam_id = $('.exam_id').val();

        if(exam_id == '' || exam_id == null)
        {
            toastr.info('Please!', 'Select Exam from filter.',{"positionClass": "toast-bottom-right"});
            return false;
        }
        else
        {
          var url = "{{url('user/result-print-all')}}"+"/"+std_ids+"/"+exam_id;
          window.open(url, 'Orders Receivable Print', 'width=1200,height=600,scrollbars=yes');
        }
       }

    });

      $(document).on('click','.delete_student_permanant',function(){
      var id = $(this).data('id');

        swal({
            title: "Are You Sure!",
            html: true,
            text: "<b>You want to delete this record !!!</b>",
            type: "info",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes!",
            cancelButtonText: "No!",
            closeOnConfirm: true,
            closeOnCancel: false
          },
            function(isConfirm) {
            if (isConfirm){
             $.ajaxSetup({
              headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
              }
            });
            $.ajax({
              url: "{{ route('delete-student-result-permanent') }}",
              method: 'post',
              data: {id:id},
              beforeSend: function(){
                $('#loader_modal').modal({
                  backdrop: 'static',
                  keyboard: false
                });
                $('#loader_modal').modal('show');
              },
              success: function (response) {

                if(response.success == true)
                {
                  toastr.success('Success!', 'Record Deleted Successfully!!!',{"positionClass": "toast-bottom-right"});
                  $('.table-students').DataTable().ajax.reload();
                  $('#loader_modal').modal('hide');
                }
               
              },
              error: function(request, status, error){
                $('#loader_modal').modal('hide');
              }
            });
            }
            else{
              swal("Cancelled", "", "error");
              
            }
          });
  });

});
</script>
@stop

