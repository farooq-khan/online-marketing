{{-- @extends('layouts.app') --}}
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Register to continue</title>
    <link href="{{asset('public/css/bootstrap.css')}}" rel="stylesheet" />
    <link href="{{asset('public/frontend/css/toastr.min.css')}}" rel="stylesheet" />
    <link href="{{asset('public/css/custom.css?')}}{{time()}}" rel="stylesheet" />
<style>
    .form-control {
    height: 2.75rem;
}
.btn-lg {
  font-size: 1.125rem
}
.card { max-width: 560px; z-index: 1;
  position: relative;}
section.signup:after {
    content: '';
    background: rgb(0 0 0 / 50%);
    height: 100%;
    width: 100%;
    position: absolute;
    z-index: 0;
    left: 0; top: 0
}
section.signup {
  background: url("{{asset('public/assets/img/page-bg.jpg')}}");
  background-size: cover;
}

@media(max-width:576px){
  .h2, h2 {
    font-size: 1.85rem;
}
}


</style>

</head>
<body style="background-color: #eee;">
    
    @if (session('msg'))
    <div class="alert alert-success">
        {{ session('msg') }}
    </div>
@endif

<!-- Sign up form -->
        {{-- <section class="signup d-none" >
            <div class="container" style="background-color: rgba(255,255,255,0.5);">
                @if (session('msg'))
                    <div class="alert alert-success">
                        {{ session('msg') }}
                    </div>
                @endif
                <div class="signup-content">
                    <div class="signup-form">
                        <h2 class="form-title">Sign In</h2>
                        <form method="POST" action="{{ route('login') }}">
                            @csrf
                            <div class="form-group">
                                <input type="text" name="email" id="email" placeholder="Your Email"/>
                            </div>
                            <div class="form-group">
                                <input type="password" name="password" id="pass" placeholder="Password"/>
                            </div>
                            <div class="form-group form-button">
                                <input type="submit" name="signup" id="signup" class="form-submit" value="Sign In"/>
                            </div>
                        </form>
                    </div>
                    <div class="signup-image" style="margin-top: 45px;">
                        <figure><img src="{{url('public/assets/img/signup-image.png')}}" alt="sing up image"></figure>
                    </div>
                </div>
            </div>
        </section> --}}
        <section class="align-items-center d-flex min-vh-100 px-3 signup">
                  <div class="card mx-auto w-100">
                    <div class="signup-form card-body p-sm-5 p-4">
                        <h2 class="mb-4 pb-sm-2 text-center">Sign up to continue</h2>
                        <form class="register-form add-users-form m-sm-0 m-2" id="register-form">
                          @csrf
                            <div class="form-group mb-sm-4">
                                <input class="form-control" type="text" name="name" id="name" placeholder="Your Full Name"/>
                            </div>
                            <div class="form-group mb-sm-4">
                                <input class="form-control" type="email" name="email" id="email" placeholder="Your Email"/>
                            </div>
                            <div class="form-group mb-sm-4">
                                <input class="form-control" type="number" name="phone" id="phone" placeholder="0300-1234567"/>
                            </div>
                            <div class="form-row">
                              <div class="col form-group mb-sm-4">
                                  <input class="form-control" type="password" name="pass" id="pass" placeholder="Password"/>
                              </div>
                              <div class="col form-group mb-sm-4">
                                  <input class="form-control" type="password" name="re_pass" id="re_pass" placeholder="Confirm password"/>
                              </div>
                            </div>
                            <div class="custom-control custom-checkbox mb-sm-4 mb-3">
                              <input type="checkbox" class="custom-control-input" id="agree-term">
                              <label class="custom-control-label font-weight-normal" for="agree-term">I agree all statements in  <a href="#" class="term-service text-link">Terms of service</a></label>
                            </div>
                            <div class="form-group form-button mb-sm-4">
                                <input type="submit" name="signup" id="signup" class="btn btn-primary btn-lg w-100 form-submit save-btn" value="Sign up"/>
                            </div>
                            <div class="text-center">
                              <p class="mb-0 me-2">Already have an account? <a href="{{ route('login') }}" class="text-link" style="text-decoration: underline">Login</a> here.</p>
                            </div>
                        </form>
                    </div>
                  </div>
        </section>

        <!-- Sing in  Form -->

<script src="{{asset('public/frontend/js/jquery.min.js')}}"></script>
<script src="{{asset('public/js/bootstrap.min.js')}}" crossorigin="anonymous"></script>
<script src="{{asset('public/frontend/js/toastr.min.js')}}"></script>
<script type="text/javascript">
$(document).ready(function(){
  $(document).on('click', '.save-btn', function(e){
      e.preventDefault();

      // if($('#sponsor_id').val() == '')
      // {
      //   toastr.error('Sorry!', 'Sponsor No. is required',{"positionClass": "toast-bottom-right"});
      //   return false;
      // }

      if($('#agree-term').prop("checked") == false)
      {
        toastr.error('Alert!', 'Please accept terms of service',{"positionClass": "toast-bottom-right"});
        return false;
      }

        var new_password= $("#pass").val();
        var confirm_new_password= $("#re_pass").val();
        if(new_password != confirm_new_password)
        {
            toastr.error('Alert!', 'Passwords does not match !!',{"positionClass": "toast-bottom-right"});
            return false;
        }     
      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
          }
      });
       $.ajax({
          url: "{{ route('add-users') }}",
          method: 'post',
          data: $('.add-users-form').serialize(),
          beforeSend: function(){
            $('#loader_modal').modal({
              backdrop: 'static',
              keyboard: false
            });
            $("#loader_modal").modal('show');
            $('.save-btn').val('Please wait...');
            $('.save-btn').addClass('disabled');
            $('.save-btn').attr('disabled', true);
          },
          success: function(result){
            $("#loader_modal").modal('hide');
            $('.save-btn').val('add');
            $('.save-btn').attr('disabled', true);
            $('.save-btn').removeAttr('disabled');
            if(result.success === true){
              $('.modal').modal('hide');
              toastr.success('Success!', 'User added successfully',{"positionClass": "toast-bottom-right"});
              $('.add-users-form')[0].reset();

              setTimeout(function(){ window.location.href = "{{route('login')}}"; }, 3000);

              

            }
            else if (result.success === false) {
              toastr.success('Success!', result.message ,{"positionClass": "toast-bottom-right"});

            }


          },
          error: function (request, status, error) {
                $("#loader_modal").modal('hide');
                $('.save-btn').val('add');
                $('.save-btn').removeClass('disabled');
                $('.save-btn').removeAttr('disabled');
                $('.form-control').removeClass('is-invalid');
                $('.form-control').next().remove();
                json = $.parseJSON(request.responseText);
                $.each(json.errors, function(key, value){
                    $('input[name="'+key+'"]').after('<span class="invalid-feedback" role="alert"><strong>'+value+'</strong>');
                     $('input[name="'+key+'"]').addClass('is-invalid');

                     $('select[name="'+key+'"]').after('<span class="invalid-feedback" role="alert"><strong>'+value+'</strong>');
                     $('select[name="'+key+'"]').addClass('is-invalid');
                });

            }
        });
    });

      // to make that field on its orignal state
  $(document).on('keyup onkeypress focusout','.fieldFocus',function(e) { 
    e.preventDefault();
    var fieldvalue = $(this).val();
    if(e.which == 0){
      
      if($(this).val() == '')
      {
        return false;
      }
      else
      {
        var id = $(this).val();
      
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
      }); 
      $.ajax({
        method: "post",
        url: "{{ url('/check-sponsor') }}",
        dataType: 'json',
        data: {id:id},
        beforeSend: function(){
          // shahsky here
        },
        success: function(data)
        {
          if(data.success == false)
          {
            toastr.error('Alert!', 'Wrong Sponsor No. !!',{"positionClass": "toast-bottom-right"});
            $('#signup').attr('disabled','true');
          }
          else
          {
            $(".sponsor_logo").attr('src',"{{asset('public/uploads/users/logos/')}}/"+data.user.logo);
            $('#signup').attr({
              disabled: 'false',
            });
          }
        },
         error: function (request, status, error) {
          swal("Something Went Wrong! Contact System Administrator!");
            $('.form-control').removeClass('is-invalid');
            $('.form-control').next().remove();
            json = $.parseJSON(request.responseText);
            $.each(json.errors, function(key, value){
                $('input[name="'+key+'"]').after('<span class="invalid-feedback" role="alert"><strong>'+value+'</strong>');
                 $('input[name="'+key+'"]').addClass('is-invalid');
            });
        }

      });    
      }
      
      // $(this).attr('readonly','true');
    } 
  });
});
</script>

