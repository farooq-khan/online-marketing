@extends('layouts.app')

@section('title','Online Marketing')

@section('content')
<style type="text/css">
.invalid-feedback {
     font-size: 100%;
}
.disabled:disabled{
  opacity:0.5;
  cursor: not-allowed;
}

body{
      /*background: linear-gradient(135deg,#1776c3 0,#56a8eb 100%)!important;*/
      background-color: #1776c3;  
}

</style>
<!-- Sign up form -->
        <section class="signup">
            <div class="container">
                <div class="signup-content">
                    <div class="signup-form">
                        <h2 class="form-title">Sign In</h2>
                        <form class="register-form add-users-form" id="register-form" method="POST" action="{{ route('login') }}">
                            <div class="form-group">
                                <label for="email"><i class="zmdi zmdi-email"></i></label>
                                <input type="email" name="email" id="email" placeholder="Your Email"/>
                            </div>
                            <div class="form-group">
                                <label for="pass"><i class="zmdi zmdi-lock"></i></label>
                                <input type="password" name="pass" id="pass" placeholder="Password"/>
                            </div>
                          
                            <div class="form-group form-button">
                                <input type="submit" name="signup" id="signup" class="form-submit" value="Sign In"/>
                            </div>
                        </form>
                    </div>
                    <div class="signup-image">
                        <figure><img src="{{url('public/assets/img/signup-image.jpg')}}" alt="sing up image"></figure>
                  
                    </div>
                </div>
            </div>
        </section>

        <!-- Sing in  Form -->
@endsection

@section('javascript')
<script type="text/javascript">
$(document).ready(function(){
    alert('hi');
@if(Session::has('suspend'))
  toastr.info('Sorry!', "Your Account Has Disabled !!!",{"positionClass": "toast-bottom-right"});
  @php
   Session()->forget('suspend');
  @endphp
@endif
});
</script>
@stop

