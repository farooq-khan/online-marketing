<!DOCTYPE html>
<html>
<head>
	<title>Test Result</title>
	<style type="text/css">
		.student_info{
			border-collapse: collapse;
		}
		.student_info tr td {
			border: 1px solid #aaa;
		}

		.body_table {
			border-collapse: collapse;
		}
		.body_table tr th {
			border: 1px solid #000;
		}
		.body_table tr td {
			border: 1px solid #000;
		}
	</style>
</head>
	@foreach($student_result as $result)
<body>
	<div style="width: 100%;">
		<table style="width: 100%">
			<tr>
				<td>
					<img src="{{asset('public/uploads/school/images/'.@$school->logo)}}" width="80" style="margin-bottom: 0px;">
				</td>
				<td>
					<p style="margin: 0;text-transform: uppercase;font-weight: normal;font-size: 14px;color: #679DFB">BEHIND EVERY GREAT ACHIEVEMENT THERE IS A BURNING DESIRE</p>
					<h2 style="margin: 0;font-family: 'Clarendon Blk BT';
    					font-weight: '900';text-transform: uppercase;">{{@$school->name}}</h2>
    				<h4 style="margin: 0;">STUDENT'S MONTHLY PROGRESS REPORT</h4>
				</td>
			</tr>
		</table>
		<hr style="height: 2px;color: black;background: black;">

		<table style="width: 100%;" class="student_info">
			<tr>
				<td width="50%">Student Name: {{@$result->student->name}}</td>
				<td width="25%">Section : {{@$result->student_class->section}}</td>
				<td width="25%" style="color: white;">Empty</td>
			</tr>

			<tr style="background-color: #eee;">
				<td width="50%">Father Name: {{$result->student->guardian}}</td>
				<td width="25%">Class: {{$result->student_class->class_name}}</td>
				<td width="25%"><b>Admission No: {{$result->student->roll_no}}</b></td>
			</tr>
		</table>
		@php
            $total = 0;
            $obtained = 0;
            $iteration= 0;
            $obtained_marks_in_week[1] = null;
            $obtained_marks_in_week[2] = null;
            $obtained_marks_in_week[3] = null;
            $obtained_marks_in_week[4] = null;
          @endphp
		<table style="width: 100%;margin-top: 20px;" class="body_table">
			<tr style="background-color: #aaa;">
				<th width="10%">S.No.</th>
				<th width="20%">Subject</th>
				<th width="35%">
					Obtained marks out of
					<table style="width: 100%;border-collapse: collapse;margin: 0;">
						<tr>
							<th width="25%" style="border-left: none;border-bottom: none">1<sup>st</sup> Week</th>
							<th width="25%" style="border-bottom: none;">2<sup>nd</sup> Week</th>
							<th width="25%" style="border-bottom: none;">3<sup>rd</sup> Week</th>
							<th width="25%" style="border-right: none;border-bottom: none;">4<sup>th</sup> Week</th>
						</tr>
					</table>
				</th>
				<th width="35%">
					Total Obtained marks
					<table style="width: 100%;border-collapse: collapse;">
						<tr>
							<th width="30%" style="border-left: none;border-bottom: none;">In Figures</th>
							<th width="70%" style="border-left: none;border-bottom: none;">In Words</th>
						</tr>
					</table>
				</th>
			</tr>
			@foreach($subjects as $sub)
			@php
				$month = \Carbon\Carbon::parse($result->from_test_date)->format('m');
            	$test_results = \App\TestResult::where('student_id',$result->student_id)->where('class_id',$result->class_id)->where('test_id',$result->test_id)->where('subject_id',$sub->id)->whereDate('from_test_date','<=',$result->from_test_date)->whereMonth('from_test_date',$month)->get();
            	$obtained_marks_in_sub = 0;
            @endphp
            @if(@$test_results->count() > 0)
				<tr>
					<td>{{@$loop->iteration}}</td>
					<td>{{@$sub->name}}</td>
					<td>
						<table width="100%" style="border-collapse: collapse;margin: 0;">
							<tr>
							@foreach($test_results as $test_result)
							@php
								$obtained_marks_in_sub += $test_result->obtained_marks;
								$obtained_marks_in_week[$loop->iteration] += $test_result->obtained_marks;
							@endphp
								@if($test_result->obtained_marks === '00')
					              <td width="25%" style="border-left: none;border-bottom: none;border-top:none;" align="center">A</td>
					            @else
									<td width="25%" style="border-left: none;border-bottom: none;border-top:none;" align="center">{{$test_result != null ? $test_result->obtained_marks : ''}}</td>
								@endif
							@endforeach
							@for($i = $test_results->count()+1; $i <= 4; $i++)
								<td width="25%"style="border-left: none;border-bottom: none;border-top:none; {{@$i==4 ? 'border-right: none' : ''}}"></td>
							@endfor
							</tr>
						</table>
					</td>
					<td style="border-left: none;">
						<table style="width: 100%;border-collapse: collapse;">
						<tr>
							<td width="30%" style="border-left: none;border-top: none;border-bottom: none;" align="center">{{$obtained_marks_in_sub}}</td>
							<td width="70%" style="border-left: none;border-top: none;border-bottom: none;">
								@php 
									$iteration++;
									$f = new NumberFormatter("en", NumberFormatter::SPELLOUT);
              						$word = $f->format($obtained_marks_in_sub);
								@endphp
								<span style="text-transform: capitalize;">{{$word}}</span>
							</td>
						</tr>
					</table>
					</td>
				</tr>
			@endif
			@endforeach
			<tr>
				<td>{{$iteration+1}}</td>
				<td><b>Total Marks</b></td>
				<td>
					<table width="100%" style="border-collapse: collapse;margin: 0;">
							<tr>
								@php
									$total_1 = $obtained_marks_in_week[1] != null ? @$iteration*50 : null;
									$total_2 = $obtained_marks_in_week[2] != null ? @$iteration*50 : null;
									$total_3 = $obtained_marks_in_week[3] != null ? @$iteration*50 : null;
									$total_4 = $obtained_marks_in_week[4] != null ? @$iteration*50 : null;
								@endphp
								<td width="25%" align="center" style="border-left: none;border-bottom: none;border-top:none;"><b>{{$total_1 }}</b></td>
								<td width="25%" align="center" style="border-left: none;border-bottom: none;border-top:none;"><b>{{$total_2}}</b></td>
								<td width="25%" align="center" style="border-left: none;border-bottom: none;border-top:none;"><b>{{$total_3}}</b></td>
								<td width="25%" align="center" style="border-left: none;border-bottom: none;border-top:none;border-right: none;"><b>{{$total_4}}</b></td>
							</tr>
						</table>
				</td>
				<td style="border-left: none;">
						<table style="width: 100%;border-collapse: collapse;">
						<tr>
							<td width="30%" style="border-left: none;border-top: none;border-bottom: none;" align="center"><b>{{$total_1+$total_2+$total_3+$total_4}}</b></td>
							<td width="70%" style="border-left: none;border-top: none;border-bottom: none;">
								@php 
									$f = new NumberFormatter("en", NumberFormatter::SPELLOUT);
              						$word = $f->format($total_1+$total_2+$total_3+$total_4);
								@endphp
								<span style="text-transform: capitalize;"><b>{{$word}}</b></span>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td>{{$iteration+2}}</td>
				<td><b>Obtained Marks</b></td>
				<td>
					<table width="100%" style="border-collapse: collapse;margin: 0;">
							<tr>
								<td width="25%" align="center" style="border-left: none;border-bottom: none;border-top:none;"><b>{{$obtained_marks_in_week[1]}}</b></td>
								<td width="25%" align="center" style="border-left: none;border-bottom: none;border-top:none;"><b>{{$obtained_marks_in_week[2]}}</b></td>
								<td width="25%" align="center" style="border-left: none;border-bottom: none;border-top:none;"><b>{{$obtained_marks_in_week[3]}}</b></td>
								<td width="25%" align="center" style="border-left: none;border-bottom: none;border-top:none;border-right: none;"><b>{{$obtained_marks_in_week[4]}}</b></td>
							</tr>
						</table>
				</td>
				<td style="border-left: none;">
						<table style="width: 100%;border-collapse: collapse;">
						<tr>
							<td width="30%" style="border-left: none;border-top: none;border-bottom: none;" align="center"><b>
								{{$obtained_marks_in_week[1] + $obtained_marks_in_week[2] + $obtained_marks_in_week[3] + $obtained_marks_in_week[4]}}
							</b></td>
							<td width="70%" style="border-left: none;border-top: none;border-bottom: none;">
								@php 
									$f = new NumberFormatter("en", NumberFormatter::SPELLOUT);
              						$word = $f->format($obtained_marks_in_week[1] + $obtained_marks_in_week[2] + $obtained_marks_in_week[3] + $obtained_marks_in_week[4]);
								@endphp
								<span style="text-transform: capitalize;"><b>{{$word}}</b></span>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td>{{$iteration+3}}</td>
				<td><b>Percentage</b></td>
				<td>
					<table width="100%" style="border-collapse: collapse;margin: 0;">
							<tr>
								@php
									$first_week_per = ($obtained_marks_in_week[1] / ($iteration*50)) * 100;
									$second_week_per = ($obtained_marks_in_week[2] / ($iteration*50)) * 100;
									$third_week_per = ($obtained_marks_in_week[3] / ($iteration*50)) * 100;
									$fourth_week_per = ($obtained_marks_in_week[4] / ($iteration*50)) * 100;
								@endphp
								<td width="25%" align="center" style="border-left: none;border-bottom: none;border-top:none;"><b>{{number_format($first_week_per,2)}} %</b></td>
								<td width="25%" align="center" style="border-left: none;border-bottom: none;border-top:none;"><b>{{number_format($second_week_per,2)}} %</b></td>
								<td width="25%" align="center" style="border-left: none;border-bottom: none;border-top:none;"><b>{{number_format($third_week_per,2)}} %</b></td>
								<td width="25%" align="center" style="border-left: none;border-bottom: none;border-top:none;border-right: none;"><b>{{number_format($fourth_week_per,2)}} %</b></td>
							</tr>
						</table>
				</td>
				<td style="border-bottom: none;">
						<table style="width: 100%;border-collapse: collapse;">
						<tr>
							<td width="30%" style="border-left: none;border-top: none;border-bottom: 1px solid black;" align="center"><b>
								@php
									$obtained = $obtained_marks_in_week[1] + $obtained_marks_in_week[2] + $obtained_marks_in_week[3] + $obtained_marks_in_week[4];

									$total = $total_1+$total_2+$total_3+$total_4;

									$per = number_format(($obtained / $total)*100,2);
								@endphp
								{{$per}} %	
							</b></td>
							<td width="70%" style="border-left: none;border-top: none;border-bottom: 1px solid white;">
								<b>Principal:</b>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td>{{$iteration+4}}</td>
				<td><b>Grade(Position)</b></td>
				<td>
					<table width="100%" style="border-collapse: collapse;margin: 0;">
							<tr>
								@php
									$result = array();
									$result[1] = \App\Student::gradePosition($first_week_per);
									$result[2] = \App\Student::gradePosition($second_week_per);
									$result[3] = \App\Student::gradePosition($third_week_per);
									$result[4] = \App\Student::gradePosition($fourth_week_per);
								@endphp
								<td width="25%" align="center" style="border-left: none;border-bottom: none;border-top:none;"><b>{{$result[1]['grade']}}({{$result[1]['position']}})</b></td>
								<td width="25%" align="center" style="border-left: none;border-bottom: none;border-top:none;"><b>{{$result[1]['grade']}}({{$result[2]['position']}})</b></td>
								<td width="25%" align="center" style="border-left: none;border-bottom: none;border-top:none;"><b>{{$result[1]['grade']}}({{$result[3]['position']}})</b></td>
								<td width="25%" align="center" style="border-left: none;border-bottom: none;border-top:none;border-right: none;"><b>{{$result[4]['grade']}}({{$result[1]['position']}})</b></td>
							</tr>
						</table>
				</td>
				<td style="border-top: none;">
						<table style="width: 100%;border-collapse: collapse;">
						<tr>
							<td width="30%" style="border-left: none;border-top: none;border-bottom: none;" align="center"><b>
								@php
									$obtained = $obtained_marks_in_week[1] + $obtained_marks_in_week[2] + $obtained_marks_in_week[3] + $obtained_marks_in_week[4];

									$total = $total_1+$total_2+$total_3+$total_4;

									$per = number_format(($obtained / $total)*100,2);
									$grade = \App\Student::gradePosition($per);
								@endphp
								{{$grade['grade']}}({{$grade['position']}})	
							</b></td>
							<td width="70%" style="border-left: none;border-top: 1px solid white;border-bottom: none;">
								
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<div style="width: 100%">
			<p style="margin: 0 10px">
				Note: Dear parents kindly check the monthly report of your child carefully and contact school administration about any weakness or any suggestion regarding your child. Thank you.
			</p>
		</div>
	</div>
</body>
	@endforeach
</html>