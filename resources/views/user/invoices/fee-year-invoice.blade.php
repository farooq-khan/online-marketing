  <!DOCTYPE html>
  <html>
  <head>
    <meta charset="utf-8"/>
    <title>Invoice</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    
    <style type="text/css">
      @page {size: auto;   /* auto is the initial value */
        margin: 5mm;}
    	  table { font-size: 10px; }
    table tr td 
    {
      vertical-align: top; padding-bottom: 3px;
    }
    table 
    {
      border-collapse: collapse;
      font-size:12px;
      border-spacing: 0px;}
      .invoicetable tr td, .invoicetable tr th {
      border: 1px solid black;
      padding: 4px 7px;
    }
    .main-table > tbody > tr > td  { padding-right: 10px; padding-left: 10px;  }

    .invoicetable tr.inv-total-tr td {
        border: none;
        padding: 10px 2px 5px;
    }
    .inv-total-td span {
        font-weight: bold;
        border-bottom: 2px solid #000;
        display: inline-block;
        padding: 0px 5px 2px;
    }
    </style>

  </head>
@php
  use Carbon\Carbon;

  $dateee = Carbon::now()->format('m');
  $current_year = Carbon::now()->format('Y');

  $current_month = $student->getMonth($dateee);

  $total_amount = 0;
  $total_paid = 0;
  $total_unpaid = 0;
@endphp
  <body style="font-family: sans-serif;padding:0px 15px;">

    <table class="table" style="width: 100%;margin-bottom: 0px;">
    <tbody>
      <tr>
        <td width="25%">
          <img src="{{asset('public/uploads/school/images/'.@$school->logo)}}" width="80" style="margin-bottom: 0px;">
         
        </td>
        <td style="vertical-align: middle;">
           <h1 style="font-size: 24px;"> {{@$school->name}}</h1>
        </td>
      
      </tr>
    </tbody>
  </table>
  <hr style="margin: 0;">

           <table style="width: 100%;margin: 0;">
             <td style="text-align: left;padding: 0;">
               <h4 style="font-style: italic;">Name : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$student->name}} <br> Guardian : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$student->guardian}} <br>Year : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$year}}</h4>
             </td>
           </table>
            <table class="table invoicetable" style="width: 100%;border-color: black;text-align: left;margin-top: 5px;">
              <thead align="left">
                <tr>
                  <th>Month</th>
                  <th>Submitted Date</th>
                  <th>Submitted To</th>
                  <th>Total Amount</th>
                  <th>Paid Amount</th>
                  <th>Unpaid Amount</th>
                  <th>Status</th>
                  <th>Remark</th>
                </tr>
              </thead>
              <tbody>
               @foreach($months as $month)
               <tr>
               <td>{{$month->name}}</td>
               @php $fee = $student->get_fee($student->id,$year,$month->id); @endphp
               <td>
                 {{$fee != null ? carbon::parse($fee->submitted_date)->format('d-M-Y') : '--'}}
               </td>
               <td>
                 {{$fee != null ? @$student->user->name : '--'}}
               </td>
               <td>
                 {{@$fee != null ? @$fee->total_amount : '--' }}
               </td>
               <td>
                 {{@$fee != null ? @$fee->paid_amount : '--' }}
               </td>
               <td>
                 {{@$fee != null ? @$fee->unpaid_amount : '--' }}
               </td>
               <td>
                 {{@$fee != null ? (@$fee->status == 1 ? 'Paid' : ($fee->status == 2 ? 'Partial Paid' : 'Unpaid')) : '--' }}
               </td>
               <td>
                 {{@$fee != null ? @$fee->remark : '--' }}
               </td>
               </tr>
               @if($current_month >= $month->id)
               @if($fee != null)
               @php
                  $total_amount += $fee->total_amount; 
                  $total_paid   += $fee->paid_amount;
                  $total_unpaid   += $fee->unpaid_amount;
               @endphp
               @elseif($fee == null && $current_month >= $month->id && $year <= $current_year)
               @php
                  $total_amount += ($student->student_class->fee * ((100 - $student->discount)/100)); 
                  $total_unpaid += ($student->student_class->fee * ((100 - $student->discount)/100)); 
               @endphp
               @endif
               @endif
               @endforeach
              </tbody>
            </table>
            <div style="font-size: 12px;">
            <h4>Total Amount : {{number_format($total_amount,2,'.',',')}}</h4>
            <h4>Total Paid : {{number_format($total_paid,2,',',',')}}</h4>
            <h4>Total Unpaid : {{number_format($total_unpaid,2,'.',',')}}</h4>
            </div>

  </body>
</html>