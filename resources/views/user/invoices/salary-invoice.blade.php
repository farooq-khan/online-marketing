  <!DOCTYPE html>
  <html>
  <head>
    <meta charset="utf-8"/>
    <title>Invoice</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    
    <style type="text/css">
      @page {size: auto;   /* auto is the initial value */
        margin: 5mm;}
    	  table { font-size: 10px; }
    table tr td 
    {
      vertical-align: top; padding-bottom: 3px;
    }
    table 
    {
      border-collapse: collapse;
      font-size:12px;
      border-spacing: 0px;}
      .invoicetable tr td, .invoicetable tr th {
      border: 1px solid black;
      padding: 4px 7px;
    }
    .main-table > tbody > tr > td  { padding-right: 10px; padding-left: 10px;  }

    .invoicetable tr.inv-total-tr td {
        border: none;
        padding: 10px 2px 5px;
    }
    .inv-total-td span {
        font-weight: bold;
        border-bottom: 2px solid #000;
        display: inline-block;
        padding: 0px 5px 2px;
    }
    </style>

  </head>
@php
  use Carbon\Carbon;
@endphp
  <body style="font-family: sans-serif;padding:15px;">

    <table class="table" style="width: 100%;margin-bottom: 0px;">
    <tbody>
      <tr>
        <td width="25%">
          <img src="{{asset('public/uploads/school/images/'.@$school->logo)}}" width="150" style="margin-bottom: 0px;">
         
        </td>
        <td style="vertical-align: middle;">
           <h1 style="font-size: 34px;"> {{@$school->name}}</h1>
        </td>
      
      </tr>
    </tbody>
  </table>
  <hr>

           <table style="width: 100%;">
             <td style="text-align: center;">
               <h2 style="font-style: italic;">Salary Slip</h2>
             </td>
           </table>
            <table class="table invoicetable" style="width: 100%;border-color: black;text-align: left;margin-top: 20px;">
              <thead align="left">
                <tr>
                  <th>Name</th>
                  <th>Invoice No.</th>
                  <th>Phone</th>
                  <th>Salary</th>
                  <th>Paid Amount</th>
                  <th>Unpaid Amount</th>
                  <th>Bonus (%)</th>
                  <th>Bonus Amount</th>
                  <th>Salary Month</th>
                  <th>Submitted Date</th>
                  <th>Status</th>
                </tr>
              </thead>
             
              <tbody>
                @foreach($salaries as $salary)
               <tr>
                <td>{{$salary->teacher->name}}</td>
                <td>Invoice_{{$salary->id}}</td>
                <td>{{$salary->teacher->phone}}</td>
                <td>{{$salary->total_amount}}</td>
                <td>{{$salary->paid_amount}}</td>
                <td>{{$salary->unpaid_amount != null ? $salary->unpaid_amount : '--'}}</td>
                <td>{{$salary->bonus != null ? $salary->bonus.' %' : '--'}}</td>
                <td>{{$salary->bonus_amount != null ? $salary->bonus_amount : '--'}}</td>
                <td>{{$salary->salary_month != null ? carbon::parse($salary->salary_month)->format('M') : '--'}}</td>
                <td>{{$salary->created_at != null ? carbon::parse($salary->created_at)->format('d-m-Y') : '--'}}</td>
                <td>{{$salary->status == 1 ? 'Paid' : 'Partial Paid'}}</td>
               </tr>
               @endforeach
              </tbody>
            </table>

      
    <div style="position: fixed;bottom: 100px;padding: 0 20px;">
      <div style="border-top: 1px solid black;">
      <p style="font-size: 12px;margin-top: 10px;"><b>Address :</b> {{@$school->address}}</p>

          <p style="font-size: 12px;margin-top: 10px;"><b>Contact :</b> {{@$school->phone}}</p>
      </div>
    </div>
      
  </body>
</html>