  <!DOCTYPE html>
  <html>
  <head>
    <meta charset="utf-8"/>
    <title>Invoice</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    
    <style type="text/css">
      @page {size: auto;   /* auto is the initial value */
        margin: 5mm;}
    	  table { font-size: 10px; }
    table tr td 
    {
      vertical-align: top; padding-bottom: 3px;
    }
    table 
    {
      border-collapse: collapse;
      font-size:12px;
      border-spacing: 0px;}
      .invoicetable tr td, .invoicetable tr th {
      border: 1px solid black;
      padding: 4px 7px;
    }
    .main-table > tbody > tr > td  { padding-right: 10px; padding-left: 10px;  }

    .invoicetable tr.inv-total-tr td {
        border: none;
        padding: 10px 2px 5px;
    }
    .inv-total-td span {
        font-weight: bold;
        border-bottom: 2px solid #000;
        display: inline-block;
        padding: 0px 5px 2px;
    }
    </style>

  </head>
@php
  use Carbon\Carbon;
@endphp
  <body style="font-family: sans-serif;padding:15px;">

    <table class="table" style="width: 100%;margin-bottom: 0px;">
    <tbody>
      <tr>
        <td width="25%">
          <img src="{{asset('public/uploads/school/images/'.@$school->logo)}}" width="150" style="margin-bottom: 0px;">
         
        </td>
        <td style="vertical-align: middle;">
           <h1 style="font-size: 34px;"> {{@$school->name}}</h1>
        </td>
      
      </tr>
    </tbody>
  </table>
  <hr>

           <table style="width: 100%;">
             <td style="text-align: center;">
               <h2 style="font-style: italic;">Fee Slip</h2>
             </td>
           </table>
<!--            <table style="width: 40%;">
            <tr>
             <td style="">
              <span>Name</span>
             </td>
             <td>{{$student_fee[0]->student->name}}</td>
             </tr>

             <tr>
             <td style="">
               <span>Father Name</span>
             </td>
             <td>{{$student_fee[0]->student->guardian}}</td>
             </tr>
           </table> -->
            <table class="table invoicetable" style="width: 100%;border-color: black;text-align: left;margin-top: 5px;">
              <thead align="left">
                <tr>
                  <th>Adm No.</th>
                  <th>Name</th>
                  <th>Father Name</th>
                  <th>Receipt No.</th>
                  <th>Class</th>
                  <th>Submitted Date</th>
                  <th>Fee Month</th>
                  <th>Total Amount</th>
                  <th>Discount</th>
                  <th>
                    Paid Amount
                  </th>
                  <th>Unpaid Amount</th>
                  <th>Status</th>
                </tr>
              </thead>
             
              <tbody>
                @foreach($student_fee as $fee)
               <tr>
                 <td width="5%">{{$fee->student->roll_no}}</td>
                 <td>{{$fee->student->name}}</td>
                 <td>{{$fee->student->guardian}}</td>
                 <td>{{$fee->receipt != null ? $fee->receipt : '--'}}</td>
                 <td>{{$fee->student->student_class->class_name}}</td>
                 <td>{{carbon::parse($fee->submitted_date)->format('d-M-Y')}}</td>
                 <td>{{carbon::parse($fee->fee_month)->format('M')}}</td>
                 <td>{{$fee->total_amount}}</td>
                 <td>{{$fee->discount}}%</td>
                 <td>{{$fee->paid_amount != null ? $fee->paid_amount : 0}}</td>
                 <td>{{$fee->unpaid_amount != null ? $fee->unpaid_amount : 0}}</td>
                 <td>{{$fee->status == 1 ? 'Paid' : 'Partial Paid'}}</td>
               </tr>
               @endforeach
              </tbody>
            </table>

      
<!--     <div style="position: fixed;bottom: 100px;padding: 0 20px;">
      <div style="border-top: 1px solid black;">
      <p style="font-size: 12px;margin-top: 10px;"><b>Address :</b> {{@$school->address}}</p>

          <p style="font-size: 12px;margin-top: 10px;"><b>Contact :</b> {{@$school->phone}}</p>
      </div>
    </div> -->
      
  </body>
</html>