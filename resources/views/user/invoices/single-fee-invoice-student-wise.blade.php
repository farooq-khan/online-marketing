  <!DOCTYPE html>
  <html>
  <head>
    <meta charset="utf-8"/>
    <title>Invoice</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    
    <style type="text/css">
    *{
      margin: 0;
      padding: 0;
    }
    	  table { font-size: 10px; }
    table tr td 
    {
      vertical-align: top; padding-bottom: 3px;
    }
    table 
    {
      border-collapse: collapse;
      font-size:12px;
      border-spacing: 0px;}
      .invoicetable tr td, .invoicetable tr th {
      border: 1px solid black;
      padding: 4px 7px;
    }
    .main-table > tbody > tr > td  { padding-right: 10px; padding-left: 10px;  }

    .invoicetable tr.inv-total-tr td {
        border: none;
        padding: 10px 2px 5px;
    }
    .inv-total-td span {
        font-weight: bold;
        border-bottom: 2px solid #000;
        display: inline-block;
        padding: 0px 5px 2px;
    }
    .fee-table tr td{
      border: 1px solid black;
      padding-left: 5px;
    }
    .custom-padding > div:nth-child(odd)
    {
      padding-right: 10px;
    }
    .custom-padding > div:nth-child(even)
    {
      padding-left: 10px;
    }
    </style>

  </head>
@php
  use Carbon\Carbon;
  use App\StudentFee;
  use App\OtherFee;
  use App\SoldBooksReport;
  $i = 0;
@endphp
  <body style="font-family: sans-serif;padding:20px;width: 100%;">
<div style="width: 100%;display: table" class="custom-padding">
@foreach($student_fee as $fee)
@php $i++; @endphp
  <div style="display: table-cell;width: 100%">
  <table class="table" style="width: 100%;margin-bottom: 0px;border: 1px solid black;">
    <tbody>
      <tr>
        <td width="10%" height="60px" style="vertical-align: middle;text-align: center;">
          <img src="{{asset('public/uploads/gif/resize_logo.png')}}" height="50px" style="margin-bottom: 0px;margin-left: 15px;">
         
        </td>
        <td style="vertical-align: middle;text-align: center;">
           <h3> {{@$school->name}}</h3>
           <h4>{{@$school->sub_title}}</h4>
           <h6 ><span style="background-color: #ccc;font-size: 14px">{{carbon::parse($fee->fee_month)->format('F')}} Fee Slip</span></h6>
        </td>
      
      </tr>
    </tbody>
  </table>
  <hr>
  <table style="width: 100%;font-size: 12px;" class="table fee-table">
    <tr>
      <td>
        <b>Name :</b>
      </td>
      <td colspan="2">
        <b>{{$fee->student->name}}</b>
      </td>
    </tr>
    <tr>
      <td>
        <b>
          F/Name :
        </b>
      </td>
      <td colspan="2">
        <b>
          {{$fee->student->guardian}}
        </b>
      </td>
    </tr>
    <tr>
      <td>Adm. No : </td>
      <td>
        {{$fee->student->roll_no}}
      </td>
      <td style="text-align: center;">Class: {{$fee->student->student_class->class_name}}</td>
    </tr>
     <tr>
      <td>Contact No :</td>
      <td>
         {{$fee->student->guardian_phone}}
      </td>
      <td style="text-align: center;"><b>Paid Fee Detail</b></td>
    </tr>
    <tr>
      <td>Issue Date :</td>
      <td>
        01-{{carbon::parse($fee->fee_month)->format('M')}}-{{carbon::parse($fee->fee_month)->format('Y')}}
      </td>
      <td style="background-color: #ccc;padding: 0;">
        <table style="width: 100%" class="fee-table">
          <tr>
            <td width="33%" height="14px">
              <b>Month</b>
            </td>
            <td width="33%"><b>Monthly</b></td>
            <td>
              <b>Other</b>
            </td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td>Due Date :</td>
      <td>
        07-{{carbon::parse($fee->fee_month)->format('M')}}-{{carbon::parse($fee->fee_month)->format('Y')}}
      </td>
      @php 
         $single_student = $fee;
          // dd($single_student);

          $std = \App\Student::with(['student_fee' => function($q) use ($single_student){
            $q->where('class_id',$single_student->class_id)->where('fee_month','<',$single_student->fee_month);
          },

          'student_other_fee' => function($q) use ($single_student){
            $q->where('class_id',$single_student->class_id)->where('fee_month','<',$single_student->fee_month);
          },
          'student_other_fee_month' => function($q) use ($single_student){
            $q->where('class_id',$single_student->class_id)->whereMonth('fee_month',carbon::parse($single_student->fee_month)->format('m'))->whereYear('fee_month',carbon::parse($single_student->fee_month)->format('Y'));
          },'student_sold_books'

          ])->find($fee->student_id);

          $dues = @$std->student_fee->sum('unpaid_amount');

          $dues = StudentFee::where('student_id',$single_student->student_id)->where('fee_month','<',$single_student->fee_month)->orderBy('id','asc')->sum('unpaid_amount');

          $other_dues = @$std->student_other_fee->sum('unpaid_amount');
          $other_dues = OtherFee::where('student_id',$single_student->student_id)->where('fee_month','<',$single_student->fee_month)->orderBy('id','asc')->sum('unpaid_amount');

          $concession = @$std->student_fee->sum('concession');
          $concession = StudentFee::where('student_id',$single_student->student_id)->where('fee_month','<',$single_student->fee_month)->orderBy('id','asc')->sum('concession');

          $other_concession = @$std->student_other_fee->sum('concession');
          $other_concession = OtherFee::where('student_id',$single_student->student_id)->where('fee_month','<',$single_student->fee_month)->orderBy('id','asc')->sum('concession');

          $received = @$std->student_fee->sum('paid_amount');
          $received = StudentFee::where('student_id',$single_student->student_id)->where('fee_month','<',$single_student->fee_month)->orderBy('id','asc')->sum('paid_amount');
          $other_received = @$std->student_other_fee->sum('paid_amount');
          $other_received = OtherFee::where('student_id',$single_student->student_id)->where('fee_month','<',$single_student->fee_month)->orderBy('id','asc')->sum('paid_amount');

          $last_records = StudentFee::where('student_id',$single_student->student_id)->where('class_id',$single_student->class_id)->where('fee_month','<=',$single_student->fee_month)->orderBy('id','asc')->limit(12)->get();

          $other_fee_current_month = $std->student_other_fee_month->sum('amount');
          $other_fee_current_month = OtherFee::where('student_id',$single_student->student_id)->whereMonth('fee_month',carbon::parse($single_student->fee_month)->format('m'))->whereYear('fee_month',carbon::parse($single_student->fee_month)->format('Y'))->orderBy('id','asc')->sum('amount');

          $books_total = @$std->student_sold_books->sum('total_amount');
          $books_paid = @$std->student_sold_books->sum('paid_amount');

          $books_total = SoldBooksReport::where('student_id',$single_student->student_id)->orderBy('id','asc')->sum('total_amount');
          $books_paid = SoldBooksReport::where('student_id',$single_student->student_id)->orderBy('id','asc')->sum('paid_amount');

          $books_dues = number_format($books_total,2,'.',',');
      @endphp
      <td style="padding: 0px;">
         <table style="width: 100%" class="fee-table">
          <tr>
            <td width="33%" height="14px">
              {{ @$last_records[0] != null ? carbon::parse(@$last_records[0]->fee_month)->format('M') : ''}}
            </td>
            <td width="33%">{{@$last_records[0] != null ? number_format(@$last_records[0]->paid_amount,0,'.',',') : ''}}</td>
            <td>
              @php 
              if(@$last_records[0] != null)
              {
                $last_records_other = \App\OtherFee::where('student_id',$single_student->student_id)->where('class_id',$single_student->class_id)->whereMonth('fee_month',carbon::parse(@$last_records[0]->fee_month)->format('m'))->whereYear('fee_month',carbon::parse(@$last_records[0]->fee_month)->format('Y'))->pluck('paid_amount')->first();
              }
              else
              {
                $last_records_other = null;
              }
              @endphp
              @if($last_records_other != null)
              {{number_format($last_records_other,0,'.',',')}}
              @endif
            </td>
          </tr>
        </table>
        
      </td>
    </tr>
    <tr>
      <td style="background-color: #ccc;text-align: center;" colspan="2"><b>New Fee To Be Paid</b></td>
      <td style="padding: 0px;">
         <table style="width: 100%" class="fee-table">
          <tr>
            <td width="33%" height="14px">{{ @$last_records[1] != null ? carbon::parse(@$last_records[1]->fee_month)->format('M') : ''}}</td>
            <td width="33%">{{@$last_records[1] != null ? number_format(@$last_records[1]->paid_amount,0,'.',',') : ''}}</td>
            <td width="33%">
              @php 
              if(@$last_records[1] != null)
              {
                $last_records_other = \App\OtherFee::where('student_id',$single_student->student_id)->where('class_id',$single_student->class_id)->whereMonth('fee_month',carbon::parse(@$last_records[1]->fee_month)->format('m'))->whereYear('fee_month',carbon::parse(@$last_records[1]->fee_month)->format('Y'))->pluck('paid_amount')->first();
              }
              else
              {
                $last_records_other = null;
              }
              @endphp
              @if($last_records_other != null)
              {{number_format($last_records_other,0,'.',',')}}
              @endif
            </td>
          </tr>
        </table>
      </td>
    </tr>
     <tr>
      <td><b>Description</b></td>
      <td><b>Amount</b></td>
      <td style="padding: 0px;">
         <table style="width: 100%" class="fee-table">
          <tr>
            <td width="33%" height="14px">{{ @$last_records[2] != null ? carbon::parse(@$last_records[2]->fee_month)->format('M') : ''}}</td>
            <td width="33%">{{@$last_records[2] != null ? number_format(@$last_records[2]->paid_amount,0,'.',',') : ''}}</td>
            <td>
              @php 
              if(@$last_records[2] != null)
              {
                $last_records_other = \App\OtherFee::where('student_id',$single_student->student_id)->where('class_id',$single_student->class_id)->whereMonth('fee_month',carbon::parse(@$last_records[2]->fee_month)->format('m'))->whereYear('fee_month',carbon::parse(@$last_records[2]->fee_month)->format('Y'))->pluck('paid_amount')->first();
              }
              else
              {
                $last_records_other = null;
              }
              @endphp
              @if($last_records_other != null)
              {{number_format($last_records_other,0,'.',',')}}
              @endif
            </td>
          </tr>
        </table>
      </td>
    </tr>
     <tr>
      <td>Monthly Fee</td>
      <td>Rs. {{number_format($single_student->total_amount,0,'.',',')}}</td>
      <td style="padding: 0px;">
         <table style="width: 100%" class="fee-table">
          <tr>
            <td width="33%" height="14px">{{ @$last_records[3] != null ? carbon::parse(@$last_records[3]->fee_month)->format('M') : ''}}</td>
            <td width="33%">{{@$last_records[3] != null ? number_format(@$last_records[3]->paid_amount,0,'.',',') : ''}}</td>
            <td>
              @php 
              if(@$last_records[3] != null)
              {
                $last_records_other = \App\OtherFee::where('student_id',$single_student->student_id)->where('class_id',$single_student->class_id)->whereMonth('fee_month',carbon::parse(@$last_records[3]->fee_month)->format('m'))->whereYear('fee_month',carbon::parse(@$last_records[3]->fee_month)->format('Y'))->pluck('paid_amount')->first();
              }
              else
              {
                $last_records_other = null;
              }
              @endphp
              @if($last_records_other != null)
              {{number_format($last_records_other,0,'.',',')}}
              @endif
            </td>
          </tr>
        </table>
      </td>
    </tr>
     <tr>
      <td>Other Fee</td>
      <td>Rs. {{number_format($other_fee_current_month,0,'.',',')}}</td>
      <td style="padding: 0px;">
         <table style="width: 100%" class="fee-table">
          <tr>
            <td width="33%" height="14px">{{ @$last_records[4] != null ? carbon::parse(@$last_records[4]->fee_month)->format('M') : ''}}</td>
            <td width="33%">{{@$last_records[4] != null ? number_format(@$last_records[4]->paid_amount,0,'.',',') : ''}}</td>
            <td>
              @php 
              if(@$last_records[4] != null)
              {
                $last_records_other = \App\OtherFee::where('student_id',$single_student->student_id)->where('class_id',$single_student->class_id)->whereMonth('fee_month',carbon::parse(@$last_records[4]->fee_month)->format('m'))->whereYear('fee_month',carbon::parse(@$last_records[4]->fee_month)->format('Y'))->pluck('paid_amount')->first();
              }
              else
              {
                $last_records_other = null;
              }
              @endphp
              @if($last_records_other != null)
              {{number_format($last_records_other,0,'.',',')}}
              @endif
            </td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td>Due Tuition Fee</td>
      <td>Rs. {{number_format($dues,0,'.',',')}}</td>
      <td style="padding: 0px;">
         <table style="width: 100%" class="fee-table">
          <tr>
            <td width="33%" height="14px">{{ @$last_records[5] != null ? carbon::parse(@$last_records[5]->fee_month)->format('M') : ''}}</td>
            <td width="33%">{{@$last_records[5] != null ? number_format(@$last_records[5]->paid_amount,0,'.',',') : ''}}</td>
            <td>
              @php 
              if(@$last_records[5] != null)
              {
                $last_records_other = \App\OtherFee::where('student_id',$single_student->student_id)->where('class_id',$single_student->class_id)->whereMonth('fee_month',carbon::parse(@$last_records[5]->fee_month)->format('m'))->whereYear('fee_month',carbon::parse(@$last_records[5]->fee_month)->format('Y'))->pluck('paid_amount')->first();
              }
              else
              {
                $last_records_other = null;
              }
              @endphp
              @if($last_records_other != null)
              {{number_format($last_records_other,0,'.',',')}}
              @endif
            </td>
          </tr>
        </table>
      </td>
    </tr>
     <tr>
      <td>Due Other Fee : </td>
      <td>Rs. {{number_format($other_dues,0,'.',',')}}</td>
      <td style="padding: 0px;">
         <table style="width: 100%" class="fee-table">
          <tr>
            <td width="33%" height="14px">{{ @$last_records[6] != null ? carbon::parse(@$last_records[6]->fee_month)->format('M') : ''}}</td>
            <td width="33%">{{@$last_records[6] != null ? number_format(@$last_records[6]->paid_amount,0,'.',',') : ''}}</td>
            <td>
              @php 
              if(@$last_records[6] != null)
              {
                $last_records_other = \App\OtherFee::where('student_id',$single_student->student_id)->where('class_id',$single_student->class_id)->whereMonth('fee_month',carbon::parse(@$last_records[6]->fee_month)->format('m'))->whereYear('fee_month',carbon::parse(@$last_records[6]->fee_month)->format('Y'))->pluck('paid_amount')->first();
              }
              else
              {
                $last_records_other = null;
              }
              @endphp
              @if($last_records_other != null)
              {{number_format($last_records_other,0,'.',',')}}
              @endif
            </td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      
      <td>Total Dues :</td>
      <td>Rs. {{number_format($dues+$other_dues,0,'.',',')}}</td>
      <td style="padding: 0px;">
         <table style="width: 100%" class="fee-table">
          <tr>
            <td width="33%" height="14px">{{ @$last_records[7] != null ? carbon::parse(@$last_records[7]->fee_month)->format('M') : ''}}</td>
            <td width="33%">{{@$last_records[7] != null ? number_format(@$last_records[7]->paid_amount,0,'.',',') : ''}}</td>
            <td>
              @php 
              if(@$last_records[7] != null)
              {
                $last_records_other = \App\OtherFee::where('student_id',$single_student->student_id)->where('class_id',$single_student->class_id)->whereMonth('fee_month',carbon::parse(@$last_records[7]->fee_month)->format('m'))->whereYear('fee_month',carbon::parse(@$last_records[7]->fee_month)->format('Y'))->pluck('paid_amount')->first();
              }
              else
              {
                $last_records_other = null;
              }
              @endphp
              @if($last_records_other != null)
              {{number_format($last_records_other,0,'.',',')}}
              @endif
            </td>
          </tr>
        </table>
      </td>
    </tr>
     <tr>
      <td style="white-space: nowrap;">Discount So Far :</td>
      <td>Rs. {{number_format($concession,0,'.',',')}}</td>
      <td style="padding: 0px;">
         <table style="width: 100%" class="fee-table">
          <tr>
            <td width="33%" height="14px">{{ @$last_records[8] != null ? carbon::parse(@$last_records[8]->fee_month)->format('M') : ''}}</td>
            <td width="33%">{{@$last_records[8] != null ? number_format(@$last_records[8]->paid_amount,0,'.',',') : ''}}</td>
            <td>
              @php 
              if(@$last_records[8] != null)
              {
                $last_records_other = \App\OtherFee::where('student_id',$single_student->student_id)->where('class_id',$single_student->class_id)->whereMonth('fee_month',carbon::parse(@$last_records[8]->fee_month)->format('m'))->whereYear('fee_month',carbon::parse(@$last_records[8]->fee_month)->format('Y'))->pluck('paid_amount')->first();
              }
              else
              {
                $last_records_other = null;
              }
              @endphp
              @if($last_records_other != null)
              {{number_format($last_records_other,0,'.',',')}}
              @endif
            </td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
       <td>Paid So Far :</td>
       <td>Rs. {{number_format($received+$other_received,0,'.',',')}}</td>
      <td style="padding: 0px;">
         <table style="width: 100%" class="fee-table">
          <tr>
            <td width="33%" height="14px">{{ @$last_records[9] != null ? carbon::parse(@$last_records[9]->fee_month)->format('M') : ''}}</td>
            <td width="33%">{{@$last_records[9] != null ? number_format(@$last_records[9]->paid_amount,0,'.',',') : ''}}</td>
            <td>
              @php 
              if(@$last_records[9] != null)
              {
                $last_records_other = \App\OtherFee::where('student_id',$single_student->student_id)->where('class_id',$single_student->class_id)->whereMonth('fee_month',carbon::parse(@$last_records[9]->fee_month)->format('m'))->whereYear('fee_month',carbon::parse(@$last_records[9]->fee_month)->format('Y'))->pluck('paid_amount')->first();
              }
              else
              {
                $last_records_other = null;
              }
              @endphp
              @if($last_records_other != null)
              {{number_format($last_records_other,0,'.',',')}}
              @endif
            </td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td rowspan="2"><b>Total Payable <br>Fee So For : </b></td>
      <td rowspan="2" valign="middle"><b>Rs.</b> {{number_format(($dues + $other_dues + $single_student->unpaid_amount + $other_fee_current_month),0,'.',',')}}</td>
      <td style="padding: 0px;">
         <table style="width: 100%" class="fee-table">
          <tr>
            <td width="33%" height="14px">{{ @$last_records[10] != null ? carbon::parse(@$last_records[10]->fee_month)->format('M') : ''}}</td>
            <td width="33%">{{@$last_records[10] != null ? number_format(@$last_records[10]->paid_amount,0,'.',',') : ''}}</td>
            <td>
              @php 
              if(@$last_records[10] != null)
              {
                $last_records_other = \App\OtherFee::where('student_id',$single_student->student_id)->where('class_id',$single_student->class_id)->whereMonth('fee_month',carbon::parse(@$last_records[10]->fee_month)->format('m'))->whereYear('fee_month',carbon::parse(@$last_records[10]->fee_month)->format('Y'))->pluck('paid_amount')->first();
              }
              else
              {
                $last_records_other = null;
              }
              @endphp
              @if($last_records_other != null)
              {{number_format($last_records_other,0,'.',',')}}
              @endif
            </td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <!-- <td></td> -->
      <!-- <td> </td> -->
      <td style="padding: 0px;">
         <table style="width: 100%" class="fee-table">
          <tr style="height: 20px">
            <td width="33%" height="14px">{{ @$last_records[11] != null ? carbon::parse(@$last_records[11]->fee_month)->format('M') : ''}}</td>
            <td width="33%">{{@$last_records[11] != null ? number_format(@$last_records[11]->paid_amount,0,'.',',') : ''}}</td>
            <td>
              @php 
              if(@$last_records[11] != null)
              {
                $last_records_other = \App\OtherFee::where('student_id',$single_student->student_id)->where('class_id',$single_student->class_id)->whereMonth('fee_month',carbon::parse(@$last_records[11]->fee_month)->format('m'))->whereYear('fee_month',carbon::parse(@$last_records[11]->fee_month)->format('Y'))->pluck('paid_amount')->first();
              }
              else
              {
                $last_records_other = null;
              }
              @endphp
              @if($last_records_other != null)
              {{number_format($last_records_other,0,'.',',')}}
              @endif
            </td>
          </tr>
        </table>
      </td>
    </tr>
<!--      <tr>
      <td></td>
      <td></td>
      <td style="padding: 0px;">
         <table style="width: 100%" class="fee-table">
          <tr>
            <td width="50%" height="14px">{{ @$last_records[11] != null ? carbon::parse(@$last_records[11]->fee_month)->format('M') : ''}}</td>
            <td width="50%">{{@$last_records[11] != null ? number_format(@$last_records[11]->paid_amount,0,'.',',') : ''}}</td>
          </tr>
        </table>
      </td>
    </tr> -->
    <tr>
      <td colspan="3" style="text-align: center;padding: 10px 5px;position: relative;">
        
        @if(@$conf->fee_slip_notice == null)
        <img src="{{asset('public/assets/img/note.PNG')}}" width="40%" height="70px">
        @else
        <div style="width: 90%;height: 70px;">
        {{@$conf->fee_slip_notice}}
        </div>
        @endif
      </td>
    </tr>
  </table>
  </div>
  @if($i == 2)
  </div>
  <br>
  <div style="width: 100%;display: table" class="custom-padding">
    @php $i = 0; @endphp
  @endif
@endforeach    
</div>
  </body>
</html>