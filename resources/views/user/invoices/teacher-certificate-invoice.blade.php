<!DOCTYPE html>
<html>
<head>
  <title>School Leaving Certificate</title>
  <style type="text/css">
  	@page{
  		padding: 0 !important;
  		margin: 0 !important;
  		margin-bottom: 0 !important;
  	}
  </style>

</head>
<?php 
	use Carbon\Carbon;
?>
@foreach($teachers as $teacher)
<body style="background-color: #f7cee0;">
<div class="container" style="background-image: url({{asset('public/uploads/school/images/frames/'.$school->certificate_frame)}});background-position: center;background-size: cover;height: 770px;">
	<div style="position: relative;">
		<p style="position: absolute;right: 180px;margin-top: 40px;">Reg : {{@$school->registration}}</p>
	<table class="table" style="width: 100%;margin-bottom: 0px;margin-left: 280px;margin-top: 30px;align-items: center;">
    <tbody>
      <tr>
        <td width="10%">
          <img src="{{asset('public/uploads/school/images/'.@$school->logo)}}" width="100" style="margin-bottom: 0px;">
         
        </td>
        <td style="vertical-align: middle;">
           <h1 style="margin: 0;"> {{@$school->name}}</h1>
           <p style="margin: 0;padding:0;font-weight: 100;margin-left: 30px;">{{@$school->address}}</p>
           <p style="margin: 0;padding:0;position: absolute;right: 80px;top: 140px;">Cert : <span style="border-bottom: 1px solid black;">0900123123</span></p>
        </td>
      
      </tr>
    </tbody>
  </table>
  <div style="text-align: center;">
  	<h1 style="letter-spacing: 5px;font-size: 32px;">Experience Certificate</h1>
  </div>
  <div style="padding: 0 100px;">
  	<p style="font-size: 20px;line-height: 1.5;"> This is to certify that <b>{{$teacher->name}}</b> S/O <b>{{$teacher->guardian}}</b> has been teaching with us as a 
      @foreach($subje as $key => $subject)
      @if(count($subje) > 1)
      @if($loop->last)
      and {{$subject}} 
      @else
      @if((count($subje)-$key) != 2)
      {{$subject}},
      @else
      {{$subject}}
      @endif
      @endif 
      @else
      {{$subject}}
      @endif
      @endforeach
       Lecturer on visiting basis from <b>{{$teacher->joining_date}}</b> to <b>{{Carbon::now()->format('Y-m-d')}}</b>. His excellent research oriented teaching and command on the subject proved to be a great asset to the students as well as to the institute. he is an excellent teacher having a deep and wide ranging understanding of the subject.</p>
    <p style="font-size: 20px;line-height: 1.5;">He will prove himself an experience, diligent teacher and colleague, whenever he is placed.</p>

    <p style="font-size: 20px;line-height: 1.5;">We wish him all the success in his future life.</p>
  </div>

  <div style="position: absolute;bottom: 180px;margin-left: 150px;">
  	<span>Issued Date : <span style="border-bottom: 1px solid black;">{{Carbon::now()->format('Y-m-d')}}</span></span>

  	<span style="float: right;margin-right: 130px;">Principle : ____________________</span>
  </div>
	</div>
</div>
</body>
@endforeach
</html>