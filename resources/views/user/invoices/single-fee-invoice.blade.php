  <!DOCTYPE html>
  <html>
  <head>
    <meta charset="utf-8"/>
    <title>Invoice</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    
    <style type="text/css">
    *{
      margin: 0;
      padding: 0;
    }
    	  table { font-size: 10px; }
    table tr td 
    {
      vertical-align: top; padding-bottom: 3px;
    }
    table 
    {
      border-collapse: collapse;
      font-size:12px;
      border-spacing: 0px;}
      .invoicetable tr td, .invoicetable tr th {
      border: 1px solid black;
      padding: 4px 7px;
    }
    .main-table > tbody > tr > td  { padding-right: 10px; padding-left: 10px;  }

    .invoicetable tr.inv-total-tr td {
        border: none;
        padding: 10px 2px 5px;
    }
    .inv-total-td span {
        font-weight: bold;
        border-bottom: 2px solid #000;
        display: inline-block;
        padding: 0px 5px 2px;
    }
    .fee-table tr td{
      border: 1px solid black;
      padding-left: 5px;
    }
    </style>

  </head>
@php
  use Carbon\Carbon;
@endphp
  <body style="font-family: sans-serif;padding:20px;width: 100%;">
    <table class="table" style="width: 100%;margin: 0px;border: 1px solid black;">
    <tbody>
      <tr>
        <td width="10%" height="60px" style="vertical-align: middle;text-align: center;">
          <img src="{{asset('public/uploads/school/images/'.@$school->logo)}}" width="70px" style="margin-bottom: 0px;margin-left: 15px;">
         
        </td>
        <td style="vertical-align: middle;text-align: center;">
           <h1> {{@$school->name}}</h1>
           <h3>{{@$school->sub_title}}</h3>
           <h6 ><span style="background-color: #ccc;font-size: 14px">{{carbon::parse($last_records[0]->fee_month)->format('F')}} Fee Slip</span></h6>
        </td>
      
      </tr>
    </tbody>
  </table>
  <!-- <hr> -->
  <table style="width: 100%;" class="table fee-table">
    <tr>
      <td>
        <b>Name :</b>
      </td>
      <td colspan="2">
        <b>{{$last_records[0]->student->name}}</b>
      </td>
    </tr>
    <tr>
      <td>
        <b>
          F/Name :
        </b>
      </td>
      <td colspan="2">
        <b>
          {{$last_records[0]->student->guardian}}
        </b>
      </td>
    </tr>
    <tr>
      <td>Admission No : </td>
      <td width="10%">
        {{$last_records[0]->student->roll_no}}
      </td>
      <td style="text-align: center;">Class: {{$last_records[0]->student->student_class->class_name}}</td>
    </tr>
     <tr>
      <td>Contact No :</td>
      <td>
         {{$last_records[0]->student->guardian_phone}}
      </td>
      <td style="text-align: center;"><b>Paid Fee Detail</b></td>
    </tr>
    <tr>
      <td>Issue Date :</td>
      <td>
        01-{{carbon::parse($single_student->fee_month)->format('M')}}-{{carbon::parse($single_student->fee_month)->format('Y')}}
      </td>
      <td style="background-color: #ccc;padding: 0;">
        <table style="width: 100%" class="fee-table">
          <tr>
            <td width="33%" height="14px">
              <b>Month</b>
            </td>
            <td width="33%"><b>Monthly</b></td>
            <td>
              <b>Other</b>
            </td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td>Due Date :</td>
      <td>
        07-{{carbon::parse($single_student->fee_month)->format('M')}}-{{carbon::parse($single_student->fee_month)->format('Y')}}
      </td>
      <td style="padding: 0px;">
        <table style="width: 100%" class="fee-table">
          <tr>
            <td width="33%" height="14px">
              {{ @$last_records[0] != null ? carbon::parse(@$last_records[0]->fee_month)->format('M') : ''}}
            </td>
            <td width="33%">{{@$last_records[0] != null ? number_format(@$last_records[0]->paid_amount,2,'.',',') : ''}}</td>
            <td>
              @php 
              if(@$last_records[0] != null)
              {
                $last_records_other = \App\OtherFee::where('student_id',$single_student->student_id)->where('class_id',$single_student->class_id)->whereMonth('fee_month',carbon::parse(@$last_records[0]->fee_month)->format('m'))->whereYear('fee_month',carbon::parse(@$last_records[0]->fee_month)->format('Y'))->pluck('paid_amount')->first();
              }
              else
              {
                $last_records_other = null;
              }
              @endphp
              @if($last_records_other != null)
              {{number_format($last_records_other,2,'.',',')}}
              @endif
            </td>
          </tr>
        </table>
        <!-- {{$last_records[0]->student->guardian_phone}} -->
      </td>
    </tr>
     <tr>
      <td style="background-color: #ccc;text-align: center;" colspan="2"><b>New Fee To Be Paid</b></td>
      <td style="padding: 0px;">
         <table style="width: 100%" class="fee-table">
          <tr>
            <td width="33%" height="14px">{{ @$last_records[1] != null ? carbon::parse(@$last_records[1]->fee_month)->format('M') : ''}}</td>
            <td width="33%">{{@$last_records[1] != null ? number_format(@$last_records[1]->paid_amount,2,'.',',') : ''}}</td>
            <td width="33%">
              @php 
              if(@$last_records[1] != null)
              {
                $last_records_other = \App\OtherFee::where('student_id',$single_student->student_id)->where('class_id',$single_student->class_id)->whereMonth('fee_month',carbon::parse(@$last_records[1]->fee_month)->format('m'))->whereYear('fee_month',carbon::parse(@$last_records[1]->fee_month)->format('Y'))->pluck('paid_amount')->first();
              }
              else
              {
                $last_records_other = null;
              }
              @endphp
              @if($last_records_other != null)
              {{number_format($last_records_other,2,'.',',')}}
              @endif
            </td>
          </tr>
        </table>
      </td>
    </tr>
     <tr>
      <td><b>Description</b></td>
      <td><b>Amount</b></td>
      <td style="padding: 0px;">
         <table style="width: 100%" class="fee-table">
          <tr>
            <td width="33%" height="14px">{{ @$last_records[2] != null ? carbon::parse(@$last_records[2]->fee_month)->format('M') : ''}}</td>
            <td width="33%">{{@$last_records[2] != null ? number_format(@$last_records[2]->paid_amount,2,'.',',') : ''}}</td>
            <td>
              @php 
              if(@$last_records[2] != null)
              {
                $last_records_other = \App\OtherFee::where('student_id',$single_student->student_id)->where('class_id',$single_student->class_id)->whereMonth('fee_month',carbon::parse(@$last_records[2]->fee_month)->format('m'))->whereYear('fee_month',carbon::parse(@$last_records[2]->fee_month)->format('Y'))->pluck('paid_amount')->first();
              }
              else
              {
                $last_records_other = null;
              }
              @endphp
              @if($last_records_other != null)
              {{number_format($last_records_other,2,'.',',')}}
              @endif
            </td>
          </tr>
        </table>
      </td>
    </tr>
     <tr>
      <td>Monthly Fee</td>
      <td>Rs. {{number_format($single_student->total_amount,2,'.',',')}}</td>
      <td style="padding: 0px;">
         <table style="width: 100%" class="fee-table">
          <tr>
            <td width="33%" height="14px">{{ @$last_records[3] != null ? carbon::parse(@$last_records[3]->fee_month)->format('M') : ''}}</td>
            <td width="33%">{{@$last_records[3] != null ? number_format(@$last_records[3]->paid_amount,2,'.',',') : ''}}</td>
            <td>
              @php 
              if(@$last_records[3] != null)
              {
                $last_records_other = \App\OtherFee::where('student_id',$single_student->student_id)->where('class_id',$single_student->class_id)->whereMonth('fee_month',carbon::parse(@$last_records[3]->fee_month)->format('m'))->whereYear('fee_month',carbon::parse(@$last_records[3]->fee_month)->format('Y'))->pluck('paid_amount')->first();
              }
              else
              {
                $last_records_other = null;
              }
              @endphp
              @if($last_records_other != null)
              {{number_format($last_records_other,2,'.',',')}}
              @endif
            </td>
          </tr>
        </table>
      </td>
    </tr>
     <tr>
      <td>Other Fee</td>
      <td>Rs. {{number_format($other_fee_current_month,2,'.',',')}}</td>
      <td style="padding: 0px;">
         <table style="width: 100%" class="fee-table">
          <tr>
            <td width="33%" height="14px">{{ @$last_records[4] != null ? carbon::parse(@$last_records[4]->fee_month)->format('M') : ''}}</td>
            <td width="33%">{{@$last_records[4] != null ? number_format(@$last_records[4]->paid_amount,2,'.',',') : ''}}</td>
            <td>
              @php 
              if(@$last_records[4] != null)
              {
                $last_records_other = \App\OtherFee::where('student_id',$single_student->student_id)->where('class_id',$single_student->class_id)->whereMonth('fee_month',carbon::parse(@$last_records[4]->fee_month)->format('m'))->whereYear('fee_month',carbon::parse(@$last_records[4]->fee_month)->format('Y'))->pluck('paid_amount')->first();
              }
              else
              {
                $last_records_other = null;
              }
              @endphp
              @if($last_records_other != null)
              {{number_format($last_records_other,2,'.',',')}}
              @endif
            </td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <!-- <td>Stationary</td> -->
      <!-- <td>Rs. {{number_format($books_dues,2,'.',',')}}</td> -->
      <td>Due Tuition Fee</td>
      <td>Rs. {{number_format($dues,2,'.',',')}}</td>
      <td style="padding: 0px;">
         <table style="width: 100%" class="fee-table">
          <tr>
            <td width="33%" height="14px">{{ @$last_records[5] != null ? carbon::parse(@$last_records[5]->fee_month)->format('M') : ''}}</td>
            <td width="33%">{{@$last_records[5] != null ? number_format(@$last_records[5]->paid_amount,2,'.',',') : ''}}</td>
            <td>
              @php 
              if(@$last_records[5] != null)
              {
                $last_records_other = \App\OtherFee::where('student_id',$single_student->student_id)->where('class_id',$single_student->class_id)->whereMonth('fee_month',carbon::parse(@$last_records[5]->fee_month)->format('m'))->whereYear('fee_month',carbon::parse(@$last_records[5]->fee_month)->format('Y'))->pluck('paid_amount')->first();
              }
              else
              {
                $last_records_other = null;
              }
              @endphp
              @if($last_records_other != null)
              {{number_format($last_records_other,2,'.',',')}}
              @endif
            </td>
          </tr>
        </table>
      </td>
    </tr>
     <tr>
      <!-- <td>Dues So For : </td> -->
      <!-- <td>Rs. {{number_format($dues + $other_dues,2,'.',',')}}</td> -->
      <td>Due Other Fee : </td>
      <td>Rs. {{number_format($other_dues,2,'.',',')}}</td>
      <td style="padding: 0px;">
         <table style="width: 100%" class="fee-table">
          <tr>
            <td width="33%" height="14px">{{ @$last_records[6] != null ? carbon::parse(@$last_records[6]->fee_month)->format('M') : ''}}</td>
            <td width="33%">{{@$last_records[6] != null ? number_format(@$last_records[6]->paid_amount,2,'.',',') : ''}}</td>
            <td>
              @php 
              if(@$last_records[6] != null)
              {
                $last_records_other = \App\OtherFee::where('student_id',$single_student->student_id)->where('class_id',$single_student->class_id)->whereMonth('fee_month',carbon::parse(@$last_records[6]->fee_month)->format('m'))->whereYear('fee_month',carbon::parse(@$last_records[6]->fee_month)->format('Y'))->pluck('paid_amount')->first();
              }
              else
              {
                $last_records_other = null;
              }
              @endphp
              @if($last_records_other != null)
              {{number_format($last_records_other,2,'.',',')}}
              @endif
            </td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      
      <td>Total Dues :</td>
      <td>Rs. {{number_format($dues+$other_dues,2,'.',',')}}</td>
      <td style="padding: 0px;">
         <table style="width: 100%" class="fee-table">
          <tr>
            <td width="33%" height="14px">{{ @$last_records[7] != null ? carbon::parse(@$last_records[7]->fee_month)->format('M') : ''}}</td>
            <td width="33%">{{@$last_records[7] != null ? number_format(@$last_records[7]->paid_amount,2,'.',',') : ''}}</td>
            <td>
              @php 
              if(@$last_records[7] != null)
              {
                $last_records_other = \App\OtherFee::where('student_id',$single_student->student_id)->where('class_id',$single_student->class_id)->whereMonth('fee_month',carbon::parse(@$last_records[7]->fee_month)->format('m'))->whereYear('fee_month',carbon::parse(@$last_records[7]->fee_month)->format('Y'))->pluck('paid_amount')->first();
              }
              else
              {
                $last_records_other = null;
              }
              @endphp
              @if($last_records_other != null)
              {{number_format($last_records_other,2,'.',',')}}
              @endif
            </td>
          </tr>
        </table>
      </td>
    </tr>
     <tr>
      <td>Discount So Far :</td>
      <td>Rs. {{number_format($concession,2,'.',',')}}</td>
      <td style="padding: 0px;">
         <table style="width: 100%" class="fee-table">
          <tr>
            <td width="33%" height="14px">{{ @$last_records[8] != null ? carbon::parse(@$last_records[8]->fee_month)->format('M') : ''}}</td>
            <td width="33%">{{@$last_records[8] != null ? number_format(@$last_records[8]->paid_amount,2,'.',',') : ''}}</td>
            <td>
              @php 
              if(@$last_records[8] != null)
              {
                $last_records_other = \App\OtherFee::where('student_id',$single_student->student_id)->where('class_id',$single_student->class_id)->whereMonth('fee_month',carbon::parse(@$last_records[8]->fee_month)->format('m'))->whereYear('fee_month',carbon::parse(@$last_records[8]->fee_month)->format('Y'))->pluck('paid_amount')->first();
              }
              else
              {
                $last_records_other = null;
              }
              @endphp
              @if($last_records_other != null)
              {{number_format($last_records_other,2,'.',',')}}
              @endif
            </td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
       <td>Paid So Far :</td>
       <td>Rs. {{number_format($received+$other_received,2,'.',',')}}</td>
      <td style="padding: 0px;">
         <table style="width: 100%" class="fee-table">
          <tr>
            <td width="33%" height="14px">{{ @$last_records[9] != null ? carbon::parse(@$last_records[9]->fee_month)->format('M') : ''}}</td>
            <td width="33%">{{@$last_records[9] != null ? number_format(@$last_records[9]->paid_amount,2,'.',',') : ''}}</td>
            <td>
              @php 
              if(@$last_records[9] != null)
              {
                $last_records_other = \App\OtherFee::where('student_id',$single_student->student_id)->where('class_id',$single_student->class_id)->whereMonth('fee_month',carbon::parse(@$last_records[9]->fee_month)->format('m'))->whereYear('fee_month',carbon::parse(@$last_records[9]->fee_month)->format('Y'))->pluck('paid_amount')->first();
              }
              else
              {
                $last_records_other = null;
              }
              @endphp
              @if($last_records_other != null)
              {{number_format($last_records_other,2,'.',',')}}
              @endif
            </td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td></td>
      <td></td>
      <td style="padding: 0px;">
         <table style="width: 100%" class="fee-table">
          <tr>
            <td width="33%" height="14px">{{ @$last_records[10] != null ? carbon::parse(@$last_records[10]->fee_month)->format('M') : ''}}</td>
            <td width="33%">{{@$last_records[10] != null ? number_format(@$last_records[10]->paid_amount,2,'.',',') : ''}}</td>
            <td>
              @php 
              if(@$last_records[10] != null)
              {
                $last_records_other = \App\OtherFee::where('student_id',$single_student->student_id)->where('class_id',$single_student->class_id)->whereMonth('fee_month',carbon::parse(@$last_records[10]->fee_month)->format('m'))->whereYear('fee_month',carbon::parse(@$last_records[10]->fee_month)->format('Y'))->pluck('paid_amount')->first();
              }
              else
              {
                $last_records_other = null;
              }
              @endphp
              @if($last_records_other != null)
              {{number_format($last_records_other,2,'.',',')}}
              @endif
            </td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td><b>Total Payable <br>Fee So For : </b></td>
      <td><b>Rs.</b>
        {{number_format($single_student->unpaid_amount + @$other_fee_current_month + $dues + $other_dues,2,'.',',')}}
       </td>
      <td style="padding: 0px;">
         <table style="width: 100%" class="fee-table">
          <tr style="height: 20px">
            <td width="33%" height="14px">{{ @$last_records[11] != null ? carbon::parse(@$last_records[11]->fee_month)->format('M') : ''}}</td>
            <td width="33%">{{@$last_records[11] != null ? number_format(@$last_records[11]->paid_amount,2,'.',',') : ''}}</td>
            <td>
              @php 
              if(@$last_records[11] != null)
              {
                $last_records_other = \App\OtherFee::where('student_id',$single_student->student_id)->where('class_id',$single_student->class_id)->whereMonth('fee_month',carbon::parse(@$last_records[11]->fee_month)->format('m'))->whereYear('fee_month',carbon::parse(@$last_records[11]->fee_month)->format('Y'))->pluck('paid_amount')->first();
              }
              else
              {
                $last_records_other = null;
              }
              @endphp
              @if($last_records_other != null)
              {{number_format($last_records_other,2,'.',',')}}
              @endif
            </td>
          </tr>
        </table>
      </td>
    </tr>
    <!--  <tr>
      <td></td>
      <td></td>
      <td style="padding: 0px;">
         <table style="width: 100%" class="fee-table">
          <tr>
            <td width="50%" height="14px">{{ @$last_records[11] != null ? carbon::parse(@$last_records[11]->fee_month)->format('M') : ''}}</td>
            <td width="50%">{{@$last_records[11] != null ? number_format(@$last_records[11]->paid_amount,2,'.',',') : ''}}</td>
          </tr>
        </table>
      </td>
    </tr> -->
    <tr>
      <td colspan="3" style="text-align: center;padding: 10px 5px;position: relative;">
        @if(@$conf->fee_slip_notice == null)
        <img src="{{asset('public/assets/img/note.PNG')}}" width="90%">
        @else
        {{@$conf->fee_slip_notice}}
        @endif
      </td>
    </tr>
  </table>
  <!-- <span style="position: absolute;z-index: 1000;bottom: 30px;left: 40px;">{{@$school->phone}}</span> -->

<!--   <table class="" style="width: 100%;margin: 10px 0px;">
              <tbody>
                @foreach($student_fee as $fee)
                <tr>
                  <td colspan="2" style="text-align: center;font-style: italic;font-weight: bold;font-size: 16px;padding-bottom: 10px;">Fee Slip</td>
                </tr>
                <tr>
                  <td>Admission No.</td>
                  <td>{{$fee->student->roll_no}}</td>
                </tr>

                <tr>
                  <td>Invoice No.</td>
                  <td>Invoice_{{$fee->id}}</td>
                </tr>
                
                <tr>
                  <td>Name</td>
                  <td>{{$fee->student->name}}</td>
                </tr>
                
                <tr>
                  <td>Father/Guardian</td>                 
                  <td>{{$fee->student->guardian}}</td>
                </tr>

                <tr>  
                  <td>Class</td>
                  <td>{{$fee->student->student_class->class_name}}</td>
                </tr>

                <tr>
                  <td>Submitted Date</td>
                  <td>{{carbon::parse($fee->submitted_date)->format('d-M-Y')}}</td>
                </tr>

                <tr>
                  <td>Fee Month</th>
                  <td>{{carbon::parse($fee->fee_month)->format('M')}}</td>
                </tr>

                <tr>
                  <td>Total Amount</td>
                  <td>{{$fee->total_amount}}</td>
               </tr>

               <tr>
                  <td>Discount</td>
                  <td>{{$fee->discount != null ? $fee->discount : 0}}%</td>
               </tr>

                <tr>
                  <td>Paid Amount</td>
                  <td>{{$fee->paid_amount}}</td>
               </tr>
               <tr>
                  <td>Unpaid Amount</td>
                  <td>{{$fee->unpaid_amount != null ? $fee->unpaid_amount : 0}}</td>
               </tr>

               <tr>
                  <td>Status</td>
                  <td>{{$fee->status == 1 ? 'Paid' : 'Partial Paid'}}</td>
               </tr>
               @endforeach
              </tbody>
            </table> -->

           <!--  <hr>
          <p style="font-size: 12px;margin-top: 10px;"><b>Address :</b> {{@$school->address}}</p>

          <p style="font-size: 12px;margin-top: 10px;"><b>Contact :</b> {{@$school->phone}}</p> -->
  
<!--       </tbody>
    </table> -->
   <!--  <div style="position: fixed;bottom: 200px;padding: 0 70px;">
      <div style="border-top: 1px solid black;">
      <p>The invoice was automatically created by: Carish OÜ</p>
      </div>
    </div> -->
      
  </body>
</html>