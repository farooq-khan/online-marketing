  <!DOCTYPE html>
  <html>
  <head>
    <meta charset="utf-8"/>
    <title>Invoice</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    
    <style type="text/css">
      @page {size: auto;   /* auto is the initial value */
        margin: 5mm;}

        .table_header tr td
        {
          /*border: 1px solid black;*/
          padding: 5px 5px;
        }

        .table_header
        {
          border-collapse: collapse;
          font-size: 12px;
        }

        .table_header_2 tr td
        {
          border: 1px solid black;
        }

        .table_header_2 tr th
        {
          border: 1px solid black;
        }

    </style>

  </head>
@php
  use Carbon\Carbon;
  $i = 0;
@endphp
  <body style="font-family: sans-serif;padding:0px;">
    <table style="width: 100%">
    <tr>
      <td>
    <table border="1" bordercolor="#aaa" cellspacing="0" style="width: 100%;margin-bottom: 10px;" class="table_header">
      <tr>
        <td width="30%" style="" align="center" style="padding: 5px;">
          <img align="center" src="{{asset('public/uploads/school/images/'.@$school->logo)}}" width="70">
        </td>
        <td width="50%" valign="top">{{$school->name}}<br><small style="color: #888;">{{$school->address}}</small></td>
      </tr>
      <tr>
        <!-- <td>Report</td> -->
        <td colspan="2" align="center">Books Invoice {{$history->id}}</td>
      </tr>
      <tr>
        <td colspan="2">Student Name : {{$student->name}}</td>
      </tr>
       <tr>
        <td colspan="2">Father Name : {{$student->guardian}}</td>
      </tr>

       <tr>
        <td >Class : {{$history->student_class->class_name}}</td>
        <td>Roll No. {{$student->roll_no}}</td>
      </tr>

      <tr>
        <td >Issue Date</td>
        <td>{{$history->created_at->format('d-m-Y')}}</td>
      </tr>
      <tr>
        <td colspan="2" style="padding: 0;">
          <table bordercolor="#aaa" cellspacing="0" style="width: 100%;" class="table_header_2">
            <tr>
                <th>S.No</th>
                <th>Subjects</th>
                <th>Price</th>
            </tr>
            @foreach($subjects as $sub)
            <tr>
              <td>{{$loop->iteration}}</td>
              <td>{{$sub->name}}</td>
              <td>{{$sub->selling_price != null ? $sub->selling_price : ''}}</td>
            </tr>
            @endforeach
            <tr>
              <td colspan="2" align="right">Total</td>
              <td>{{$history->total_amount}}</td>
            </tr>

            <tr>
              <td colspan="2" align="right">Paid Amount</td>
              <td>{{$history->paid_amount}}</td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
    </td>
    </tr>
    </table>
  </body>
</html>