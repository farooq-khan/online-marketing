  <!DOCTYPE html>
  <html>
  <head>
    <meta charset="utf-8"/>
    <title>Invoice</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    
    <style type="text/css">
      @page {size: auto;   /* auto is the initial value */
        margin: 5mm;}

        .table_header tr td
        {
          /*border: 1px solid black;*/
          padding: 2px 5px;
        }

        .table_header
        {
          border-collapse: collapse;
          font-size: 12px;
        }

        .table_header_2 tr td
        {
          border: 1px solid black;
          font-size: 10px;
        }

        .table_header_2 tr th
        {
          border: 1px solid black;
        }

    </style>

  </head>
@php
  use Carbon\Carbon;
  $i = 0;
@endphp
  <body style="font-family: sans-serif;padding:0px;">
    <table>
      <tr>
        
      
    @foreach($student_result as $result)
    @php $i++; @endphp
    <td width="33%">
    <table border="1" bordercolor="#aaa" cellspacing="0" style="width: 100%;margin-bottom: 10px;margin-right: 5px" class="table_header">
      <tr>
        <td width="30%" style="" align="center" style="padding: 5px;">
          <img align="center" src="{{asset('public/uploads/school/images/'.@$school->logo)}}" width="40">
        </td>
        <td width="50%" valign="top">{{$school->name}}<br><small style="color: #888;">{{$school->address}}</small></td>
      </tr>
      <tr>
        <td style="font-size: 8px;">Report</td>
        <td style="font-size: 8px;">{{$test->name}}</td>
      </tr>
      <tr>
        <td colspan="2" style="font-size: 8px;">Student Name : {{$result->student->name}}</td>
      </tr>
       <tr>
        <td colspan="2" style="font-size: 8px;">Father Name : {{$result->student->guardian}}</td>
      </tr>

       <tr>
        <td  style="font-size: 8px;">Class : {{$result->student_class->class_name}}</td>
        <td style="font-size: 8px;">Roll No. {{$result->student->roll_no}}</td>
      </tr>

      <tr>
        <td  style="font-size: 8px;">Week</td>
        <td style="font-size: 8px;">{{carbon::parse($result->from_test_date)->format('d-M')}} to {{carbon::parse($result->to_test_date)->format('d-M-Y')}}</td>
      </tr>

     <!--  <tr>
        <td >Issue Date</td>
        <td>{{carbon::now()->format('d-M-Y')}}</td>
      </tr> -->
      <tr>
        <td colspan="2" style="padding: 0;">
          @php
            $total = 0;
            $obtained = 0;
            $iteration= 0;
          @endphp
          <table bordercolor="#aaa" cellspacing="0" style="width: 100%;" class="table_header_2">
            <tr>
                <th>S.No</th>
                <th>Subjects</th>
                <th>T.M</th>
                <th>Ob.M</th>
            </tr>
            @foreach($subjects as $sub)
            @php
            $test_result = \App\TestResult::where('student_id',$result->student_id)->where('class_id',$result->class_id)->where('test_id',$result->test_id)->where('subject_id',$sub->id)->whereDate('from_test_date',$result->from_test_date)->first();
              $total += @$test_result->total_marks;
              $obtained += @$test_result->obtained_marks;
              $grade = '';
              $position = '';
              $percentage = $total != 0 ? round(($obtained / $total)*100,2) : 0;
              ++$iteration;
              if($percentage >= 90 && $percentage <=100){
                $grade = 'A1';
                $position = '1st';
              }elseif($percentage >= 80 && $percentage <=89){
                $grade = 'A+';
                $position = '2nd';
              }elseif($percentage >= 70 && $percentage <=79){
                $grade = 'A';
                $position = '3rd';
              }elseif($percentage >= 60 && $percentage <=69){
                $grade = 'B+';
                $position = '';
              }elseif($percentage >= 50 && $percentage <=59){
                $grade = 'B';
                $position = '';
              }elseif($percentage < 50){
                $grade = 'X';
                $position = '';
              }
            @endphp
            @if(@$test_result->obtained_marks != '')
            <tr>
              <td>{{$loop->iteration}}</td>
              <td>{{$sub->name}}</td>
              <td>{{$test_result != null ? $test_result->total_marks : $test->total_marks}}</td>
              @if($test_result->obtained_marks === '00')
              <td>A</td>
              @else
              <td>{{$test_result != null ? $test_result->obtained_marks : ''}}</td>
              @endif
            </tr>
            @endif
            @endforeach
            <tr>
              <td>{{++$iteration}}</td>
              <td align="center">Total</td>
              <td>{{$total}}</td>
              <td>{{$obtained}}</td>
            </tr>
            <tr>
              <td align="">{{++$iteration}}</td>
              <td align="center">Percentage</td>
              <td align="center" colspan="2">{{ $percentage }} %</td>
            </tr>
            <tr>
              <td align="">{{++$iteration}}</td>
              <td align="center">Grade-Position</td>
              <td colspan="2" align="center">{{$grade}}{{$position != '' ? ' - '.$position : ''}}</td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
    </td>
    @if($i % 3 == 0)
    </tr>
    <tr>
    @endif
    @endforeach
</tr>
    </table>
  </body>
</html>