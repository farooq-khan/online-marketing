  <!DOCTYPE html>
  <html>
  <head>
    <meta charset="utf-8"/>
    <title>Invoice</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    
    <style type="text/css">
      @page {size: auto;   /* auto is the initial value */
        margin: 5mm;}
    	  table { font-size: 10px; }
    table tr td 
    {
      vertical-align: top; padding-bottom: 3px;
    }
    table 
    {
      border-collapse: collapse;
      font-size:12px;
      border-spacing: 0px;}
      .invoicetable tr td, .invoicetable tr th {
      border: 1px solid black;
      padding: 4px 7px;
    }
    .main-table > tbody > tr > td  { padding-right: 10px; padding-left: 10px;  }

    .invoicetable tr.inv-total-tr td {
        border: none;
        padding: 10px 2px 5px;
    }
    .inv-total-td span {
        font-weight: bold;
        border-bottom: 2px solid #000;
        display: inline-block;
        padding: 0px 5px 2px;
    }
    </style>

  </head>
@php
  use Carbon\Carbon;
@endphp
  <body style="font-family: sans-serif;padding:0px 15px;">

    <table class="table" style="width: 100%;margin-bottom: 0px;">
    <tbody>
      <tr>
        <td width="25%">
          <img src="{{asset('public/uploads/school/images/'.@$school->logo)}}" width="80" style="margin-bottom: 0px;">
         
        </td>
        <td style="vertical-align: middle;">
           <h1 style="font-size: 24px;"> {{@$school->name}}</h1>
        </td>
      
      </tr>
    </tbody>
  </table>
  <hr style="margin: 0;">

           <table style="width: 100%;margin: 0;">
             <td style="text-align: center;padding: 0;">
               <h2 style="font-style: italic;">Attendance of {{$student->name}} S/D/O {{$student->guardian}} Year {{$year}}</h2>
             </td>
           </table>
            <table class="table invoicetable" style="width: 100%;border-color: black;text-align: left;margin-top: 5px;">
              <thead align="left">
                <tr>
                  <th>Day</th>
                 @foreach($months as $month)
                 <th>{{$month->name}}</th>
                 @endforeach
                </tr>
              </thead>
              @for($i = 1; $i < 32; $i++)
              @php $value = str_pad($i,2,"0",STR_PAD_LEFT); @endphp
              <tr>
              <td>{{$value}}</td>
              <td>
                {{$student->get_student_attendance($student->id,'01',$year,$student->class_id,$value)}}
              </td>
              <td>
                {{$student->get_student_attendance($student->id,'02',$year,$student->class_id,$value)}}
              </td>
              <td>
                {{$student->get_student_attendance($student->id,'03',$year,$student->class_id,$value)}}
              </td>
              <td>
                {{$student->get_student_attendance($student->id,'04',$year,$student->class_id,$value)}}
              </td>
              <td>
                {{$student->get_student_attendance($student->id,'05',$year,$student->class_id,$value)}}
              </td>
              <td>
                {{$student->get_student_attendance($student->id,'06',$year,$student->class_id,$value)}}
              </td>
              <td>
                {{$student->get_student_attendance($student->id,'07',$year,$student->class_id,$value)}}
              </td>
              <td>
                {{$student->get_student_attendance($student->id,'08',$year,$student->class_id,$value)}}
              </td>
              <td>
                {{$student->get_student_attendance($student->id,'09',$year,$student->class_id,$value)}}
              </td>
              <td>
                {{$student->get_student_attendance($student->id,'10',$year,$student->class_id,$value)}}
              </td>
              <td>
                {{$student->get_student_attendance($student->id,'11',$year,$student->class_id,$value)}}
              </td>
              <td>
                {{$student->get_student_attendance($student->id,'12',$year,$student->class_id,$value)}}
              </td>
              </tr>
              @endfor

              <tr>
                <td>Present</td>
                <td>{{$student->get_student_attendance_by_month($student->id,'01',$year,$student->class_id,'present')}}</td>
                <td>{{$student->get_student_attendance_by_month($student->id,'02',$year,$student->class_id,'present')}}</td>
                <td>{{$student->get_student_attendance_by_month($student->id,'03',$year,$student->class_id,'present')}}</td>
                <td>{{$student->get_student_attendance_by_month($student->id,'04',$year,$student->class_id,'present')}}</td>
                <td>{{$student->get_student_attendance_by_month($student->id,'05',$year,$student->class_id,'present')}}</td>
                <td>{{$student->get_student_attendance_by_month($student->id,'06',$year,$student->class_id,'present')}}</td>
                <td>{{$student->get_student_attendance_by_month($student->id,'07',$year,$student->class_id,'present')}}</td>
                <td>{{$student->get_student_attendance_by_month($student->id,'08',$year,$student->class_id,'present')}}</td>
                <td>{{$student->get_student_attendance_by_month($student->id,'09',$year,$student->class_id,'present')}}</td>
                <td>{{$student->get_student_attendance_by_month($student->id,'10',$year,$student->class_id,'present')}}</td>
                <td>{{$student->get_student_attendance_by_month($student->id,'11',$year,$student->class_id,'present')}}</td>
                <td>{{$student->get_student_attendance_by_month($student->id,'12',$year,$student->class_id,'present')}}</td>
              </tr>
              <tr>
                <td>Absent</td>
                <td>{{$student->get_student_attendance_by_month($student->id,'01',$year,$student->class_id,'absent')}}</td>
                <td>{{$student->get_student_attendance_by_month($student->id,'02',$year,$student->class_id,'absent')}}</td>
                <td>{{$student->get_student_attendance_by_month($student->id,'03',$year,$student->class_id,'absent')}}</td>
                <td>{{$student->get_student_attendance_by_month($student->id,'04',$year,$student->class_id,'absent')}}</td>
                <td>{{$student->get_student_attendance_by_month($student->id,'05',$year,$student->class_id,'absent')}}</td>
                <td>{{$student->get_student_attendance_by_month($student->id,'06',$year,$student->class_id,'absent')}}</td>
                <td>{{$student->get_student_attendance_by_month($student->id,'07',$year,$student->class_id,'absent')}}</td>
                <td>{{$student->get_student_attendance_by_month($student->id,'08',$year,$student->class_id,'absent')}}</td>
                <td>{{$student->get_student_attendance_by_month($student->id,'09',$year,$student->class_id,'absent')}}</td>
                <td>{{$student->get_student_attendance_by_month($student->id,'10',$year,$student->class_id,'absent')}}</td>
                <td>{{$student->get_student_attendance_by_month($student->id,'11',$year,$student->class_id,'absent')}}</td>
                <td>{{$student->get_student_attendance_by_month($student->id,'12',$year,$student->class_id,'absent')}}</td>
              </tr>
              <tr>
                <td>Leave</td>
                <td>{{$student->get_student_attendance_by_month($student->id,'01',$year,$student->class_id,'leave')}}</td>
                <td>{{$student->get_student_attendance_by_month($student->id,'02',$year,$student->class_id,'leave')}}</td>
                <td>{{$student->get_student_attendance_by_month($student->id,'03',$year,$student->class_id,'leave')}}</td>
                <td>{{$student->get_student_attendance_by_month($student->id,'04',$year,$student->class_id,'leave')}}</td>
                <td>{{$student->get_student_attendance_by_month($student->id,'05',$year,$student->class_id,'leave')}}</td>
                <td>{{$student->get_student_attendance_by_month($student->id,'06',$year,$student->class_id,'leave')}}</td>
                <td>{{$student->get_student_attendance_by_month($student->id,'07',$year,$student->class_id,'leave')}}</td>
                <td>{{$student->get_student_attendance_by_month($student->id,'08',$year,$student->class_id,'leave')}}</td>
                <td>{{$student->get_student_attendance_by_month($student->id,'09',$year,$student->class_id,'leave')}}</td>
                <td>{{$student->get_student_attendance_by_month($student->id,'10',$year,$student->class_id,'leave')}}</td>
                <td>{{$student->get_student_attendance_by_month($student->id,'11',$year,$student->class_id,'leave')}}</td>
                <td>{{$student->get_student_attendance_by_month($student->id,'12',$year,$student->class_id,'leave')}}</td>
              </tr>
              <tbody>
               
              </tbody>
            </table>

            <p> <b>Total Attendance : {{$month_total}}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Total Presents : {{$month_total_present}}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Total Absents : {{$month_total_absent}}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Total Leave : {{$month_total_leave}}</b></p>
      
  </body>
</html>