<!DOCTYPE html>
<html>
<head>
  <title>School Leaving Certificate</title>
  <style type="text/css">
  	@page{
  		padding: 0 !important;
  		margin: 0 !important;
  		margin-bottom: 0 !important;
  	}
  </style>

</head>
<?php 
	use Carbon\Carbon;
?>
@foreach($students as $student)
<body style="background-image: url({{asset('public/uploads/school/images/frames/'.$school->certificate_frame)}});background-position: center;background-size: cover;background-repeat:no-repeat;height: 760px;padding: 20px;">
<div class="container" >
	<div style="position: relative;">
		<p style="position: absolute;right: 180px;margin-top: 40px;">Reg : {{@$school->registration_no}}</p>
	<table class="table" style="width: 100%;margin-bottom: 0px;margin-left: 280px;margin-top: 30px;align-items: center;">
    <tbody>
      <tr>
        <td width="10%">
          <img src="{{asset('public/uploads/school/images/'.@$school->logo)}}" width="100" style="margin-bottom: 0px;">
         
        </td>
        <td style="vertical-align: middle;">
           <h1 style="margin: 0;"> {{@$school->name}}</h1>
           <p style="margin: 0;padding:0;font-weight: 100;margin-left: 30px;">{{@$school->address}}</p>
           <p style="margin: 0;padding:0;position: absolute;right: 80px;top: 140px;">Cert : <span style="border-bottom: 1px solid black;">0900123123</span></p>
        </td>
      
      </tr>
    </tbody>
  </table>
  <div style="text-align: center;">
  	<h1 style="letter-spacing: 5px;font-size: 32px;">School Leaving Certificate</h1>
  </div>
  <div style="padding: 0 100px;">
  	<p style="font-size: 20px;line-height: 1.5;"> It is certified that <b>{{$student->name}}</b> S/O <b>{{$student->guardian}}</b> is a bonafide student of this School. His Date of birth per school record is <b>{{$student->dob}}</b>. He/She has passed Class <b>{{$student->student_class->class_name}}</b>. I wish him/her a very bright future. This certificate is being issued upon the request of the above-named student for whatever legal purpose it may serve.</p>
  </div>

  <div style="position: absolute;bottom: 180px;margin-left: 150px;">
  	<span>Issued Date : <span style="border-bottom: 1px solid black;">{{Carbon::now()->format('Y-m-d')}}</span></span>

  	<span style="float: right;margin-right: 130px;">Principle : ____________________</span>
  </div>
	</div>
</div>
</body>
@endforeach
</html>