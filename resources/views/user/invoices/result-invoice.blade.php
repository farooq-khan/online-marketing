  <!DOCTYPE html>
  <html>
  <head>
    <meta charset="utf-8"/>
    <title>Invoice</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- <link href="//db.onlinewebfonts.com/c/c77b24ed6e38fde787d3c573f1f4265d?family=Clarendon+Blk+BT" rel="stylesheet" type="text/css"/> -->
    <style type="text/css">
      /*@import url(//db.onlinewebfonts.com/c/c77b24ed6e38fde787d3c573f1f4265d?family=Clarendon+Blk+BT);*/

      /*@font-face {font-family: "Clarendon Blk BT"; src: url("//db.onlinewebfonts.com/t/c77b24ed6e38fde787d3c573f1f4265d.eot"); src: url("//db.onlinewebfonts.com/t/c77b24ed6e38fde787d3c573f1f4265d.eot?#iefix") format("embedded-opentype"), url("//db.onlinewebfonts.com/t/c77b24ed6e38fde787d3c573f1f4265d.woff2") format("woff2"), url("//db.onlinewebfonts.com/t/c77b24ed6e38fde787d3c573f1f4265d.woff") format("woff"), url("//db.onlinewebfonts.com/t/c77b24ed6e38fde787d3c573f1f4265d.ttf") format("truetype"), url("//db.onlinewebfonts.com/t/c77b24ed6e38fde787d3c573f1f4265d.svg#Clarendon Blk BT") format("svg"); }*/
      body{
        font-family: 'Times';
      }
      @page {size: auto;   /* auto is the initial value */
        margin: 5mm;}
    	  table { font-size: 10px; }
    table tr td 
    {
      vertical-align: top; padding-bottom: 3px;
    }
    table 
    {
      border-collapse: collapse;
      font-size:12px;
      border-spacing: 0px;}
      .invoicetable tr td, .invoicetable tr th {
      border: 1px solid black;
      padding: 4px 7px;
    }
    .main-table > tbody > tr > td  { padding-right: 10px; padding-left: 10px;  }

    .invoicetable tr.inv-total-tr td {
        border: none;
        padding: 10px 2px 5px;
    }
    .inv-total-td span {
        font-weight: bold;
        border-bottom: 2px solid #000;
        display: inline-block;
        padding: 0px 5px 2px;
    }
    .std_info tr td 
    {
      border: 1px solid #ccc;
      padding: 5px;
      color: white;
    }
    .record_table thead
    {
      background-color: #ccc;
    }
    .record_table tr td
    {
      border: 0.3px solid #aaa !important;
    }
     .record_table tr th
    {
      border: 0.3px solid #888 !important;
    }
    ol li
    {
      padding: 3px 0;
    }
    /*h2{
      font-family: 'Clarendon BT';
    font-weight: '900'
    }*/
    .green{
      color: #70ad47;
    }
    .green-bg{
      background-color: #70ad47;
    }
    .blue-bg{
      background-color: #4472c4;
    }
    .all_exams tr td 
    {
      border: 1px solid white;
      color: black;
    }
    .all_exams tr th 
    {
      border: 1px solid white;
      color: white;
    }
    .even_td{
      background-color: #c5e0b3;
    }
    .odd_td{
      background-color: #e2efd9;
    }
    .yellow_bg{
      background-color: #ffd966;
    }
    </style>

  </head>
@php
  use Carbon\Carbon;
  use App\Student;
  $std_obj = new Student;
@endphp
  <body style="font-family: sans-serif;padding:15px;">
    <div style="width: 100%;text-align: center;">
      <img src="{{asset('public/uploads/school/images/'.@$school->logo)}}" width="80" style="margin-bottom: 0px;">
      <p style="margin: 0;text-transform: uppercase;font-weight: normal;font-size: 14px;color: #679DFB">BEHIND EVERY GREAT ACHIEVEMENT THERE IS A BURNING DESIRE</p>
      <h2 style="margin: 0;color: red;font-family: 'Clarendon Blk BT';
    font-weight: '900';text-transform: uppercase;">{{@$school->name}}</h2>
      <table style="width: 100%;text-align: center;">
        <tr>
          <td>Registration No : <b>{{$student_result->student->roll_no}}</b></td>
          <td>BISE Registration N0 : <b>{{$school->registration_no}}</b></td>
        </tr>
      </table>
      <p style="margin: 0;text-transform: uppercase;margin-top: 10px;font-weight: 900;" class="green">Student's Progress Report</p>
    </div>
    <hr style="height: 2px;background-color: black;">
    <table style="width: 100%;margin-top: 15px;" class="std_info">
      <tr>
        <td width="70%" colspan="2" class="blue-bg">Student Name : <b>{{$student_result->student->name}}</b></td>
        <td width="30%" style="border-bottom: none;position: relative;z-index: 1;overflow: auto;" rowspan="4">
          @if($student->image != null)
          <img src="{{public_path('uploads/students/images/'.$student->image)}}" style="width: 95%;position: absolute;top: 0;height: 142px;z-index: 1000;overflow: auto;">
          @else
            <img src="{{public_path('assets/img/'.$student->gender.'.jpg')}}" style="width: 95%;position: absolute;top: 0;height: 142px;z-index: 1000;overflow: auto;">
          @endif
        </td>
      </tr> 
      <tr>
        <td width="70%" colspan="2" class="blue-bg">Father Name : <b>{{$student_result->student->guardian}}</b></td>
        <!-- <td width="30%" style="border-top: none;border-bottom: none;"></td> -->
      </tr>
      <tr>
        <td width="35%" class="blue-bg">Admission No : <b>{{$student_result->student->roll_no}}</b></td>
        <td width="35%" class="blue-bg">Roll No : <b>{{$student_result->student->id}}</b></td>
        <!-- <td width="30%" style="border-top: none;border-bottom: none;"></td> -->
      </tr>
      <tr>
        <td width="35%" class="blue-bg">Section : <b></b></td>
        <td width="35%" class="blue-bg">Class : <b>{{$student_result->student->student_class->class_name}}</b></td>
        <!-- <td width="30%" style="border-top: none;border-bottom: none;"></td> -->
      </tr>

    </table>

    <table style="width:100%;" class="all_exams">
      <thead>
        <tr>
          <th class="green-bg" width="5%"></th>
          <th class="green-bg" width="15%"></th>
          <th class="green-bg" colspan="4" width="24%">Obtained Marks Out of 50</th>
          <th class="green-bg" colspan="2" width="24.5%">Total Obtained Marks</th>
          <th style="background-color: white;border-top: none;border-right: 1px solid #ccc;z-index: 1;visibility: hidden;" width="35%"></th>
        </tr>
      </thead>
      <thead>
        <tr>
          <th class="green-bg" width="5%">S.NO.</th>
          <th class="green-bg" width="18%">Subjects</th>
          <th class="green-bg" width="6%">Spring Term</th>
          <th class="green-bg" width="6%">Summer Term</th>
          <th class="green-bg" width="6%">Fall Term</th>
          <th class="green-bg" width="6%">Winter Term</th>
          <th class="green-bg" width="6%">In Figures</th>
          <th class="green-bg" width="15.5%">In Words</th>
          <th class="green-bg" style="background-color: white;border-top: none;border-right: 1px solid #ccc;visibility: hidden;"></th>
        </tr>
      </thead>
      <tbody>
        @php
          $spring_obtained = 0;
          $summer_obtained = 0;
          $fall_obtained = 0;
          $winter_obtained = 0;

          $winter_sub_obtained = null;
          $fall_sub_obtained = null;
          $summer_sub_obtained = null;
          $spring_sub_obtained = null;

        @endphp
        @foreach($subjects as $sub)
        @php
          $check = @$subjects_result[0]->student_exam->name;
          if($check == 'Spring Term Exam'){
            $spring_sub_obtained = $sub->subject_exam_results()->where('student_id',$student_result->student_id)->whereHas('student_exam',function($ex){
              $ex->where('name','Spring Term Exam');
            })->pluck('obtained_marks')->first(); 
            $spring_obtained += intval($spring_sub_obtained);
          }elseif($check == 'Summer Term Exam'){
            $spring_sub_obtained = $sub->subject_exam_results()->where('student_id',$student_result->student_id)->whereHas('student_exam',function($ex){
              $ex->where('name','Spring Term Exam');
            })->pluck('obtained_marks')->first(); 
            $spring_obtained += intval($spring_sub_obtained);

            $summer_sub_obtained = $sub->subject_exam_results()->where('student_id',$student_result->student_id)->whereHas('student_exam',function($ex){
            $ex->where('name','Summer Term Exam');
            })->pluck('obtained_marks')->first();
            $summer_obtained += $summer_sub_obtained;
          }elseif($check == 'Fall Term Exam'){
            $spring_sub_obtained = $sub->subject_exam_results()->where('student_id',$student_result->student_id)->whereHas('student_exam',function($ex){
              $ex->where('name','Spring Term Exam');
            })->pluck('obtained_marks')->first(); 
            $spring_obtained += intval($spring_sub_obtained);

            $summer_sub_obtained = $sub->subject_exam_results()->where('student_id',$student_result->student_id)->whereHas('student_exam',function($ex){
            $ex->where('name','Summer Term Exam');
            })->pluck('obtained_marks')->first();
            $summer_obtained += $summer_sub_obtained;

            $fall_sub_obtained = $sub->subject_exam_results()->where('student_id',$student_result->student_id)->whereHas('student_exam',function($ex){
            $ex->where('name','Fall Term Exam');
            })->pluck('obtained_marks')->first();
            $fall_obtained += $fall_sub_obtained;

            $winter_sub_obtained = $sub->subject_exam_results()->where('student_id',$student_result->student_id)->whereHas('student_exam',function($ex){
              $ex->where('name','Winter Term Exam');
            })->pluck('obtained_marks')->first();
            $winter_obtained += $winter_sub_obtained;
          }elseif($check == 'Winter Term Exam'){
            $spring_sub_obtained = $sub->subject_exam_results()->where('student_id',$student_result->student_id)->whereHas('student_exam',function($ex){
                $ex->where('name','Spring Term Exam');
              })->pluck('obtained_marks')->first(); 
              $spring_obtained += intval($spring_sub_obtained);

            $summer_sub_obtained = $sub->subject_exam_results()->where('student_id',$student_result->student_id)->whereHas('student_exam',function($ex){
            $ex->where('name','Summer Term Exam');
            })->pluck('obtained_marks')->first();
            $summer_obtained += $summer_sub_obtained;

            $fall_sub_obtained = $sub->subject_exam_results()->where('student_id',$student_result->student_id)->whereHas('student_exam',function($ex){
              $ex->where('name','Fall Term Exam');
            })->pluck('obtained_marks')->first();
            $fall_obtained += $fall_sub_obtained;

            $winter_sub_obtained = $sub->subject_exam_results()->where('student_id',$student_result->student_id)->whereHas('student_exam',function($ex){
              $ex->where('name','Winter Term Exam');
            })->pluck('obtained_marks')->first();
            $winter_obtained += $winter_sub_obtained;
          }

          
        @endphp
        <tr class="{{$loop->iteration % 2 == 0 ? 'even_td' : 'odd_td'}}">
          <td style="background-color: #70ad47;text-align: center;color: white;" >{{$loop->iteration}}</td>
          <td>{{$sub->name}}</td>
          <td align="center">{{$spring_sub_obtained}}</td>
          <td align="center">{{$summer_sub_obtained}}</td>
          <td align="center">{{$fall_sub_obtained}}</td>
          <td align="center">{{$winter_sub_obtained}}</td>
          <td align="center">
            @php
              $total_ob_sub = $spring_sub_obtained + $summer_sub_obtained + $fall_sub_obtained + $winter_sub_obtained;

              $f = new NumberFormatter("en", NumberFormatter::SPELLOUT);
              $word = $f->format($total_ob_sub);
            @endphp
            {{$total_ob_sub}}
          </td>
          <td colspan="2" style="text-transform: capitalize;">{{str_replace('-', ' ', $word)}}</td>
        </tr>
        @endforeach
        <tr class="odd_td">
          @php 
            $spring_total = $spring_obtained != 0 ? $grand_total : 0;
            $summer_total = $summer_obtained != 0 ? $grand_total : 0;
            $fall_total = $fall_obtained != 0 ? $grand_total : 0;
            $winter_total = $winter_obtained != 0 ? $grand_total : 0;
            $total_marks = $spring_total + $summer_total + $fall_total + $winter_total;

            $f = new NumberFormatter("en", NumberFormatter::SPELLOUT);
              $word = $f->format($total_marks);
          @endphp
          <td style="background-color: #70ad47;text-align: center;color: white;">09</td>
          <td><b>Total Marks</b></td>
          <td align="center"><b>{{$spring_obtained != 0 ? $grand_total : ''}}</b></td>
          <td align="center"><b>{{$summer_obtained != 0 ? $grand_total : ''}}</b></td>
          <td align="center"><b>{{$fall_obtained != 0 ?   $grand_total : ''}}</b></td>
          <td align="center"><b>{{$winter_obtained != 0 ? $grand_total : ''}}</b></td>
          <td align="center">
            <b>{{$total_marks}}</b>
          </td>
          <td colspan="2" valign="bottom" style="text-transform: capitalize;">{{str_replace('-', ' ', $word)}}</td>
        </tr>
        <tr class="even_td">
          @php 
            $spring_obtained_total = $spring_obtained != 0 ? $spring_obtained : 0;
            $summer_obtained_total = $summer_obtained != 0 ? $summer_obtained : 0;
            $fall_obtained_total = $fall_obtained != 0 ? $fall_obtained : 0;
            $winter_obtained_total = $winter_obtained != 0 ? $winter_obtained : 0;
            $obtained_marks = $spring_obtained_total + $summer_obtained_total + $fall_obtained_total + $winter_obtained_total;

            $f = new NumberFormatter("en", NumberFormatter::SPELLOUT);
              $word = $f->format($obtained_marks);
          @endphp
          <td style="background-color: #70ad47;text-align: center;color: white;">10</td>
          <td><b>Obtained Marks</b></td>
          <td align="center"><b>{{$spring_obtained != 0 ? $spring_obtained : ''}}</b></td>
          <td align="center"><b>{{$summer_obtained != 0 ? $summer_obtained : ''}}</b></td>
          <td align="center"><b>{{$fall_obtained != 0 ? $fall_obtained : ''}}</b></td>
          <td align="center"><b>{{$winter_obtained != 0 ? $winter_obtained : ''}}</b></td>
          <td align="center"><b>{{$obtained_marks}}</b></td>
          <td colspan="2" rowspan="2" valign="bottom">{{str_replace('-', ' ', $word)}}</td>
        </tr>
        <tr class="odd_td">
          @php
            $spring_per = $spring_obtained != 0 ? round(($spring_obtained_total / $spring_total) * 100,2) : '';
            $summer_per = $summer_obtained != 0 ? round(($summer_obtained_total / $summer_total) * 100,2) : '';
            $fall_per = $fall_obtained != 0 ? round(($fall_obtained / $fall_total) * 100,2) : '';
            $winter_per = $winter_obtained != 0 ? round(($winter_obtained / $winter_total) * 100,2) : '';

            $overall_per = $total_marks != 0 ? round(($obtained_marks/$total_marks)*100,2) : '';
          @endphp
          <td style="background-color: #70ad47;text-align: center;color: white;">11</td>
          <td><b>Percentage</b></td>
          <td align="center" style="white-space: nowrap;font-size: 11px"><b>{{$spring_per != '' ? $spring_per.'%' : ''}}</b></td>
          <td align="center"><b>{{$summer_per != '' ? $summer_per.'%' : ''}}</b></td>
          <td align="center"><b>{{$fall_per != '' ? $fall_per.'%' : ''}}</b></td>
          <td align="center"><b>{{$winter_per != '' ? $winter_per.'%' : ''}}</b></td>
          <td align="center"><b>{{$overall_per != '' ? $overall_per.'%' : ''}}</b></td>
          <!-- <td colspan="2">Five Hundred twent</td> -->
        </tr>
        <tr class="even_td">
          <td style="background-color: #70ad47;text-align: center;color: white;">12</td>
          <td><b>Grade</b></td>
          <td align="center"><b>{{$std_obj->get_grade($spring_per)}}</b></td>
          <td align="center"><b>{{$std_obj->get_grade($summer_per)}}</b></td>
          <td align="center"><b>{{$std_obj->get_grade($fall_per)}}</b></td>
          <td align="center"><b>{{$std_obj->get_grade($winter_per)}}</b></td>
          <td align="center"><b>{{$std_obj->get_grade($overall_per)}}</b></td>
          <td colspan="2"></td>
        </tr>
        <tr class="odd_td">
          <td style="background-color: #70ad47;text-align: center;color: white;">13</td>
          <td><b>Position</b></td>
          <td align="center"><b>{{$std_obj->get_position($spring_per)}}</b></td>
          <td align="center"><b>{{$std_obj->get_position($summer_per)}}</b></td>
          <td align="center"><b>{{$std_obj->get_position($fall_per)}}</b></td>
          <td align="center"><b>{{$std_obj->get_position($winter_per)}}</b></td>
          <td align="center"><b>{{$std_obj->get_position($overall_per)}}</b></td>
          <td colspan="2"></td>
        </tr>
      </tbody>
    </table>
 @php
                $per = 90;
              @endphp
            <table style="width: 100%;margin-top: 15px;" class="yellow_bg">

              <tr>
                <td width="30%">Overall Percentage</td>
                <td width="70%">:<b style="text-decoration: underline;"> {{$overall_per}} %</b></td>
              </tr>
              <tr>
                <td width="30%">Grade & Position</td>
                <td width="70%">:<b style="text-decoration: underline;"> {{$std_obj->get_grade($overall_per)}} - {{$std_obj->get_position($overall_per)}}</b></td>
              </tr>
              <tr>
                <td width="30%">Class Teacher Remarks</td>
                <td width="70%">: <b style="text-decoration: underline;">
                @if($per >= 90)
                <span>Congratulations, The Result is Outstanding.</span>
                @elseif($per >= 80 && $per < 90)
                <span>Congratulations, The Result is Excellent.</span>
                @elseif($per >= 70 && $per < 80)
                <span>Well-done, Try More For Better.</span>
                @elseif($per >= 60 && $per < 70)
                <span>The Result is Good, But Try to Work Hard More.</span>
                @elseif($per >= 50 && $per < 60)
                <span>The Result is Just Satisfactory Try to Work Hard And Improve Your Result.</span>
                @elseif($per < 50)
                <span>The Result is Unsatisfactory Try to Work Hard And Improve Your Result.</span>
                @endif
              </b></td>
              </tr>
              <tr>
                <td width="30%">Class Teacher Name & Sign</td>
                <td width="70%">:<b style="text-decoration: underline;">{{@$student->student_class->class_teacher->name}}</b></td>
              </tr>
            </table>
            <table style="width: 100%;margin-top: 20px;font-size: 10px">
              <tr>
                <td width="33%" style="padding-top: 20px;">
                  <span>Date of Issue: <b>{{carbon::now()->format('d F Y')}}</b></span>
                </td>
                <td width="33%"><b>Controller of Examination Sign:</b> </td>
                <td width="33%"><b>Principal Sign:</b></td>
              </tr>
              <tr>
                <td colspan="3" height="20px"></td>
              </tr>
              <tr>
                <td>Place of Issue : <b>Dubai Adda</b></td>
                <td style="padding: 0 10px"><span style="border-bottom: 1px solid black;"><span style="visibility: hidden;">Controller of Examination Sign:</span></span></td>
                <td style="padding: 0 10px"><span style="border-bottom: 1px solid black;"><span style="visibility: hidden;">Controller of Examination Sign:</span></span></td>
              </tr>
            </table>
            <hr>

            <table style="width: 100%" style="font-size: 10px;" class="grading__rule">
              <tr>
                <td width="60%" style="padding-left: 30px;">
                  <ol>
                    <li> The overall result is uploaded on school f/b page.</li>
                    <li> Always visit the page <b>{{@$school->name}}</b> for any
                    type of updates.</li>
                    <li> Join the WhatsApp class group to get daily homework, Weekly
                    Progress report and Monthly progress report.</li>
                    <li> Feel free to contact anytime the school administration.</li>
                    <li> Visit <b>https://mentoreducationsystem.com/login</b> enter your Student id and
                    password and check the result.</li>
                  </ol>
                  <div><span><b>Note: </b><span style="font-size: 10px;font-weight: 100;">Errors and omissions are accepted. Any mistake in above particulars must
be intimated within two days</span></span></div>
                </td>
                <td width="40%">
                  <table class="table invoicetable record_table" style="width: 100%;margin-top: 5px;font-style: 10px">
                    <tr style="color: white;" class="blue-bg">
                      <th width="20%">Grade</th>
                      <th width="20%">Grade</th>
                      <th width="40%">Performance</th>
                      <th width="20%">Position</th>
                    </tr>
                    <tr>
                      <td>90-100</td>
                      <td>A1</td>
                      <td>Outstanding</td>
                      <td>1st</td>
                    </tr>
                    <tr>
                      <td>80-89</td>
                      <td>A+</td>
                      <td>Excellent</td>
                      <td>2nd</td>
                    </tr>
                    <tr>
                      <td>70-79</td>
                      <td>A</td>
                      <td>Very Good</td>
                      <td>3rd</td>
                    </tr>
                    <tr>
                      <td>60-69</td>
                      <td>B+</td>
                      <td>Good</td>
                      <td class="blue-bg" style="border-bottom: none;"></td>
                    </tr>
                    <tr>
                      <td>50-59</td>
                      <td>B</td>
                      <td>Satisfactory</td>
                      <td class="blue-bg" style="border-bottom: none;border-top: none;"></td>
                    </tr>
                    <tr>
                      <td>Below 50</td>
                      <td>X</td>
                      <td>Disqualified</td>
                      <td class="blue-bg"></td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
      
  </body>
</html>