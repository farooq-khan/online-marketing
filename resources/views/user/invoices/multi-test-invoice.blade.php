  <!DOCTYPE html>
  <html>
  <head>
    <meta charset="utf-8"/>
    <title>Invoice</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    
    <style type="text/css">
      @page {size: auto;   /* auto is the initial value */
        margin: 5mm;}
    	  table { font-size: 10px; }
    table tr td 
    {
      vertical-align: top; padding-bottom: 3px;
    }
    table 
    {
      border-collapse: collapse;
      font-size:12px;
      border-spacing: 0px;}
      .invoicetable tr td, .invoicetable tr th {
      border: 1px solid black;
      padding: 4px 7px;
    }
    .main-table > tbody > tr > td  { padding-right: 10px; padding-left: 10px;  }

    .invoicetable tr.inv-total-tr td {
        border: none;
        padding: 10px 2px 5px;
    }
    .inv-total-td span {
        font-weight: bold;
        border-bottom: 2px solid #000;
        display: inline-block;
        padding: 0px 5px 2px;
    }
    </style>

  </head>
@php
  use Carbon\Carbon;
  $m_total = 0;
  $m_obtained = 0;
@endphp
  <body style="font-family: sans-serif;padding:15px;">

    <table class="table" style="width: 100%;margin-bottom: 0px;">
    <tbody>
      <tr>
        <td width="25%">
          <img src="{{asset('public/uploads/school/images/'.@$school->logo)}}" width="150" style="margin-bottom: 0px;">
         
        </td>
        <td style="vertical-align: middle;">
           <h1 style="font-size: 34px;"> {{@$school->name}}</h1>
        </td>
      
      </tr>
    </tbody>
  </table>
  <hr>
   <table style="width: 100%;">
   <td style="text-align: center;">
     <h2 style="font-style: italic;">Test Result</h2>
   </td>
 </table>
  <table>
    <tr>
      <td><b>Student Name :</b> </td>
      <td>{{$student->name}}</td>
    </tr>
    <tr>
      <td><b>Father Name :</b> </td>
      <td>{{$student->guardian}}</td>
    </tr>
    <tr>
      <td><b>Class :</b> </td>
      <td>{{$student_class->class_name}}</td>
    </tr>
  </table>


            <table class="table invoicetable" style="width: 100%;border-color: black;text-align: left;margin-top: 20px;">
              <thead align="left">
                <tr>
                  <th>Test</th>
                  <th width="15%">From Date</th>
                  <th width="15%">To Date</th>
                  @foreach($subjects as $sub)
                  <th>{{$sub->name}}</th>
                  @endforeach
                  <th>Total Marks</th>
                  <th>Obtained Marks</th>
                </tr>
              </thead>
             
              <tbody>
                @foreach($query as $q)
                @php 
                  $total_marks = 0;
                  $obtained_marks = 0;
                @endphp
                  <tr>
                    <td>{{$q->student_test != null ? $q->student_test->name : ''}}</td>
                    <td>{{carbon::parse($q->from_test_date)->format('d-m-Y')}}</td>
                    <td>{{carbon::parse($q->to_test_date)->format('d-m-Y')}}</td>
                    @foreach($subjects as $sub)
                    <td>
                      @php 
                        $marks = $sub->subject_results != null ? $sub->subject_results()->where('student_id',$student->id)->where('class_id',$class_id)->where('test_id',$q->test_id)->whereDate('from_test_date',$q->from_test_date)->pluck('obtained_marks')->first() : '';

                        $total = $sub->subject_results != null ? $sub->subject_results()->where('student_id',$student->id)->where('class_id',$class_id)->where('test_id',$q->test_id)->whereDate('from_test_date',$q->from_test_date)->pluck('total_marks')->first() : '';
                        $total_marks += $total;
                        $obtained_marks += $marks;
                        $m_total += $total;
                        $m_obtained += $marks;
                      @endphp
                      {{$marks}}
                    </td>
                    @endforeach
                    <td>{{$total_marks}}</td>
                    <td>{{$obtained_marks}}</td>
                  </tr>
                @endforeach

              </tbody>
            </table>

      
    <div style="position: fixed;bottom: 100px;padding: 0 20px;">
      <div style="border-top: 1px solid black;">
      <p style="font-size: 12px;margin-top: 10px;"><b>Address :</b> {{@$school->address}}</p>

          <p style="font-size: 12px;margin-top: 10px;"><b>Contact :</b> {{@$school->phone}}</p>
      </div>
    </div>
      
  </body>
</html>