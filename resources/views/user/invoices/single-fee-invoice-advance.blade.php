  <!DOCTYPE html>
  <html>
  <head>
    <meta charset="utf-8"/>
    <title>Invoice</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    
    <style type="text/css">
    *{
      margin: 0;
      padding: 0;
    }
    	  table { font-size: 10px; }
    table tr td 
    {
      vertical-align: top; padding-bottom: 3px;
    }
    table 
    {
      border-collapse: collapse;
      font-size:12px;
      border-spacing: 0px;}
      .invoicetable tr td, .invoicetable tr th {
      border: 1px solid black;
      padding: 4px 7px;
    }
    .main-table > tbody > tr > td  { padding-right: 10px; padding-left: 10px;  }

    .invoicetable tr.inv-total-tr td {
        border: none;
        padding: 10px 2px 5px;
    }
    .inv-total-td span {
        font-weight: bold;
        border-bottom: 2px solid #000;
        display: inline-block;
        padding: 0px 5px 2px;
    }
    </style>

  </head>
@php
  use Carbon\Carbon;
@endphp
 @foreach($students as $student)
  <body style="font-family: sans-serif;padding:20px;width: 100%;">
    <table class="table" style="width: 100%;margin-bottom: 0px;">
    <tbody>
      <tr>
        <td width="10%">
          <img src="{{asset('public/uploads/school/images/'.@$school->logo)}}" width="60" style="margin-bottom: 0px;">
         
        </td>
        <td style="vertical-align: middle;">
           <h1> {{@$school->name}}</h1>
        </td>
      
      </tr>
    </tbody>
  </table>
  <hr>
  <table class="" style="width: 100%;margin: 10px 0px;">
              <tbody>
               
                <tr>
                  <td colspan="2" style="text-align: center;font-style: italic;font-weight: bold;font-size: 16px;padding-bottom: 10px;">Fee Slip</td>
                </tr>
                <tr>
                  <td>Admission No.</td>
                  <td>{{$student->roll_no}}</td>
                </tr>

                <tr>
                  <td>Invoice No.</td>
                  <td>Invoice_{{$student->id}}</td>
                </tr>
                
                <tr>
                  <td>Name</td>
                  <td>{{$student->name}}</td>
                </tr>
                
                <tr>
                  <td>Father/Guardian</td>                 
                  <td>{{$student->guardian}}</td>
                </tr>

                <tr>  
                  <td>Class</td>
                  <td>{{$student->student_class->class_name}}</td>
                </tr>

                <tr>
                  <td>Submitted Date</td>
                  <td>--</td>
                </tr>

                <tr>
                  <td>Fee Month</th>
                  <td>{{carbon::parse($fee_month)->format('F')}}</td>
                </tr>

                <tr>
                  <td>Total Amount</td>
                  <td>{{$student->fee}}</td>
               </tr>

               <tr>
                  <td>Discount</td>
                  <td>{{$student->discount != null ? $student->discount : 0}}%</td>
               </tr>

                <tr>
                  <td>Paid Amount</td>
                  <td>0</td>
               </tr>
               <tr>
                  <td>Unpaid Amount</td>
                  <td>{{$student->fee != null ? $student->fee : 0}}</td>
               </tr>

               <tr>
                  <td>Status</td>
                  <td>Unpaid</td>
               </tr>
       
              </tbody>
            </table>

            <hr>
          <p style="font-size: 12px;margin-top: 10px;"><b>Address :</b> {{@$school->address}}</p>

          <p style="font-size: 12px;margin-top: 10px;"><b>Contact :</b> {{@$school->phone}}</p>
  
      </tbody>
    </table>
   <!--  <div style="position: fixed;bottom: 200px;padding: 0 70px;">
      <div style="border-top: 1px solid black;">
      <p>The invoice was automatically created by: Carish OÜ</p>
      </div>
    </div> -->
      
  </body>
          @endforeach
</html>