@extends('user.layouts.layout')

@section('title','Library Books')

@section('content')
<style type="text/css">
   
</style>


                    <div class="row page-title-row">
                      <div class="col-md col-sm col page-title-col align-self-center">
                          <h1 class="page-title">Library Books</h1>
                      </div>
                      <div class="col-sm col-auto mt-sm-0 ml-auto text-right page-action-button">
                          <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addBook">+Add Book</button>
                      </div>    
                    </div>

                     <div class="pb-2">
                      <span style="color: red;font-style: italic;">Note : </span> <span style="font-style: italic;padding-left: 5px;"> Double Click The Record For Updating</span>
                    </div>


                    <div class="card">
                      <div class="card-body">
                          <div class="d-sm-flex justify-content-between">
                          <div class="delete-selected-item mb-4 d-none">
                            <a href="javascript:void(0);" class="btn selected-item-btn btn-sm delete_class deleteIcon" data-type="quotation" title="Delete Class" >
                            <i class="zmdi zmdi-delete" style="font-size: 25px;"></i></a>
                          </div>
                        </div>
                        <table class="table table-striped nowrap table-books">
                        <thead>
                        <tr>
                          <th>Title</th>
                          <th>Author</th>
                          <th>Edition</th>
                          <th>Price</th>
                          <th>Publisher</th>
                          <th>Book No.</th>
                          <th>Fund Type</th>
                          <th>No. of Pages</th>
                          <th>Subject</th>
                          <th>Total Quantity</th>
                          <th>Remaining Quantity</th>
                          <th>Created Date</th>
                          <th>Action</th>
                        </tr>
                        </thead>
                        </table>
                        </div>
                    </div>



<!--Add Student Modal -->
<div class="modal fade" id="addBook" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add Book</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form id="add-book-form" method="POST">
        @csrf
      <div class="modal-body ">
          <div class="form-row">
            <div class="form-group col-6">
                <label for="name">Book Title</label>
                <input type="text" name="title" id="title" class="form-control" placeholder="Book Title" required="required" />
            </div>

            <div class="form-group col-6">
                <label for="author">Author</label>
                <input type="text" name="author" id="author" class="form-control" placeholder="Book Author" />
            </div>

            <div class="form-group col-6">
                <label for="edition">Edition</label>
                <input type="text" name="edition" id="edition" class="form-control" placeholder="i.e 2nd Edition" />
            </div>


            <div class="form-group col-6">
                <label for="price">Price</label>
                <input type="number" name="price" id="price" class="form-control" placeholder="i.e 300" required="required" />
            </div>

            <div class="form-group col-6">
                <label for="publisher">Publisher</label>
                <input type="text" name="publisher" id="publisher" class="form-control" placeholder="Publisher..." required="required" />
            </div>

            <div class="form-group col-6">
                <label for="book_no">Book No.</label>
                <input type="text" name="book_no" id="book_no" class="form-control" placeholder="Book No..." required="required" />
            </div>

            <div class="form-group col-6">
                <label for="book_fund">Book Fund Type</label>
                <input type="text" name="book_fund" id="book_fund" class="form-control" placeholder="Book Fund Type..." required="required" />
            </div>

            <div class="form-group col-6">
                <label for="book_pages">Book Pages</label>
                <input type="number" name="book_pages" id="book_pages" class="form-control" placeholder="Book Pages..." required="required" />
            </div>

            <div class="form-group col-6">
                <label for="subject">Subject</label>
                <input type="text" name="subject" id="subject" class="form-control" placeholder="Book Pages..." required="required" />
            </div>

            <div class="form-group col-6">
                <label for="total_quantity">Quantity</label>
                <input type="text" name="total_quantity" id="total_quantity" class="form-control" placeholder="Book Total Quantity..." required="required" />
            </div>
          </div>
          
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary save-btn">Add</button>
      </div>
      </form>
    </div>
  </div>
</div>
@endsection

@section('javascript')
<script type="text/javascript">

    var table2 =  $('.table-books').DataTable({
    processing: true,
    searching: true,
    "language": {
    processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw" style="color:#13436c;"></i><span class="sr-only">Loading...</span> '},
    ordering: false,
    pageLength: {{50}},
    serverSide: true,
    autoWidth: false,
    "lengthMenu": [50,100,150,200],
    
    scrollX:true,
    scrollY : '90vh',
    scrollCollapse: true,
    ajax: 
    {
      beforeSend: function(){
        $('#loader_modal').modal({
          backdrop: 'static',
          keyboard: false
        });
        $('#loader_modal').modal('show');
      },
      url: "{!! route('get-library-books') !!}",
      data: function(data) {},
    },
    columns: [
      { data: 'book_title', name: 'book_title' },  
      { data: 'book_author', name: 'book_author' },  
      { data: 'book_edition', name: 'book_edition' },  
      { data: 'book_price', name: 'book_price' },  
      { data: 'publisher', name: 'publisher' },  
      { data: 'book_no', name: 'book_no' },  
      { data: 'book_type', name: 'book_type' },  
      { data: 'book_pages', name: 'book_pages' }, 
      { data: 'book_subject', name: 'book_subject' }, 
      { data: 'total_quantity', name: 'total_quantity' }, 
      { data: 'remaining_quantity', name: 'remaining_quantity' }, 
      { data: 'created_date', name: 'created_date' }, 
      { data: 'checkbox', name: 'checkbox' },  
    ],
    initComplete: function () {
    // Enable THEAD scroll bars
    $('.dataTables_scrollHead').css('overflow', 'auto');
    // Sync THEAD scrolling with TBODY
      $('.dataTables_scrollHead').on('scroll', function () {
          $('.dataTables_scrollBody').scrollLeft($(this).scrollLeft());
      });
    },
    drawCallback: function(){
      $('#loader_modal').modal('hide');
    },
  });

  $('#add-book-form').on('submit',function(e){
    e.preventDefault();

     e.preventDefault();
      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
          }
      });
       $.ajax({
          url: "{{ route('add-book-to-library') }}",
          dataType: 'json',
          type: 'post',
          data: new FormData(this),
          contentType: false,
          cache: false,
          processData:false,
          beforeSend: function(){
            $('.save-btn').addClass('disabled');
            $('.save-btn').attr('disabled', true);
            $('#loader_modal').modal({
              backdrop: 'static',
              keyboard: false
            });
            $('#loader_modal').modal('show');
          },
          success: function(result){
            $('.save-btn').attr('disabled', true);
            $('.save-btn').removeAttr('disabled');
            if(result.success == true){
              toastr.success('Success!', 'Book Added Successfully!!!',{"positionClass": "toast-bottom-right"});
              $('.table-books').DataTable().ajax.reload();
              $('#add-book-form')[0].reset();
              $('#addBook').modal('hide');
            }else{
              toastr.error('Error!', result.errormsg,{"positionClass": "toast-bottom-right"});
              $('#loader_modal').modal('hide');
            }

          },
          error: function (request, status, error) {
                /*$('.form-control').removeClass('is-invalid');
                $('.form-control').next().remove();*/
                $('#loader_modal').modal('hide');
                $('.save-btn').removeClass('disabled');
                $('.save-btn').removeAttr('disabled');
                json = $.parseJSON(request.responseText);
                $.each(json.errors, function(key, value){
                      $('input[name="'+key+'"]').after('<span class="invalid-feedback" role="alert"><strong>'+value+'</strong>');
                     $('input[name="'+key+'"]').addClass('is-invalid');
                     $('textarea[name="'+key+'"]').after('<span class="invalid-feedback" role="alert"><strong>'+value+'</strong>');
                     $('textarea[name="'+key+'"]').addClass('is-invalid');


                });
            }
        });
  });

  $(document).ready(function(){
    $(document).on("dblclick",".inputDoubleClick",function(){
    $(this).addClass('d-none');
    $(this).next().removeClass('d-none');
    $(this).next().addClass('active');
    $(this).next().focus();
  });

    $(document).on('keypress keyup focusout',".fieldFocus",function(e) {
    // alert('hi');
    var id = $(this).data('id');
    if (e.keyCode === 27 && $(this).hasClass('active')) {
      var fieldvalue = $(this).prev().data('fieldvalue');
      var thisPointer = $(this);
      thisPointer.addClass('d-none');
      thisPointer.val(fieldvalue);
      thisPointer.removeClass('active');
      thisPointer.prev().removeClass('d-none');
    }

    var name = $(this).attr('name');
    // alert(name);

    if( (e.keyCode === 13 || e.which === 0) && $(this).val().length > 0 && $(this).hasClass('active')){
      var str = $(this).val();
      if(($(this).val().length < 1 ||  !str.replace(/\s/g, '').length))
      {
        swal({ html:true, title:'Alert !!!', text:'<b>Please Enter At Least 1 Characters (this string may only contain white spaces)!!!</b>'});
        return false;
      }
      else
      {
        $(this).removeClass('active');
        var fieldvalue = $(this).prev().data('fieldvalue');
        var new_value = $(this).val();
        // alert(new_value);
        if(fieldvalue == new_value)
        {
          var thisPointer = $(this);
          thisPointer.addClass('d-none');
          thisPointer.prev().removeClass('d-none');
          $(this).prev().html(fieldvalue);
        }
        else
        {
          var thisPointer = $(this);
          thisPointer.addClass('d-none');
          thisPointer.prev().removeClass('d-none');
          if(new_value != '')
          {
            $(this).prev().html(new_value);
          }
          $(this).prev().data('fieldvalue', new_value);
          saveData(thisPointer,thisPointer.attr('name'), thisPointer.val(),fieldvalue,id);
        }
      }
    }

  });

      function saveData(thisPointer,field_name,field_value,new_select_value,id){

      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
      });
      $.ajax({
        method: "post",

        url: "{{ url('user/update-book-library') }}",
        dataType: 'json',
        data: 'id='+id+'&'+field_name+'='+encodeURIComponent(field_value)+'&'+'new_select_value'+'='+encodeURIComponent(new_select_value),
        beforeSend: function(){
          $('#loader_modal').modal({
            backdrop: 'static',
            keyboard: false
          });
          $('#loader_modal').modal('show');
        },
        success: function(data)
        {
          $("#loader_modal").modal('hide');
          if(data.success == true)
          {
            
            toastr.success('Success!', 'Information updated successfully.',{"positionClass": "toast-bottom-right"});
            return true;
          }

          if(field_name == 'stock')
          {
              $('.table-books').DataTable().ajax.reload();
              $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
          }

        },

      });
    }

      $(document).on('click','.delete_stock',function(){
      var id = $(this).data('id');

        swal({
            title: "Alert!",
            html: true,
            text: "<b>Are you sure!!! You want to delete this record !!!</b>",
            type: "info",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes!",
            cancelButtonText: "No!",
            closeOnConfirm: true,
            closeOnCancel: false
          },
            function(isConfirm) {
            if (isConfirm){
             $.ajaxSetup({
              headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
              }
            });
            $.ajax({
              url: "{{ route('delete-book-library') }}",
              method: 'post',
              data: {id:id},
              beforeSend: function(){
                $('#loader_modal').modal({
                  backdrop: 'static',
                  keyboard: false
                });
                $('#loader_modal').modal('show');
              },
              success: function (response) {
                if(response.success == false)
                {
                  toastr.warning('Sorry!', 'Cannot delete this Record, as it is already issued to students!!!',{"positionClass": "toast-bottom-right"});
                  $('#loader_modal').modal('hide');
                }

                if(response.success == true)
                {
                  toastr.success('Success!', 'Book Deleted Successfully!!!',{"positionClass": "toast-bottom-right"});
                  $('.table-books').DataTable().ajax.reload();
                  $('#loader_modal').modal('hide');
                }
               
              },
              error: function(request, status, error){
                $('#loader_modal').modal('hide');
              }
            });
            }
            else{
              swal("Cancelled", "", "error");
              
            }
          });
  });

  });
</script>
@stop

