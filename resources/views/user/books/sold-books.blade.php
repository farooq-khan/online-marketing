@extends('user.layouts.layout')

@section('title','All Sold Books')

@section('content')
<style type="text/css">
   
</style>

                    <div class="row page-title-row">
                        <div class="col-md col-sm col page-title-col align-self-center">
                            <h1 class="page-title">All Sold Books</h1>
                        </div> 
                        <div class="col-sm col-auto mt-sm-0 ml-auto text-right selected-item catalogue-btn-group d-none">
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#soldBook">+Sold Book</button>
                        </div> 
                    </div>

                    <div class="pb-2">
                      <span style="color: red;font-style: italic;">Note : </span> <span style="font-style: italic;padding-left: 5px;"> Double Click The Record For Updating</span>
                    </div>

                    <div class="card">
                        <div class="card-body">
                          <div class="row mb-0 form-row row-cols-md-3 row-cols-sm-3 row-cols-2">
                            <div class="form-group col">
                              <select class="form-control form-control student_classes_select state-tags" name="warehouse_id" >
                                <option value="">-- All Classes --</option>
                                  @foreach($classes as $class)
                                  <option value="{{$class->id}}"> {{$class->class_name}} </option>
                                  @endforeach
                              </select>
                            </div>
                            <div class="form-group col">
                              <select class="form-control form-control stock_filter state-tags" name="status_filter" disabled="disabled">
                                <option value="">-- Select Status --</option>
                                  <option value="1" selected="selected">Available</option>
                                  <option value="0">Out of Stock</option>
                              </select>
                            </div>
                              <div class="form-group col ml-auto text-md-left text-right">
                                <button class="btn btn-primary px-4" type="reset" id="reset">Reset</button>  
                            </div>
                        </div>
                        <table class="table table-striped nowrap table-books">
                        <thead>
                        <tr>
                          <th class="noVis">
                              <div class="custom-control custom-checkbox custom-checkbox1 d-inline-block">
                                <input type="checkbox" class="custom-control-input check-all1" name="check_all" id="check-all">
                                <label class="custom-control-label" for="check-all"></label>
                              </div>
                            </th>
                            <th>Name</th>
                            <th>Author</th>
                            <th>Description</th>
                            <th>Class</th>
                            <th>Sale Price</th>
                            <th>Stock</th>
                            <th>Status</th>
                        </tr>
                        </thead>
                        </table>
                        </div>
                    </div>



<!--Add Student Modal -->
<!-- <div class="modal fade" id="addBook" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add Book Stock</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form id="add-book-form" method="POST">
        @csrf
      <div class="modal-body ">
          <div class="form-row">

             <div class="form-group col-lg-6">
                <label for="name">Class</label>
                <select class="form-control form-control select_class state-tags" name="book_class" >
                  <option value="">-- All Classes --</option>
                   @foreach($classes as $class)
                    <option value="{{$class->id}}"> {{$class->class_name}} </option>
                    @endforeach
                </select>
              </div>

               <div class="form-group col-md-6">
                <label for="roll_no"><i class="zmdi zmdi-account material-icons-name pr-2"> Admission No.</i></label>
                <select class="form-control-lg" id="student_admission_no" name="student_admission_no" style="display: block !important;width: 100%;height: 100px;padding: 10px !important;">
                  <option value="">--Select Student Admission No.--</option>
                </select>
            </div>

            <div class="form-group col-md-6">
                <label for="name">Book Name</label>
                <input type="text" name="name" id="name" class="form-control" placeholder="Book Name" required="required" />
            </div>

            <div class="form-group col-md-6">
                <label for="author">Author</label>
                <input type="text" name="author" id="author" class="form-control" placeholder="Book Author" />
            </div>

            <div class="form-group col-md-6">
                <label for="description">Description</label>
                <input type="text" name="description" id="description" class="form-control" placeholder="Description" />
            </div>

            <div class="form-group col-md-6">
              <label for="class_id">Class</label>
              <select class="form-control class_id" name="class_id" id="class_id" required="required">
                <option value="">-- All Classes --</option>
                 @foreach($classes as $class)
                  <option value="{{$class->id}}"> {{$class->class_name}} </option>
                  @endforeach
              </select>
            </div>

            <div class="form-group col-md-6">
                <label for="price">Purchasing Price</label>
                <input type="number" name="price" id="price" class="form-control" placeholder="Purchasing Price" required="required" />
            </div>

            <div class="form-group col-md-6">
                <label for="sale_price">Sale Price</label>
                <input type="number" name="sale_price" id="sale_price" class="form-control" placeholder="Sale Price" required="required" />
            </div>

            <div class="form-group col-md-6">
                <label for="stock">Stock</label>
                <input type="number" name="stock" id="stock" class="form-control" placeholder="Enter Stock i.e 50" required="required" />
            </div>
          </div>
          
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary save-btn">Add</button>
      </div>
      </form>
    </div>
  </div>
</div> -->

<!--Add Student Modal -->
<div class="modal fade" id="soldBook" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Sold Book(s)</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form id="sold-books-form" method="POST">
        @csrf
      <div class="modal-body pb-2">
        <input type="hidden" name="selected_books_ids" id="selected_books_ids">
          <div class="form-row">
            <div class="form-group col-sm-6 co-12">
              <label for="roll_no">Admission No.</label>
              <select class="form-control" id="student_admission_no" name="student_admission_no" style="display: block !important;width: 100%;height: 100px;padding: 10px !important;">
                <option value="">--Select Student Admission No.--</option>
              </select>
            </div>

            <div class="form-group col-sm-6 co-12">
                <label for="total_amount">Total Amount</label>
                <input type="text" name="total_amount" id="total_amount" value="0.00" class="form-control" placeholder="Total Amount" readonly="true" />
            </div>

            <div class="form-group col-sm-6 co-12">
                <label for="received_amount">Enter Received Amount</label>
                <input type="text" name="received_amount" id="received_amount" class="form-control" placeholder="Received Amount" />
            </div>

          </div>
          
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary save-btn">Submit</button>
      </div>
      </form>
    </div>
  </div>
</div>
@endsection

@section('javascript')
<script type="text/javascript">

    var table2 =  $('.table-books').DataTable({
    processing: true,
    searching: true,
    "language": {
    processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw" style="color:#13436c;"></i><span class="sr-only">Loading...</span> '},
    ordering: false,
    pageLength: {{50}},
    serverSide: true,
    autoWidth: false,
    "lengthMenu": [50,100,150,200],
    
    scrollX:true,
    scrollY : '90vh',
    scrollCollapse: true,
    ajax: 
    {
      beforeSend: function(){
        $('#loader_modal').modal({
          backdrop: 'static',
          keyboard: false
        });
        $('#loader_modal').modal('show');
      },
      url: "{!! route('get-books-for-sold') !!}",
      data: function(data) { data.student_classes_select = $('.student_classes_select option:selected').val(),data.stock = $('.stock_filter option:selected').val()},
    },
    columns: [
        { data: 'checkbox', name: 'checkbox' },   
        { data: 'name', name: 'name' },  
        { data: 'author', name: 'author' },  
        { data: 'description', name: 'description' },  
        { data: 'class_id', name: 'class_id' },   
        { data: 'selling_price', name: 'selling_price' },  
        { data: 'stock', name: 'stock' },  
        { data: 'status', name: 'status' }, 
    ],
    initComplete: function () {
    // Enable THEAD scroll bars
    $('.dataTables_scrollHead').css('overflow', 'auto');
    // Sync THEAD scrolling with TBODY
      $('.dataTables_scrollHead').on('scroll', function () {
          $('.dataTables_scrollBody').scrollLeft($(this).scrollLeft());
      });
    },
    drawCallback: function(){
      $('#loader_modal').modal('hide');
    },
  });

  $('#sold-books-form').on('submit',function(e){
     e.preventDefault();

    var selected_books_ids = [];
    $("input.check:checked").each(function() {
      selected_books_ids.push($(this).val());
    });

    $('#selected_books_ids').val(selected_books_ids);

      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
          }
      });
       $.ajax({
          url: "{{ route('sold-books-to-students') }}",
          dataType: 'json',
          type: 'post',
          data: new FormData(this),
          contentType: false,
          cache: false,
          processData:false,
          beforeSend: function(){
            $('.save-btn').addClass('disabled');
            $('.save-btn').attr('disabled', true);
            $('#loader_modal').modal({
              backdrop: 'static',
              keyboard: false
            });
            $('#loader_modal').modal('show');
          },
          success: function(result){
            $('.save-btn').attr('disabled', true);
            $('.save-btn').removeAttr('disabled');
            if(result.success == true){
              toastr.success('Success!', 'Data Added Successfully!!!',{"positionClass": "toast-bottom-right"});
              $('.table-books').DataTable().ajax.reload();
              $('#sold-books-form')[0].reset();
              $('#soldBook').modal('hide');
            }else{
              toastr.error('Error!', result.errormsg,{"positionClass": "toast-bottom-right"});
              $('#loader_modal').modal('hide');
            }

          },
          error: function (request, status, error) {
                /*$('.form-control').removeClass('is-invalid');
                $('.form-control').next().remove();*/
                $('#loader_modal').modal('hide');
                $('.save-btn').removeClass('disabled');
                $('.save-btn').removeAttr('disabled');
                json = $.parseJSON(request.responseText);
                $.each(json.errors, function(key, value){
                      $('input[name="'+key+'"]').after('<span class="invalid-feedback" role="alert"><strong>'+value+'</strong>');
                     $('input[name="'+key+'"]').addClass('is-invalid');
                     $('textarea[name="'+key+'"]').after('<span class="invalid-feedback" role="alert"><strong>'+value+'</strong>');
                     $('textarea[name="'+key+'"]').addClass('is-invalid');


                });
            }
        });
  });

  $(document).ready(function(){
    $('#student_admission_no').select2();
  //   $(document).on("dblclick",".inputDoubleClick",function(){
  //   $(this).addClass('d-none');
  //   $(this).next().removeClass('d-none');
  //   $(this).next().addClass('active');
  //   $(this).next().focus();
  // });

    $(document).on('keypress keyup focusout',".fieldFocus",function(e) {
    // alert('hi');
    var id = $(this).data('id');
    if (e.keyCode === 27 && $(this).hasClass('active')) {
      var fieldvalue = $(this).prev().data('fieldvalue');
      var thisPointer = $(this);
      thisPointer.addClass('d-none');
      thisPointer.val(fieldvalue);
      thisPointer.removeClass('active');
      thisPointer.prev().removeClass('d-none');
    }

    var name = $(this).attr('name');
    // alert(name);

    if( (e.keyCode === 13 || e.which === 0) && $(this).val().length > 0 && $(this).hasClass('active')){
      var str = $(this).val();
      if(($(this).val().length < 1 ||  !str.replace(/\s/g, '').length))
      {
        swal({ html:true, title:'Alert !!!', text:'<b>Please Enter At Least 1 Characters (this string may only contain white spaces)!!!</b>'});
        return false;
      }
      else
      {
        $(this).removeClass('active');
        var fieldvalue = $(this).prev().data('fieldvalue');
        var new_value = $(this).val();
        // alert(new_value);
        if(fieldvalue == new_value)
        {
          var thisPointer = $(this);
          thisPointer.addClass('d-none');
          thisPointer.prev().removeClass('d-none');
          $(this).prev().html(fieldvalue);
        }
        else
        {
          var thisPointer = $(this);
          thisPointer.addClass('d-none');
          thisPointer.prev().removeClass('d-none');
          if(new_value != '')
          {
            $(this).prev().html(new_value);
          }
          $(this).prev().data('fieldvalue', new_value);
          saveData(thisPointer,thisPointer.attr('name'), thisPointer.val(),fieldvalue,id);
        }
      }
    }

  });

      function saveData(thisPointer,field_name,field_value,new_select_value,id){

      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
      });
      $.ajax({
        method: "post",

        url: "{{ url('user/update-book-stock') }}",
        dataType: 'json',
        data: 'id='+id+'&'+field_name+'='+encodeURIComponent(field_value)+'&'+'new_select_value'+'='+encodeURIComponent(new_select_value),
        beforeSend: function(){
          $('#loader_modal').modal({
            backdrop: 'static',
            keyboard: false
          });
          $('#loader_modal').modal('show');
        },
        success: function(data)
        {
          $("#loader_modal").modal('hide');
          if(data.success == true)
          {
            
            toastr.success('Success!', 'Information updated successfully.',{"positionClass": "toast-bottom-right"});
            return true;
          }

          if(field_name == 'stock')
          {
              $('.table-books').DataTable().ajax.reload();
              $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
          }

        },

      });
    }

      $(document).on('click','.delete_stock',function(){
      var id = $(this).data('id');

        swal({
            title: "Alert!",
            html: true,
            text: "<b>Are you sure!!! You want to delete this record !!!</b>",
            type: "info",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes!",
            cancelButtonText: "No!",
            closeOnConfirm: true,
            closeOnCancel: false
          },
            function(isConfirm) {
            if (isConfirm){
             $.ajaxSetup({
              headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
              }
            });
            $.ajax({
              url: "{{ route('delete-book-stock') }}",
              method: 'post',
              data: {id:id},
              beforeSend: function(){
                $('#loader_modal').modal({
                  backdrop: 'static',
                  keyboard: false
                });
                $('#loader_modal').modal('show');
              },
              success: function (response) {
                if(response.success == false)
                {
                  toastr.warning('Sorry!', 'Cannot delete this Record, as it is already sold to students!!!',{"positionClass": "toast-bottom-right"});
                  $('#loader_modal').modal('hide');
                }

                if(response.success == true)
                {
                  toastr.success('Success!', 'Book Deleted Successfully!!!',{"positionClass": "toast-bottom-right"});
                  $('.table-books').DataTable().ajax.reload();
                  $('#loader_modal').modal('hide');
                }
               
              },
              error: function(request, status, error){
                $('#loader_modal').modal('hide');
              }
            });
            }
            else{
              swal("Cancelled", "", "error");
              
            }
          });
  });

    $(document).on('change','.student_classes_select',function(){
    $('.table-books').DataTable().ajax.reload();
    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
  });

    $(document).on('change','.stock_filter',function(){
    $('.table-books').DataTable().ajax.reload();
    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
  });

    $(document).on('click','#reset',function(){
    $('.student_classes_select').val('');
    $('.stock_filter').val('');
    $('.table-books').DataTable().ajax.reload();
    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
  });

  $(document).on('click', '.check-all1', function () {

    if($('.student_classes_select option:selected').val() == null || $('.student_classes_select option:selected').val() == '')
    {
       toastr.info('Please!', 'Select Class First!!!',{"positionClass": "toast-bottom-right"});
       return false;
    }
    // alert('hi');
  if(this.checked == true){
  $('.check').prop('checked', true);
  $('.check').parents('tr').addClass('selected');
  var cb_length = $( ".check:checked" ).length;
  if(cb_length > 0){
    $('.selected-item').removeClass('d-none');
  }
}else{
  $('.check').prop('checked', false);
  $('.check').parents('tr').removeClass('selected');
  $('.selected-item').addClass('d-none');
  
}
});

   $(document).on('click', '.check', function () {
    var id = $(this).data('id');
    if($('.student_classes_select option:selected').val() == null || $('.student_classes_select option:selected').val() == '')
    {
       toastr.info('Please!', 'Select Class First!!!',{"positionClass": "toast-bottom-right"});
       return false;
    }
    // $(this).removeClass('d-none');
        var cb_length = $( ".check:checked" ).length;
        var st_pieces = $(this).parents('tr').attr('data-pieces');
        if(this.checked == true){
        $('.selected-item').removeClass('d-none');
        var total = $('#total_amount').val();
        var new_to = $('#selling_price_'+id).val();

        $('#total_amount').val(parseFloat(total)+parseFloat(new_to));
        $(this).parents('tr').addClass('selected');
      }else{
        $(this).parents('tr').removeClass('selected');
        var total = $('#total_amount').val();
        var new_to = $('#selling_price_'+id).val();

        $('#total_amount').val(parseFloat(total)-parseFloat(new_to));
        if(cb_length == 0){
         $('.selected-item').addClass('d-none');
        }
        
      }
    });

    $(document).on('change','.student_classes_select',function(){
    // alert($(this).val());
    var id = $(this).val();

    $.ajaxSetup({
              headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
              }
            });
            $.ajax({
              url: "{{ route('fetch-students-for-books') }}",
              method: 'post',
              data: {id:id},
              beforeSend: function(){
                $('#loader_modal').modal({
                  backdrop: 'static',
                  keyboard: false
                });
                $('#loader_modal').modal('show');
              },
              success: function (response) {
                
                if(response.success == true)
                {
                  // alert(response.fee);
                  $('#student_admission_no').empty();
                  $('#student_admission_no').append(response.html);
                  $('#loader_modal').modal('hide');
                }
               
              },
              error: function(request, status, error){
                $('#loader_modal').modal('hide');
              }
            });
  });

  });
</script>
@stop

