@extends('user.layouts.layout')

@section('title','Issued Books')

@section('content')
<style type="text/css">
   
</style>


                  <div class="row page-title-row">
                      <div class="col-md col-sm col page-title-col align-self-center">
                          <h1 class="page-title">Issued Books</h1>
                      </div>
                      <div class="col-sm col-auto mt-sm-0 ml-auto text-right page-action-button">
                          <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addBook">+ Issue Book</button>
                      </div>    
                  </div>

                  <div class="pb-2">
                      <span style="color: red;font-style: italic;">Note :</span> <span style="font-style: italic;padding-left: 5px;"> Please check the checkbox(s) to receive the book</span>
                  </div>

                  <div class="card card-border-top mb-4">
                    <div class="card-header">
                      <h6 class="mb-0">Filters</h6>
                    </div>
                    <div class="card-body pb-2">
                      <div class="row mb-0 form-row">
                          <div class="form-group col-lg col-6">
                            <label for="to_date">User Type</label>
                            <select class="form-control form-control user_type_select state-tags" name="user_type_filter" >
                              <option value="">-- Select User Type --</option>
                               <option value="student">Student</option>
                               <option value="teacher">Teacher</option>
                            </select>
                          </div>

                          <div class="form-group col-lg col-6">
                            <label for="to_date">Status</label>
                            <select class="form-control form-control status_select state-tags" name="status_select" >
                              <option value="">-- Select Status--</option>
                               <option value="issued">Issued</option>
                               <option value="returned">Returned</option>
                            </select>
                          </div>
                          <div class="form-group col-lg col-6">
                            <label for="from_date">From Returned Date</label>
                              <input type="date" name="from_date" id="from_date" class="form-control" />
                          </div>
                          <div class="form-group col-lg col-6">
                            <label for="to_date">To Returned Date</label>
                              <input type="date" name="to_date" id="to_date" class="form-control" />
                          </div>
                          <div class="form-group align-self-end col-lg-auto col text-right">
                              <button class="btn btn-primary px-4" type="reset" id="reset">Reset</button>  
                          </div>
                        </div>
                      </div>
                  </div>


                    
                        <div class="card">
                          <div class="card-body">
                            <div class="selected-item catalogue-btn-group d-none mb-3">
                              <button class="btn btn-outline-primary download_invoice2 " title="Receive Book(s)">Receive Book(s)</button>
                            </div>
                            <table class="table table-striped nowrap table-books">
                            <thead>
                            <tr>
                                <th class="noVis">
                                  <div class="custom-control custom-checkbox custom-checkbox1 d-inline-block">
                                    <input type="checkbox" class="custom-control-input check-all1" name="check_all" id="check-all">
                                    <label class="custom-control-label" for="check-all"></label>
                                  </div>
                                </th>
                                <th>Book Name</th>
                                <th>Issued By</th>
                                <th>User type</th>
                                <th>User Name</th>
                                <th>Issue Date</th>
                                <th>Returned Date</th>
                                <th>Receive Date</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            </table>
                          </div>
                    </div>



<!--Add Student Modal -->
<div class="modal fade" id="addBook" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Issue Book</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form id="add-book-form" method="POST">
        @csrf
      <div class="modal-body pb-2">
          <div class="form-row">
            <div class="form-group col-6">
                <label for="user_type">Select User Type</label>
                <select class="form-control-lg" id="user_type" name="user_type" style="display: block !important;width: 100%;height: 100px;padding: 10px !important;">
                  <option value="">--Select User Type--</option>
                  <option value="student">Student</option>
                  <option value="teacher">Teacher</option>
                </select>
            </div>

            <div class="form-group col-6">
                <label for="user_id">Select Student/Teacher</label>
                <select class="form-control-lg" id="user_id" name="user_id" style="display: block !important;width: 100%;height: 100px;padding: 10px !important;">
                  <option value="">--Select one of the following--</option>
                </select>
            </div>

            <div class="form-group col-6">
                <label for="book">Book</label>
                <select class="form-control-lg" id="book_id" name="book_id" style="display: block !important;width: 100%;height: 100px;padding: 10px !important;">
                  <option value="">--Select Book--</option>
                  @foreach($books as $bk)
                  <option value="{{$bk->id}}"> {{$bk->title}} ({{$bk->author}}) (Book No. {{$bk->book_no}}) </option>
                  @endforeach
                </select>
            </div>
            <div class="form-group col-6">
                <label for="issue_date">Issue Date</label>
                <input type="date" name="issue_date" id="issue_date" class="form-control" />
            </div>
            <div class="form-group col-6">
                <label for="return_date">Return Date</label>
                <input type="date" name="return_date" id="return_date" class="form-control" />
            </div>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary save-btn">Add</button>
      </div>
      </form>
    </div>
  </div>
</div>
  <input type="hidden" name="fees_selected" class="fees_selected">

@endsection

@section('javascript')
<script type="text/javascript">

    var table2 =  $('.table-books').DataTable({
    processing: true,
    searching: true,
    "language": {
    processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw" style="color:#13436c;"></i><span class="sr-only">Loading...</span> '},
    ordering: false,
    pageLength: {{50}},
    serverSide: true,
    autoWidth: false,
    "lengthMenu": [50,100,150,200],
    
    scrollX:true,
    scrollY : '90vh',
    scrollCollapse: true,
    ajax: 
    {
      beforeSend: function(){
        $('#loader_modal').modal({
          backdrop: 'static',
          keyboard: false
        });
        $('#loader_modal').modal('show');
      },
      url: "{!! route('get-library-issued-books') !!}",
      data: function(data) {data.user_type = $('.user_type_select option:selected').val(),data.status = $('.status_select option:selected').val(), data.from_date = $('#from_date').val(), data.to_date = $('#to_date').val()},
    },
    columns: [
        { data: 'checkbox', name: 'checkbox' },  
        { data: 'book_title', name: 'book_title' },  
        { data: 'issued_by', name: 'issued_by' },  
        { data: 'user_type', name: 'user_type' },  
        { data: 'username', name: 'username' },  
        { data: 'issue_date', name: 'issue_date' },  
        { data: 'actual_return_date', name: 'actual_return_date' },  
        { data: 'std_return_date', name: 'std_return_date' },  
        { data: 'status', name: 'status' },  
        { data: 'action', name: 'action' },  
    ],
    initComplete: function () {
    // Enable THEAD scroll bars
    $('.dataTables_scrollHead').css('overflow', 'auto');
    // Sync THEAD scrolling with TBODY
      $('.dataTables_scrollHead').on('scroll', function () {
          $('.dataTables_scrollBody').scrollLeft($(this).scrollLeft());
      });
    },
    drawCallback: function(){
      $('#loader_modal').modal('hide');
    },
  });

  $(document).on('change','.user_type_select',function(){
    $('.table-books').DataTable().ajax.reload();
    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
  });

  $(document).on('change','.status_select',function(){
    $('.table-books').DataTable().ajax.reload();
    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
  });

  $(document).on('change','#from_date',function(){
    $('.table-books').DataTable().ajax.reload();
    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
  });

   $(document).on('change','#to_date',function(){
    $('.table-books').DataTable().ajax.reload();
    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
  });

   $(document).on('click','#reset',function(){
    $('.user_type_select').val('');
    $('.status_select').val('');
    $('#from_date').val('');    
    $('#to_date').val('');
    $('.table-books').DataTable().ajax.reload();
    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
  });

  $('#add-book-form').on('submit',function(e){
    e.preventDefault();

     e.preventDefault();
      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
          }
      });
       $.ajax({
          url: "{{ route('issue-book-to-user') }}",
          dataType: 'json',
          type: 'post',
          data: new FormData(this),
          contentType: false,
          cache: false,
          processData:false,
          beforeSend: function(){
            $('.save-btn').addClass('disabled');
            $('.save-btn').attr('disabled', true);
            $('#loader_modal').modal({
              backdrop: 'static',
              keyboard: false
            });
            $('#loader_modal').modal('show');
          },
          success: function(result){
            $('.save-btn').attr('disabled', true);
            $('.save-btn').removeAttr('disabled');
            if(result.success == true){
              toastr.success('Success!', 'Book Issued Successfully!!!',{"positionClass": "toast-bottom-right"});
              $('.table-books').DataTable().ajax.reload();
              $('#add-book-form')[0].reset();
              $('#addBook').modal('hide');
            }
            else if(result.success == false && result.already_exist == true)
            {
              toastr.info('Sorry!', 'Book already issued to this user!!!',{"positionClass": "toast-bottom-right"});
            }
            else
            {
              toastr.error('Error!', result.errormsg,{"positionClass": "toast-bottom-right"});
              $('#loader_modal').modal('hide');
            }

          },
          error: function (request, status, error) {
                /*$('.form-control').removeClass('is-invalid');
                $('.form-control').next().remove();*/
                $('#loader_modal').modal('hide');
                $('.save-btn').removeClass('disabled');
                $('.save-btn').removeAttr('disabled');
                json = $.parseJSON(request.responseText);
                $.each(json.errors, function(key, value){
                      $('input[name="'+key+'"]').after('<span class="invalid-feedback" role="alert"><strong>'+value+'</strong>');
                     $('input[name="'+key+'"]').addClass('is-invalid');
                     $('textarea[name="'+key+'"]').after('<span class="invalid-feedback" role="alert"><strong>'+value+'</strong>');
                     $('textarea[name="'+key+'"]').addClass('is-invalid');


                });
            }
        });
  });

  $(document).ready(function(){
    $(document).on("dblclick",".inputDoubleClick",function(){
    $(this).addClass('d-none');
    $(this).next().removeClass('d-none');
    $(this).next().addClass('active');
    $(this).next().focus();
  });

    $(document).on('keypress keyup focusout',".fieldFocus",function(e) {
    // alert('hi');
    var id = $(this).data('id');
    if (e.keyCode === 27 && $(this).hasClass('active')) {
      var fieldvalue = $(this).prev().data('fieldvalue');
      var thisPointer = $(this);
      thisPointer.addClass('d-none');
      thisPointer.val(fieldvalue);
      thisPointer.removeClass('active');
      thisPointer.prev().removeClass('d-none');
    }

    var name = $(this).attr('name');
    // alert(name);

    if( (e.keyCode === 13 || e.which === 0) && $(this).val().length > 0 && $(this).hasClass('active')){
      var str = $(this).val();
      if(($(this).val().length < 1 ||  !str.replace(/\s/g, '').length))
      {
        swal({ html:true, title:'Alert !!!', text:'<b>Please Enter At Least 1 Characters (this string may only contain white spaces)!!!</b>'});
        return false;
      }
      else
      {
        $(this).removeClass('active');
        var fieldvalue = $(this).prev().data('fieldvalue');
        var new_value = $(this).val();
        // alert(new_value);
        if(fieldvalue == new_value)
        {
          var thisPointer = $(this);
          thisPointer.addClass('d-none');
          thisPointer.prev().removeClass('d-none');
          $(this).prev().html(fieldvalue);
        }
        else
        {
          var thisPointer = $(this);
          thisPointer.addClass('d-none');
          thisPointer.prev().removeClass('d-none');
          if(new_value != '')
          {
            $(this).prev().html(new_value);
          }
          $(this).prev().data('fieldvalue', new_value);
          saveData(thisPointer,thisPointer.attr('name'), thisPointer.val(),fieldvalue,id);
        }
      }
    }

  });

      function saveData(thisPointer,field_name,field_value,new_select_value,id){

      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
      });
      $.ajax({
        method: "post",

        url: "{{ url('user/update-book-library') }}",
        dataType: 'json',
        data: 'id='+id+'&'+field_name+'='+encodeURIComponent(field_value)+'&'+'new_select_value'+'='+encodeURIComponent(new_select_value),
        beforeSend: function(){
          $('#loader_modal').modal({
            backdrop: 'static',
            keyboard: false
          });
          $('#loader_modal').modal('show');
        },
        success: function(data)
        {
          $("#loader_modal").modal('hide');
          if(data.success == true)
          {
            
            toastr.success('Success!', 'Information updated successfully.',{"positionClass": "toast-bottom-right"});
            return true;
          }

          if(field_name == 'stock')
          {
              $('.table-books').DataTable().ajax.reload();
              $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
          }

        },

      });
    }

      $(document).on('click','.delete_stock',function(){
      var id = $(this).data('id');

        swal({
            title: "Alert!",
            html: true,
            text: "<b>Are you sure!!! You want to delete this record !!!</b>",
            type: "info",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes!",
            cancelButtonText: "No!",
            closeOnConfirm: true,
            closeOnCancel: false
          },
            function(isConfirm) {
            if (isConfirm){
             $.ajaxSetup({
              headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
              }
            });
            $.ajax({
              url: "{{ route('delete-issue-book-library') }}",
              method: 'post',
              data: {id:id},
              beforeSend: function(){
                $('#loader_modal').modal({
                  backdrop: 'static',
                  keyboard: false
                });
                $('#loader_modal').modal('show');
              },
              success: function (response) {
                if(response.success == true)
                {
                  toastr.success('Success!', 'Record Deleted Successfully!!!',{"positionClass": "toast-bottom-right"});
                  $('.table-books').DataTable().ajax.reload();
                  $('#loader_modal').modal('hide');
                }
               
              },
              error: function(request, status, error){
                $('#loader_modal').modal('hide');
              }
            });
            }
            else{
              swal("Cancelled", "", "error");
              
            }
          });
  });

  });
  $('#user_type').select2();
  $('#book_id').select2();
  $('#user_id').select2();
  $(document).on('change','#user_type',function(){
    // alert($(this).val());
    var id = $(this).val();

    $.ajaxSetup({
              headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
              }
            });
            $.ajax({
              url: "{{ route('fetch-users-to-issue-book') }}",
              method: 'post',
              data: {id:id},
              beforeSend: function(){
                $('#loader_modal').modal({
                  backdrop: 'static',
                  keyboard: false
                });
                $('#loader_modal').modal('show');
              },
              success: function (response) {
                
                if(response.success == true)
                {
                  $('#loader_modal').modal('hide');
                  $('#user_id').empty();
                  $('#user_id').append(response.options);
                }
                else
                {
                  toastr.info('Sorry!', 'Select Student or Teacher!!!',{"positionClass": "toast-bottom-right"});
                }
               
              },
              error: function(request, status, error){
                $('#loader_modal').modal('hide');
              }
            });
  });
  $(document).on('click', '.check-all1', function () {
    if(this.checked == true)
    {
      $('.check').prop('checked', true);
      $('.check').parents('tr').addClass('selected');
      var cb_length = $( ".check:checked" ).length;
      if(cb_length > 0)
      {
        $('.selected-item').removeClass('d-none');
      }
    }
    else
    {
      $('.check').prop('checked', false);
      $('.check').parents('tr').removeClass('selected');
      $('.selected-item').addClass('d-none');
    }
  });

   $(document).on('click', '.check', function () {
    // $(this).removeClass('d-none');
        var cb_length = $( ".check:checked" ).length;
        var st_pieces = $(this).parents('tr').attr('data-pieces');
        if(this.checked == true){
        $('.selected-item').removeClass('d-none');
        $(this).parents('tr').addClass('selected');
      }else{
        $(this).parents('tr').removeClass('selected');
        if(cb_length == 0){
         $('.selected-item').addClass('d-none');
        }
        
      }
    });

  $(document).on('click', '.download_invoice2', function(){
    var selected_fees = [];
    $("input.check:checked").each(function() {
      selected_fees.push($(this).val());
    });

    if(selected_fees == ''){
            toastr.error('Error!', 'Select Record First.',{"positionClass": "toast-bottom-right"});
        return false;
        }else{
        $('.fees_selected').val(selected_fees);

        var fees = $('.fees_selected').val();
        swal({
            title: "Are You Sure!",
            html: true,
            text: "<b>You want to receive these books !!!</b>",
            type: "info",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes!",
            cancelButtonText: "No!",
            closeOnConfirm: true,
            closeOnCancel: false
          },
            function(isConfirm) {
            if (isConfirm){
               $.ajaxSetup({
              headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
              }
            });
            $.ajax({
              url: "{{ route('receive-library-books') }}",
              method: 'post',
              data: {fee_ids:fees},
              beforeSend: function(){
                $('#loader_modal').modal({
                  backdrop: 'static',
                  keyboard: false
                });
                $('#loader_modal').modal('show');
              },
              success: function (response) {
                if(response.success == true)
                {
                  toastr.success('Success!', 'Book(s) Received Successfully!!!',{"positionClass": "toast-bottom-right"});
                  $('.table-books').DataTable().ajax.reload();
                  $('#loader_modal').modal('hide');
                }
                else if(response.success == false)
                {
                  toastr.info('Sorry!', 'Some of the book(s) are already received!!!',{"positionClass": "toast-bottom-right"});
                  $('.table-books').DataTable().ajax.reload();
                  $('#loader_modal').modal('hide');
                }
               
              },
              error: function(request, status, error){
                // $('#loader_modal').modal('hide');
              }
            });
            }
            else{
              swal("Cancelled", "", "error");
              
            }
          });
         
        }
      });
</script>
@stop

