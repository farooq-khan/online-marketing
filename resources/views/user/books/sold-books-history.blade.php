@extends('user.layouts.layout')

@section('title','All Sold Books')

@section('content')
<style type="text/css">
   
</style>


                    <div class="row page-title-row">
                        <div class="col-md col-sm col-12 page-title-col align-self-center">
                            <h1 class="page-title">All Sold Books History</h1>
                        </div>
                        <div class="col-sm col-12 mt-sm-0 mt-3 ml-auto text-right page-action-button">
                            <!--  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addBook">+Sold Book</button> -->
                        </div> 
                    </div>

                     <div class="pb-2">
                      <span style="color: red;font-style: italic;">Note : </span> <span style="font-style: italic;padding-left: 5px;"> Double Click The Record For Updating</span>
                    </div>

                    <div class="card">
                      <div class="card-body">
                        <div class="row mb-0 form-row row-cols-md-3 row-cols-sm-3 row-cols-2">
                          <div class="form-group col">
                            <select class="form-control form-control student_classes_select state-tags" name="warehouse_id" >
                              <option value="">-- All Classes --</option>
                               @foreach($classes as $class)
                                <option value="{{$class->id}}"> {{$class->class_name}} </option>
                                @endforeach
                            </select>
                          </div>

                          <div class="form-group col">
                            <select class="form-control form-control status-filter state-tags" name="warehouse_id" >
                              <option value="">-- All Statuses --</option>
                                <option value="paid"> Paid </option>
                                <option value="unpaid"> Unpaid </option>
                            </select>
                          </div>
                    
                          <div class="form-group col ml-auto text-md-left text-right">
                            <button class="btn btn-primary px-4" type="reset" id="reset">Reset</button>  
                          </div>
                        </div>
                        <table class="table table-striped nowrap table-books-history">
                        <thead>
                        <tr>
                          <th class="noVis">
                              Action
                            </th>
                            <th>Student Name</th>
                            <th>Class</th>
                            <th>Book(s)</th>
                            <th>Total Amount</th>
                            <th>Received Amount</th>
                            <th>Remaining Amount</th>
                            <th>Status</th>
                        </tr>
                        </thead>
                        </table>
                      </div>
                  </div>


@endsection

@section('javascript')
<script type="text/javascript">

    var table2 =  $('.table-books-history').DataTable({
    processing: true,
    searching: true,
    "language": {
    processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw" style="color:#13436c;"></i><span class="sr-only">Loading...</span> '},
    ordering: false,
    pageLength: {{50}},
    serverSide: true,
    autoWidth: false,
    "lengthMenu": [50,100,150,200],
    
    scrollX:true,
    scrollY : '90vh',
    scrollCollapse: true,
    ajax: 
    {
      beforeSend: function(){
        $('#loader_modal').modal({
          backdrop: 'static',
          keyboard: false
        });
        $('#loader_modal').modal('show');
      },
      url: "{!! route('get-sold-books-history') !!}",
      data: function(data) { data.student_classes_select = $('.student_classes_select option:selected').val(),data.status_filter = $('.status-filter option:selected').val()},
    },
    columns: [
        { data: 'checkbox', name: 'checkbox' },   
        { data: 'name', name: 'name' },  
        { data: 'std_class', name: 'std_class' },  
        { data: 'books', name: 'books' },  
        { data: 'total_amount', name: 'total_amount' },   
        { data: 'paid_amount', name: 'paid_amount' },  
        { data: 'unpaid_amount', name: 'unpaid_amount' },  
        { data: 'status', name: 'status' }, 
    ],
    initComplete: function () {
    // Enable THEAD scroll bars
    $('.dataTables_scrollHead').css('overflow', 'auto');
    // Sync THEAD scrolling with TBODY
      $('.dataTables_scrollHead').on('scroll', function () {
          $('.dataTables_scrollBody').scrollLeft($(this).scrollLeft());
      });
    },
    drawCallback: function(){
      $('#loader_modal').modal('hide');
    },
  });


  $(document).ready(function(){
    $('#student_admission_no').select2();
    $(document).on("dblclick",".inputDoubleClick",function(){
    $(this).addClass('d-none');
    $(this).next().removeClass('d-none');
    $(this).next().addClass('active');
    $(this).next().focus();
  });

    $(document).on('keypress keyup focusout',".fieldFocus",function(e) {
    // alert('hi');
    var id = $(this).data('id');
    if (e.keyCode === 27 && $(this).hasClass('active')) {
      var fieldvalue = $(this).prev().data('fieldvalue');
      var thisPointer = $(this);
      thisPointer.addClass('d-none');
      thisPointer.val(fieldvalue);
      thisPointer.removeClass('active');
      thisPointer.prev().removeClass('d-none');
    }

    var name = $(this).attr('name');
    // alert(name);

    if( (e.keyCode === 13 || e.which === 0) && $(this).val().length > 0 && $(this).hasClass('active')){
      var str = $(this).val();
      if(($(this).val().length < 1 ||  !str.replace(/\s/g, '').length))
      {
        swal({ html:true, title:'Alert !!!', text:'<b>Please Enter At Least 1 Characters (this string may only contain white spaces)!!!</b>'});
        return false;
      }
      else
      {
        $(this).removeClass('active');
        var fieldvalue = $(this).prev().data('fieldvalue');
        var new_value = $(this).val();
        // alert(new_value);
        if(fieldvalue == new_value)
        {
          var thisPointer = $(this);
          thisPointer.addClass('d-none');
          thisPointer.prev().removeClass('d-none');
          $(this).prev().html(fieldvalue);
        }
        else
        {
          var thisPointer = $(this);
          thisPointer.addClass('d-none');
          thisPointer.prev().removeClass('d-none');
          if(new_value != '')
          {
            $(this).prev().html(new_value);
          }
          $(this).prev().data('fieldvalue', new_value);
          saveData(thisPointer,thisPointer.attr('name'), thisPointer.val(),fieldvalue,id);
        }
      }
    }

  });

      function saveData(thisPointer,field_name,field_value,new_select_value,id){

      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
      });
      $.ajax({
        method: "post",

        url: "{{ url('user/update-sold-books-data') }}",
        dataType: 'json',
        data: 'id='+id+'&'+field_name+'='+encodeURIComponent(field_value)+'&'+'new_select_value'+'='+encodeURIComponent(new_select_value),
        beforeSend: function(){
          $('#loader_modal').modal({
            backdrop: 'static',
            keyboard: false
          });
          $('#loader_modal').modal('show');
        },
        success: function(data)
        {
          $("#loader_modal").modal('hide');
          if(data.success == true)
          {
            $('.table-books-history').DataTable().ajax.reload();
            toastr.success('Success!', 'Information updated successfully.',{"positionClass": "toast-bottom-right"});
            return true;
          }

          if(field_name == 'stock')
          {
              $('.table-books-history').DataTable().ajax.reload();
              $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
          }

        },

      });
    }


    $(document).on('change','.student_classes_select',function(){
    $('.table-books-history').DataTable().ajax.reload();
    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
  });

    $(document).on('change','.status-filter',function(){
    $('.table-books-history').DataTable().ajax.reload();
    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
  });


    $(document).on('click','#reset',function(){
    $('.student_classes_select').val('');
    $('.status-filter').val('');
    $('.table-books-history').DataTable().ajax.reload();
    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
  });

  $(document).on('click', '.download_invoice', function(){

    var id = $(this).data('id');
    var url = "{{url('user/sold-books-print')}}"+"/"+id;
    window.open(url, 'Sold Books History!!!', 'width=1200,height=600,scrollbars=yes');
    $('#loader_modal').modal('hide');


  });

  });
</script>
@stop

