@extends('user.layouts.layout')

@section('title','Online Marketing')
<style>

  .update-profile {

      max-width: 400px;
    }
    .update-profile h1 {
      font-size: 1.8rem;
  }
  .update-profile img {
      width: 100px;
      height: 100px;
      border-radius: 50%;
      object-fit: cover;
      margin-bottom: 1rem;
      border: 2px solid #0073e6;
  }

</style>
@section('content')

          <div class="card update-profile mx-auto w-100 p-sm-4">
            <div class="card-body p-md-2">
                <h1 class="text-center mb-3">Update Profile</h1>

                <div class="justify-content-center d-flex align-items-center mb-3">  
                  @if(Auth::user()->logo != null && file_exists( public_path() . '/uploads/users/logos/'.Auth::user()->logo))                          
                    <img src="{{asset('public/uploads/users/logos/'.Auth::user()->logo)}}" width="20%" style="border-radius: 50%;" height="130px">
                  @else
                    <img src="{{asset('public/assets/img/user-avatar.png')}}" width="25%">
                  @endif  
                </div>
                <form class="register-form add-users-form row mb-0" id="register-form">
                            <div class="col-12 form-group">
                                <label for="name">Name</label>
                                <input class="form-control" type="text" name="name" id="name" placeholder="Your Name" value="{{Auth::user()->name}}" />
                            </div>
                            <div class="col-12 form-group">
                                <label for="email">Email</label>
                                <input class="form-control" type="email" name="email" id="email" placeholder="Your Email" value="{{Auth::user()->email}}" disabled="true" />
                            </div>

                            <div class="col-12 col-sm-6 form-group">
                                <label for="password" title="Password">Password</label>
                                <input class="form-control" type="text" name="password" id="password" placeholder="Enter New Password..." title="User Password"/>
                            </div>

                            <div class="col-12 col-sm-6 form-group">
                                <label for="confirm_password">Confirm Password</label>
                                <input class="form-control" type="text" name="confirm_password" id="confirm_password" placeholder="Confirm Password..." title="User Password" />
                            </div>

                            <div class="col-12 form-group">
                                <label for="logo">Upload Photo</label>
                                <input class="form-control" type="file" name="logo" id="logo"/>
                            </div>
                           
                            <div class="col-12 mb-0 form-button">
                                <input type="submit" name="signup" id="signup" class="form-submit save-btn btn btn-primary w-100" value="Update"/>
                            </div>
                  </form>
    </div>
</div>            
@endsection

@section('javascript')
<script type="text/javascript">
$(document).ready(function(){
   $(document).on('submit', '.add-users-form', function(e){  
      e.preventDefault();

      var pass = $('#password').val();
      var confirm_password = $('#confirm_password').val();

      if((pass != '') && (confirm_password == ''))
      {
        toastr.info('Sorry!', 'Both Password Fields Must Be Filled !!!',{"positionClass": "toast-bottom-right"});
        return false;
      }

      if((pass == '' ) && (confirm_password != ''))
      {
        toastr.info('Sorry!', 'Both Password Fields Must Be Filled !!!',{"positionClass": "toast-bottom-right"});
        return false;
      }

      if(pass != '' && confirm_password != '')
      {
        if(pass != confirm_password)
        {
          toastr.info('Sorry!', 'Both Password Does Not Match !!!',{"positionClass": "toast-bottom-right"});
          return false;
        }
      }
      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
          }
      });
       $.ajax({

          url: "{{ route('update-profile-changes') }}",
          method: 'post',
          
          data: new FormData(this), 
          contentType: false,       
          cache: false,             
          processData:false,
        
          // data: $('.add-supplier-form').serialize(),
          beforeSend: function(){
            $('#loader_modal').modal({
              backdrop: 'static',
              keyboard: false
            });
            $("#loader_modal").modal('show');
            $('.save-btn').val('Please wait...');
            $('.save-btn').addClass('disabled');
            $('.save-btn').attr('disabled', true);
          },
          success: function(data){
            $('.save-btn').val('add');
            $('.save-btn').attr('disabled', true);
            $('.save-btn').removeAttr('disabled');
            if(data.error == false){
              
              toastr.success('Success!', 'Profile updated successfully',{"positionClass": "toast-bottom-right"});
              $('.add-users-form')[0].reset();
              var role = "{{Auth::user()->role}}";
              if(role == 'user')
              {
                setTimeout(function(){ window.location.href = "{{url('user/dashboard')}}"; }, 2000);
              }
              else if(role == 'admin')
              {
                setTimeout(function(){ window.location.href = "{{url('admin/dashboard')}}"; }, 2000);
              }
              
              
            }
            else
            {
            $("#loader_modal").modal('hide');

            }
            
            
          },
          error: function (request, status, error) {
                $('.save-btn').val('add');
                $('.save-btn').removeClass('disabled');
                $('.save-btn').removeAttr('disabled');
                $('.form-control').removeClass('is-invalid');
                $('.form-control').next().remove();
                json = $.parseJSON(request.responseText);
                $.each(json.errors, function(key, value){
                    $('input[name="'+key+'"]').after('<span class="invalid-feedback" role="alert"><strong>'+value+'</strong>');
                     $('input[name="'+key+'"]').addClass('is-invalid');
                });
            }
        });
    });
});
</script>
@stop
