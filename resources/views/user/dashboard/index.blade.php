@extends('user.layouts.layout')

@section('title','Online Marketing')

@section('content')
<style type="text/css">
    table.dataTable
    {
        margin-top: 24px !important;
    }

    .dataTables_length > label
    {
        margin-left: 15px !important;
    }
</style>

                    <div>
                        <div class="row page-title-row">
                            <div class="col-12">
                                <h1 class="page-title">Irfan Star Private High School</h1>
                            </div>
                        </div>
                   {{--  <div class="row p-0 ">
                        <div class="col-10 p-0 bg-white pt-4 pr-4 container-shadow">
                            <div class="row">
                                <div class="col-6">
                                    <button class="btn-style text-left pr-5 mb-2"><span class="ml-4">Officer</span></button>
                                    <div class="pl-4">
                                    <table class="officer_table mb-4">
                                        <tbody>
                                            <tr>
                                                <td class="officer_table_title" width="50%">Joining Date :</td>
                                                <td class="officer_table_values">{{Auth::user()->created_at}}</td>
                                            </tr>
                                            <tr>
                                                <td class="officer_table_title">Last Login :</td>
                                                <td class="officer_table_values">{{Auth::user()->created_at}}</td>
                                            </tr>
                                            <tr>
                                                <td class="officer_table_title">Membership Type : </td>
                                                <td class="officer_table_values">Gold</td>
                                            </tr>
                                            <tr>
                                                <td class="officer_table_title">Upgrade Expires :</td>
                                                <td class="officer_table_values">{{Auth::user()->created_at}}</td>
                                            </tr>
                                            <tr>
                                                <td><button class="btn btn-primary mt-3">UPGRADE</button></td>
                                                <td></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="row">
                                        <div class="col">
                                            <button class="btn btn-primary theme-btn">Edit Profile</button>
                                        </div>
                                        <div class="col">
                                            <button class="btn btn-success theme-btn">View Ads</button>
                                        </div>
                                    </div>
                                    <div class="row mt-2">
                                        <div class="col-12">
                                            <button class="btn btn-primary theme-btn">Monday Bonus: S(0.2400%) - B(0.5100%)</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>    
                    </div>
                    <div class="row p-0 mt-4">
                        <div class="col-10 p-0 pb-5 bg-white pt-1 pr-4 d-flex container-shadow justify-content-center align-items-center">
                            <span class="theme-height"><span class="theme-color theme-size">3.03</span><br><span class="widget_text">Earn Points</span></span>
                        </div>
                    </div> --}}

                    <div class="row">
                        <div class="col-12">
                        <div class="row">  
                        <div class="col pl-0">
                            <div class="bg-white container-shadow d-flex container-shadow justify-content-center align-items-center pt-5 pb-5">
                            <span class="theme-height"><span class="theme-color theme-size">{{$students_count}}</span><br><span class="widget_text">Total Students<br><span style="visibility: hidden;">({{$start}} - {{$end}})</span></span></span>
                            </div>
                        </div>
                        <div class="col pl-0">
                            <div class="bg-white container-shadow d-flex container-shadow justify-content-center align-items-center pt-5 pb-5">
                            <span class="theme-height"><span class="theme-color theme-size">{{$previous_month_fee + $previous_month_fee_unpaid}}</span><br><span class="widget_text">Total Fee</span><br><span>({{$start}} - {{$end}})</span></span>
                            </div>
                        </div>
                        <div class="col pl-0">
                            <div class="bg-white container-shadow d-flex container-shadow justify-content-center align-items-center pt-5 pb-5">
                            <span class="theme-height"><span class="theme-color theme-size">{{$previous_month_fee}}</span><br><span class="widget_text">Submitted Fee</span><br><span>({{$start}} - {{$end}})</span></span>
                            </div>
                        </div>
                        <div class="col pr-0 pl-0">
                            <div class="bg-white container-shadow d-flex container-shadow justify-content-center align-items-center pt-5 pb-5">
                            <span class="theme-height"><span class="theme-color theme-size">{{$previous_month_fee_unpaid}}</span><br><span class="widget_text">Unsubmitted Fee</span><br><span>({{$start}} - {{$end}})</span></span>
                            </div>
                        </div>

            
                        </div>
                        </div>
                    </div>

{{--                     <div class="row p-0 mt-4">
                        <div class="bg-white col-12 mt-4">
                        <h1 class="page-title">Students Record</h1>    
                        <table id="students_record" class="table table-borderd col-12 mt-4" style="width: 100%">
                        <thead>
                        <tr>
                        <th>ID#</th>
                        <th>Image</th>
                        <th>Name</th>
                        <th>Roll No.</th>
                        <th>Class</th>
                        <th>Gender</th>
                        <th>Phone No.</th>
                        <th>Fee</th>
                      </tr>
                    </thead>

                    <tbody>
                        <tr>
                            <td>1</td>
                            <td><img src="{{asset('public/assets/img/user-avatar.png')}}" width="60px"></td>
                            <td>ABC</td>
                            <td>80012</td>
                            <td>10th</td>
                            <td>Male</td>
                            <td>0900 1234567</td>
                            <td>1200 /-</td>
                        </tr>
                        <tr>
                            <td>1</td>
                            <td><img src="{{asset('public/assets/img/user-avatar.png')}}" width="60px"></td>
                            <td>ABC</td>
                            <td>80012</td>
                            <td>10th</td>
                            <td>Male</td>
                            <td>0900 1234567</td>
                            <td>1200 /-</td>
                        </tr>
                        <tr>
                            <td>1</td>
                            <td><img src="{{asset('public/assets/img/user-avatar.png')}}" width="60px"></td>
                            <td>ABC</td>
                            <td>80012</td>
                            <td>10th</td>
                            <td>Male</td>
                            <td>0900 1234567</td>
                            <td>1200 /-</td>
                        </tr>
                        <tr>
                            <td>1</td>
                            <td><img src="{{asset('public/assets/img/user-avatar.png')}}" width="60px"></td>
                            <td>ABC</td>
                            <td>80012</td>
                            <td>10th</td>
                            <td>Male</td>
                            <td>0900 1234567</td>
                            <td>1200 /-</td>
                        </tr>
                        <tr>
                            <td>1</td>
                            <td><img src="{{asset('public/assets/img/user-avatar.png')}}" width="60px"></td>
                            <td>ABC</td>
                            <td>80012</td>
                            <td>10th</td>
                            <td>Male</td>
                            <td>0900 1234567</td>
                            <td>1200 /-</td>
                        </tr>
                        <tr>
                            <td>1</td>
                            <td><img src="{{asset('public/assets/img/user-avatar.png')}}" width="60px"></td>
                            <td>ABC</td>
                            <td>80012</td>
                            <td>10th</td>
                            <td>Male</td>
                            <td>0900 1234567</td>
                            <td>1200 /-</td>
                        </tr>
                        <tr>
                            <td>1</td>
                            <td><img src="{{asset('public/assets/img/user-avatar.png')}}" width="60px"></td>
                            <td>ABC</td>
                            <td>80012</td>
                            <td>10th</td>
                            <td>Male</td>
                            <td>0900 1234567</td>
                            <td>1200 /-</td>
                        </tr>
                        <tr>
                            <td>1</td>
                            <td><img src="{{asset('public/assets/img/user-avatar.png')}}" width="60px"></td>
                            <td>ABC</td>
                            <td>80012</td>
                            <td>10th</td>
                            <td>Male</td>
                            <td>0900 1234567</td>
                            <td>1200 /-</td>
                        </tr>
                        <tr>
                            <td>1</td>
                            <td><img src="{{asset('public/assets/img/user-avatar.png')}}" width="60px"></td>
                            <td>ABC</td>
                            <td>80012</td>
                            <td>10th</td>
                            <td>Male</td>
                            <td>0900 1234567</td>
                            <td>1200 /-</td>
                        </tr>

                    </tbody>
                        </table>
                        </div>
                    </div> --}}
                    </div>

@endsection

@section('javascript')
<script type="text/javascript">
$(document).ready(function(){
   $('#students_record').dataTable( {
    "autoWidth": false,
    searching: true
});
});
</script>
@stop

