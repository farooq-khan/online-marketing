@extends('user.layouts.layout')

@section('title','Single Student')

@section('content')
<style type="text/css">
   .student-image
   {
    padding-top: 100%;
    background-position: center;
    background-size: cover;
   }

   .student_image
   {
    box-shadow: 0px 0px 15px 0px rgba(0,0,0,0.5);

   }
</style>
<?php 
  use Carbon\Carbon;
?>
             <div class="row page-title-row">
                  <div class="col-12">
                      <h1 class="page-title">Student Fee History</h1>
                  </div>                           
              </div>
                <div class="card">
                  <div class="card-body">
                    <div class="selected-item catalogue-btn-group mb-3 d-none">
                      <a href="javascript:void(0)" class="btn btn-outline-primary download_invoice" data-id="1" title="Download Invoice">Download Invoice</a>
                  </div>
                    <div class="row form-row">
                      <div class="form-group col-lg col-md-4 col-sm-4 col-6">
                        <label for="">Classes</label>
                        <select class="form-control form-control student_classes_select state-tags" name="student_classes_select" >
                          <option value="">All Classes</option>
                           @foreach($classes as $class)
                            <option value="{{$class->id}}" {{$class->id == $class_id ? 'selected' : ''}}> {{$class->class_name}} </option>
                            @endforeach
                        </select>
                      </div>

                       <div class="form-group col-lg col-md-4 col-sm-4 col-6">
                        <label for="">Discount</label>
                        <select class="form-control form-control discount_student state-tags" name="discount_student" >
                          <option value="">Discount</option>
                            <option value="discount"> Discounted Students </option>
                            <option value="non-discount"> Non Discounted Students </option>
                        </select>
                      </div>

                       <div class="form-group col-lg col-md-4 col-sm-4 col-6">
                        <label for="">Status</label>
                        <select class="form-control form-control status_student state-tags" name="status" >
                          <option value="">Status</option>
                            <option value="1">Paid</option>
                            <option value="2">Partial Paid</option>
                        </select>
                      </div>

                       <div class="form-group col-lg col-md-4 col-sm-4 col-6">
                          <label for="">From date</label>
                          <input type="date" name="from_date" id="from_date" class="form-control" placeholder="Search by year ..." />
                      </div>

                      <div class="form-group col-lg col-md-4 col-sm-4 col-6">
                        <label for="">To date</label>
                          <input type="date" name="to_date" id="to_date" class="form-control" placeholder="Search by year ..." />
                      </div>
                
                      <div class="form-group align-self-end col-auto">
                          <button class="btn btn-outline-danger px-3" type="reset" id="reset" style="">Reset</button>  
                      </div>
                      {{-- <div class="col-lg-1 col-md-1"></div> --}}

                    </div>
                    <div class="table-responsive">
                      <table class="table table-striped nowrap table-students-fee">
                      <thead>
                      <tr>
                          <th class="noVis">
                            <div class="custom-control custom-checkbox custom-checkbox1 d-inline-block empty-checkbox">
                              <input type="checkbox" class="custom-control-input check-all1" name="check_all" id="check-all">
                              <label class="custom-control-label" for="check-all"></label>
                            </div>
                          </th>
                          <th>Admission No.</th>
                          <th>Receipt</th>
                          <th>Name</th>
                          <th>Class</th>
                          <th>Gender</th>
                          <th>Father/Guardian Name</th>
                          <th>Fee(Inc Discount)</th>
                          <th>Paid Amount</th>
                          <th>Unpaid Amount</th>
                          <th>Discount</th>
                          <th>Fee Month</th>
                          <th>Submitted Date</th>
                          <th>Status</th>
                          
                      </tr>
                      </thead>
                      </table>
                    </div>
                  </div>
                </div>


                <div class="mt-4 pt-2">
                    <h1 class="page-title">Student Fee History By Year</h1>
                </div>                           


              <div class="card mt-3">
                <div class="card-body">
                  <div class="align-items-end form-row">
                    <div class="form-group col-lg-3 col-md-4 col-sm-4 col-6">
                        <label for="">Search by year</label>
                        <input type="number" name="year" id="year" class="form-control" value="{{carbon::now()->format('Y')}}" placeholder="Search by year ..." />
                    </div>
              
                    <div class="form-group col-auto">
                        <button class="btn btn-outline-primary px-md-4 px-3" type="button" id="apply_year" style="">Apply</button>  
                    </div>
                    <div class="form-group col-auto">
                        <button class="btn btn-outline-primary px-md-4 px-3" type="button" id="export_attendance" style="">Export</button>  
                    </div>
                    {{-- <div class="col-lg-1 col-md-1"></div> --}}

                  </div>
                  <table class="table table-striped nowrap table-students-fee-month">
                  <thead>
                  <tr>
                      <th>Month</th>
                      <th>Submitted Date</th>
                      <th>Submitted To</th>
                      <th>Total Amount</th>
                      <th>Paid Amount</th>
                      <th>Unpaid Amount</th>
                      <th>Status</th>
                      <th>Remark</th>
                  </tr>
                  </thead>
                  </table>
                </div>
            </div>
        
          <input type="hidden" name="fees_selected" class="fees_selected">
    

@endsection

@section('javascript')
<script type="text/javascript">
  $(document).ready(function(){

    $(document).on('click', '.check-all1', function () {
        if(this.checked == true){
        $('.check').prop('checked', true);
        $('.check').parents('tr').addClass('selected');
        var cb_length = $( ".check:checked" ).length;
        if(cb_length > 0){
          $('.selected-item').removeClass('d-none');
        }
      }else{
        $('.check').prop('checked', false);
        $('.check').parents('tr').removeClass('selected');
        $('.selected-item').addClass('d-none');
        
      }
    });

   $(document).on('click', '.check', function () {
    // $(this).removeClass('d-none');
        var cb_length = $( ".check:checked" ).length;
        var st_pieces = $(this).parents('tr').attr('data-pieces');
        if(this.checked == true){
        $('.selected-item').removeClass('d-none');
        $(this).parents('tr').addClass('selected');
      }else{
        $(this).parents('tr').removeClass('selected');
        if(cb_length == 0){
         $('.selected-item').addClass('d-none');
        }
        
      }
    });
  // $('.js-example-basic-single').select2();
  var student_id = "{{$student->id}}";
    var table2 =  $('.table-students-fee').DataTable({
    processing: true,
    searching: false,
    "language": {
    processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw" style="color:#13436c;"></i><span class="sr-only">Loading...</span> '},
    ordering: false,
    pageLength: {{50}},
    serverSide: true,
    autoWidth: false,
    "lengthMenu": [50,100,150,200],
    
    scrollX:true,
    scrollY : '90vh',
    scrollCollapse: true,
    ajax: 
    {
      beforeSend: function(){
        $('#loader_modal').modal({
          backdrop: 'static',
          keyboard: false
        });
        $('#loader_modal').modal('show');
      },
      url: "{!! route('get-single-student-fee') !!}",
       data: function(data) { data.student_classes_select = $('.student_classes_select option:selected').val(),data.discount_student = $('.discount_student option:selected').val(),data.status_student = $('.status_student option:selected').val(), data.from_date = $('#from_date').val(), data.to_date = $('#to_date').val(), data.student_id = student_id } ,
    },
    columns: [
        { data: 'checkbox', name: 'checkbox' }, 
        { data: 'roll_no', name: 'roll_no' }, 
        { data: 'invoice_no', name: 'invoice_no' }, 
        { data: 'name', name: 'name' }, 
        { data: 'class', name: 'class' }, 
        { data: 'gender', name: 'gender' }, 
        { data: 'guardian', name: 'guardian' },  
        { data: 'fee', name: 'fee' }, 
        { data: 'paid_amount', name: 'paid_amount' }, 
        { data: 'unpaid_amount', name: 'unpaid_amount' }, 
        { data: 'discount', name: 'discount' }, 
        { data: 'fee_month', name: 'fee_month' }, 
        { data: 'submitted_date', name: 'submitted_date' }, 
        { data: 'status', name: 'status' }, 
    ],
    initComplete: function () {
    // Enable THEAD scroll bars
    $('.dataTables_scrollHead').css('overflow', 'auto');
    // Sync THEAD scrolling with TBODY
      $('.dataTables_scrollHead').on('scroll', function () {
          $('.dataTables_scrollBody').scrollLeft($(this).scrollLeft());
      });
    },
    drawCallback: function(){
      $('#loader_modal').modal('hide');
    },
  });

    $(document).on('change','.student_classes_select',function(){
    $('.table-students-fee').DataTable().ajax.reload();
    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
  });

  $(document).on('change','.discount_student',function(){
    $('.table-students-fee').DataTable().ajax.reload();
    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
  });

   $(document).on('change','.status_student',function(){
    $('.table-students-fee').DataTable().ajax.reload();
    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
  });

   $(document).on('change','#from_date',function(){
    $('.table-students-fee').DataTable().ajax.reload();
    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
  });

   $(document).on('change','#to_date',function(){
    $('.table-students-fee').DataTable().ajax.reload();
    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
  });

  $(document).on('click','#reset',function(){
    $('.student_classes_select').val('');
    $('.discount_student').val('');
    $('.status_student').val('');
    $('#from_date').val('');
    $('#to_date').val('');
    $('.table-students-fee').DataTable().ajax.reload();
    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
  });

  var table2 =  $('.table-students-fee-month').DataTable({
    processing: true,
    searching: false,
    "language": {
    processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw" style="color:#13436c;"></i><span class="sr-only">Loading...</span> '},
    ordering: false,
    pageLength: {{50}},
    serverSide: true,
    autoWidth: false,
    "lengthMenu": [50,100,150,200],
    
    scrollX:true,
    scrollY : '90vh',
    scrollCollapse: true,
    ajax: 
    {
      beforeSend: function(){
        $('#loader_modal').modal({
          backdrop: 'static',
          keyboard: false
        });
        $('#loader_modal').modal('show');
      },
      url: "{!! route('get-single-student-fee-month') !!}",
       data: function(data) { data.student_id = student_id, data.year = $('#year').val() } ,
    },
    columns: [ 
        { data: 'month', name: 'month' }, 
        { data: 'submitted_date', name: 'submitted_date' }, 
        { data: 'submitted_to', name: 'submitted_to' }, 
        { data: 'total_amount', name: 'total_amount' }, 
        { data: 'paid_amount', name: 'paid_amount' }, 
        { data: 'unpaid_amount', name: 'unpaid_amount' }, 
        { data: 'status', name: 'status' }, 
        { data: 'remark', name: 'remark' }, 
    ],
    initComplete: function () {
    // Enable THEAD scroll bars
    $('.dataTables_scrollHead').css('overflow', 'auto');
    // Sync THEAD scrolling with TBODY
      $('.dataTables_scrollHead').on('scroll', function () {
          $('.dataTables_scrollBody').scrollLeft($(this).scrollLeft());
      });
    },
    drawCallback: function(){
      $('#loader_modal').modal('hide');
    },
  });

  $(document).on('click','#apply_year',function(){
    $('.table-students-fee-month').DataTable().ajax.reload();
    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
  });

    $(document).on('click', '#export_attendance', function(){ 
    var year = $('#year').val();
    // alert('student id '+student_id+' year '+year);
     var url = "{{url('user/fee-year-invoice-print')}}"+"/"+student_id+"/"+year;
      window.open(url, 'Orders Receivable Print', 'width=1200,height=600,scrollbars=yes');
  });

        $(document).on('click', '.download_invoice', function(){
    var selected_fees = [];
    $("input.check:checked").each(function() {
      selected_fees.push($(this).val());
    });

    if(selected_fees == ''){
            toastr.error('Error!', 'Select Fees First.',{"positionClass": "toast-bottom-right"});
        return false;
        }else{
        $('.fees_selected').val(selected_fees);

        var fees = $('.fees_selected').val();

        $.ajaxSetup({
              headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
              }
            });
            $.ajax({
              url: "{{ route('check-same-student') }}",
              method: 'post',
              data: {fee_ids:fees},
              beforeSend: function(){
                $('#loader_modal').modal({
                  backdrop: 'static',
                  keyboard: false
                });
                $('#loader_modal').modal('show');
              },
              success: function (response) {
                if(response.same_student > 1)
                {
                  toastr.info('Sorry!', 'Please select same student fee(s)!!!',{"positionClass": "toast-bottom-right"});
                  $('#loader_modal').modal('hide');
                }
                else
                {
                   var url = "{{url('user/fee-invoice-print')}}"+"/"+fees;
                    window.open(url, 'Orders Receivable Print', 'width=1200,height=600,scrollbars=yes');
                  $('#loader_modal').modal('hide');
                    
                }
               
              },
              error: function(request, status, error){
                // $('#loader_modal').modal('hide');
              }
            });
        // console.log(orders);
        // return false;
         // $('.export-account-received-form')[0].submit();
         // var url = "{{url('user/fee-invoice-print')}}"+"/"+fees;
          // window.open(url, 'Orders Receivable Print', 'width=1200,height=600,scrollbars=yes');
       }

    });

  });
</script>
@stop

