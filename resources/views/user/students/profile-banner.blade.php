          <div class="row mt-4 mb-2">
            <div class="col-4">
              <div class="row">
              <div class="col-6"><h4>Student Image</h4></div>
              </div>
            </div>
            <div class="col-4">
              <h4>Student Personal Information</h4>
            </div>
            <div class="col-4">
              <h4>Student Parent/Guardian Information</h4>
            </div>
          </div>
            <div class="row">
              <div class="col-lg-4 col-md-4 col-sm-4 col-4 bg-white p-2 student_image">
                <div class="student-image" style="background-image: url('{{asset('public/uploads/students/images/'.$student->image)}}')">
                  
                </div>
              </div>
              <div class="col-lg-4 col-md-4 col-sm-4 col-4 pl-2 pr-2">
                <div class="bg-white h-100 p-4">
                  <table style="width: 100%;font-size: 18px">
                    <tr>
                      <td>Admission No. :</td>
                      <td>
                        <span class="m-l-15 inputDoubleClick" id="roll_no"  data-fieldvalue="{{$student->roll_no}}">
                        {{$student->roll_no != null ? $student->roll_no : '--'}}
                      </span>
                        <input type="text" autocomplete="nope" name="roll_no" class="fieldFocus d-none form-control" value="{{(@$student->roll_no!=null)?$student->roll_no:''}}">
                      </td>
                    </tr>

                    <tr>
                      <td>Name :</td>
                      <td>
                        <span class="m-l-15 inputDoubleClick" id="roll_no"  data-fieldvalue="{{$student->name}}">
                        {{$student->name != null ? $student->name : '--'}}
                      </span>
                        <input type="text" autocomplete="nope" name="name" class="fieldFocus d-none form-control" value="{{(@$student->name!=null)?$student->name:''}}">
                      </td>
                    </tr>

                    <tr>
                      <td>Class :</td>
                      <td>{{$student->student_class != null ? $student->student_class->class_name : '--'}}</td>
                    </tr>

                    <tr>
                      <td>Gender :</td>
                      <td>{{$student->gender == 'male' ? 'Male' : ($student->gender == 'female' ? 'Female' : 'Other')}}</td>
                    </tr>

                    <tr>
                      <td> Date of Birth :</td>
                      <td>
                        <span class="m-l-15 inputDoubleClick" id="dob"  data-fieldvalue="{{$student->dob}}">
                        {{$student->dob != null ? $student->dob : '--'}}
                      </span>
                        <input type="date" autocomplete="nope" name="dob" class="fieldFocus d-none form-control" value="{{(@$student->dob!=null)?$student->dob:''}}">
                       
                      </td>
                    </tr>

                    <tr>
                      <td>Date of Admission</td>
                      <td>{{$student->date_of_admission}}</td>
                    </tr>

                    <tr>
                      <td>Address:</td>
                      <td>{{$student->address}}</td>
                    </tr>

                    <tr>
                      <td>Phone :</td>
                      <td>
                        <span class="m-l-15 inputDoubleClick" id="roll_no"  data-fieldvalue="{{$student->std_phone}}">
                        {{$student->std_phone != null ? $student->std_phone : '--'}}
                      </span>
                        <input type="number" autocomplete="nope" name="std_phone" class="fieldFocus d-none form-control" value="{{(@$student->std_phone!=null)?$student->std_phone:''}}">
                        
                      </td>
                    </tr>

                    <tr>
                      <td>Fee(Inc Discount) :</td>
                      <td>
                   
                        {{$student->fee != null ? $student->fee : 0}}
                        
                      </td>
                    </tr>

                    <tr>
                      <td>Discount :</td>
                      <td>
                        <span class="m-l-15 inputDoubleClick" id="discount"  data-fieldvalue="{{$student->discount}}">
                        {{$student->discount != null ? $student->discount : '--'}} %
                      </span>
                        <input type="number" autocomplete="nope" name="discount" class="fieldFocus d-none form-control" value="{{(@$student->discount!=null)?$student->discount:''}}">
                        
                         </td>
                    </tr>
                  </table>
                </div>
              </div>

              <div class="col-lg-4 col-md-4 col-sm-4 col-4 pl-2 pr-2">
                <div class="bg-white h-100 p-4">
                  <table style="width: 100%;font-size: 18px">
                    <tr>
                      <td>Parent/Guardian Name :</td>
                      <td>
                        <span class="m-l-15 inputDoubleClick" id="guardian"  data-fieldvalue="{{$student->guardian}}">
                        {{$student->guardian != null ? $student->guardian : '--'}}
                      </span>
                        <input type="text" autocomplete="nope" name="guardian" class="fieldFocus d-none form-control" value="{{(@$student->guardian!=null)?$student->guardian:''}}">
                        
                      </td>
                    </tr>

                    <tr>
                      <td>Parent/Guardian Cnic :</td>
                      <td>
                        <span class="m-l-15 inputDoubleClick" id="guardian_cnic"  data-fieldvalue="{{$student->guardian_cnic}}">
                        {{$student->guardian_cnic != null ? $student->guardian_cnic : '--'}}
                      </span>
                        <input type="text" autocomplete="nope" name="guardian_cnic" class="fieldFocus d-none form-control" value="{{(@$student->guardian_cnic!=null)?$student->guardian_cnic:''}}">
                        </td>
                    </tr>

                    <tr>
                      <td>Parent/Guardian Phone No. :</td>
                      <td>
                        <span class="m-l-15 inputDoubleClick" id="guardian_phone"  data-fieldvalue="{{$student->guardian_phone}}">
                        {{$student->guardian_phone != null ? $student->guardian_phone : '--'}}
                      </span>
                        <input type="text" autocomplete="nope" name="guardian_phone" class="fieldFocus d-none form-control" value="{{(@$student->guardian_phone!=null)?$student->guardian_phone:''}}">
                       
                      </td>
                    </tr>
                  </table>
                </div>
              </div>
            </div>