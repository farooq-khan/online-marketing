@extends('user.layouts.layout')

@section('title','Single Student')

@section('content')
<style type="text/css">
   .student-image
   {
    padding-top: 100%;
    background-position: center;
    background-size: cover;
   }

   .student_image
   {
    box-shadow: 0px 0px 15px 0px rgba(0,0,0,0.5);

   }
</style>


                    <div class="row page-title-row">
                        <div class="col-md col-sm col-12 page-title-col align-self-center">
                            <h1 class="page-title">Student profile</h1>
                        </div>
                    </div>
                    @php
                     $inputdblClickclass = Auth::user()->role == 'admin' ? 'inputDoubleClick' : '';
                    @endphp
                      <div class="card">
                        <div class="card-body">
                         
                          <div class="row mx-0 py-md-4 py-3 px-md-2 profile-header rounded-lg align-items-center">
                            <div class="col-sm col-12 mb-2 pb-1 profile-image-col mx-auto">
                              <div class="overflow-hidden profile-image-border rounded-circle">
                                @if(file_exists(asset('public/uploads/students/images/'.$student->image)))
                                <img src="{{asset('public/uploads/students/images/'.$student->image)}}" alt="" class="img-fluid">
                                @else
                                <img src="{{asset('public/assets/img/user-avatar.png')}}" alt="" class="img-fluid">
                                @endif
                              </div>
                            </div>
                            <div class="col-sm col-12 profile-name-col">
                              <div class="row align-items-center text-sm-left text-center">
                                <div class="col-sm col-12 mb-sm-0 mb-2">
                                  <h5 class="mb-1 student-name mb-0">
                                    <span class="m-l-15 {{$inputdblClickclass}}" id="roll_no"  data-fieldvalue="{{$student->name}}">
                                      {{$student->name != null ? $student->name : '--'}}
                                    </span>
                                    @if (Auth::user()->role == 'admin')
                                    <input type="text" autocomplete="nope" name="name" class="fieldFocus d-none form-control" value="{{(@$student->name!=null)?$student->name:''}}">
                                    @endif
                                  </h5>
                                  <p class="mb-0">Class :
                                      <span class="m-l-15 inputDoubleClick" id="class_id"  data-fieldvalue="{{$student->class_id}}">
                                        {{$student->student_class->class_name ?? '--'}}
                                      </span>
                                      <select class="form-control form-control-sm fieldFocus d-none" name="class_id" data-fieldvalue="{{ $student->class_id }}">
                                      <option>--Select Class--</option>
                                      @foreach($classes as $class)
                                        <option value="{{ $class->id }}" {{ $class->id == $student->class_id ? 'selected' : null }}>{{ $class->class_name }}</option>
                                      @endforeach
                                    </select>
                                     | Admission No. :<span class="m-l-15" id="roll_no"  data-fieldvalue="{{$student->roll_no}}">
                                    {{$student->roll_no != null ? $student->roll_no : '--'}}
                                  </span>
                                </p>
                                </div>
                                  @if (Auth::user()->role == 'admin')
                                    <div class="col-sm-auto col-12 image-upload-col text-sm-right">
                                      <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#updateImage">+Update Image</button>
                                    </div>
                                  @endif
                              </div>
                            </div>   
                          </div>
                        </div>
                      </div>
                    
  
                        <div class="row">

 
                          <div class="col-12 mt-4 student-info">
                            <div class="card">
                            <div class="card-header card-border-top">
                              <h5 class="mb-0">Student Details</h5>
                            </div>
                            <div class="card-body">
                            <div class="student-info-body">

                            <!-- Nav tabs -->
                            <ul class="nav nav-fill nav-tabs" id="myTab" role="tablist">
                              <li class="nav-item" role="presentation">
                                <button class="nav-link active" id="personal-tab" data-toggle="tab" data-target="#personal" type="button" role="tab" aria-controls="personal" aria-selected="true">Personal Info</button>
                              </li>
                              <li class="nav-item" role="presentation">
                                <button class="nav-link" id="parent-detail-tab" data-toggle="tab" data-target="#parent-tab" type="button" role="tab" aria-controls="parent-tab" aria-selected="false">Parent/Guardian Info</button>
                              </li>
                              <li class="nav-item" role="presentation">
                                <button class="nav-link" id="fee-tab" data-toggle="tab" data-target="#fee" type="button" role="tab" aria-controls="fee" aria-selected="false">Fee Info</button>
                              </li>
                              <li class="nav-item" role="presentation">
                                <button class="nav-link" id="history-tab" data-toggle="tab" data-target="#history" type="button" role="tab" aria-controls="history" aria-selected="false">Academic History</button>
                              </li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                              {{-- Personal detail tabs starts here --}}
                              <div class="tab-pane active" id="personal" role="tabpanel" aria-labelledby="personal-tab">
                                <div class="row mx-sm-n3 mx-n2">
                                  <div class="col-lg-4 col-md-4 col-sm-6 col-6 mb-3 student-info-col px-sm-3 px-2">
                                    <h6 class="student-info-title">Admission No. :</h6>
                                    <p class="mb-0">
                                      <span class="m-l-15" id="roll_no"  data-fieldvalue="{{$student->roll_no}}">
                                      {{$student->roll_no != null ? $student->roll_no : '--'}}
                                    </span>
  
                                    </p>
                                  </div>
  
                                  <div class="col-lg-4 col-md-4 col-sm-6 col-6 mb-3 student-info-col px-sm-3 px-2">
                                    <h6 class="student-info-title">Name :</h6>
                                    <p class="mb-0">
                                      <span class="m-l-15 {{$inputdblClickclass}}" id="roll_no"  data-fieldvalue="{{$student->name}}">
                                      {{$student->name != null ? $student->name : '--'}}
                                    </span>
                                    @if (Auth::user()->role == 'admin')
                                    <input type="text" autocomplete="nope" name="name" class="fieldFocus d-none form-control" value="{{(@$student->name!=null)?$student->name:''}}">
                                    @endif
                                    </p>
                                  </div>
  
                                  <div class="col-lg-4 col-md-4 col-sm-6 col-6 mb-3 student-info-col px-sm-3 px-2">
                                    <h6 class="student-info-title">Class :</h6>
                                    {{-- <p class="mb-0">
                                      @if (Auth::user()->role == 'admin')
                                    <select class="form-control form-control-sm change-student-class" name="class_id" data-fieldvalue="{{ $student->class_id }}" readonly>
                                      <option>--Select Class--</option>
                                      @foreach($classes as $class)
                                        <option value="{{ $class->id }}" {{ $class->id == $student->class_id ? 'selected' : null }}>{{ $class->class_name }}</option>
                                      @endforeach
                                    </select>
                                    @else
                                    {{$student->student_class != null ? $student->student_class->class_name : '--'}}
                                    @endif
                                    </p> --}}
  
                                    <p class="mb-0">
                                      <span class="m-l-15 inputDoubleClick" id="class_id"  data-fieldvalue="{{$student->class_id}}">
                                        {{$student->student_class->class_name ?? '--'}}
                                      </span>
                                      <select class="form-control form-control-sm fieldFocus d-none" name="class_id" data-fieldvalue="{{ $student->class_id }}">
                                      <option>--Select Class--</option>
                                      @foreach($classes as $class)
                                        <option value="{{ $class->id }}" {{ $class->id == $student->class_id ? 'selected' : null }}>{{ $class->class_name }}</option>
                                      @endforeach
                                    </select>
                                    </p>
                                  </div>
  
                                  <div class="col-lg-4 col-md-4 col-sm-6 col-6 mb-3 student-info-col px-sm-3 px-2">
                                    <h6 class="student-info-title">Gender :</h6>
                                    <p class="mb-0">
                                      <span class="m-l-15 inputDoubleClick" id="gender"  data-fieldvalue="{{$student->gender}}">
                                        {{$student->gender == 'male' ? 'Male' : ($student->gender == 'female' ? 'Female' : 'Other')}}
                                      </span>
                                      <select class="fieldFocus d-none form-control" name="gender">
                                        <option value="">---Select Gender---</option>
                                        <option value="male" {{$student->gender == 'male' ? 'selected' : ''}}>Male</option>
                                        <option value="female" {{$student->gender == 'female' ? 'selected' : ''}}>Female</option>
                                      </select>
                                    </p>
                                  </div>
  
                                  <div class="col-lg-4 col-md-4 col-sm-6 col-6 mb-3 student-info-col px-sm-3 px-2">
                                    <h6 class="student-info-title">Date of Birth :</h6>
                                    <p class="mb-0">
                                      <span class="m-l-15 {{$inputdblClickclass}}" id="dob"  data-fieldvalue="{{$student->dob}}">
                                      {{$student->dob != null ? $student->dob : '--'}}
                                    </span>
                                    @if (Auth::user()->role == 'admin')
                                    <input type="date" autocomplete="nope" name="dob" class="fieldFocus d-none form-control" value="{{(@$student->dob!=null)?$student->dob:''}}">
                                    @endif
  
                                    </p>
                                  </div>
  
                                  <div class="col-lg-4 col-md-4 col-sm-6 col-6 mb-3 student-info-col px-sm-3 px-2">
                                    <h6 class="student-info-title">Date of Admission :</h6>
                                    <p class="mb-0">
                                      <!-- {{$student->date_of_admission}} -->
                                      <span class="m-l-15 {{$inputdblClickclass}}" id="date_of_admission"  data-fieldvalue="{{$student->date_of_admission}}">
                                      {{$student->date_of_admission != null ? $student->date_of_admission : '--'}}
                                    </span>
                                    @if (Auth::user()->role == 'admin')
                                    <input type="date" autocomplete="nope" name="date_of_admission" class="fieldFocus d-none form-control" value="{{(@$student->date_of_admission!=null)?$student->date_of_admission:''}}">
                                    @endif
                                    </p>
                                  </div>
  
                                  <div class="col-lg-4 col-md-4 col-sm-6 col-6 mb-3 student-info-col px-sm-3 px-2">
                                    <h6 class="student-info-title">Address :</h6>
                                    <p class="mb-0">
                                      <!-- {{$student->address}} -->
                                       <span class="m-l-15 {{$inputdblClickclass}}" id="address"  data-fieldvalue="{{$student->address}}">
                                      {{$student->address != null ? $student->address : '--'}}
                                    </span>
                                    @if (Auth::user()->role == 'admin')
                                    <input type="text" autocomplete="nope" name="address" class="fieldFocus d-none form-control" value="{{(@$student->address!=null)?$student->address:''}}">
                                    @endif
                                    </p>
                                  </div>
  
                                  <div class="col-lg-4 col-md-4 col-sm-6 col-6 mb-3 student-info-col px-sm-3 px-2">
                                    <h6 class="student-info-title">Phone :</h6>
                                    <p class="mb-0">
                                      <span class="m-l-15 {{$inputdblClickclass}}" id="roll_no"  data-fieldvalue="{{$student->std_phone}}">
                                      {{$student->std_phone != null ? $student->std_phone : '--'}}
                                    </span>
                                    @if (Auth::user()->role == 'admin')
                                    <input type="number" autocomplete="nope" name="std_phone" class="fieldFocus d-none form-control" value="{{(@$student->std_phone!=null)?$student->std_phone:''}}">
                                    @endif
  
                                    </p>
                                  </div>
  
                                  <div class="col-lg-4 col-md-4 col-sm-6 col-6 mb-3 student-info-col px-sm-3 px-2">
                                    <h6 class="student-info-title">Fee(Inc Discount) :</h6>
                                    <p class="mb-0">
  
                                      {{$student->fee != null ? $student->fee : 0}}
  
                                    </p>
                                  </div>
  
                                  <div class="col-lg-4 col-md-4 col-sm-6 col-6 mb-3 student-info-col px-sm-3 px-2">
                                    <h6 class="student-info-title">Discount :</h6>
                                    <p class="mb-0">
                                      <span class="m-l-15" id="discount"  data-fieldvalue="{{$student->discount}}">
                                      {{$student->discount != null ? $student->discount : '--'}} %
                                    </span>
                                    @if (Auth::user()->role == 'admin')
                                    <input type="number" autocomplete="nope" name="discount" class="fieldFocus d-none form-control" value="{{(@$student->discount!=null)?$student->discount:''}}">
                                    @endif
  
                                       </p>
                                  </div>
                                  <div class="col-lg-4 col-md-4 col-sm-6 col-6 mb-3 student-info-col px-sm-3 px-2">
                                    <h6 class="student-info-title">Discount Amount :</h6>
                                    <p class="mb-0">
                                      <span class="m-l-15 {{$inputdblClickclass}}" id="discount_amount"  data-fieldvalue="{{$student->discount_amount}}">
                                      {{$student->discount_amount != null ? $student->discount_amount : '--'}}
                                    </span>
                                    @if (Auth::user()->role == 'admin')
                                    <input type="number" autocomplete="nope" name="discount_amount" class="fieldFocus d-none form-control" value="{{(@$student->discount_amount!=null)?$student->discount_amount:''}}">
                                    @endif
  
                                       </p>
                                  </div>
  
                                  <div class="col-lg-4 col-md-4 col-sm-6 col-6 mb-3 student-info-col px-sm-3 px-2">
                                    <h6 class="student-info-title">Status :</h6>
                                    <p class="mb-0">
                                      <span class="m-l-15" id="status"  data-fieldvalue="{{$student->status}}">
                                      @if($student->status == 1)
                                      <span>Active</span>
                                      @else
                                      <span style="color: red;font-style: italic;"><b>Suspended</b></span>
                                      @endif
                                    </span>
                                       </p>
                                  </div>
                                  {{-- <div class="col-lg-4 col-md-4 col-sm-6 col-6 mb-3 student-info-col px-sm-3 px-2">
                                    <p class="mb-0">Promotion : </p>
                                    <p class="mb-0">{{$std_cl}}</td>
                                  </div> --}}
                                  <div class="col-12 mb-1 px-sm-3 px-2">
                                    <h6 class="mb-0">WithDrawal No.</td>
                                  </div>
                                  <div class="col-lg-4 col-md-4 col-sm-6 col-6 mb-3 student-info-col px-sm-3 px-2">
                                    <h6 class="student-info-title">Primary :</h6>
                                    <p class="mb-0">
                                       <span class="m-l-15 {{$inputdblClickclass}}" id="primary"  data-fieldvalue="{{$student->primary_withdrawal_no}}">
                                          {{$student->primary_withdrawal_no != null ? $student->primary_withdrawal_no : '--'}}
                                        </span>
                                        @if (Auth::user()->role == 'admin')
                                        <input type="text" autocomplete="nope" name="primary_withdrawal_no" class="fieldFocus d-none form-control" value="{{(@$student->primary_withdrawal_no!=null)?$student->primary_withdrawal_no:''}}">
                                        @endif
                                    </p>
                                  </div>
  
                                  <div class="col-lg-4 col-md-4 col-sm-6 col-6 mb-3 student-info-col px-sm-3 px-2">
                                    <h6 class="student-info-title">Middle :</h6>
                                    <p class="mb-0">
                                       <span class="m-l-15 {{$inputdblClickclass}}" id="middle"  data-fieldvalue="{{$student->middle_withdrawal_no}}">
                                          {{$student->middle_withdrawal_no != null ? $student->middle_withdrawal_no : '--'}}
                                        </span>
                                        @if (Auth::user()->role == 'admin')
                                        <input type="text" autocomplete="nope" name="middle_withdrawal_no" class="fieldFocus d-none form-control" value="{{(@$student->middle_withdrawal_no!=null)?$student->middle_withdrawal_no:''}}">
                                        @endif
                                    </p>
                                  </div>
                                  <div class="col-lg-4 col-md-4 col-sm-6 col-6 mb-3 student-info-col px-sm-3 px-2">
                                    <h6 class="student-info-title">High :</h6>
                                    <p class="mb-0">
                                       <span class="m-l-15 {{$inputdblClickclass}}" id="high"  data-fieldvalue="{{$student->high_withdrawal_no}}">
                                          {{$student->high_withdrawal_no != null ? $student->high_withdrawal_no : '--'}}
                                        </span>
                                        @if (Auth::user()->role == 'admin')
                                        <input type="text" autocomplete="nope" name="high_withdrawal_no" class="fieldFocus d-none form-control" value="{{(@$student->high_withdrawal_no!=null)?$student->high_withdrawal_no:''}}">
                                        @endif
                                    </p>
                                  </div>
                                </div>
                              </div>
                              {{-- Personal detail tabs ends here --}}
                              {{-- parent-detail tabs start here --}}
                              <div class="tab-pane" id="parent-tab" role="tabpanel" aria-labelledby="parent-detail-tab">
                                <div class="row mx-sm-n3 mx-n2">
                                  <div class="col-lg-4 col-md-4 col-sm-6 col-6 mb-3 student-info-col px-sm-3 px-2">
                                    <h6 class="student-info-title">Parent/Guardian Name :</h6>
                                    <p class="mb-0">
                                      <span class="m-l-15 {{$inputdblClickclass}}" id="guardian"  data-fieldvalue="{{$student->guardian}}">
                                      {{$student->guardian != null ? $student->guardian : '--'}}
                                    </span>
                                    @if (Auth::user()->role == 'admin')
                                    <input type="text" autocomplete="nope" name="guardian" class="fieldFocus d-none form-control" value="{{(@$student->guardian!=null)?$student->guardian:''}}">
                                    @endif
                                    </p>
                                  </div>
                                  <div class="col-lg-4 col-md-4 col-sm-6 col-6 mb-3 student-info-col px-sm-3 px-2">
                                    <h6 class="student-info-title">Parent/Guardian NickName :</h6>
                                    <p class="mb-0">
                                      <span class="m-l-15 {{$inputdblClickclass}}" id="guardian_nickname"  data-fieldvalue="{{$student->guardian_nickname}}">
                                      {{$student->guardian_nickname != null ? $student->guardian_nickname : '--'}}
                                    </span>
                                    @if (Auth::user()->role == 'admin')
                                    <input type="text" autocomplete="nope" name="guardian_nickname" class="fieldFocus d-none form-control" value="{{(@$student->guardian_nickname!=null)?$student->guardian_nickname:''}}">
                                    @endif
                                    </p>
                                  </div>
                                  <div class="col-lg-4 col-md-4 col-sm-6 col-6 mb-3 student-info-col px-sm-3 px-2">
                                    <h6 class="student-info-title">Parent/Guardian Cnic :</h6>
                                    <p class="mb-0">
                                      <span class="m-l-15 {{$inputdblClickclass}}" id="guardian_cnic"  data-fieldvalue="{{$student->guardian_cnic}}">
                                      {{$student->guardian_cnic != null ? $student->guardian_cnic : '--'}}
                                    </span>
                                    @if (Auth::user()->role == 'admin')
                                    <input type="text" autocomplete="nope" name="guardian_cnic" class="fieldFocus d-none form-control" value="{{(@$student->guardian_cnic!=null)?$student->guardian_cnic:''}}">
                                    @endif
                                      </p>
                                  </div>
                                  <div class="col-lg-4 col-md-4 col-sm-6 col-6 mb-3 student-info-col px-sm-3 px-2">
                                    <h6 class="student-info-title">Parent/Guardian Phone No. :</h6>
                                    <p class="mb-0">
                                      <span class="m-l-15 {{$inputdblClickclass}}" id="guardian_phone"  data-fieldvalue="{{$student->guardian_phone}}">
                                      {{$student->guardian_phone != null ? $student->guardian_phone : '--'}}
                                    </span>
                                    @if (Auth::user()->role == 'admin')
                                    <input type="text" autocomplete="nope" name="guardian_phone" class="fieldFocus d-none form-control" value="{{(@$student->guardian_phone!=null)?$student->guardian_phone:''}}">
                                    @endif
                                    </p>
                                  </div>
                                </div>
                              </div>
                              {{-- parent-detail tabs ends here --}}
                              {{-- fee-detail tabs starts here --}}
                              <div class="tab-pane" id="fee" role="tabpanel" aria-labelledby="fee-tab">
                                <div class="row mx-sm-n3 mx-n2">
                                  <div class="col-12 mb-2 pb-1 px-sm-3 px-2">
                                    <h6 class="mb-0">Tuition Fee :</h6>
                                  </div>
                                  <div class="col-lg-4 col-md-4 col-sm-6 col-6 mb-3 student-info-col px-sm-3 px-2">
                                    <h6 class="student-info-title">Total Tuition Fee :</h6>
                                    <p class="mb-0">{{number_format($student_total_fee,2,'.',',')}}</p>
                                  </div>
                                  <div class="col-lg-4 col-md-4 col-sm-6 col-6 mb-3 student-info-col px-sm-3 px-2">
                                    <h6 class="student-info-title">Paid Tuition Fee :</h6>
                                    <p class="mb-0">{{number_format($student_paid_fee,2,'.',',')}}</p>
                                  </div>
                                  <div class="col-lg-4 col-md-4 col-sm-6 col-6 mb-3 student-info-col px-sm-3 px-2">
                                    <h6 class="student-info-title">Concession in Tuition Fee :</h6>
                                    <p class="mb-0">{{number_format($tuition_fee_concession,2,'.',',')}}</p>
                                  </div>
                                  <div class="col-lg-4 col-md-4 col-sm-6 col-6 mb-3 student-info-col px-sm-3 px-2">
                                    <h6 class="student-info-title">Unpaid Tuition Fee :</h6>
                                    <p class="mb-0"><b class="text-danger">{{number_format($student_unpaid_fee,2,'.',',')}}</b></p>
                                  </div>
                                  <div class="col-12">
                                    <hr class="mb-3 mt-0">
                                  </div>
                                  <div class="col-12 mb-2 pb-1 px-sm-3 px-2">
                                    <h6 class="mb-0">Other Fee :</h6>
                                  </div>
                                  <div class="col-lg-4 col-md-4 col-sm-6 col-6 mb-3 student-info-col px-sm-3 px-2">
                                    <h6 class="student-info-title">Total Other Fee :</h6>
                                    <p class="mb-0">{{number_format($student_total_other_fee,2,'.',',')}}</p>
                                  </div>
                                  <div class="col-lg-4 col-md-4 col-sm-6 col-6 mb-3 student-info-col px-sm-3 px-2">
                                    <h6 class="student-info-title">Total Paid Other Fee :</h6>
                                    <p class="mb-0">{{number_format($student_paid_other_fee,2,'.',',')}}</p>
                                  </div>
                                  <div class="col-lg-4 col-md-4 col-sm-6 col-6 mb-3 student-info-col px-sm-3 px-2">
                                    <h6 class="student-info-title">Total Concession in Other Fee :</h6>
                                    <p class="mb-0">{{number_format($other_fee_concession,2,'.',',')}}</p>
                                  </div>
                                  <div class="col-lg-4 col-md-4 col-sm-6 col-6 mb-3 student-info-col px-sm-3 px-2">
                                    <h6 class="student-info-title">Other Fee Unpaid Amount :</h6>
                                    <p class="mb-0"><b class="text-danger">{{number_format($other_unpaid_fee,2,'.',',')}}</b></p>
                                  </div>
                                  <div class="col-12">
                                    <hr class="mb-3 mt-0">
                                  </div>
                                  <div class="col-12 mb-2 pb-1 px-sm-3 px-2">
                                    <h6 class="mb-0">Total Fee :</h6>
                                  </div>
                                  <div class="col-lg-4 col-md-4 col-sm-6 col-6 mb-3 student-info-col px-sm-3 px-2">
                                    <h6 class="student-info-title">Total Fee So Far :</h6>
                                    <p class="mb-0">{{number_format($student_total_fee + $student_total_other_fee,2,'.',',')}}</p>
                                  </div>
                                  <div class="col-lg-4 col-md-4 col-sm-6 col-6 mb-3 student-info-col px-sm-3 px-2">
                                    <h6 class="student-info-title">Total Paid Fee So Far :</h6>
                                    <p class="mb-0">{{number_format($student_paid_fee + $student_paid_other_fee,2,'.',',')}}</p>
                                  </div>
                                  <div class="col-lg-4 col-md-4 col-sm-6 col-6 mb-3 student-info-col px-sm-3 px-2">
                                    <h6 class="student-info-title">Total Concession So Far :</h6>
                                    <p class="mb-0"><b class="text-danger">{{number_format($tuition_fee_concession + $other_fee_concession,2,'.',',')}}</b></p>
                                  </div>
                                  <div class="col-lg-4 col-md-4 col-sm-6 col-6 mb-3 student-info-col px-sm-3 px-2">
                                    <h6 class="student-info-title">Total Unpaid So Far :</h6>
                                    <p class="mb-0"><b class="text-danger">{{number_format($student_unpaid_fee + $other_unpaid_fee,2,'.',',')}}</b></p>
                                  </div>
                                </div>
                              </div>
                              {{-- fee-detail tabs ends here --}}

                              {{-- history-detail tabs starts here --}}
                              <div class="tab-pane" id="history" role="tabpanel" aria-labelledby="history-tab">
                                <ul class="mb-0 list-unstyled">
                                  <li class="list-inline-item"><a class="btn btn-outline-primary" target="_blank" href="{{url('user/student-fee/'.$student->id.'/'.$student->class_id)}}">Tuition Fee History</a></li>
                                  <li class="list-inline-item"><a class="btn btn-outline-primary" target="_blank" href="{{url('user/other-fee-history/'.$student->id)}}">Other Fee History</a></li>
                                  <li class="list-inline-item"><a class="btn btn-outline-primary" target="_blank" href="{{url('user/student-exam-history/'.$student->id.'/'.$student->class_id)}}">Exam History</a></li>
                                  <li class="list-inline-item"><a class="btn btn-outline-primary" target="_blank" href="{{url('user/student-test-history/'.$student->id.'/'.$student->class_id)}}">Tests History</a></li>
                                  <li class="list-inline-item"><a class="btn btn-outline-primary" target="_blank" href="{{url('user/student-attendance/'.$student->id.'/'.$student->class_id)}}">Attendance History</a></li>
                                </ul>
                              </div>
                              {{-- history-detail tabs starts here --}}
                            </div>
                            {{-- tabs end here --}}
                            </div>
                            </div>
                            </div>
                          </div>
                        </div>
                        {{-- Row Ends here --}}
                        
                        {{-- Student family starts here --}}
                        <div class="card mt-4">
                          <div class="card-header card-border-top d-flex justify-content-between align-items-center">
                            <h5 class="mb-0">Student Family</h5>
                            @if (Auth::user()->role == 'admin')
                            <div class="ml-auto text-right page-action-button">
                                <button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#addStudentFamily">+Add Student Family</button>
                            </div>
                            @endif
                          </div>
                          <div class="card-body">
                            <table class="table table-striped nowrap table-family">
                              <thead>
                              <tr>
                                <th>Family Member</th>
                                <th>Father/Guardian</th>
                                <th>Class</th>
                                <th>Relation</th>
                                <th>Action</th>
                              </tr>
                              </thead>
                              </table>
                          </div>
                        </div>
                        {{-- Student family ends here --}}
    
          


<!--Add Student Modal -->
<div class="modal fade" id="addStudentFamily" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add Student Family</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form id="add-student-family-form" method="POST" enctype="multipart/form-data">
        <input type="hidden" name="std_id" value="{{$student->id}}">
      <div class="modal-body">

          <div class="form-row">
            <div class="form-group col-md-6">
                <label for="student_class">Class</label>
                <select class="form-control" id="student_class" name="student_class">
                  <option value="">--Select Class--</option>
                  @foreach($classes as $class)
                  <option value="{{$class->id}}"> {{$class->class_name}} </option>
                  @endforeach
                </select>
              </div>

               <div class="form-group col-md-6">
                <label for="name">Select Father/Guardian</label>
                <select class="form-control selecting-students-guardian state-tags" id="student_guardian" name="member" disabled="true">

                </select>
            </div>

            <hr>

            <div class="form-group col-md-6">
                <label for="name">Select Family Member</label>
                <select class="form-control selecting-students state-tags" name="member" disabled="true">

                </select>
            </div>
            <div class="form-group col-md-6">
                <label for="relation">Relation</label>
                <input type="text" name="relation" id="relation" class="form-control" placeholder="Enter Student Relation..."/>
            </div>
          </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save</button>
      </div>
      </form>
    </div>
  </div>
</div>

<!--Add Student Modal -->
<div class="modal fade" id="updateImage" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Update Student Image</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form id="add-student-photo" method="POST" enctype="multipart/form-data">
        <input type="hidden" name="std_id" value="{{$student->id}}">
      <div class="modal-body">

          <div class="form-row">
            <div class="form-group col-md-12">
                <label for="relation">Upload Photo</label>
                <input type="file" name="image" id="image" class="form-control" accept="image/*"/>
            </div>
          </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary update_image" disabled="true">Update</button>
      </div>
      </form>
    </div>
  </div>
</div>
@endsection

@section('javascript')
<script type="text/javascript">
$(document).ready(function(){
  var std_id = "{{$student->id}}";
   // to make fields double click editable
  $(document).on("dblclick",".inputDoubleClick",function(){
    $(this).addClass('d-none');
    $(this).next().removeClass('d-none');
    $(this).next().addClass('active');
    $(this).next().focus();
  });

  $(document).on("dblclick",".change-student-class",function(){
    $(this).attr('readonly', false);
  });

    $(document).on('keypress keyup focusout',".fieldFocus",function(e) {
    // alert('hi');
    if (e.keyCode === 27 && $(this).hasClass('active')) {
      var fieldvalue = $(this).prev().data('fieldvalue');
      var thisPointer = $(this);
      thisPointer.addClass('d-none');
      thisPointer.val(fieldvalue);
      thisPointer.removeClass('active');
      thisPointer.prev().removeClass('d-none');
    }

    var name = $(this).attr('name');
    // alert(name);

    if( (e.keyCode === 13 || e.which === 0) && $(this).val().length > 0 && $(this).hasClass('active')){
      var str = $(this).val();
      // if(($(this).val().length < 2 ||  !str.replace(/\s/g, '').length))
      // {
      //   swal({ html:true, title:'Alert !!!', text:'<b>Please Enter At Least 2 Characters (this string may only contain white spaces)!!!</b>'});
      //   return false;
      // }
      if(true)
      {
        $(this).removeClass('active');
        var fieldvalue = $(this).prev().data('fieldvalue');
        var new_value = $(this).val();
        // alert(new_value);
        if(fieldvalue == new_value)
        {
          var thisPointer = $(this);
          thisPointer.addClass('d-none');
          thisPointer.prev().removeClass('d-none');
          $(this).prev().html(fieldvalue);
        }
        else
        {
          var thisPointer = $(this);
          thisPointer.addClass('d-none');
          thisPointer.prev().removeClass('d-none');
          if(new_value != '')
          {
            $(this).prev().html(new_value);
          }
          $(this).prev().data('fieldvalue', new_value);
          saveStudentData(thisPointer,thisPointer.attr('name'), thisPointer.val(),fieldvalue);
        }
      }
    }

  });

    $(document).on('change',".change-student-class",function(e) {
    // alert('hi');
    // if (e.keyCode === 27 ) {
    //   var fieldvalue = $(this).data('fieldvalue');
    //   var thisPointer = $(this);
    //   thisPointer.addClass('d-none');
    //   thisPointer.val(fieldvalue);
    //   thisPointer.removeClass('active');
    //   thisPointer.prev().removeClass('d-none');
    // }

    var name = $(this).attr('name');
    // alert(name);

    // if( (e.keyCode === 13 || e.which === 0) && $(this).val().length > 0){
      var str = $(this).val();
      // if(($(this).val().length < 2 ||  !str.replace(/\s/g, '').length))
      // {
      //   swal({ html:true, title:'Alert !!!', text:'<b>Please Enter At Least 2 Characters (this string may only contain white spaces)!!!</b>'});
      //   return false;
      // }
      if(true)
      {
        // $(this).removeClass('active');
        var fieldvalue = $(this).data('fieldvalue');
        var new_value = $(this).val();
        // alert(new_value);
        if(fieldvalue == new_value)
        {
          var thisPointer = $(this);
          // thisPointer.addClass('d-none');
          // thisPointer.prev().removeClass('d-none');
          // $(this).prev().html(fieldvalue);
        }
        else
        {
          var thisPointer = $(this);
          // thisPointer.addClass('d-none');
          // thisPointer.prev().removeClass('d-none');
          // if(new_value != '')
          // {
          //   $(this).prev().html(new_value);
          // }
          $(this).data('fieldvalue', new_value);
          saveStudentData(thisPointer,thisPointer.attr('name'), thisPointer.val(),fieldvalue);
        }
      }
    // }

  });

      function saveStudentData(thisPointer,field_name,field_value,new_select_value){
      var id = "{{$student->id}}";

      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
      });
      $.ajax({
        method: "post",

        url: "{{ url('user/update-student-data') }}",
        dataType: 'json',
        data: 'id='+id+'&'+field_name+'='+encodeURIComponent(field_value)+'&'+'new_select_value'+'='+encodeURIComponent(new_select_value),
        beforeSend: function(){
          $('#loader_modal').modal({
            backdrop: 'static',
            keyboard: false
          });
          $('#loader_modal').modal('show');
        },
        success: function(data)
        {
          // alert(field_name);
          $("#loader_modal").modal('hide');
          if(data.success == true)
          {
            $(this).attr('readonly', true);

            toastr.success('Success!', 'Information updated successfully.',{"positionClass": "toast-bottom-right"});
            // return true;
          }
          if(field_name === 'discount_amount')
          {
            // alert('here');
            location.reload();
          }

        },

      });
    }

    var table2 =  $('.table-family').DataTable({
    processing: true,
    searching: false,
    "language": {
    processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw" style="color:#13436c;"></i><span class="sr-only">Loading...</span> '},
    ordering: false,
    pageLength: {{50}},
    serverSide: true,
    autoWidth: false,
    "lengthMenu": [50,100,150,200],

    scrollX:true,
    scrollY : '90vh',
    scrollCollapse: true,
    ajax:
    {
      beforeSend: function(){
        $('#loader_modal').modal({
          backdrop: 'static',
          keyboard: false
        });
        $('#loader_modal').modal('show');
      },
      url: "{!! route('get-student-family') !!}",
       data: function(data) { data.id = std_id } ,
    },
    columns: [
      { data: 'name', name: 'name' },
      { data: 'father', name: 'father' },
      { data: 'std_class', name: 'std_class' },
      { data: 'relation', name: 'relation' },
      { data: 'checkbox', name: 'checkbox' },
    ],
    initComplete: function () {
    // Enable THEAD scroll bars
    $('.dataTables_scrollHead').css('overflow', 'auto');
    // Sync THEAD scrolling with TBODY
      $('.dataTables_scrollHead').on('scroll', function () {
          $('.dataTables_scrollBody').scrollLeft($(this).scrollLeft());
      });
    },
    drawCallback: function(){
      $('#loader_modal').modal('hide');
    },
  });

    $('#add-student-family-form').on('submit',function(e){
    e.preventDefault();
      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
          }
      });
       $.ajax({
          url: "{{ route('add-student-family') }}",
          dataType: 'json',
          type: 'post',
          data: new FormData(this),
          contentType: false,
          cache: false,
          processData:false,
          beforeSend: function(){
            $('.save-btn').addClass('disabled');
            $('.save-btn').attr('disabled', true);
            $('#loader_modal').modal({
              backdrop: 'static',
              keyboard: false
            });
            $('#loader_modal').modal('show');
          },
          success: function(result){
            $('.save-btn').attr('disabled', true);
            $('.save-btn').removeAttr('disabled');
            if(result.success == true){
              toastr.success('Success!', 'Student Family Added Successfully!!!',{"positionClass": "toast-bottom-right"});
              $('.table-family').DataTable().ajax.reload();
              /*setTimeout(function(){
                window.location.reload();
              }, 2000);*/

              $('#add-student-family-form')[0].reset();
              $('#addStudentFamily').modal('hide');
            }
            else if(result.already_exist == true)
            {
               toastr.warning('Sorry!', 'Family Member Already Exists!!!',{"positionClass": "toast-bottom-right"});
               $('#add-student-family-form')[0].reset();
              $('#addStudentFamily').modal('hide');
            }
            else{
              toastr.error('Error!', result.errormsg,{"positionClass": "toast-bottom-right"});
              $('#loader_modal').modal('hide');
            }

            $('#loader_modal').modal('hide');


          },
          error: function (request, status, error) {
                /*$('.form-control').removeClass('is-invalid');
                $('.form-control').next().remove();*/
                $('#loader_modal').modal('hide');
                $('.save-btn').removeClass('disabled');
                $('.save-btn').removeAttr('disabled');
                json = $.parseJSON(request.responseText);
                $.each(json.errors, function(key, value){
                      $('input[name="'+key+'"]').after('<span class="invalid-feedback" role="alert"><strong>'+value+'</strong>');
                     $('input[name="'+key+'"]').addClass('is-invalid');
                     $('textarea[name="'+key+'"]').after('<span class="invalid-feedback" role="alert"><strong>'+value+'</strong>');
                     $('textarea[name="'+key+'"]').addClass('is-invalid');


                });
            }
        });
  });

    $(document).on('click','.delete_student_family',function(){
      var id = $(this).data('id');

        swal({
            title: "Are You Sure!",
            html: true,
            text: "<b>You want to delete this family member !!!</b>",
            type: "info",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes!",
            cancelButtonText: "No!",
            closeOnConfirm: true,
            closeOnCancel: false
          },
            function(isConfirm) {
            if (isConfirm){
             $.ajaxSetup({
              headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
              }
            });
            $.ajax({
              url: "{{ route('delete-student-family') }}",
              method: 'post',
              data: {id:id},
              beforeSend: function(){
                $('#loader_modal').modal({
                  backdrop: 'static',
                  keyboard: false
                });
                $('#loader_modal').modal('show');
              },
              success: function (response) {

                if(response.success == true)
                {
                  toastr.success('Success!', 'Student Family Deleted Successfully!!!',{"positionClass": "toast-bottom-right"});
                  $('.table-family').DataTable().ajax.reload();
                  $('#loader_modal').modal('hide');
                }

              },
              error: function(request, status, error){
                $('#loader_modal').modal('hide');
              }
            });
            }
            else{
              swal("Cancelled", "", "error");

            }
          });
  });

    $(document).on('change','#student_class',function(){
    // alert($(this).val());
    var id = $(this).val();

    $.ajaxSetup({
              headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
              }
            });
            $.ajax({
              url: "{{ route('fetch-family-students') }}",
              method: 'post',
              data: {id:id},
              beforeSend: function(){
                $('#loader_modal').modal({
                  backdrop: 'static',
                  keyboard: false
                });
                $('#loader_modal').modal('show');
              },
              success: function (response) {

                if(response.success == true)
                {
                  // alert(response.fee);
                  $('.selecting-students').removeAttr('disabled');
                  $('.selecting-students-guardian').removeAttr('disabled');
                  $('.selecting-students').empty();
                  $('.selecting-students-guardian').empty();
                  $('.selecting-students').append(response.html);
                  $('.selecting-students-guardian').append(response.html2);
                  $('#loader_modal').modal('hide');

                }

              },
              error: function(request, status, error){
                $('#loader_modal').modal('hide');
              }
            });
  });

    $(document).on('change','#student_guardian',function(){
    // alert($(this).val());
    var id = $(this).val();
    var class_id = $('#student_class').val();

    $.ajaxSetup({
              headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
              }
            });
            $.ajax({
              url: "{{ route('fetch-family-students-by-guardian') }}",
              method: 'post',
              data: {id:id,class_id:class_id},
              beforeSend: function(){
                $('#loader_modal').modal({
                  backdrop: 'static',
                  keyboard: false
                });
                $('#loader_modal').modal('show');
              },
              success: function (response) {

                if(response.success == true)
                {
                  // alert(response.fee);
                  $('.selecting-students').removeAttr('disabled');
                  // $('.selecting-students-guardian').removeAttr('disabled');
                  $('.selecting-students').empty();
                  // $('.selecting-students-guardian').empty();
                  $('.selecting-students').append(response.html);
                  // $('.selecting-students-guardian').append(response.html2);
                  $('#loader_modal').modal('hide');

                }

              },
              error: function(request, status, error){
                $('#loader_modal').modal('hide');
              }
            });
  });

    $('#add-student-photo').on('submit',function(e){
    e.preventDefault();
    var img = $('#image').val();
    if(img == '' || img == null)
    {
      toastr.error('Sorry!', 'Please a upload a photo first!!!',{"positionClass": "toast-bottom-right"});
      return false;
    }
      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
          }
      });
       $.ajax({
          url: "{{ route('add-student-photo') }}",
          dataType: 'json',
          type: 'post',
          data: new FormData(this),
          contentType: false,
          cache: false,
          processData:false,
          beforeSend: function(){
            $('.save-btn').addClass('disabled');
            $('.save-btn').attr('disabled', true);
            $('#loader_modal').modal({
              backdrop: 'static',
              keyboard: false
            });
            $('#loader_modal').modal('show');
          },
          success: function(result){
            $('.save-btn').attr('disabled', true);
            $('.save-btn').removeAttr('disabled');
            if(result.success == true){
              toastr.success('Success!', 'Student Photo Updated Successfully!!!',{"positionClass": "toast-bottom-right"});
              // $('.table-family').DataTable().ajax.reload();
              /*setTimeout(function(){
                window.location.reload();
              }, 2000);*/

              // $('#add-student-family-form')[0].reset();
              // $('#addStudentFamily').modal('hide');
              location.reload();
            }else{
              toastr.error('Error!', result.errormsg,{"positionClass": "toast-bottom-right"});
              $('#loader_modal').modal('hide');
            }

          },
          error: function (request, status, error) {
                /*$('.form-control').removeClass('is-invalid');
                $('.form-control').next().remove();*/
                $('#loader_modal').modal('hide');
                $('.save-btn').removeClass('disabled');
                $('.save-btn').removeAttr('disabled');
                json = $.parseJSON(request.responseText);
                $.each(json.errors, function(key, value){
                      $('input[name="'+key+'"]').after('<span class="invalid-feedback" role="alert"><strong>'+value+'</strong>');
                     $('input[name="'+key+'"]').addClass('is-invalid');
                     $('textarea[name="'+key+'"]').after('<span class="invalid-feedback" role="alert"><strong>'+value+'</strong>');
                     $('textarea[name="'+key+'"]').addClass('is-invalid');


                });
            }
        });
  });

        $('#image').on('change', function() {
        let size = this.files[0].size; // this is in bytes
        if (size > 1000000) {
        // do something. Prevent form submit. Show message, etc.
          toastr.error('Sorry!', 'Student Photo Size Must Be Less Than 1MB!!!',{"positionClass": "toast-bottom-right"});

        }
        else
        {
          $('.update_image').removeAttr('disabled');
        }
    });
});
</script>
@stop

