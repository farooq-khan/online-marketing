@extends('user.layouts.layout')

@section('title','Online Marketing')

@section('content')
<style>
  
</style>
                    <div>
                        <div class="row page-title-row">
                            <div class="col-sm col page-title-col align-self-center">
                                <h1 class="page-title">All Students</h1>
                            </div>
                            @if (Auth::user()->role == 'admin')
                              <div class="col-sm-auto col-auto text-right">
                                <button type="button" class="btn btn-outline-primary my-1" data-toggle="modal" data-target="#import-modal"><span class="fa fa-plus  mr-1"></span>Upload Data</button>
                                <button type="button" class="btn btn-primary my-1 mr-1" data-toggle="modal" data-target="#addStudent"><span class="fa fa-plus"></span>Add Student</button>
                              </div>
                            @endif
                        </div>

                      <div class="card card-border-top mb-4">
                        <div class="card-header">
                              <h6 class="mb-0">Filters</h6>
                          </div>
                        <div class="card-body">
                              
                          <div class="row mb-0 form-row row-cols-lg-4 row-cols-md-4 row-cols-sm-2 row-cols-2">
                            <div class="col form-group">
                              <label for="">Classes</label>
                              <select class="form-control form-control student_classes_select state-tags" name="warehouse_id" >
                                <option value="">-- All Classes --</option>
                                @foreach($classes as $class)
                                  <option value="{{$class->id}}">{{$class->class_name}} </option>
                                  @endforeach
                              </select>
                            </div>

                            <div class="col form-group">
                              <label for="">Discount</label>
                              <select class="form-control form-control discount_student state-tags" name="warehouse_id" >
                                <option value="">-- Discount --</option>
                                  <option value="discount">Discounted Students </option>
                                  <option value="non-discount">Non Discounted Students </option>
                              </select>
                            </div>

                            <div class="col form-group">
                              <label for="">Status</label>
                              <select class="form-control form-control student_status state-tags" name="student_status" >
                                <option value="">-- Status --</option>
                                  <option value="1" selected="true">Active </option>
                                  <option value="2">Suspended </option>
                              </select>
                            </div>

                            <div class="col form-group">
                              <label>Father Name</label>
                                <div class="form-group mb-0">
                                  <input type="text" placeholder="Father-Nick Name ..." name="input_keyword" class="form-control" id="input_keyword" autocomplete="off">
                                </div>
                            </div>

                            <div class="col form-group">
                              <label>Student Name</label>
                                <div class="form-group mb-0">
                                  <input type="text" placeholder="Search Student's Name ..." name="input_keyword_std" class="form-control" id="input_keyword_std" autocomplete="off">
                                </div>
                            </div>

                            <div class="col form-group">
                              <label for="from_date">Suspend From Date:</label>
                                <input type="date" name="from_date" id="from_date" class="form-control" placeholder="Search by year ..." />
                            </div>

                            <div class="col form-group">
                              <label for="to_date">Suspend To Date:</label>
                                <input type="date" name="to_date" id="to_date" class="form-control" placeholder="Search by year ..." />
                            </div>
                            <div class="col-lg col-md col-sm col ml-auto text-right mb-3 align-self-end">
                              <!-- <label for=""style="visibility: hidden;"><b>Submitted From Date :</b></label> -->
                              <div class=" mt-sm-4">
                                <button class="btn btn-outline-primary" type="reset" id="reset">Reset</button>
                                <button class="btn btn-primary search-btn ml-md-2 ml-1" type="button" id="search_btn">Search</button>
                              </div>
                            </div>
                          </div>
                        </div>
                  </div>

                  <div class="form-row for-cols-sm-3 row-cols-2 d-none">
                    <div class="col d-flex mb-2">
                        <div class="card mobile-card flex-fill">
                            <div class="card-header d-flex align-items-center justify-content-between">
                                <h5 class="card-title">John Doe</h5>
                                <div class="card-actions d-flex align-items-center">
                                  <input type="checkbox" class="mr-1" name="" id="" value="checkedValue">
                                  <div class="dropdown">
                                    <button class="fa fa-ellipsis-v action-dropdown-btn" type="button" id="triggerId" data-toggle="dropdown" aria-haspopup="true"
                                        aria-expanded="false">
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="triggerId">
                                      <a class="dropdown-item" href="#">Edit</a>
                                      <a class="dropdown-item" href="#">Suspend</a>
                                      <a class="dropdown-item" href="#">Delete</a>
                                    </div>
                                  </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <p class="card-text"><span class="font-weight-semibold">Class:</span> 10</p>
                                <p class="card-text"><span class="font-weight-semibold">Admission No:</span> 10</p>
                                <p class="card-text"><strong class="font-weight-semibold">Gender:</strong> Male</p>
                                <div class="collapse-content">
                                  <p class="card-text"><strong class="font-weight-semibold">Father/Guardian Name:</strong> Richard Doe</p>
                                  <p class="card-text"><strong class="font-weight-semibold">Father/Guardian Phone:</strong> +1234567890</p>
                                  <p class="card-text"><strong class="font-weight-semibold">Total Payable:</strong> $5000</p>
                                  <p class="card-text"><strong class="font-weight-semibold">Fee (Inc Discount):</strong> $4500</p>
                                  <p class="card-text"><strong class="font-weight-semibold">Discount:</strong> $500</p>
                                  <p class="card-text"><strong class="font-weight-semibold">Suspend Date:</strong> N/A</p>
                                </div>
                                <p class="collapse-toggle">Show More</p>
                            </div>
                        </div>
                    </div>
                    <div class="col d-flex mb-2">
                        <div class="card mobile-card flex-fill">
                            <div class="card-header d-flex align-items-center justify-content-between">
                                <h5 class="card-title">John Doe</h5>
                                <div class="card-actions d-flex align-items-center">
                                  <input type="checkbox" class="mr-1" name="" id="" value="checkedValue">
                                  <div class="dropdown">
                                    <button class="fa fa-ellipsis-v action-dropdown-btn" type="button" id="triggerId" data-toggle="dropdown" aria-haspopup="true"
                                        aria-expanded="false">
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="triggerId">
                                      <a class="dropdown-item" href="#">Edit</a>
                                      <a class="dropdown-item" href="#">Suspend</a>
                                      <a class="dropdown-item" href="#">Delete</a>
                                    </div>
                                  </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <p class="card-text"><span class="font-weight-semibold">Class:</span> 10</p>
                                <p class="card-text"><span class="font-weight-semibold">Admission No:</span> 10</p>
                                <p class="card-text"><strong class="font-weight-semibold">Gender:</strong> Male</p>
                                <div class="collapse-content">
                                  <p class="card-text"><strong class="font-weight-semibold">Father/Guardian Name:</strong> Richard Doe</p>
                                  <p class="card-text"><strong class="font-weight-semibold">Father/Guardian Phone:</strong> +1234567890</p>
                                  <p class="card-text"><strong class="font-weight-semibold">Total Payable:</strong> $5000</p>
                                  <p class="card-text"><strong class="font-weight-semibold">Fee (Inc Discount):</strong> $4500</p>
                                  <p class="card-text"><strong class="font-weight-semibold">Discount:</strong> $500</p>
                                  <p class="card-text"><strong class="font-weight-semibold">Suspend Date:</strong> N/A</p>
                                </div>
                                <p class="collapse-toggle">Show More</p>
                            </div>
                        </div>
                    </div>
                    <div class="col d-flex mb-2">
                        <div class="card mobile-card flex-fill">
                            <div class="card-header d-flex align-items-center justify-content-between">
                                <h5 class="card-title">John Doe</h5>
                                <div class="card-actions d-flex align-items-center">
                                  <input type="checkbox" class="mr-1" name="" id="" value="checkedValue">
                                  <div class="dropdown">
                                    <button class="fa fa-ellipsis-v action-dropdown-btn" type="button" id="triggerId" data-toggle="dropdown" aria-haspopup="true"
                                        aria-expanded="false">
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="triggerId">
                                      <a class="dropdown-item" href="#">Edit</a>
                                      <a class="dropdown-item" href="#">Suspend</a>
                                      <a class="dropdown-item" href="#">Delete</a>
                                    </div>
                                  </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <p class="card-text"><span class="font-weight-semibold">Class:</span> 10</p>
                                <p class="card-text"><span class="font-weight-semibold">Admission No:</span> 10</p>
                                <p class="card-text"><strong class="font-weight-semibold">Gender:</strong> Male</p>
                                <div class="collapse-content">
                                  <p class="card-text"><strong class="font-weight-semibold">Father/Guardian Name:</strong> Richard Doe</p>
                                  <p class="card-text"><strong class="font-weight-semibold">Father/Guardian Phone:</strong> +1234567890</p>
                                  <p class="card-text"><strong class="font-weight-semibold">Total Payable:</strong> $5000</p>
                                  <p class="card-text"><strong class="font-weight-semibold">Fee (Inc Discount):</strong> $4500</p>
                                  <p class="card-text"><strong class="font-weight-semibold">Discount:</strong> $500</p>
                                  <p class="card-text"><strong class="font-weight-semibold">Suspend Date:</strong> N/A</p>
                                </div>
                                <p class="collapse-toggle">Show More</p>
                            </div>
                        </div>
                    </div>
                    <div class="col d-flex mb-2">
                        <div class="card mobile-card flex-fill">
                            <div class="card-header d-flex align-items-center justify-content-between">
                                <h5 class="card-title">John Doe</h5>
                                <div class="card-actions d-flex align-items-center">
                                  <input type="checkbox" class="mr-1" name="" id="" value="checkedValue">
                                  <div class="dropdown">
                                    <button class="fa fa-ellipsis-v action-dropdown-btn" type="button" id="triggerId" data-toggle="dropdown" aria-haspopup="true"
                                        aria-expanded="false">
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="triggerId">
                                      <a class="dropdown-item" href="#">Edit</a>
                                      <a class="dropdown-item" href="#">Suspend</a>
                                      <a class="dropdown-item" href="#">Delete</a>
                                    </div>
                                  </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <p class="card-text"><span class="font-weight-semibold">Class:</span> 10</p>
                                <p class="card-text"><span class="font-weight-semibold">Admission No:</span> 10</p>
                                <p class="card-text"><strong class="font-weight-semibold">Gender:</strong> Male</p>
                                <div class="collapse-content">
                                  <p class="card-text"><strong class="font-weight-semibold">Father/Guardian Name:</strong> Richard Doe</p>
                                  <p class="card-text"><strong class="font-weight-semibold">Father/Guardian Phone:</strong> +1234567890</p>
                                  <p class="card-text"><strong class="font-weight-semibold">Total Payable:</strong> $5000</p>
                                  <p class="card-text"><strong class="font-weight-semibold">Fee (Inc Discount):</strong> $4500</p>
                                  <p class="card-text"><strong class="font-weight-semibold">Discount:</strong> $500</p>
                                  <p class="card-text"><strong class="font-weight-semibold">Suspend Date:</strong> N/A</p>
                                </div>
                                <p class="collapse-toggle">Show More</p>
                            </div>
                        </div>
                    </div>
                    <div class="col d-flex mb-2">
                        <div class="card mobile-card flex-fill">
                            <div class="card-header d-flex align-items-center justify-content-between">
                                <h5 class="card-title">John Doe</h5>
                                <div class="card-actions d-flex align-items-center">
                                  <input type="checkbox" class="mr-1" name="" id="" value="checkedValue">
                                  <div class="dropdown">
                                    <button class="fa fa-ellipsis-v action-dropdown-btn" type="button" id="triggerId" data-toggle="dropdown" aria-haspopup="true"
                                        aria-expanded="false">
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="triggerId">
                                      <a class="dropdown-item" href="#">Edit</a>
                                      <a class="dropdown-item" href="#">Suspend</a>
                                      <a class="dropdown-item" href="#">Delete</a>
                                    </div>
                                  </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <p class="card-text"><span class="font-weight-semibold">Class:</span> 10</p>
                                <p class="card-text"><span class="font-weight-semibold">Admission No:</span> 10</p>
                                <p class="card-text"><strong class="font-weight-semibold">Gender:</strong> Male</p>
                                <div class="collapse-content">
                                  <p class="card-text"><strong class="font-weight-semibold">Father/Guardian Name:</strong> Richard Doe</p>
                                  <p class="card-text"><strong class="font-weight-semibold">Father/Guardian Phone:</strong> +1234567890</p>
                                  <p class="card-text"><strong class="font-weight-semibold">Total Payable:</strong> $5000</p>
                                  <p class="card-text"><strong class="font-weight-semibold">Fee (Inc Discount):</strong> $4500</p>
                                  <p class="card-text"><strong class="font-weight-semibold">Discount:</strong> $500</p>
                                  <p class="card-text"><strong class="font-weight-semibold">Suspend Date:</strong> N/A</p>
                                </div>
                                <p class="collapse-toggle">Show More</p>
                            </div>
                        </div>
                    </div>
                    <div class="col d-flex mb-2">
                        <div class="card mobile-card flex-fill">
                            <div class="card-header d-flex align-items-center justify-content-between">
                                <h5 class="card-title">John Doe</h5>
                                <div class="card-actions d-flex align-items-center">
                                  <input type="checkbox" class="mr-1" name="" id="" value="checkedValue">
                                  <div class="dropdown">
                                    <button class="fa fa-ellipsis-v action-dropdown-btn" type="button" id="triggerId" data-toggle="dropdown" aria-haspopup="true"
                                        aria-expanded="false">
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="triggerId">
                                      <a class="dropdown-item" href="#">Edit</a>
                                      <a class="dropdown-item" href="#">Suspend</a>
                                      <a class="dropdown-item" href="#">Delete</a>
                                    </div>
                                  </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <p class="card-text"><span class="font-weight-semibold">Class:</span> 10</p>
                                <p class="card-text"><span class="font-weight-semibold">Admission No:</span> 10</p>
                                <p class="card-text"><strong class="font-weight-semibold">Gender:</strong> Male</p>
                                <div class="collapse-content">
                                  <p class="card-text"><strong class="font-weight-semibold">Father/Guardian Name:</strong> Richard Doe</p>
                                  <p class="card-text"><strong class="font-weight-semibold">Father/Guardian Phone:</strong> +1234567890</p>
                                  <p class="card-text"><strong class="font-weight-semibold">Total Payable:</strong> $5000</p>
                                  <p class="card-text"><strong class="font-weight-semibold">Fee (Inc Discount):</strong> $4500</p>
                                  <p class="card-text"><strong class="font-weight-semibold">Discount:</strong> $500</p>
                                  <p class="card-text"><strong class="font-weight-semibold">Suspend Date:</strong> N/A</p>
                                </div>
                                <p class="collapse-toggle">Show More</p>
                            </div>
                        </div>
                    </div>
                    <div class="col d-flex mb-2">
                        <div class="card mobile-card flex-fill">
                            <div class="card-header d-flex align-items-center justify-content-between">
                                <h5 class="card-title">John Doe</h5>
                                <div class="card-actions d-flex align-items-center">
                                  <input type="checkbox" class="mr-1" name="" id="" value="checkedValue">
                                  <div class="dropdown">
                                    <button class="fa fa-ellipsis-v action-dropdown-btn" type="button" id="triggerId" data-toggle="dropdown" aria-haspopup="true"
                                        aria-expanded="false">
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="triggerId">
                                      <a class="dropdown-item" href="#">Edit</a>
                                      <a class="dropdown-item" href="#">Suspend</a>
                                      <a class="dropdown-item" href="#">Delete</a>
                                    </div>
                                  </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <p class="card-text"><span class="font-weight-semibold">Class:</span> 10</p>
                                <p class="card-text"><span class="font-weight-semibold">Admission No:</span> 10</p>
                                <p class="card-text"><strong class="font-weight-semibold">Gender:</strong> Male</p>
                                <div class="collapse-content">
                                  <p class="card-text"><strong class="font-weight-semibold">Father/Guardian Name:</strong> Richard Doe</p>
                                  <p class="card-text"><strong class="font-weight-semibold">Father/Guardian Phone:</strong> +1234567890</p>
                                  <p class="card-text"><strong class="font-weight-semibold">Total Payable:</strong> $5000</p>
                                  <p class="card-text"><strong class="font-weight-semibold">Fee (Inc Discount):</strong> $4500</p>
                                  <p class="card-text"><strong class="font-weight-semibold">Discount:</strong> $500</p>
                                  <p class="card-text"><strong class="font-weight-semibold">Suspend Date:</strong> N/A</p>
                                </div>
                                <p class="collapse-toggle">Show More</p>
                            </div>
                        </div>
                    </div>
                    <div class="col d-flex mb-2">
                        <div class="card mobile-card flex-fill">
                            <div class="card-header d-flex align-items-center justify-content-between">
                                <h5 class="card-title">John Doe</h5>
                                <div class="card-actions d-flex align-items-center">
                                  <input type="checkbox" class="mr-1" name="" id="" value="checkedValue">
                                  <div class="dropdown">
                                    <button class="fa fa-ellipsis-v action-dropdown-btn" type="button" id="triggerId" data-toggle="dropdown" aria-haspopup="true"
                                        aria-expanded="false">
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="triggerId">
                                      <a class="dropdown-item" href="#">Edit</a>
                                      <a class="dropdown-item" href="#">Suspend</a>
                                      <a class="dropdown-item" href="#">Delete</a>
                                    </div>
                                  </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <p class="card-text"><span class="font-weight-semibold">Class:</span> 10</p>
                                <p class="card-text"><span class="font-weight-semibold">Admission No:</span> 10</p>
                                <p class="card-text"><strong class="font-weight-semibold">Gender:</strong> Male</p>
                                <div class="collapse-content">
                                  <p class="card-text"><strong class="font-weight-semibold">Father/Guardian Name:</strong> Richard Doe</p>
                                  <p class="card-text"><strong class="font-weight-semibold">Father/Guardian Phone:</strong> +1234567890</p>
                                  <p class="card-text"><strong class="font-weight-semibold">Total Payable:</strong> $5000</p>
                                  <p class="card-text"><strong class="font-weight-semibold">Fee (Inc Discount):</strong> $4500</p>
                                  <p class="card-text"><strong class="font-weight-semibold">Discount:</strong> $500</p>
                                  <p class="card-text"><strong class="font-weight-semibold">Suspend Date:</strong> N/A</p>
                                </div>
                                <p class="collapse-toggle">Show More</p>
                            </div>
                        </div>
                    </div>
                </div>


                    <div class="row p-0 mt-0">
                        <div class="selected-item catalogue-btn-group col d-none">
                          <!-- <a href="javascript:void(0)" class="actionicon viewIcon download_invoice" data-id="1" data-toggle="modal" data-target="#promoteStudents">Promote Students</a> -->
                          <button class="btn btn-primary mb-1 download_invoice" data-id="1" data-toggle="modal" data-target="#promoteStudents" title="Promote Students" data-id="2">Promote Students</button>
                          <button class="btn btn-primary mb-1 download_invoice" data-id="1" data-toggle="modal" data-target="#moveStudents" title="Move Students" data-id="2">Move Students</button>
                          <button class="btn btn-primary mb-1 download_invoice" data-id="1" data-toggle="modal" data-target="#graduateStudents">Pass Out</button>
                          <button class="btn btn-primary mb-1 download_invoice generate_rollno">Generate Roll No.</button>
                          <select class="form-control mb-1 d-inline-block student_exams_select state-tags w-auto" name="exam_id">
                              <option value="">-- All Exams --</option>
                               @foreach($exams as $exam)
                                <option value="{{$exam->id}}">{{$exam->name}} </option>
                                @endforeach
                            </select>
                          <!-- <a href="javascript:void(0)" class="actionicon viewIcon download_invoice" data-id="1" data-toggle="modal" data-target="#graduateStudents">Pass Out</a> -->
                        </div>
                        <div class="col-12 mt-2">
                        <div class="card">
                          <div class="card-body">
                            <div class="table-responsive">
                              <table class="table table-bordered nowrap table-students table-striped">
                              <thead>
                              <tr>
                                <th>S.No</th>
                                  <th class="noVis">
                                    <div class="custom-control custom-checkbox custom-checkbox1 d-inline-block">
                                      <input type="checkbox" class="custom-control-input check-all1" name="check_all" id="check-all">
                                      <label class="custom-control-label" for="check-all"></label>
                                    </div>
                                  </th>
                                  <th>Name</th>
                                  <th>Class</th>
                                  <th>Admission No.</th>
                                  <th>Gender</th>
                                  <th>Father/Guardian Name</th>
                                  <th>Father/Guardian Phone</th>
                                  <th>Total Payable</th>
                                  <th>Fee(Inc Discount)</th>
                                  <th>Discount</th>
                                  <th>Suspend Date</th>
                                  <th>Action</th>
                              </tr>
                              </thead>
                              <tfoot>
                                <tr>
                                  <th></th>
                                  <th></th>
                                  <th></th>
                                  <th></th>
                                  <th></th>
                                  <th></th>
                                  <th></th>
                                  <th></th>
                                  <th></th>
                                  <th></th>
                                  <th></th>
                                  <th></th>
                                  <th></th>
                                </tr>
                              </tfoot>
                              </table>
                            </div>
                        </div>
                    </div>
                </div>
                    </div>
                    </div>


<!--Add Student Modal -->
<div class="modal fade" id="addStudent" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add Student</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form id="add-student-form" method="POST" enctype="multipart/form-data">
      <div class="modal-body">

          <div class="form-row row-cols-lg-3 row-cols-sm-2 row-cols-2">
            <div class="form-group col">
                <label for="name">Student Name</label>
                <input type="text" name="name" id="name" class="form-control" placeholder="Student Name"/>
            </div>
            <div class="form-group col">
                <label for="guardian">Father Name</label>
                <input type="text" name="guardian" id="guardian" class="form-control" placeholder="Student Father Name"/>
            </div>
            <div class="form-group col">
                <label for="guardian_nickname">Father Nick Name</label>
                <input type="text" name="guardian_nickname" id="guardian_nickname" class="form-control" placeholder="Student Father Nick Name"/>
            </div>

            <div class="form-group col">
                <label for="guardian_cnic">Father Cnic</label>
                <input type="text" name="guardian_cnic" id="guardian_cnic" class="form-control" placeholder="Father Cnic"/>
            </div>
            <div class="form-group col">
                <label for="guardian_phone">Father Phone No.</label>
                <input type="number" name="guardian_phone" id="guardian_phone" class="form-control" placeholder="Father Phone No."/>
            </div>

            <div class="form-group col">
                <label for="roll_no">Admission No</label>
                <input type="text" name="roll_no" id="roll_no" class="form-control" placeholder="Admission No."/>
            </div>

             <div class="form-group col">
                <label for="gender">Gender</label>
                <select class="form-control" id="gender" name="gender">
                  <option value="">--Select Gender--</option>
                  <option value="male">Male</option>
                  <option value="female">Female</option>
                  <option value="other">Other</option>
                </select>
              </div>

            <div class="form-group col">
                <label for="admission_date">Admission Dat</label>
                <input type="date" name="admission_date" id="admission_date" class="form-control" placeholder="Admission Date"/>
            </div>
            <div class="form-group col">
                <label for="dob">Date of Birth</label>
                <input type="date" name="dob" id="dob" class="form-control" placeholder="DoB"/>
            </div>

            <div class="form-group col">
                <label for="std_phone">Phone No</label>
                <input type="text" name="std_phone" id="std_phone" class="form-control" placeholder="Phone No"/>
            </div>
            <div class="form-group col">
                <label for="address">Address</label>
                <input type="text" name="address" id="address" class="form-control" placeholder="Address"/>
            </div>

            <div class="form-group col">
              <label for="student_class">Class</label>
              <select class="form-control" id="student_class" name="student_class">
                <option value="">--Select Class--</option>
                @foreach($classes as $class)
                <option value="{{$class->id}}">{{$class->class_name}} </option>
                @endforeach
              </select>
            </div>

            <div class="form-group col">
                <label for="fee">Fee</label>
                <input type="text" name="fee" id="fee" class="form-control" placeholder="0.00" readonly="" />
                <input type="hidden" name="fee_before_discount" id="fee_before_discount" class="form-control" placeholder="0.00" disabled="disabled" />
            </div>

            <div class="form-group col">
                <label for="discount_amount">Fee Discount Amount</label>
                <input type="number" name="discount_amount" id="discount_amount" class="form-control" placeholder="Fee Discount Amount"/>
            </div>

              <div class="form-group col">
                <label for="discount">Fee Discount</label>
                <input type="number" name="discount" id="discount" min="0" max="100" class="form-control" placeholder="Fee Discount" readonly="" />
            </div>

            <div class="form-group col">
                <label for="image">Upload Photo</label>
                <input type="file" name="image" id="image" class="form-control" placeholder="Image" accept="image/*" />
            </div>
            </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save</button>
      </div>
      </form>
    </div>
  </div>
</div>

<!--Add Student Modal -->
<div class="modal fade" id="promoteStudents" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Promotes Students</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form id="promotes-students-form" method="POST">
        @csrf
      <div class="modal-body ">
          <div class="form-row">
            <input type="hidden" name="selected_students_ids" id="selected_students_ids">
            <div class="form-group col-md-6">
              <select class="form-control form-control student_classes_select2 state-tags" name="class_id" required="required">
              <option value="">-- All Classes --</option>
               @foreach($classes as $class)
                <option value="{{$class->id}}">{{$class->class_name}} </option>
                @endforeach
            </select>
            </div>

          </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary save-btn">Submit</button>
      </div>
      </form>
    </div>
  </div>
</div>

<!--Move Student Modal -->
<div class="modal fade" id="moveStudents" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Move Students</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form id="move-students-form" method="POST">
        @csrf
      <div class="modal-body ">
          <div class="form-row">
            <input type="hidden" name="selected_students_ids" id="selected_students_ids_for_moving">
            <div class="form-group col-md-6">
              <select class="form-control form-control student_classes_select2 state-tags" name="class_id" required="required">
              <option value="">-- All Classes --</option>
               @foreach($classes as $class)
                <option value="{{$class->id}}">{{$class->class_name}} </option>
                @endforeach
            </select>
            </div>

          </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary save-btn">Submit</button>
      </div>
      </form>
    </div>
  </div>
</div>

<!--Add Student Modal -->
<div class="modal fade" id="graduateStudents" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Pass Out Students</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form id="graduate-students-form" method="POST">
        @csrf
      <div class="modal-body ">
          <div class="form-row">
            <input type="hidden" name="selected_students_ids2" id="selected_students_ids2">
            <div class="form-group col-md-6">
              <label for="graduate_date">Enter Pass Out Date</label>
              <input type="date" name="graduate_date" class="graduate_date" id="graduate_date">
            </div>

          </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary save-btn">Submit</button>
      </div>
      </form>
    </div>
  </div>
</div>

<!-- Modal for Import Items  -->
<div class="modal fade" id="import-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Upload Excel File</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <div class="modal-body">
        <div>
          <label class="mt-2">
            <strong>Note : </strong><span class="text-danger">Also Don't Upload Empty File.</span>
          </label>
          <form class="upload-excel-form" id="upload-excel-form" enctype="multipart/form-data">
            @csrf
            <input type="file" class="form-control" name="product_excel" id="product_excel" accept=".xls,.xlsx" required="" style="white-space: pre-wrap;"><br>
            <button class="btn btn-info product-upload-btn" type="submit">Upload</button>
          </form>
        </div>
      </div>

    </div>
  </div>
</div>
@endsection

@section('javascript')
<script type="text/javascript">
$(document).ready(function(){
  document.querySelectorAll('.collapse-toggle').forEach(function(toggle) {
        toggle.addEventListener('click', function() {
            const card = this.closest('.card');
            card.classList.toggle('show-more');
            this.textContent = card.classList.contains('show-more') ? 'Show Less' : 'Show More';
        });
    });
    $('#input_keyword').keyup(function(event){
    var query = $(this).val();

    if(event.keyCode == 13)
    {
      if(query.length > 2)
      {

        // alert(query);
        $('.table-students').DataTable().ajax.reload();
      }
      else if(query.length == 0)
      {
        $('.table-students').DataTable().ajax.reload();
      }
      else
      {
        // $('#input_keyword').empty();
        toastr.error('Error!', 'Please enter atlesat 3 characters then press Enter !!!' ,{"positionClass": "toast-bottom-right"});
      }
    }
  });
    $('#input_keyword_std').keyup(function(event){
    var query = $(this).val();

    if(event.keyCode == 13)
    {
      if(query.length > 2)
      {

        // alert(query);
        $('.table-students').DataTable().ajax.reload();
      }
      else if(query.length == 0)
      {
        $('.table-students').DataTable().ajax.reload();
      }
      else
      {
        // $('#input_keyword_std').empty();
        toastr.error('Error!', 'Please enter atlesat 3 characters then press Enter !!!' ,{"positionClass": "toast-bottom-right"});
      }
    }
  });
        $(document).on('click', '.check-all1', function () {
        if($('.student_classes_select option:selected').val() == null || $('.student_classes_select option:selected').val() == '')
        {
           toastr.info('Please!', 'Select Class First!!!',{"positionClass": "toast-bottom-right"});
           return false;
        }
        if(this.checked == true){
        $('.check').prop('checked', true);
        $('.check').parents('tr').addClass('selected');
        var cb_length = $( ".check:checked" ).length;
        if(cb_length > 0){
          $('.selected-item').removeClass('d-none');
        }
      }else{
        $('.check').prop('checked', false);
        $('.check').parents('tr').removeClass('selected');
        $('.selected-item').addClass('d-none');

      }
    });

   $(document).on('click', '.check', function () {
      if($('.student_classes_select option:selected').val() == null || $('.student_classes_select option:selected').val() == '')
      {
         toastr.info('Please!', 'Select Class First!!!',{"positionClass": "toast-bottom-right"});
         return false;
      }
    // $(this).removeClass('d-none');
        var cb_length = $( ".check:checked" ).length;
        var st_pieces = $(this).parents('tr').attr('data-pieces');
        if(this.checked == true){
        $('.selected-item').removeClass('d-none');
        $(this).parents('tr').addClass('selected');
      }else{
        $(this).parents('tr').removeClass('selected');
        if(cb_length == 0){
         $('.selected-item').addClass('d-none');
        }

      }
    });

     $('#promotes-students-form').on('submit',function(e){
     e.preventDefault();

    var selected_students_ids = [];
    $("input.check:checked").each(function() {
      selected_students_ids.push($(this).val());
    });

    $('#selected_students_ids').val(selected_students_ids);

      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
          }
      });
       $.ajax({
          url: "{{ route('promote-students') }}",
          dataType: 'json',
          type: 'post',
          data: new FormData(this),
          contentType: false,
          cache: false,
          processData:false,
          beforeSend: function(){
            $('.save-btn').addClass('disabled');
            $('.save-btn').attr('disabled', true);
            $('#loader_modal').modal({
              backdrop: 'static',
              keyboard: false
            });
            $('#loader_modal').modal('show');
          },
          success: function(result){
            $('.save-btn').attr('disabled', true);
            $('.save-btn').removeAttr('disabled');
            if(result.success == true){
              toastr.success('Success!', 'Students Promoted Successfully!!!',{"positionClass": "toast-bottom-right"});
              // $('.table-students').DataTable().ajax.reload();
              $('#reset').trigger('click');
              $('.check-all1').prop('checked', false);
              $('.selected-item').addClass('d-none');

              $('#promotes-students-form')[0].reset();
              $('#promoteStudents').modal('hide');
            }else{
              toastr.error('Error!', result.errormsg,{"positionClass": "toast-bottom-right"});
              $('#loader_modal').modal('hide');
            }

          },
          error: function (request, status, error) {
                /*$('.form-control').removeClass('is-invalid');
                $('.form-control').next().remove();*/
                $('#loader_modal').modal('hide');
                $('.save-btn').removeClass('disabled');
                $('.save-btn').removeAttr('disabled');
                json = $.parseJSON(request.responseText);
                $.each(json.errors, function(key, value){
                      $('input[name="'+key+'"]').after('<span class="invalid-feedback" role="alert"><strong>'+value+'</strong>');
                     $('input[name="'+key+'"]').addClass('is-invalid');
                     $('textarea[name="'+key+'"]').after('<span class="invalid-feedback" role="alert"><strong>'+value+'</strong>');
                     $('textarea[name="'+key+'"]').addClass('is-invalid');


                });
            }
        });
  });
    $('#move-students-form').on('submit',function(e){
     e.preventDefault();

    var selected_students_ids = [];
    $("input.check:checked").each(function() {
      selected_students_ids.push($(this).val());
    });

    $('#selected_students_ids_for_moving').val(selected_students_ids);

      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
          }
      });
       $.ajax({
          url: "{{ route('move-students') }}",
          dataType: 'json',
          type: 'post',
          data: new FormData(this),
          contentType: false,
          cache: false,
          processData:false,
          beforeSend: function(){
            $('.save-btn').addClass('disabled');
            $('.save-btn').attr('disabled', true);
            $('#loader_modal').modal({
              backdrop: 'static',
              keyboard: false
            });
            $('#loader_modal').modal('show');
          },
          success: function(result){
            $('.save-btn').attr('disabled', true);
            $('.save-btn').removeAttr('disabled');
            if(result.success == true){
              toastr.success('Success!', 'Students Moved Successfully!!!',{"positionClass": "toast-bottom-right"});
              // $('.table-students').DataTable().ajax.reload();
              $('#reset').trigger('click');
              $('.check-all1').prop('checked', false);
              $('.selected-item').addClass('d-none');

              $('#move-students-form')[0].reset();
              $('#moveStudents').modal('hide');
            }else{
              toastr.error('Error!', result.errormsg,{"positionClass": "toast-bottom-right"});
              $('#loader_modal').modal('hide');
            }

          },
          error: function (request, status, error) {
                /*$('.form-control').removeClass('is-invalid');
                $('.form-control').next().remove();*/
                $('#loader_modal').modal('hide');
                $('.save-btn').removeClass('disabled');
                $('.save-btn').removeAttr('disabled');
                json = $.parseJSON(request.responseText);
                $.each(json.errors, function(key, value){
                      $('input[name="'+key+'"]').after('<span class="invalid-feedback" role="alert"><strong>'+value+'</strong>');
                     $('input[name="'+key+'"]').addClass('is-invalid');
                     $('textarea[name="'+key+'"]').after('<span class="invalid-feedback" role="alert"><strong>'+value+'</strong>');
                     $('textarea[name="'+key+'"]').addClass('is-invalid');


                });
            }
        });
  });

    $('#graduate-students-form').on('submit',function(e){
     e.preventDefault();

    var selected_students_ids2 = [];
    $("input.check:checked").each(function() {
      selected_students_ids2.push($(this).val());
    });

    $('#selected_students_ids2').val(selected_students_ids2);

      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
          }
      });
       $.ajax({
          url: "{{ route('graduate-students') }}",
          dataType: 'json',
          type: 'post',
          data: new FormData(this),
          contentType: false,
          cache: false,
          processData:false,
          beforeSend: function(){
            $('.save-btn').addClass('disabled');
            $('.save-btn').attr('disabled', true);
            $('#loader_modal').modal({
              backdrop: 'static',
              keyboard: false
            });
            $('#loader_modal').modal('show');
          },
          success: function(result){
            $('.save-btn').attr('disabled', true);
            $('.save-btn').removeAttr('disabled');
            if(result.success == true){
              toastr.success('Success!', 'Students Graduated Successfully!!!',{"positionClass": "toast-bottom-right"});
              // $('.table-students').DataTable().ajax.reload();
              $('#reset').trigger('click');
              $('.check-all1').prop('checked', false);
              $('.selected-item').addClass('d-none');

              $('#graduate-students-form')[0].reset();
              $('#graduateStudents').modal('hide');
            }else{
              toastr.error('Error!', result.errormsg,{"positionClass": "toast-bottom-right"});
              $('#loader_modal').modal('hide');
            }

          },
          error: function (request, status, error) {
                /*$('.form-control').removeClass('is-invalid');
                $('.form-control').next().remove();*/
                $('#loader_modal').modal('hide');
                $('.save-btn').removeClass('disabled');
                $('.save-btn').removeAttr('disabled');
                json = $.parseJSON(request.responseText);
                $.each(json.errors, function(key, value){
                      $('input[name="'+key+'"]').after('<span class="invalid-feedback" role="alert"><strong>'+value+'</strong>');
                     $('input[name="'+key+'"]').addClass('is-invalid');
                     $('textarea[name="'+key+'"]').after('<span class="invalid-feedback" role="alert"><strong>'+value+'</strong>');
                     $('textarea[name="'+key+'"]').addClass('is-invalid');


                });
            }
        });
  });

  var table2 =  $('.table-students').DataTable({
    processing: true,
    searching: true,
    "language": {
    processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw" style="color:#13436c;"></i><span class="sr-only">Loading...</span> '},
    ordering: false,
    pageLength: {{50}},
    serverSide: true,
    autoWidth: false,
    "lengthMenu": [50,100,150,200],

    scrollX:true,
    scrollY : '90vh',
    scrollCollapse: true,
    dom: 'Bfrtlip',
        buttons: [
            {
                extend: 'excelHtml5',
                exportOptions: {
                    columns: [3,4,5,6,7,8,9,10]
                }
            },
        ],
    ajax:
    {
      beforeSend: function(){
        $('#loader_modal').modal({
          backdrop: 'static',
          keyboard: false
        });
        $('#loader_modal').modal('show');
      },
      url: "{!! route('get-students') !!}",
       data: function(data) { data.student_classes_select = $('.student_classes_select option:selected').val(),data.discount_student = $('.discount_student option:selected').val() ,data.student_status = $('.student_status option:selected').val(),data.input_keyword = $('#input_keyword').val(), data.from_date = $('#from_date').val(), data.to_date = $('#to_date').val(),data.input_keyword_std = $('#input_keyword_std').val() } ,
    },
    columns: [
        { data: 'empty_col', name: 'empty_col' },
        { data: 'checkbox', name: 'checkbox' },
        { data: 'name', name: 'name', class: 'text-capitalize' },
        { data: 'class', name: 'class' },
        { data: 'roll_no', name: 'roll_no' },
        { data: 'gender', name: 'gender', class: 'text-capitalize' },
        { data: 'guardian', name: 'guardian', class: 'text-capitalize' },
        { data: 'guardian_phone', name: 'guardian_phone' },
        { data: 'total_payable', name: 'total_payable' },
        { data: 'fee', name: 'fee' },
        { data: 'discount', name: 'discount' },
        { data: 'suspended_date', name: 'suspended_date' },
        { data: 'action', name: 'action' }
    ],
    initComplete: function () {
    // Enable THEAD scroll bars
    $('.dataTables_scrollHead').css('overflow', 'auto');
    // Sync THEAD scrolling with TBODY
      $('.dataTables_scrollHead').on('scroll', function () {
          $('.dataTables_scrollBody').scrollLeft($(this).scrollLeft());
      });
    },
    drawCallback: function(){
      $('#loader_modal').modal('hide');
    },
    footerCallback: function ( row, data, start, end, display ) {
        var api              = this.api();
        var json             = api.ajax.json();
        var total_amount     = json.total_amount;

        total_amount     = parseFloat(total_amount).toFixed(2);
        total_amount     = total_amount.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ",");

        var res     = json.res;

        res     = parseFloat(res).toFixed(2);
        res     = res.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ",");

        $( api.column( 9 ).footer() ).html(res);
        $( api.column( 10 ).footer() ).html(total_amount);
      },
  });

    table2.on( 'draw.dt', function () {
    var PageInfo = $('.table-students').DataTable().page.info();
         table2.column(0, { page: 'current' }).nodes().each( function (cell, i) {
            cell.innerHTML = i + 1 + PageInfo.start;
        } );
    } );

  $('.search-btn').on('click',function(){
    $('.table-students').DataTable().ajax.reload();
  });

  $(document).on('change','#student_class',function(){
    // alert($(this).val());
    var id = $(this).val();

    $.ajaxSetup({
              headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
              }
            });
            $.ajax({
              url: "{{ route('fetch-fee') }}",
              method: 'post',
              data: {id:id},
              beforeSend: function(){
                $('#loader_modal').modal({
                  backdrop: 'static',
                  keyboard: false
                });
                $('#loader_modal').modal('show');
              },
              success: function (response) {

                if(response.success == true)
                {
                  // alert(response.fee);
                  $('#fee').val(response.fee);
                  $('#fee_before_discount').val(response.fee);
                $('#loader_modal').modal('hide');

                }

              },
              error: function(request, status, error){
                $('#loader_modal').modal('hide');
              }
            });
  });

  $(document).on('focusout','#discount',function(){
    // alert($(this).val());
    var discount = $(this).val();

    var fee = $('#fee_before_discount').val();

    $('#fee').val(parseFloat(fee) * parseFloat((100 - discount)/100));
  });

  $(document).on('focusout','#discount_amount',function(){
    // alert($(this).val());
    var discount = $(this).val();

    var fee = $('#fee_before_discount').val();

    $('#fee').val(parseFloat(fee) - parseFloat(discount));
    var perc = (discount / fee ) * 100;
    var final_per = parseFloat(perc).toFixed(2);
      $('#discount').val(final_per);
  });

    $('#add-student-form').on('submit',function(e){
    e.preventDefault();

    var name = $('#name').val();
    var guardian = $('#guardian').val();
    var guardian_nickname = $('#guardian_nickname').val();
    if(name == null || name == '')
    {
      toastr.error('Sorry!', 'Student Name Cannot Be Null!!!',{"positionClass": "toast-bottom-right"});
      return false;
    }

    if(guardian == null || guardian == '')
    {
      toastr.error('Sorry!', 'Father/Guardian Name Cannot Be Null!!!',{"positionClass": "toast-bottom-right"});
      return false;
    }
    if(guardian_nickname == null || guardian_nickname == '')
    {
      toastr.error('Sorry!', 'Father/Guardian Nick Name Cannot Be Null!!!',{"positionClass": "toast-bottom-right"});
      return false;
    }

    var guardian_cnic = $('#guardian_cnic').val();
    if(guardian_cnic == null || guardian_cnic == '')
    {
      toastr.error('Sorry!', 'Father/Guardian Cnic Cannot Be Null!!!',{"positionClass": "toast-bottom-right"});
      return false;
    }

    var guardian_phone = $('#guardian_phone').val();
    if(guardian_phone == null || guardian_phone == '')
    {
      toastr.error('Sorry!', 'Father/Guardian Phone No. Cannot Be Null!!!',{"positionClass": "toast-bottom-right"});
      return false;
    }

    var roll_no = $('#roll_no').val();
    if(roll_no == null || roll_no == '')
    {
      toastr.error('Sorry!', 'Admission Date Cannot Be Null!!!',{"positionClass": "toast-bottom-right"});
      return false;
    }

    var gender = $('#gender').val();
    if(gender == null || gender == '')
    {
      toastr.error('Sorry!', 'Gender Cannot Be Null!!!',{"positionClass": "toast-bottom-right"});
      return false;
    }

    var admission_date = $('#admission_date').val();
    if(admission_date == null || admission_date == '')
    {
      toastr.error('Sorry!', 'Admission Date Cannot Be Null!!!',{"positionClass": "toast-bottom-right"});
      return false;
    }

    var dob = $('#dob').val();
    if(dob == null || dob == '')
    {
      toastr.error('Sorry!', 'DoB Cannot Be Null!!!',{"positionClass": "toast-bottom-right"});
      return false;
    }

    var std_phone = $('#std_phone').val();
    if(std_phone == null || std_phone == '')
    {
      toastr.error('Sorry!', 'Student Phone No. Cannot Be Null!!!',{"positionClass": "toast-bottom-right"});
      return false;
    }

    var address = $('#address').val();
    if(address == null || address == '')
    {
      toastr.error('Sorry!', 'Address Cannot Be Null!!!',{"positionClass": "toast-bottom-right"});
      return false;
    }

    var student_class = $('#student_class').val();
    if(student_class == null || student_class == '')
    {
      toastr.error('Sorry!', 'Class Cannot Be Null!!!',{"positionClass": "toast-bottom-right"});
      return false;
    }

    var fee = $('#fee').val();
    if(fee == null || fee == '')
    {
      toastr.error('Sorry!', 'Fee Cannot Be Null!!!',{"positionClass": "toast-bottom-right"});
      return false;
    }

    // var image = $('#image').val();
    // if(image == null || image == '')
    // {
    //   toastr.error('Sorry!', 'Image Cannot Be Null!!!',{"positionClass": "toast-bottom-right"});
    //   return false;
    // }

    var discount = $('#discount').val();
    if(discount < 0 || discount > 100)
    {
      toastr.error('Sorry!', 'Discount must be greater than zero and less than 100!!!',{"positionClass": "toast-bottom-right"});
      return false;
    }

      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
          }
      });
       $.ajax({
          url: "{{ route('add-student') }}",
          dataType: 'json',
          type: 'post',
          data: new FormData(this),
          contentType: false,
          cache: false,
          processData:false,
          beforeSend: function(){
            $('.save-btn').addClass('disabled');
            $('.save-btn').attr('disabled', true);
            $('#loader_modal').modal({
              backdrop: 'static',
              keyboard: false
            });
            $('#loader_modal').modal('show');
          },
          success: function(result){
            $('.save-btn').attr('disabled', true);
            $('.save-btn').removeAttr('disabled');
            if(result.success == true){
              toastr.success('Success!', 'Student Added Successfully!!!',{"positionClass": "toast-bottom-right"});
              $('.table-students').DataTable().ajax.reload();
              /*setTimeout(function(){
                window.location.reload();
              }, 2000);*/

              $('#add-student-form')[0].reset();
              $('#addStudent').modal('hide');
              $('#loader_modal').modal('hide');

            }
            else if(result.roll_no_exists == true)
            {
              toastr.error('Error!', 'Student Admission No. Already Exist Please Change !!!',{"positionClass": "toast-bottom-right"});
              $('#loader_modal').modal('hide');
              // $('#addStudent').modal('hide');
            }
            else{
              toastr.error('Error!', result.errormsg,{"positionClass": "toast-bottom-right"});
              $('#loader_modal').modal('hide');
            }

          },
          error: function (request, status, error) {
                /*$('.form-control').removeClass('is-invalid');
                $('.form-control').next().remove();*/
                $('#loader_modal').modal('hide');
                $('.save-btn').removeClass('disabled');
                $('.save-btn').removeAttr('disabled');
                json = $.parseJSON(request.responseText);
                $.each(json.errors, function(key, value){
                      $('input[name="'+key+'"]').after('<span class="invalid-feedback" role="alert"><strong>'+value+'</strong>');
                     $('input[name="'+key+'"]').addClass('is-invalid');
                     $('textarea[name="'+key+'"]').after('<span class="invalid-feedback" role="alert"><strong>'+value+'</strong>');
                     $('textarea[name="'+key+'"]').addClass('is-invalid');


                });
            }
        });
  });
  $(document).on('click','.delete_student',function(){
      var id = $(this).data('id');

        swal({
            title: "Are You Sure!",
            html: true,
            text: "<b>You want to suspend this student !!!</b>",
            type: "info",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes!",
            cancelButtonText: "No!",
            closeOnConfirm: true,
            closeOnCancel: false
          },
            function(isConfirm) {
            if (isConfirm){
             $.ajaxSetup({
              headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
              }
            });
            $.ajax({
              url: "{{ route('delete-student') }}",
              method: 'post',
              data: {id:id},
              beforeSend: function(){
                $('#loader_modal').modal({
                  backdrop: 'static',
                  keyboard: false
                });
                $('#loader_modal').modal('show');
              },
              success: function (response) {
                if(response.success == false)
                {
                  toastr.warning('Sorry!', 'Cannot suspend this student, as it is already bond to record(s)!!!',{"positionClass": "toast-bottom-right"});
                  $('#loader_modal').modal('hide');
                }

                if(response.success == true)
                {
                  toastr.success('Success!', 'Student Suspended Successfully!!!',{"positionClass": "toast-bottom-right"});
                  $('.table-students').DataTable().ajax.reload();
                  $('#loader_modal').modal('hide');
                }

              },
              error: function(request, status, error){
                $('#loader_modal').modal('hide');
              }
            });
            }
            else{
              swal("Cancelled", "", "error");

            }
          });
  });

  $(document).on('click','.suspend_student',function(e){
    e.preventDefault();
    var id = $(this).data('id');
    $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
          }
      });
       $.ajax({
          url: "{{ route('delete-student') }}",
          // dataType: 'json',
          method: 'post',
          data: $('#edit-student-fee-form-'+id).serialize(),
          // contentType: false,
          // cache: false,
          // processData:false,
          beforeSend: function(){
            $('.save-btn').addClass('disabled');
            $('.save-btn').attr('disabled', true);
            $('#loader_modal').modal({
              backdrop: 'static',
              keyboard: false
            });
            $('#loader_modal').modal('show');
          },
          success: function(response){
            if(response.success == false)
                {
                  toastr.warning('Sorry!', 'Cannot suspend this student, as it is already bond to record(s)!!!',{"positionClass": "toast-bottom-right"});
                  $('#loader_modal').modal('hide');
                }

                if(response.success == true)
                {
                  toastr.success('Success!', 'Student Suspended Successfully!!!',{"positionClass": "toast-bottom-right"});
                  $('.table-students').DataTable().ajax.reload();
                  $('#loader_modal').modal('hide');
                }

          },
          error: function (request, status, error) {
                /*$('.form-control').removeClass('is-invalid');
                $('.form-control').next().remove();*/
                $('#loader_modal').modal('hide');
                $('.save-btn').removeClass('disabled');
                $('.save-btn').removeAttr('disabled');
                json = $.parseJSON(request.responseText);
                $.each(json.errors, function(key, value){
                      $('input[name="'+key+'"]').after('<span class="invalid-feedback" role="alert"><strong>'+value+'</strong>');
                     $('input[name="'+key+'"]').addClass('is-invalid');
                     $('textarea[name="'+key+'"]').after('<span class="invalid-feedback" role="alert"><strong>'+value+'</strong>');
                     $('textarea[name="'+key+'"]').addClass('is-invalid');


                });
            }
        });
  });

    $(document).on('click','.delete_student_permanant',function(){
      var id = $(this).data('id');

        swal({
            title: "Are You Sure!",
            html: true,
            text: "<b>You want to delete this student !!!</b>",
            type: "info",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes!",
            cancelButtonText: "No!",
            closeOnConfirm: true,
            closeOnCancel: false
          },
            function(isConfirm) {
            if (isConfirm){
             $.ajaxSetup({
              headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
              }
            });
            $.ajax({
              url: "{{ route('delete-student-permanent') }}",
              method: 'post',
              data: {id:id},
              beforeSend: function(){
                $('#loader_modal').modal({
                  backdrop: 'static',
                  keyboard: false
                });
                $('#loader_modal').modal('show');
              },
              success: function (response) {
                if(response.success == false)
                {
                  toastr.warning('Sorry!', 'Cannot delete this student, as it is already bond to record(s)!!!',{"positionClass": "toast-bottom-right"});
                  $('#loader_modal').modal('hide');
                }

                if(response.success == true)
                {
                  toastr.success('Success!', 'Student Deleted Successfully!!!',{"positionClass": "toast-bottom-right"});
                  $('.table-students').DataTable().ajax.reload();
                  $('#loader_modal').modal('hide');
                }

              },
              error: function(request, status, error){
                $('#loader_modal').modal('hide');
              }
            });
            }
            else{
              swal("Cancelled", "", "error");

            }
          });
  });

    $(document).on('click','.activate_student',function(){
      var id = $(this).data('id');

        swal({
            title: "Are You Sure!",
            html: true,
            text: "<b>You want to activate this student !!!</b>",
            type: "info",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes!",
            cancelButtonText: "No!",
            closeOnConfirm: true,
            closeOnCancel: false
          },
            function(isConfirm) {
            if (isConfirm){
             $.ajaxSetup({
              headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
              }
            });
            $.ajax({
              url: "{{ route('activate-student') }}",
              method: 'post',
              data: {id:id},
              beforeSend: function(){
                $('#loader_modal').modal({
                  backdrop: 'static',
                  keyboard: false
                });
                $('#loader_modal').modal('show');
              },
              success: function (response) {
                if(response.success == false)
                {
                  toastr.warning('Sorry!', 'Cannot delete this class, as it is already bond to student(s)!!!',{"positionClass": "toast-bottom-right"});
                  $('#loader_modal').modal('hide');
                }

                if(response.success == true)
                {
                  toastr.success('Success!', 'Student Activated Successfully!!!',{"positionClass": "toast-bottom-right"});
                  $('.table-students').DataTable().ajax.reload();
                  $('#loader_modal').modal('hide');
                }

              },
              error: function(request, status, error){
                $('#loader_modal').modal('hide');
              }
            });
            }
            else{
              swal("Cancelled", "", "error");

            }
          });
  });

  $(document).on('change','.student_classes_select',function(){
    $('.table-students').DataTable().ajax.reload();
    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
  });

  $(document).on('change','.discount_student',function(){
    $('.table-students').DataTable().ajax.reload();
    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
  });
  $(document).on('change','#from_date',function(){
    $('.table-students').DataTable().ajax.reload();
    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
  });

   $(document).on('change','#to_date',function(){
    $('.table-students').DataTable().ajax.reload();
    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
  });

    $(document).on('change','.student_status',function(){
    $('.table-students').DataTable().ajax.reload();
    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
  });

  $(document).on('click','#reset',function(){
    $('.student_classes_select').val('');
    $('.discount_student').val('');
    $('.student_status').val('');
    $('#input_keyword').val('');
    $('#input_keyword_std').val('');
    $('.table-students').DataTable().ajax.reload();
    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
  });

  $('#image').on('change', function() {
        let size = this.files[0].size; // this is in bytes
        if (size > 1000000) {
        // do something. Prevent form submit. Show message, etc.
          toastr.error('Sorry!', 'Student Photo Size Must Be Less Than 1MB!!!',{"positionClass": "toast-bottom-right"});

        }
        else
        {
          $('.update_image').removeAttr('disabled');
        }
    });

$('.upload-excel-form').on('submit',function(e){
  $('#import-modal').modal('hide');
  e.preventDefault();
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
    }
  });
  $.ajax({
    url: "{{ route('upload-students-excel-file') }}",
    method: 'post',
    data: new FormData(this),
    cache: false,
    contentType: false,
    processData: false,
    beforeSend: function(){
      $('#loader_modal').modal({
        backdrop: 'static',
        keyboard: false
      });
      $("#loader_modal").data('bs.modal')._config.backdrop = 'static';
      $('#loader_modal').modal('show');
      $(".product-upload-btn").attr("disabled", true);
    },
    success: function(data){
      $('#loader_modal').modal('hide');
      $('#import-modal').modal('hide');
      $(".product-upload-btn").attr("disabled", false);
      if(data.success == true)
      {
        toastr.success('Success!', data.msg, {"positionClass": "toast-bottom-right"});
        if(data.errorMsg != null && data.errorMsg != '')
        {
          $('#msgs_alerts').html(data.errorMsg);
          $('.errormsgDiv').removeClass('d-none');
        }
        $('.exp_imp_btn').click();
        $('.upload-excel-form')[0].reset();
        $('.product_table').DataTable().ajax.reload();
        // $('.table-purchase-order-history').DataTable().ajax.reload();
      }
      if(data.success == "withissues")
      {
        toastr.warning('Warning!', data.msg, {"positionClass": "toast-bottom-right"});
        if(data.errorMsg != null && data.errorMsg != '')
        {
          $('#msgs_alerts').html(data.errorMsg);
          $('.errormsgDiv').removeClass('d-none');
        }
        // $('.exp_imp_btn').click();
        $('.upload-excel-form')[0].reset();
        $('.product_table').DataTable().ajax.reload();
        // $('.table-purchase-order-history').DataTable().ajax.reload();
      }
      if(data.success == false)
      {
        toastr.error('Error!', data.msg, {"positionClass": "toast-bottom-right"});
        // $('.exp_imp_btn').click();
        $('.upload-excel-form')[0].reset();
        $('.product_table').DataTable().ajax.reload();
        // $('.table-purchase-order-history').DataTable().ajax.reload();
      }
    },
    error: function (request, status, error) {
        $('#loader_modal').modal('hide');
        $(".product-upload-btn").attr("disabled", false);
        $('.po-porducts-details').DataTable().ajax.reload();
        $('.table-purchase-order-history').DataTable().ajax.reload();
        json = $.parseJSON(request.responseText);
        $.each(json.errors, function(key, value){
          $('input[name="'+key+'[]"]').after('<span class="invalid-feedback" role="alert"><strong>'+value+'</strong>');
          $('input[name="'+key+'[]"]').addClass('is-invalid');
        });
      }
  });
});

$('.generate_rollno').on('click',function(e){
  var exam_id = $('.student_exams_select').val();
  if(exam_id == ""){
    toastr.warning('Sorry!', 'Please select exam first!!!',{"positionClass": "toast-bottom-right"});
    return false;
  }
  var selected_fees = [];
  $("input.check:checked").each(function() {
    selected_fees.push($(this).val());
  });

  if(selected_fees == ''){
          toastr.error('Error!', 'Select Student(s) First.',{"positionClass": "toast-bottom-right"});
      return false;
      }else{

      var url = "{{url('user/student-exam-rollno')}}"+"/"+selected_fees+"/"+exam_id;
                  window.open(url, 'Orders Receivable Print', 'width=1200,height=600,scrollbars=yes');
                $('#loader_modal').modal('hide');
      }
})
});
</script>
@stop

