@extends('user.layouts.layout')

@section('title','Online Marketing')

@section('content')
<style type="text/css">
   
</style>

                    <div>
                        <div class="row page-title-row">
                            <div class="col-md col-sm col-12 page-title-col align-self-center">
                                <h1 class="page-title">Renew Monthly Fees</h1>
                            </div>
                            
                    </div>

                    <div class="row mb-0">
                      <div class="col-lg-12 col-md-12 title-col">
                        <div class="d-sm-flex justify-content-center align-items-center">
                          <div class="col">
                            <label for="to_date" style="visibility: hidden;">To Date:</label>
                            <select class="form-control form-control student_classes_select state-tags" name="warehouse_id" >
                              <option value="">-- All Classes --</option>
                               @foreach($classes as $class)
                                <option value="{{$class->id}}"> {{$class->class_name}} </option>
                                @endforeach
                            </select>
                          </div>

                           <div class="col">
                            <label for="from_date"><b>From Date :</b></label>
                              <input type="date" name="from_date" id="from_date" class="form-control" placeholder="Search by year ..." />
                          </div>

                          <div class="col">
                            <label for="to_date"><b>To Date :</b></label>
                              <input type="date" name="to_date" id="to_date" class="form-control" placeholder="Search by year ..." />
                          </div>
                    
                          <div class="col-lg-2 col-md-2">
                            <label for="to_date" style="visibility: hidden;">To Date:</label>
                            <div class="input-group-append">
                              <button class="btn btn-primary" type="reset" id="reset" style="padding: 5px 30px;">Reset</button>  
                            </div>
                          </div>
                          {{-- <div class="col-lg-1 col-md-1"></div> --}}

                        </div>
                      </div>
                    </div>

                    <div class="row p-0 mt-0">
                        <div class="selected-item catalogue-btn-group mt-4 mt-sm-3 ml-3 d-none col-12">
    
                            <a href="javascript:void(0)" class="actionicon viewIcon download_invoice" data-id="1" title="Download Invoice"><i class="zmdi zmdi-download"></i></a>

                             <a href="javascript:void(0)" class="actionicon viewIcon download_invoice1" data-id="1" title="Download Invoice"><i class="zmdi zmdi-download"></i> Print On Single Page</a>

                             <a href="javascript:void(0)" class="actionicon viewIcon download_invoice2" data-id="1" title="Make Fee Entry"><i class="fa fa-check"></i> Make Fee Entry</a>

                           <label for="fee_month_date" style="margin-left: 40px;"><b>Fee Month :</b></label>

                            <input type="date" name="fee_month_date" id="fee_month_date" class="form-control d-inline-block" style="width: 30%" />


                        </div>
                        <div class="col-12 mt-4">
                        <div class="card p-4">
                        <div class="table-responsive">
                        <table class="table table-striped nowrap table-students-fee">
                        <thead>
                        <tr>
                          <th class="noVis">
                              <div class="custom-control custom-checkbox custom-checkbox1 d-inline-block">
                                <input type="checkbox" class="custom-control-input check-all1" name="check_all" id="check-all">
                                <label class="custom-control-label" for="check-all"></label>
                              </div>
                            </th>
                            <th>S.No</th>
                            <th>Action</th>
                            <th>Admission No.</th>
                            <th>Name</th>
                            <th>Class</th>
                            <th>Gender</th>
                            <th>Father/Guardian Name</th>
                            <th>Fee(Inc Discount)</th>
                            <th>Discount</th>
                        </tr>
                        </thead>
                        </table>
                        </div>
                    </div>
                </div>
                    </div>
                    </div>
                <input type="hidden" name="fees_selected" class="fees_selected">



@endsection

@section('javascript')
<script type="text/javascript">
$(document).ready(function(){
        $(document).on('click', '.check-all1', function () {
        if(this.checked == true){
        $('.check').prop('checked', true);
        $('.check').parents('tr').addClass('selected');
        var cb_length = $( ".check:checked" ).length;
        if(cb_length > 0){
          $('.selected-item').removeClass('d-none');
        }
      }else{
        $('.check').prop('checked', false);
        $('.check').parents('tr').removeClass('selected');
        $('.selected-item').addClass('d-none');
        
      }
    });

   $(document).on('click', '.check', function () {
    // $(this).removeClass('d-none');
        var cb_length = $( ".check:checked" ).length;
        var st_pieces = $(this).parents('tr').attr('data-pieces');
        if(this.checked == true){
        $('.selected-item').removeClass('d-none');
        $(this).parents('tr').addClass('selected');
      }else{
        $(this).parents('tr').removeClass('selected');
        if(cb_length == 0){
         $('.selected-item').addClass('d-none');
        }
        
      }
    });

    $(document).on('click', '.download_invoice', function(){
    var selected_fees = [];
    $("input.check:checked").each(function() {
      selected_fees.push($(this).val());
    });

    if(selected_fees == ''){
            toastr.error('Error!', 'Select Students First.',{"positionClass": "toast-bottom-right"});
        return false;
        }else{
        $('.fees_selected').val(selected_fees);

        var fees = $('.fees_selected').val();

        var url = "{{url('user/fee-invoice-print-advance')}}"+"/"+fees+"/"+$('#from_date').val();
                    window.open(url, 'Orders Receivable Print', 'width=1200,height=600,scrollbars=yes');
                  $('#loader_modal').modal('hide');
                }
    });

    $(document).on('click', '.download_invoice1', function(){
    var selected_fees = [];
    $("input.check:checked").each(function() {
      selected_fees.push($(this).val());
    });

    if(selected_fees == ''){
            toastr.error('Error!', 'Select Students First.',{"positionClass": "toast-bottom-right"});
        return false;
        }else{
        $('.fees_selected').val(selected_fees);

        var fees = $('.fees_selected').val();

        var url = "{{url('user/fee-invoice-print-advance-all')}}"+"/"+fees+"/"+$('#from_date').val();
                    window.open(url, 'Orders Receivable Print', 'width=1200,height=600,scrollbars=yes');
                  $('#loader_modal').modal('hide');
                }
    });

        $(document).on('click', '.download_invoice2', function(){
          var m_date = $('#fee_month_date').val();
          var class_id = $('.student_classes_select').val();
          var selected_fees = [];
          $("input.check:checked").each(function() {
            selected_fees.push($(this).val());
          });

          if(selected_fees == ''){
                  toastr.error('Error!', 'Select Students First.',{"positionClass": "toast-bottom-right"});
                  return false;
              }
          if(m_date == '')
              {
                  toastr.info('Info!', 'Please Select Fee Month First!!!',{"positionClass": "toast-bottom-right"});
                  return false;
              }
              if(true){
              $('.fees_selected').val(selected_fees);

              var fees = $('.fees_selected').val();

        swal({
            title: "Are You Sure!",
            html: true,
            text: "<b>You want to make fee entry!!!</b>",
            type: "info",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes!",
            cancelButtonText: "No!",
            closeOnConfirm: true,
            closeOnCancel: false
          },
            function(isConfirm) {
            if (isConfirm){
             $.ajaxSetup({
              headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
              }
            });
            $.ajax({
              url: "{{ route('make-fee-entry') }}",
              method: 'post',
              data: {std_ids:fees,m_date:m_date,class_id:class_id},
              beforeSend: function(){
                $('#loader_modal').modal({
                  backdrop: 'static',
                  keyboard: false
                });
                $('#loader_modal').modal('show');
              },
              success: function (response) {
                if(response.success == false)
                {
                  toastr.warning('Sorry!', 'Cannot delete this class, as it is already bond to student(s)!!!',{"positionClass": "toast-bottom-right"});
                  $('#loader_modal').modal('hide');
                }

                if(response.success == true)
                {
                  toastr.success('Success!', 'Fee Entry Submitted Successfully!!!',{"positionClass": "toast-bottom-right"});
                  $('.table-students-fee').DataTable().ajax.reload();
                  $('#loader_modal').modal('hide');
                }
               
              },
              error: function(request, status, error){
                $('#loader_modal').modal('hide');
              }
            });
            }
            else{
              swal("Cancelled", "", "error");
              
            }
          });

              
              }
          });
  var table2 =  $('.table-students-fee').DataTable({
    processing: true,
    searching: true,
    "language": {
    processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw" style="color:#13436c;"></i><span class="sr-only">Loading...</span> '},
    ordering: false,
    pageLength: {{50}},
    serverSide: true,
    autoWidth: false,
    "lengthMenu": [50,100,150,200],
    
    scrollX:true,
    scrollY : '90vh',
    scrollCollapse: true,
    ajax: 
    {
      beforeSend: function(){
        $('#loader_modal').modal({
          backdrop: 'static',
          keyboard: false
        });
        $('#loader_modal').modal('show');
      },
      url: "{!! route('get-students-unsubmitted-fee') !!}",
       data: function(data) { data.student_classes_select = $('.student_classes_select option:selected').val(),data.from_date = $('#from_date').val(), data.to_date = $('#to_date').val() } ,
    },
    columns: [
        { data: 'checkbox', name: 'checkbox' }, 
        { data: 'id', name: 'id' }, 
        { data: 'action', name: 'action' }, 
        { data: 'roll_no', name: 'roll_no' }, 
        { data: 'name', name: 'name' }, 
        { data: 'class', name: 'class' }, 
        { data: 'gender', name: 'gender' }, 
        { data: 'guardian', name: 'guardian' },  
        { data: 'fee', name: 'fee' }, 
        { data: 'discount', name: 'discount' }, 
    ],
    initComplete: function () {
    // Enable THEAD scroll bars
    $('.dataTables_scrollHead').css('overflow', 'auto');
    // Sync THEAD scrolling with TBODY
      $('.dataTables_scrollHead').on('scroll', function () {
          $('.dataTables_scrollBody').scrollLeft($(this).scrollLeft());
      });
    },
    drawCallback: function(){
      $('#loader_modal').modal('hide');
    },
  });

    table2.on( 'draw.dt', function () {
    var PageInfo = $('.table-students-fee').DataTable().page.info();
         table2.column(1, { page: 'current' }).nodes().each( function (cell, i) {
            cell.innerHTML = i + 1 + PageInfo.start;
        } );
    } );

      $(document).on('change','.student_classes_select',function(){
    $('.table-students-fee').DataTable().ajax.reload();
    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
  });

   $(document).on('change','#from_date',function(){
    $('.table-students-fee').DataTable().ajax.reload();
    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
  });

   $(document).on('change','#to_date',function(){
    $('.table-students-fee').DataTable().ajax.reload();
    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
  });


  $(document).on('click','#reset',function(){
    $('.student_classes_select').val('');
    $('#from_date').val('');
    $('#to_date').val('');
    $('.table-students-fee').DataTable().ajax.reload();
    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
  });
});
</script>
@stop

