@extends('user.layouts.layout')

@section('title','Online Marketing')

@section('content')
<div class="bg-white">
  <form id="add-student-form" method="POST" enctype="multipart/form-data">
      <div class="modal-body">

          <div class="form-row row-cols-lg-3 row-cols-sm-2 row-cols-2">
            <div class="form-group col">
                <label for="name">Student Name</label>
                <input type="text" name="name" id="name" class="form-control" placeholder="Student Name"/>
            </div>
            <div class="form-group col">
                <label for="guardian">Father Name</label>
                <input type="text" name="guardian" id="guardian" class="form-control" placeholder="Student Father Name"/>
            </div>
            <div class="form-group col">
                <label for="guardian_nickname">Father Nick Name</label>
                <input type="text" name="guardian_nickname" id="guardian_nickname" class="form-control" placeholder="Student Father Nick Name"/>
            </div>

            <div class="form-group col">
                <label for="guardian_cnic">Father Cnic</label>
                <input type="text" name="guardian_cnic" id="guardian_cnic" class="form-control" placeholder="Father Cnic"/>
            </div>
            <div class="form-group col">
                <label for="guardian_phone">Father Phone No.</label>
                <input type="number" name="guardian_phone" id="guardian_phone" class="form-control" placeholder="Father Phone No."/>
            </div>

            <div class="form-group col">
                <label for="roll_no">Admission No</label>
                <input type="text" name="roll_no" id="roll_no" class="form-control" placeholder="Admission No."/>
            </div>

             <div class="form-group col">
                <label for="gender">Gender</label>
                <select class="form-control" id="gender" name="gender">
                  <option value="">--Select Gender--</option>
                  <option value="male">Male</option>
                  <option value="female">Female</option>
                  <option value="other">Other</option>
                </select>
              </div>

            <div class="form-group col">
                <label for="admission_date">Admission Dat</label>
                <input type="date" name="admission_date" id="admission_date" class="form-control" placeholder="Admission Date"/>
            </div>
            <div class="form-group col">
                <label for="dob">Date of Birth</label>
                <input type="date" name="dob" id="dob" class="form-control" placeholder="DoB"/>
            </div>

            <div class="form-group col">
                <label for="std_phone">Phone No</label>
                <input type="text" name="std_phone" id="std_phone" class="form-control" placeholder="Phone No"/>
            </div>
            <div class="form-group col">
                <label for="address">Address</label>
                <input type="text" name="address" id="address" class="form-control" placeholder="Address"/>
            </div>

            <div class="form-group col">
              <label for="student_class">Class</label>
              <select class="form-control" id="student_class" name="student_class">
                <option value="">--Select Class--</option>
                @foreach($classes as $class)
                <option value="{{$class->id}}">{{$class->class_name}} </option>
                @endforeach
              </select>
            </div>

            <div class="form-group col">
                <label for="fee">Fee</label>
                <input type="text" name="fee" id="fee" class="form-control" placeholder="0.00" readonly="" />
                <input type="hidden" name="fee_before_discount" id="fee_before_discount" class="form-control" placeholder="0.00" disabled="disabled" />
            </div>

            <div class="form-group col">
                <label for="discount_amount">Fee Discount Amount</label>
                <input type="number" name="discount_amount" id="discount_amount" class="form-control" placeholder="Fee Discount Amount"/>
            </div>

              <div class="form-group col">
                <label for="discount">Fee Discount</label>
                <input type="number" name="discount" id="discount" min="0" max="100" class="form-control" placeholder="Fee Discount" readonly="" />
            </div>

            <div class="form-group col">
                <label for="image">Upload Photo</label>
                <input type="file" name="image" id="image" class="form-control" placeholder="Image" accept="image/*" />
            </div>
            </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save</button>
      </div>
      </form>
</div>
@endsection

@section('javascript')
<script type="text/javascript">
$(document).ready(function(){
  $('#add-student-form').on('submit',function(e){
    e.preventDefault();

    var name = $('#name').val();
    var guardian = $('#guardian').val();
    var guardian_nickname = $('#guardian_nickname').val();
    if(name == null || name == '')
    {
      toastr.error('Sorry!', 'Student Name Cannot Be Null!!!',{"positionClass": "toast-bottom-right"});
      return false;
    }

    if(guardian == null || guardian == '')
    {
      toastr.error('Sorry!', 'Father/Guardian Name Cannot Be Null!!!',{"positionClass": "toast-bottom-right"});
      return false;
    }
    if(guardian_nickname == null || guardian_nickname == '')
    {
      toastr.error('Sorry!', 'Father/Guardian Nick Name Cannot Be Null!!!',{"positionClass": "toast-bottom-right"});
      return false;
    }

    var guardian_cnic = $('#guardian_cnic').val();
    if(guardian_cnic == null || guardian_cnic == '')
    {
      toastr.error('Sorry!', 'Father/Guardian Cnic Cannot Be Null!!!',{"positionClass": "toast-bottom-right"});
      return false;
    }

    var guardian_phone = $('#guardian_phone').val();
    if(guardian_phone == null || guardian_phone == '')
    {
      toastr.error('Sorry!', 'Father/Guardian Phone No. Cannot Be Null!!!',{"positionClass": "toast-bottom-right"});
      return false;
    }

    var roll_no = $('#roll_no').val();
    if(roll_no == null || roll_no == '')
    {
      toastr.error('Sorry!', 'Admission Date Cannot Be Null!!!',{"positionClass": "toast-bottom-right"});
      return false;
    }

    var gender = $('#gender').val();
    if(gender == null || gender == '')
    {
      toastr.error('Sorry!', 'Gender Cannot Be Null!!!',{"positionClass": "toast-bottom-right"});
      return false;
    }

    var admission_date = $('#admission_date').val();
    if(admission_date == null || admission_date == '')
    {
      toastr.error('Sorry!', 'Admission Date Cannot Be Null!!!',{"positionClass": "toast-bottom-right"});
      return false;
    }

    var dob = $('#dob').val();
    if(dob == null || dob == '')
    {
      toastr.error('Sorry!', 'DoB Cannot Be Null!!!',{"positionClass": "toast-bottom-right"});
      return false;
    }

    var std_phone = $('#std_phone').val();
    if(std_phone == null || std_phone == '')
    {
      toastr.error('Sorry!', 'Student Phone No. Cannot Be Null!!!',{"positionClass": "toast-bottom-right"});
      return false;
    }

    var address = $('#address').val();
    if(address == null || address == '')
    {
      toastr.error('Sorry!', 'Address Cannot Be Null!!!',{"positionClass": "toast-bottom-right"});
      return false;
    }

    var student_class = $('#student_class').val();
    if(student_class == null || student_class == '')
    {
      toastr.error('Sorry!', 'Class Cannot Be Null!!!',{"positionClass": "toast-bottom-right"});
      return false;
    }

    var fee = $('#fee').val();
    if(fee == null || fee == '')
    {
      toastr.error('Sorry!', 'Fee Cannot Be Null!!!',{"positionClass": "toast-bottom-right"});
      return false;
    }

    // var image = $('#image').val();
    // if(image == null || image == '')
    // {
    //   toastr.error('Sorry!', 'Image Cannot Be Null!!!',{"positionClass": "toast-bottom-right"});
    //   return false;
    // }

    var discount = $('#discount').val();
    if(discount < 0 || discount > 100)
    {
      toastr.error('Sorry!', 'Discount must be greater than zero and less than 100!!!',{"positionClass": "toast-bottom-right"});
      return false;
    }

      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
          }
      });
       $.ajax({
          url: "{{ route('add-student') }}",
          dataType: 'json',
          type: 'post',
          data: new FormData(this),
          contentType: false,
          cache: false,
          processData:false,
          beforeSend: function(){
            $('.save-btn').addClass('disabled');
            $('.save-btn').attr('disabled', true);
            $('#loader_modal').modal({
              backdrop: 'static',
              keyboard: false
            });
            $('#loader_modal').modal('show');
          },
          success: function(result){
            $('.save-btn').attr('disabled', true);
            $('.save-btn').removeAttr('disabled');
            if(result.success == true){
              toastr.success('Success!', 'Student Added Successfully!!!',{"positionClass": "toast-bottom-right"});
              $('#add-student-form')[0].reset();
              $('#loader_modal').modal('hide');

            }
            else if(result.roll_no_exists == true)
            {
              toastr.error('Error!', 'Student Admission No. Already Exist Please Change !!!',{"positionClass": "toast-bottom-right"});
              $('#loader_modal').modal('hide');
              // $('#addStudent').modal('hide');
            }
            else{
              toastr.error('Error!', result.errormsg,{"positionClass": "toast-bottom-right"});
              $('#loader_modal').modal('hide');
            }

          },
          error: function (request, status, error) {
                /*$('.form-control').removeClass('is-invalid');
                $('.form-control').next().remove();*/
                $('#loader_modal').modal('hide');
                $('.save-btn').removeClass('disabled');
                $('.save-btn').removeAttr('disabled');
                json = $.parseJSON(request.responseText);
                $.each(json.errors, function(key, value){
                      $('input[name="'+key+'"]').after('<span class="invalid-feedback" role="alert"><strong>'+value+'</strong>');
                     $('input[name="'+key+'"]').addClass('is-invalid');
                     $('textarea[name="'+key+'"]').after('<span class="invalid-feedback" role="alert"><strong>'+value+'</strong>');
                     $('textarea[name="'+key+'"]').addClass('is-invalid');


                });
            }
        });
  });
});
</script>
@stop

