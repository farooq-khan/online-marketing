@extends('user.layouts.layout')

@section('title','Online Marketing')

@section('content')
<style type="text/css">
   
</style>

                        <div class="row page-title-row">
                            <div class="col-12">
                                <h1 class="page-title">Pass Out Students</h1>
                            </div>
                        </div>


                        <div class="card">
                          <div class="card-body">
                              <div class="form-row align-items-end pb-2">
                                <div class="form-group col-sm col-6">
                                  <label>Pass out students</label>
                                  <select class="form-control form-control graduate_date_filter state-tags" name="warehouse_id" >
                                    <option value="">-- Pass Out Date --</option>
                                    @foreach($graduate_dates as $dat)
                                      <option value="{{$dat->graduate_date}}"> Pass Out Date ({{$dat->graduate_date}}) </option>
                                    @endforeach
                                  </select>
                                </div>
                                 <div class="form-group col-sm col-6">
                                  <label for="from_date">From Date</label>
                                    <input type="date" name="from_date" id="from_date" class="form-control" placeholder="Passing Out From Date ..." />
                                </div>
                                <div class="form-group col-sm col-6">
                                  <label for="to_date">To Date</label>
                                    <input type="date" name="to_date" id="to_date" class="form-control" placeholder="Passing Out To Date ..." />
                                </div>
                                <div class="form-group col-sm-auto col-6">
                                    <button class="btn btn-outline-danger px-lg-4 px-3" type="reset" id="reset">Reset</button>  
                                </div>
                              </div>
                              <table class="table nowrap table-striped table-students">
                                <thead>
                                <tr>
                                    <th>Action</th>
                                    <th>Admission No.</th>
                                    <th>Name</th>
                                    <th>Class</th>
                                    <th>Gender</th>
                                    <th>Father/Guardian Name</th>
                                    <th>Father/Guardian Phone</th>
                                    <th>Fee(Inc Discount)</th>
                                    <th>Discount</th>
                                    <th>Pass Out Date</th>
                                </tr>
                                </thead>
                              </table>
                          </div>
                        </div>

@endsection

@section('javascript')
<script type="text/javascript">
$(document).ready(function(){

  var table2 =  $('.table-students').DataTable({
    processing: true,
    searching: true,
    "language": {
    processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw" style="color:#13436c;"></i><span class="sr-only">Loading...</span> '},
    ordering: false,
    pageLength: {{50}},
    serverSide: true,
    autoWidth: false,
    "lengthMenu": [50,100,150,200],
    
    scrollX:true,
    scrollY : '90vh',
    scrollCollapse: true,
    ajax: 
    {
      beforeSend: function(){
        $('#loader_modal').modal({
          backdrop: 'static',
          keyboard: false
        });
        $('#loader_modal').modal('show');
      },
      url: "{!! route('get-passout-students') !!}",
       data: function(data) {data.graduate_date = $('.graduate_date option:selected').val(),data.from_date = $('#from_date').val(), data.to_date = $('#to_date').val()} ,
    },
    columns: [
        { data: 'action', name: 'action' }, 
        { data: 'roll_no', name: 'roll_no' }, 
        { data: 'name', name: 'name' }, 
        { data: 'class', name: 'class' }, 
        { data: 'gender', name: 'gender' }, 
        { data: 'guardian', name: 'guardian' }, 
        { data: 'guardian_phone', name: 'guardian_phone' }, 
        { data: 'fee', name: 'fee' }, 
        { data: 'discount', name: 'discount' }, 
        { data: 'passout_date', name: 'passout_date' }, 
    ],
    initComplete: function () {
    // Enable THEAD scroll bars
    $('.dataTables_scrollHead').css('overflow', 'auto');
    // Sync THEAD scrolling with TBODY
      $('.dataTables_scrollHead').on('scroll', function () {
          $('.dataTables_scrollBody').scrollLeft($(this).scrollLeft());
      });
    },
    drawCallback: function(){
      $('#loader_modal').modal('hide');
    },
  });

    $(document).on('change','.graduate_date_filter',function(){
    $('.table-students').DataTable().ajax.reload();
    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
  });
    $(document).on('change','#from_date',function(){
    $('.table-students').DataTable().ajax.reload();
    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
  });

   $(document).on('change','#to_date',function(){
    $('.table-students').DataTable().ajax.reload();
    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
  });

  $(document).on('click','#reset',function(){
    $('.graduate_date_filter').val('');
    $('#from_date').val('');
    $('#to_date').val('');
    $('.table-students').DataTable().ajax.reload();
    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
  })
});
</script>
@stop

