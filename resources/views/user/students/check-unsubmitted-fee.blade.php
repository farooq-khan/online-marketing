@extends('user.layouts.layout')

@section('title','Online Marketing')

@section('content')
<style type="text/css">
   
</style>
                    <div class="row page-title-row">
                            <div class="col-md col-sm col-12 page-title-col align-self-center">
                                <h1 class="page-title">Students Unsubmitted Fee History</h1>
                            </div>
                            
                    </div>

                    <div class="card card-border-top mb-4">
                      <div class="card-header">
                            <h6 class="mb-0">Filters</h6>
                      </div>
                      <div class="card-body pb-2">
                        <div class="form-row mb-0">
                          <div class="col-sm col-12 form-group">
                            <label for="to_date">Class</label>
                            <select class="form-control form-control student_classes_select state-tags" name="warehouse_id" >
                              <option value="">-- Select Class(s) --</option>
                                <option value="all">All Classes</option>
                               @foreach($classes as $class)
                                <option value="{{$class->id}}"> {{$class->class_name}} </option>
                                @endforeach
                            </select>
                          </div>
                           <div class="col form-group">
                            <label for="from_date">From Date</label>
                              <input type="date" name="from_date" id="from_date" class="form-control" placeholder="Search by year ..." />
                          </div>
                          <div class="col form-group">
                            <label for="to_date">To Date</label>
                              <input type="date" name="to_date" id="to_date" class="form-control" placeholder="Search by year ..." />
                          </div>
                          <div class="col-xl-auto col-12 ml-auto text-right mb-3 align-self-end">
                            <button class="btn btn-outline-primary px-3" type="reset" id="reset">Reset</button>
                          </div>
                          {{-- <div class="col-lg-1 col-md-1"></div> --}}

                        </div>
                      </div>
                    </div>


                        <div class="card">
                        <div class="card-body">
                          <div class="selected-item catalogue-btn-group mb-2 pb-1 d-none">
    
                            <!-- <a href="javascript:void(0)" class="actionicon viewIcon " data-id="1" title="Download Invoice"><i class="zmdi zmdi-download"></i></a> -->
                            <button class="btn btn-outline-primary mb-1 download_invoice" title="Download Invoice"><i class="fa fa-download"></i> Download Invoice</button>

                             <!-- <a href="javascript:void(0)" class="actionicon viewIcon download_invoice1" data-id="1" title="Download Invoice"><i class="fa fa-download"></i> Print On Single Page</a> -->

                             <button class="btn btn-outline-primary mb-1 download_invoice1" title="Download Invoice" data-id="1"><i class="fa fa-download"></i> Print On Single Page</button>

                             <!-- <a href="javascript:void(0)" class="actionicon viewIcon download_invoice2" data-id="1" title="Make Fee Entry"><i class="fa fa-check"></i> Make Fee Entry</a> -->
                             <button type="button" class="btn btn-outline-primary mb-1" data-toggle="modal" data-target="#sumbitOtherFee">Make Other Fee Entry</button>

                             <button class="btn btn-outline-primary mb-1 download_invoice2" title="Make Fee Entry" data-id="1"><i class="fa fa-check"></i> Make Fee Entry</button>

                           <label for="fee_month_date" style="margin-left: 40px;">Fee Month</label>

                            <input type="date" name="fee_month_date" id="fee_month_date" class="form-control d-inline-block" style="width: 30%" />
                        </div>
                        <table class="table table-striped nowrap table-students-fee">
                        <thead>
                        <tr>
                          <th class="noVis">
                              <div class="custom-control custom-checkbox custom-checkbox1 d-inline-block">
                                <input type="checkbox" class="custom-control-input check-all1" name="check_all" id="check-all">
                                <label class="custom-control-label" for="check-all"></label>
                              </div>
                            </th>
                            <th>S.No</th>
                            <th>Name</th>
                            <th>Class</th>
                            <th>Admission No.</th>
                            <th>Discount</th>
                            <th>Gender</th>
                            <th>Father/Guardian Name</th>
                            <th>Fee(Inc Discount)</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        </table>
                        </div>
                    </div>
       
                        <div class="c mt-4">
                          <h4 class="mb-3">Fee Renew History</h4>

                                <div class="card p-4">
                                <div class="table-responsive">
                                <table class="table table-striped nowrap table-fee-renew">
                                <thead>
                                <tr>
                                    <th>S.No</th>
                                    <th>Class</th>
                                    <th>Fee Month</th>
                                    <th>Fee Type</th>
                                    <th>Created By</th>
                                    <th>Created Date</th>
                                </tr>
                                </thead>
                                </table>
                                </div>
                            </div>
                        </div>

                <input type="hidden" name="fees_selected" class="fees_selected">



<!--Make Other Fee Entry Modal -->
<div class="modal fade" id="sumbitOtherFee" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Make  Other Fee Entry</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form id="submit-other-fee-form" method="POST">
        @csrf
      <div class="modal-body ">
        <input type="hidden" name="other_fee_std_ids" value="" class="other_fee_std_ids">
        <input type="hidden" name="other_fee_class_id" value="" class="other_fee_class_id">
          <div class="form-row">
            <div class="form-group col-md-6">
                <label for="fee_type"><i class="zmdi zmdi-account material-icons-name pr-2"> Fee Type</i></label>
                <select class="form-control" id="fee_type" name="fee_type" style="" required="true">
                  <option value="">--Select Fee Type--</option>
                  @foreach($fee_types as $type)
                  <option value="{{$type->id}}"> {{$type->title}} </option>
                  @endforeach
                </select>
            </div>

            <div class="form-group col-md-6">
                <label for="amount"><i class="zmdi zmdi-money-off pr-2"></i>Total Amount</label>
                <input type="number" name="amount" id="amount" class="form-control" placeholder="Enter Total Amount..." required="true" />
            </div>

            <!-- <div class="form-group col-md-6">
                <label for="paid_amount"><i class="zmdi zmdi-money-off pr-2"></i>Paid Amount</label>
                <input type="number" name="paid_amount" id="paid_amount" class="form-control" placeholder="Enter Paid Amount..." required="true" />
            </div> -->

            <!-- <div class="form-group col-md-6">
                <label for="concession"><i class="zmdi zmdi-money-off pr-2"></i>Concession</label>
                <input type="number" name="concession" id="concession" class="form-control" placeholder="Enter Concession Amount..." />
            </div> -->

            <div class="form-group col-md-6">
                <label for="fee_month"><i class="zmdi zmdi-time pr-2"></i>Fee Month</label>
                <input type="date" name="fee_month" id="fee_month" class="form-control" required="required" />
            </div>

            <!-- <div class="form-group col-md-6">
                <label for="submitted_date"><i class="zmdi zmdi-time pr-2"></i>Submitted Date</label>
                <input type="date" name="submitted_date" id="submitted_date" class="form-control" required="required" />
            </div> -->

            <!-- <div class="form-group col-md-6">
                <label for="remark"><i class="zmdi zmdi-account material-icons-name pr-2"> Remark</i></label>
                <input type="text" name="remark" id="remark" class="form-control" placeholder="Enter Remark..."/>
            </div> -->
            
          </div>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary save-btn">Add</button>
      </div>
      </form>
    </div>
  </div>
</div>

@endsection

@section('javascript')
<script type="text/javascript">
$(document).ready(function(){
        $(document).on('click', '.check-all1', function () {
        if(this.checked == true){
        $('.check').prop('checked', true);
        $('.check').parents('tr').addClass('selected');
        var cb_length = $( ".check:checked" ).length;
        if(cb_length > 0){
          $('.selected-item').removeClass('d-none');
        }
      }else{
        $('.check').prop('checked', false);
        $('.check').parents('tr').removeClass('selected');
        $('.selected-item').addClass('d-none');
        
      }
    });

   $(document).on('click', '.check', function () {
    // $(this).removeClass('d-none');
        var cb_length = $( ".check:checked" ).length;
        var st_pieces = $(this).parents('tr').attr('data-pieces');
        if(this.checked == true){
        $('.selected-item').removeClass('d-none');
        $(this).parents('tr').addClass('selected');
      }else{
        $(this).parents('tr').removeClass('selected');
        if(cb_length == 0){
         $('.selected-item').addClass('d-none');
        }
        
      }
    });

    $(document).on('click', '.download_invoice', function(){
    var selected_fees = [];
    $("input.check:checked").each(function() {
      selected_fees.push($(this).val());
    });

    if(selected_fees == ''){
            toastr.error('Error!', 'Select Students First.',{"positionClass": "toast-bottom-right"});
        return false;
        }else{
        $('.fees_selected').val(selected_fees);

        var fees = $('.fees_selected').val();

        var url = "{{url('user/fee-invoice-print-advance')}}"+"/"+fees+"/"+$('#from_date').val();
                    window.open(url, 'Orders Receivable Print', 'width=1200,height=600,scrollbars=yes');
                  $('#loader_modal').modal('hide');
                }
    });

    $(document).on('click', '.download_invoice1', function(){
    var selected_fees = [];
    $("input.check:checked").each(function() {
      selected_fees.push($(this).val());
    });

    if(selected_fees == ''){
            toastr.error('Error!', 'Select Students First.',{"positionClass": "toast-bottom-right"});
        return false;
        }else{
        $('.fees_selected').val(selected_fees);

        var fees = $('.fees_selected').val();

        var url = "{{url('user/fee-invoice-print-advance-all')}}"+"/"+fees+"/"+$('#from_date').val();
                    window.open(url, 'Orders Receivable Print', 'width=1200,height=600,scrollbars=yes');
                  $('#loader_modal').modal('hide');
                }
    });

        $(document).on('click', '.download_invoice2', function(){
          var m_date = $('#fee_month_date').val();
          var class_id = $('.student_classes_select').val();
          var selected_fees = [];
          $("input.check:checked").each(function() {
            selected_fees.push($(this).val());
          });

          if(selected_fees == ''){
                  toastr.error('Error!', 'Select Students First.',{"positionClass": "toast-bottom-right"});
                  return false;
              }
          if(m_date == '')
              {
                  toastr.info('Info!', 'Please Select Fee Month First!!!',{"positionClass": "toast-bottom-right"});
                  return false;
              }
              if(true){
              $('.fees_selected').val(selected_fees);

              var fees = $('.fees_selected').val();

        swal({
            title: "Are You Sure!",
            html: true,
            text: "<b>You want to make fee entry!!!</b>",
            type: "info",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes!",
            cancelButtonText: "No!",
            closeOnConfirm: true,
            closeOnCancel: false
          },
            function(isConfirm) {
            if (isConfirm){
             $.ajaxSetup({
              headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
              }
            });
            $.ajax({
              url: "{{ route('make-fee-entry') }}",
              method: 'post',
              data: {std_ids:fees,m_date:m_date,class_id:class_id},
              beforeSend: function(){
                $('#loader_modal').modal({
                  backdrop: 'static',
                  keyboard: false
                });
                $('#loader_modal').modal('show');
              },
              success: function (response) {
                if(response.success == true)
                {
                  toastr.success('Success!', 'Fee Entry Submitted Successfully!!!',{"positionClass": "toast-bottom-right"});
                  $('.table-students-fee').DataTable().ajax.reload();
                  $('.table-fee-renew').DataTable().ajax.reload();
                  $('#loader_modal').modal('hide');
                }
               
              },
              error: function(request, status, error){
                $('#loader_modal').modal('hide');
              }
            });
            }
            else{
              swal("Cancelled", "", "error");
              
            }
          });

              
              }
          });
  var table2 =  $('.table-students-fee').DataTable({
    processing: true,
    searching: true,
    "language": {
    processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw" style="color:#13436c;"></i><span class="sr-only">Loading...</span> '},
    ordering: false,
    pageLength: {{50}},
    serverSide: true,
    autoWidth: false,
    "lengthMenu": [50,100,150,200],
    
    scrollX:true,
    scrollY : '90vh',
    scrollCollapse: true,
    ajax: 
    {
      beforeSend: function(){
        $('#loader_modal').modal({
          backdrop: 'static',
          keyboard: false
        });
        $('#loader_modal').modal('show');
      },
      url: "{!! route('get-students-unsubmitted-fee') !!}",
       data: function(data) { data.student_classes_select = $('.student_classes_select option:selected').val(),data.from_date = $('#from_date').val(), data.to_date = $('#to_date').val() } ,
    },
    columns: [
        { data: 'checkbox', name: 'checkbox' }, 
        { data: 'id', name: 'id' }, 
        { data: 'name', name: 'name', class: 'text-capitalize' }, 
        { data: 'class', name: 'class' }, 
        { data: 'roll_no', name: 'roll_no' }, 
        { data: 'discount', name: 'discount' }, 
        { data: 'gender', name: 'gender', class: 'text-capitalize'  }, 
        { data: 'guardian', name: 'guardian', class: 'text-capitalize'  },  
        { data: 'fee', name: 'fee' }, 
        { data: 'action', name: 'action' }, 
    ],
    initComplete: function () {
    // Enable THEAD scroll bars
    $('.dataTables_scrollHead').css('overflow', 'auto');
    // Sync THEAD scrolling with TBODY
      $('.dataTables_scrollHead').on('scroll', function () {
          $('.dataTables_scrollBody').scrollLeft($(this).scrollLeft());
      });
    },
    drawCallback: function(){
      $('#loader_modal').modal('hide');
    },
  });

    table2.on( 'draw.dt', function () {
    var PageInfo = $('.table-students-fee').DataTable().page.info();
         table2.column(1, { page: 'current' }).nodes().each( function (cell, i) {
            cell.innerHTML = i + 1 + PageInfo.start;
        } );
    } );

    var table3 =  $('.table-fee-renew').DataTable({
    processing: true,
    searching: true,
    "language": {
    processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw" style="color:#13436c;"></i><span class="sr-only">Loading...</span> '},
    ordering: false,
    pageLength: {{50}},
    serverSide: true,
    autoWidth: false,
    "lengthMenu": [50,100,150,200],
    
    scrollX:true,
    scrollY : '90vh',
    scrollCollapse: true,
    ajax: 
    {
      beforeSend: function(){
        $('#loader_modal').modal({
          backdrop: 'static',
          keyboard: false
        });
        $('#loader_modal').modal('show');
      },
      url: "{!! route('get-fee-renew-history') !!}",
       data: function(data) { } ,
    },
    columns: [ 
        { data: 'id', name: 'id' }, 
        { data: 'class_name', name: 'class_name' }, 
        { data: 'fee_month', name: 'fee_month' }, 
        { data: 'fee_type', name: 'fee_type' }, 
        { data: 'created_by', name: 'created_by', class: 'text-capitalize'  }, 
        { data: 'created_at', name: 'created_at' }, 
    ],
    initComplete: function () {
    // Enable THEAD scroll bars
    $('.dataTables_scrollHead').css('overflow', 'auto');
    // Sync THEAD scrolling with TBODY
      $('.dataTables_scrollHead').on('scroll', function () {
          $('.dataTables_scrollBody').scrollLeft($(this).scrollLeft());
      });
    },
    drawCallback: function(){
      $('#loader_modal').modal('hide');
    },
  });

    table3.on( 'draw.dt', function () {
    var PageInfo = $('.table-fee-renew').DataTable().page.info();
         table2.column(0, { page: 'current' }).nodes().each( function (cell, i) {
            cell.innerHTML = i + 1 + PageInfo.start;
        } );
    } );
      $(document).on('change','.student_classes_select',function(){
    $('.table-students-fee').DataTable().ajax.reload();
    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
  });

   $(document).on('change','#from_date',function(){
    $('.table-students-fee').DataTable().ajax.reload();
    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
  });

   $(document).on('change','#to_date',function(){
    $('.table-students-fee').DataTable().ajax.reload();
    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
  });


  $(document).on('click','#reset',function(){
    $('.student_classes_select').val('');
    $('#from_date').val('');
    $('#to_date').val('');
    $('.table-students-fee').DataTable().ajax.reload();
    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
  });

  $('#submit-other-fee-form').on('submit',function(e){
    e.preventDefault();
    var class_id = $('.student_classes_select').val();
    $('.other_fee_class_id').val(class_id);
    var selected_fees = [];
    $("input.check:checked").each(function() {
      selected_fees.push($(this).val());
    });

    $('.other_fee_std_ids').val(selected_fees);

    if(selected_fees == '')
    {
      toastr.error('Error!', 'Select Students First.',{"positionClass": "toast-bottom-right"});
                  return false;
    }

      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
          }
      });
       $.ajax({
          url: "{{ route('make-other-fee-entry') }}",
          dataType: 'json',
          type: 'post',
          data: new FormData(this),
          contentType: false,
          cache: false,
          processData:false,
          beforeSend: function(){
            $('.save-btn').addClass('disabled');
            $('.save-btn').attr('disabled', true);
            $('#loader_modal').modal({
              backdrop: 'static',
              keyboard: false
            });
            $('#loader_modal').modal('show');
          },
          success: function(result){
            $('.save-btn').attr('disabled', true);
            $('.save-btn').removeAttr('disabled');
            if(result.success == true){
              toastr.success('Success!', 'Fee Added Successfully!!!',{"positionClass": "toast-bottom-right"});
              $('.table-fee-type').DataTable().ajax.reload();
              $('#submit-other-fee-form')[0].reset();
              $('#sumbitOtherFee').modal('hide');
              $('#loader_modal').modal('hide');
            }else{
              toastr.error('Error!', result.errormsg,{"positionClass": "toast-bottom-right"});
              $('#loader_modal').modal('hide');
            }

          },
          error: function (request, status, error) {
                /*$('.form-control').removeClass('is-invalid');
                $('.form-control').next().remove();*/
                $('#loader_modal').modal('hide');
                $('.save-btn').removeClass('disabled');
                $('.save-btn').removeAttr('disabled');
                json = $.parseJSON(request.responseText);
                $.each(json.errors, function(key, value){
                      $('input[name="'+key+'"]').after('<span class="invalid-feedback" role="alert"><strong>'+value+'</strong>');
                     $('input[name="'+key+'"]').addClass('is-invalid');
                     $('textarea[name="'+key+'"]').after('<span class="invalid-feedback" role="alert"><strong>'+value+'</strong>');
                     $('textarea[name="'+key+'"]').addClass('is-invalid');


                });
            }
        });
  });
});
</script>
@stop

