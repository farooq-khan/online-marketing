@extends('user.layouts.layout')

@section('title','Online Marketing')

@section('content')
<div class="bg-white">
<form id="add-student-fee-form" method="POST" enctype="multipart/form-data">
      <div class="modal-body">
          <div class="form-row row-cols-md-3 row-cols-2">
             <div class="form-group col">
                <label for="roll_no">Admission No.</label>
                <select class="form-control-lg" id="student_admission_no" name="student_admission_no" style="display: block !important;width: 100%;height: 100px;padding: 10px !important;">
                  <option value="">--Select Student Admission No.--</option>
                  @foreach($students as $std)
                  <option value="{{$std->id}}"> {{$std->roll_no}} ({{$std->name}}) (class {{$std->student_class->class_name}}) </option>
                  @endforeach
                </select>
            </div>
            <div class="form-group col">
                <label for="name">Student Name</label>
                <input type="text" name="name" id="name" class="form-control" placeholder="Student Name" disabled="true" />
            </div>
              <div class="form-group col">
                <label for="guardian">Father Name</label>
                <input type="text" name="guardian" id="guardian" class="form-control" placeholder="Student Father Name" disabled="true" />
            </div>

            <div class="form-group col">
                <label for="std_class">Class</label>
                <input type="text" name="std_class" id="std_class" class="form-control" placeholder="Student Class" disabled="true" />
              </div>
              <div class="form-group col">
                <label for="fee">Fee</label>
                <input type="text" name="fee" id="fee" class="form-control" placeholder="0.00" disabled="disabled" />
            </div>

              <div class="form-group col">
                <label for="discount">Fee Discount</label>
                <input type="number" name="discount" id="discount" min="0" max="100" class="form-control" placeholder="Fee Discount" disabled="true" />
            </div>

            <div class="form-group col">
                <label for="paid_amount">Enter Amount To Pay</label>
                <input type="text" name="paid_amount" id="paid_amount" class="form-control" placeholder="0.00" required="required" />
            </div>

            <div class="form-group col">
                <label for="remaining_amount">Remaining Amount</label>
                <input type="text" name="remaining_amount" id="remaining_amount" class="form-control" placeholder="0.00" disabled="true" />
            </div>

            <div class="form-group col">
                <label for="concession">Concession</label>
                <input type="number" name="concession" id="concession" class="form-control" placeholder="0.00" />
            </div>

            <div class="form-group col">
                <label for="concession_reason">Concession Reason</label>
                <input type="text" name="concession_reason" id="concession_reason" class="form-control" placeholder="Reason" />
            </div>

            <div class="form-group col">
                <label for="submitted_date">Submitted Date</label>
                <input type="date" name="submitted_date" id="submitted_date" class="form-control" required="required" />
            </div>

            <div class="form-group col">
                <label for="fee_month">Fee Month</label>
                <input type="date" name="fee_month" id="fee_month" class="form-control" required="required" />
            </div>

            <div class="form-group col">
                <label for="remark">Remark</label>
                <input type="text" name="remark" id="remark" class="form-control" placeholder="Enter Remark..." />
            </div>
            <div class="form-group col">
                <label for="receipt">Receipt No.</label>
                <input type="text" name="receipt" id="receipt" class="form-control" placeholder="Enter Receipt No..." />
            </div>

            </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
      </form>
</div>
@endsection

@section('javascript')
<script type="text/javascript">
$(document).ready(function(){
  $('#student_admission_no').select2();
  $(document).on('change','#student_admission_no',function(){
    // alert($(this).val());
    var id = $(this).val();

    $.ajaxSetup({
              headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
              }
            });
            $.ajax({
              url: "{{ route('fetch-student-detail') }}",
              method: 'post',
              data: {id:id},
              beforeSend: function(){
                $('#loader_modal').modal({
                  backdrop: 'static',
                  keyboard: false
                });
                $('#loader_modal').modal('show');
              },
              success: function (response) {
                
                if(response.success == true)
                {
                  // alert(response.fee);
                $('#loader_modal').modal('hide');
                // console.log(response.student);
                var fee_discount = response.student.fee;
                  $('#name').val(response.student.name);
                  $('#guardian').val(response.student.guardian);
                  // $('#guardian_cnic').val(response.student.guardian_cnic);
                  // $('#guardian_cnic').val(response.student.guardian_cnic);
                  $('#std_class').val(response.student.student_class.class_name);
                  $('#fee').val(fee_discount);
                  $('#paid_amount').attr('min',0);
                  $('#paid_amount').attr('max',fee_discount);
                  $('#discount').val(response.student.discount);

                }
               
              },
              error: function(request, status, error){
                $('#loader_modal').modal('hide');
              }
            });
  });

  $('#add-student-fee-form').on('submit',function(e){
    e.preventDefault();

      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
          }
      });
       $.ajax({
          url: "{{ route('add-student-fee') }}",
          dataType: 'json',
          type: 'post',
          data: new FormData(this),
          contentType: false,
          cache: false,
          processData:false,
          beforeSend: function(){
            $('.save-btn').addClass('disabled');
            $('.save-btn').attr('disabled', true);
            $('#loader_modal').modal({
              backdrop: 'static',
              keyboard: false
            });
            $('#loader_modal').modal('show');
          },
          success: function(result){
            $('.save-btn').attr('disabled', true);
            $('.save-btn').removeAttr('disabled');
            if(result.success == true){
              toastr.success('Success!', 'Student Fee Added Successfully!!!',{"positionClass": "toast-bottom-right"});
              $('.table-students-fee').DataTable().ajax.reload();
              /*setTimeout(function(){
                window.location.reload();
              }, 2000);*/

              $('#add-student-fee-form')[0].reset();
              $('#addStudentFee').modal('hide');
            }
            else if(result.paid == true)
            {
               toastr.warning('Warning!', 'This month fee already submitted!!!',{"positionClass": "toast-bottom-right"});
              $('.table-students-fee').DataTable().ajax.reload();
              $('#add-student-fee-form')[0].reset();
              $('#addStudentFee').modal('hide');
            }
            else if(result.total_amount == true)
            {
                toastr.info('Sorry!', 'Paid Amount Cannot Be Greater Than Total Amount!!!',{"positionClass": "toast-bottom-right"});
                $('#loader_modal').modal('hide');
            }
            else{
              toastr.error('Error!', result.errormsg,{"positionClass": "toast-bottom-right"});
              $('#loader_modal').modal('hide');
            }

          },
          error: function (request, status, error) {
                /*$('.form-control').removeClass('is-invalid');
                $('.form-control').next().remove();*/
                $('#loader_modal').modal('hide');
                $('.save-btn').removeClass('disabled');
                $('.save-btn').removeAttr('disabled');
                json = $.parseJSON(request.responseText);
                $.each(json.errors, function(key, value){
                      $('input[name="'+key+'"]').after('<span class="invalid-feedback" role="alert"><strong>'+value+'</strong>');
                     $('input[name="'+key+'"]').addClass('is-invalid');
                     $('textarea[name="'+key+'"]').after('<span class="invalid-feedback" role="alert"><strong>'+value+'</strong>');
                     $('textarea[name="'+key+'"]').addClass('is-invalid');


                });
            }
        });
  });
});
</script>
@stop

