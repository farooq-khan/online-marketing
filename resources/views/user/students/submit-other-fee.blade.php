@extends('user.layouts.layout')

@section('title','Submit Other Fee')

@section('content')
<style type="text/css">
   
</style>


                    <div class="row page-title-row">
                            <div class="col-md col-sm col-12 page-title-col align-self-center">
                                <h1 class="page-title">Other Fee</h1>
                            </div>
                            <div class="col-sm col-12 mt-sm-0 mt-3 ml-auto text-right page-action-button">
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#sumbitOtherFee">+Submit Fee</button>
                            </div>
                            
                    </div>

                    <div class="card card-border-top mb-4">
                      <div class="card-header">
                            <h6 class="mb-0">Filters</h6>
                      </div>
                      <div class="card-body pb-2">
                        <div class="row mb-0 form-row row-cols-xl-5 row-cols-md-3 row-cols-sm-2 row-cols-2">
                          <div class="col form-group">
                            <label for="">Classes</label>
                            <select class="form-control form-control student_classes_select state-tags" name="warehouse_id" >
                              <option value="">All Classes</option>
                               @foreach($classes as $class)
                                <option value="{{$class->id}}"> {{$class->class_name}} </option>
                                @endforeach
                            </select>
                          </div>

                           <div class="col form-group">
                             <label for="">Fee Type</label>
                            <select class="form-control" id="fee_type_select" name="fee_type_id" style="" required="true">
                              <option value="">Select Fee Type</option>
                              @foreach($fee_types as $type)
                              <option value="{{$type->id}}"> {{$type->title}} </option>
                              @endforeach
                          </select>
                          </div>

                          <div class="form-group col">
                            <label for="">Student</label>
                              <select class="form-control" id="student_filter" name="student_filter" required="true" style="display: block !important;width: 100%;height: 100px;padding: 10px !important;">
                                <option value="">Select Student</option>
                                @foreach($students as $std)
                                <option value="{{$std->id}}" {{$page == $std->id ? 'selected' : ''}}> {{$std->name}} </option>
                                @endforeach
                              </select>
                          </div>
                          <div class="col form-group">
                            <label for="">Status</label>
                            <select class="form-control form-control status_student state-tags" name="status" >
                              <option value="">Select status</option>
                                <option value="1">Paid</option>
                                <option value="0">Partial Paid</option>
                            </select>
                          </div>

                          <div class="col form-group">
                            <label for="">Receipt</label>
                              <input type="text" name="receipt_filter" id="receipt_filter" class="form-control" placeholder="Receipt i.e 10-100" />
                          </div>
                    
                        
                           <div class="col form-group">
                            <label for="from_date">Fee Month From Date</label>
                              <input type="date" name="from_date" id="from_date" class="form-control" placeholder="Search by year ..." />
                          </div>

                          <div class="col form-group">
                            <label for="to_date">Fee Month To Date</label>
                              <input type="date" name="to_date" id="to_date" class="form-control" placeholder="Search by year ..." />
                          </div>

                          <div class="col form-group">
                            <label for="from_date">Submitted From Date</label>
                              <input type="date" name="sub_from_date" id="sub_from_date" class="form-control" placeholder="Search by year ..." />
                          </div>

                          <div class="col form-group">
                            <label for="to_date">Submitted To Date</label>
                              <input type="date" name="sub_to_date" id="sub_to_date" class="form-control" placeholder="Search by year ..." />
                          </div>

                           <div class="col form-group">
                            <label for="father_name">Father Name</label>
                              <input type="text" name="father_name" id="father_name" class="form-control" placeholder="Search by Father ..." />
                          </div>

                          <div class="col form-group">
                            <label for="student_search">Student Name</label>
                              <input type="text" name="student_search" id="student_search" class="form-control" placeholder="Search by Student ..." />
                          </div>
                          <div class="col-xl col-12 ml-auto text-right mb-3 align-self-end">
                              <button class="btn btn-outline-primary px-3" type="reset" id="reset">Reset</button>
                              <button class="btn btn-primary px-3 search-btn ml-2" type="button" id="search_btn">Search</button>  
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row pl-3 mb-2" style="font-size: 18px;">
                      <span style="color: red;font-style: italic;">Note : </span> <span style="font-style: italic;padding-left: 5px;"> Double Click The Record For Updating</span>
                    </div>

                    
                    <div class="card">
                        <div class="card-body">
                          <div class="delete-selected-item mb-4 d-none">
                            <a href="javascript:void(0);" class="btn selected-item-btn btn-sm delete_class deleteIcon" data-type="quotation" title="Delete Class" >
                            <i class="zmdi zmdi-delete" style="font-size: 25px;"></i></a>
                          </div>
                        <table class="table table-striped nowrap table-fee-type">
                        <thead>
                        <tr>
                            <th>S.No</th>
                            <th>Admission No.</th>
                            <th>Name</th>
                            <th>Father Name</th>
                            <th>Class</th>
                            <th>Status</th>
                            <th>Fee Type</th>
                            <th>Total Amount</th>
                            <th>Concession</th>
                            <th>Paid Amount</th>
                            <th>Unpaid Amount</th>
                            <th>Fee Month</th>
                            <th>Submitted Date</th>
                            <th>Receipt No</th>
                            <th>Remark</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tfoot>
                          <tr>
                            <th colspan="7" align="right">Total Amount</th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                          </tr>
                        </tfoot>
                        </table>
                        </div>
                    </div>



<!--Add Student Modal -->
<div class="modal fade" id="sumbitOtherFee" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Submit Other Fee</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form id="submit-other-fee-form" method="POST">
        @csrf
      <div class="modal-body ">

        <div class="form-row row-cols-lg-3 row-cols-sm-2 row-cols-2">
          <div class="form-group col">
                <label for="roll_no">Admission No.</i></label>
                <select class="form-control-lg" id="student_admission_no" name="student_admission_no" required="true" style="display: block !important;width: 100%;height: 100px;padding: 10px !important;">
                  <option value="">--Select Student Admission No.--</option>
                  @foreach($students as $std)
                  <option value="{{$std->id}}"> {{$std->roll_no}} ({{$std->name}}) (class {{$std->student_class->class_name}}) </option>
                  @endforeach
                </select>
            </div>
            <div class="form-group col">
                <label for="name">Student Name</i></label>
                <input type="text" name="name" id="name" class="form-control" placeholder="Student Name" disabled="true" />
            </div>

              <div class="form-group col">
                <label for="guardian">Father Name</label>
                <input type="text" name="guardian" id="guardian" class="form-control" placeholder="Student Father Name" disabled="true" />
            </div>

            <div class="form-group col">
                <label for="std_class">Class</label>
                <input type="text" name="std_class" id="std_class" class="form-control" placeholder="Student Class" disabled="true" />
              </div>

            <div class="form-group col">
                <label for="fee_type">Fee Type</i></label>
                <select class="form-control" id="fee_type" name="fee_type" style="" required="true">
                  <option value="">--Select Fee Type--</option>
                  @foreach($fee_types as $type)
                  <option value="{{$type->id}}"> {{$type->title}} </option>
                  @endforeach
                </select>
            </div>

            <div class="form-group col">
                <label for="amount">Total Amount</label>
                <input type="number" name="amount" id="amount" class="form-control" placeholder="Enter Total Amount..." required="true" />
            </div>

            <div class="form-group col">
                <label for="paid_amount">Paid Amount</label>
                <input type="number" name="paid_amount" id="paid_amount" class="form-control" placeholder="Enter Paid Amount..." required="true" />
            </div>

            <div class="form-group col">
                <label for="concession">Concession</label>
                <input type="number" name="concession" id="concession" class="form-control" placeholder="Enter Concession Amount..." />
            </div>

            <div class="form-group col">
                <label for="fee_month">Fee Month</label>
                <input type="date" name="fee_month" id="fee_month" class="form-control" required="required" />
            </div>

            <div class="form-group col">
                <label for="submitted_date">Submitted Date</label>
                <input type="date" name="submitted_date" id="submitted_date" class="form-control" required="required" />
            </div>

            <div class="form-group col">
                <label for="receipt">Receipt No.</i></label>
                <input type="text" name="receipt" id="receipt" class="form-control" placeholder="Enter Receipt No..."/>
            </div>

            <div class="form-group col">
                <label for="remark">Remark</i></label>
                <input type="text" name="remark" id="remark" class="form-control" placeholder="Enter Remark..."/>
            </div>
            
          </div>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary save-btn">Submit</button>
      </div>
      </form>
    </div>
  </div>
</div>
@endsection

@section('javascript')
<script type="text/javascript">
$(document).ready(function(){
    $(document).on("dblclick",".inputDoubleClick",function(){
    $(this).addClass('d-none');
    $(this).next().removeClass('d-none');
    $(this).next().addClass('active');
    $(this).next().focus();
  });

    $(document).on('keypress keyup focusout',".fieldFocus",function(e) {
    // alert('hi');
    if (e.keyCode === 27 && $(this).hasClass('active')) {
      var fieldvalue = $(this).prev().data('fieldvalue');
      var thisPointer = $(this);
      thisPointer.addClass('d-none');
      thisPointer.val(fieldvalue);
      thisPointer.removeClass('active');
      thisPointer.prev().removeClass('d-none');
    }

    var name = $(this).attr('name');
    // alert(name);

    if( (e.keyCode === 13 || e.which === 0) && $(this).val().length > 0 && $(this).hasClass('active')){
      var str = $(this).val();
      // if(($(this).val().length < 2 ||  !str.replace(/\s/g, '').length))
      // {
      //   swal({ html:true, title:'Alert !!!', text:'<b>Please Enter At Least 2 Characters (this string may only contain white spaces)!!!</b>'});
      //   return false;
      // }
      if(true)
      {
        $(this).removeClass('active');
        var fieldvalue = $(this).prev().data('fieldvalue');
        var new_value = $(this).val();
        // alert(new_value);
        if(fieldvalue == new_value)
        {
          var thisPointer = $(this);
          thisPointer.addClass('d-none');
          thisPointer.prev().removeClass('d-none');
          $(this).prev().html(fieldvalue);
        }
        else
        {
          var thisPointer = $(this);
          thisPointer.addClass('d-none');
          thisPointer.prev().removeClass('d-none');
          if(new_value != '')
          {
            $(this).prev().html(new_value);
          }
          $(this).prev().data('fieldvalue', new_value);
          var id = $(this).data('id');
          saveStudentData(thisPointer,thisPointer.attr('name'), thisPointer.val(),fieldvalue,id);
        }
      }
    }

  });

      function saveStudentData(thisPointer,field_name,field_value,new_select_value,id){
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
      });
      $.ajax({
        method: "post",

        url: "{{ route('update-other-fee-data') }}",
        dataType: 'json',
        data: 'id='+id+'&'+field_name+'='+encodeURIComponent(field_value)+'&'+'new_select_value'+'='+encodeURIComponent(new_select_value),
        beforeSend: function(){
          $('#loader_modal').modal({
            backdrop: 'static',
            keyboard: false
          });
          $('#loader_modal').modal('show');
        },
        success: function(data)
        {
          $("#loader_modal").modal('hide');
          if(data.success == true)
          {
            
            toastr.success('Success!', 'Information updated successfully.',{"positionClass": "toast-bottom-right"});
            $('.table-fee-type').DataTable().ajax.reload();

            return true;
          }

        },

      });
    }

  $('#student_admission_no').select2();
  $('#student_filter').select2();

  $('#father_name, #receipt_filter').keyup(function(event){
    var query = $(this).val();

    if(event.keyCode == 13)
    {
      if(query.length > 0)
      {

        // alert(query);
        $('.table-fee-type').DataTable().ajax.reload();
      }
      else if(query.length == 0)
      {
        $('.table-fee-type').DataTable().ajax.reload();
      }
      else
      {
        // $('#input_keyword_std').empty();
        toastr.error('Error!', 'Please enter atlesat 1 characters then press Enter !!!' ,{"positionClass": "toast-bottom-right"});
      }
    }
  });

  $('#student_search').keyup(function(event){
    var query = $(this).val();

    if(event.keyCode == 13)
    {
      if(query.length > 2)
      {

        // alert(query);
        $('.table-fee-type').DataTable().ajax.reload();
      }
      else if(query.length == 0)
      {
        $('.table-fee-type').DataTable().ajax.reload();
      }
      else
      {
        // $('#input_keyword_std').empty();
        toastr.error('Error!', 'Please enter atlesat 3 characters then press Enter !!!' ,{"positionClass": "toast-bottom-right"});
      }
    }
  });

  var table2 =  $('.table-fee-type').DataTable({
    processing: true,
    searching: true,
    "language": {
    processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw" style="color:#13436c;"></i><span class="sr-only">Loading...</span> '},
    ordering: false,
    pageLength: {{50}},
    serverSide: true,
    autoWidth: false,
    "lengthMenu": [50,100,150,200],
    
    scrollX:true,
    scrollY : '90vh',
    scrollCollapse: true,
    ajax: 
    {
      beforeSend: function(){
        $('#loader_modal').modal({
          backdrop: 'static',
          keyboard: false
        });
        $('#loader_modal').modal('show');
      },
      url: "{!! route('get-other-fee') !!}",
      data: function(data) { data.student_classes_select = $('.student_classes_select option:selected').val(),data.fee_type_select = $('#fee_type_select option:selected').val(), data.student_id = $('#student_filter option:selected').val(),data.status_student = $('.status_student option:selected').val(),data.from_date = $('#from_date').val(), data.to_date = $('#to_date').val(),data.sub_from_date = $('#sub_from_date').val(), data.sub_to_date = $('#sub_to_date').val(), data.father_name = $('#father_name').val(), data.student_search = $('#student_search').val(), data.receipt = $('#receipt_filter').val() } ,
    },
    columns: [
        { data: 'empty_col', name: 'empty_col' },  
        { data: 'roll_no', name: 'roll_no' },  
        { data: 'name', name: 'name' }, 
        { data: 'father_name', name: 'father_name' }, 
        { data: 'class', name: 'class' }, 
        { data: 'status', name: 'status' }, 
        { data: 'fee_type', name: 'fee_type' }, 
        { data: 'amount', name: 'amount' }, 
        { data: 'concession', name: 'concession' }, 
        { data: 'paid_amount', name: 'paid_amount' }, 
        { data: 'unpaid_amount', name: 'unpaid_amount' }, 
        { data: 'fee_month', name: 'fee_month' }, 
        { data: 'submitted_date', name: 'submitted_date' }, 
        { data: 'receipt', name: 'receipt' }, 
        { data: 'remark', name: 'remark' }, 
        { data: 'action', name: 'action' },  
    ],
    initComplete: function () {
    // Enable THEAD scroll bars
    $('.dataTables_scrollHead').css('overflow', 'auto');
    // Sync THEAD scrolling with TBODY
      $('.dataTables_scrollHead').on('scroll', function () {
          $('.dataTables_scrollBody').scrollLeft($(this).scrollLeft());
      });
    },
    drawCallback: function(){
      $('#loader_modal').modal('hide');
    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
      
    },
    footerCallback: function ( row, data, start, end, display ) {
        var api              = this.api();
        var json             = api.ajax.json();
        var total_amount     = json.total_amount;
        var concession     = json.concession;
        var paid_amount     = json.paid_amount;
        var unpaid_amount     = json.unpaid_amount;
        total_amount     = parseFloat(total_amount).toFixed(2);
        total_amount     = total_amount.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ",");

        concession     = parseFloat(concession).toFixed(2);
        concession     = concession.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ",");
        paid_amount     = parseFloat(paid_amount).toFixed(2);
        paid_amount     = paid_amount.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ",");
        unpaid_amount     = parseFloat(unpaid_amount).toFixed(2);
        unpaid_amount     = unpaid_amount.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ",");

        $( api.column( 7 ).footer() ).html(total_amount); 
        $( api.column( 8 ).footer() ).html(concession); 
        $( api.column( 9 ).footer() ).html(paid_amount); 
        $( api.column( 10 ).footer() ).html(unpaid_amount); 
      },
  });

    table2.on( 'draw.dt', function () {
    var PageInfo = $('.table-fee-type').DataTable().page.info();
         table2.column(0, { page: 'current' }).nodes().each( function (cell, i) {
            cell.innerHTML = i + 1 + PageInfo.start;
        } );
    } );

  $('.search-btn').on('click',function(){
    $('.table-fee-type').DataTable().ajax.reload();
  });

  $('#submit-other-fee-form').on('submit',function(e){
    e.preventDefault();

     e.preventDefault();
      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
          }
      });
       $.ajax({
          url: "{{ route('submit-other-fee-record') }}",
          dataType: 'json',
          type: 'post',
          data: new FormData(this),
          contentType: false,
          cache: false,
          processData:false,
          beforeSend: function(){
            $('.save-btn').addClass('disabled');
            $('.save-btn').attr('disabled', true);
            $('#loader_modal').modal({
              backdrop: 'static',
              keyboard: false
            });
            $('#loader_modal').modal('show');
          },
          success: function(result){
            $('.save-btn').attr('disabled', true);
            $('.save-btn').removeAttr('disabled');
            if(result.success == true){
              toastr.success('Success!', 'Fee Added Successfully!!!',{"positionClass": "toast-bottom-right"});
              $('.table-fee-type').DataTable().ajax.reload();
              $('#submit-other-fee-form')[0].reset();
              $('#sumbitOtherFee').modal('hide');
            }else{
              toastr.error('Error!', result.errormsg,{"positionClass": "toast-bottom-right"});
              $('#loader_modal').modal('hide');
            }

          },
          error: function (request, status, error) {
                /*$('.form-control').removeClass('is-invalid');
                $('.form-control').next().remove();*/
                $('#loader_modal').modal('hide');
                $('.save-btn').removeClass('disabled');
                $('.save-btn').removeAttr('disabled');
                json = $.parseJSON(request.responseText);
                $.each(json.errors, function(key, value){
                      $('input[name="'+key+'"]').after('<span class="invalid-feedback" role="alert"><strong>'+value+'</strong>');
                     $('input[name="'+key+'"]').addClass('is-invalid');
                     $('textarea[name="'+key+'"]').after('<span class="invalid-feedback" role="alert"><strong>'+value+'</strong>');
                     $('textarea[name="'+key+'"]').addClass('is-invalid');


                });
            }
        });
  });

    $(document).on('change','#student_admission_no',function(){
    // alert($(this).val());
    var id = $(this).val();

    $.ajaxSetup({
              headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
              }
            });
            $.ajax({
              url: "{{ route('fetch-student-detail') }}",
              method: 'post',
              data: {id:id},
              beforeSend: function(){
                $('#loader_modal').modal({
                  backdrop: 'static',
                  keyboard: false
                });
                $('#loader_modal').modal('show');
              },
              success: function (response) {
                
                if(response.success == true)
                {
                  // alert(response.fee);
                $('#loader_modal').modal('hide');
                // console.log(response.student);
                  $('#name').val(response.student.name);
                  $('#guardian').val(response.student.guardian);
                  // $('#guardian_cnic').val(response.student.guardian_cnic);
                  // $('#guardian_cnic').val(response.student.guardian_cnic);
                  $('#std_class').val(response.student.student_class.class_name);

                }
               
              },
              error: function(request, status, error){
                $('#loader_modal').modal('hide');
              }
            });
  });

      $(document).on('change','.student_classes_select',function(){
    $('.table-fee-type').DataTable().ajax.reload();
    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
  });

    $(document).on('change','#fee_type_select',function(){
    $('.table-fee-type').DataTable().ajax.reload();
    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
  });

     $(document).on('change','#student_filter',function(){
    $('.table-fee-type').DataTable().ajax.reload();
    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
  });
    $(document).on('change','.status_student',function(){
      $('.table-fee-type').DataTable().ajax.reload();
    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
  });

    $(document).on('change','#from_date',function(){
    $('.table-fee-type').DataTable().ajax.reload();
    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
  });

   $(document).on('change','#to_date',function(){
    $('.table-fee-type').DataTable().ajax.reload();
    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
  });

      $(document).on('change','#sub_from_date',function(){
    $('.table-fee-type').DataTable().ajax.reload();
    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
  });

   $(document).on('change','#sub_to_date',function(){
    $('.table-fee-type').DataTable().ajax.reload();
    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
  });

      $(document).on('click','#reset',function(){
    $('.student_classes_select').val('');
    $('#fee_type_select').val('');
    $('.status_student').val('');
    $('#student_filter').val(null).trigger('change');
    $('#from_date').val('');
    $('#sub_from_date').val('');
    $('#to_date').val('');
    $('#sub_to_date').val('');
    $('.table-fee-type').DataTable().ajax.reload();
    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
  });

    $(document).on('click','.edit_fee',function(e){
    e.preventDefault();
    var id = $(this).data('id');

    var unpaid = $('#unpaid_amount_edit_'+id).val();
    var new_paid = $('#new_payment_'+id).val();

    // if(new_paid > unpaid || new_paid < 0 || new_paid == 0)
    // {
    //   toastr.error('Sorry!', 'New payment must be less or equal to unpaid amount!!!',{"positionClass": "toast-bottom-right"});
    //   return false;
    // }
    // alert(id);
      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
          }
      });
       $.ajax({
          url: "{{ route('edit-student-other-fee') }}",
          // dataType: 'json',
          method: 'post',
          data: $('#edit-student-fee-form-'+id).serialize(),
          // contentType: false,
          // cache: false,
          // processData:false,
          beforeSend: function(){
            $('.save-btn').addClass('disabled');
            $('.save-btn').attr('disabled', true);
            $('#loader_modal').modal({
              backdrop: 'static',
              keyboard: false
            });
            $('#loader_modal').modal('show');
          },
          success: function(result){
            $('.save-btn').attr('disabled', true);
            $('.save-btn').removeAttr('disabled');
            if(result.success == true){
              toastr.success('Success!', 'Student Fee Updated Successfully!!!',{"positionClass": "toast-bottom-right"});
              $('.table-fee-type').DataTable().ajax.reload();
              /*setTimeout(function(){
                window.location.reload();
              }, 2000);*/

              // $('#edit-student-fee-form')[0].reset();
              $('#EditStudentFee'+id).modal('hide');
            }
            else{
              toastr.error('Error!', result.errormsg,{"positionClass": "toast-bottom-right"});
              $('#loader_modal').modal('hide');
            }

          },
          error: function (request, status, error) {
                /*$('.form-control').removeClass('is-invalid');
                $('.form-control').next().remove();*/
                $('#loader_modal').modal('hide');
                $('.save-btn').removeClass('disabled');
                $('.save-btn').removeAttr('disabled');
                json = $.parseJSON(request.responseText);
                $.each(json.errors, function(key, value){
                      $('input[name="'+key+'"]').after('<span class="invalid-feedback" role="alert"><strong>'+value+'</strong>');
                     $('input[name="'+key+'"]').addClass('is-invalid');
                     $('textarea[name="'+key+'"]').after('<span class="invalid-feedback" role="alert"><strong>'+value+'</strong>');
                     $('textarea[name="'+key+'"]').addClass('is-invalid');


                });
            }
        });
  });

      $(document).on('click','.delete_student_permanant',function(){
      var id = $(this).data('id');

        swal({
            title: "Are You Sure!",
            html: true,
            text: "<b>You want to delete this record !!!</b>",
            type: "info",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes!",
            cancelButtonText: "No!",
            closeOnConfirm: true,
            closeOnCancel: false
          },
            function(isConfirm) {
            if (isConfirm){
             $.ajaxSetup({
              headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
              }
            });
            $.ajax({
              url: "{{ route('delete-other-fee-permanent') }}",
              method: 'post',
              data: {id:id},
              beforeSend: function(){
                $('#loader_modal').modal({
                  backdrop: 'static',
                  keyboard: false
                });
                $('#loader_modal').modal('show');
              },
              success: function (response) {

                if(response.success == true)
                {
                  toastr.success('Success!', 'Record Deleted Successfully!!!',{"positionClass": "toast-bottom-right"});
                  $('.table-fee-type').DataTable().ajax.reload();
                  $('#loader_modal').modal('hide');
                }
               
              },
              error: function(request, status, error){
                $('#loader_modal').modal('hide');
              }
            });
            }
            else{
              swal("Cancelled", "", "error");
              
            }
          });
  });
});
</script>
@stop

