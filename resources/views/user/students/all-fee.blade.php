@extends('user.layouts.layout')

@section('title','Online Marketing')

@section('content')
<style type="text/css">
</style>


                    <div class="row page-title-row">
                        <div class="col-md col-sm col-12 page-title-col align-self-center">
                            <h1 class="page-title">Students Fee History</h1>
                        </div>
                        <div class="col-sm col-12 mt-sm-0 mt-3 ml-auto text-right page-action-button">
                          <button type="button" class="btn btn-outline-primary my-1 mr-1" data-toggle="modal" data-target="#import-modal">+Upload Data</button>
                            <button type="button" class="btn btn-primary my-1" data-toggle="modal" data-target="#addStudentFee">+Submit Fee</button>
                        </div> 
                    </div>
                    <div class="card card-border-top mb-4">
                      <div class="card-header">
                            <h6 class="mb-0">Filters</h6>
                      </div>
                      <div class="card-body pb-2">
                        <div class="row mb-0 form-row row-cols-xl-5 row-cols-md-3 row-cols-sm-2 row-cols-2">
                          <div class="col form-group">
                            <label for="to_date">Classes</label>
                            <select class="form-control form-control student_classes_select state-tags" name="warehouse_id" >
                              <option value="">All Classes</option>
                               @foreach($classes as $class)
                                <option value="{{$class->id}}"> {{$class->class_name}} </option>
                                @endforeach
                            </select>
                          </div>
                           <div class="col form-group">
                            <label for="to_date">Discount</label>
                            <select class="form-control form-control discount_student state-tags" name="warehouse_id" >
                              <option value="">Select Discount</option>
                                <option value="discount"> Discounted Students </option>
                                <option value="non-discount"> Non Discounted Students </option>
                            </select>
                          </div>
                           <div class="col form-group">
                            <label for="to_date">Status</label>
                            <select class="form-control form-control status_student state-tags" name="status" >
                              <option value="">Select status</option>
                                <option value="0" >Unpaid</option>
                                <option value="1">Paid</option>
                                <option value="2">Partial Paid</option>
                            </select>
                          </div>
                          <div class="col form-group">
                            <label for="receipt_filter">Receipt</label>
                            <input type="text" name="receipt" class="receipt_filter form-control" id="receipt_filter" placeholder="Receipt i.e 10-100">
                          </div>
                          <div class="col form-group">
                            <label for="from_date">Fee Month From Date</label>
                              <input type="date" name="from_date" id="from_date" class="form-control" placeholder="Search by year ..." />
                          </div>

                          <div class="col form-group">
                            <label for="to_date">Fee Month To Date</label>
                              <input type="date" name="to_date" id="to_date" class="form-control" placeholder="Search by year ..." />
                          </div>

                          <div class="col form-group">
                            <label for="from_date">Submitted From Date</label>
                              <input type="date" name="sub_from_date" id="sub_from_date" class="form-control" placeholder="Search by year ..." />
                          </div>

                          <div class="col form-group">
                            <label for="to_date">Submitted To Date</label>
                              <input type="date" name="sub_to_date" id="sub_to_date" class="form-control" placeholder="Search by year ..." />
                          </div>

                          <div class="col form-group">
                            <label for="father_name">Father Name</label>
                              <input type="text" name="father_name" id="father_name" class="form-control" placeholder="Father-Nick Name ..." />
                          </div>

                          <div class="col form-group">
                            <label for="student_search">Student</label>
                              <input type="text" name="student_search" id="student_search" class="form-control" placeholder="Search by Student ..." />
                          </div>
                          <div class="col-xl col-12 ml-auto text-right mb-3 align-self-end">
                              <button class="btn btn-outline-primary px-3 mt-1" type="reset" id="reset">Reset</button>
                              <button class="btn btn-primary search-btn px-3 mt-1 ml-md-2 ml-1" type="button" id="search_btn">Search</button>  
                          </div>
                        </div>
                      </div>
                </div>

                <div class="card">
                    <div class="card-body">
                        <div class="selected-item catalogue-btn-group mb-2 pb-1 d-none">
    
                            <!-- <a href="javascript:void(0)" class="actionicon viewIcon download_invoice btn btn-primary" data-id="1" title="Download Invoice"><i class="zmdi zmdi-download"></i></a> -->

                            <button class="btn btn-outline-primary mb-1 download_invoice" title="Download Invoice" data-id="1"><i class="fa fa-download"></i> Download Invoice</button>

                             <!-- <a href="javascript:void(0)" class="actionicon viewIcon download_invoice1 btn btn-primary" data-id="2" title="Download Invoice"><i class="fa fa-download"></i> Generate Print Student Wise</a> -->

                             <button class="btn btn-outline-primary mb-1 download_invoice1" title="Download Invoice" data-id="2"><i class="fa fa-download"></i> Generate Print Student Wise</button>

                             <button class="btn btn-outline-primary mb-1 download_invoice_large" title="Download Invoice" data-id="2"><i class="fa fa-download"></i> Generate Print Student Wise Large</button>

                             <!-- <a href="javascript:void(0)" class="actionicon viewIcon download_invoice2 btn btn-primary" data-id="2" title="Delete Record"><i class="fa fa-trash-alt"></i> Delete Record</a> -->

                             <button class="btn btn-outline-primary mb-1 download_invoice2" title="Delete Record" data-id="2"><i class="fa fa-trash-alt"></i> Delete Record</button>

                        </div>
                        <div class="alert alert-primary export-alert d-none"  role="alert">
                              <i class="  fa fa-spinner fa-spin"></i>
                                <b> PDF file is being prepared! Please wait.. </b>
                        </div>
                        <div class="alert alert-success export-alert-success d-none"  role="alert">
                            <i class=" fa fa-check "></i>

                            <b>PDF file is ready to download.
                            <a class="download__file" href="{{url('download-file')}}" target="_blank"><u>Click Here</u></a>
                            </b>
                        </div>
                        <div class="table-responsive">
                        <table class="table table-striped nowrap table-students-fee">
                        <thead>
                          <tr>
                              <th class="noVis">
                                <div class="custom-control custom-checkbox custom-checkbox1 d-inline-block">
                                  <input type="checkbox" class="custom-control-input check-all1" name="check_all" id="check-all">
                                  <label class="custom-control-label" for="check-all"></label>
                                </div>
                              </th>
                              <th>Name</th>
                              <th>Fee Month</th>
                              <th>Admission No.</th>
                              <th>Class</th>
                              <th>Status</th>
                              <th>Gender</th>
                              <th>Father/Guardian Name</th>
                              <th>Fee(Inc Discount)</th>
                              <th>Paid Amount</th>
                              <th>Concession</th>
                              <th>Reason</th>
                              <th>Unpaid Amount</th>
                              <th>Discount</th>
                              <th>Receipt</th>
                              <th>Submitted Date</th>
                              <th>Action</th>
                          </tr>
                        </thead>
                        <tfoot>
                          <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                          </tr>
                        </tfoot>
                        </table>
                    </div>
                  </div>
                </div>

<!--Add Student Modal -->
<div class="modal fade" id="addStudentFee" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Submit Fee</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form id="add-student-fee-form" method="POST" enctype="multipart/form-data">
      <div class="modal-body">
          <div class="form-row row-cols-md-3 row-cols-2">
             <div class="form-group col">
                <label for="roll_no">Admission No.</label>
                <select class="form-control-lg" id="student_admission_no" name="student_admission_no" style="display: block !important;width: 100%;height: 100px;padding: 10px !important;">
                  <option value="">--Select Student Admission No.--</option>
                  @foreach($students as $std)
                  <option value="{{$std->id}}"> {{$std->roll_no}} ({{$std->name}}) (class {{$std->student_class->class_name}}) </option>
                  @endforeach
                </select>
            </div>
            <div class="form-group col">
                <label for="name">Student Name</label>
                <input type="text" name="name" id="name" class="form-control" placeholder="Student Name" disabled="true" />
            </div>
              <div class="form-group col">
                <label for="guardian">Father Name</label>
                <input type="text" name="guardian" id="guardian" class="form-control" placeholder="Student Father Name" disabled="true" />
            </div>

            <div class="form-group col">
                <label for="std_class">Class</label>
                <input type="text" name="std_class" id="std_class" class="form-control" placeholder="Student Class" disabled="true" />
              </div>
              <div class="form-group col">
                <label for="fee">Fee</label>
                <input type="text" name="fee" id="fee" class="form-control" placeholder="0.00" disabled="disabled" />
            </div>

              <div class="form-group col">
                <label for="discount">Fee Discount</label>
                <input type="number" name="discount" id="discount" min="0" max="100" class="form-control" placeholder="Fee Discount" disabled="true" />
            </div>

            <div class="form-group col">
                <label for="paid_amount">Enter Amount To Pay</label>
                <input type="text" name="paid_amount" id="paid_amount" class="form-control" placeholder="0.00" required="required" />
            </div>

            <div class="form-group col">
                <label for="remaining_amount">Remaining Amount</label>
                <input type="text" name="remaining_amount" id="remaining_amount" class="form-control" placeholder="0.00" disabled="true" />
            </div>

            <div class="form-group col">
                <label for="concession">Concession</label>
                <input type="number" name="concession" id="concession" class="form-control" placeholder="0.00" />
            </div>

            <div class="form-group col">
                <label for="concession_reason">Concession Reason</label>
                <input type="text" name="concession_reason" id="concession_reason" class="form-control" placeholder="Reason" />
            </div>

            <div class="form-group col">
                <label for="submitted_date">Submitted Date</label>
                <input type="date" name="submitted_date" id="submitted_date" class="form-control" required="required" />
            </div>

            <div class="form-group col">
                <label for="fee_month">Fee Month</label>
                <input type="date" name="fee_month" id="fee_month" class="form-control" required="required" />
            </div>

            <div class="form-group col">
                <label for="remark">Remark</label>
                <input type="text" name="remark" id="remark" class="form-control" placeholder="Enter Remark..." />
            </div>
            <div class="form-group col">
                <label for="receipt">Receipt No.</label>
                <input type="text" name="receipt" id="receipt" class="form-control" placeholder="Enter Receipt No..." />
            </div>

            </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
      </form>
    </div>
  </div>

  <input type="hidden" name="fees_selected" class="fees_selected">
</div>

<!-- Modal for Import Items  -->
<div class="modal fade" id="import-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Upload Excel File</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <div class="modal-body">
        <div align="center">
          <label class="mt-2">
            <strong>Note : </strong><span class="text-danger"> Also Don't Upload Empty File.</span>
          </label>
          <form class="upload-excel-form" id="upload-excel-form" enctype="multipart/form-data">
            @csrf
            <input type="file" class="form-control" name="product_excel" id="product_excel" accept=".xls,.xlsx" required="" style="white-space: pre-wrap;"><br>
            <button class="btn btn-info product-upload-btn" type="submit">Upload</button>
          </form>
        </div>
      </div>

    </div>
  </div>
</div>

<!-- Modal -->
<div id="history_table" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Fee History</h4>
      </div>
      <div class="modal-body history_table_data">
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<form class="export-invoices-form" method="post">
    {{csrf_field()}}
    <input type="hidden" name="invoices_ids" class="invoices_ids" id="invoices_ids">
  </form>
@endsection

@section('javascript')
<script type="text/javascript">
$(document).ready(function(){

      $(document).on('click', '.check-all1', function () {
        if(this.checked == true){
        $('.check').prop('checked', true);
        $('.check').parents('tr').addClass('selected');
        var cb_length = $( ".check:checked" ).length;
        if(cb_length > 0){
          $('.selected-item').removeClass('d-none');
        }
      }else{
        $('.check').prop('checked', false);
        $('.check').parents('tr').removeClass('selected');
        $('.selected-item').addClass('d-none');
        
      }
    });

   $(document).on('click', '.check', function () {
    // $(this).removeClass('d-none');
        var cb_length = $( ".check:checked" ).length;
        var st_pieces = $(this).parents('tr').attr('data-pieces');
        if(this.checked == true){
        $('.selected-item').removeClass('d-none');
        $(this).parents('tr').addClass('selected');
      }else{
        $(this).parents('tr').removeClass('selected');
        if(cb_length == 0){
         $('.selected-item').addClass('d-none');
        }
        
      }
    });

  $('.js-example-basic-single').select2();
  $('#student_admission_no').select2();

  $('#father_name').keyup(function(event){
    var query = $(this).val();

    if(event.keyCode == 13)
    {
      if(query.length > 2)
      {

        // alert(query);
        $('.table-students-fee').DataTable().ajax.reload();
      }
      else if(query.length == 0)
      {
        $('.table-students-fee').DataTable().ajax.reload();
      }
      else
      {
        // $('#input_keyword_std').empty();
        toastr.error('Error!', 'Please enter atlesat 3 characters then press Enter !!!' ,{"positionClass": "toast-bottom-right"});
      }
    }
  });
  $('#receipt_filter').keyup(function(event){
    var query = $(this).val();

    if(event.keyCode == 13)
    {
      if(query.length > 0)
      {

        // alert(query);
        $('.table-students-fee').DataTable().ajax.reload();
      }
      else if(query.length == 0)
      {
        $('.table-students-fee').DataTable().ajax.reload();
      }
      else
      {
        // $('#input_keyword_std').empty();
        toastr.error('Error!', 'Please enter atlesat a characters then press Enter !!!' ,{"positionClass": "toast-bottom-right"});
      }
    }
  });

  $('#student_search').keyup(function(event){
    var query = $(this).val();

    if(event.keyCode == 13)
    {
      if(query.length > 2)
      {

        // alert(query);
        $('.table-students-fee').DataTable().ajax.reload();
      }
      else if(query.length == 0)
      {
        $('.table-students-fee').DataTable().ajax.reload();
      }
      else
      {
        // $('#input_keyword_std').empty();
        toastr.error('Error!', 'Please enter atlesat 3 characters then press Enter !!!' ,{"positionClass": "toast-bottom-right"});
      }
    }
  });

  var table2 =  $('.table-students-fee').DataTable({
    processing: true,
    searching: true,
    "language": {
    processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw" style="color:#13436c;"></i><span class="sr-only">Loading...</span> '},
    ordering: false,
    pageLength: {{50}},
    serverSide: true,
    autoWidth: false,
    "lengthMenu": [50,100,150,200],
    
    scrollX:true,
    scrollY : '90vh',
    scrollCollapse: true,
    ajax: 
    {
      beforeSend: function(){
        $('#loader_modal').modal({
          backdrop: 'static',
          keyboard: false
        });
        $('#loader_modal').modal('show');
      },
      url: "{!! route('get-students-fee') !!}",
       data: function(data) { data.student_classes_select = $('.student_classes_select option:selected').val(),data.discount_student = $('.discount_student option:selected').val(),data.status_student = $('.status_student option:selected').val(), data.from_date = $('#from_date').val(), data.to_date = $('#to_date').val(),data.sub_from_date = $('#sub_from_date').val(), data.sub_to_date = $('#sub_to_date').val(), data.father_name = $('#father_name').val(), data.student_search = $('#student_search').val(),data.receipt = $('.receipt_filter').val() } ,
    },
    columns: [
        { data: 'checkbox', name: 'checkbox' }, 
        { data: 'name', name: 'name', class: 'text-capitalize' }, 
        { data: 'fee_month', name: 'fee_month' }, 
        { data: 'class', name: 'class' }, 
        { data: 'roll_no', name: 'roll_no' }, 
        { data: 'status', name: 'status', class: 'text-capitalize' }, 
        { data: 'gender', name: 'gender', class: 'text-capitalize' }, 
        { data: 'guardian', name: 'guardian', class: 'text-capitalize' },  
        { data: 'fee', name: 'fee' }, 
        { data: 'paid_amount', name: 'paid_amount' }, 
        { data: 'concession', name: 'concession' }, 
        { data: 'reason', name: 'reason' }, 
        { data: 'unpaid_amount', name: 'unpaid_amount' }, 
        { data: 'discount', name: 'discount' }, 
        { data: 'invoice_no', name: 'invoice_no' }, 
        { data: 'submitted_date', name: 'submitted_date' }, 
        { data: 'action', name: 'action' }, 
    ],
    initComplete: function () {
    // Enable THEAD scroll bars
    $('.dataTables_scrollHead').css('overflow', 'auto');
    // Sync THEAD scrolling with TBODY
      $('.dataTables_scrollHead').on('scroll', function () {
          $('.dataTables_scrollBody').scrollLeft($(this).scrollLeft());
      });
    },
    drawCallback: function(){
      $('#loader_modal').modal('hide');
    },
    footerCallback: function ( row, data, start, end, display ) {
        var api              = this.api();
        var json             = api.ajax.json();
        var total_amount     = json.total_amount;
        var paid_amount     = json.paid_amount;
        var concession     = json.concession;
        var unpaid_amount     = json.unpaid_amount;
        total_amount     = parseFloat(total_amount).toFixed(2);
        total_amount     = total_amount.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ",");

        paid_amount     = parseFloat(paid_amount).toFixed(2);
        paid_amount     = paid_amount.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ",");

        concession     = parseFloat(concession).toFixed(2);
        concession     = concession.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ",");

        unpaid_amount     = parseFloat(unpaid_amount).toFixed(2);
        unpaid_amount     = unpaid_amount.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ",");

        $( api.column( 8 ).footer() ).html(total_amount); 
        $( api.column( 9 ).footer() ).html(paid_amount); 
        $( api.column( 10 ).footer() ).html(concession); 
        $( api.column( 12 ).footer() ).html(unpaid_amount); 
      },
  });

  $('.search-btn').on('click',function(){
    $('.table-students-fee').DataTable().ajax.reload();
  });

  $(document).on('change','#student_admission_no',function(){
    // alert($(this).val());
    var id = $(this).val();

    $.ajaxSetup({
              headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
              }
            });
            $.ajax({
              url: "{{ route('fetch-student-detail') }}",
              method: 'post',
              data: {id:id},
              beforeSend: function(){
                $('#loader_modal').modal({
                  backdrop: 'static',
                  keyboard: false
                });
                $('#loader_modal').modal('show');
              },
              success: function (response) {
                
                if(response.success == true)
                {
                  // alert(response.fee);
                $('#loader_modal').modal('hide');
                // console.log(response.student);
                var fee_discount = response.student.fee;
                  $('#name').val(response.student.name);
                  $('#guardian').val(response.student.guardian);
                  // $('#guardian_cnic').val(response.student.guardian_cnic);
                  // $('#guardian_cnic').val(response.student.guardian_cnic);
                  $('#std_class').val(response.student.student_class.class_name);
                  $('#fee').val(fee_discount);
                  $('#paid_amount').attr('min',0);
                  $('#paid_amount').attr('max',fee_discount);
                  $('#discount').val(response.student.discount);

                }
               
              },
              error: function(request, status, error){
                $('#loader_modal').modal('hide');
              }
            });
  });

  $('#add-student-fee-form').on('submit',function(e){
    e.preventDefault();

      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
          }
      });
       $.ajax({
          url: "{{ route('add-student-fee') }}",
          dataType: 'json',
          type: 'post',
          data: new FormData(this),
          contentType: false,
          cache: false,
          processData:false,
          beforeSend: function(){
            $('.save-btn').addClass('disabled');
            $('.save-btn').attr('disabled', true);
            $('#loader_modal').modal({
              backdrop: 'static',
              keyboard: false
            });
            $('#loader_modal').modal('show');
          },
          success: function(result){
            $('.save-btn').attr('disabled', true);
            $('.save-btn').removeAttr('disabled');
            if(result.success == true){
              toastr.success('Success!', 'Student Fee Added Successfully!!!',{"positionClass": "toast-bottom-right"});
              $('.table-students-fee').DataTable().ajax.reload();
              /*setTimeout(function(){
                window.location.reload();
              }, 2000);*/

              $('#add-student-fee-form')[0].reset();
              $('#addStudentFee').modal('hide');
            }
            else if(result.paid == true)
            {
               toastr.warning('Warning!', 'This month fee already submitted!!!',{"positionClass": "toast-bottom-right"});
              $('.table-students-fee').DataTable().ajax.reload();
              $('#add-student-fee-form')[0].reset();
              $('#addStudentFee').modal('hide');
            }
            else if(result.total_amount == true)
            {
                toastr.info('Sorry!', 'Paid Amount Cannot Be Greater Than Total Amount!!!',{"positionClass": "toast-bottom-right"});
                $('#loader_modal').modal('hide');
            }
            else{
              toastr.error('Error!', result.errormsg,{"positionClass": "toast-bottom-right"});
              $('#loader_modal').modal('hide');
            }

          },
          error: function (request, status, error) {
                /*$('.form-control').removeClass('is-invalid');
                $('.form-control').next().remove();*/
                $('#loader_modal').modal('hide');
                $('.save-btn').removeClass('disabled');
                $('.save-btn').removeAttr('disabled');
                json = $.parseJSON(request.responseText);
                $.each(json.errors, function(key, value){
                      $('input[name="'+key+'"]').after('<span class="invalid-feedback" role="alert"><strong>'+value+'</strong>');
                     $('input[name="'+key+'"]').addClass('is-invalid');
                     $('textarea[name="'+key+'"]').after('<span class="invalid-feedback" role="alert"><strong>'+value+'</strong>');
                     $('textarea[name="'+key+'"]').addClass('is-invalid');


                });
            }
        });
  });

  $(document).on('click','.edit_fee',function(e){
    e.preventDefault();
    var id = $(this).data('id');

    var unpaid = $('#unpaid_amount_edit_'+id).val();
    var new_paid = $('#new_payment_'+id).val();

    // if(new_paid > unpaid || new_paid < 0 || new_paid == 0)
    // {
    //   toastr.error('Sorry!', 'New payment must be less or equal to unpaid amount!!!',{"positionClass": "toast-bottom-right"});
    //   return false;
    // }
    // alert(id);
      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
          }
      });
       $.ajax({
          url: "{{ route('edit-student-fee') }}",
          // dataType: 'json',
          method: 'post',
          data: $('#edit-student-fee-form-'+id).serialize(),
          // contentType: false,
          // cache: false,
          // processData:false,
          beforeSend: function(){
            $('.save-btn').addClass('disabled');
            $('.save-btn').attr('disabled', true);
            $('#loader_modal').modal({
              backdrop: 'static',
              keyboard: false
            });
            $('#loader_modal').modal('show');
          },
          success: function(result){
            $('.save-btn').attr('disabled', true);
            $('.save-btn').removeAttr('disabled');
            if(result.success == true){
              toastr.success('Success!', 'Student Fee Updated Successfully!!!',{"positionClass": "toast-bottom-right"});
              $('.table-students-fee').DataTable().ajax.reload();
              /*setTimeout(function(){
                window.location.reload();
              }, 2000);*/

              // $('#edit-student-fee-form')[0].reset();
              $('#EditStudentFee'+id).modal('hide');
            }
            else if(result.paid == true)
            {
               toastr.warning('Warning!', 'This month fee already submitted!!!',{"positionClass": "toast-bottom-right"});
              $('.table-students-fee').DataTable().ajax.reload();
              $('#add-student-fee-form')[0].reset();
              $('#addStudentFee').modal('hide');
            }
            else{
              toastr.error('Error!', result.errormsg,{"positionClass": "toast-bottom-right"});
              $('#loader_modal').modal('hide');
            }

          },
          error: function (request, status, error) {
                /*$('.form-control').removeClass('is-invalid');
                $('.form-control').next().remove();*/
                $('#loader_modal').modal('hide');
                $('.save-btn').removeClass('disabled');
                $('.save-btn').removeAttr('disabled');
                json = $.parseJSON(request.responseText);
                $.each(json.errors, function(key, value){
                      $('input[name="'+key+'"]').after('<span class="invalid-feedback" role="alert"><strong>'+value+'</strong>');
                     $('input[name="'+key+'"]').addClass('is-invalid');
                     $('textarea[name="'+key+'"]').after('<span class="invalid-feedback" role="alert"><strong>'+value+'</strong>');
                     $('textarea[name="'+key+'"]').addClass('is-invalid');


                });
            }
        });
  });
  $(document).on('click','.delete_student',function(){
      var id = $(this).data('id');

        swal({
            title: "Are You Sure!",
            html: true,
            text: "<b>You want to delete this student !!!</b>",
            type: "info",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes!",
            cancelButtonText: "No!",
            closeOnConfirm: true,
            closeOnCancel: false
          },
            function(isConfirm) {
            if (isConfirm){
             $.ajaxSetup({
              headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
              }
            });
            $.ajax({
              url: "{{ route('delete-student') }}",
              method: 'post',
              data: {id:id},
              beforeSend: function(){
                $('#loader_modal').modal({
                  backdrop: 'static',
                  keyboard: false
                });
                $('#loader_modal').modal('show');
              },
              success: function (response) {
                if(response.success == false)
                {
                  toastr.warning('Sorry!', 'Cannot delete this class, as it is already bond to student(s)!!!',{"positionClass": "toast-bottom-right"});
                  $('#loader_modal').modal('hide');
                }

                if(response.success == true)
                {
                  toastr.success('Success!', 'Student Deleted Successfully!!!',{"positionClass": "toast-bottom-right"});
                  $('.table-students').DataTable().ajax.reload();
                  $('#loader_modal').modal('hide');
                }
               
              },
              error: function(request, status, error){
                $('#loader_modal').modal('hide');
              }
            });
            }
            else{
              swal("Cancelled", "", "error");
              
            }
          });
  });
  $(document).on('click','.delete_history__icon',function(){
      var id = $(this).data('id');
        swal({
            title: "Are You Sure!",
            html: true,
            text: "<b>You want to delete this history !!!</b>",
            type: "info",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes!",
            cancelButtonText: "No!",
            closeOnConfirm: true,
            closeOnCancel: false
          },
            function(isConfirm) {
            if (isConfirm){
             $.ajaxSetup({
              headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
              }
            });
            $.ajax({
              url: "{{ route('delete-updated-fee-history') }}",
              method: 'post',
              data: {id:id},
              beforeSend: function(){
                $('#loader_modal').modal({
                  backdrop: 'static',
                  keyboard: false
                });
                $('#loader_modal').modal('show');
              },
              success: function (response) {
                if(response.success == true)
                {
                  toastr.success('Success!', 'Record Deleted Successfully!!!',{"positionClass": "toast-bottom-right"});
                  $('.table-students-fee').DataTable().ajax.reload();
                  $('#loader_modal').modal('hide');
                  $('#history_table').modal('hide');
                }

                if(response.success == false)
                {
                  toastr.error('Sorry!', 'Cannot delete record please contact administration!!!',{"positionClass": "toast-bottom-right"});
                  $('.table-students-fee').DataTable().ajax.reload();
                  $('#loader_modal').modal('hide');
                }
               
              },
              error: function(request, status, error){
                $('#loader_modal').modal('hide');
              }
            });
            }
            else{
              swal("Cancelled", "", "error");
              
            }
          });
  });

  $(document).on('change','.student_classes_select',function(){
    $('.table-students-fee').DataTable().ajax.reload();
    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
  });

  $(document).on('change','.discount_student',function(){
    $('.table-students-fee').DataTable().ajax.reload();
    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
  });

   $(document).on('change','.status_student',function(){
    $('.table-students-fee').DataTable().ajax.reload();
    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
  });

   $(document).on('change','#from_date',function(){
    $('.table-students-fee').DataTable().ajax.reload();
    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
  });

   $(document).on('change','#to_date',function(){
    $('.table-students-fee').DataTable().ajax.reload();
    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
  });

      $(document).on('change','#sub_from_date',function(){
    $('.table-students-fee').DataTable().ajax.reload();
    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
  });

   $(document).on('change','#sub_to_date',function(){
    $('.table-students-fee').DataTable().ajax.reload();
    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
  });

  $(document).on('click','#reset',function(){
    $('.student_classes_select').val('');
    $('.discount_student').val('');
    $('.status_student').val('');
    $('#from_date').val('');
    $('#sub_from_date').val('');
    $('#to_date').val('');
    $('#sub_to_date').val('');
    $('.table-students-fee').DataTable().ajax.reload();
    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
  });

  $('#paid_amount').on('focusout',function(){
    var paid = $('#paid_amount').val();

    var total = $('#fee').val();

    var diff = total - paid;

    $('#remaining_amount').val(diff);
  });

    $(document).on('click', '.download_invoice', function(){
    var selected_fees = [];
    $("input.check:checked").each(function() {
      selected_fees.push($(this).val());
    });

    if(selected_fees == ''){
            toastr.error('Error!', 'Select Fees First.',{"positionClass": "toast-bottom-right"});
        return false;
        }else{
        $('.fees_selected').val(selected_fees);

        var fees = $('.fees_selected').val();

        var url = "{{url('user/fee-invoice-print')}}"+"/"+fees;
                    window.open(url, 'Orders Receivable Print', 'width=1200,height=600,scrollbars=yes');
                  $('#loader_modal').modal('hide');

        // $.ajaxSetup({
        //       headers: {
        //         'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        //       }
        //     });
        //     $.ajax({
        //       url: "{{ route('check-same-student') }}",
        //       method: 'post',
        //       data: {fee_ids:fees},
        //       beforeSend: function(){
        //         $('#loader_modal').modal({
        //           backdrop: 'static',
        //           keyboard: false
        //         });
        //         $('#loader_modal').modal('show');
        //       },
        //       success: function (response) {
        //         if(response.same_student > 1)
        //         {
        //           toastr.info('Sorry!', 'Please select same student fee(s)!!!',{"positionClass": "toast-bottom-right"});
        //           $('#loader_modal').modal('hide');
        //         }
        //         else
        //         {
        //            var url = "{{url('user/fee-invoice-print')}}"+"/"+fees;
        //             window.open(url, 'Orders Receivable Print', 'width=1200,height=600,scrollbars=yes');
        //           $('#loader_modal').modal('hide');
                    
        //         }
               
        //       },
        //       error: function(request, status, error){
        //         // $('#loader_modal').modal('hide');
        //       }
        //     });
        // console.log(orders);
        // return false;
         // $('.export-account-received-form')[0].submit();
         // var url = "{{url('user/fee-invoice-print')}}"+"/"+fees;
          // window.open(url, 'Orders Receivable Print', 'width=1200,height=600,scrollbars=yes');
       }

    });

    $(document).on('click', '.download_invoice1', function(){
    var selected_fees = [];
    $("input.check:checked").each(function() {
      selected_fees.push($(this).val());
    });

    if(selected_fees == ''){
            toastr.error('Error!', 'Select Fees First.',{"positionClass": "toast-bottom-right"});
        return false;
        }else{
        $('.fees_selected').val(selected_fees);

        var fees = $('.fees_selected').val();

        var url = "{{url('user/fee-invoice-print-student-wise')}}"+"/"+fees;
                    window.open(url, 'Orders Receivable Print', 'width=1200,height=600,scrollbars=yes');
                  $('#loader_modal').modal('hide');
        }
      });
    $(document).on('click', '.download_invoice_large', function(){
    var selected_fees = [];
    $("input.check:checked").each(function() {
      selected_fees.push($(this).val());
    });

    if(selected_fees == ''){
            toastr.error('Error!', 'Select Fees First.',{"positionClass": "toast-bottom-right"});
        return false;
        }else{
        $('.invoices_ids').val(selected_fees);
        var fees = $('.invoices_ids').val();
          $.ajaxSetup({
              headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
              }
            });
        $.ajax({
            method: "get",
            url: "{{ url('user/export-student-invoices') }}",
            data:{ids: fees},
             beforeSend:function(){
                $('.export-pdf2').attr('title','Please Wait...');
                $('.export-pdf2').prop('disabled',true);
                $('.download-btn').addClass('d-none');
              },
            success: function(data)
            {
              if(data.success === true){
                $('.export-alert-success').addClass('d-none');
                $('.export-alert').removeClass('d-none');
                $('.export-pdf2').attr('title','Please Wait...');
                $('.export-pdf2').prop('disabled',true);
                getPdfStatus();
              }
            },
            error:function()
            {
                $('.export-alert').addClass('d-none');
                $('.export-pdf2').attr('title','Print');
                $('.export-pdf2').prop('disabled',false);
                toastr.error('Error!', 'Something went wrong.',{"positionClass": "toast-bottom-right"});
            }

          });
        }
      });
    function getPdfStatus()
    {
      $.ajax({
              method:"get",
              url:"{{route('get-pdf-status')}}",
              success:function(data){
                    if(data.status==1)
                    {
                      console.log("Status " +data.status);
                      setTimeout(
                        function(){
                          console.log("Calling Function Again");
                          getPdfStatus();
                        }, 5000);
                    }
                    else if(data.status==0 && data.exception == null)
                    {
                        $('.export-alert').addClass('d-none');
                        $('.export-pdf2').attr('title','Print');
                        $('.export-pdf2').prop('disabled',false);
                        $('.export-alert-success').removeClass('d-none');
                    }
                    else if(data.status==0 && data.exception != null)
                    {
                      $('.export-alert').addClass('d-none');
                      $('.export-pdf2').attr('title','Print');
                      $('.export-pdf2').prop('disabled',false);
                      $('.export-alert-success').removeClass('d-none');
                      toastr.error('Error!', 'Something went wrong.',{"positionClass": "toast-bottom-right"});
                    }
              }
            });
    }

    $(document).on('click', '.download_invoice2', function(){
    var selected_fees = [];
    $("input.check:checked").each(function() {
      selected_fees.push($(this).val());
    });

    if(selected_fees == ''){
            toastr.error('Error!', 'Select Fees First.',{"positionClass": "toast-bottom-right"});
        return false;
        }else{
        $('.fees_selected').val(selected_fees);

        var fees = $('.fees_selected').val();
        swal({
            title: "Are You Sure!",
            html: true,
            text: "<b>You want to delete these records !!!</b>",
            type: "info",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes!",
            cancelButtonText: "No!",
            closeOnConfirm: true,
            closeOnCancel: false
          },
            function(isConfirm) {
            if (isConfirm){
               $.ajaxSetup({
              headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
              }
            });
            $.ajax({
              url: "{{ route('delete-fee-records') }}",
              method: 'post',
              data: {fee_ids:fees},
              beforeSend: function(){
                $('#loader_modal').modal({
                  backdrop: 'static',
                  keyboard: false
                });
                $('#loader_modal').modal('show');
              },
              success: function (response) {
                if(response.success == true)
                {
                  toastr.success('Success!', 'Record(s) deleted Successfully!!!',{"positionClass": "toast-bottom-right"});
                  $('.table-students-fee').DataTable().ajax.reload();
                  $('#loader_modal').modal('hide');
                }
                else
                {
                    
                }
               
              },
              error: function(request, status, error){
                // $('#loader_modal').modal('hide');
              }
            });
            }
            else{
              swal("Cancelled", "", "error");
              
            }
          });
         
        }
      });

    $(document).on('click','.download_single_fee',function(){
      var fee = $(this).data('id');
      var url = "{{url('user/fee-invoice-print')}}"+"/"+fee;
        window.open(url, 'Fee Print', 'width=1200,height=600,scrollbars=yes');
      $('#loader_modal').modal('hide');
    })

    $('.upload-excel-form').on('submit',function(e){
  $('#import-modal').modal('hide');
  e.preventDefault();
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
    }
  });
  $.ajax({
    url: "{{ route('upload-students-excel-file-for-fees') }}",
    method: 'post',
    data: new FormData(this),
    cache: false,
    contentType: false,
    processData: false,
    beforeSend: function(){
      $('#loader_modal').modal({
        backdrop: 'static',
        keyboard: false
      });
      $("#loader_modal").data('bs.modal')._config.backdrop = 'static';
      $('#loader_modal').modal('show');
      $(".product-upload-btn").attr("disabled", true);
    },
    success: function(data){
      $('#loader_modal').modal('hide');
      $('#import-modal').modal('hide');
      $(".product-upload-btn").attr("disabled", false);
      if(data.success == true)
      {
        toastr.success('Success!', data.msg, {"positionClass": "toast-bottom-right"});
        if(data.errorMsg != null && data.errorMsg != '')
        {
          $('#msgs_alerts').html(data.errorMsg);
          $('.errormsgDiv').removeClass('d-none');
        }
        $('.exp_imp_btn').click();
        $('.upload-excel-form')[0].reset();
        $('.product_table').DataTable().ajax.reload();
        // $('.table-purchase-order-history').DataTable().ajax.reload();
      }
      if(data.success == "withissues")
      {
        toastr.warning('Warning!', data.msg, {"positionClass": "toast-bottom-right"});
        if(data.errorMsg != null && data.errorMsg != '')
        {
          $('#msgs_alerts').html(data.errorMsg);
          $('.errormsgDiv').removeClass('d-none');
        }
        // $('.exp_imp_btn').click();
        $('.upload-excel-form')[0].reset();
        $('.product_table').DataTable().ajax.reload();
        // $('.table-purchase-order-history').DataTable().ajax.reload();
      }
      if(data.success == false)
      {
        toastr.error('Error!', data.msg, {"positionClass": "toast-bottom-right"});
        // $('.exp_imp_btn').click();
        $('.upload-excel-form')[0].reset();
        $('.product_table').DataTable().ajax.reload();
        // $('.table-purchase-order-history').DataTable().ajax.reload();
      }
    },
    error: function (request, status, error) {
        $('#loader_modal').modal('hide');
        $(".product-upload-btn").attr("disabled", false);
        $('.po-porducts-details').DataTable().ajax.reload();
        $('.table-purchase-order-history').DataTable().ajax.reload();
        json = $.parseJSON(request.responseText);
        $.each(json.errors, function(key, value){
          $('input[name="'+key+'[]"]').after('<span class="invalid-feedback" role="alert"><strong>'+value+'</strong>');
          $('input[name="'+key+'[]"]').addClass('is-invalid');
        });
      }
  });
});

    $(document).on('click','.view_history',function(e) {
      var id = $(this).data('id');
      $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
      });
      $.ajax({
        method:"post",
        url:"{{route('view_fee_history')}}",
        data:{id: id,sub_from_date: $('#sub_from_date').val(), sub_to_date: $('#sub_to_date').val()},
        beforeSend:function(){
        },
        success:function(data){
          $('.history_table_data').html(data.html);
        },
        error:function(){
          $('.export-btn').prop('disabled',false);
        }
      });
    });
});
</script>
@stop

