@extends('user.layouts.layout')

@section('title','Single Student')

@section('content')
<style type="text/css">
.student-image
{
padding-top: 100%;
background-position: center;
background-size: cover;
}

.student_image
{
box-shadow: 0px 0px 15px 0px rgba(0,0,0,0.5);

}
</style>
<?php 
use Carbon\Carbon;
?>

     <div class="row page-title-row">
          <div class="col-12">
              <h1 class="page-title">Student Other Fee History</h1>
          </div>                           
      </div>

        <div class="card">
            <div class="card-body">
            <table class="table table-striped nowrap table-student-other-fee-history">
            <thead>
            <tr>
                <th>Fee Type</th>
                <th>Class</th>
                <th>Submitted Date</th>
                <th>Total Amount</th>
                <th>Concession</th>
                <th>Reason</th>
                <th>Paid Amount</th>
                <th>Unpaid Amount</th>
                <th>Remark</th>
            </tr>
            </thead>
            </table>
            </div>
      </div>
@endsection

@section('javascript')
<script type="text/javascript">
var table2 =  $('.table-student-other-fee-history').DataTable({
    processing: true,
    searching: false,
    "language": {
    processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw" style="color:#13436c;"></i><span class="sr-only">Loading...</span> '},
    ordering: false,
    pageLength: {{50}},
    serverSide: true,
    autoWidth: false,
    "lengthMenu": [50,100,150,200],
    
    scrollX:true,
    scrollY : '90vh',
    scrollCollapse: true,
    ajax: 
    {
      beforeSend: function(){
        $('#loader_modal').modal({
          backdrop: 'static',
          keyboard: false
        });
        $('#loader_modal').modal('show');
      },
      url: "{!! route('get-student-other-fee-history') !!}",
       data: function(data) { data.student_id = {{$id}} } ,
    },
    columns: [ 
        { data: 'fee_type', name: 'fee_type' }, 
        { data: 'cls', name: 'cls' }, 
        { data: 'submitted_date', name: 'submitted_date' }, 
        { data: 'amount', name: 'amount' }, 
        { data: 'concession', name: 'concession' }, 
        { data: 'reason', name: 'reason' }, 
        { data: 'paid_amount', name: 'paid_amount' }, 
        { data: 'unpaid_amount', name: 'unpaid_amount' },
        { data: 'remark', name: 'remark' }, 
    ],
    initComplete: function () {
    // Enable THEAD scroll bars
    $('.dataTables_scrollHead').css('overflow', 'auto');
    // Sync THEAD scrolling with TBODY
      $('.dataTables_scrollHead').on('scroll', function () {
          $('.dataTables_scrollBody').scrollLeft($(this).scrollLeft());
      });
    },
    drawCallback: function(){
      $('#loader_modal').modal('hide');
    },
  });
</script>
@stop

