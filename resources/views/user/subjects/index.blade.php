@extends('user.layouts.layout')

@section('title','All Subjects')

@section('content')
<style type="text/css">
   
</style>


                    <div class="row page-title-row">
                            <div class="col-md col-sm col page-title-col align-self-center">
                                <h1 class="page-title">All Subjects</h1>
                            </div>
                            <div class="col-sm col-auto ml-auto text-right page-action-button">
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addSubject">+Add subject</button>
                            </div>
                            
                    </div>

                  <div class="card">
                        <div class="card-body">
                          <div class="d-sm-flex justify-content-between">
                          <div class="delete-selected-item mb-4 d-none">
                            <a href="javascript:void(0);" class="btn selected-item-btn btn-sm delete_class deleteIcon" data-type="quotation" title="Delete Class" >
                            <i class="zmdi zmdi-delete" style="font-size: 25px;"></i></a>
                          </div>
                        </div>
                        <div class="table-responsive">
                        <table class="table table-striped nowrap"
                        <thead>
                        <tr>
                            <th>Action</th>
                            <th>Class</th>
                            <th>Name</th>
                        </tr>
                        </thead>
                        <tbody>
                          @foreach($classes as $class)
                          <tr>
                              <td>
                                <a href="{{url('user/update-subject/'.$class->id)}}" class="actionicon viewIcon delete_order_transaction" data-id="{{$class->id}}" title="Edit Subjects"><i class="zmdi zmdi-edit"></i></a>
                              </td>
                              <td>{{$class->class_name}}</td>
                              <td class="p-0">
                                <table style="width: 100%">
                                <tr>
                                @foreach($class->get_subjects as $subject)
                                  <td>{{$subject->name}}</td>
                                @endforeach
                              </tr>

                              <tr>
                                @foreach($class->get_subjects as $subject)
                                  <td>{{$subject->exam_score}}</td>
                                @endforeach
                              </tr>
                               
                                </table>
                              </td>
                              
                          </tr>
                        @endforeach
                        </tbody>
                        </table>
                        </div>
                    </div>
                </div>



<!--Add Student Modal -->
<div class="modal fade" id="addSubject" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add Subject</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form id="add-subject-form" method="POST">
        @csrf
      <div class="modal-body ">
          <div class="form-row next_subject">
            <div class="form-group col-md-5">
                <label for="name1">Subject Name</label>
                <input type="text" name="name[]" id="name1" class="form-control" placeholder="Subject Name..."/>
            </div>

            <div class="form-group col-md-5">
                <label for="exam_score1">Exam Marks</label>
                <input type="number" name="exam_score[]" id="exam_score1" class="form-control" placeholder="Enter Exam Marks..."/>
            </div>

            <div class="form-group col-md-2">
                 <label for="" style="visibility: hidden">Subject Name</label>
                <input type="button" class="form-control btn btn-primary add_subject_btn" value="+" />
            </div>
          </div>
          <div class="form-row">

            <div class="form-group col-md-6">
                <label for="subject_class">Subject Class</label>
                <select class="form-control form-control student_classes_select state-tags" id="subject_class" name="class_id" >
                  <option value="">-- All Classes --</option>
                   @foreach($classes as $class)
                    <option value="{{$class->id}}"> {{$class->class_name}} </option>
                    @endforeach
                </select>
            </div>
            
          </div>
          
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary save-btn">Add</button>
      </div>
      </form>
    </div>
  </div>
</div>
@endsection

@section('javascript')
<script type="text/javascript">
$(document).ready(function(){
  var table2 =  $('.table-subjects').DataTable({
    processing: true,
    searching: true,
    "language": {
    processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw" style="color:#13436c;"></i><span class="sr-only">Loading...</span> '},
    ordering: false,
    pageLength: {{50}},
    serverSide: true,
    autoWidth: false,
    "lengthMenu": [50,100,150,200],
    
    scrollX:true,
    scrollY : '90vh',
    scrollCollapse: true,
    ajax: 
    {
      beforeSend: function(){
        $('#loader_modal').modal({
          backdrop: 'static',
          keyboard: false
        });
        $('#loader_modal').modal('show');
      },
      url: "{!! route('get-subjects') !!}",
    },
    columns: [
        { data: 'checkbox', name: 'checkbox' },  
        { data: 'class', name: 'class' },  
        { data: 'name', name: 'name' },  
        { data: 'exam_score', name: 'exam_score' },  
    ],
    initComplete: function () {
    // Enable THEAD scroll bars
    $('.dataTables_scrollHead').css('overflow', 'auto');
    // Sync THEAD scrolling with TBODY
      $('.dataTables_scrollHead').on('scroll', function () {
          $('.dataTables_scrollBody').scrollLeft($(this).scrollLeft());
      });
    },
    drawCallback: function(){
      $('#loader_modal').modal('hide');
    },
  });

  $('#add-subject-form').on('submit',function(e){
    e.preventDefault();
    var class_id = $('.student_classes_select').val();

    if(class_id == null || class_id == '')
    {
        toastr.error('Sorry!', 'Selection of class is compulsory!!!',{"positionClass": "toast-bottom-right"});
        return false;
    }
      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
          }
      });
       $.ajax({
          url: "{{ route('add-student-subject') }}",
          dataType: 'json',
          type: 'post',
          data: new FormData(this),
          contentType: false,
          cache: false,
          processData:false,
          beforeSend: function(){
            $('.save-btn').addClass('disabled');
            $('.save-btn').attr('disabled', true);
            $('#loader_modal').modal({
              backdrop: 'static',
              keyboard: false
            });
            $('#loader_modal').modal('show');
          },
          success: function(result){
            $('.save-btn').attr('disabled', true);
            $('.save-btn').removeAttr('disabled');
            if(result.success == true && result.valid_subjects > 0){
              toastr.success('Success!', 'Subject Added Successfully!!!',{"positionClass": "toast-bottom-right"});
              $('.table-subjects').DataTable().ajax.reload();
              $('#loader_modal').modal('hide');

              setTimeout(function(){
                window.location.reload();
              }, 2000);

              $('#add-subject-form')[0].reset();
              $('#addSubject').modal('hide');
            }

            if(result.invalid_subjects > 0)
            {
              toastr.error('Sorry!', 'Cannot add subject(s) due to incomplete information for '+result.invalid_subjects+' subject(s)!!!',{"positionClass": "toast-bottom-right"});
              $('#loader_modal').modal('hide');
            }

            if(result.already_exists_subjects != null)
            {
              toastr.error('Sorry!', 'Subject(s) '+result.already_exists_subjects+' already exist for the given class!!!',{"positionClass": "toast-bottom-right"});
              $('#add-subject-form')[0].reset();
              $('#addSubject').modal('hide');

              $('#loader_modal').modal('hide');
            }

          },
          error: function (request, status, error) {
                /*$('.form-control').removeClass('is-invalid');
                $('.form-control').next().remove();*/
                $('#loader_modal').modal('hide');
                $('.save-btn').removeClass('disabled');
                $('.save-btn').removeAttr('disabled');
                json = $.parseJSON(request.responseText);
                $.each(json.errors, function(key, value){
                      $('input[name="'+key+'"]').after('<span class="invalid-feedback" role="alert"><strong>'+value+'</strong>');
                     $('input[name="'+key+'"]').addClass('is-invalid');
                     $('textarea[name="'+key+'"]').after('<span class="invalid-feedback" role="alert"><strong>'+value+'</strong>');
                     $('textarea[name="'+key+'"]').addClass('is-invalid');


                });
            }
        });
  });

  $(document).on('click','.delete_class',function(){
      var id = $(this).data('id');

        swal({
            title: "Alert!",
            html: true,
            text: "<b>Are you sure!!! You want to delete this class !!!</b>",
            type: "info",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes!",
            cancelButtonText: "No!",
            closeOnConfirm: true,
            closeOnCancel: false
          },
            function(isConfirm) {
            if (isConfirm){
             $.ajaxSetup({
              headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
              }
            });
            $.ajax({
              url: "{{ route('delete-class') }}",
              method: 'post',
              data: {id:id},
              beforeSend: function(){
                $('#loader_modal').modal({
                  backdrop: 'static',
                  keyboard: false
                });
                $('#loader_modal').modal('show');
              },
              success: function (response) {
                if(response.success == false)
                {
                  toastr.warning('Sorry!', 'Cannot delete this class, as it is already bond to student(s)!!!',{"positionClass": "toast-bottom-right"});
                  $('#loader_modal').modal('hide');
                }

                if(response.success == true)
                {
                  toastr.success('Success!', 'Class Deleted Successfully!!!',{"positionClass": "toast-bottom-right"});
                  $('.table-classes').DataTable().ajax.reload();
                  $('#loader_modal').modal('hide');
                }
               
              },
              error: function(request, status, error){
                $('#loader_modal').modal('hide');
              }
            });
            }
            else{
              swal("Cancelled", "", "error");
              
            }
          });
  });

  $(document).on('click','.add_subject_btn',function(){
    var count = 2;
    var html = '<div class="form-group col-md-5"><label for="name'+count+'">Subject Name</label><input type="text" name="name[]" id="name'+count+'" class="form-control" placeholder="Subject Name..."/></div><div class="form-group col-md-5"><label for="exam_score'+count+'">Exam Marks</label><input type="number" name="exam_score[]" id="exam_score'+count+'" class="form-control" placeholder="Enter Exam Marks..."/></div>';
      count++;
    $('.next_subject').append(html);
  })
});
</script>
@stop

