@extends('user.layouts.layout')

@section('title','All Subjects')

@section('content')
<style type="text/css">
   
</style>

                    <div>
                        <div class="row page-title-row">
                            <div class="col-md col-sm col-12 page-title-col align-self-center">
                                <h1 class="page-title">Update Subjects</h1>
                            </div>
                           <!--  <div class="col-sm col-12 mt-sm-0 mt-3 ml-auto text-right page-action-button">
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addSubject">+Add subject</button>
                            </div> -->
                            
                    </div>

                    <div class="row">
                        <div class="col-12 mt-4">
                    
                        <div class="card p-4">
                           <form id="update-subject-form" method="POST" action="{{route('update-subjects')}}">
        @csrf
      <div class="modal-body subjects-modal p-4">
        <div class="form-row next_subject">
        <input type="hidden" name="class_id" value="{{$id}}">

        @foreach($class_subjects as $sub)
        <input type="hidden" name="ids[]" value="{{$sub->id}}">

            <div class="form-group col-md-5">
                <label for="name{{$sub->id}}">Subject Name</label>
                <input type="text" name="name[]" id="name{{$sub->id}}" class="form-control" value="{{$sub->name}}" placeholder="Subject Name..."/>
            </div>

            <div class="form-group col-md-5">
                <label for="exam_score{{$sub->id}}">Exam Marks</label>
                <input type="number" name="exam_score[]" id="exam_score{{$sub->id}}" value="{{$sub->exam_score}}" class="form-control" placeholder="Enter Exam Marks..."/>
            </div>

            <div class="form-group col-1">
                <label style="visibility: hidden;">Exam Marks</label>

              <a href="javascript:void(0);" class="btn selected-item-btn btn-sm delete_subject deleteIcon form-control" data-type="Delete" data-id="{{$sub->id}}" title="Delete Subject" >
              <i class="zmdi zmdi-delete" style="font-size: 18px;"></i></a>
            </div>

          
          @endforeach
          </div>
          <div class="form-group col-md-2">
                 <label for="" style="visibility: hidden">Subject Name</label>
                <input type="button" class="form-control btn btn-primary add_subject_btn" value="+" />
            </div>
        
      </div>
      <div class="modal-footer">
    
        <button type="submit" class="btn btn-primary save-btn">Update</button>
      </div>
      </form>
                    </div>
                </div>
                    </div>
                    </div>


@endsection

@section('javascript')
<script type="text/javascript">
  $(document).ready(function(){
    @if(Session::has('update'))
      toastr.success('Success!', "Information Updated Successfully!!!",{"positionClass": "toast-bottom-right"});
      @php
       Session()->forget('update');
      @endphp
    @endif

    @if(Session::has('msg'))
      toastr.info('Sorry!', "{{ Session::get('msg') }}",{"positionClass": "toast-bottom-right"});
      @php
       Session()->forget('msg');
      @endphp
    @endif

    // $(document).on('submit','#update-subject-form',function(e){
    //   e.preventDefault();

    //   alert('hi');
    //   // $('#update-subject-form').submit();
    // });

      $(document).on('click','.delete_subject',function(){
      var id = $(this).data('id');

        swal({
            title: "Alert!",
            html: true,
            text: "<b>Are you sure!!! You want to delete this subject !!!</b>",
            type: "info",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes!",
            cancelButtonText: "No!",
            closeOnConfirm: true,
            closeOnCancel: false
          },
            function(isConfirm) {
            if (isConfirm){
             $.ajaxSetup({
              headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
              }
            });
            $.ajax({
              url: "{{ route('delete-subject') }}",
              method: 'post',
              data: {id:id},
              beforeSend: function(){
                $('#loader_modal').modal({
                  backdrop: 'static',
                  keyboard: false
                });
                $('#loader_modal').modal('show');
              },
              success: function (response) {
                if(response.success == false)
                {
                  toastr.warning('Sorry!', 'Cannot delete this subject, as it is already bond to student(s) result!!!',{"positionClass": "toast-bottom-right"});
                  $('#loader_modal').modal('hide');
                }

                if(response.success == true)
                {
                  toastr.success('Success!', 'Subject Deleted Successfully!!!',{"positionClass": "toast-bottom-right"});
                  // $('.table-classes').DataTable().ajax.reload();
                  
            setTimeout(function(){ location.reload(); }, 2000);
                  // $('#loader_modal').modal('hide');
                }
               
              },
              error: function(request, status, error){
                $('#loader_modal').modal('hide');
              }
            });
            }
            else{
              swal("Cancelled", "", "error");
              
            }
          });
  });
  });

    $(document).on('click','.add_subject_btn',function(){
    var count = "{{$class_subjects->count()}}";
    var html = '<div class="form-group col-md-5"><label for="name'+count+'">Subject Name</label><input type="text" name="name[]" id="name'+count+'" class="form-control" placeholder="Subject Name..."/></div><div class="form-group col-md-5"><label for="exam_score'+count+'">Exam Marks</label><input type="number" name="exam_score[]" id="exam_score'+count+'" class="form-control" placeholder="Enter Exam Marks..."/></div>';
      count++;
    $('.next_subject').append(html);
  });
</script>
@stop

