@extends('layouts.app')

@section('title','School Mangement System')

@section('content')
<style type="text/css">
.invalid-feedback {
     font-size: 100%;
}
.disabled:disabled{
  opacity:0.5;
  cursor: not-allowed;
}
#page-header
{
    background-color: #515f6d;
}
.offer-course-header
{
  color: #3f9ce8!important;
  margin: 0;
}

ul {
  font-family: "Roboto";
  font-size: 13px;
  line-height: 1.5em;
  margin: 5px 0 15px;
  padding: 0;
}
ul li {
  list-style: none;
  position: relative;
  padding: 0 0 0 20px;
  margin-bottom: 5px;
}
ul.circle-checkmark li::before {
  content: "";
  position: absolute;
  left: 0;
  top: 2px;
  border: solid 8px #ff6600;
  border-radius: 8px;
  -moz-border-radius: 8px;
  -webkit-border-radius: 8px;
}
ul.circle-checkmark li::after {
  content: "";
  position: absolute;
  left: 0.6%;
  top: 5px;
  width: 3px;
  height: 8px;
  border: solid #fff;
  border-width: 0 2px 2px 0;
  -webkit-transform: rotate(45deg);
  -moz-transform: rotate(45deg);
  -o-transform: rotate(45deg);
  transform: rotate(45deg);
}

</style>
<!-- Main Container -->
            <main id="main-container" style="margin-top: 68px;">
                <!-- Hero -->
                <div class="bg-gd-primary overflow-hidden">
                    <div class="hero bg-black-op-25" style="height: 600px !important;">
                            <div id="demo" class="carousel slide" data-ride="carousel" style="width: 100%;height: 100%;">

                              <!-- Indicators -->
                              <ul class="carousel-indicators">
                                <li data-target="#demo" data-slide-to="0" class="active"></li>
                                <li data-target="#demo" data-slide-to="1"></li>
                                <li data-target="#demo" data-slide-to="2"></li>
                              </ul>

                              <!-- The slideshow -->
                              <div class="carousel-inner">
                                @if($conf == null)
                                <div class="carousel-item active">
                                  <img src="https://media.istockphoto.com/photos/pakistan-monument-islamabad-picture-id535695503?k=6&m=535695503&s=612x612&w=0&h=uP8aDK4xlfjk3kEiyr9wwUiuh80UwAiICweFpiBDosk=" alt="Los Angeles" style="width: 100%;background-size: cover;background-position: center;max-height: 500px;">
                                </div>
                                <div class="carousel-item">
                                  <img src="https://cdn.pixabay.com/photo/2015/12/01/20/28/road-1072823__340.jpg" alt="Chicago" style="width: 100%;background-size: cover;background-position: center;max-height: 500px;">
                                </div>
                                <div class="carousel-item">
                                  <img src="https://www.itl.cat/pngfile/big/30-303191_background-images-for-editing-editing-pictures-background-background.jpg" alt="New York" style="width: 100%;background-size: cover;background-position: center;max-height: 500px;">
                                </div>
                                @else
                                <div class="carousel-item active">
                                  <img src="{{asset('public/uploads/website/banners/'.$conf->banner1)}}" alt="Los Angeles" style="width: 100%;background-size: cover;background-position: center;max-height: 500px;">
                                </div>

                                <div class="carousel-item">
                                  <img src="{{asset('public/uploads/website/banners/'.$conf->banner2)}}" alt="Los Angeles" style="width: 100%;background-size: cover;background-position: center;max-height: 500px;">
                                </div>

                                <div class="carousel-item">
                                  <img src="{{asset('public/uploads/website/banners/'.$conf->banner3)}}" alt="Los Angeles" style="width: 100%;background-size: cover;background-position: center;max-height: 500px;">
                                </div>
                                @endif
                              </div>

                              <!-- Left and right controls -->
                              <a class="carousel-control-prev" href="#demo" data-slide="prev">
                                <span class="carousel-control-prev-icon"></span>
                              </a>
                              <a class="carousel-control-next" href="#demo" data-slide="next">
                                <span class="carousel-control-next-icon"></span>
                              </a>

                            </div>
                        {{-- </div> --}}
                    </div>
                </div>
                <div class="custom-container p-4" style="margin: 0 20%;">
                <div class="row bg-white" style="margin-top: -75px">
                    <div class="col text-left bg-white shadow" style="padding: 20px 30px;border-radius: 5px;">
                      <div class="row">
                        <div class="col-xl-8 col-lg-12 col-md-12 col-sm-12">
                        <h4 class="offer-course-header text-black">Detail Of {{$course->name}}</h4>
                        <p><b>Course Name :</b> {{$course->name}}</p>
                        <p><b>Course Fee :</b> {{$course->course_fee}}</p>
                        <p><b>Duration :</b> {{$course->duration}}</p>
                        </div>  
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                        <div class="pull-right">
                          <button class="btn btn-primary"><a href="{{route('apply-online')}}" style="color: white;">Apply Online</a></button>
                          <button class="btn btn-secondary"><a target="_blank" href="{{asset('public/assets/files/admission-form.pdf')}}">Apply Offline</a></button>
                          <button class="btn btn-info"><a href="{{route('contact-us')}}" style="color: white;">Contact Us</a></button>
                        </div>
                        </div>
                      </div>

                        <hr>
                        @if($course_detail != null)
                          @foreach($course_detail as $detail)
                            <h4 class="offer-course-header text-black">{{$detail->header}}</h4>
                            @php $sub_details = explode('--',$detail->description) @endphp
                            @if(count($sub_details) == 1)
                            <p>{{$sub_details[0]}}</p>
                            <hr>

                            @else
                            <ul class="circle-checkmark">
                              @foreach($sub_details as $sub)
                                <li>{{$sub}}</li>
                              @endforeach
                            </ul>
                            <hr>
                            @endif
                          @endforeach
                        @endif

                    </div>
                    
                </div>

                </div>
                <!-- END Hero -->
              </main>
            <!-- END Main Container -->
@endsection

@section('javascript')
<!-- Jquery needed -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    

<!-- Function used to shrink nav bar removing paddings and adding black background -->
    <script>
        $(window).scroll(function() {
            if ($(document).scrollTop() > 50) {
                $('.nav').addClass('affix');
                console.log("OK");
            } else {
                $('.nav').removeClass('affix');
            }
        });
    </script>
@stop

