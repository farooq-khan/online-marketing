<nav class="navbar top-nanbar navbar-expand-md">
          {{-- <button class="navbar-toggler d-lg-none" type="button" data-toggle="collapse" data-target="#collapsibleNav" aria-controls="collapsibleNav"
              aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button> --}}
          <div class="collapse navbar-collapse collapsibleNav" id="collapsibleNav">
            <ul class="justify-content-around ml-auto navbar-nav w-100">
              <li class="nav-item">
                <a class="nav-link" href="{{route('dashboard')}}"><i class="fa-solid fa-dashboard"></i> Home</a>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="dropdownId" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa-solid fa-user"></i> HR</a>
                <ul class="dropdown-menu" aria-labelledby="dropdownId">
                  <li class="sub-dropdown">
                    <a class="dropdown-item" href="#">Staff Setup</a>
                    <ul class="sub-dropdown-menu">
                      <a class="dropdown-item" href="{{route('all-departments')}}">Department</a>
                      <a class="dropdown-item" href="{{route('all-designations')}}">Designation</a>
                      <a class="dropdown-item" href="{{route('all-payscales')}}">Pay Scales</a>
                    </ul>
                  </li>
                  <li><a class="dropdown-item" href="{{route('all-teachers')}}">Staff List</a></li>
                  <li><a class="dropdown-item" href="{{route('teacher-salaries')}}">Staff Salaries</a></li>
                  <li class="sub-dropdown">
                    <a class="dropdown-item" href="#">Staff Attendance</a>
                    <ul class="sub-dropdown-menu">
                      <a class="dropdown-item" href="javascript:void(0)" data-toggle="modal" data-target="#addTeacherAttendanceCommon">Add Attendance</a>
                      <a class="dropdown-item" href="{{route('teachers-all-attendance')}}">Attendance Report</a>
                    </ul>
                  </li>
                  <li><a class="dropdown-item" href="{{route('teacher-certificate')}}">Certificates</a></li>
                  <li class="sub-dropdown">
                    <a class="dropdown-item" href="#">Communication</a>
                    <ul class="sub-dropdown-menu">
                      <a class="dropdown-item" href="{{route('under-construction')}}">Phone Call / SMS</a>
                      <a class="dropdown-item" href="{{route('under-construction')}}">Whatsapp</a>
                      <a class="dropdown-item" href="{{route('under-construction')}}">Email</a>
                    </ul>
                  </li>
                  <li><a class="dropdown-item" href="#">Staff Performance</a></li>
                </ul>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="dropdownId" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa-solid fa-book"></i> Academics</a>
                <ul class="dropdown-menu" aria-labelledby="dropdownId">
                  {{-- <li><a class="dropdown-item" href="{{route('all-classes')}}">Classes</a></li> --}}
                  <li class="sub-dropdown">
                    <a class="dropdown-item" href="#">Classes</a>
                    <ul class="sub-dropdown-menu">
                      <a class="dropdown-item" href="{{route('under-construction')}}">Section List</a>
                      <a class="dropdown-item" href="{{route('all-classes')}}">Class List</a>
                      <a class="dropdown-item" href="{{route('under-construction')}}">Assign Class Teacher</a>
                    </ul>
                    </li>
                  {{-- <li><a class="dropdown-item" href="{{route('all-subjects')}}">Subjects</a></li> --}}
                  <li class="sub-dropdown">
                    <a class="dropdown-item" href="#">Subjects</a>
                    <ul class="sub-dropdown-menu">
                      <a class="dropdown-item" href="{{route('all-subjects')}}">Subject List</a>
                      <a class="dropdown-item" href="{{route('under-construction')}}">Assign Class Subject</a>
                      <a class="dropdown-item" href="{{route('under-construction')}}">Assign Subject Teacher</a>
                    </ul>
                    </li>
                  <li><a class="dropdown-item" href="{{route('under-construction')}}">Syllabus</a></li>
                  <li><a class="dropdown-item" href="{{url('timetables')}}">Time Table</a></li>
                </ul>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="dropdownId" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa-solid fa-user-graduate"></i> Students</a>
                <ul class="dropdown-menu" aria-labelledby="dropdownId">
                  <li><a class="dropdown-item" href="{{route('add-new-student')}}">Add Student</a></li>
                  <li><a class="dropdown-item" href="{{route('all-students')}}">Students Profiles</a></li>
                  <li><a class="dropdown-item" href="{{route('pass-out-students')}}">Passout Students</a></li>
                  <li><a class="dropdown-item" href="#">Student Performance</a></li>
                  {{-- <li><a class="dropdown-item" href="{{route('all-attendance')}}">Students Attendance</a></li> --}}
                  <li class="sub-dropdown">
                    <a class="dropdown-item" href="#">Students Attendance</a>
                    <ul class="sub-dropdown-menu">
                      <a class="dropdown-item" href="{{route('student-attendence')}}">Add Attendance</a>
                      <a class="dropdown-item" href="{{route('all-attendance')}}">Attendance Report</a>
                      <a class="dropdown-item" href="{{route('under-construction')}}">Absent Students</a>
                    </ul>
                  </li>
                  <li class="sub-dropdown">
                    <a class="dropdown-item" href="#">Communicatins</a>
                    <ul class="sub-dropdown-menu">
                      <a class="dropdown-item" href="{{route('under-construction')}}">Phone Call / SMS</a>
                      <a class="dropdown-item" href="{{route('under-construction')}}">Whatsapp</a>
                      <a class="dropdown-item" href="{{route('under-construction')}}">Email</a>
                    </ul>
                  </li>
                  <li class="sub-dropdown">
                    <a class="dropdown-item" href="#">Log In Credentials</a>
                    <ul class="sub-dropdown-menu">
                      <a class="dropdown-item" href="{{route('under-construction')}}">Parents Credentials</a>
                      <a class="dropdown-item" href="{{route('register-student-account')}}">Add Student Credentials</a>
                      <a class="dropdown-item" href="{{route('get-register-account', ['role' => 'student'])}}">Students Credentials</a>
                    </ul>
                  </li>
                  <li><a class="dropdown-item" href="{{ route('school-leaving-certificate') }}">Certificates</a></li>
                </ul>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="dropdownId" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa-solid fa-credit-card"></i> Fee</a>
                <ul class="dropdown-menu" aria-labelledby="dropdownId">
                  {{-- <li><a class="dropdown-item" href="{{route('all-fee-types')}}">Fee Setup</a></li> --}}
                  <li class="sub-dropdown">
                    <a class="dropdown-item" href="#">Fee Setup</a>
                    <ul class="sub-dropdown-menu">
                      <a class="dropdown-item" href="{{route('all-fee-types')}}">Fee Types</a>
                      <a class="dropdown-item" href="{{route('under-construction')}}">Fee Amount</a>
                    </ul>
                    </li>
                  <li><a class="dropdown-item" href="{{route('assign-fee')}}">Assign Fee</a></li>
                  <li><a class="dropdown-item" href="{{route('all-fee')}}">Submit Tuition Fee</a></li>
                  <li><a class="dropdown-item" href="{{route('submit-other-fee')}}">Submit Other Fee</a></li>
                  <li class="sub-dropdown">
                    <a class="dropdown-item" href="#">Fee Reports</a>
                    <ul class="sub-dropdown-menu">
                      <a class="dropdown-item" href="{{route('submitted-fee')}}">Submitted Fee</a>
                      <a class="dropdown-item" href="{{route('submitted-fee', ['type' => 'due'])}}">Due Fee</a>
                    </ul>
                    </li>
                  <li><a class="dropdown-item" href="{{route('check-unsubmited-fee')}}">Defaulter List</a></li>
                </ul>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="dropdownId" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa-solid fa-clipboard"></i> Examination</a>
                <ul class="dropdown-menu" aria-labelledby="dropdownId">
                  {{-- <li><a class="dropdown-item" href="{{route('all-result')}}">Annual Exam</a></li> --}}
                  <li class="sub-dropdown">
                    <a class="dropdown-item" href="#">Annual Exam</a>
                    <ul class="sub-dropdown-menu">
                      <a class="dropdown-item" href="{{route('all-exams')}}">Exam Settings</a>
                      <a class="dropdown-item" href="{{route('under-construction')}}">2-Terms / 3-Terms / 4-Terms</a>
                      <a class="dropdown-item" href="{{route('all-result')}}">Results</a>
                    </ul>
                    </li>
                  {{-- <li><a class="dropdown-item" href="{{route('all-test-result')}}">Test System</a></li> --}}
                  <li class="sub-dropdown">
                    <a class="dropdown-item" href="#">Test System</a>
                    <ul class="sub-dropdown-menu">
                      <a class="dropdown-item" href="{{route('all-tests')}}">Test Settings</a>
                      <a class="dropdown-item" href="{{route('under-construction')}}">Test Type</a>
                      <a class="dropdown-item" href="{{route('all-test-result')}}">Results</a>
                    </ul>
                    </li>
                  <li class="sub-dropdown">
                    <a class="dropdown-item" href="#">Entry Test</a>
                    <ul class="sub-dropdown-menu">
                      <a class="dropdown-item" href="{{route('under-construction')}}">Register Student</a>
                      <a class="dropdown-item" href="{{route('under-construction')}}">Test Type</a>
                      <a class="dropdown-item" href="{{route('under-construction')}}">Results</a>
                    </ul>
                    </li>
                  <li class="sub-dropdown">
                    <a class="dropdown-item" href="#">Scholarship Test</a>
                    <ul class="sub-dropdown-menu">
                      <a class="dropdown-item" href="{{route('under-construction')}}">Register Student</a>
                      <a class="dropdown-item" href="{{route('under-construction')}}">Test Type</a>
                      <a class="dropdown-item" href="{{route('under-construction')}}">Results</a>
                    </ul>
                    </li>
                  <li class="sub-dropdown">
                    <a class="dropdown-item" href="#">Assessments</a>
                    <ul class="sub-dropdown-menu">
                      <a class="dropdown-item" href="{{route('under-construction')}}">Assessment Type</a>
                      <a class="dropdown-item" href="{{route('under-construction')}}">Assessment Sheet</a>
                    </ul>
                    </li>
                </ul>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="dropdownId" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa-solid fa-wallet"></i> Finance</a>
                <ul class="dropdown-menu" aria-labelledby="dropdownId">
                  {{-- <li><a class="dropdown-item" href="#">Income</a></li>
                  <li><a class="dropdown-item" href="#">Balance Sheet</a></li> --}}
                  <li class="sub-dropdown">
                    <a class="dropdown-item" href="#">Expenses</a>
                    <ul class="sub-dropdown-menu">
                      <a class="dropdown-item" href="{{route('all-expenses-types')}}">Expense Type</a>
                      <a class="dropdown-item" href="{{route('add-expense')}}">Add Expense</a>
                      <a class="dropdown-item" href="{{route('all-expenses')}}">Expense Report</a>
                    </ul>
                  </li>
                  <li class="sub-dropdown">
                    <a class="dropdown-item" href="#">Income</a>
                    <ul class="sub-dropdown-menu">
                      <a class="dropdown-item" href="{{route('under-construction')}}">Income Type</a>
                      <a class="dropdown-item" href="{{route('under-construction')}}">Add Income</a>
                      <a class="dropdown-item" href="{{route('under-construction')}}">Income Report</a>
                    </ul>
                  </li>
                </ul>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="dropdownId" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa-solid fa-ellipsis-h"></i>More Departments</a>
                <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownId">
                  
                  <li class="sub-dropdown">
                      <a class="dropdown-item" href="#">Books</a>
                      <ul class="sub-dropdown-menu">
                        <a class="dropdown-item" href="{{route('all-books')}}">All Books</a>
                        <a class="dropdown-item" href="{{route('sold-books')}}">Sold Books</a>
                        <a class="dropdown-item" href="{{route('sold-books-history')}}">Sold Books History</a>
                      </ul>
                  </li>

                  <li class="sub-dropdown">
                      <a class="dropdown-item" href="#">Library</a>
                      <ul class="sub-dropdown-menu">
                        <a class="dropdown-item" href="{{route('library-all-books')}}">Library Books</a>
                        <a class="dropdown-item" href="{{route('issued-books')}}">Issued To Students</a>
                      </ul>
                  </li>

                  <li><a class="dropdown-item" href="{{route('under-construction')}}">Hostel</a></li>
                  <li><a class="dropdown-item" href="{{route('under-construction')}}">Computer Lab</a></li>
                  <li><a class="dropdown-item" href="{{route('under-construction')}}">Science Lab</a></li>
                  <li><a class="dropdown-item" href="{{route('under-construction')}}">Canteen</a></li>
                  <li><a class="dropdown-item" href="{{route('under-construction')}}">Stock Room</a></li>
                  <li><a class="dropdown-item" href="{{route('under-construction')}}">School Assets</a></li>
                </ul>
              </li>
              <li class="nav-item dropdown"> 
                <a class="nav-link dropdown-toggle" href="#" id="dropdownId" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa-solid fa-gear"></i> Settings</a>
                <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownId">
                  <li><a class="dropdown-item" href="{{route('all-schools')}}">School Information</a></li>
                  <li><a class="dropdown-item" href="{{route('all-applies')}}">Online Apply</a></li>
                  {{-- <li><a class="dropdown-item" href="#">Sign Up</a> --}}
                  <li><a class="dropdown-item" href="{{route('all-website-configuration')}}">Website Configuration</a></li>
                  <li class="sub-dropdown">
                    <a class="dropdown-item" href="#">Accounts</a>
                    <ul class="sub-dropdown-menu">
                      <a class="dropdown-item" href="{{route('register-account')}}">Register Account</a>
                      <a class="dropdown-item" href="{{route('get-register-account')}}">Accounts List</a>
                    </ul>
                  </li>
                  

                  <li><a class="dropdown-item" href="{{route('custom-certificate')}}">Custom Certificate</a></li>
                </ul>
              </li>
            </ul>
          </div>
          </nav>
   
          <script>
            // Get the current page URL (pathname)
// const currentPath = window.location.pathname;

// // Select all menu links
// const menuLinks = document.querySelectorAll('.navbar li a');

// // Iterate over each link
// menuLinks.forEach(link => {
//     // Get the href attribute of the link (it could be absolute or relative)
//     const linkPath = new URL(link.href).pathname;
//     console.log('console.log("linkPath");' + linkPath);
    

//     // Compare the href path with the current page URL path
//     if (linkPath === currentPath) {
//         link.classList.add('active'); // Add the 'active' class if they match
//         console.log('console.log("not matching");' + link);
//     } else {
//         link.classList.remove('active'); // Remove the 'active' class if not matching
//         console.log('console.log("not matching")' + link);
//     }
// });

          </script>