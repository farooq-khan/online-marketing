<div id="layoutSidenav_nav">
                <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
                    <div class="sb-sidenav-menu">
                        <div class="nav">
                            <div class="row w-100">
                                <div class="col-12 d-flex justify-content-center align-items-center mt-4">
                                @if(Auth::user()->logo != null && file_exists( public_path() . '/uploads/users/logos/'.Auth::user()->logo))                          
                                    <img src="{{asset('public/uploads/users/logos/'.Auth::user()->logo)}}" width="50%" style="border-radius: 50%;" height="130px">
                                @else
                                    <img src="{{asset('public/assets/img/user-avatar.png')}}" width="50%">
                                @endif
                                </div>
                                <div class="col-12 d-flex justify-content-center align-items-center text-white mt-1">
                                <h5>{{Auth::user()->name}}</h5>
                                </div>
                            </div>
                            <a class="nav-link" href="{{url('user/dashboard')}}">
                                <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt text-white"></i></div>
                                Dashboard
                            </a>

                           <!--  <a class="nav-link" href="{{route('all-students')}}">
                                <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt theme-color"></i></div>
                                All Students
                            </a>
 -->
                            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#studentsLayouts" aria-expanded="false" aria-controls="studentsLayouts">
                                <div class="sb-nav-link-icon"><i class="fas fa-users text-white"></i></div>
                                Students
                                <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                            </a>
                            <div class="collapse" id="studentsLayouts" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                                <nav class="sb-sidenav-menu-nested nav">
                                    <a class="nav-link" href="{{route('all-students')}}">All Students</a>
                                </nav>
                            </div>

                            <a class="nav-link" href="{{route('all-attendance')}}">
                                <div class="sb-nav-link-icon"><i class="fas fa-clock text-white"></i></div>
                                Attendance
                            </a>

                            {{-- <a class="nav-link" href="{{route('all-fee')}}">
                                <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt theme-color"></i></div>
                                Submit Fee
                            </a> --}}

                             <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#feeLayouts" aria-expanded="false" aria-controls="feeLayouts">
                                <div class="sb-nav-link-icon"><i class="fas fa-money-check-alt text-white"></i></div>
                                Fee
                                <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                            </a>
                            <div class="collapse" id="feeLayouts" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                                <nav class="sb-sidenav-menu-nested nav">
                                    <a class="nav-link" href="{{route('all-fee-types')}}">Add Fee Type(s)</a>
                                    <a class="nav-link" href="{{route('all-fee')}}">Submit Tuition Fee</a>
                                    <a class="nav-link" href="{{route('submit-other-fee')}}">Submit Other Fee</a>
                                </nav>
                            </div>

                           {{--  <a class="nav-link" href="{{route('all-result')}}">
                                <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt theme-color"></i></div>
                                Results
                            </a> --}}

                             <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#resultLayouts" aria-expanded="false" aria-controls="resultLayouts">
                                <div class="sb-nav-link-icon"><i class="fas fa-poll text-white"></i></div>
                                Results
                                <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                            </a>
                            <div class="collapse" id="resultLayouts" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                                <nav class="sb-sidenav-menu-nested nav">
                                    <a class="nav-link" href="{{route('all-result')}}">Exam Result</a>
                                    <a class="nav-link" href="{{route('all-test-result')}}">Test(s) Result</a>
                                </nav>
                            </div>

                            <a class="nav-link" href="{{route('all-classes')}}">
                                <div class="sb-nav-link-icon"><i class="fas fa-school text-white"></i></div>
                                All Classes
                            </a>

                             <a class="nav-link" href="{{route('all-subjects')}}">
                                <div class="sb-nav-link-icon"><i class="fas fa-book text-white"></i></div>
                                All Subjects
                            </a>

                            

                            {{-- <a class="nav-link" href="{{route('all-exams')}}">
                                <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt theme-color"></i></div>
                                Exams
                            </a> --}}


                             <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#examLayouts" aria-expanded="false" aria-controls="examLayouts">
                                <div class="sb-nav-link-icon"><i class="fas fa-pager text-white"></i></div>
                                Exams
                                <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                            </a>
                            <div class="collapse" id="examLayouts" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                                <nav class="sb-sidenav-menu-nested nav">
                                    <a class="nav-link" href="{{route('all-exams')}}">Add Exam</a>
                                    <a class="nav-link" href="{{route('all-tests')}}">Add Test</a>
                                </nav>
                            </div>

                            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseLayouts" aria-expanded="false" aria-controls="collapseLayouts">
                                <div class="sb-nav-link-icon"><i class="fas fa-chalkboard-teacher text-white"></i></div>
                                Teachers
                                <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                            </a>
                            <div class="collapse" id="collapseLayouts" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                                <nav class="sb-sidenav-menu-nested nav">
                                    <a class="nav-link" href="{{route('all-teachers')}}">All Teachers</a>
                                    {{-- <a class="nav-link" href="{{route('suspended-teachers')}}">Suspended Teachers</a> --}}
                                    <a class="nav-link" href="{{ route('teacher-salaries') }}">Teachers Salary</a>
                                    <a class="nav-link" href="{{route('teachers-all-attendance')}}">Teachers Attendance</a>
                                </nav>
                            </div>

                            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#certificateLayouts" aria-expanded="false" aria-controls="certificateLayouts">
                                <div class="sb-nav-link-icon"><i class="fas fa-certificate text-white"></i></div>
                                Certificates
                                <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                            </a>
                            <div class="collapse" id="certificateLayouts" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                                <nav class="sb-sidenav-menu-nested nav">
                                    <a class="nav-link" href="{{route('school-leaving-certificate')}}">Students Certificate</a>
                                    <a class="nav-link" href="{{route('teacher-certificate')}}">Teacher Experience Certificate</a>
                                </nav>
                            </div>

                            

                            {{-- <a class="nav-link" href="javascript:void(0)">
                                <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt theme-color"></i></div>
                                Fee History
                            </a> --}}
                           
                            {{-- <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseLayouts" aria-expanded="false" aria-controls="collapseLayouts">
                                <div class="sb-nav-link-icon"><i class="fas fa-columns"></i></div>
                                Layouts
                                <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                            </a>
                            <div class="collapse" id="collapseLayouts" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                                <nav class="sb-sidenav-menu-nested nav">
                                    <a class="nav-link" href="javascript:void(0)">Static Navigation</a>
                                    <a class="nav-link" href="javascript:void(0)">Light Sidenav</a>
                                </nav>
                            </div>
                            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="false" aria-controls="collapsePages">
                                <div class="sb-nav-link-icon"><i class="fas fa-book-open"></i></div>
                                Pages
                                <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                            </a>
                            <div class="collapse" id="collapsePages" aria-labelledby="headingTwo" data-parent="#sidenavAccordion">
                                <nav class="sb-sidenav-menu-nested nav accordion" id="sidenavAccordionPages">
                                    <a class="nav-link collapsed" href="javascript:void(0)" data-toggle="collapse" data-target="#pagesCollapseAuth" aria-expanded="false" aria-controls="pagesCollapseAuth">
                                        Authentication
                                        <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                                    </a>
                                    <div class="collapse" id="pagesCollapseAuth" aria-labelledby="headingOne" data-parent="#sidenavAccordionPages">
                                        <nav class="sb-sidenav-menu-nested nav">
                                            <a class="nav-link" href="login.html">Login</a>
                                            <a class="nav-link" href="register.html">Register</a>
                                            <a class="nav-link" href="password.html">Forgot Password</a>
                                        </nav>
                                    </div>
                                    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#pagesCollapseError" aria-expanded="false" aria-controls="pagesCollapseError">
                                        Error
                                        <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                                    </a>
                                    <div class="collapse" id="pagesCollapseError" aria-labelledby="headingOne" data-parent="#sidenavAccordionPages">
                                        <nav class="sb-sidenav-menu-nested nav">
                                            <a class="nav-link" href="401.html">401 Page</a>
                                            <a class="nav-link" href="404.html">404 Page</a>
                                            <a class="nav-link" href="500.html">500 Page</a>
                                        </nav>
                                    </div>
                                </nav>
                            </div> --}}
                            
                        </div>
                    </div>
                    <div class="sb-sidenav-footer">
                        <div class="small">Logged in as:</div>
                        {{Auth::user()->name}}
                    </div>
                </nav>
            </div>