<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="_token" content="{{csrf_token()}}" />
<title>School Managment System</title>
<link href="https://fonts.googleapis.com/css2?family=Karla:ital,wght@0,200..800;1,200..800&family=Montserrat:ital,wght@0,100..900;1,100..900&display=swap" rel="stylesheet">
<link href="{{asset('public/vendor/mdi-font/css/material-design-iconic-font.min.css')}}" rel="stylesheet" media="all">
<link rel="stylesheet" type="text/css" href="{{asset('public/css/jquery.dataTables.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('public/css/buttons.dataTables.min.css')}}">
<link href="{{asset('public/css/bootstrap.css')}}" rel="stylesheet" />
<link href="{{asset('public/frontend/css/toastr.min.css')}}" rel="stylesheet" />
<link href="{{asset('public/css/sweetalert.min.css')}}" rel="stylesheet">
<link href="{{asset('public/css/select2.min.css')}}" rel="stylesheet">
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.6.0/css/all.min.css" rel="stylesheet">
<link href="{{asset('public/css/custom.css?')}}{{time()}}" rel="stylesheet" />

@include('layouts.google-analytics')
</head>
<body class="sb-nav-fixed">
  @include('user.layouts.header')

  {{-- @if(true)
  @include('user.layouts.header')
  @else
  @include('admin.layouts.header')
  @endif --}}


  {{-- @if(Auth::user()->role == 'user')
  @include('user.layouts.sidebar')
  @elseif(Auth::user()->role == 'student')
  @include('student.layouts.sidebar')
  @elseif(Auth::user()->role == 'teacher')
  @include('teacher.layouts.sidebar')
  @else
  @include('admin.layouts.sidebar')
  @endif --}}
  
  <main class="main-content">
    {{-- @if(auth()->user()->role != 'student')
    @include('admin.layouts.sidebar')
    @endif --}}
    <section class="right-content">
    @yield('content')
    </section>
  @include('common.common-modal')
</main>

<!-- Loader Modal -->
<div class="modal" id="loader_modal" role="dialog">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-body">
        <h3 style="text-align:center;color: black;">Please wait</h3>
        <p style="text-align:center;"><img src="{{ asset('public/uploads/gif/waiting.gif') }}"></p>
      </div>
    </div>
  </div>
</div>

{{-- Toastr Plugin --}}
{{-- <script src="{{asset('public/frontend/js/jquery.min.js')}}"></script> --}}
{{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.7.1/jquery.min.js"></script> --}}
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

 <script src="{{asset('public/js/bootstrap.min.js')}}" crossorigin="anonymous"></script>

<script src="{{asset('public/vendor/select2/select2.min.js')}}"></script>
<script src="{{asset('public/vendor/datepicker/moment.min.js')}}"></script>
<script src="{{asset('public/vendor/datepicker/daterangepicker.js')}}"></script>
 {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/js/all.min.js" crossorigin="anonymous"></script> --}}
 {{-- <script src="{{asset('public/js/all.min.js')}}" crossorigin="anonymous"></script> --}}

 <script src="{{asset('public/user/layouts/js/scripts.js')}}"></script>
<script src="{{asset('public/frontend/js/toastr.min.js')}}"></script>

 {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script> --}}
{{-- <script src="{{asset('public/user/layouts/demo/chart-area-demo.js')}}"></script> --}}
{{-- <script src="{{asset('public/user/layouts/demo/chart-bar-demo.js')}}"></script> --}}
{{-- <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script> --}}
{{-- <script src="{{asset('public/user/layouts/demo/datatables-demo.js')}}"></script> --}}
{{-- <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script> --}}
 <!-- DataTables -->
<script src="{{asset('public/js/jquery.dataTables.js')}}"></script>
<script src="{{asset('public/js/dataTables.buttons.min.js')}}"></script>
<!-- <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.5/js/dataTables.buttons.min.js"></script> -->
<!-- <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.flash.min.js"></script> -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.html5.min.js"></script>
{{-- Sweet Alert --}}
<script src="{{asset('public/js/sweetalert.min.js')}}"></script>


<script type="text/javascript">

$(document).ready(function () {

  
//   $('.ellipsis-button').on('shown.bs.dropdown', function () {
//   // do something...
//   console.log('working');
  
// })
$('.dropdown').on('show.bs.dropdown', function () {
  console.log('Dropdown is about to be shown:', this);
});
    // Add event listener to dropdown button
    // $(document).on('click', '.ellipsis-button', function (e) {
    //     e.stopPropagation(); // Prevent event from bubbling to the document
    //     $(this).parents('.dataTables_scrollBody').addClass('scrollBody-active');
    //     let dropdownHeight = $(this).siblings('.dropdown-menu').outerHeight();
    //     $('.scrollBody-active').css('min-height', dropdownHeight + 20 + 'px');
        
    // });
    // Remove the class when clicking anywhere outside the dropdown
    $(document).on('click', function () {
        $('.dataTables_scrollBody').removeClass('scrollBody-active'); // Remove the class from the datatable body
        $('.dataTables_scrollBody').css('min-height', 'auto');
    });

    // Prevent click inside the dropdown menu from closing it
    $(document).on('click', '.dropdown-menu', function (e) {
        e.stopPropagation();
    });
});

jQuery(document).ready(function($){
  setTimeout(() => {
  const fullUrl = window.location.href;
  var target = $('nav a[href="'+fullUrl+'"]');
  target.addClass('active').parents('li').addClass('active');
  }, 1000);
});


 $(document).ready(function() {

  $('#add-teacher-attendance-common').on('submit',function(e){
    e.preventDefault();
    var attendance_date = $('#date').val();
    var btn = 'click';

     if(attendance_date == null || attendance_date == '')
    {
      toastr.info('Sorry!', 'Select Date First!!!',{"positionClass": "toast-bottom-right"});
      return false;
    }

    window.location.href = "{{url('user/submit-teacher-attendance')}}/"+attendance_date+"/"+btn;
  });

//   $('.dropdown-menu a.dropdown-toggle').on('click', function(e) {
//   if (!$(this).next().hasClass('show')) {
//     $(this).parents('.dropdown-menu').first().find('.show').removeClass('show');
//   }
//   var $subMenu = $(this).next('.dropdown-menu');
//   $subMenu.toggleClass('show');


//   $(this).parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown', function(e) {
//     $('.dropdown-submenu .show').removeClass('show');
//   });


//   return false;
// });





  let headerHeight = $('header.header').height();
  $('body').css('padding-top', headerHeight);
  


$(".toggle-btn").on("click", function () {
    window.setTimeout(function () {
      $(".header-logo").toggleClass("header-logo-collapsed");
      $("aside.sidebar").toggleClass("sidebar-collapsed");
    }, 500);
  });

// Get the current page URL pathname
// const currentPage = window.location.pathname;

// // Select all menu items (adjust the selector to match your menu structure)
// const menuItems = document.querySelectorAll('nav a'); 

// // Loop through each menu item
// menuItems.forEach(item => {
//     // Get the href attribute of the menu item
//     const itemHref = item.getAttribute('href');

//     // Check if the href attribute matches the current page
//     if (itemHref === currentPage || itemHref === currentPage + '/') {
//     item.classList.add('action');
// }
// });
});

</script>
@yield('javascript')

</body>
</html>