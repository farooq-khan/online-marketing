<header class="bg-white header fixed-top">
  <div class="row mx-0 header-top align-items-center">
    <div class="align-items-center col-auto d-flex pr-2 school-info school-name">
        <div class="px-0 col-auto school-logo">
          <img src="https://img.freepik.com/premium-vector/education-school-logo-design_586739-1335.jpg" alt="">
        </div>
        <div class="col px-0 school-name">
          <h5 class="mb-0">Mentor Education System</h5>
          <p class="mb-0 s-location">Location: Dubai Adda Campus</p>
          <p class="mb-0 s-registration">BISEM Reg NO: 12345 | PSRA Reg No: 12345678901234</p>
          <p class="mb-0 s-contact">Email: thementordubai@gmail.com | Phone: 03080000270</p>
        </div>
        <div class="col px-0 menu-button-mobile d-md-none">
          <button class="navbar-toggler d-lg-none" type="button" data-toggle="collapse" data-target="#collapsibleNav" aria-controls="collapsibleNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
        </div>
    </div>
    {{-- <div class="col school-contact-info py-2">
      
    </div> --}}
    @include('user.layouts.header-info')
    <div class="col header-menu py-2 d-none">
        <div class="row">
            <div class="col header-search">
              <div class="input-group mb-0">
                <input type="text" class="form-control" id="inlineFormInputGroup" placeholder="Username">
                <div class="input-group-prepend">
                  <div class="input-group-text">@</div>
                </div>
              </div>
            </div>
            @include('user.layouts.header-info')
            
        </div>
        
    </div>
  </div>

  @include('user.layouts.menu')

</header>


<header class="bg-white d-none">
    <div class="row mx-0">
      @if(@auth()->user()->role != 'student')
        <div class="col-auto header-logo ">
          <div class="logo">
            <a href="#">
                <img src="https://dev1.schainwizard.com/uploads/configuration/1704712475.png" class="img-fluid">
            </a>
          </div>
          <button class="btn toggle-btn fa fa-bars align-self-center ml-md-auto mr-md-0 mx-auto">
            {{-- <svg class="menu-bars" xmlns="http://www.w3.org/2000/svg" height="36px" viewBox="0 -960 960 960" width="36px" fill="#fff"><path d="M148.25-216.83q-18.5 0-31.29-12.82-12.79-12.81-12.79-31.74 0-18.55 12.79-31.25 12.79-12.71 31.29-12.71h663.5q18.5 0 31.48 12.82 12.97 12.82 12.97 31.37 0 18.93-12.97 31.63-12.98 12.7-31.48 12.7h-663.5Zm0-219.1q-18.5 0-31.29-12.82t-12.79-31.37q0-18.55 12.79-31.25t31.29-12.7h663.5q18.5 0 31.48 12.82 12.97 12.82 12.97 31.37t-12.97 31.25q-12.98 12.7-31.48 12.7h-663.5Zm0-218.72q-18.5 0-31.29-12.82t-12.79-31.75q0-18.55 12.79-31.25t31.29-12.7h663.5q18.5 0 31.48 12.82 12.97 12.81 12.97 31.36 0 18.93-12.97 31.63-12.98 12.71-31.48 12.71h-663.5Z"></path></svg>
            <svg class="menu-close" xmlns="http://www.w3.org/2000/svg" height="40px" viewBox="0 -960 960 960" width="40px" fill="#FFFFFF"><path d="m480-418.26-197.8 197.8q-12.87 12.87-30.87 12.87t-30.87-12.87q-12.87-12.87-12.87-30.87t12.87-30.87L418.26-480l-197.8-197.8q-12.87-12.87-12.87-30.87t12.87-30.87q12.87-12.87 30.87-12.87t30.87 12.87L480-541.74l197.8-197.8q12.87-12.87 30.87-12.87t30.87 12.87q12.87 12.87 12.87 30.87t-12.87 30.87L541.74-480l197.8 197.8q12.87 12.87 12.87 30.87t-12.87 30.87q-12.87 12.87-30.87 12.87t-30.87-12.87L480-418.26Z"/></svg> --}}
          </button>
        </div>
        @endif
        <nav class="col py-0 align-items-stretch header-menu navbar navbar-expand-lg d-noneeeeeee">
            <a class="navbar-brand d-lg-none" href="#">
              <img src="https://dev1.schainwizard.com/uploads/configuration/1704712475.png" class="img-fluid">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <svg class="hamburger" xmlns="http://www.w3.org/2000/svg" height="24px" viewBox="0 -960 960 960" width="24px" fill="#0c4475"><path d="M149-191q-26 0-44.5-18.5T86-254q0-26 18.5-44.5T149-317h662q26 0 44.5 18.5T874-254q0 26-18.5 44.5T811-191H149Zm0-226q-26 0-44.5-18.5T86-480q0-26 18.5-44.5T149-543h662q26 0 44.5 18.5T874-480q0 26-18.5 44.5T811-417H149Zm0-226q-26 0-44.5-18.5T86-706q0-26 18.5-44.5T149-769h662q26 0 44.5 18.5T874-706q0 26-18.5 44.5T811-643H149Z"/></svg>
                <svg class="menu-close" xmlns="http://www.w3.org/2000/svg" height="24px" viewBox="0 -960 960 960" width="24px" fill="#0c4475"><path d="M480-392 300-212q-18 18-44 18t-44-18q-18-18-18-44t18-44l180-180-180-180q-18-18-18-44t18-44q18-18 44-18t44 18l180 180 180-180q18-18 44-18t44 18q18 18 18 44t-18 44L568-480l180 180q18 18 18 44t-18 44q-18 18-44 18t-44-18L480-392Z"/></svg>
            </button>
          
            <div class="align-items-stretch collapse navbar-collapse" id="navbarSupportedContent">
              <ul class="justify-content-lg-around mr-auto navbar-nav w-100">
                <li class="nav-item align-items-stretch d-flex">
                  <a class="nav-link" href="{{url('admin/dashboard')}}">Dashboard</a>
                </li>
                <li class="nav-item align-items-stretch d-flex dropdown">
                  <a class="nav-link align-self-center dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-expanded="false">
                    User Management
                  </a>
                  <div class="dropdown-menu">
                    <a class="dropdown-item" href="{{route('all-students')}}">All Students</a>
                    <a class="dropdown-item" href="{{route('pass-out-students')}}">Pass Out Students</a>
                    <a class="dropdown-item" href="{{route('all-attendance')}}">Attendance</a>
                    <hr>
                    <a class="dropdown-item" href="{{route('all-teachers')}}">All Teachers</a>
                                    {{-- <a class="dropdown-item" href="{{route('suspended-teachers')}}">Suspended Teachers</a> --}}
                    <a class="dropdown-item" href="{{ route('teacher-salaries') }}">Teachers Salary</a>
                    <a class="dropdown-item" href="{{route('teachers-all-attendance')}}">Teachers Attendance</a>
                    <hr>
                    <a class="dropdown-item" href="{{route('register-account')}}">Register Account</a>
                    <a class="dropdown-item" href="{{route('get-register-account')}}">All Accounts</a>
                  </div>
                </li>
                <li class="nav-item align-items-stretch d-flex dropdown">
                  <a class="nav-link align-self-center dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-expanded="false">
                    Academics
                  </a>
                  <div class="dropdown-menu">
                    <a class="dropdown-item" href="{{route('all-classes')}}">All Classes</a>
                    <a class="dropdown-item" href="{{route('all-subjects')}}">All Subjects</a>
                    <hr>
                    <a class="dropdown-item" href="{{route('all-exams')}}">Add Exam</a>
                    <a class="dropdown-item" href="{{route('all-tests')}}">Add Test</a>
                    <hr>
                    <a class="dropdown-item" href="{{route('all-result')}}">Exam Result</a>
                    <a class="dropdown-item" href="{{route('all-test-result')}}">Test(s) Result</a>
                  </div>
                </li>
                <li class="nav-item align-items-stretch d-flex dropdown">
                  <a class="nav-link align-self-center dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-expanded="false">
                    Administration
                  </a>
                  <div class="dropdown-menu">
                    <a class="dropdown-item" href="{{route('all-fee-types')}}">Add Fee Type(s)</a>
                    <a class="dropdown-item" href="{{route('all-fee')}}">Submit Tuition Fee</a>
                    <a class="dropdown-item" href="{{route('submit-other-fee')}}">Submit Other Fee</a>
                    <a class="dropdown-item" href="{{route('check-unsubmited-fee')}}">Unsubmitted Fee</a>
                    <hr>
                    <a class="dropdown-item" href="{{route('all-expenses-types')}}">Add Expense Type</a>
                    <a class="dropdown-item" href="{{route('all-expenses')}}">All Expenses</a>
                    <hr>
                    <a class="dropdown-item" href="{{route('all-books')}}">All Books Stock</a>
                    <a class="dropdown-item" href="{{route('sold-books')}}">Sold Books</a>
                    <a class="dropdown-item" href="{{route('sold-books-history')}}">Sold Books History</a>
                    <hr>
                    <a class="dropdown-item" href="{{route('library-all-books')}}">library Books</a>
                    <a class="dropdown-item" href="{{route('issued-books')}}">Issued Books</a>
                  </div>
                </li>
                <li class="nav-item align-items-stretch d-flex dropdown">
                  <a class="nav-link align-self-center dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-expanded="false">
                    Certificates
                  </a>
                  <div class="dropdown-menu">
                    <a class="dropdown-item" href="{{route('school-leaving-certificate')}}">Students Certificates</a>
                    <a class="dropdown-item" href="{{route('teacher-certificate')}}">Teacher Experience Certificate</a>
                    <a class="dropdown-item" href="{{route('custom-certificate')}}">Custom Certificate</a>
                  </div>
                </li>
                
                <li class="nav-item align-items-stretch d-flex dropdown">
                  <a class="nav-link align-self-center dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-expanded="false">
                    Configuration
                  </a>
                  <div class="dropdown-menu">
                    <a class="dropdown-item" href="{{route('all-schools')}}">School Information</a>
                    <a class="dropdown-item" href="{{route('all-offer-courses')}}">Offer Courses</a>
                    <a class="dropdown-item" href="{{route('all-applies')}}">Online Applies</a>
                    <a class="dropdown-item" href="{{url('/signup')}}">Sign Up</a>
                    <a class="dropdown-item" href="{{route('all-website-configuration')}}">Website Configuration</a>
                  </div>
                </li>
              </ul>
            </div>
        </nav>
        <div class="align-self-lg-center col-auto header-info ml-auto">
          <ul class="align-items-center flex-row ml-auto ml-md-0 navbar-nav">
            <li class="nav-item dropdown msg-dropdown">
                <a class="nav-link" href="#" role="button" data-toggle="dropdown" aria-expanded="false">
                  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M160 368c26.5 0 48 21.5 48 48l0 16 72.5-54.4c8.3-6.2 18.4-9.6 28.8-9.6L448 368c8.8 0 16-7.2 16-16l0-288c0-8.8-7.2-16-16-16L64 48c-8.8 0-16 7.2-16 16l0 288c0 8.8 7.2 16 16 16l96 0zm48 124l-.2 .2-5.1 3.8-17.1 12.8c-4.8 3.6-11.3 4.2-16.8 1.5s-8.8-8.2-8.8-14.3l0-21.3 0-6.4 0-.3 0-4 0-48-48 0-48 0c-35.3 0-64-28.7-64-64L0 64C0 28.7 28.7 0 64 0L448 0c35.3 0 64 28.7 64 64l0 288c0 35.3-28.7 64-64 64l-138.7 0L208 492z"/></svg>
                </a>
                <div class="dropdown-menu dropdown-menu-right">
                  <div class="item-header">
                      <h6 class="item-title">05 Message</h6>
                  </div>
                  <div class="item-content">
                      <div class="media">
                          <div class="item-img bg-skyblue author-online">
                              <img src="{{asset('public/assets/img/admin.jpg')}}" alt="img">
                          </div>
                          <div class="media-body space-sm">
                              <div class="item-title">
                                  <a href="#" contenteditable="false" style="cursor: pointer;">
                                      <span class="item-name">Maria Zaman</span> 
                                      <span class="item-time">18:30</span> 
                                  </a>  
                              </div>
                              <p>What is the reason of buy this item. 
                              Is it usefull for me.....</p>
                          </div>
                      </div>
                      <div class="media">
                          <div class="item-img bg-yellow author-online">
                              <img src="{{asset('public/assets/img/admin.jpg')}}" alt="img">
                          </div>
                          <div class="media-body space-sm">
                              <div class="item-title">
                                  <a href="#" contenteditable="false" style="cursor: pointer;">
                                      <span class="item-name">Benny Roy</span> 
                                      <span class="item-time">10:35</span> 
                                  </a>  
                              </div>
                              <p>What is the reason of buy this item. 
                              Is it usefull for me.....</p>
                          </div>
                      </div>
                      <div class="media">
                          <div class="item-img bg-pink">
                              <img src="{{asset('public/assets/img/admin.jpg')}}" alt="img">
                          </div>
                          <div class="media-body space-sm">
                              <div class="item-title">
                                  <a href="#" contenteditable="false" style="cursor: pointer;">
                                      <span class="item-name">Steven</span> 
                                      <span class="item-time">02:35</span> 
                                  </a>  
                              </div>
                              <p>What is the reason of buy this item. 
                              Is it usefull for me.....</p>
                          </div>
                      </div>
                      <div class="media">
                          <div class="item-img bg-violet-blue">
                              <img src="{{asset('public/assets/img/admin.jpg')}}" alt="img">
                          </div>
                          <div class="media-body space-sm">
                              <div class="item-title">
                                  <a href="#" contenteditable="false" style="cursor: pointer;">
                                      <span class="item-name">Joshep Joe</span> 
                                      <span class="item-time">12:35</span> 
                                  </a>  
                              </div>
                              <p>What is the reason of buy this item. 
                              Is it usefull for me.....</p>
                          </div>
                      </div>
                  </div>
              </div>
            </li>
            <li class="nav-item dropdown ml-3 ntf-dropdown">
                <a class="nav-link" href="#" role="button" data-toggle="dropdown" aria-expanded="false"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path d="M224 0c-17.7 0-32 14.3-32 32l0 19.2C119 66 64 130.6 64 208l0 25.4c0 45.4-15.5 89.5-43.8 124.9L5.3 377c-5.8 7.2-6.9 17.1-2.9 25.4S14.8 416 24 416l400 0c9.2 0 17.6-5.3 21.6-13.6s2.9-18.2-2.9-25.4l-14.9-18.6C399.5 322.9 384 278.8 384 233.4l0-25.4c0-77.4-55-142-128-156.8L256 32c0-17.7-14.3-32-32-32zm0 96c61.9 0 112 50.1 112 112l0 25.4c0 47.9 13.9 94.6 39.7 134.6L72.3 368C98.1 328 112 281.3 112 233.4l0-25.4c0-61.9 50.1-112 112-112zm64 352l-64 0-64 0c0 17 6.7 33.3 18.7 45.3s28.3 18.7 45.3 18.7s33.3-6.7 45.3-18.7s18.7-28.3 18.7-45.3z"/></svg>
                </a>
                <div class="dropdown-menu dropdown-menu-right">
                  <div class="item-header">
                      <h6 class="item-title">03 Notifiacations</h6>
                  </div>
                  <div class="item-content">
                      <div class="media">
                          <div class="item-icon bg-skyblue">
                              <i class="fas fa-check"></i>
                          </div>
                          <div class="media-body space-sm">
                              <div class="post-title">Complete Today Task</div>
                              <span>1 Mins ago</span>
                          </div>
                      </div>
                      <div class="media">
                          <div class="item-icon bg-orange">
                              <i class="fas fa-calendar-alt"></i>
                          </div>
                          <div class="media-body space-sm">
                              <div class="post-title">Director Metting</div>
                              <span>20 Mins ago</span>
                          </div>
                      </div>
                      <div class="media">
                          <div class="item-icon bg-violet-blue">
                              <i class="fas fa-cogs"></i>
                          </div>
                          <div class="media-body space-sm">
                              <div class="post-title">Update Password</div>
                              <span>45 Mins ago</span>
                          </div>
                      </div>
                  </div>
              </div>
            </li>
            <li class="nav-item dropdown ml-3 user-profile">
                <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-expanded="false">
                  <img src="{{asset('public/assets/img/admin.jpg')}}" alt="" class="rounded-circle">
                </a>
                <div class="dropdown-menu dropdown-menu-right">
                  <div class="item-header">
                      <h6 class="item-title">{{Auth::user()->name}}</h6>
                  </div>
                  <div class="item-content">
                      <ul class="settings-list">
                          <li><a href="{{route('update-profile')}}"><i class="flaticon-user"></i>My Profile</a></li>
                          <li><a href="#"><i class="flaticon-list"></i>Task</a></li>
                          <li><a href="#"><i class="flaticon-chat-comment-oval-speech-bubble-with-text-lines"></i>Message</a></li>
                          <li><a href="#"><i class="flaticon-gear-loading"></i>Account Settings</a></li>
                          <div class="dropdown-divider"></div>
                          <li><a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();"><i class="flaticon-turn-off"></i>Log Out</a></li>
                      </ul>
                  </div>
                    {{-- <a class="dropdown-item" href="{{route('update-profile')}}">Update Profile</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="{{ route('logout') }}"  onclick="event.preventDefault();document.getElementById('logout-form').submit();">Logout</a> --}}
                </div>
            </li>
        </ul>
        </div>
    </div>
</header>
{{-- <nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark d-none">
            <a class="navbar-brand" href="index.html">{{Auth::user()->name}}</a>
            <button class="btn btn-link btn-sm order-1 order-lg-0" id="sidebarToggle" href="#"><i class="fas fa-bars"></i></button>
            <ul class="navbar-nav ml-auto ml-md-0">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" id="userDropdown" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-user fa-fw"></i></a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                        <a class="dropdown-item" href="{{route('update-profile')}}">Update Profile</a>
                       
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="{{ route('logout') }}"  onclick="event.preventDefault();document.getElementById('logout-form').submit();">Logout</a>
                    </div>
                </li>
            </ul>
        </nav> --}}

        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        @csrf            
        </form>