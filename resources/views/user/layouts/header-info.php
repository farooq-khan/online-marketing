<div class="align-self-lg-center col-auto header-info ml-auto">
              <ul class="align-items-center flex-row ml-auto ml-md-0 navbar-nav">
                <li class="nav-item dropdown msg-dropdown">
                    <a class="nav-link" href="#" role="button" data-toggle="dropdown" aria-expanded="false">
                      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M160 368c26.5 0 48 21.5 48 48l0 16 72.5-54.4c8.3-6.2 18.4-9.6 28.8-9.6L448 368c8.8 0 16-7.2 16-16l0-288c0-8.8-7.2-16-16-16L64 48c-8.8 0-16 7.2-16 16l0 288c0 8.8 7.2 16 16 16l96 0zm48 124l-.2 .2-5.1 3.8-17.1 12.8c-4.8 3.6-11.3 4.2-16.8 1.5s-8.8-8.2-8.8-14.3l0-21.3 0-6.4 0-.3 0-4 0-48-48 0-48 0c-35.3 0-64-28.7-64-64L0 64C0 28.7 28.7 0 64 0L448 0c35.3 0 64 28.7 64 64l0 288c0 35.3-28.7 64-64 64l-138.7 0L208 492z"></path></svg>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" style="">
                      <div class="item-header">
                          <h6 class="item-title">05 Message</h6>
                      </div>
                      <div class="item-content">
                          <div class="media">
                              <div class="item-img bg-skyblue author-online">
                                  <img src="http://localhost/online-marketing/public/assets/img/admin.jpg" alt="img">
                              </div>
                              <div class="media-body space-sm">
                                  <div class="item-title">
                                      <a href="#" contenteditable="false" style="cursor: pointer;">
                                          <span class="item-name">Maria Zaman</span> 
                                          <span class="item-time">18:30</span> 
                                      </a>  
                                  </div>
                                  <p>What is the reason of buy this item. 
                                  Is it usefull for me.....</p>
                              </div>
                          </div>
                          <div class="media">
                              <div class="item-img bg-yellow author-online">
                                  <img src="http://localhost/online-marketing/public/assets/img/admin.jpg" alt="img">
                              </div>
                              <div class="media-body space-sm">
                                  <div class="item-title">
                                      <a href="#" contenteditable="false" style="cursor: pointer;">
                                          <span class="item-name">Benny Roy</span> 
                                          <span class="item-time">10:35</span> 
                                      </a>  
                                  </div>
                                  <p>What is the reason of buy this item. 
                                  Is it usefull for me.....</p>
                              </div>
                          </div>
                          <div class="media">
                              <div class="item-img bg-pink">
                                  <img src="http://localhost/online-marketing/public/assets/img/admin.jpg" alt="img">
                              </div>
                              <div class="media-body space-sm">
                                  <div class="item-title">
                                      <a href="#" contenteditable="false" style="cursor: pointer;">
                                          <span class="item-name">Steven</span> 
                                          <span class="item-time">02:35</span> 
                                      </a>  
                                  </div>
                                  <p>What is the reason of buy this item. 
                                  Is it usefull for me.....</p>
                              </div>
                          </div>
                          <div class="media">
                              <div class="item-img bg-violet-blue">
                                  <img src="http://localhost/online-marketing/public/assets/img/admin.jpg" alt="img">
                              </div>
                              <div class="media-body space-sm">
                                  <div class="item-title">
                                      <a href="#" contenteditable="false" style="cursor: pointer;">
                                          <span class="item-name">Joshep Joe</span> 
                                          <span class="item-time">12:35</span> 
                                      </a>  
                                  </div>
                                  <p>What is the reason of buy this item. 
                                  Is it usefull for me.....</p>
                              </div>
                          </div>
                      </div>
                  </div>
                </li>
                <li class="nav-item dropdown ml-md-3 ml-2 ntf-dropdown">
                    <a class="nav-link" href="#" role="button" data-toggle="dropdown" aria-expanded="false"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path d="M224 0c-17.7 0-32 14.3-32 32l0 19.2C119 66 64 130.6 64 208l0 25.4c0 45.4-15.5 89.5-43.8 124.9L5.3 377c-5.8 7.2-6.9 17.1-2.9 25.4S14.8 416 24 416l400 0c9.2 0 17.6-5.3 21.6-13.6s2.9-18.2-2.9-25.4l-14.9-18.6C399.5 322.9 384 278.8 384 233.4l0-25.4c0-77.4-55-142-128-156.8L256 32c0-17.7-14.3-32-32-32zm0 96c61.9 0 112 50.1 112 112l0 25.4c0 47.9 13.9 94.6 39.7 134.6L72.3 368C98.1 328 112 281.3 112 233.4l0-25.4c0-61.9 50.1-112 112-112zm64 352l-64 0-64 0c0 17 6.7 33.3 18.7 45.3s28.3 18.7 45.3 18.7s33.3-6.7 45.3-18.7s18.7-28.3 18.7-45.3z"></path></svg>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right">
                      <div class="item-header">
                          <h6 class="item-title">03 Notifiacations</h6>
                      </div>
                      <div class="item-content">
                          <div class="media">
                              <div class="item-icon bg-skyblue">
                                  <i class="fas fa-check"></i>
                              </div>
                              <div class="media-body space-sm">
                                  <div class="post-title">Complete Today Task</div>
                                  <span>1 Mins ago</span>
                              </div>
                          </div>
                          <div class="media">
                              <div class="item-icon bg-orange">
                                  <i class="fas fa-calendar-alt"></i>
                              </div>
                              <div class="media-body space-sm">
                                  <div class="post-title">Director Metting</div>
                                  <span>20 Mins ago</span>
                              </div>
                          </div>
                          <div class="media">
                              <div class="item-icon bg-violet-blue">
                                  <i class="fas fa-cogs"></i>
                              </div>
                              <div class="media-body space-sm">
                                  <div class="post-title">Update Password</div>
                                  <span>45 Mins ago</span>
                              </div>
                          </div>
                      </div>
                  </div>
                </li>
                <li class="nav-item dropdown ml-md-3 ml-2 user-profile">
                    <a class="nav-link py-0" href="#" role="button" data-toggle="dropdown" aria-expanded="false">
                      <img src="http://localhost/online-marketing/public/assets/img/admin.jpg" alt="" class="rounded-circle">
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" style="">
                      <div class="item-header">
                          <h6 class="item-title">Sajjad Ahmad</h6>
                      </div>
                      <div class="item-content">
                          <ul class="settings-list">
                              <li><a href="http://localhost/online-marketing/user/update-profile"><i class="flaticon-user"></i>My Profile</a></li>
                              <li><a href="#"><i class="flaticon-list"></i>Task</a></li>
                              <li><a href="#"><i class="flaticon-chat-comment-oval-speech-bubble-with-text-lines"></i>Message</a></li>
                              <li><a href="#"><i class="flaticon-gear-loading"></i>Account Settings</a></li>
                              <div class="dropdown-divider"></div>
                              <li><a href="http://localhost/online-marketing/logout" onclick="event.preventDefault();document.getElementById('logout-form').submit();"><i class="flaticon-turn-off"></i>Log Out</a></li>
                          </ul>
                      </div>
                        
                    </div>
                </li>
            </ul>
            </div>