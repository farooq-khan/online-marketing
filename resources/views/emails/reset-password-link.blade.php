<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Registration Email</title>
    <link href="https://fonts.googleapis.com/css2?family=Rubik:wght@400;500;700&display=swap" rel="stylesheet">


    <style>
    .main-table{box-shadow: 0px 0px 15px rgba(0,0,0,0.1) !important;font-family: 'Rubik', sans-serif;}
</style>
</head>
<body ><table width="640" cellspacing="0" cellpadding="0" border="0" style="margin: 0 auto; border-collapse: collapse;box-shadow: 0px 0px 15px rgba(0,0,0,0.2);border: 1px solid #eee;" class="main-table">
<tr>
  <td style="">
    <table width="100%" cellspacing="0" cellpadding="0" border="0" style="box-shadow: 0px 0px 15px #eee !important;">
      <tr>
        <td style="text-align: center;position: relative;background: #FAFAFA; padding: 10px 0px;">
            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
     viewBox="0 0 155 47" style="enable-background:new 0 0 155 47;" xml:space="preserve" height="75">
<style type="text/css">
    .st0{fill:#6D4A96;}
    .st1{fill:#F9BF1C;}
    .st2{fill:#F7941E;}
    .st3{fill:#02B9FF;}
    .st4{fill:#008CFF;}
    .st5{fill:#956DAF;}
    .st6{fill:#377DFF;}
</style>
<g id="XMLID_759_">
    <g id="XMLID_940_">
        <circle id="XMLID_942_" class="st0" cx="4.2" cy="21.5" r="2.6"/>
        <polygon id="XMLID_941_" class="st0" points="0.3,26.7 0.3,32.4 5.3,29.5 8.4,34.9 11.1,33.4 13.8,31.8 10.7,26.4 15.7,23.5 
            10.7,20.7 8.1,22.1 7.1,26.5 2.9,25.2        "/>
    </g>
    <g id="XMLID_894_">
        <circle id="XMLID_939_" class="st1" cx="11.3" cy="37.8" r="2.6"/>
        <polygon id="XMLID_938_" class="st1" points="13.8,43.8 18.8,46.7 18.8,40.9 25,40.9 25,37.8 25,34.7 18.8,34.7 18.8,28.9 
            13.8,31.8 13.8,34.8 17.1,37.8 13.8,40.8         "/>
    </g>
    <g id="XMLID_769_">
        <circle id="XMLID_893_" class="st2" cx="28.9" cy="39.9" r="2.6"/>
        <polygon id="XMLID_892_" class="st2" points="35.4,40.7 40.4,37.8 35.4,34.9 38.5,29.5 35.8,28 33.1,26.4 30,31.8 25,28.9 
            25,34.7 27.6,36.2 31.8,34.8 32.8,39.2       "/>
    </g>
    <g id="XMLID_766_">
        <circle id="XMLID_768_" class="st3" cx="39.6" cy="25.6" r="2.6"/>
        <polygon id="XMLID_767_" class="st3" points="43.5,20.4 43.5,14.6 38.5,17.5 35.4,12.1 32.7,13.7 30,15.3 33.1,20.7 28.1,23.5 
            33.1,26.4 35.7,24.9 36.6,20.6 40.9,21.9         "/>
    </g>
    <g id="XMLID_763_">
        <circle id="XMLID_765_" class="st4" cx="32.5" cy="9.3" r="2.6"/>
        <polygon id="XMLID_764_" class="st4" points="30,3.3 25,0.4 25,6.1 18.8,6.1 18.8,9.3 18.8,12.4 25,12.4 25,18.2 30,15.3 30,12.3 
            26.7,9.3 30,6.2         "/>
    </g>
    <g id="XMLID_760_">
        <circle id="XMLID_762_" class="st5" cx="14.8" cy="7.2" r="2.6"/>
        <polygon id="XMLID_761_" class="st5" points="8.4,6.4 3.4,9.3 8.4,12.1 5.3,17.5 8,19.1 10.7,20.7 13.8,15.3 18.8,18.2 18.8,12.4 
            16.2,10.9 11.9,12.2 10.9,7.9        "/>
    </g>
</g>
<g>
    <path id="XMLID_757_" class="st6" d="M59.1,33.8l3.3-4.6c2,2.1,5.2,3.9,9.1,3.9c3.4,0,5-1.5,5-3.2c0-5.1-16.5-1.6-16.5-12.4
        c0-4.8,4.1-8.7,10.9-8.7c4.6,0,8.4,1.4,11.2,4l-3.4,4.5C76.3,15,73.2,14,70.3,14c-2.6,0-4.1,1.2-4.1,2.9c0,4.6,16.5,1.5,16.5,12.2
        c0,5.3-3.7,9.2-11.5,9.2C65.7,38.4,61.7,36.5,59.1,33.8z"/>
    <path id="XMLID_741_" class="st6" d="M109.6,37.8v-12H96.1v12.1H90V9.1h6.1v11.3h13.6V9.1h6.2v28.7
        C115.9,37.8,109.6,37.8,109.6,37.8z"/>
    <path id="XMLID_680_" class="st6" d="M123.1,23.5c0-9.1,6.9-14.9,15.3-14.9c6,0,9.7,3,11.9,6.4l-5.1,2.8c-1.3-2-3.7-3.7-6.8-3.7
        c-5.2,0-9,4-9,9.4s3.8,9.4,9,9.4c2.5,0,5-1.1,6.1-2.2v-3.4h-7.6V22h13.7v11c-2.9,3.3-7,5.4-12.2,5.4
        C129.9,38.4,123.1,32.6,123.1,23.5z"/>
</g>
</svg>

        </td>
      </tr>
      <tr>
        <td style="padding:10px">
          <div>
            <p>Hello <b>{{ $user->email }}</b>,</p>
            <p>You have requested password reset on SHG!</p>

            <p>Please <a href="{{config('app.frontend_url')}}reset-password/{{$randomToken}}" style="color: #6E00EE !important;"><b>Click Here</b></a> to reset your password</p>
        </div>
        </td>
      </tr>
      <tr>
        <td style="padding: 10px;height: 49px; background-color: #C8FFF4;text-decoration: none !important; text-align: center;">
          <p style="color: #00000099;margin: 0px;">&copy; {{ date('Y') }} SHG</p>
        </td>
      </tr>
    </table>
  </td>
</tr>
</table></body>
</html>