{{-- @extends('layouts.app') --}}
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Best School Management System - All-in-One Solution for Educational Institutions</title>
    <meta name="keywords" content="school management software, education management, student information system, school admin platform, teacher management, school dashboard, school attendance, academic management, online school management">
    <meta name="description" content="Make school management easy with our all-in-one School Management System. Manage attendance, grades, schedules, and communication—all in one platform for smooth school operations and improved student success.">
        
    <!-- Open Graph Meta Tags -->
    <meta property="og:title" content="School Management System - All-in-One Solution for Schools">
    <meta property="og:description" content="Streamline school operations with our all-in-one platform. Track attendance, manage grades, set schedules, and communicate—all in one place to support student success and operational efficiency.">
    <meta property="og:image" content="{{asset('public/assets/img/page-bg.jpg')}}"> <!-- Replace with actual image URL -->
    <meta property="og:url" content="{{ route('fronted-home')}}">
        
    <!-- Twitter Card Meta Tags -->
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="School Management System - All-in-One Solution for Schools">
    <meta name="twitter:description" content="Manage school operations seamlessly with our all-in-one platform. Track attendance, grades, schedules, and communication with ease—boosting student success and operational efficiency.">
    <meta name="twitter:image" content="{{asset('public/assets/img/page-bg.jpg')}}"> <!-- Replace with actual image URL -->

    <!-- Canonical URL -->
    <link rel="canonical" href="{{ route('fronted-home')}}">
    
    <link href="https://fonts.googleapis.com/css2?family=Karla:ital,wght@0,200..800;1,200..800&family=Montserrat:ital,wght@0,100..900;1,100..900&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">
    <link href="{{asset('public/css/bootstrap.css')}}" rel="stylesheet" />
    <link href="{{asset('public/css/frontend.css?')}}{{time()}}" rel="stylesheet" />
    <!-- Google tag (gtag.js) -->
    @include('layouts.google-analytics')
</head>
<body>
@include('frontend.header')

<section class="banner d-flex align-items-center" style='background-image: url("{{asset('public/assets/img/page-bg.jpg')}}"); background-size:cover'>
  <div class="container py-5">
    <div class="col-12 col-lg-8 col-md-9 col-xl-7 px-0">
        <h1 class="mb-4">Better Solutions For Your School & College</h1>
        <p class="mb-4 pb-2">Our software streamlines school administration, automates tasks, and enhances communication, providing tailored solutions for efficient management and reporting.</p>
        @if (!Auth::check())
            <a href="{{ route('get-started') }}" class="btn btn-primary px-4">Register</a>
        @endif
      </div>
     </div>
</section>

<section class="about-us text-center py-5" id="about-us">
  <div class="container py-md-4">
    <div class="row">
      <div class="col-xl-10 col-12 mx-auto">
        <h2 class="mb-4 h1 section-title">About us</h2>
        <p class="sub-heading">Our School Management Software is a comprehensive, end-to-end solution designed for educational institutions of all types. As an online, web-based system, it simplifies and automates routine tasks while providing enhanced control, visibility, and reporting with advanced analytics.</p>
        <p class="sub-heading">Our powerful system integrates management, administration, parents, teachers, and students on a unified platform, reducing administrative workload and improving data consistency. Developed from years of research in the education sector, our software is easily customizable to meet the specific needs of any institution.</p>
        <p class="sub-heading mb-0">We are committed to ongoing development to address future educational trends and ensure our software continues to evolve with the needs of the sector.</p>
        
      </div>
    </div>
  </div>
</section>
<section class="features py-lg-4 py-1 text-center" id="features">
  <div class="container py-5">
    <div class="row">
      <div class="col-xl-10 col-lg-11 col-12 mx-auto feature-col pb-2">
        <h1 class="section-title">Features</h1>
        <p class="sub-heading mb-0">Explore our comprehensive suite of tools designed to streamline school management. From student and teacher administration to fee handling and exam scheduling, our features ensure efficient and effective school operations. Discover how our software enhances productivity and simplifies daily tasks.</p>
      </div>
    </div>
    <div class="mx-lg-n3 mx-n2 row row-cols-1 row-cols-lg-3 row-cols-md-3 row-cols-sm-2 justify-content-center">
      <div class="col feature-col mt-lg-4 mt-3 d-flex pt-5 px-lg-3 px-2">
        <div class="card flex-fill px-3 pb-3 px-lg-4 pb-lg-4">
          <i class="fas fa-2x fa-users mb-3"></i>
          <h4 class="mb-2 pb-1">Students</h4>
          <p class="mb-0">Manages student profiles, academic records, attendance, and progress</p>
        </div>
      </div>
      <div class="col feature-col mt-lg-4 mt-3 d-flex pt-5 px-lg-3 px-2">
        <div class="card flex-fill px-3 pb-3 px-lg-4 pb-lg-4">
          <i class="fas fa-2x fa-chalkboard-teacher mb-3"></i>
          <h4 class="mb-2 pb-1">Teachers</h4>
          <p class="mb-0">Maintains teacher profiles, qualifications, and class assignments</p>
        </div>
      </div>
      <div class="col feature-col mt-lg-4 mt-3 d-flex pt-5 px-lg-3 px-2">
        <div class="card flex-fill px-3 pb-3 px-lg-4 pb-lg-4">
          <i class="fas fa-2x fa-clock mb-3"></i>
          <h4 class="mb-2 pb-1">Attendance</h4>
          <p class="mb-0">Tracks and records daily attendance for students and staff.</p>
        </div>
      </div>
      <div class="col feature-col mt-lg-4 mt-3 d-flex pt-5 px-lg-3 px-2">
        <div class="card flex-fill px-3 pb-3 px-lg-4 pb-lg-4">
          <i class="fas fa-2x fa-money-check-alt mb-3"></i>
          <h4 class="mb-2 pb-1">Fee</h4>
          <p class="mb-0">Handles billing, invoicing, fee collection, and payment tracking.</p>
        </div>
      </div>
      <div class="col feature-col mt-lg-4 mt-3 d-flex pt-5 px-lg-3 px-2">
        <div class="card flex-fill px-3 pb-3 px-lg-4 pb-lg-4">
          <i class="fas fa-2x fa-poll mb-3"></i>
          <h4 class="mb-2 pb-1">Results</h4>
          <p class="mb-0">Manages grades, generates report cards, and analyzes student performance.</p>
        </div>
      </div>
      <div class="col feature-col mt-lg-4 mt-3 d-flex pt-5 px-lg-3 px-2">
        <div class="card flex-fill px-3 pb-3 px-lg-4 pb-lg-4">
          <i class="fas fa-2x fa-school mb-3"></i>
          <h4 class="mb-2 pb-1">Classes</h4>
          <p class="mb-0">Organizes class schedules, teacher assignments, and classroom allocations.</p>
        </div>
      </div>
      <div class="col feature-col mt-lg-4 mt-3 d-flex pt-5 px-lg-3 px-2">
        <div class="card flex-fill px-3 pb-3 px-lg-4 pb-lg-4">
          <i class="fas fa-2x fa-book mb-3"></i>
          <h4 class="mb-2 pb-1">Subjects</h4>
          <p class="mb-0">Manages subjects, curriculum, and syllabus coverage.</p>
        </div>
      </div>
      <div class="col feature-col mt-lg-4 mt-3 d-flex pt-5 px-lg-3 px-2">
        <div class="card flex-fill px-3 pb-3 px-lg-4 pb-lg-4">
          <i class="fas fa-2x fa-pager mb-3"></i>
          <h4 class="mb-2 pb-1">Exams</h4>
          <p class="mb-0">Schedules and administers exams, tracks results, and analyzes performance.</p>
        </div>
      </div>
      <div class="col feature-col mt-lg-4 mt-3 d-flex pt-5 px-lg-3 px-2">
        <div class="card flex-fill px-3 pb-3 px-lg-4 pb-lg-4">
          <i class="fas fa-2x fa-certificate mb-3"></i>
          <h4 class="mb-2 pb-1">Certificates</h4>
          <p class="mb-0">Generates and issues certificates for achievements and completions.</p>
        </div>
      </div>
      <div class="col feature-col mt-lg-4 mt-3 d-flex pt-5 px-lg-3 px-2">
        <div class="card flex-fill px-3 pb-3 px-lg-4 pb-lg-4">
          <i class="fas fa-2x fa-hand-holding-usd mb-3"></i>
          <h4 class="mb-2 pb-1">Expenses</h4>
          <p class="mb-0">Tracks school expenditures, generates expense reports, and aids budget planning.</p>
        </div>
      </div>
      <div class="col feature-col mt-lg-4 mt-3 d-flex pt-5 px-lg-3 px-2">
        <div class="card flex-fill px-3 pb-3 px-lg-4 pb-lg-4">
          <i class="fas fa-2x fa-book mb-3"></i>
          <h4 class="mb-2 pb-1">Book &amp; Library</h4>
          <p class="mb-0">Manages library inventory, check-ins, check-outs, and fines.</p>
        </div>
      </div>
      <div class="col feature-col mt-lg-4 mt-3 d-flex pt-5 px-lg-3 px-2">
        <div class="card flex-fill px-3 pb-3 px-lg-4 pb-lg-4">
          <i class="fas fa-2x fa-cogs mb-3"></i>
          <h4 class="mb-2 pb-1">Configuration</h4>
          <p class="mb-0">Customizes software settings, user roles, and permissions.</p>
        </div>
      </div>
      <div class="col feature-col mt-lg-4 mt-3 d-flex pt-5 px-lg-3 px-2">
        <div class="card flex-fill px-3 pb-3 px-lg-4 pb-lg-4">
          <i class="fas fa-2x fa-users mb-3"></i>
          <h4 class="mb-2 pb-1">Accounts</h4>
          <p class="mb-0">Manages financial accounts, income, expenses, and reporting.</p>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="our-clients py-lg-4 py-1 text-center d-none" id="our-clients">
  <div class="container py-5">
    <div class="row">
      <div class="col-xl-10 col-lg-11 col-12 mx-auto feature-col pb-2">
        <h1 class="section-title">Our Clients</h1>
        <p class="sub-heading mb-0">Meet our happy clients</p>
      </div>
    </div>
    <div class="mx-lg-n3 mx-n2 row row-cols-2 row-cols-lg-3 row-cols-md-3 row-cols-sm-2 justify-content-center">
      <div class="align-items-center d-flex justify-content-center py-ms-3 pt-1 mt-4">
        <img src="https://res.cloudinary.com/dxfq3iotg/image/upload/v1559460149/abof.png" alt="">
      </div>
      <div class="align-items-center d-flex justify-content-center py-ms-3 pt-1 mt-4">
        <img src="https://res.cloudinary.com/dxfq3iotg/image/upload/v1559460149/abof.png" alt="">
      </div>
      <div class="align-items-center d-flex justify-content-center py-ms-3 pt-1 mt-4">
        <img src="https://res.cloudinary.com/dxfq3iotg/image/upload/v1559460149/abof.png" alt="">
      </div>
      <div class="align-items-center d-flex justify-content-center py-ms-3 pt-1 mt-4">
        <img src="https://res.cloudinary.com/dxfq3iotg/image/upload/v1559460149/abof.png" alt="">
      </div>
      <div class="align-items-center d-flex justify-content-center py-ms-3 pt-1 mt-4">
        <img src="https://res.cloudinary.com/dxfq3iotg/image/upload/v1559460149/abof.png" alt="">
      </div>
      <div class="align-items-center d-flex justify-content-center py-ms-3 pt-1 mt-4">
        <img src="https://res.cloudinary.com/dxfq3iotg/image/upload/v1559460149/abof.png" alt="">
      </div>
    </div>
  </div>
</section>
<section class="our-customers py-1 py-lg-4 text-center d-none" id="testimonials">
  <div class="container py-5">
    <div class="row">
      <div class="col-xl-10 col-lg-11 col-12 mx-auto feature-col pb-2">
        <h1 class="section-title">Customer Testimonials</h1>
        <p class="sub-heading mb-0">Hear from our satisfied customers and read their positive reviews.</p>
      </div>
    </div>
    <div class="row mt-4">
      <div class="col-xl-9 col-lg-10 col-12 mx-auto">
        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
          <div class="carousel-inner">
            <div class="carousel-item active">
              <div class="customer">
                <img src="{{asset('public/assets/img/customer-profile-img-1.jpeg')}}" class="img-fluid rounded-circle mx-auto mb-4" style="max-width: 130px;" alt="image">
                <h6 class="h-5 mb-3 font-weight-bold">Farooq Khan</h6>
                <p class="mb-0">Lorem ipsum dolor sit amet consectetur adipisicing elit. Quidem cum ab assumenda, deserunt, excepturi nam corporis quas ipsam earum placeat corrupti ex voluptate consequuntur quisquam asperiores nisi fugiat commodi. Sunt.</p>
              </div>
            </div>
            <div class="carousel-item">
              <div class="customer">
                <img src="{{asset('public/assets/img/customer-profile-img-1.jpeg')}}" class="img-fluid rounded-circle mx-auto mb-4" style="max-width: 130px;" alt="image">
                <h6 class="h-5 mb-3 font-weight-bold">Farooq Khan</h6>
                <p class="mb-0">Lorem ipsum dolor sit amet consectetur adipisicing elit. Quidem cum ab assumenda, deserunt, excepturi nam corporis quas ipsam earum placeat corrupti ex voluptate consequuntur quisquam asperiores nisi fugiat commodi. Sunt.</p>
              </div>
            </div>
            <div class="carousel-item">
              <div class="customer">
                <img src="{{asset('public/assets/img/customer-profile-img-1.jpeg')}}" class="img-fluid rounded-circle mx-auto mb-4" style="max-width: 130px;" alt="image">
                <h6 class="h-5 mb-3 font-weight-bold">Farooq Khan</h6>
                <p class="mb-0">Lorem ipsum dolor sit amet consectetur adipisicing elit. Quidem cum ab assumenda, deserunt, excepturi nam corporis quas ipsam earum placeat corrupti ex voluptate consequuntur quisquam asperiores nisi fugiat commodi. Sunt.</p>
              </div>
            </div>
          </div>
          <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon fa fa-2x fa-chevron-left text-dark" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
            <span class="carousel-control-next-icon fa fa-2x fa-chevron-right text-dark" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
      </div>
    </div>
  </div>
</section>

{{-- <div class="container text-center py-5">
  <a href="#" class="btn btn-primary px-4 py-2">Request a Live Demo</a>
</div> --}}



@include('frontend.footer')





</body>
</html>

