<!doctype html>
<html lang="en">
<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="_token" content="{{csrf_token()}}" />
<title>Tech4U</title>
 <!-- Icons -->
        <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
        <link rel="shortcut icon" href="assets/media/favicons/favicon.png">
        <link rel="icon" type="image/png" sizes="192x192" href="assets/media/favicons/favicon-192x192.png">
        <link rel="apple-touch-icon" sizes="180x180" href="assets/media/favicons/apple-touch-icon-180x180.png">
        <!-- END Icons -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        

        <!-- Stylesheets -->
        <!-- Fonts and Codebase framework -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito+Sans:300,400,400i,600,700&display=swap">
        <!-- You can include a specific file from css/themes/ folder to alter the default color theme of the template. eg: -->
        <!-- <link rel="stylesheet" id="css-theme" href="assets/css/themes/flat.min.css"> -->
        <!-- END Stylesheets -->

 <!-- Icons font CSS-->
    <link href="{{asset('public/vendor/mdi-font/css/material-design-iconic-font.min.css')}}" rel="stylesheet" media="all">
    <link href="{{asset('public/vendor/font-awesome-4.7/css/font-awesome.min.css')}}" rel="stylesheet" media="all">
    <!-- Font special for pages-->
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link rel="stylesheet" id="css-main" href="{{asset('public/css/codebase.min.css')}}">

    <!-- Vendor CSS-->
    <link href="{{asset('public/vendor/select2/select2.min.css')}}" rel="stylesheet" media="all">
    <link href="{{asset('public/vendor/datepicker/daterangepicker.css')}}" rel="stylesheet" media="all">
    <link href="{{asset('public/css/style.css')}}" rel="stylesheet" type="text/css">
    <!-- Main CSS-->
    {{-- <link href="{{asset('public/css/main.css')}}" rel="stylesheet" media="all"> --}}

    {{-- Toastr Plugin --}}
        <link href="{{asset('public/frontend/css/toastr.min.css')}}" rel="stylesheet">

        <style type="text/css">
          #page-header
          {
            background-color: #647484 !important;
          }
          #page-footer p, #page-footer h4
          {
            color: white !important;
          }
        </style>
</head>
<body>
<div id="page-container" class="sidebar-inverse side-scroll page-header-fixed page-header-glass page-header-inverse main-content-boxed enable-page-overlay">
 {{-- header code --}}
  @include('layouts.header')
  @yield('content')

<!-- Footer -->
<footer id="page-footer" class="text-white opacity-0" style="background-color: #343a40;">
  <div class="content content-full">
      <!-- Footer Navigation -->
     {{--  <div class="row items-push-2x mt-30">
          <div class="col-6 col-md-4">
              <h3 class="h5 font-w700">Heading</h3>
              <ul class="list list-simple-mini font-size-sm">
                  <li>
                      <a class="link-effect font-w600" href="javascript:void(0)">Link #1</a>
                  </li>
                  <li>
                      <a class="link-effect font-w600" href="javascript:void(0)">Link #2</a>
                  </li>
                  <li>
                      <a class="link-effect font-w600" href="javascript:void(0)">Link #3</a>
                  </li>
                  <li>
                      <a class="link-effect font-w600" href="javascript:void(0)">Link #4</a>
                  </li>
                  <li>
                      <a class="link-effect font-w600" href="javascript:void(0)">Link #5</a>
                  </li>
                  <li>
                      <a class="link-effect font-w600" href="javascript:void(0)">Link #6</a>
                  </li>
              </ul>
          </div>
          <div class="col-6 col-md-4">
              <h3 class="h5 font-w700">Heading</h3>
              <ul class="list list-simple-mini font-size-sm">
                  <li>
                      <a class="link-effect font-w600" href="javascript:void(0)">Link #1</a>
                  </li>
                  <li>
                      <a class="link-effect font-w600" href="javascript:void(0)">Link #2</a>
                  </li>
                  <li>
                      <a class="link-effect font-w600" href="javascript:void(0)">Link #3</a>
                  </li>
                  <li>
                      <a class="link-effect font-w600" href="javascript:void(0)">Link #4</a>
                  </li>
                  <li>
                      <a class="link-effect font-w600" href="javascript:void(0)">Link #5</a>
                  </li>
                  <li>
                      <a class="link-effect font-w600" href="javascript:void(0)">Link #6</a>
                  </li>
              </ul>
          </div>
          <div class="col-md-4">
              <h3 class="h5 font-w700">Company LTD</h3>
              <div class="font-size-sm mb-30">
                  1080 Sunshine Valley, Suite 2563<br>
                  San Francisco, CA 85214<br>
                  <abbr title="Phone">P:</abbr> (123) 456-7890
              </div>
              <h3 class="h5 font-w700">Our Newsletter</h3>
              <form>
                  <div class="input-group">
                      <input type="email" class="form-control" id="ld-subscribe-email" name="ld-subscribe-email" placeholder="Your email..">
                      <div class="input-group-append">
                          <button type="submit" class="btn btn-secondary">Subscribe</button>
                      </div>
                  </div>
              </form>
          </div>
      </div> --}}
      <!-- END Footer Navigation -->
      <div class="row">
        <div class="col">
          <h4 class="m-0"><i class="zmdi zmdi-phone"></i> Phone</h4>
          <p>0308-8551251</p>
        </div>

        <div class="col">
          <h4 class="m-0"><i class="zmdi zmdi-email"></i> Email</h4>
          <p>afzalkhan8338@gmail.com</p>
        </div>

        <div class="col">
          <h4 class="m-0"><i class="zmdi zmdi-pin"></i> Address</h4>
          <p>Tech4U Computer Institute Near Speen Jumat Ashkhel Landikotal Khyber Agency</p>
        </div>

        <div class="col">
          <h4 class="m-0"> Affiliation & Accreditations</h4>
          <p>Tech4U institute has been registered from KPK Technical Board. The Institute surving the market demands since last 10 year and reverse the privilege to have its students in variety of private and government firms at both national and international level.
          </p>
        </div>

        <div class="col">
          <h4 class="m-0"> Follow Us</h4>
          <span class=""><a target="_blank" href="https://www.facebook.com/tech4uc/" class="fa fa-facebook-official p-2 rounded" style="color: #3b5998;font-size: 24px;"></a></span>
          <span class=""><a class="zmdi zmdi-linkedin-box p-2 rounded" href="https://www.linkedin.com/company/tech4uc" style="color: #0e76a8;font-size: 24px;"></a></span>
          <span class=""><a class="fa fa-youtube-play p-2 rounded" href="https://www.youtube.com/channel/UC0DxmHdvJixEpzokmE0A-bg" style="color: red;font-size: 24px;"></a></span>
          <span class=""><i class="fa fa-google-plus p-2 rounded" style="font-size: 24px;"></i></span>
          <span class=""><i class="fa fa-instagram p-2 rounded" style="font-size: 24px;color: #3f729b"></i></span>
        </div>
      </div>
      <!-- Copyright Info -->
      <div class="font-size-sm clearfix border-t pt-20 pb-10">
          {{-- <div class="float-right">
              Crafted with <i class="fa fa-heart text-pulse"></i> by <a class="font-w600" href="https://1.envato.market/ydb" target="_blank">pixelcave</a>
          </div> --}}
          <div class="float-left">
              <a class="font-w600" href="javascript:void(0)" target="_blank"></a> &copy; <span class="js-year-copy"></span><span> All Rights Reserved By Tech4U Institute</span>
          </div>
      </div>
      <!-- END Copyright Info -->
  </div>
</footer>
<!-- END Footer -->
</div>

<!-- Loader Modal -->
<div class="modal" id="loader_modal" role="dialog">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-body">
        <h3 style="text-align:center;">Please wait</h3>
        <p style="text-align:center;"><img src="{{ asset('public/uploads/gif/waiting.gif') }}"></p>
      </div>
    </div>
  </div>
</div>

{{-- Toastr Plugin --}}
<script src="{{asset('public/frontend/js/jquery.min.js')}}"></script>
<script src="{{asset('public/frontend/js/toastr.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script src="{{asset('public/vendor/select2/select2.min.js')}}"></script>
<script src="{{asset('public/vendor/datepicker/moment.min.js')}}"></script>
<script src="{{asset('public/vendor/datepicker/daterangepicker.js')}}"></script>
<script src="{{asset('public/js/script.js')}}"></script>

<!-- Login main js-->
<script src="{{asset('public/js/global.js')}}"></script>

<script type="text/javascript">
</script>
@yield('javascript')
  <!-- Codebase JS -->
<script src="{{asset('public/frontend/js/codebase.core.min.js')}}"></script>
<script src="{{asset('public/frontend/js/codebase.app.min.js')}}"></script>
</body>
</html>