@if (app()->environment('production'))
  <script async src="https://www.googletagmanager.com/gtag/js?id=G-1HE2QY24QX"></script>
  <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
      gtag('config', 'G-1HE2QY24QX');
  </script>
@endif