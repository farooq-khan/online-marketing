@extends('user.layouts.layout')

@section('title','Online Marketing')
<style type="text/css">
    .my-icons
{
  font-size: 34px;
  margin-right: 15px;
}
</style>
<?php 
    use Carbon\Carbon;
    $total_received = 0;
    $total_paid = 0;
    $total_income_of_year = 0;
    foreach($months_names as $mon){
        // $total_ = '';
        ${$mon} = 0;
    }
?>
@section('content')

                    <div>
                        <div class="row page-title-row">
                            <div class="col-12">
                                <h1 class="page-title">Admin Dashboard</h1>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-xl-3 col-md-6 status-card">
                                <div class="card active-students mb-4 custom__shadow">

                                    <div class="card-body">
                                        <div class="d-flex justify-content-between align-items-center mb-2">
                                            <div class="status-icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" height="24px" viewBox="0 -960 960 960" width="24px" fill="#17a2b8"><path d="M40-160v-112q0-34 17.5-62.5T104-378q62-31 126-46.5T360-440q66 0 130 15.5T616-378q29 15 46.5 43.5T680-272v112H40Zm720 0v-120q0-44-24.5-84.5T666-434q51 6 96 20.5t84 35.5q36 20 55 44.5t19 53.5v120H760ZM360-480q-66 0-113-47t-47-113q0-66 47-113t113-47q66 0 113 47t47 113q0 66-47 113t-113 47Zm400-160q0 66-47 113t-113 47q-11 0-28-2.5t-28-5.5q27-32 41.5-71t14.5-81q0-42-14.5-81T544-792q14-5 28-6.5t28-1.5q66 0 113 47t47 113ZM120-240h480v-32q0-11-5.5-20T580-306q-54-27-109-40.5T360-360q-56 0-111 13.5T140-306q-9 5-14.5 14t-5.5 20v32Zm240-320q33 0 56.5-23.5T440-640q0-33-23.5-56.5T360-720q-33 0-56.5 23.5T280-640q0 33 23.5 56.5T360-560Zm0 320Zm0-400Z"/></svg></div>
                                            <div class="status-count"><a class="stretched-link" href="javascript:void(0)"><b>{{number_format($students_count,0,'.',',')}}</b></a></div>
                                        </div>
                                        <div class="status-title-name"><b>Active Students</b></div>
                                        
                                        </div>

                                </div>
                            </div>

                          <!--   <div class="col-xl-3 col-md-6">
                                <div class="card bg-primary mb-4">
                                    <div class="card-body"><b><i class="fas fa-users my-icons"></i> Suspended Students</b></div>
                                    <div class="card-footer d-flex align-items-center justify-content-between">
                                        <a class="small stretched-link" href="javascript:void(0)"><b>{{number_format($suspended_students,0,'.',',')}}</b></a>
                                        {{-- <div class="small"><i class="fas fa-angle-right"></i></div> --}}
                                    </div>
                                </div>
                            </div>

                            <div class="col-xl-3 col-md-6">
                                <div class="card bg-primary mb-4">
                                    <div class="card-body"><b><i class="fas fa-users my-icons"></i> Pass Out Students</b></div>
                                    <div class="card-footer d-flex align-items-center justify-content-between">
                                        <a class="small stretched-link" href="javascript:void(0)"><b>{{number_format($graduated_students,0,'.',',')}}</b></a>
                                        {{-- <div class="small"><i class="fas fa-angle-right"></i></div> --}}
                                    </div>
                                </div>
                            </div> -->
                            <div class="col-xl-3 col-md-6 status-card">
                                <div class="card suspended-students mb-4 custom__shadow">
                                    <div class="card-body">
                                        <div class="d-flex justify-content-between align-items-center mb-2">
                                            <div class="status-icon"><svg xmlns="http://www.w3.org/2000/svg" height="24px" viewBox="0 -960 960 960" width="24px" fill="#ff5567"><path d="M500-482q29-32 44.5-73t15.5-85q0-44-15.5-85T500-798q60 8 100 53t40 105q0 60-40 105t-100 53Zm220 322v-120q0-36-16-68.5T662-406q51 18 94.5 46.5T800-280v120h-80Zm240-360H720v-80h240v80Zm-640 40q-66 0-113-47t-47-113q0-66 47-113t113-47q66 0 113 47t47 113q0 66-47 113t-113 47ZM0-160v-112q0-34 17.5-62.5T64-378q62-31 126-46.5T320-440q66 0 130 15.5T576-378q29 15 46.5 43.5T640-272v112H0Zm320-400q33 0 56.5-23.5T400-640q0-33-23.5-56.5T320-720q-33 0-56.5 23.5T240-640q0 33 23.5 56.5T320-560ZM80-240h480v-32q0-11-5.5-20T540-306q-54-27-109-40.5T320-360q-56 0-111 13.5T100-306q-9 5-14.5 14T80-272v32Zm240-400Zm0 400Z"/></svg></div>
                                            <div class="status-count"><a class="stretched-link" href="javascript:void(0)"><b>{{number_format($suspended_students,0,'.',',')}}</b></a></div>
                                        </div>
                                        <div class="status-title-name"><b>Suspended Students</b></div>
                                        </div>
                                    {{-- <div class="card-footer d-flex align-items-center justify-content-between">
                                        <a class="small stretched-link" href="javascript:void(0)"><b>{{number_format($suspended_students,0,'.',',')}}</b></a>
                                    </div> --}}
                                </div>
                            </div>
                           <!--  <div class="col-xl-3 col-md-6">
                                <div class="card bg-warning mb-4">
                                    <div class="card-body"><b><i class="fas fa-money-check-alt"></i> Total Fee ({{$start}} - {{$end}})</b></div>
                                    <div class="card-footer d-flex align-items-center justify-content-between">
                                        <a class="small stretched-link" href="javascript:void(0)"><b>{{number_format($previous_month_fee + $previous_month_fee_unpaid,2,'.',',')}}</b></a>
                                        {{-- <div class="small"><i class="fas fa-angle-right"></i></div> --}}
                                    </div>
                                </div>
                            </div> -->
                            <div class="col-xl-3 col-md-6 status-card">
                                <div class="card pass-out-students mb-4 custom__shadow">
                                    <div class="card-body">
                                        <div class="d-flex justify-content-between align-items-center mb-2">
                                            <div class="status-icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" height="24px" viewBox="0 -960 960 960" width="24px" fill="#008f00"><path d="M702-480 560-622l57-56 85 85 170-170 56 57-226 226Zm-342 0q-66 0-113-47t-47-113q0-66 47-113t113-47q66 0 113 47t47 113q0 66-47 113t-113 47ZM40-160v-112q0-34 17.5-62.5T104-378q62-31 126-46.5T360-440q66 0 130 15.5T616-378q29 15 46.5 43.5T680-272v112H40Zm80-80h480v-32q0-11-5.5-20T580-306q-54-27-109-40.5T360-360q-56 0-111 13.5T140-306q-9 5-14.5 14t-5.5 20v32Zm240-320q33 0 56.5-23.5T440-640q0-33-23.5-56.5T360-720q-33 0-56.5 23.5T280-640q0 33 23.5 56.5T360-560Zm0 260Zm0-340Z"/></svg></div>
                                            <div class="status-count"><a class="stretched-link" href="javascript:void(0)"><b>{{number_format($graduated_students,0,'.',',')}}</b></a></div>
                                        </div>
                                        <div class="status-title-name"><b>Pass Out Students</b></div>
                                        </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-6 status-card">
                                <div class="card total-registered-students mb-4 custom__shadow">
                                    <div class="card-body">
                                        <div class="d-flex justify-content-between align-items-center mb-2">
                                            <div class="status-icon"><svg xmlns="http://www.w3.org/2000/svg" height="24px" viewBox="0 -960 960 960" width="24px" fill="#123df7"><path d="M500-482q29-32 44.5-73t15.5-85q0-44-15.5-85T500-798q60 8 100 53t40 105q0 60-40 105t-100 53Zm220 322v-120q0-36-16-68.5T662-406q51 18 94.5 46.5T800-280v120h-80Zm80-280v-80h-80v-80h80v-80h80v80h80v80h-80v80h-80Zm-480-40q-66 0-113-47t-47-113q0-66 47-113t113-47q66 0 113 47t47 113q0 66-47 113t-113 47ZM0-160v-112q0-34 17.5-62.5T64-378q62-31 126-46.5T320-440q66 0 130 15.5T576-378q29 15 46.5 43.5T640-272v112H0Zm320-400q33 0 56.5-23.5T400-640q0-33-23.5-56.5T320-720q-33 0-56.5 23.5T240-640q0 33 23.5 56.5T320-560ZM80-240h480v-32q0-11-5.5-20T540-306q-54-27-109-40.5T320-360q-56 0-111 13.5T100-306q-9 5-14.5 14T80-272v32Zm240-400Zm0 400Z"/></svg></div>
                                            <div class="status-count"><a class="stretched-link" href="javascript:void(0)"><b>{{number_format($graduated_students+$suspended_students+$students_count,0,'.',',')}}</b></a></div>
                                        </div>
                                        <div class="status-title-name"><b>Total Registered Students</b></div>
                                </div>
                            </div>
                            </div>
                            <!-- <div class="col-xl-3 col-md-6">
                                <div class="card bg-success mb-4">
                                    <div class="card-body"><b><i class="fas fa-money-check-alt"></i> Submitted Fee ({{$start}} - {{$end}})</b></div>
                                    <div class="card-footer d-flex align-items-center justify-content-between">
                                        <a class="small stretched-link" href="javascript:void(0)"><b>{{number_format($previous_month_fee,2,'.',',')}}</b></a>
                                        {{-- <div class="small"><i class="fas fa-angle-right"></i></div> --}}
                                    </div>
                                </div>
                            </div> -->
                            <!-- <div class="col-xl-3 col-md-6">
                                <div class="card bg-danger mb-4">
                                    <div class="card-body"><b><i class="fas fa-money-check-alt"></i> Unsubmitted Fee ({{$start}} - {{$end}})</b></div>
                                    <div class="card-footer d-flex align-items-center justify-content-between">
                                        <a class="small stretched-link" href="javascript:void(0)"><b>{{number_format($previous_month_fee_unpaid,2,'.',',')}}</b></a>
                                        {{-- <div class="small"><i class="fas fa-angle-right"></i></div> --}}
                                    </div>
                                </div>
                            </div> -->

                            <div class="col-xl-12">
                                <div class="card card-border-top mb-4">
                                    <div class="card-header" style="color: black;">
                                        <i class="fas fa-chart-area mr-1"></i>
                                        <b>Students Strength Classwise</b>
                                        @php
                                            $total_male = 0;
                                            $total_female = 0;
                                            $total_male_female = 0;
                                        @endphp
                                    </div>
                                    <div class="card-body">
                                        <div class="table-responsive">
                                        <table class="table table-striped text-nowrap">
                                            <thead>
                                            <tr>
                                                <th></th>
                                                @foreach($all_classes as $class)
                                                <th>
                                                    {{$class->class_name}}
                                                </th>
                                                @endforeach
                                                <th>Total</th>
                                            </tr>
                                        </thead>
                                            <tr>
                                                <td>Male</td>
                                                @foreach($all_classes as $class)
                                                @php
                                                    $total_m = $class->get_students()->where('gender','male')->where('status',1)->count();
                                                    $total_male += $total_m;
                                                @endphp
                                                <td>{{$total_m}}</td>
                                                @endforeach
                                                <td>{{$total_male}}</td>
                                            </tr>
                                            <tr>
                                                <td>Female</td>
                                                
                                                @foreach($all_classes as $class)
                                                @php
                                                    $total_f = $class->get_students()->where('gender','female')->where('status',1)->count();
                                                    $total_female += $total_f;
                                                @endphp
                                                <td>{{$total_f}}</td>
                                                @endforeach
                                                <td>{{$total_female}}</td>
                                            </tr>
                                            <tr>
                                                <td>Total</td>
                                                @foreach($all_classes as $class)
                                                <td>{{$class->get_students()->where('status',1)->count()}}</td>
                                                @endforeach
                                                <td>{{$total_male + $total_female}}</td>
                                            </tr>
                                        </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                  

                        
                        <div class="form-row mb-2">
                            <h5 class="col-12 mb-3">Fee Detail</h5>
                            <div class="col-xl-3 col-md-6 mb-2">
                                <div class="card card-border-bottom h-100">
                                    <div class="card-body">
                                        <p class="font-weight-bold mb-2 h6">Monthly Fee</p>
                                        <a class="text-color-1 h5 stretched-link" href="javascript:void(0)" contenteditable="false" style="cursor: pointer;"><b>{{number_format($total_fee,2,'.',',')}}</b></a>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xl-3 col-md-6 mb-2">
                                <div class="card card-border-bottom h-100">
                                    <div class="card-body">
                                        <p class="font-weight-bold mb-2 h6">Total Paid In Current Month</p>
                                        <a class="text-color-1 h5 stretched-link" href="javascript:void(0)" contenteditable="false" style="cursor: pointer;"><b>{{number_format($fee_submitted_current_month,2,'.',',')}}</b></a>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xl-3 col-md-6 mb-2">
                                <div class="card card-border-bottom h-100">
                                    <div class="card-body">
                                        <p class="font-weight-bold mb-2 h6">Total Dues of Active Students</p>
                                        <a class="text-color-1 h5 stretched-link" href="javascript:void(0)" contenteditable="false" style="cursor: pointer;"><b>{{number_format($active_student_pending_fee,2,'.',',')}}</b></a>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xl-3 col-md-6 mb-2">
                                <div class="card card-border-bottom h-100">
                                    <div class="card-body">
                                        <p class="font-weight-bold mb-2 h6">Total Dues of Suspended Students</p>
                                        <a class="text-color-1 h5 stretched-link" href="javascript:void(0)" contenteditable="false" style="cursor: pointer;"><b>{{number_format($suspended_student_pending_fee,2,'.',',')}}</b></a>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xl-3 col-md-6 mb-2">
                                <div class="card card-border-bottom h-100">
                                    <div class="card-body">
                                        <p class="font-weight-bold mb-2 h6">Total Dues of Passout Students</p>
                                        <a class="text-color-1 h5 stretched-link" href="javascript:void(0)" contenteditable="false" style="cursor: pointer;"><b>{{number_format($passout_student_pending_fee,2,'.',',')}}</b></a>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xl-3 col-md-6 mb-2">
                                <div class="card card-border-bottom h-100">
                                    <div class="card-body">
                                        <p class="font-weight-bold mb-2 h6">Total Dues of Tuition Fee</p>
                                        <a class="text-color-1 h5 stretched-link" href="javascript:void(0)" contenteditable="false" style="cursor: pointer;"><b>{{number_format($student_pending_fee,2,'.',',')}}</b></a>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xl-3 col-md-6 mb-2">
                                <div class="card card-border-bottom h-100">
                                    <div class="card-body">
                                        <p class="font-weight-bold mb-2 h6">Total Dues of Other Fee</p>
                                        <a class="text-color-1 h5 stretched-link" href="javascript:void(0)" contenteditable="false" style="cursor: pointer;"><b>{{number_format($total_dues_of_other_fee,2,'.',',')}}</b></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-6 mb-2">
                                <div class="card card-border-bottom h-100">
                                    <div class="card-body">
                                        <p class="font-weight-bold mb-2 h6">Total Dues So Far</p>
                                        <a class="text-color-1 h5 stretched-link" href="javascript:void(0)" contenteditable="false" style="cursor: pointer;"><b>{{number_format($total_dues_of_other_fee+$student_pending_fee,2,'.',',')}}</b></a>
                                    </div>
                                </div>
                            </div>
                        </div>

                                <div class="card card-border-top mb-4">
                                    <div class="card-header" style="color: black;">
                                        <i class="fas fa-chart-area mr-1"></i>
                                        <b>Last 12 Months Fee Record</b>
                                    </div>
                                    <div class="card-body">
                                        <div class="table-responsive">
                                        <table class="table table-striped">
                                            <thead>
                                            <tr>
                                                <th></th>
                                                @foreach($months_names as $mon)
                                                <th>
                                                    {{carbon::parse($mon)->format('F')}}
                                                </th>
                                                @endforeach
                                            </tr>
                                            </thead>
                                            <tr>
                                                <td>Total Tuition Fee</td>
                                                @foreach($months_names as $mon)
                                                <td>
                                                    {{ number_format(\App\StudentFee::where('school_id', auth()->user()->school_id)->whereMonth('fee_month',carbon::parse($mon)->format('m'))->whereYear('fee_month',$years_names[$loop->index])->sum('total_amount'),2,'.',',') }}
                                                </td>
                                                @endforeach

                                            </tr>
                                                                                        <tr>
                                                <td>Total Concession in Tuition Fee</td>
                                                @foreach($months_names as $mon)
                                                <td>
                                                    {{ number_format(\App\StudentFee::where('school_id', auth()->user()->school_id)->whereMonth('fee_month',carbon::parse($mon)->format('m'))->whereYear('fee_month',$years_names[$loop->index])->sum('concession'),2,'.',',') }}
                                                </td>
                                                @endforeach

                                            </tr>
                                            <tr>
                                                <td>Total Paid Tuition Fee</td>
                                                @foreach($months_names as $mon)
                                                <td>
                                                    {{ number_format(\App\StudentFee::where('school_id', auth()->user()->school_id)->whereMonth('fee_month',carbon::parse($mon)->format('m'))->whereYear('fee_month',$years_names[$loop->index])->sum('paid_amount'),2,'.',',') }}
                                                </td>
                                                @endforeach
                                            </tr>
                                            <tr>
                                                <td>Total Unpaid Tuition Fee</td>
                                                @foreach($months_names as $mon)
                                                <td>
                                                    {{ number_format(\App\StudentFee::where('school_id', auth()->user()->school_id)->whereMonth('fee_month',carbon::parse($mon)->format('m'))->whereYear('fee_month',$years_names[$loop->index])->sum('unpaid_amount'),2,'.',',') }}
                                                </td>
                                                @endforeach
                                            </tr>
                                        </table>
                                    </div>
                                    </div>
                                </div>

                        <h5 class="mb-3">Teachers Salary Detail</h5>
                        <div class="row form-row mb-2">
                            <div class="col-xl-3 col-md-6 mb-2">
                                <div class="card card-border-bottom h-100">
                                    <div class="card-body">
                                        <p class="font-weight-bold mb-2 h6">Total Salaries</p>
                                        <a class="text-color-1 h5 stretched-link" href="javascript:void(0)"><b>{{number_format($teachers_total_salary,2,'.',',')}}</b></a>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xl-3 col-md-6 mb-2">
                                <div class="card card-border-bottom h-100">
                                    <div class="card-body">
                                        <p class="font-weight-bold mb-2 h6">Total Deduction in {{$lastMonth_in_words}}</p>
                                        <a class="text-color-1 h5 stretched-link" href="javascript:void(0)"><b>{{number_format($teachers_deduction_salary,2,'.',',')}}</b></a>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xl-3 col-md-6 mb-2">
                                <div class="card card-border-bottom h-100">
                                    <div class="card-body">
                                        <p class="font-weight-bold mb-2 h6">Total B.A.D.E Amount in {{$lastMonth_in_words}}</p>
                                        <a class="text-color-1 h5 stretched-link" href="javascript:void(0)"><b>{{number_format($bonus_amount,2,'.',',')}}</b></a>
                                    </div>
                                </div>
                            </div>

                            <!-- <div class="col-xl-3 col-md-6 mb-2">
                                <div class="card bg-warning text-white mb-4">
                                    <div class="card-body"><b><i class="fas fa-money-check-alt text-white my-icons"></i>Total Due Salary of {{$lastMonth_in_words}}</b></div>
                                    <div class="card-footer d-flex align-items-center justify-content-between">
                                        <a class="small stretched-link" href="javascript:void(0)"><b>{{number_format($second_last_month_dues,2,'.',',')}}</b></a>
                                        {{-- <div class="small text-white"><i class="fas fa-angle-right"></i></div> --}}
                                    </div>
                                </div>
                            </div> -->

                            <div class="col-xl-3 col-md-6 mb-2">
                                <div class="card card-border-bottom h-100">
                                    <div class="card-body">
                                        <p class="font-weight-bold mb-2 h6">Total Paid in {{$lastMonth_in_words}}</p>
                                        <a class="text-color-1 h5 stretched-link" href="javascript:void(0)"><b>{{number_format($paid_amount_in_last_m,2,'.',',')}}</b></a>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xl-3 col-md-6 mb-2">
                                <div class="card card-border-bottom h-100">
                                    <div class="card-body">
                                        <p class="font-weight-bold mb-2 h6">Total Unpaid in {{$lastMonth_in_words}}</p>
                                        <a class="text-color-1 h5 stretched-link" href="javascript:void(0)"><b>{{number_format($unpaid_amount_in_last_m,0,'.',',')}}</b></a>
                                    </div>
                                </div>
                            </div>

                         <!--    <div class="col-xl-3 col-md-6 mb-2">
                                <div class="card bg-danger text-white mb-4">
                                    <div class="card-body"><b><i class="fas fa-money-check-alt text-white my-icons"></i> Unsubmitted Fee ({{$start}} - {{$end}})</b></div>
                                    <div class="card-footer d-flex align-items-center justify-content-between">
                                        <a class="small stretched-link" href="javascript:void(0)"><b>{{number_format($previous_month_fee_unpaid,2,'.',',')}}</b></a>
                                        {{-- <div class="small text-white"><i class="fas fa-angle-right"></i></div> --}}
                                    </div>
                                </div>
                            </div> -->
                        </div>
                                <div class="card card-border-top mb-4">
                                    <div class="card-header" style="color: black;">
                                        <i class="fas fa-chart-area mr-1"></i>
                                        <b>Teachers Salaries History</b>
                                    </div>
                                    <div class="card-body">
                                        <div class="table-responsive">
                                        <table class="table table-striped">
                                            <thead>
                                                <tr>
                                                    <th></th>
                                                @foreach($months_names as $mon)
                                                    <th>
                                                        {{carbon::parse($mon)->format('F')}}
                                                    </th>
                                                    @endforeach
                                                </tr>
                                            </thead>
                                            <tr>
                                                <td>Total Salaries</td>
                                                @foreach($months_names as $mon)
                                                <td>
                                                    {{ number_format(\App\TeacherSalary::where('school_id', auth()->user()->school_id)->whereMonth('salary_month',carbon::parse($mon)->format('m'))->whereYear('salary_month',$years_names[$loop->index])->sum('total_amount'),2,'.',',') }}
                                                </td>
                                                @endforeach
                                            </tr>
                                            <tr>
                                                <td>Total Deductions</td>
                                                @foreach($months_names as $mon)
                                                <td>
                                                    {{ number_format(\App\TeacherSalary::where('school_id', auth()->user()->school_id)->whereMonth('salary_month',carbon::parse($mon)->format('m'))->whereYear('salary_month',$years_names[$loop->index])->sum('deduction'),2,'.',',') }}
                                                </td>
                                                @endforeach
                                            </tr>

                                            <tr>
                                                <td>Total B.A.D.E Amount</td>
                                                @foreach($months_names as $mon)
                                                <td>
                                                    {{ number_format(\App\TeacherSalary::where('school_id', auth()->user()->school_id)->whereMonth('salary_month',carbon::parse($mon)->format('m'))->whereYear('salary_month',$years_names[$loop->index])->sum('bonus_amount'),2,'.',',') }}
                                                </td>
                                                @endforeach
                                            </tr>

                                            <tr>
                                                <td>Total Payable</td>
                                                @foreach($months_names as $mon)
                                                @php 
                                                    $total = \App\TeacherSalary::where('school_id', auth()->user()->school_id)->whereMonth('salary_month',carbon::parse($mon)->format('m'))->whereYear('salary_month',$years_names[$loop->index])->sum('total_amount');

                                                    $ded = \App\TeacherSalary::where('school_id', auth()->user()->school_id)->whereMonth('salary_month',carbon::parse($mon)->format('m'))->whereYear('salary_month',$years_names[$loop->index])->sum('deduction');

                                                    $bonus = \App\TeacherSalary::where('school_id', auth()->user()->school_id)->whereMonth('salary_month',carbon::parse($mon)->format('m'))->whereYear('salary_month',$years_names[$loop->index])->sum('bonus_amount');

                                                    $gross = $total - $ded + $bonus;
                                                @endphp
                                                <td>
                                                    {{ number_format($gross,2,'.',',') }}
                                                </td>
                                                @endforeach
                                            </tr>
                                            <tr>
                                                <td>Total Paid</td>
                                                 @foreach($months_names as $mon)
                                                <td>
                                                    {{ number_format(\App\TeacherSalary::where('school_id', auth()->user()->school_id)->whereMonth('salary_month',carbon::parse($mon)->format('m'))->whereYear('salary_month',$years_names[$loop->index])->sum('paid_amount'),2,'.',',') }}
                                                </td>
                                                @endforeach
                                            </tr>
                                            <tr>
                                                <td>Total Unpaid</td>
                                                 @foreach($months_names as $mon)
                                                <td>
                                                    {{ number_format(\App\TeacherSalary::where('school_id', auth()->user()->school_id)->whereMonth('salary_month',carbon::parse($mon)->format('m'))->whereYear('salary_month',$years_names[$loop->index])->sum('unpaid_amount'),2,'.',',') }}
                                                </td>
                                                @endforeach
                                            </tr>
                                        </table>
                                        </div>
                                    </div>
                                </div>
  
                            <div class="form-row mb-2">
                            <h5 class="mb-3 col-12">Expenses Detail</h5>

                            <div class="col-xl-3 col-md-6 mb-2">
                                <div class="card card-border-bottom h-100">
                                    <div class="card-body">
                                        <p class="font-weight-bold mb-2 h6">Total Expenses</p>
                                        <a class="text-color-1 h5 stretched-link" href="javascript:void(0)"><b>{{number_format($personal_expenses + $school_expenses + $other_expenses,2,'.',',')}}</b></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-6 mb-2">
                                <div class="card card-border-bottom h-100">
                                    <div class="card-body">
                                        <p class="font-weight-bold mb-2 h6">Personal Expenses ({{$current_start}} - {{$current_end}})</p>
                                        <a class="text-color-1 h5 stretched-link" href="javascript:void(0)"><b>{{number_format($personal_expenses,2,'.',',')}}</b></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-6 mb-2">
                                <div class="card card-border-bottom h-100">
                                    <div class="card-body">
                                        <p class="font-weight-bold mb-2 h6">School Expenses ({{$current_start}} - {{$current_end}})</p>
                                        <a class="text-color-1 h5 stretched-link" href="javascript:void(0)"><b>{{number_format($school_expenses,2,'.',',')}}</b></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-6 mb-2">
                                <div class="card card-border-bottom h-100">
                                    <div class="card-body">
                                        <p class="font-weight-bold mb-2 h6">Other Expenses ({{$current_start}} - {{$current_end}})</p>
                                        <a class="text-color-1 h5 stretched-link" href="javascript:void(0)"><b>{{number_format($other_expenses,2,'.',',')}}</b></a>
                                    </div>
                                </div>
                            </div>
                            </div>

                                <div class="card card-border-top mb-4">
                                    <div class="card-header" style="color: black;">
                                        <i class="fas fa-chart-area mr-1"></i>
                                        <b>Expenses Summary Of {{carbon::now()->format('F')}}</b>
                                    </div>
                                    <div class="card-body">
                                        <div class="table-responsive">
                                        <table class="expenses-table table table-striped">
                                            <thead>
                                            <tr>
                                                <th class="expenses-head">Personal Expenses</th>
                                                <th class="expenses-head">School Expenses</th>
                                                <th class="expenses-head">Other Expenses</th>
                                            </tr>
                                            </thead>
                                            <tr>
                                                <td style="padding: 0;" class="align-top">
                                                    <table class="w-100">
                                                        <tr>
                                                            <th>Type</th>
                                                            <th>Amount</th>
                                                        </tr>

                                                        @foreach($personal_expenses_types as $exp)
                                                            <tr>
                                                                <td>{{$exp->title}}</td>
                                                                <td>{{$exp->get_expenses != null ? number_format($exp->get_expenses()->whereMonth('expense_date',carbon::now()->format('m'))->whereYear('expense_date',carbon::now()->format('Y'))->sum('paid_amount'),2,'.',',') : 0}}</td>
                                                            </tr>
                                                        @endforeach
                                                    </table>
                                                </td>
                                                <td style="padding: 0;" class="align-top">
                                                    <table class="w-100">
                                                        <tr>
                                                            <th>Type</th>
                                                            <th>Amount</th>
                                                        </tr>

                                                        @foreach($school_expenses_types as $exp)
                                                            <tr>
                                                                <td>{{$exp->title}}</td>
                                                                <td>{{$exp->get_expenses != null ? number_format($exp->get_expenses()->whereMonth('expense_date',carbon::now()->format('m'))->whereYear('expense_date',carbon::now()->format('Y'))->sum('paid_amount'),2,'.',',') : 0}}</td>
                                                            </tr>
                                                        @endforeach
                                                    </table>
                                                </td>

                                                <td style="padding: 0;" class="align-top">
                                                    <table class="w-100" >
                                                        <tr>
                                                            <th>Type</th>
                                                            <th>Amount</th>
                                                        </tr>

                                                        @foreach($other_expenses_types as $exp)
                                                            <tr>
                                                                <td>{{$exp->title}}</td>
                                                                <td>{{$exp->get_expenses != null ? number_format($exp->get_expenses()->whereMonth('expense_date',carbon::now()->format('m'))->whereYear('expense_date',carbon::now()->format('Y'))->sum('paid_amount'),2,'.',',') : 0}}</td>
                                                            </tr>
                                                        @endforeach
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    </div>
                                </div>

                                <div class="card card-border-top mb-4">
                                    <div class="card-header" style="color: black;">
                                        <i class="fas fa-chart-area mr-1"></i>
                                        <b>Payable Amount</b>
                                    </div>
                                    <div class="card-body">
                                        <div class="table-responsive">
                                        <table class="table table-striped">
                                            <thead>
                                            <tr>
                                                <th>Title</th>
                                                <th>Description</th>
                                                <th>Total Amount</th>
                                                <th>Paid Amount</th>
                                                <th>Remaining Amount</th>
                                            </tr>
                                            </thead>
                                            @if($payble_amounts->count() > 0)
                                            @foreach($payble_amounts as $pay)
                                            <tr>
                                                <td>{{$pay->expense != null ? $pay->expense->title : '--'}}</td>
                                                <td>{{$pay->description != null ? $pay->description : '--'}}</td>
                                                <td>{{$pay->amount}}</td>
                                                <td>{{$pay->paid_amount != null ? number_format($pay->paid_amount,2,'.',',') : 0}}</td>
                                                <td>{{number_format($pay->amount - $pay->paid_amount,2,'.',',')}}</td>
                                            </tr>
                                            @endforeach
                                            @else
                                            <tr>
                                                <td colspan="5">No Data Found</td>
                                            </tr>
                                            @endif
                                        </table>
                                    </div>
                                </div>
                                </div>
    

                       <!--  <div class="row">
                            <div class="col-xl-3 col-md-6">
                                <div class="card bg-primary text-white mb-4">
                                    <div class="card-body"><b><i class="fas fa-chalkboard-teacher text-white my-icons"></i> All Teachers</b></div>
                                    <div class="card-footer d-flex align-items-center justify-content-between">
                                        <a class="small stretched-link" href="javascript:void(0)"><b>{{number_format($all_teachers_count,0,'.',',')}}</b></a>
                                        {{-- <div class="small text-white"><i class="fas fa-angle-right"></i></div> --}}
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-6">
                                <div class="card bg-warning text-white mb-4">
                                    <div class="card-body"><b><i class="fas fa-coins text-white my-icons"></i> Total Salary ({{$start}} - {{$end}})</b></div>
                                    <div class="card-footer d-flex align-items-center justify-content-between">
                                        <a class="small stretched-link" href="javascript:void(0)"><b>{{number_format($teachers_total_salary,2,'.',',')}}</b></a>
                                        {{-- <div class="small text-white"><i class="fas fa-angle-right"></i></div> --}}
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-6">
                                <div class="card bg-success text-white mb-4">
                                    <div class="card-body"><b><i class="fas fa-coins text-white my-icons"></i> Paid Salary ({{$start}} - {{$end}})</b></div>
                                    <div class="card-footer d-flex align-items-center justify-content-between">
                                        <a class="small stretched-link" href="javascript:void(0)"><b>{{number_format($teachers_paid_salary,2,'.',',')}}</b></a>
                                        {{-- <div class="small text-white"><i class="fas fa-angle-right"></i></div> --}}
                                    </div>
                                </div>
                            </div>

                            <div class="col-xl-3 col-md-6">
                                <div class="card bg-danger text-white mb-4">
                                    <div class="card-body"><b><i class="fas fa-coins text-white my-icons"></i> Unpaid Salary ({{$start}} - {{$end}})</b></div>
                                    <div class="card-footer d-flex align-items-center justify-content-between">
                                        <a class="small stretched-link" href="javascript:void(0)"><b>{{number_format($teachers_total_salary - $teachers_paid_salary,2,'.',',')}}</b></a>
                                        {{-- <div class="small text-white"><i class="fas fa-angle-right"></i></div> --}}
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                        <div class="row">
                            <div class="col-xl-3 col-md-6">
                                <div class="card bg-primary text-white mb-4">
                                    <div class="card-body"><b><i class="fas fa-book-open text-white my-icons"></i> All Books Stock</b></div>
                                    <div class="card-footer d-flex align-items-center justify-content-between">
                                        <a class="small stretched-link" href="javascript:void(0)"><b>{{number_format($all_books_stock,0,'.',',')}}</b></a>
                                        {{-- <div class="small text-white"><i class="fas fa-angle-right"></i></div> --}}
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-6">
                                <div class="card bg-warning text-white mb-4">
                                    <div class="card-body"><b><i class="fas fa-wallet text-white my-icons"></i> Books Total Amount</b></div>
                                    <div class="card-footer d-flex align-items-center justify-content-between">
                                        <a class="small stretched-link" href="javascript:void(0)"><b>{{number_format($all_books_total_amount,2,'.',',')}}</b></a>
                                        {{-- <div class="small text-white"><i class="fas fa-angle-right"></i></div> --}}
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-6">
                                <div class="card bg-success text-white mb-4">
                                    <div class="card-body"><b><i class="fas fa-wallet text-white my-icons"></i> Books Received Amount</b></div>
                                    <div class="card-footer d-flex align-items-center justify-content-between">
                                        <a class="small stretched-link" href="javascript:void(0)"><b>{{number_format($all_books_paid_amount,2,'.',',')}}</b></a>
                                        {{-- <div class="small text-white"><i class="fas fa-angle-right"></i></div> --}}
                                    </div>
                                </div>
                            </div>

                            <div class="col-xl-3 col-md-6">
                                <div class="card bg-danger text-white mb-4">
                                    <div class="card-body"><b><i class="fas fa-wallet text-white my-icons"></i> Books Pending Amount</b></div>
                                    <div class="card-footer d-flex align-items-center justify-content-between">
                                        <a class="small stretched-link" href="javascript:void(0)"><b>{{number_format($all_books_total_amount - $all_books_paid_amount,2,'.',',')}}</b></a>
                                        {{-- <div class="small text-white"><i class="fas fa-angle-right"></i></div> --}}
                                    </div>
                                </div>
                            </div>
                            
                        </div> -->
                        <!-- <ol class="breadcrumb mb-4 d-none">
                            <li class="breadcrumb-item active">Other Details</li>
                        </ol>
                        <div class="row d-none">
                            <div class="col-xl-3 col-md-6">
                                <div class="card bg-primary text-white mb-4">
                                    <div class="card-body"><b><i class="fas fa-receipt text-white my-icons"></i> Other Fee ({{$start}} - {{$end}})</b></div>
                                    <div class="card-footer d-flex align-items-center justify-content-between">
                                        <a class="small stretched-link" href="javascript:void(0)" style="font-size: 18px;">
                                            <b>{{number_format($previous_month_other_fee,2,'.',',')}}</b>
                                        </a>
                                        {{-- <div class="small text-white"><i class="fas fa-angle-right"></i></div> --}}
                                    </div>
                                </div>
                            </div>

                            <div class="col-xl-3 col-md-6">
                                <div class="card bg-warning text-white mb-4">
                                    <div class="card-body"><b><i class="fas fa-receipt text-white my-icons"></i> Net Income ({{$start}} - {{$end}})</b></div>
                                    <div class="card-footer d-flex align-items-center justify-content-between">
                                        <a class="small stretched-link" href="javascript:void(0)"><b>{{number_format(($previous_month_fee + $previous_month_other_fee) - (($personal_expenses + $school_expenses + $other_expenses) + $teachers_paid_salary),2,'.',',')}}</b></a>
                                        {{-- <div class="small text-white"><i class="fas fa-angle-right"></i></div> --}}
                                    </div>
                                </div>
                            </div>
                           
                        </div> -->
                        <div class="mb-4">
                            <h5 class="mb-3">Income Details</h5>
                                <div class="card card-border-top">
                                    <div class="card-header" style="color: black;">
                                        <i class="fas fa-chart-area mr-1"></i>
                                        <b>Last 12 Months Income Record</b>
                                    </div>
                                    <div class="card-body">
                                        <div class="table-responsive">
                                        <table class="table table-striped">
                                            <thead>
                                                <tr>
                                                    <th></th>
                                                    @foreach($months_names as $mon)
                                                    <th>
                                                        {{carbon::parse($mon)->format('F')}}
                                                    </th>
                                                    @endforeach
                                                </tr>
                                            </thead>
                                            <tr>
                                                <td>Total Tuition Fee</td>
                                                @foreach($months_names as $mon)
                                                @php 
                                                $total_fee = \App\StudentFee::where('school_id', auth()->user()->school_id)->whereMonth('fee_month',carbon::parse($mon)->format('m'))->whereYear('fee_month',$years_names[$loop->index])->sum('total_amount');
                                                @endphp
                                                <td>
                                                    {{ number_format($total_fee,2,'.',',')}}
                                                </td>
                                                @endforeach

                                            </tr>
                                            <tr>
                                                <td>Total Concession in Tuition Fee</td>
                                                @foreach($months_names as $mon)
                                                @php 
                                                $total_fee = \App\StudentFee::where('school_id', auth()->user()->school_id)->whereMonth('fee_month',carbon::parse($mon)->format('m'))->whereYear('fee_month',$years_names[$loop->index])->sum('concession');
                                                @endphp
                                                <td>
                                                    {{ number_format($total_fee,2,'.',',')}}
                                                </td>
                                                @endforeach

                                            </tr>
                                            <tr>
                                                <td>Total Paid Tuition Fee</td>
                                                @foreach($months_names as $mon)
                                                <?php
                                                $total_fee_paid = \App\StudentFee::where('school_id', auth()->user()->school_id)->whereMonth('fee_month',carbon::parse($mon)->format('m'))->whereYear('fee_month',$years_names[$loop->index])->sum('paid_amount');
                                                $total_received += $total_fee_paid;
                                                $$mon += $total_fee_paid;
                                                ?>
                                                <td>
                                                    {{ number_format($total_fee_paid,2,'.',',') }}
                                                </td>
                                                @endforeach
                                            </tr>
                                            <tr>
                                                <td>Total Unpaid Tuition Fee</td>
                                                @foreach($months_names as $mon)
                                                <td>
                                                    {{ number_format(\App\StudentFee::where('school_id', auth()->user()->school_id)->whereMonth('fee_month',carbon::parse($mon)->format('m'))->whereYear('fee_month',$years_names[$loop->index])->sum('unpaid_amount'),2,'.',',') }}
                                                </td>
                                                @endforeach
                                            </tr>

                                            <tr>
                                                <td>Total Other Fee</td>
                                                @foreach($months_names as $mon)
                                                @php 
                                                $total_other_fee_paid = \App\OtherFee::where('school_id', auth()->user()->school_id)->whereMonth('submitted_date',carbon::parse($mon)->format('m'))->whereYear('submitted_date',$years_names[$loop->index])->sum('amount');
                                                $total_received += $total_other_fee_paid;
                                                $$mon += $total_other_fee_paid;
                                                @endphp
                                                <td>
                                                    {{ number_format($total_other_fee_paid,2,'.',',') }}
                                                </td>
                                                @endforeach

                                            </tr>
                                            <tr>
                                                <td>Total Concession in Other Fee</td>
                                                @foreach($months_names as $mon)
                                                @php 
                                                $total_concession_in_other_fee = \App\OtherFee::where('school_id', auth()->user()->school_id)->whereMonth('submitted_date',carbon::parse($mon)->format('m'))->whereYear('submitted_date',$years_names[$loop->index])->sum('concession');
                                                @endphp
                                                <td>
                                                    {{ number_format($total_concession_in_other_fee,2,'.',',') }}
                                                </td>
                                                @endforeach

                                            </tr>
                                            <tr>
                                                <td>Total Paid Other Fee</td>
                                                @foreach($months_names as $mon)
                                                @php 
                                                $total_paid_in_other_fee = \App\OtherFee::where('school_id', auth()->user()->school_id)->whereMonth('submitted_date',carbon::parse($mon)->format('m'))->whereYear('submitted_date',$years_names[$loop->index])->sum('paid_amount');
                                                @endphp
                                                <td>
                                                    {{ number_format($total_paid_in_other_fee,2,'.',',') }}
                                                </td>
                                                @endforeach

                                            </tr>
                                            <tr>
                                                <td>Total Unpaid Other Fee</td>
                                                @foreach($months_names as $mon)
                                                @php 
                                                $total_unpaid_in_other_fee = \App\OtherFee::where('school_id', auth()->user()->school_id)->whereMonth('submitted_date',carbon::parse($mon)->format('m'))->whereYear('submitted_date',$years_names[$loop->index])->sum('unpaid_amount');
                                                @endphp
                                                <td>
                                                    {{ number_format($total_unpaid_in_other_fee,2,'.',',') }}
                                                </td>
                                                @endforeach

                                            </tr>

                                            <tr>
                                                <td>Total Salaries</td>
                                                @foreach($months_names as $mon)
                                                <td>
                                                    {{ number_format(\App\TeacherSalary::where('school_id', auth()->user()->school_id)->whereMonth('salary_month',carbon::parse($mon)->format('m'))->whereYear('salary_month',$years_names[$loop->index])->sum('total_amount'),2,'.',',') }}
                                                </td>
                                                @endforeach
                                            </tr>
                                            <tr>
                                                <td>Total Deduction in Salaries</td>
                                                @foreach($months_names as $mon)
                                                <td>
                                                    {{ number_format(\App\TeacherSalary::where('school_id', auth()->user()->school_id)->whereMonth('salary_month',carbon::parse($mon)->format('m'))->whereYear('salary_month',$years_names[$loop->index])->sum('deduction'),2,'.',',') }}
                                                </td>
                                                @endforeach
                                            </tr>
                                            <tr>
                                                <td> Total Paid Salaries</td>
                                                 @foreach($months_names as $mon)
                                                 @php
                                                    $paid_sa = \App\TeacherSalary::where('school_id', auth()->user()->school_id)->whereMonth('salary_month',carbon::parse($mon)->format('m'))->whereYear('salary_month',$years_names[$loop->index])->sum('paid_amount');
                                                    $$mon -= $paid_sa;
                                                 @endphp
                                                <td>
                                                    {{ number_format($paid_sa,2,'.',',') }}
                                                </td>
                                                @endforeach
                                            </tr>
                                            <tr>
                                                <td>Total Unpaid Salary</td>
                                                 @foreach($months_names as $mon)
                                                <td>
                                                    {{ number_format(\App\TeacherSalary::where('school_id', auth()->user()->school_id)->whereMonth('salary_month',carbon::parse($mon)->format('m'))->whereYear('salary_month',$years_names[$loop->index])->sum('unpaid_amount'),2,'.',',') }}
                                                </td>
                                                @endforeach
                                            </tr>

                                            <tr>
                                                <td>Total Personal Expenses</td>
                                                @foreach($months_names as $mon)
                                                @php
                                                    $tpep = \App\Expense::where('status',1)->whereMonth('expense_date',carbon::parse($mon)->format('m'))->whereYear('expense_date',$years_names[$loop->index])->whereHas('expense',function($z){
                                                        $z->where('type','personal');
                                                    })->sum('paid_amount');
                                                    $$mon -= $tpep;
                                                @endphp
                                                <td>
                                                    {{ number_format($tpep,2,'.',',')}}
                                                </td>
                                                @endforeach
                                            </tr>

                                            <!-- <tr>
                                                <td>Total Personal Expenses</td>
                                                @foreach($months_names as $mon)
                                                <th>
                                                

                                                    {{ \App\Expense::where('status',1)->whereMonth('expense_date',carbon::parse($mon)->format('m'))->whereYear('expense_date',$years_names[$loop->index])->whereHas('expense',function($z){
                                                        $z->where('type','personal');
                                                    })->sum('amount')}}
                                                </th>
                                                @endforeach
                                            </tr> -->

                                            <tr>
                                                <td>Total School Expenses</td>
                                                @foreach($months_names as $mon)
                                                <td>                                                
                                                    {{ number_format(\App\Expense::whereMonth('expense_date',carbon::parse($mon)->format('m'))->whereYear('expense_date',$years_names[$loop->index])->whereHas('expense',function($z){
                                                        $z->where('type','school');
                                                    })->sum('amount'),2,'.',',')}}
                                                </td>
                                                @endforeach
                                            </tr>
                                            <tr>
                                                <td>Total School Expenses Paid</td>
                                                @foreach($months_names as $mon)
                                                @php
                                                    $tsep = \App\Expense::whereMonth('expense_date',carbon::parse($mon)->format('m'))->whereYear('expense_date',$years_names[$loop->index])->whereHas('expense',function($z){
                                                        $z->where('type','school');
                                                    })->sum('paid_amount');
                                                    $$mon -= $tsep;
                                                @endphp
                                                <td>
                                                    {{ number_format($tsep,2,'.',',') }}
                                                </td>
                                                @endforeach
                                            </tr>

                                            <tr>
                                                <td>Total School Expenses Unpaid</td>
                                                @foreach($months_names as $mon)
                                                <td>
                                                

                                                    {{ number_format(\App\Expense::whereMonth('expense_date',carbon::parse($mon)->format('m'))->whereYear('expense_date',$years_names[$loop->index])->whereHas('expense',function($z){
                                                        $z->where('type','school');
                                                    })->sum('amount') - \App\Expense::whereMonth('expense_date',carbon::parse($mon)->format('m'))->whereYear('expense_date',$years_names[$loop->index])->whereHas('expense',function($z){
                                                        $z->where('type','school');
                                                    })->sum('paid_amount'),2,'.',',')}}
                                                </td>
                                                @endforeach
                                            </tr>
                                            <tr>
                                                <td>Total Income</td>
                                                @foreach($months_names as $mon)
                                                @php 
                                                $t = ${$mon};
                                                $total_income_of_year += $t;
                                                @endphp
                                                    <td>{{number_format($t,2,'.',',')}}</td>
                                                @endforeach

                                            </tr>
                                            <tr>
                                                <td>Total Income Of Last 12 Months</td>
                                                <td colspan="12">{{number_format($total_income_of_year,2,'.',',')}}</td>
                                                
                                            </tr>
                                        </table>
                                        </div>
                                    </div>
                                </div>
                        </div>
                        <div class="row">
                            <div class="col-xl-6">
                                <div class="card card-border-top mb-4">
                                    <div class="card-header" style="color: black;">
                                        <i class="fas fa-chart-area mr-1"></i>
                                        <b>Submitted Fee Chart From ({{$start}} - {{$end}})</b>
                                    </div>
                                    <div class="card-body"><canvas id="monthChart" width="100%" height="40"></canvas></div>
                                </div>
                            </div>
                            <div class="col-xl-6">
                                <div class="card card-border-top mb-4">
                                    <div class="card-header" style="color: black;">
                                        <i class="fas fa-chart-bar mr-1"></i>
                                        <b>Monthly Income Chart</b>
                                    </div>
                                    <div class="card-body"><canvas id="myBarChart1" width="100%" height="40"></canvas></div>
                                </div>
                            </div>

                             <div class="col-xl-12">
                                <div class="card card-border-top mb-4">
                                    <div class="card-header" style="color: black;">
                                        <i class="fas fa-chart-bar mr-1"></i>
                                        <b>Students In Each Class</b>
                                    </div>
                                    <div class="card-body"><canvas id="myBarChart12" width="100%" height="40"></canvas></div>
                                </div>
                            </div>
                        </div>
                       
                    </div>
           
@endsection

@section('javascript')
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>
<script src="{{asset('public/user/layouts/demo/chart-area-demo.js')}}"></script>
<script src="{{asset('public/user/layouts/demo/chart-bar-demo.js')}}"></script>
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
{{-- <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script> --}}
<script src="{{asset('public/user/layouts/demo/datatables-demo.js')}}"></script>

<script type="text/javascript">
$(document).ready(function(){
   // Set new default font family and font color to mimic Bootstrap's default styling
Chart.defaults.global.defaultFontFamily = '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
Chart.defaults.global.defaultFontColor = '#292b2c';

// Area Chart Example
var ctx = document.getElementById("monthChart");
var labels = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
var data = ['{{$jan}}','{{$feb}}','{{$mar}}','{{$apr}}','{{$may}}','{{$jun}}','{{$jul}}','{{$aug}}','{{$sep}}','{{$oct}}','{{$nov}}','{{$dec}}'];
var myLineChart = new Chart(ctx, {
  type: 'line',
  data: {
    labels: labels,
    datasets: [{
      label: "Sessions",
      lineTension: 0.3,
      backgroundColor: "rgba(2,117,216,0.2)",
      borderColor: "rgba(2,117,216,1)",
      pointRadius: 5,
      pointBackgroundColor: "rgba(2,117,216,1)",
      pointBorderColor: "rgba(255,255,255,0.8)",
      pointHoverRadius: 5,
      pointHoverBackgroundColor: "rgba(2,117,216,1)",
      pointHitRadius: 50,
      pointBorderWidth: 2,
      data: data,
    }],
  },
  options: {
    scales: {
      xAxes: [{
        time: {
          unit: 'date'
        },
        gridLines: {
          display: false
        },
        ticks: {
          maxTicksLimit: 7
        }
      }],
      yAxes: [{
        ticks: {
          min: 0,
          max: 40000,
          maxTicksLimit: 5
        },
        gridLines: {
          color: "rgba(0, 0, 0, .125)",
        }
      }],
    },
    legend: {
      display: false
    }
  }
});

// Bar Chart Example
var ctx = document.getElementById("myBarChart1");
var myLineChart = new Chart(ctx, {
  type: 'bar',
  data: {
    labels:labels,
    datasets: [{
      label: "Net Income",
      backgroundColor: "rgba(2,117,216,1)",
      borderColor: "rgba(2,117,216,1)",
      data: ['{{$jan_income}}','{{$feb_income}}','{{$mar_income}}','{{$apr_income}}','{{$may_income}}','{{$jun_income}}','{{$jul_income}}','{{$aug_income}}','{{$sep_income}}','{{$oct_income}}','{{$nov_income}}','{{$dec_income}}'],
    }],
  },
  options: {
    scales: {
      xAxes: [{
        time: {
          unit: 'month'
        },
        gridLines: {
          display: false
        },
        ticks: {
          maxTicksLimit: 12
        }
      }],
      yAxes: [{
        ticks: {
          // min: 0,
          // max: 15000,
          maxTicksLimit: 5
        },
        gridLines: {
          display: true
        }
      }],
    },
    legend: {
      display: false
    }
  }
});

// Bar Chart Example
var classes = new Array();
var members = new Array();
var colors = new Array();

<?php foreach ($classes as $cls): ?>
classes.push('{{$cls}}');    
<?php endforeach ?>
<?php foreach ($students_array as $cls): ?>
members.push('{{$cls}}');   
 colors.push('#'+Math.floor(Math.random()*16777215).toString(16)); 
<?php endforeach ?>
// alert(colors);

var data = {
    datasets: [{
        data: members,
        backgroundColor: colors,
        label: 'My dataset' // for legend
    }],
    labels: classes
};

var pieOptions = {
  events: false,
  animation: {
    duration: 500,
    easing: "easeOutQuart",
    onComplete: function () {
      var ctx = this.chart.ctx;
      ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontFamily, 'normal', Chart.defaults.global.defaultFontFamily);
      ctx.textAlign = 'center';
      ctx.textBaseline = 'bottom';

      this.data.datasets.forEach(function (dataset) {

        for (var i = 0; i < dataset.data.length; i++) {
          var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model,
              total = dataset._meta[Object.keys(dataset._meta)[0]].total,
              mid_radius = model.innerRadius + (model.outerRadius - model.innerRadius)/2,
              start_angle = model.startAngle,
              end_angle = model.endAngle,
              mid_angle = start_angle + (end_angle - start_angle)/2;

          var x = mid_radius * Math.cos(mid_angle);
          var y = mid_radius * Math.sin(mid_angle);

          ctx.fillStyle = '#fff';
          if (i == 3){ // Darker text color for lighter background
            ctx.fillStyle = '#444';
          }

          var val = dataset.data[i];
          var percent = String(Math.round(val/total*100)) + "%";

          if(val != 0) {
            ctx.fillText(dataset.data[i], model.x + x, model.y + y);
            // Display percent in another line, line break doesn't work for fillText
            ctx.fillText(percent, model.x + x, model.y + y + 15);
          }
        }
      });               
    }
  }
};
var ctx = document.getElementById("myBarChart12");
var myPieChart = new Chart(ctx, {
    type: 'pie',
    // data: data,
    // options: options,
    data : data,
  options: pieOptions
});
});
</script>
@stop

