@extends('user.layouts.layout')

@section('title','Online Marketing')

@section('content')
<style type="text/css">
   
</style>

    <div>
    	<div class="card mt-4">
		  <h2 class="card-header" style="color:black;">Sms management</h2>
		  <div class="card-body">
		    <div style="display: flex;" class="bg-white">
		      <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical" style="width: 20%;background: rgba(0,0,0,0.4)">
				  <a class="custom-sms-links active" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true"><i class="fa fa-edit"></i> Compose New Message</a>
				  <a class="custom-sms-links" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="false"><i class="fa fa-edit"></i> Compose New Bulk Message</a>
				  <a class="custom-sms-links" id="v-pills-messages-tab" data-toggle="pill" href="#v-pills-messages" role="tab" aria-controls="v-pills-messages" aria-selected="false"><i class="fa fa-envelope"></i> Pending Messages</a>
				  <a class="custom-sms-links" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-settings" role="tab" aria-controls="v-pills-settings" aria-selected="false"><i class="fa fa-sent"></i> Sent Messages</a>
				</div>
				<div class="tab-content" id="v-pills-tabContent" style="width: 80%;">
				  <div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">...</div>
				  <div class="tab-pane fade" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">...</div>
				  <div class="tab-pane fade" id="v-pills-messages" role="tabpanel" aria-labelledby="v-pills-messages-tab">...</div>
				  <div class="tab-pane fade" id="v-pills-settings" role="tabpanel" aria-labelledby="v-pills-settings-tab">...</div>
				</div>
				</div>
		  </div>
		</div>
      
    </div>
>

@endsection

@section('javascript')
<script type="text/javascript">

</script>
@stop

