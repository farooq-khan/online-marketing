@extends('user.layouts.layout')

@section('title','Online Marketing')

@section('content')
<style type="text/css">
   
</style>

<div>
    <div class="row page-title-row">
        <div class="col-md col-sm col-12 page-title-col align-self-center">
            <h1 class="page-title">Website Configuration</h1>
        </div>
    </div>

    <form id="add-school-form" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="row">
        <div class="col-6">
          <div class="card">
            <div class="card-body">
              <h4>Website Main Section</h4>
              <div class="form-row">
              <div class="form-group col-md-6">
                  <label for="image1">Main Quote</label>
                  <input type="text" name="quote" id="quote" class="form-control" placeholder="Quote..." value="{{ @$conf->quote }}" />
              </div>

              <div class="form-group col-md-6">
                  <div class="d-flex justify-content-between mb-2">
                    <label for="image1">Main Banner</label>
                    @if(@$conf->banner1 != null)
                      <button class="btn btn-danger btn-sm delete-image" data-type="main" type="button">Delete Image</button>
                    @endif
                  </div>
                  @if(@$conf->banner1 == null)
                  <input type="file" name="image1" id="image1" class="form-control" placeholder="Logo..."/>
                  @else

                    <img src="{{asset('public/uploads/website/banners/'.@$conf->banner1)}}" class="img-fluid" style="max-height: 120px;">
                  @endif
              </div>
            </div>
            </div>
          </div>
        </div>
        <div class="col-6">
          <div class="card">
            <div class="card-body">
              <h4>About Us Section</h4>
              <div class="form-row">
              <div class="form-group col-md-12">
                  <label for="image1">About Us</label>
                  <textarea name="about_us" class="form-control">{{@$conf->about_us}}</textarea>
              </div>
            </div>
            <hr>
            <h4>Principal Information</h4>
              <div class="form-row">
              <div class="form-group col-md-6">
                  <label for="principal_name">Name</label>
                  <input type="text" name="principal_name" class="form-control" placeholder="i.e ABC ..." value="{{ @$conf->principal_name }}">
              </div>
              <div class="form-group col-md-6">
                  <div class="d-flex justify-content-between mb-2">
                    <label for="principal_photo">Photo</label>
                    @if(@$conf->principal_photo != null)
                      <button class="btn btn-danger btn-sm delete-image" data-type="principal" type="button">Delete Image</button>
                    @endif
                  </div>
                  
                  @if(@$conf->principal_photo == null)
                  <input type="file" name="principal_photo" id="principal_photo" class="form-control" placeholder="Logo..."/>
                  @else

                    <img src="{{asset('public/uploads/website/banners/'.@$conf->principal_photo)}}" class="img-fluid" style="max-height: 120px;">
                  @endif
              </div>
              <div class="form-group col-md-12">
                  <label for="principal_description">Description</label>
                  <textarea name="principal_description" class="form-control">{{@$conf->principal_description}}</textarea>
              </div>
            </div>
            </div>
          </div>
        </div>


        <div class="col-6 mt-2">
          <div class="card">
            <div class="card-body">
              <h4>Why choose us</h4>
              <div class="form-row">
              <div class="form-group col-md-12">
                  <label for="why_choose_us">Why Choose Us</label>
                  <textarea name="why_choose_us" class="form-control">{{@$conf->why_choose_us}}</textarea>
              </div>
              <div class="form-group col-md-4">
                  <label for="books_and_library">Books & Library</label>
                  <textarea name="books_and_library" class="form-control">{{@$conf->books_and_library}}</textarea>
              </div>

              <div class="form-group col-md-4">
                  <label for="quality_teachers">Quanlity Teachers</label>
                  <textarea name="quality_teachers" class="form-control">{{@$conf->quality_teachers}}</textarea>
              </div>

              <div class="form-group col-md-4">
                  <label for="great_certification">Great Certification</label>
                  <textarea name="great_certification" class="form-control">{{@$conf->great_certification}}</textarea>
              </div>
            </div>
            </div>
          </div>
        </div>

        <div class="col-6 mt-2">
          <div class="card">
            <div class="card-body">
              <div class="d-flex justify-content-between">
                <h4>Counter Section</h4>
                <button type="button" class="btn btn-primary btn-sm" id="add-field">Add Label</button>
              </div>
              <div id="dynamic-fields">
                @foreach($labels as $index => $label)
                <div class="form-group dynamic-field d-flex justify-content-between align-items-center mt-2">
                    <label for="labels[${index}]">Label</label>
                    <input type="text" class="form-control form-control-sm" name="labels[${index}][label]" value="{{$label->label}}">
                    <label for="values[${index}]">Value</label>
                    <input type="text" class="form-control form-control-sm" name="labels[${index}][value]" value="{{$label->value}}">
                    <button type="button" class="btn btn-danger btn-sm remove-field">Remove</button>
                </div>
                @endforeach
              </div>
            </div>
            </div>
          </div>

          <div class="col-6 mt-2">
          <div class="card">
            <div class="card-body">
              <h4>Our Expert Teachers Description</h4>
              <div class="form-row">
              <div class="form-group col-md-12">
                  <label for="teacher_section_description">Description</label>
                  <textarea class="form-control" id="teacher_section_description" name="teacher_section_description">{{@$conf->teacher_section_description}}</textarea>
              </div>
            </div>
            </div>
            </div>
          </div>

          <div class="col-6 mt-2">
          <div class="card">
            <div class="card-body">
              <h4>Image Gallery Description</h4>
              <div class="form-row">
              <div class="form-group col-md-12">
                  <label for="image_gallery_description">Description</label>
                  <textarea class="form-control" id="image_gallery_description" name="image_gallery_description">{{@$conf->image_gallery_description}}</textarea>
              </div>
            </div>
            </div>
            </div>
          </div>

        </div>
    <button type="submit" class="btn btn-primary save-btn">Save</button>
    </form>

    <div class="row mt-2">
        <div class="col-6">
          <div class="card">
            <div class="card-body">
              <div class="d-flex justify-content-between mb-4">
                <h4>Our Expert Teachers</h4>
                <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#addTeacherModal">+Add Teacher</button>
              </div>
            <div class="form-row">
              <div class="form-group col-md-12">
                <table class="table table-striped nowrap table-teachers">
                <thead>
                <tr>
                    <th>Action</th>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Image</th>
                </tr>
                </thead>
                </table>
              </div>
            </div>
            </div>
          </div>
        </div>

        <div class="col-6">
          <div class="card">
            <div class="card-body">
              <div class="d-flex justify-content-between mb-4">
                <h4>Images Gallery</h4>
                <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#addImageGalleryModal">+Add Gallery Image</button>
              </div>
            <div class="form-row">
              <div class="form-group col-md-12">
                <table class="table table-striped nowrap table-image-gallery">
                <thead>
                <tr>
                    <th>Action</th>
                    <th>Image</th>
                </tr>
                </thead>
                </table>
              </div>
            </div>
            </div>
          </div>
        </div>

    </div>
</div> 

<!--Add Student Modal -->
<div class="modal fade" id="addTeacherModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add Teacher</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form id="add-teacher-form" method="POST" enctype="multipart/form-data">
        @csrf
      <div class="modal-body ">
          <div class="form-row">
            <div class="form-group col-md-6">
                <label for="name">Name</label>
                <input type="text" name="name" id="name" class="form-control" placeholder="Course Name..."/>
            </div>

            <div class="form-group col-md-6">
                <label for="teacher_image">Image</label>
                <input type="file" name="teacher_image" id="teacher_image" class="form-control"/>
            </div>

            <div class="form-group col-md-12">
                <label for="teacher_description">Description</label>
                <textarea class="form-control" id="teacher_description" name="teacher_description"></textarea>
            </div>
            <input type="hidden" name="configuration_id" value="{{@$conf->id}}">
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary save-btn">Add</button>
      </div>
      </form>
    </div>
  </div>
</div> 

<!--Add Images Gallery Modal -->
<div class="modal fade" id="addImageGalleryModal" tabindex="-1" role="dialog" aria-labelledby="imageGalleryModal" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="imageGalleryModal">Add Gallery Image</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form id="add-gallery-form" method="POST" enctype="multipart/form-data">
        @csrf
      <div class="modal-body ">
          <div class="form-row">
            <div class="form-group col-md-6">
                <label for="gallery_image">Image</label>
                <input type="file" name="gallery_image" id="gallery_image" class="form-control"/>
            </div>
            <input type="hidden" name="configuration_id" value="{{@$conf->id}}">
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary save-btn">Add</button>
      </div>
      </form>
    </div>
  </div>
</div> 

@endsection

@section('javascript')
<script type="text/javascript">
  $(document).ready(function () {
        $('#add-field').click(function () {
            let index = $('.dynamic-field').length;
            let fieldHtml = `
                <div class="form-group dynamic-field d-flex justify-content-between align-items-center mt-2">
                    <label for="labels[${index}]">Label</label>
                    <input type="text" class="form-control form-control-sm" name="labels[${index}][label]" required>
                    <label for="values[${index}]">Value</label>
                    <input type="text" class="form-control form-control-sm" name="labels[${index}][value]" required>
                    <button type="button" class="btn btn-danger btn-sm remove-field">Remove</button>
                </div>
            `;
            $('#dynamic-fields').append(fieldHtml);
        });

        $(document).on('click', '.remove-field', function () {
            $(this).parent('.dynamic-field').remove();
        });

        var table2 =  $('.table-teachers').DataTable({
          processing: true,
          searching: true,
          "language": {
          processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw" style="color:#13436c;"></i><span class="sr-only">Loading...</span> '},
          ordering: false,
          pageLength: {{50}},
          serverSide: true,
          autoWidth: false,
          "lengthMenu": [50,100,150,200],
          
          scrollX:true,
          scrollY : '90vh',
          scrollCollapse: true,
          ajax: 
          {
            beforeSend: function(){
              $('#loader_modal').modal({
                backdrop: 'static',
                keyboard: false
              });
              $('#loader_modal').modal('show');
            },
            data: function(data){
              data.configuration_id = "{{ @$conf->id }}"
            },
            url: "{!! route('get-website-teachers') !!}",
          },
          columns: [
              { data: 'action', name: 'action' },  
              { data: 'name', name: 'name' },  
              { data: 'description', name: 'description' }, 
              { data: 'photo', name: 'photo' }, 
          ],
          initComplete: function () {
          // Enable THEAD scroll bars
          $('.dataTables_scrollHead').css('overflow', 'auto');
          // Sync THEAD scrolling with TBODY
            $('.dataTables_scrollHead').on('scroll', function () {
                $('.dataTables_scrollBody').scrollLeft($(this).scrollLeft());
            });
          },
          drawCallback: function(){
            $('#loader_modal').modal('hide');
          },
        });

        $('#add-teacher-form').on('submit',function(e){
          e.preventDefault();

           e.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            });
             $.ajax({
                url: "{{ route('add-website-teacher') }}",
                dataType: 'json',
                type: 'post',
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData:false,
                beforeSend: function(){
                  $('.save-btn').addClass('disabled');
                  $('.save-btn').attr('disabled', true);
                  $('#loader_modal').modal({
                    backdrop: 'static',
                    keyboard: false
                  });
                  $('#loader_modal').modal('show');
                },
                success: function(result){
                  $('.save-btn').attr('disabled', true);
                  $('.save-btn').removeAttr('disabled');
                  if(result.success == true){
                    toastr.success('Success!', 'Teacher Added Successfully!!!',{"positionClass": "toast-bottom-right"});
                    $('.table-teachers').DataTable().ajax.reload();

                    $('#add-teacher-form')[0].reset();
                    $('#addOfferCourse').modal('hide');
                  }
                  else{
                    toastr.error('Error!', result.errormsg,{"positionClass": "toast-bottom-right"});
                    $('#loader_modal').modal('hide');
                  }

                },
                error: function (request, status, error) {
                      /*$('.form-control').removeClass('is-invalid');
                      $('.form-control').next().remove();*/
                      $('#loader_modal').modal('hide');
                      $('.save-btn').removeClass('disabled');
                      $('.save-btn').removeAttr('disabled');
                      json = $.parseJSON(request.responseText);
                      $.each(json.errors, function(key, value){
                            $('input[name="'+key+'"]').after('<span class="invalid-feedback" role="alert"><strong>'+value+'</strong>');
                           $('input[name="'+key+'"]').addClass('is-invalid');
                           $('textarea[name="'+key+'"]').after('<span class="invalid-feedback" role="alert"><strong>'+value+'</strong>');
                           $('textarea[name="'+key+'"]').addClass('is-invalid');


                      });
                  }
              });
        });

        var table2 =  $('.table-image-gallery').DataTable({
          processing: true,
          searching: true,
          "language": {
          processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw" style="color:#13436c;"></i><span class="sr-only">Loading...</span> '},
          ordering: false,
          pageLength: {{50}},
          serverSide: true,
          autoWidth: false,
          "lengthMenu": [50,100,150,200],
          
          scrollX:true,
          scrollY : '90vh',
          scrollCollapse: true,
          ajax: 
          {
            beforeSend: function(){
              $('#loader_modal').modal({
                backdrop: 'static',
                keyboard: false
              });
              $('#loader_modal').modal('show');
            },
            data: function(data){
              data.configuration_id = "{{ @$conf->id }}"
            },
            url: "{!! route('get-website-gallery') !!}",
          },
          columns: [
              { data: 'action', name: 'action' },
              { data: 'photo', name: 'photo' }, 
          ],
          initComplete: function () {
          // Enable THEAD scroll bars
          $('.dataTables_scrollHead').css('overflow', 'auto');
          // Sync THEAD scrolling with TBODY
            $('.dataTables_scrollHead').on('scroll', function () {
                $('.dataTables_scrollBody').scrollLeft($(this).scrollLeft());
            });
          },
          drawCallback: function(){
            $('#loader_modal').modal('hide');
          },
        });

        $('#add-gallery-form').on('submit',function(e){
          e.preventDefault();

           e.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            });
             $.ajax({
                url: "{{ route('add-website-gallery') }}",
                dataType: 'json',
                type: 'post',
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData:false,
                beforeSend: function(){
                  $('.save-btn').addClass('disabled');
                  $('.save-btn').attr('disabled', true);
                  $('#loader_modal').modal({
                    backdrop: 'static',
                    keyboard: false
                  });
                  $('#loader_modal').modal('show');
                },
                success: function(result){
                  $('.save-btn').attr('disabled', true);
                  $('.save-btn').removeAttr('disabled');
                  if(result.success == true){
                    toastr.success('Success!', 'Image added successfully!!!',{"positionClass": "toast-bottom-right"});
                    $('.table-image-gallery').DataTable().ajax.reload();

                    $('#add-gallery-form')[0].reset();
                    $('#addImageGalleryModal').modal('hide');
                  }
                  else{
                    toastr.error('Error!', result.errormsg,{"positionClass": "toast-bottom-right"});
                    $('#loader_modal').modal('hide');
                  }

                },
                error: function (request, status, error) {
                      /*$('.form-control').removeClass('is-invalid');
                      $('.form-control').next().remove();*/
                      $('#loader_modal').modal('hide');
                      $('.save-btn').removeClass('disabled');
                      $('.save-btn').removeAttr('disabled');
                      json = $.parseJSON(request.responseText);
                      $.each(json.errors, function(key, value){
                            $('input[name="'+key+'"]').after('<span class="invalid-feedback" role="alert"><strong>'+value+'</strong>');
                           $('input[name="'+key+'"]').addClass('is-invalid');
                           $('textarea[name="'+key+'"]').after('<span class="invalid-feedback" role="alert"><strong>'+value+'</strong>');
                           $('textarea[name="'+key+'"]').addClass('is-invalid');


                      });
                  }
              });
        });
    });
$(document).ready(function(){
  $('#add-school-form').on('submit',function(e){
    e.preventDefault();
      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
          }
      });
       $.ajax({
          url: "{{ route('add-website-configuration') }}",
          dataType: 'json',
          type: 'post',
          data: new FormData(this),
          contentType: false,
          cache: false,
          processData:false,
          beforeSend: function(){
            $('.save-btn').addClass('disabled');
            $('.save-btn').attr('disabled', true);
            $('#loader_modal').modal({
              backdrop: 'static',
              keyboard: false
            });
            $('#loader_modal').modal('show');
          },
          success: function(result){
            $('.save-btn').attr('disabled', true);
            $('.save-btn').removeAttr('disabled');
            if(result.success == true){
              toastr.success('Success!', 'Website Configuration Added Successfully!!!',{"positionClass": "toast-bottom-right"});
              // $('.table-teachers').DataTable().ajax.reload();
              setTimeout(function(){
                window.location.reload();
              }, 2000);

              $('#add-school-form')[0].reset();
              $('#addWebsiteConfiguration').modal('hide');
            }
            else if(result.success == false)
            {
              toastr.info('Sorry!', 'One Configuration Can Be Registered At A Time , Please Click The Edit Icon In Order To Update The Current Configuration!!!',{"positionClass": "toast-bottom-right"});
              $('.table-teachers').DataTable().ajax.reload();
              $('#add-school-form')[0].reset();
              $('#addWebsiteConfiguration').modal('hide');
            }
            else{
              toastr.error('Error!', result.errormsg,{"positionClass": "toast-bottom-right"});
              $('#loader_modal').modal('hide');
            }

          },
          error: function (request, status, error) {
                /*$('.form-control').removeClass('is-invalid');
                $('.form-control').next().remove();*/
                $('#loader_modal').modal('hide');
                $('.save-btn').removeClass('disabled');
                $('.save-btn').removeAttr('disabled');
                json = $.parseJSON(request.responseText);
                $.each(json.errors, function(key, value){
                      $('input[name="'+key+'"]').after('<span class="invalid-feedback" role="alert"><strong>'+value+'</strong>');
                     $('input[name="'+key+'"]').addClass('is-invalid');
                     $('textarea[name="'+key+'"]').after('<span class="invalid-feedback" role="alert"><strong>'+value+'</strong>');
                     $('textarea[name="'+key+'"]').addClass('is-invalid');


                });
            }
        });
  });

  $(document).on('click','.delete-teacher',function(){
      var id = $(this).data('id');

        swal({
            title: "Alert!",
            html: true,
            text: "<b>Are you sure!!! You want to delete this record !!!</b>",
            type: "info",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes!",
            cancelButtonText: "No!",
            closeOnConfirm: true,
            closeOnCancel: false
          },
            function(isConfirm) {
            if (isConfirm){
             $.ajaxSetup({
              headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
              }
            });
            $.ajax({
              url: "{{ route('delete-website-teacher') }}",
              method: 'delete',
              data: {id:id},
              beforeSend: function(){
                // $('#loader_modal').modal({
                //   backdrop: 'static',
                //   keyboard: false
                // });
                // $('#loader_modal').modal('show');
              },
              success: function (response) {
                if(response.success == true)
                {
                  toastr.success('Success!', 'Record Deleted Successfully!!!',{"positionClass": "toast-bottom-right"});
                  $('.table-teachers').DataTable().ajax.reload();
                  $('#loader_modal').modal('hide');
                  return;
                }
                toastr.error('Sorry!', response.msg,{"positionClass": "toast-bottom-right"});
                return;
              },
              error: function(request, status, error){
                $('#loader_modal').modal('hide');
              }
            });
            }
            else{
              swal("Cancelled", "", "error");
              
            }
          });
  });

  $(document).on('click','.delete-website-gallery',function(){
      var id = $(this).data('id');

        swal({
            title: "Alert!",
            html: true,
            text: "<b>Are you sure!!! You want to delete this record !!!</b>",
            type: "info",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes!",
            cancelButtonText: "No!",
            closeOnConfirm: true,
            closeOnCancel: false
          },
            function(isConfirm) {
            if (isConfirm){
             $.ajaxSetup({
              headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
              }
            });
            $.ajax({
              url: "{{ route('delete-website-gallery') }}",
              method: 'delete',
              data: {id:id},
              beforeSend: function(){
                $('#loader_modal').modal({
                  backdrop: 'static',
                  keyboard: false
                });
                $('#loader_modal').modal('show');
              },
              success: function (response) {
                if(response.success == true)
                {
                  toastr.success('Success!', 'Record Deleted Successfully!!!',{"positionClass": "toast-bottom-right"});
                  $('.table-image-gallery').DataTable().ajax.reload();
                  $('#loader_modal').modal('hide');
                  return;
                }
                toastr.error('Sorry!', response.msg,{"positionClass": "toast-bottom-right"});
                return;
              },
              error: function(request, status, error){
                $('#loader_modal').modal('hide');
              }
            });
            }
            else{
              swal("Cancelled", "", "error");
              
            }
          });
  });

  $(document).on('click','.delete-image',function(){
      var type = $(this).data('type');

        swal({
            title: "Alert!",
            html: true,
            text: "<b>Are you sure!!! You want to delete this image !!!</b>",
            type: "info",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes!",
            cancelButtonText: "No!",
            closeOnConfirm: true,
            closeOnCancel: false
          },
            function(isConfirm) {
            if (isConfirm){
             $.ajaxSetup({
              headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
              }
            });
            $.ajax({
              url: "{{ route('delete-configuration-image') }}",
              method: 'delete',
              data: {type:type, id: "{{@$conf->id}}"},
              beforeSend: function(){
                $('#loader_modal').modal({
                  backdrop: 'static',
                  keyboard: false
                });
                $('#loader_modal').modal('show');
              },
              success: function (response) {
                if(response.success == true)
                {
                  location.reload();
                  return;
                }
                toastr.error('Sorry!', response.msg,{"positionClass": "toast-bottom-right"});
                return;
              },
              error: function(request, status, error){
                $('#loader_modal').modal('hide');
              }
            });
            }
            else{
              swal("Cancelled", "", "error");
              
            }
          });
  });

});
</script>
@stop

