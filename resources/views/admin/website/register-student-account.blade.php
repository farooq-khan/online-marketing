@extends('user.layouts.layout')

@section('title','Online Marketing')

@section('content')
<style type="text/css">
   
</style>
    
          <div id="studentAccount" class="row mx-0 justify-content-center signup-content">
                    <div class="signup-form card col-xl-6 col-7 col-md-8 col-12 px-0">
                    <div class="signup-form card-body">
                        {{-- <h2 class="form-title" style="color: black;">Sign up</h2> --}}
                        <form class="register-form add-student-form" id="register-form">
                              <div class="row">
                                <div class="col-6 form-group">
                                  <label for="roll_no">Select Student</label>
                                    <select class="form-control form-control-sm" id="student_admission_no" name="name" style="display: block !important;width: 100%;height: auto !important;">
                                      <option value="">--Select Student--</option>
                                      @foreach($students as $std)
                                      <option value="{{$std->id}}"> {{$std->roll_no}} ({{$std->name}}) (class {{$std->student_class->class_name}}) </option>
                                      @endforeach
                                    </select>
                                </div>
                                <div class="col-6 form-group">
                                      <label for="student_email">Email</label>
                                      <input type="email" name="email" id="student_email" class="form-control" placeholder="Student Email"/>
                                </div>
                                <div class="col-6 form-group">
                                      <label for="student_pass">Password</label>
                                      <input type="password" name="pass" id="student_pass" placeholder="Password" class="form-control" />
                                </div>
                                <div class="col-6 form-group">
                                      <label for="student_repass">Confirm password</label>
                                      <input type="password" name="re_pass" id="student_repass" class="form-control" placeholder="Repeat student password"/>
                                </div>
                              </div>
                            <div class="text-right">
                                <input type="submit" name="signup" id="signup" class="btn form-submit save-btn-student btn-primary" value="Sign Up"/>
                            </div>
                        </form>
                    </div>
                    </div>
          </div>

    
@endsection

@section('javascript')
<script type="text/javascript">
$(document).ready(function(){
    $('#student_admission_no').select2();

  var validate_email = function(email){
  var pattern = /^([a-zA-A0-9_.-])+@([a-zA-Z0-9_.-])+([a-zA-Z])+/;
  var is_email_valid = false;
  if(email.match(pattern) != null){
    is_email_valid = true;
  }
  return is_email_valid;
}

$(document).on("keyup", "#email", function(event){
  var keypressed = event.which;
  var input_val = $(this).val();
  var is_success = true;
  if(keypressed == 9){
    is_success = validate_email(input_val);   
    if(!is_success){
      // alert('Enter Valid Email');
            toastr.warning('Please!', 'Enter Valid Email !!',{"positionClass": "toast-bottom-right"});

    }
  }
});

$(document).on("focusout", "#email", function(){
  var input_val = $(this).val();
  var is_success = validate_email(input_val); 

  if(!is_success){
    // $("#email").focus();
      // alert('Enter Valid Email');
            toastr.warning('Please!', 'Enter Valid Email !!',{"positionClass": "toast-bottom-right"});
      
  }
});

    $(document).on('click', '.save-btn-student', function(e){
      e.preventDefault();
        var new_password= $("#student_pass").val();
        var confirm_new_password= $("#student_repass").val();
        if(new_password != confirm_new_password)
        {
            toastr.error('Alert!', 'Passwords does not match !!',{"positionClass": "toast-bottom-right"});
            return false;
        }     
      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
          }
      });
       $.ajax({
          url: "{{ route('add-users-student') }}",
          method: 'post',
          data: $('.add-student-form').serialize(),
          beforeSend: function(){
            $('#loader_modal').modal({
              backdrop: 'static',
              keyboard: false
            });
            $("#loader_modal").modal('show');
            $('.save-btn').val('Please wait...');
            $('.save-btn').addClass('disabled');
            $('.save-btn').attr('disabled', true);
          },
          success: function(result){
            $("#loader_modal").modal('hide');
            $('.save-btn').val('add');
            $('.save-btn').attr('disabled', true);
            $('.save-btn').removeAttr('disabled');
            if(result.success === true){
              $('.modal').modal('hide');
              toastr.success('Success!', 'Student Account Created successfully',{"positionClass": "toast-bottom-right"});
              $('.add-student-form')[0].reset();

              // setTimeout(function(){ window.location.href = "{{route('login')}}"; }, 3000);
              setTimeout(function(){ window.location.href = "{{route('get-register-account')}}"; }, 3000);
            }
            else if (result.success === false) {
              toastr.success('Success!', result.message ,{"positionClass": "toast-bottom-right"});

            }


          },
          error: function (request, status, error) {
                $("#loader_modal").modal('hide');
                $('.save-btn').val('add');
                $('.save-btn').removeClass('disabled');
                $('.save-btn').removeAttr('disabled');
                $('.form-control').removeClass('is-invalid');
                $('.form-control').next().remove();
                json = $.parseJSON(request.responseText);
                $.each(json.errors, function(key, value){
                    $('input[name="'+key+'"]').after('<span class="invalid-feedback" role="alert"><strong>'+value+'</strong>');
                     $('input[name="'+key+'"]').addClass('is-invalid');

                     $('select[name="'+key+'"]').after('<span class="invalid-feedback" role="alert"><strong>'+value+'</strong>');
                     $('select[name="'+key+'"]').addClass('is-invalid');
                });

            }
        });
    });

});
</script>
@stop

