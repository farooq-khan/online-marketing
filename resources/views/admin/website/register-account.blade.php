@extends('user.layouts.layout')

@section('title','Online Marketing')

@section('content')
<style type="text/css">
   
</style>
    
      <div class="container bg-white" style="border-radius: 5px;margin-top: 40px;">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
          <li class="nav-item">
            <a class="nav-link active" data-toggle="tab" href="#userAccount"><b>Create User Account</b></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#studentAccount"><b>Create Student Account</b></a>
          </li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
          <div id="userAccount" class="tab-pane active"><br>
            <div class="signup-content" style="padding: 0;padding-bottom: 70px;">
                    <div class="signup-form">
                        <h2 class="form-title" style="color: black;">Sign up</h2>
                        <form class="register-form add-users-form" id="register-form">
                            <div class="form-group">
                                <label for="name"><i class="zmdi zmdi-account material-icons-name"></i></label>
                                <input type="text" name="name" id="name" placeholder="User Name" required="required" />
                            </div>
                            <div class="form-group">
                                <label for="email"><i class="zmdi zmdi-email"></i></label>
                                <input type="email" name="email" id="email" placeholder="User Email" required="required"/>
                            </div>
                            <div class="form-group">
                                <label for="role"><i class="zmdi zmdi-account"></i> Role</label>
                                <select name="role" id="role" class="form-control" required="required">
                                  <option disabled="true">--Select Role--</option>
                                  <option value="admin">Admin</option>
                                  <option value="user">User</option>
                                  <option value="teacher">Teacher</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="pass"><i class="zmdi zmdi-lock"></i></label>
                                <input type="password" name="pass" id="pass" placeholder="Password" required="required"/>
                            </div>
                            <div class="form-group">
                                <label for="re-pass"><i class="zmdi zmdi-lock-outline"></i></label>
                                <input type="password" name="re_pass" id="re_pass" placeholder="Repeat user password" required="required"/>
                            </div>
                            <div class="form-group">
                                <input type="checkbox" name="agree-term" id="agree-term" class="agree-term" />
                                <label for="agree-term" class="label-agree-term"><span><span></span></span>I agree all statements in  <a href="#" class="term-service">Terms of service</a></label>
                            </div>
                            <div class="form-group form-button">
                                <input type="submit" name="signup" id="signup" class="form-submit save-btn" value="Sign Up"/>
                            </div>
                        </form>
                    </div>
                    
                </div>
          </div>
          <div id="studentAccount" class="tab-pane fade"><br>
            <div class="signup-content" style="padding: 0;padding-bottom: 70px;">
                    <div class="signup-form">
                        {{-- <h2 class="form-title" style="color: black;">Sign up</h2> --}}
                        <form class="register-form add-student-form" id="register-form">
                             
                              <div class="row">
                                <div class="col-6">
                                  <label for="roll_no"><i class="zmdi zmdi-account material-icons-name pr-2"> Select Student</i></label>
                                    <select class="form-control form-control-sm" id="student_admission_no" name="name" style="display: block !important;width: 100%;height: auto !important;">
                                      <option value="">--Select Student--</option>
                                      @foreach($students as $std)
                                      <option value="{{$std->id}}"> {{$std->roll_no}} ({{$std->name}}) (class {{$std->student_class->class_name}}) </option>
                                      @endforeach
                                    </select>
                                </div>
                                <div class="col-6">
                                  <div class="form-group">
                                      <label for="student_email"><i class="zmdi zmdi-email"></i></label>
                                      <input type="email" name="email" id="student_email" class="form-control" placeholder="Student Email"/>
                                  </div>
                                </div>
                                <div class="col-6">
                                  <div class="form-group">
                                      <label for="student_pass"><i class="zmdi zmdi-lock"></i></label>
                                      <input type="password" name="pass" id="student_pass" placeholder="Password" class="form-control" />
                                  </div>
                                </div>
                                <div class="col-6">
                                  <div class="form-group">
                                      <label for="student_repass"><i class="zmdi zmdi-lock-outline"></i></label>
                                      <input type="password" name="re_pass" id="student_repass" class="form-control" placeholder="Repeat student password"/>
                                  </div>
                                </div>
                              </div>
                            <div class="form-group form-button">
                                <input type="submit" name="signup" id="signup" class="form-submit save-btn-student btn-primary btn-sm" value="Sign Up"/>
                            </div>
                        </form>
                    </div>
                    
                </div>
          </div>
        </div>
      </div>
    
@endsection

@section('javascript')
<script type="text/javascript">
$(document).ready(function(){
    $('#student_admission_no').select2();
  $(document).on('click', '.save-btn', function(e){
      e.preventDefault();


      // if($('#sponsor_id').val() == '')
      // {
      //   toastr.error('Sorry!', 'Sponsor No. is required',{"positionClass": "toast-bottom-right"});
      //   return false;
      // }

      if($('#agree-term').prop("checked") == false)
      {
        toastr.error('Alert!', 'Please accept terms of service',{"positionClass": "toast-bottom-right"});
        return false;
      }

        var new_password= $("#pass").val();
        var confirm_new_password= $("#re_pass").val();
        if(new_password != confirm_new_password)
        {
            toastr.error('Alert!', 'Passwords does not match !!',{"positionClass": "toast-bottom-right"});
            return false;
        }     
      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
          }
      });
       $.ajax({
          url: "{{ route('add-users') }}",
          method: 'post',
          data: $('.add-users-form').serialize(),
          beforeSend: function(){
            $('#loader_modal').modal({
              backdrop: 'static',
              keyboard: false
            });
            $("#loader_modal").modal('show');
            $('.save-btn').val('Please wait...');
            $('.save-btn').addClass('disabled');
            $('.save-btn').attr('disabled', true);
          },
          success: function(result){
            $("#loader_modal").modal('hide');
            $('.save-btn').val('add');
            $('.save-btn').attr('disabled', true);
            $('.save-btn').removeAttr('disabled');
            if(result.success === true){
              $('.modal').modal('hide');
              toastr.success('Success!', 'User added successfully',{"positionClass": "toast-bottom-right"});
              $('.add-users-form')[0].reset();

              setTimeout(function(){ window.location.href = "{{route('get-register-account')}}"; }, 3000);

              

            }
            else if (result.success === false) {
              toastr.success('Success!', result.message ,{"positionClass": "toast-bottom-right"});

            }


          },
          error: function (request, status, error) {
                $("#loader_modal").modal('hide');
                $('.save-btn').val('add');
                $('.save-btn').removeClass('disabled');
                $('.save-btn').removeAttr('disabled');
                $('.form-control').removeClass('is-invalid');
                $('.form-control').next().remove();
                json = $.parseJSON(request.responseText);
                $.each(json.errors, function(key, value){
                    $('input[name="'+key+'"]').after('<span class="invalid-feedback" role="alert"><strong>'+value+'</strong>');
                     $('input[name="'+key+'"]').addClass('is-invalid');

                     $('select[name="'+key+'"]').after('<span class="invalid-feedback" role="alert"><strong>'+value+'</strong>');
                     $('select[name="'+key+'"]').addClass('is-invalid');
                });

            }
        });
    });

  var validate_email = function(email){
  var pattern = /^([a-zA-A0-9_.-])+@([a-zA-Z0-9_.-])+([a-zA-Z])+/;
  var is_email_valid = false;
  if(email.match(pattern) != null){
    is_email_valid = true;
  }
  return is_email_valid;
}

$(document).on("keyup", "#email", function(event){
  var keypressed = event.which;
  var input_val = $(this).val();
  var is_success = true;
  if(keypressed == 9){
    is_success = validate_email(input_val);   
    if(!is_success){
      // alert('Enter Valid Email');
            toastr.warning('Please!', 'Enter Valid Email !!',{"positionClass": "toast-bottom-right"});

    }
  }
});

$(document).on("focusout", "#email", function(){
  var input_val = $(this).val();
  var is_success = validate_email(input_val); 

  if(!is_success){
    // $("#email").focus();
      // alert('Enter Valid Email');
            toastr.warning('Please!', 'Enter Valid Email !!',{"positionClass": "toast-bottom-right"});
      
  }
});

    $(document).on('click', '.save-btn-student', function(e){
      e.preventDefault();
        var new_password= $("#student_pass").val();
        var confirm_new_password= $("#student_repass").val();
        if(new_password != confirm_new_password)
        {
            toastr.error('Alert!', 'Passwords does not match !!',{"positionClass": "toast-bottom-right"});
            return false;
        }     
      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
          }
      });
       $.ajax({
          url: "{{ route('add-users-student') }}",
          method: 'post',
          data: $('.add-student-form').serialize(),
          beforeSend: function(){
            $('#loader_modal').modal({
              backdrop: 'static',
              keyboard: false
            });
            $("#loader_modal").modal('show');
            $('.save-btn').val('Please wait...');
            $('.save-btn').addClass('disabled');
            $('.save-btn').attr('disabled', true);
          },
          success: function(result){
            $("#loader_modal").modal('hide');
            $('.save-btn').val('add');
            $('.save-btn').attr('disabled', true);
            $('.save-btn').removeAttr('disabled');
            if(result.success === true){
              $('.modal').modal('hide');
              toastr.success('Success!', 'Student Account Created successfully',{"positionClass": "toast-bottom-right"});
              $('.add-student-form')[0].reset();

              // setTimeout(function(){ window.location.href = "{{route('login')}}"; }, 3000);
              setTimeout(function(){ window.location.href = "{{route('get-register-account')}}"; }, 3000);
            }
            else if (result.success === false) {
              toastr.success('Success!', result.message ,{"positionClass": "toast-bottom-right"});

            }


          },
          error: function (request, status, error) {
                $("#loader_modal").modal('hide');
                $('.save-btn').val('add');
                $('.save-btn').removeClass('disabled');
                $('.save-btn').removeAttr('disabled');
                $('.form-control').removeClass('is-invalid');
                $('.form-control').next().remove();
                json = $.parseJSON(request.responseText);
                $.each(json.errors, function(key, value){
                    $('input[name="'+key+'"]').after('<span class="invalid-feedback" role="alert"><strong>'+value+'</strong>');
                     $('input[name="'+key+'"]').addClass('is-invalid');

                     $('select[name="'+key+'"]').after('<span class="invalid-feedback" role="alert"><strong>'+value+'</strong>');
                     $('select[name="'+key+'"]').addClass('is-invalid');
                });

            }
        });
    });

});
</script>
@stop

