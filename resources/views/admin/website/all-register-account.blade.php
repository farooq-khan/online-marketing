@extends('user.layouts.layout')

@section('title','Add Exams')

@section('content')
<style type="text/css">
   
</style>

                    <div>
                        <div class="row page-title-row">
                            <div class="col-md col-sm col-12 page-title-col align-self-center">
                                <h1 class="page-title">All Registered Accounts</h1>
                                <p class="d-block mt-2 mb-0"><span style="color: red;">Note:</span> Double Click The Record For Updating</p>

                            </div>
                            
<!--                             <div class="col-sm col-12 mt-sm-0 mt-3 ml-auto text-right page-action-button">
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addBook">+Add Book</button>
                            </div>
                             -->
                    </div>

                     {{-- <div class="row pl-3" style="font-size: 18px;">
                      <span style="color: red;font-style: italic;">Note : </span> <span style="font-style: italic;padding-left: 5px;"> Double Click The Record For Updating</span>
                    </div> --}}

                                        {{-- <div class="row mb-4">
                      <div class="col-lg-12 col-md-12 title-col">
                        <div class="d-sm-flex justify-content-left align-items-center">

                          <div class="col-lg-3 col-md-3">
                            <select class="form-control form-control status_filter state-tags" name="status_filter" >
                              <option value="">-- Select Status --</option>
                               <option value="1">Active</option>
                               <option value="0">Pending</option>
                            </select>
                          </div>
                    
                          <div class="col-lg-1 col-md-1"></div>

                        </div>
                      </div>
                    </div> --}}


                        <div class="card p-4">
                          <div class="align-items-center form-group form-row row">
                            <label class="col-auto mb-0">Accounts Status</label>
                            <div class="col-auto">
                              <select class="form-control form-control status_filter state-tags" name="status_filter" >
                                <option value="">-- Select Status --</option>
                                <option value="1">Active</option>
                                <option value="0">Pending</option>
                              </select>
                            </div>
                          </div>
                            <div class="d-sm-flex justify-content-between">
                              <div class="delete-selected-item mb-4 d-none">
                                <a href="javascript:void(0);" class="btn selected-item-btn btn-sm delete_class deleteIcon" data-type="quotation" title="Delete Class" >
                                <i class="zmdi zmdi-delete" style="font-size: 25px;"></i></a>
                              </div>
                            </div>
                          <div class="table-responsive">
                            <table class="table table-striped nowrap table-accounts">
                            <thead>
                            <tr>
                                <th>Action</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Role</th>
                                <th>status</th>
                                <th>Created Date</th>
                            </tr>
                            </thead>
                            </table>
                          </div>
                        </div>
                    </div>


@endsection

@section('javascript')
<script type="text/javascript">

    var table2 =  $('.table-accounts').DataTable({
    processing: true,
    searching: true,
    "language": {
    processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw" style="color:#13436c;"></i><span class="sr-only">Loading...</span> '},
    ordering: false,
    pageLength: {{50}},
    serverSide: true,
    autoWidth: false,
    "lengthMenu": [50,100,150,200],
    
    scrollX:true,
    scrollY : '90vh',
    scrollCollapse: true,
    ajax: 
    {
      beforeSend: function(){
        $('#loader_modal').modal({
          backdrop: 'static',
          keyboard: false
        });
        $('#loader_modal').modal('show');
      },
      url: "{!! route('get-register-account-record') !!}",
      data: function(data) { data.status = $('.status_filter option:selected').val(), data.role = "{{$role}}"}
    },
    columns: [
        { data: 'action', name: 'action' },  
        { data: 'name', name: 'name' },  
        { data: 'email', name: 'email' },  
        { data: 'role', name: 'role' },  
        { data: 'status', name: 'status' },  
        { data: 'created_at', name: 'created_at' },  
    ],
    initComplete: function () {
    // Enable THEAD scroll bars
    // $(document).on('click', '.ellipsis-button', function (e) {
    //     e.stopPropagation(); // Prevent event from bubbling to the document
    //     $(this).parents('.dataTables_scrollBody').addClass('scrollBody-active');
    //     let dropdownHeight = $(this).siblings('.dropdown-menu').outerHeight();
    //     $('.scrollBody-active').css('min-height', dropdownHeight + 20 + 'px');
        
    // });
    $('.dataTables_scrollHead').css('overflow', 'auto');
    // Sync THEAD scrolling with TBODY
      $('.dataTables_scrollHead').on('scroll', function () {
          $('.dataTables_scrollBody').scrollLeft($(this).scrollLeft());
      });
    },
    drawCallback: function(){
      $('#loader_modal').modal('hide');
    },
  });

      $(document).on('click','.delete_account',function(){
      var id = $(this).data('id');

        swal({
            title: "Alert!",
            html: true,
            text: "<b>Are you sure!!! You want to delete this account !!!</b>",
            type: "info",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes!",
            cancelButtonText: "No!",
            closeOnConfirm: true,
            closeOnCancel: false
          },
            function(isConfirm) {
            if (isConfirm){
             $.ajaxSetup({
              headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
              }
            });
            $.ajax({
              url: "{{ route('delete-account') }}",
              method: 'post',
              data: {id:id},
              beforeSend: function(){
                $('#loader_modal').modal({
                  backdrop: 'static',
                  keyboard: false
                });
                $('#loader_modal').modal('show');
              },
              success: function (response) {
                if(response.success == true)
                {
                  toastr.success('Success!', 'Account Deleted Successfully!!!',{"positionClass": "toast-bottom-right"});
                  $('.table-accounts').DataTable().ajax.reload();
                  $('#loader_modal').modal('hide');
                }
               
              },
              error: function(request, status, error){
                $('#loader_modal').modal('hide');
              }
            });
            }
            else{
              swal("Cancelled", "", "error");
              
            }
          });
  });


  $(document).ready(function(){
    $(document).on("dblclick",".inputDoubleClick",function(){
    $(this).addClass('d-none');
    $(this).next().removeClass('d-none');
    $(this).next().addClass('active');
    $(this).next().focus();
  });

    $(document).on('keypress keyup focusout',".fieldFocus",function(e) {
    // alert('hi');
    var id = $(this).data('id');
    if (e.keyCode === 27 && $(this).hasClass('active')) {
      var fieldvalue = $(this).prev().data('fieldvalue');
      var thisPointer = $(this);
      thisPointer.addClass('d-none');
      thisPointer.val(fieldvalue);
      thisPointer.removeClass('active');
      thisPointer.prev().removeClass('d-none');
    }

    var name = $(this).attr('name');
    // alert(name);

    if( (e.keyCode === 13 || e.which === 0) && $(this).val().length > 0 && $(this).hasClass('active')){
      var str = $(this).val();
      if(($(this).val().length < 1 ||  !str.replace(/\s/g, '').length))
      {
        swal({ html:true, title:'Alert !!!', text:'<b>Please Enter At Least 1 Characters (this string may only contain white spaces)!!!</b>'});
        return false;
      }
      else
      {
        $(this).removeClass('active');
        var fieldvalue = $(this).prev().data('fieldvalue');
        var new_value = $(this).val();
        // alert(new_value);
        if(fieldvalue == new_value)
        {
          var thisPointer = $(this);
          thisPointer.addClass('d-none');
          thisPointer.prev().removeClass('d-none');
          $(this).prev().html(fieldvalue);
        }
        else
        {
          var thisPointer = $(this);
          thisPointer.addClass('d-none');
          thisPointer.prev().removeClass('d-none');
          if(new_value != '')
          {
            $(this).prev().html(new_value);
          }
          $(this).prev().data('fieldvalue', new_value);
          saveData(thisPointer,thisPointer.attr('name'), thisPointer.val(),fieldvalue,id);
        }
      }
    }

  });

      function saveData(thisPointer,field_name,field_value,new_select_value,id){

      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
      });
      $.ajax({
        method: "post",

        url: "{{ url('user/update-book-stock') }}",
        dataType: 'json',
        data: 'id='+id+'&'+field_name+'='+encodeURIComponent(field_value)+'&'+'new_select_value'+'='+encodeURIComponent(new_select_value),
        beforeSend: function(){
          $('#loader_modal').modal({
            backdrop: 'static',
            keyboard: false
          });
          $('#loader_modal').modal('show');
        },
        success: function(data)
        {
          $("#loader_modal").modal('hide');
          if(data.success == true)
          {
            
            toastr.success('Success!', 'Information updated successfully.',{"positionClass": "toast-bottom-right"});
            return true;
          }

          if(field_name == 'stock')
          {
              $('.table-accounts').DataTable().ajax.reload();
              $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
          }

        },

      });
    }

      $(document).on('click','.disable_account',function(){
      var id = $(this).data('id');

        swal({
            title: "Alert!",
            html: true,
            text: "<b>Are you sure!!! You want to disable this account !!!</b>",
            type: "info",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes!",
            cancelButtonText: "No!",
            closeOnConfirm: true,
            closeOnCancel: false
          },
            function(isConfirm) {
            if (isConfirm){
             $.ajaxSetup({
              headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
              }
            });
            $.ajax({
              url: "{{ route('disable-account') }}",
              method: 'post',
              data: {id:id},
              beforeSend: function(){
                $('#loader_modal').modal({
                  backdrop: 'static',
                  keyboard: false
                });
                $('#loader_modal').modal('show');
              },
              success: function (response) {

                if(response.success == true)
                {
                  toastr.success('Success!', 'Account Disabled Successfully!!!',{"positionClass": "toast-bottom-right"});
                  $('.table-accounts').DataTable().ajax.reload();
                  $('#loader_modal').modal('hide');
                }

                if(response.success == false)
                {
                  toastr.error('Sorry!', 'Something went wrong. Try Again !!!',{"positionClass": "toast-bottom-right"});
                  $('.table-accounts').DataTable().ajax.reload();
                  $('#loader_modal').modal('hide');
                }
               
              },
              error: function(request, status, error){
                $('#loader_modal').modal('hide');
              }
            });
            }
            else{
              swal("Cancelled", "", "error");
              
            }
          });
  });

      $(document).on('click','.activate_account',function(){
      var id = $(this).data('id');

        swal({
            title: "Alert!",
            html: true,
            text: "<b>Are you sure!!! You want to activate this account !!!</b>",
            type: "info",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes!",
            cancelButtonText: "No!",
            closeOnConfirm: true,
            closeOnCancel: false
          },
            function(isConfirm) {
            if (isConfirm){
             $.ajaxSetup({
              headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
              }
            });
            $.ajax({
              url: "{{ route('activate-account') }}",
              method: 'post',
              data: {id:id},
              beforeSend: function(){
                $('#loader_modal').modal({
                  backdrop: 'static',
                  keyboard: false
                });
                $('#loader_modal').modal('show');
              },
              success: function (response) {

                if(response.success == true)
                {
                  toastr.success('Success!', 'Account Activated Successfully!!!',{"positionClass": "toast-bottom-right"});
                  $('.table-accounts').DataTable().ajax.reload();
                  $('#loader_modal').modal('hide');
                }

                if(response.success == false)
                {
                  toastr.error('Sorry!', 'Something went wrong. Try Again !!!',{"positionClass": "toast-bottom-right"});
                  $('.table-accounts').DataTable().ajax.reload();
                  $('#loader_modal').modal('hide');
                }
               
              },
              error: function(request, status, error){
                $('#loader_modal').modal('hide');
              }
            });
            }
            else{
              swal("Cancelled", "", "error");
              
            }
          });
  });

    $(document).on('change','.status_filter',function(){
    $('.table-accounts').DataTable().ajax.reload();
    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
  });

  });
</script>
@stop

