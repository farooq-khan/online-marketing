@extends('user.layouts.layout')

@section('title','Single Student')

@section('content')
<style type="text/css">
.student-image
{
padding-top: 100%;
background-position: center;
background-size: cover;
}

.student_image
{
box-shadow: 0px 0px 15px 0px rgba(0,0,0,0.5);

}
</style>
<?php 
use Carbon\Carbon;
?>
<div class="container-fluid">
     <div class="row page-title-row">
          <div class="col-8">
              <h1 class="page-title">TimeTables</h1>
          </div>
          <div class="col d-flex justify-content-end">
            <a href="{{ route('timetables.create') }}" class="btn btn-primary">Create Timetable</a>
          </div>                           
      </div>

    <div class="row p-0 mt-0">
        <div class="col-12">
        <div class="card p-4">
        <div class="table-responsive">
        <table class="table table-striped nowrap table-student-other-fee-history text-center">
        <thead>
                <tr>
                    <th>Class</th>
                    <th>Subject</th>
                    <th>Teacher</th>
                    <th>Date</th>
                    <th>Start Time</th>
                    <th>End Time</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($timetables as $timetable)
                    <tr>
                        <td>{{ $timetable->class->class_name }}</td>
                        <td>{{ $timetable->subject->name }}</td>
                        <td>{{ $timetable->teacher->name }}</td>
                        <td>{{ $timetable->date }}</td>
                        <td>{{ $timetable->start_time }}</td>
                        <td>{{ $timetable->end_time }}</td>
                        <td>
                            <a href="{{ route('timetables.edit', $timetable->id) }}" class="btn btn-warning">Edit</a>
                            <form action="{{ route('timetables.destroy', $timetable->id) }}" method="POST" style="display:inline-block;">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-danger">Delete</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        </div>
        </div>
        </div>
    </div>
</div>
@endsection

@section('javascript')
<script type="text/javascript">

</script>
@stop

