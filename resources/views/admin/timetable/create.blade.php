@extends('user.layouts.layout')

@section('title','Single Student')

@section('content')
<style type="text/css">
.student-image
{
padding-top: 100%;
background-position: center;
background-size: cover;
}

.student_image
{
box-shadow: 0px 0px 15px 0px rgba(0,0,0,0.5);

}
</style>
<?php 
use Carbon\Carbon;
?>
<div class="container-fluid">
    <div class="row page-title-row">
      <div class="col-8">
          <h1 class="page-title">Create Timetable</h1>
      </div>                          
    </div>

    <div class="row p-0 mt-0">
        <div class="col-12 card p-4">
            <form action="{{ route('timetables.store') }}" method="POST">
            @csrf
            <div class="form-group">
                <label for="class_id">Class</label>
                <select name="class_id" id="class_id" class="form-control" onchange="fetchSubjects()">
                    <option>-- Select Class --</option>
                    @foreach ($classes as $class)
                        <option value="{{ $class->id }}">{{ $class->class_name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="subject_id">Subject</label>
                <select name="subject_id" id="subject_id" class="form-control">
                    <option value="">Select Subject</option>
                </select>
            </div>
            <div class="form-group">
                <label for="teacher_id">Teacher</label>
                <select name="teacher_id" id="teacher_id" class="form-control">
                    @foreach ($teachers as $teacher)
                        <option value="{{ $teacher->id }}">{{ $teacher->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="date">Date</label>
                <input type="date" name="date" id="date" class="form-control">
            </div>
            <div class="form-group">
                <label for="start_time">Start Time</label>
                <input type="time" name="start_time" id="start_time" class="form-control">
            </div>
            <div class="form-group">
                <label for="end_time">End Time</label>
                <input type="time" name="end_time" id="end_time" class="form-control">
            </div>
            <input type="hidden" name="school_id" value="{{auth()->user()->school_id}}">
            <button type="submit" class="btn btn-primary">Create</button>
        </form>
        </div>
    </div>
@endsection

@section('javascript')
<script type="text/javascript">
function fetchSubjects() {
            const classId = document.getElementById('class_id').value;
            const subjectSelect = document.getElementById('subject_id');
            
            subjectSelect.innerHTML = '<option value="">Select Subject</option>'; // Clear previous subjects

            // if (classId) {
            //     fetch(``)
            //         .then(response => response.json())
            //         .then(data => {
            //             data.forEach(subject => {
            //                 const option = document.createElement('option');
            //                 option.value = subject.id;
            //                 option.textContent = subject.name;
            //                 subjectSelect.appendChild(option);
            //             });
            //         });
            // }
      
        $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
        });
        $.ajax({
        method: "get",

        url: "{{ route('get-class-subjects') }}",
        dataType: 'json',
        data: 'id='+classId,
        beforeSend: function(){
          $('#loader_modal').modal({
            backdrop: 'static',
            keyboard: false
          });
          $('#loader_modal').modal('show');
        },
        success: function(data)
        {
            data.forEach(subject => {
                const option = document.createElement('option');
                option.value = subject.id;
                option.textContent = subject.name;
                subjectSelect.appendChild(option);
            });
            $('#loader_modal').modal('hide');
        },

        });
        }
</script>
@stop

