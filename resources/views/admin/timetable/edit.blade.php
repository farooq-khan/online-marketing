@extends('user.layouts.layout')

@section('title','Single Student')

@section('content')
<style type="text/css">
.student-image
{
padding-top: 100%;
background-position: center;
background-size: cover;
}

.student_image
{
box-shadow: 0px 0px 15px 0px rgba(0,0,0,0.5);

}
</style>
<?php 
use Carbon\Carbon;
?>
    <div class="container">
        <h1>Edit Timetable</h1>
        <form action="{{ route('timetables.update', $timetable->id) }}" method="POST">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label for="class_id">Class</label>
                <select name="class_id" id="class_id" class="form-control">
                    @foreach ($classes as $class)
                        <option value="{{ $class->id }}" {{ $class->id == $timetable->class_id ? 'selected' : '' }}>{{ $class->class_name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="subject_id">Subject</label>
                <select name="subject_id" id="subject_id" class="form-control">
                    @foreach ($subjects as $subject)
                        <option value="{{ $subject->id }}" {{ $subject->id == $timetable->subject_id ? 'selected' : '' }}>{{ $subject->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="teacher_id">Teacher</label>
                <select name="teacher_id" id="teacher_id" class="form-control">
                    @foreach ($teachers as $teacher)
                        <option value="{{ $teacher->id }}" {{ $teacher->id == $timetable->teacher_id ? 'selected' : '' }}>{{ $teacher->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="date">Date</label>
                <input type="date" name="date" id="date" class="form-control" value="{{ $timetable->date }}">
            </div>
            <div class="form-group">
                <label for="start_time">Start Time</label>
                <input type="time" name="start_time" id="start_time" class="form-control" value="{{ $timetable->start_time }}">
            </div>
            <div class="form-group">
                <label for="end_time">End Time</label>
                <input type="time" name="end_time" id="end_time" class="form-control" value="{{ $timetable->end_time }}">
            </div>
            <button type="submit" class="btn btn-primary">Update</button>
        </form>
    </div>
@endsection

@section('javascript')
<script type="text/javascript">

</script>
@stop

