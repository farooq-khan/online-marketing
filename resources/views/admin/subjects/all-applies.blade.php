@extends('user.layouts.layout')

@section('title','Online Marketing')

@section('content')
<style type="text/css">
   
</style>

                    <div>
                        <div class="row page-title-row">
                            <div class="col-md col-sm col-12 page-title-col align-self-center">
                                <h1 class="page-title">All Online Applies</h1>
                            </div>
                            
                    </div>

                    <div class="row">
                        <div class="col-12 mt-4">
                    
                        <div class="card p-4">
                          <div class="d-sm-flex justify-content-between">
                         <div class="selected-item catalogue-btn-group mb-4 mt-sm-3 ml-3 d-none">
    
                            <a href="javascript:void(0)" class="actionicon viewIcon download_invoice" data-id="1" title="Delete Records"><i class="fa fa-trash-alt"></i></a>

                        </div>
                        </div>
                        <div class="table-responsive">
                        <table class="table table-striped nowrap table-school ">
                        <thead>
                        <tr>
                            <th class="noVis">
                              <div class="custom-control custom-checkbox custom-checkbox1 d-inline-block">
                                <input type="checkbox" class="custom-control-input check-all1" name="check_all" id="check-all">
                                <label class="custom-control-label" for="check-all"></label>
                              </div>
                            </th>
                            <!-- <th>Action</th> -->
                            <th>Name</th>
                            <th>Father Name</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>Course</th>
                            <th>Apply Date</th>
                        </tr>
                        </thead>
                        </table>
                        </div>
                    </div>
                </div>
                    </div>
                    </div>


<!--Add Student Modal -->
<div class="modal fade" id="addOfferCourse" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add Course</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form id="add-course-form" method="POST" enctype="multipart/form-data">
        @csrf
      <div class="modal-body ">
          <div class="form-row">
            <div class="form-group col-md-6">
                <label for="name">Name</label>
                <input type="text" name="name" id="name" class="form-control" placeholder="Course Name..."/>
            </div>
            <div class="form-group col-md-6">
                <label for="course_fee">Course Fee</label>
                <input type="number" name="course_fee" id="course_fee" class="form-control" placeholder="Course Fee..."/>
            </div>

             <div class="form-group col-md-6">
                <label for="duration">Duration</label>
                <input type="text" name="duration" id="duration" class="form-control" placeholder="i.e 1 Year..."/>
            </div>


            <div class="form-group col-md-6">
                <label for="image">Image</label>
                <input type="file" name="image" id="image" class="form-control" placeholder="School Logo..."/>
            </div>
          </div>
          
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary save-btn">Add</button>
      </div>
      </form>
    </div>
  </div>
</div>
  <input type="hidden" name="fees_selected" class="fees_selected">

@endsection

@section('javascript')
<script type="text/javascript">
$(document).ready(function(){
  $(document).on('click', '.check-all1', function () {
        if(this.checked == true){
        $('.check').prop('checked', true);
        $('.check').parents('tr').addClass('selected');
        var cb_length = $( ".check:checked" ).length;
        if(cb_length > 0){
          $('.selected-item').removeClass('d-none');
        }
      }else{
        $('.check').prop('checked', false);
        $('.check').parents('tr').removeClass('selected');
        $('.selected-item').addClass('d-none');
        
      }
    });

   $(document).on('click', '.check', function () {
    // $(this).removeClass('d-none');
        var cb_length = $( ".check:checked" ).length;
        var st_pieces = $(this).parents('tr').attr('data-pieces');
        if(this.checked == true){
        $('.selected-item').removeClass('d-none');
        $(this).parents('tr').addClass('selected');
      }else{
        $(this).parents('tr').removeClass('selected');
        if(cb_length == 0){
         $('.selected-item').addClass('d-none');
        }
        
      }
    });

       $(document).on('click', '.download_invoice', function(){
    var selected_fees = [];
    $("input.check:checked").each(function() {
      selected_fees.push($(this).val());
    });

    if(selected_fees == ''){
            toastr.error('Error!', 'Select Fees First.',{"positionClass": "toast-bottom-right"});
        return false;
        }else{
        $('.fees_selected').val(selected_fees);

        var fees = $('.fees_selected').val();

        $.ajaxSetup({
              headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
              }
            });
            $.ajax({
              url: "{{ route('delete-online-applies') }}",
              method: 'post',
              data: {ids:fees},
              beforeSend: function(){
                $('#loader_modal').modal({
                  backdrop: 'static',
                  keyboard: false
                });
                $('#loader_modal').modal('show');
              },
              success: function (response) {
                if(response.success == true)
                {
                  toastr.success('Success!', 'Record Deleted Successfully!!!',{"positionClass": "toast-bottom-right"});
                  $('.selected-item').addClass('d-none');
                  $('.table-school').DataTable().ajax.reload();

                }
                  $('#loader_modal').modal('hide');

               
              },
              error: function(request, status, error){
                // $('#loader_modal').modal('hide');
              }
            });
        // console.log(orders);
        // return false;
         // $('.export-account-received-form')[0].submit();
         // var url = "{{url('user/fee-invoice-print')}}"+"/"+fees;
          // window.open(url, 'Orders Receivable Print', 'width=1200,height=600,scrollbars=yes');
       }

    });

  var table2 =  $('.table-school').DataTable({
    processing: true,
    searching: true,
    "language": {
    processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw" style="color:#13436c;"></i><span class="sr-only">Loading...</span> '},
    ordering: false,
    pageLength: {{50}},
    serverSide: true,
    autoWidth: false,
    "lengthMenu": [50,100,150,200],
    
    scrollX:true,
    scrollY : '90vh',
    scrollCollapse: true,
    ajax: 
    {
      beforeSend: function(){
        $('#loader_modal').modal({
          backdrop: 'static',
          keyboard: false
        });
        $('#loader_modal').modal('show');
      },
      url: "{!! route('get-all-online-applies') !!}",
    },
    columns: [
        { data: 'checkbox', name: 'checkbox' },  
        { data: 'name', name: 'name' },  
        { data: 'fathername', name: 'fathername' }, 
        { data: 'email', name: 'email' }, 
        { data: 'phone', name: 'phone' }, 
        { data: 'course', name: 'course' }, 
        { data: 'date', name: 'date' }, 
    ],
    initComplete: function () {
    // Enable THEAD scroll bars
    $('.dataTables_scrollHead').css('overflow', 'auto');
    // Sync THEAD scrolling with TBODY
      $('.dataTables_scrollHead').on('scroll', function () {
          $('.dataTables_scrollBody').scrollLeft($(this).scrollLeft());
      });
    },
    drawCallback: function(){
      $('#loader_modal').modal('hide');
    },
  });

  $('#add-course-form').on('submit',function(e){
    e.preventDefault();

     e.preventDefault();
      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
          }
      });
       $.ajax({
          url: "{{ route('add-offer-course') }}",
          dataType: 'json',
          type: 'post',
          data: new FormData(this),
          contentType: false,
          cache: false,
          processData:false,
          beforeSend: function(){
            $('.save-btn').addClass('disabled');
            $('.save-btn').attr('disabled', true);
            $('#loader_modal').modal({
              backdrop: 'static',
              keyboard: false
            });
            $('#loader_modal').modal('show');
          },
          success: function(result){
            $('.save-btn').attr('disabled', true);
            $('.save-btn').removeAttr('disabled');
            if(result.success == true){
              toastr.success('Success!', 'Course Added Successfully!!!',{"positionClass": "toast-bottom-right"});
              $('.table-school').DataTable().ajax.reload();

              $('#add-course-form')[0].reset();
              $('#addOfferCourse').modal('hide');
            }
            else{
              toastr.error('Error!', result.errormsg,{"positionClass": "toast-bottom-right"});
              $('#loader_modal').modal('hide');
            }

          },
          error: function (request, status, error) {
                /*$('.form-control').removeClass('is-invalid');
                $('.form-control').next().remove();*/
                $('#loader_modal').modal('hide');
                $('.save-btn').removeClass('disabled');
                $('.save-btn').removeAttr('disabled');
                json = $.parseJSON(request.responseText);
                $.each(json.errors, function(key, value){
                      $('input[name="'+key+'"]').after('<span class="invalid-feedback" role="alert"><strong>'+value+'</strong>');
                     $('input[name="'+key+'"]').addClass('is-invalid');
                     $('textarea[name="'+key+'"]').after('<span class="invalid-feedback" role="alert"><strong>'+value+'</strong>');
                     $('textarea[name="'+key+'"]').addClass('is-invalid');


                });
            }
        });
  });
});
</script>
@stop

