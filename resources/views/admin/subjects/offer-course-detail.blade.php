@extends('user.layouts.layout')

@section('title','Online Marketing')

@section('content')
<style type="text/css">
  ul {
  font-family: "Roboto";
  font-size: 13px;
  line-height: 1.5em;
  margin: 5px 0 15px;
  padding: 0;
}
ul li {
  list-style: none;
  position: relative;
  padding: 0 0 0 20px;
  margin-bottom: 5px;
}
ul.circle-checkmark li::before {
  content: "";
  position: absolute;
  left: 0;
  top: 2px;
  border: solid 8px #ff6600;
  border-radius: 8px;
  -moz-border-radius: 8px;
  -webkit-border-radius: 8px;
}
ul.circle-checkmark li::after {
  content: "";
  position: absolute;
  left: 0.6%;
  top: 5px;
  width: 3px;
  height: 8px;
  border: solid #fff;
  border-width: 0 2px 2px 0;
  -webkit-transform: rotate(45deg);
  -moz-transform: rotate(45deg);
  -o-transform: rotate(45deg);
  transform: rotate(45deg);
}
</style>

                    <div>
                        <div class="row page-title-row">
                            <div class="col-md col-sm col-12 page-title-col align-self-center">
                                <h1 class="page-title">Offer Cource Details</h1>
                            </div>
                            <div class="col-sm col-12 mt-sm-0 mt-3 ml-auto text-right page-action-button">
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addOfferCourse">+Add Detail</button>
                            </div>
                            
                    </div>

                    <div class="row">
                        <div class="col-8 offset-2 mt-4">
                        <div class="entriesbg bg-white custompadding customborder shadow p-4">
                          <div class="row text-center">
                            <div class="col justify-content-center align-items-center">
                              <img class="" src="{{asset('public/uploads/school/courses/'.$course->image)}}" style="width: 10%">
                            </div>
                          </div>

                          <div class="row">
                            <div class="col">
                              <p><b>Course Name :</b> {{$course->name}}</p>
                              <p><b>Course Fee :</b> {{$course->course_fee}}</p>
                              <p><b>Duration :</b> {{$course->duration}}</p>
                            </div>
                        </div>
                        <div class="row mt-2">
                          <div class="col">
                            @if($course_detail != null)
                              @foreach($course_detail as $detail)
                                <h4 style="color:#007bff;">{{$detail->header}}</h4>
                                @php $sub_details = explode('--',$detail->description) @endphp
                                @if(count($sub_details) == 1)
                                <p>{{$sub_details[0]}}</p>
                                @else
                                <ul class="circle-checkmark">
                                  @foreach($sub_details as $sub)
                                    <li>{{$sub}}</li>
                                  @endforeach
                                </ul>
                                @endif
                              @endforeach
                              @endif
                          </div>
                        </div>
                        </div>
                        </div>
                    </div>
                    
                    </div>


<!--Add Student Modal -->
<div class="modal fade" id="addOfferCourse" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add Course Detail</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form id="add-course-form" method="POST" enctype="multipart/form-data">
        @csrf
        <input type="hidden" name="course_id" value="{{$course->id}}">
      <div class="modal-body ">
          <div class="form-row">
            <div class="form-group col-md-12">
                <label for="header">Header</label>
                <input type="text" name="header" id="header" class="form-control" placeholder="Detail Header..."/>
            </div>
            <div class="form-group col-md-12">
                <label for="description">Description</label>
                <textarea class="form-control" name="description" id="description" placeholder="Description..."></textarea>
            </div>
          </div>
          
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary save-btn">Add</button>
      </div>
      </form>
    </div>
  </div>
</div>
@endsection

@section('javascript')
<script type="text/javascript">
$(document).ready(function(){
  $('#add-course-form').on('submit',function(e){
    e.preventDefault();

     e.preventDefault();
      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
          }
      });
       $.ajax({
          url: "{{ route('add-offer-course-detail') }}",
          dataType: 'json',
          type: 'post',
          data: new FormData(this),
          contentType: false,
          cache: false,
          processData:false,
          beforeSend: function(){
            $('.save-btn').addClass('disabled');
            $('.save-btn').attr('disabled', true);
            $('#loader_modal').modal({
              backdrop: 'static',
              keyboard: false
            });
            $('#loader_modal').modal('show');
          },
          success: function(result){
            $('.save-btn').attr('disabled', true);
            $('.save-btn').removeAttr('disabled');
            if(result.success == true){
              toastr.success('Success!', 'Course Detail Added Successfully!!!',{"positionClass": "toast-bottom-right"});
              // $('.table-school').DataTable().ajax.reload();
              /*setTimeout(function(){
                window.location.reload();
              }, 2000);*/
              location.reload();
              $('#add-course-form')[0].reset();
              $('#addOfferCourse').modal('hide');
            }
            else{
              toastr.error('Error!', result.errormsg,{"positionClass": "toast-bottom-right"});
              $('#loader_modal').modal('hide');
            }

          },
          error: function (request, status, error) {
                /*$('.form-control').removeClass('is-invalid');
                $('.form-control').next().remove();*/
                $('#loader_modal').modal('hide');
                $('.save-btn').removeClass('disabled');
                $('.save-btn').removeAttr('disabled');
                json = $.parseJSON(request.responseText);
                $.each(json.errors, function(key, value){
                      $('input[name="'+key+'"]').after('<span class="invalid-feedback" role="alert"><strong>'+value+'</strong>');
                     $('input[name="'+key+'"]').addClass('is-invalid');
                     $('textarea[name="'+key+'"]').after('<span class="invalid-feedback" role="alert"><strong>'+value+'</strong>');
                     $('textarea[name="'+key+'"]').addClass('is-invalid');


                });
            }
        });
  });
});
</script>
@stop

