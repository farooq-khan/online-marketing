@extends('user.layouts.layout')

@section('title','Online Marketing')

@section('content')
<style type="text/css">
   
</style>

                    <div>
                        <div class="row page-title-row">
                            <div class="col-md col-sm col-12 page-title-col align-self-center">
                                <h1 class="page-title">Schools</h1>
                            </div>
                            <div class="col-sm col-12 mt-sm-0 mt-3 ml-auto text-right page-action-button">
                                {{-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addSchool">+Add school</button> --}}
                            </div>
                            
                    </div>

                    <div class="row">
                        <div class="col-12 mt-4">
                    
                        <div class="card p-4">
                          <div class="d-sm-flex justify-content-between">
                          <div class="delete-selected-item mb-4 d-none">
                            <a href="javascript:void(0);" class="btn selected-item-btn btn-sm delete_class deleteIcon" data-type="quotation" title="Delete Class" >
                            <i class="zmdi zmdi-delete" style="font-size: 25px;"></i></a>
                          </div>
                        </div>
                        <div class="table-responsive">
                        <table class="table table-striped nowrap table-school ">
                        <thead>
                        <tr>
                            <th>Action</th>
                            <th>Name</th>
                            <th>Registration No.</th>
                            <th>Address</th>
                            <th>Phone</th>
                        </tr>
                        </thead>
                        </table>
                        </div>
                    </div>
                </div>
                    </div>
                    </div>
                

<!--Add Student Modal -->
<div class="modal fade" id="addSchool" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add School</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form id="add-school-form" method="POST" enctype="multipart/form-data">
        @csrf
      <div class="modal-body ">
          <div class="form-row">
            <div class="form-group col-md-6">
                <label for="name">Name</label>
                <input type="text" name="name" id="name" class="form-control" placeholder="School Name..."/>
            </div>
            <div class="form-group col-md-6">
                <label for="registration_no">Registration No.</label>
                <input type="text" name="registration_no" id="registration_no" class="form-control" placeholder="School Registration No..."/>
            </div>

             <div class="form-group col-md-6">
                <label for="address">Address</label>
                <input type="text" name="address" id="address" class="form-control" placeholder="School Address..."/>
            </div>

            <div class="form-group col-md-6">
                <label for="phone">Phone</label>
                <input type="number" name="phone" id="phone" class="form-control" placeholder="School Phone No..."/>
            </div>

            <div class="form-group col-md-6">
                <label for="image">Logo</label>
                <input type="file" name="image" id="image" class="form-control" placeholder="School Logo..."/>
            </div>

            <div class="form-group col-md-6">
                <label for="certificate_frame">Certificate Frame</label>
                <input type="file" name="certificate_frame" id="certificate_frame" class="form-control" placeholder="School Frame..."/>
            </div>
          </div>
          
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary save-btn">Add</button>
      </div>
      </form>
    </div>
  </div>
</div>
@endsection

@section('javascript')
<script type="text/javascript">
$(document).ready(function(){
  var table2 =  $('.table-school').DataTable({
    processing: true,
    searching: true,
    "language": {
    processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw" style="color:#13436c;"></i><span class="sr-only">Loading...</span> '},
    ordering: false,
    pageLength: {{50}},
    serverSide: true,
    autoWidth: false,
    "lengthMenu": [50,100,150,200],
    
    scrollX:true,
    scrollY : '90vh',
    scrollCollapse: true,
    ajax: 
    {
      beforeSend: function(){
        $('#loader_modal').modal({
          backdrop: 'static',
          keyboard: false
        });
        $('#loader_modal').modal('show');
      },
      url: "{!! route('get-school') !!}",
    },
    columns: [
        { data: 'checkbox', name: 'checkbox' },  
        { data: 'name', name: 'name' },  
        { data: 'registration_no', name: 'registration_no' }, 
        { data: 'address', name: 'address' }, 
        { data: 'phone', name: 'phone' }, 
    ],
    initComplete: function () {
    // Enable THEAD scroll bars
    $('.dataTables_scrollHead').css('overflow', 'auto');
    // Sync THEAD scrolling with TBODY
      $('.dataTables_scrollHead').on('scroll', function () {
          $('.dataTables_scrollBody').scrollLeft($(this).scrollLeft());
      });
    },
    drawCallback: function(){
      $('#loader_modal').modal('hide');
    },
  });

  $('#add-school-form').on('submit',function(e){
    e.preventDefault();

     e.preventDefault();
      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
          }
      });
       $.ajax({
          url: "{{ route('add-school') }}",
          dataType: 'json',
          type: 'post',
          data: new FormData(this),
          contentType: false,
          cache: false,
          processData:false,
          beforeSend: function(){
            $('.save-btn').addClass('disabled');
            $('.save-btn').attr('disabled', true);
            $('#loader_modal').modal({
              backdrop: 'static',
              keyboard: false
            });
            $('#loader_modal').modal('show');
          },
          success: function(result){
            $('.save-btn').attr('disabled', true);
            $('.save-btn').removeAttr('disabled');
            if(result.success == true){
              toastr.success('Success!', 'School Added Successfully!!!',{"positionClass": "toast-bottom-right"});
              $('.table-school').DataTable().ajax.reload();
              /*setTimeout(function(){
                window.location.reload();
              }, 2000);*/

              $('#add-school-form')[0].reset();
              $('#addSchool').modal('hide');
            }
            else if(result.success == false)
            {
              toastr.info('Sorry!', 'One School Can Be Registered At A Time!!!',{"positionClass": "toast-bottom-right"});
              $('.table-school').DataTable().ajax.reload();
              $('#add-school-form')[0].reset();
              $('#addSchool').modal('hide');
            }
            else{
              toastr.error('Error!', result.errormsg,{"positionClass": "toast-bottom-right"});
              $('#loader_modal').modal('hide');
            }

          },
          error: function (request, status, error) {
                /*$('.form-control').removeClass('is-invalid');
                $('.form-control').next().remove();*/
                $('#loader_modal').modal('hide');
                $('.save-btn').removeClass('disabled');
                $('.save-btn').removeAttr('disabled');
                json = $.parseJSON(request.responseText);
                $.each(json.errors, function(key, value){
                      $('input[name="'+key+'"]').after('<span class="invalid-feedback" role="alert"><strong>'+value+'</strong>');
                     $('input[name="'+key+'"]').addClass('is-invalid');
                     $('textarea[name="'+key+'"]').after('<span class="invalid-feedback" role="alert"><strong>'+value+'</strong>');
                     $('textarea[name="'+key+'"]').addClass('is-invalid');


                });
            }
        });
  });
});
</script>
@stop

