@extends('user.layouts.layout')

@section('title','Single Student')

@section('content')
<style type="text/css">
   .student-image
   {
    padding-top: 80%;
    background-position: center;
    background-size: cover;
   }

   .student_image
   {
    box-shadow: 0px 0px 15px 0px rgba(0,0,0,0.5);

   }
</style>

                    <div>
                      <div class="row mt-4 mb-2">
                        <div class="col-4">
                          <div class="row">
                          <div class="col-6"><h4>School Logo</h4></div>
                          <div class="col-6">
                            <button type="button" class="btn btn-primary pull-right" style="padding: 2px 5px;" data-toggle="modal" data-target="#updateImage">+Update Logo</button></div>
                          </div>
                        </div>
                        <div class="col-4">
                          <h4>School Information</h4>
                        </div>
                        <div class="col-4">
                          <div class="row">
                          <div class="col-6"><h4>Certificate Fram</h4></div>
                          <div class="col-6">
                            <button type="button" class="btn btn-primary pull-right" style="padding: 2px 5px;" data-toggle="modal" data-target="#updateFrame">+Update Frame</button></div>
                          </div>
                        </div>
                      </div>
                        <div class="row">
                          <div class="col-lg-4 col-md-4 col-sm-4 col-4 bg-white p-2 student_image">
                            <div class="student-image" style="background-image: url('{{asset('public/uploads/school/images/'.$school->logo)}}')">
                              
                            </div>
                          </div>
                          <div class="col-lg-4 col-md-4 col-sm-4 col-4 pl-2 pr-2">
                            <div class="bg-white h-100 p-4">
                              <table style="width: 100%;font-size: 18px">
                                <tr>
                                  <td>School Name :</td>
                                  <td>
                                    <span class="m-l-15 inputDoubleClick" id="name"  data-fieldvalue="{{$school->name}}">
                                    {{$school->name != null ? $school->name : '--'}}
                                  </span>
                                    <input type="text" autocomplete="nope" name="name" class="fieldFocus d-none form-control" value="{{(@$school->name!=null)?$school->name:''}}">
                                  </td>
                                </tr>

                                <tr>
                                  <td>Sub Title :</td>
                                  <td>
                                    <span class="m-l-15 inputDoubleClick" id="sub_title"  data-fieldvalue="{{$school->sub_title}}">
                                    {{$school->sub_title != null ? $school->sub_title : '--'}}
                                  </span>
                                    <input type="text" autocomplete="nope" name="sub_title" class="fieldFocus d-none form-control" value="{{(@$school->sub_title!=null)?$school->sub_title:''}}">
                                  </td>
                                </tr>

                                <tr>
                                  <td>Registration No :</td>
                                  <td>
                                    <span class="m-l-15 inputDoubleClick" id="registration_no"  data-fieldvalue="{{$school->registration_no}}">
                                    {{$school->registration_no != null ? $school->registration_no : '--'}}
                                  </span>
                                    <input type="text" autocomplete="nope" name="registration_no" class="fieldFocus d-none form-control" value="{{(@$school->registration_no!=null)?$school->registration_no:''}}">
                                  </td>
                                </tr>

                                <tr>
                                  <td>Address :</td>
                                  <td>
                                    <span class="m-l-15 inputDoubleClick" id="address"  data-fieldvalue="{{$school->address}}">
                                    {{$school->address != null ? $school->address : '--'}}
                                  </span>
                                    <input type="text" autocomplete="nope" name="address" class="fieldFocus d-none form-control" value="{{(@$school->address!=null)?$school->address:''}}">
                                  </td>
                                </tr>

                                <tr>
                                  <td>Phone :</td>
                                  <td>
                                    <span class="m-l-15 inputDoubleClick" id="phone"  data-fieldvalue="{{$school->phone}}">
                                    {{$school->phone != null ? $school->phone : '--'}}
                                  </span>
                                    <input type="number" autocomplete="nope" name="phone" class="fieldFocus d-none form-control" value="{{(@$school->phone!=null)?$school->phone:''}}">
                                    
                                  </td>
                                </tr>
                              </table>
                            </div>
                          </div>

                          <div class="col-lg-4 col-md-4 col-sm-4 col-4 bg-white p-2 student_image">
                            <div class="student-image" style="padding-top: 0;">
                              <img src="{{asset('public/uploads/school/images/frames/'.$school->certificate_frame)}}">
                            </div>
                          </div>
                        </div>

                    </div>


<!--Add Student Modal -->
<div class="modal fade" id="updateImage" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Update School Logo</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form id="add-school-photo" method="POST" enctype="multipart/form-data">
        <input type="hidden" name="std_id" value="{{$school->id}}">
      <div class="modal-body ">

          <div class="form-row">
            <div class="form-group col-md-12">
                <label for="relation"><i class="zmdi zmdi-account pr-2"></i> Upload Logo</label>
                <input type="file" name="image" id="image" class="form-control" accept="image/*"/>
            </div>
          </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Update</button>
      </div>
      </form>
    </div>
  </div>
</div>

<!--Add Student Modal -->
<div class="modal fade" id="updateFrame" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Update Certificate Frame</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form id="add-certificate-frame" method="POST" enctype="multipart/form-data">
        <input type="hidden" name="std_id" value="{{$school->id}}">
      <div class="modal-body ">

          <div class="form-row">
            <div class="form-group col-md-12">
                <label for="relation"><i class="zmdi zmdi-account pr-2"></i> Upload Logo</label>
                <input type="file" name="image" id="certificate" class="form-control" accept="image/*"/>
            </div>
          </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Update</button>
      </div>
      </form>
    </div>
  </div>
</div>
@endsection

@section('javascript')
<script type="text/javascript">
$(document).ready(function(){
  var std_id = "{{$school->id}}";
   // to make fields double click editable
  $(document).on("dblclick",".inputDoubleClick",function(){
    $(this).addClass('d-none');
    $(this).next().removeClass('d-none');
    $(this).next().addClass('active');
    $(this).next().focus();
  });

    $(document).on('keypress keyup focusout',".fieldFocus",function(e) {
    // alert('hi');
    if (e.keyCode === 27 && $(this).hasClass('active')) {
      var fieldvalue = $(this).prev().data('fieldvalue');
      var thisPointer = $(this);
      thisPointer.addClass('d-none');
      thisPointer.val(fieldvalue);
      thisPointer.removeClass('active');
      thisPointer.prev().removeClass('d-none');
    }

    var name = $(this).attr('name');
    // alert(name);

    if( (e.keyCode === 13 || e.which === 0) && $(this).val().length > 0 && $(this).hasClass('active')){
      var str = $(this).val();
      if(($(this).val().length < 2 ||  !str.replace(/\s/g, '').length))
      {
        swal({ html:true, title:'Alert !!!', text:'<b>Please Enter At Least 2 Characters (this string may only contain white spaces)!!!</b>'});
        return false;
      }
      else
      {
        $(this).removeClass('active');
        var fieldvalue = $(this).prev().data('fieldvalue');
        var new_value = $(this).val();
        // alert(new_value);
        if(fieldvalue == new_value)
        {
          var thisPointer = $(this);
          thisPointer.addClass('d-none');
          thisPointer.prev().removeClass('d-none');
          $(this).prev().html(fieldvalue);
        }
        else
        {
          var thisPointer = $(this);
          thisPointer.addClass('d-none');
          thisPointer.prev().removeClass('d-none');
          if(new_value != '')
          {
            $(this).prev().html(new_value);
          }
          $(this).prev().data('fieldvalue', new_value);
          saveStudentData(thisPointer,thisPointer.attr('name'), thisPointer.val(),fieldvalue);
        }
      }
    }

  });

      function saveStudentData(thisPointer,field_name,field_value,new_select_value){
      var id = "{{$school->id}}";
      
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
      });
      $.ajax({
        method: "post",

        url: "{{ url('admin/update-school-data') }}",
        dataType: 'json',
        data: 'id='+id+'&'+field_name+'='+encodeURIComponent(field_value)+'&'+'new_select_value'+'='+encodeURIComponent(new_select_value),
        beforeSend: function(){
          $('#loader_modal').modal({
            backdrop: 'static',
            keyboard: false
          });
          $('#loader_modal').modal('show');
        },
        success: function(data)
        {
          $("#loader_modal").modal('hide');
          if(data.success == true)
          {
            
            toastr.success('Success!', 'Information updated successfully.',{"positionClass": "toast-bottom-right"});
            return true;
          }

        },

      });
    }

    $('#add-school-photo').on('submit',function(e){
    e.preventDefault();
    var img = $('#image').val();
    if(img == '' || img == null)
    {
      toastr.error('Sorry!', 'Please a upload a photo first!!!',{"positionClass": "toast-bottom-right"});
      return false;
    }
      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
          }
      });
       $.ajax({
          url: "{{ route('add-school-photo') }}",
          dataType: 'json',
          type: 'post',
          data: new FormData(this),
          contentType: false,
          cache: false,
          processData:false,
          beforeSend: function(){
            $('.save-btn').addClass('disabled');
            $('.save-btn').attr('disabled', true);
            $('#loader_modal').modal({
              backdrop: 'static',
              keyboard: false
            });
            $('#loader_modal').modal('show');
          },
          success: function(result){
            $('.save-btn').attr('disabled', true);
            $('.save-btn').removeAttr('disabled');
            if(result.success == true){
              toastr.success('Success!', 'School Logo Updated Successfully!!!',{"positionClass": "toast-bottom-right"});
              // $('.table-family').DataTable().ajax.reload();
              /*setTimeout(function(){
                window.location.reload();
              }, 2000);*/

              // $('#add-student-family-form')[0].reset();
              // $('#addStudentFamily').modal('hide');
              location.reload();
            }else{
              toastr.error('Error!', result.errormsg,{"positionClass": "toast-bottom-right"});
              $('#loader_modal').modal('hide');
            }

          },
          error: function (request, status, error) {
                /*$('.form-control').removeClass('is-invalid');
                $('.form-control').next().remove();*/
                $('#loader_modal').modal('hide');
                $('.save-btn').removeClass('disabled');
                $('.save-btn').removeAttr('disabled');
                json = $.parseJSON(request.responseText);
                $.each(json.errors, function(key, value){
                      $('input[name="'+key+'"]').after('<span class="invalid-feedback" role="alert"><strong>'+value+'</strong>');
                     $('input[name="'+key+'"]').addClass('is-invalid');
                     $('textarea[name="'+key+'"]').after('<span class="invalid-feedback" role="alert"><strong>'+value+'</strong>');
                     $('textarea[name="'+key+'"]').addClass('is-invalid');


                });
            }
        });
  });

        $('#add-certificate-frame').on('submit',function(e){
    e.preventDefault();
    var img = $('#certificate').val();
    if(img == '' || img == null)
    {
      toastr.error('Sorry!', 'Please a upload a photo first!!!',{"positionClass": "toast-bottom-right"});
      return false;
    }
      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
          }
      });
       $.ajax({
          url: "{{ route('add-certificate-photo') }}",
          dataType: 'json',
          type: 'post',
          data: new FormData(this),
          contentType: false,
          cache: false,
          processData:false,
          beforeSend: function(){
            $('.save-btn').addClass('disabled');
            $('.save-btn').attr('disabled', true);
            $('#loader_modal').modal({
              backdrop: 'static',
              keyboard: false
            });
            $('#loader_modal').modal('show');
          },
          success: function(result){
            $('.save-btn').attr('disabled', true);
            $('.save-btn').removeAttr('disabled');
            if(result.success == true){
              toastr.success('Success!', 'Certificate Frame Updated Successfully!!!',{"positionClass": "toast-bottom-right"});
              // $('.table-family').DataTable().ajax.reload();
              /*setTimeout(function(){
                window.location.reload();
              }, 2000);*/

              // $('#add-student-family-form')[0].reset();
              // $('#addStudentFamily').modal('hide');
              location.reload();
            }else{
              toastr.error('Error!', result.errormsg,{"positionClass": "toast-bottom-right"});
              $('#loader_modal').modal('hide');
            }

          },
          error: function (request, status, error) {
                /*$('.form-control').removeClass('is-invalid');
                $('.form-control').next().remove();*/
                $('#loader_modal').modal('hide');
                $('.save-btn').removeClass('disabled');
                $('.save-btn').removeAttr('disabled');
                json = $.parseJSON(request.responseText);
                $.each(json.errors, function(key, value){
                      $('input[name="'+key+'"]').after('<span class="invalid-feedback" role="alert"><strong>'+value+'</strong>');
                     $('input[name="'+key+'"]').addClass('is-invalid');
                     $('textarea[name="'+key+'"]').after('<span class="invalid-feedback" role="alert"><strong>'+value+'</strong>');
                     $('textarea[name="'+key+'"]').addClass('is-invalid');


                });
            }
        });
  });
});
</script>
@stop

