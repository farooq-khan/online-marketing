<aside class="sidebar">
<nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
                    <div class="sb-sidenav-menu">
                        <div class="nav flex-column">
                            <li class="nav-item">
                                <a class="nav-link" href="{{url('admin/dashboard')}}">
                                    <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                                    <span class="nav-link-text">Dashboard</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#studentsLayouts" aria-expanded="false" aria-controls="studentsLayouts">
                                    <div class="sb-nav-link-icon"><i class="fas fa-users"></i></div>
                                    <span class="nav-link-text">Students</span>
                                </a>
                                <div class="collapse collapse-menu" id="studentsLayouts" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                                    <div class="collapse-body">
                                        <a class="nav-link" href="{{route('all-students')}}">All Students</a>
                                        <a class="nav-link" href="{{route('pass-out-students')}}">Pass Out Students</a>
                                    </div>
                                </div>
                            </li>
                            <li class="nav-item"> 
                                <a class="nav-link" href="{{route('all-attendance')}}">
                                    <div class="sb-nav-link-icon"><i class="fas fa-clock"></i></div>
                                    <span class="nav-link-text">Attendance</span>
                                </a>
                            </li>
                            {{-- <a class="nav-link" href="{{route('all-fee')}}">
                                <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt theme-color"></i></div>
                                Submit Fee
                            </a> --}}
                            <li class="nav-item"> 
                                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#feeLayouts" aria-expanded="false" aria-controls="feeLayouts">
                                    <div class="sb-nav-link-icon"><i class="fas fa-money-check-alt"></i></div>
                                    <span class="nav-link-text">Fee</span>
                                </a>
                                <div class="collapse collapse-menu" id="feeLayouts" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                                    <div class="collapse-body">
                                        <a class="nav-link" href="{{route('all-fee-types')}}">Add Fee Type(s)</a>
                                        <a class="nav-link" href="{{route('all-fee')}}">Submit Tuition Fee</a>
                                        <a class="nav-link" href="{{route('submit-other-fee')}}">Submit Other Fee</a>
                                        <a class="nav-link" href="{{route('check-unsubmited-fee')}}">Unsubmitted Fee</a>
                                    
                                    </div>
                                </div>
                            </li>
                           {{--  <a class="nav-link" href="{{route('all-result')}}">
                                <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt theme-color"></i></div>
                                Results
                            </a> --}}
                            <li class="nav-item">
                                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#resultLayouts" aria-expanded="false" aria-controls="resultLayouts">
                                    <div class="sb-nav-link-icon"><i class="fas fa-poll"></i></div>
                                    <span class="nav-link-text">Results</span>
                                </a>
                                <div class="collapse collapse-menu" id="resultLayouts" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                                    <div class="collapse-body">
                                        <a class="nav-link" href="{{route('all-result')}}">Exam Result</a>
                                        <a class="nav-link" href="{{route('all-test-result')}}">Test(s) Result</a>
                                    </div>
                                </div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{route('all-classes')}}">
                                    <div class="sb-nav-link-icon"><i class="fas fa-school"></i></div>
                                    <span class="nav-link-text">All Classes</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{route('all-subjects')}}">
                                    <div class="sb-nav-link-icon"><i class="fas fa-book"></i></div>
                                    <span class="nav-link-text">All Subjects</span>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link" href="{{route('timetables.index')}}">
                                    <div class="sb-nav-link-icon"><i class="fas fa-calendar"></i></div>
                                    <span class="nav-link-text">Timetable</span>
                                </a>
                            </li>
                            {{-- <li class="nav-item">
                                <a class="nav-link" href="{{route('sms-management')}}">
                                    <div class="sb-nav-link-icon"><i class="fas fa-book"></i></div>
                                    <span class="nav-link-text">SMS Management</span>
                                </a>
                            </li> --}}
                            {{-- <a class="nav-link" href="{{route('all-exams')}}">
                                <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt theme-color"></i></div>
                                Exams
                            </a> --}}
                            <li class="nav-item"> 
                                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#examLayouts" aria-expanded="false" aria-controls="examLayouts">
                                    <div class="sb-nav-link-icon"><i class="fas fa-pager"></i></div>
                                    <span class="nav-link-text">Exams</span>
                                </a>
                                <div class="collapse collapse-menu" id="examLayouts" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                                    <div class="collapse-body">
                                        <a class="nav-link" href="{{route('all-exams')}}">Add Exam</a>
                                        <a class="nav-link" href="{{route('all-tests')}}">Add Test</a>
                                    </div>
                                </div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseLayouts" aria-expanded="false" aria-controls="collapseLayouts">
                                    <div class="sb-nav-link-icon"><i class="fas fa-chalkboard-teacher"></i></div>
                                    <span class="nav-link-text">Teachers</span>
                                </a>
                                <div class="collapse collapse-menu" id="collapseLayouts" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                                    <div class="collapse-body">
                                        <a class="nav-link" href="{{route('all-teachers')}}">All Teachers</a>
                                        {{-- <a class="nav-link" href="{{route('suspended-teachers')}}">Suspended Teachers</a> --}}
                                        <a class="nav-link" href="{{ route('teacher-salaries') }}">Teachers Salary</a>
                                        <a class="nav-link" href="{{route('teachers-all-attendance')}}">Teachers Attendance</a>
                                    </div>
                                </div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#certificateLayouts" aria-expanded="false" aria-controls="certificateLayouts">
                                    <div class="sb-nav-link-icon"><i class="fas fa-certificate"></i></div>
                                    <span class="nav-link-text">Certificates</span>
                                </a>
                                <div class="collapse collapse-menu" id="certificateLayouts" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                                    <div class="collapse-body">
                                        <a class="nav-link" href="{{route('school-leaving-certificate')}}">Students Certificates</a>
                                        <a class="nav-link" href="{{route('teacher-certificate')}}">Teacher Experience Certificate</a>
                                        <a class="nav-link" href="{{route('custom-certificate')}}">Custom Certificate</a>
                                    </div>
                                </div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#expansesLayout" aria-expanded="false" aria-controls="expansesLayout">
                                    <div class="sb-nav-link-icon"><i class="fas fa-hand-holding-usd"></i></div>
                                    <span class="nav-link-text">Expenses</span>
                                </a>
                                <div class="collapse collapse-menu" id="expansesLayout" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                                    <div class="collapse-body">
                                        <a class="nav-link" href="{{route('all-expenses-types')}}">Add Type</a>
                                        <a class="nav-link" href="{{route('all-expenses')}}">All Expenses</a>
                                    </div>
                                </div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#bookLayouts" aria-expanded="false" aria-controls="bookLayouts">
                                    <div class="sb-nav-link-icon"><i class="fas fa-book"></i></div>
                                    <span class="nav-link-text">Book</span>
                                </a>
                                <div class="collapse collapse-menu" id="bookLayouts" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                                    <div class="collapse-body">
                                        <a class="nav-link" href="{{route('all-books')}}">All Books Stock</a>
                                        <a class="nav-link" href="{{route('sold-books')}}">Sold Books</a>
                                        <a class="nav-link" href="{{route('sold-books-history')}}">Sold Books History</a>
                                    </div>
                                </div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#libraryLayouts" aria-expanded="false" aria-controls="libraryLayouts">
                                    <div class="sb-nav-link-icon"><i class="fas fa-book"></i></div>
                                    <span class="nav-link-text">Library</span>
                                </a>
                                <div class="collapse collapse-menu" id="libraryLayouts" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                                    <div class="collapse-body">
                                        <a class="nav-link" href="{{route('library-all-books')}}">library Books</a>
                                        <a class="nav-link" href="{{route('issued-books')}}">Issued Books</a>
                                    </div>
                                </div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#configurationLayouts" aria-expanded="false" aria-controls="configurationLayouts">
                                    <div class="sb-nav-link-icon"><i class="fas fa-cogs"></i></div>
                                    <span class="nav-link-text">Configuration</span>
                                </a>
                                <div class="collapse collapse-menu" id="configurationLayouts" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                                    <div class="collapse-body">
                                        <a class="nav-link" href="{{route('all-schools')}}">School Information</a>
                                        <a class="nav-link" href="{{route('all-offer-courses')}}">Offer Courses</a>
                                        <a class="nav-link" href="{{route('all-applies')}}">Online Applies</a>
                                        <a class="nav-link" href="{{url('/signup')}}">Sign Up</a>
                                        <a class="nav-link" href="{{route('all-website-configuration')}}">Website Configuration</a>
                                        {{-- <a class="nav-link" href="{{route('teacher-certificate')}}">Teacher Experience Certificate</a> --}}
                                    </div>
                                </div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#accountLayouts" aria-expanded="false" aria-controls="accountLayouts">
                                    <div class="sb-nav-link-icon"><i class="fas fa-users"></i></div>
                                    <span class="nav-link-text">Accounts</span>
                                </a>
                                <div class="collapse collapse-menu" id="accountLayouts" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                                    <div class="collapse-body">
                                    <a class="nav-link" href="{{route('register-account')}}">Register Account</a>
                                    <a class="nav-link" href="{{route('get-register-account')}}">All Accounts</a>
                                    </div>
                                </div>
                            </li>
                        </div>
                    </div>
                    {{-- <div class="sb-sidenav-footer">
                        <div class="small">Logged in as:</div>
                        {{Auth::user()->name}}
                    </div> --}}
                </nav>
</aside>