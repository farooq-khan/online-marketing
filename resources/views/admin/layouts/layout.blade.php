<!doctype html>
<html lang="en">
<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="_token" content="{{csrf_token()}}" />
<title>Online Marketing</title>
<link href="{{asset('public/css/bootstrap.css')}}" rel="stylesheet" />
<link href="{{asset('public/user/layouts/css/styles.css')}}" rel="stylesheet" />
<link href="{{asset('public/frontend/css/toastr.min.css')}}" rel="stylesheet" />
<link href="{{asset('public/css/custom.css?')}}{{time()}}" rel="stylesheet" />
<link href="https://fonts.googleapis.com/css2?family=Karla:ital,wght@0,200..800;1,200..800&family=Montserrat:ital,wght@0,100..900;1,100..900&display=swap" rel="stylesheet">
<link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet" crossorigin="anonymous" />

</head>
<body class="sb-nav-fixed">

  @include('user.layouts.header')
  <div id="layoutSidenav">
   @include('admin.layouts.sidebar') 
  <div id="layoutSidenav_content">
  @yield('content')
  @include('user.layouts.footer')
</div>
</div>

<!-- Loader Modal -->
<div class="modal" id="loader_modal" role="dialog">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-body">
        <h3 style="text-align:center;color: black;">Please wait</h3>
        <p style="text-align:center;"><img src="{{ asset('public/uploads/gif/waiting.gif') }}"></p>
      </div>
    </div>
  </div>
</div>

{{-- Toastr Plugin --}}
<script src="{{asset('public/frontend/js/jquery.min.js')}}"></script>
<script src="{{asset('public/frontend/js/toastr.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script src="{{asset('public/vendor/select2/select2.min.js')}}"></script>
<script src="{{asset('public/vendor/datepicker/moment.min.js')}}"></script>
<script src="{{asset('public/vendor/datepicker/daterangepicker.js')}}"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/js/all.min.js" crossorigin="anonymous"></script>
 <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
 <script src="{{asset('public/user/layouts/js/scripts.js')}}"></script>

 <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>
<script src="{{asset('public/user/layouts/demo/chart-area-demo.js')}}"></script>
<script src="{{asset('public/user/layouts/demo/chart-bar-demo.js')}}"></script>
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
<script src="{{asset('public/user/layouts/demo/datatables-demo.js')}}"></script>
<script type="text/javascript">
</script>
@yield('javascript')

</body>
</html>