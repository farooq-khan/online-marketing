<?php

namespace App\Jobs;

use App\ExportStatus;
use App\StudentFee;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\MaxAttemptsExceededException;
use Illuminate\Queue\SerializesModels;
use PDF;
use File;
class ExportFeeInvoiceToPdfJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    public $tries = 2;
    protected $user_id;
    protected $data;
    public $timeout = 500;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data, $user_id)
    {
        $this->data=$data;
        $this->user_id=$user_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try{
            ini_set('memory_limit', '-1');
            $data=$this->data;
            $fees = explode(',',$data['ids']);
            $student_fee = StudentFee::with('student.student_class')->whereIn('id',$fees)->get();
            $pdf = PDF::loadView('user.invoices.single-fee-invoice-student-wise',compact('student_fee'));
            $pdf->getDomPDF()->set_option("enable_php", true);
            // making pdf name starts
            $img_path = storage_path('/app/system_pdfs/Fee_Invoices.pdf');
            if(File::exists($img_path)) {
                File::delete($img_path);
            }
            $makePdfName = 'Fee_Invoices';        
            $path=storage_path('/app/system_pdfs');
            if(!is_dir($path))
            {
                mkdir($path,0755,true);
            }
            $pdf->save($path.'/'.$makePdfName.'.pdf');
            ExportStatus::where('user_id',$this->user_id)->where('type','all_fee_invoice')->update(['status'=>0,'exception' => null]);
        }catch(\Exception $e) {
            $this->failed($e);
        }
        catch(MaxAttemptsExceededException $e) {
            $this->failed($e);
        }
    }
    public function failed($exception)
    {
        ExportStatus::where('user_id',$this->user_id)->where('type','all_fee_invoice')->update(['status'=>0,'exception' => $exception->getMessage()]);
    }
}
