<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WebsiteConfigurationLabel extends Model
{
    protected $fillable = ['website_configuration_id', 'label', 'value'];
}
