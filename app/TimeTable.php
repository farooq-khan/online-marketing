<?php

namespace App;

use App\StudentClass;
use App\Subject;
use App\Teacher;
use Illuminate\Database\Eloquent\Model;

class TimeTable extends Model
{
    protected $fillable = [
        'class_id', 'subject_id', 'teacher_id', 'date', 'start_time', 'end_time', 'school_id'
    ];

    public function class()
    {
        return $this->belongsTo(StudentClass::class);
    }

    public function subject()
    {
        return $this->belongsTo(Subject::class);
    }

    public function teacher()
    {
        return $this->belongsTo(Teacher::class);
    }
}
