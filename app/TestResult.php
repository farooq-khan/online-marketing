<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TestResult extends Model
{
    protected $fillable = ['test_id', 'student_id', 'class_id', 'from_test_date', 'to_test_date', 'school_id'];
    public function student_class(){
    	return $this->belongsTo('App\StudentClass', 'class_id', 'id');
    }

    public function student_test(){
    	return $this->belongsTo('App\StudentTest', 'test_id', 'id');
    }

    public function student(){
    	return $this->belongsTo('App\Student', 'student_id', 'id');
    }

    public function student_subject(){
    	return $this->belongsTo('App\Subject', 'subject_id', 'id');
    }

}
