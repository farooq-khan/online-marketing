<?php

namespace App;

use App\Result;
use Illuminate\Database\Eloquent\Model;

class Exam extends Model
{
    public function get_position($exam_id,$class_id,$per)
    {
          $query = Result::select(\DB::Raw('sum(total_marks) as total, sum(obtained_marks) as sub_total'),'results.*')->where('class_id',$class_id)->where('exam_id',$exam_id)->groupBy('exam_id','student_id')->orderBy('id', 'DESC')->get();
          $pos = 1;
          foreach ($query as $record) {
              if($record->sub_total > $per)
              {
                $pos++;
              }
          }
            $locale = 'en_US';
            $nf = new \NumberFormatter($locale, \NumberFormatter::ORDINAL);
            return $nf->format($pos);
          return $pos;

    }
}
