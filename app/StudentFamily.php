<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudentFamily extends Model
{
    protected $table = 'student_families';

    public function student_family(){
    	return $this->belongsTo('App\Student', 'student_id', 'id');
    }

    public function student_family_member(){
    	return $this->belongsTo('App\Student', 'member_id', 'id');
    }
}
