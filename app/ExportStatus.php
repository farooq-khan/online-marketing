<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExportStatus extends Model
{
    protected $fillable = ['user_id','exception'];
}
