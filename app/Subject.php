<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    public function student_class(){
    	return $this->belongsTo('App\StudentClass', 'class_id', 'id');
    }

     public function subject_results(){
        return $this->hasMany('App\TestResult', 'subject_id', 'id');
    }
    public function subject_exam_results(){
        return $this->hasMany('App\Result', 'subject_id', 'id');
    }
}
