<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GetMonth extends Model
{
    protected $table = 'months';

    public function get_fees(){
    	return $this->hasMany('App\StudentFee', 'month_id', 'id');
    }
}
