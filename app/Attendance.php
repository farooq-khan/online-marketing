<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attendance extends Model
{
	 public function student_class(){
    	return $this->belongsTo('App\StudentClass', 'class_id', 'id');
    }
}
