<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Expense extends Model
{
    public function expense(){
		return $this->belongsTo('App\ExpansesType', 'expense_id', 'id');
    }
}
