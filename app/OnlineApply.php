<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OnlineApply extends Model
{
    public function get_course(){
    	return $this->belongsTo('App\OfferCourse', 'course_id', 'id');
    }
}
