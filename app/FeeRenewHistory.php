<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FeeRenewHistory extends Model
{
    public function student_class(){
    	return $this->belongsTo('App\StudentClass', 'class_id', 'id');
    }

    public function user(){
    	return $this->belongsTo('App\User', 'user_id', 'id');
    }
}
