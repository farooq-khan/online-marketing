<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IssuedBook extends Model
{
    public function book(){
    	return $this->belongsTo('App\LibraryBook', 'book_id', 'id');
    }

    public function user(){
    	return $this->belongsTo('App\User', 'created_by', 'id');
    }

    public function student(){
    	return $this->belongsTo('App\Student', 'user_id', 'id');
    }

    public function teacher(){
    	return $this->belongsTo('App\Student', 'user_id', 'id');
    }
}
