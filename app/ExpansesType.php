<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExpansesType extends Model
{
    public function get_expenses(){
    	return $this->hasMany('App\expense', 'expense_id', 'id');
    }
}
