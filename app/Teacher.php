<?php

namespace App;

use App\Models\Common\Department;
use App\Models\Common\Designation;
use App\Models\Common\Payscale;
use App\TeacherAttendance;
use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
    public function department()
    {
        return $this->belongsTo(Department::class);
    }

    public function designation()
    {
        return $this->belongsTo(Designation::class);
    }

    public function payscale()
    {
        return $this->belongsTo(Payscale::class);
    }
    
    public function teacher_attendance(){
        return $this->hasMany('App\TeacherAttendance', 'teacher_id', 'id');
    }

    public function get_teacher_attendance_by_month($teacher_id, $month, $year,$status)
    {
        $attendance = TeacherAttendance::where('teacher_id',$teacher_id)->whereYear('attendance_date',$year)->whereMonth('attendance_date',$month)->where('status',$status)->count();
    
        return $attendance;
    }

    public function get_teacher_attendance($teacher_id, $month, $year,$day)
    {
        $attendance = TeacherAttendance::where('teacher_id',$teacher_id)->whereYear('attendance_date',$year)->whereMonth('attendance_date',$month)->whereDay('attendance_date',$day)->first();
        $att = '--';

        if($attendance == null)
        {
            $att = '--';
        }
        else if($attendance->status == 'present')
        {
            $att = 'P';
        }
        else if($attendance->status == 'absent')
        {
            $att = 'A';
        }
        else if($attendance->status == 'leave')
        {
            $att = 'L';
        }

        return $att;
    }
}
