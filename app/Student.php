<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Attendance;
use App\StudentFee;

class Student extends Model
{
    public function student_class(){
    	return $this->belongsTo('App\StudentClass', 'class_id', 'id');
    }

    public function getMonth($month)
    {
    	if($month == '01')
    	{
    		return 1;
    	}
    	elseif($month == '02')
    	{
    		return 2;
    	}
    	elseif($month == '03')
    	{
    		return 3;
    	}
    	elseif($month == '04')
    	{
    		return 4;
    	}
    	elseif($month == '05')
    	{
    		return 5;
    	}
    	elseif($month == '06')
    	{
    		return 6;
    	}
    	elseif($month == '07')
    	{
    		return 7;
    	}
    	elseif($month == '08')
    	{
    		return 8;
    	}
    	elseif($month == '09')
    	{
    		return 9;
    	}
    	else
    	{
    		return $month;
    	}

    }

    public function user(){
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function get_grade($score)
    {
        if($score >= 90)
        {
            return 'A1';
        }
        else if($score >= 80 && $score < 90)
        {
            return 'A+';
        }
        else if($score < 80 and $score >= 70)
        {
            return 'A';
        }
        else if($score < 70 and $score >= 60)
        {
            return 'B+';
        }
        else if($score < 60 and $score >= 50)
        {
            return 'B';
        }
        else if($score < 50 && $score != '')
        {
            return 'X';
        }else
        {
            return '';
        }
        // else if($score < 40 and $score >= 33)
        // {
        //     return 'E';
        // }
        // else if($score < 33)
        // {
        //     return 'F';
        // }
    }
    public function get_position($score)
    {
        if($score >= 90)
        {
            return '1st';
        }
        else if($score >= 80 && $score < 90)
        {
            return '2nd';
        }
        else if($score < 80 and $score >= 70)
        {
            return '3rd';
        }
        return '';
    }

     public function student_attendance(){
        return $this->hasMany('App\Attendance', 'student_id', 'id');
    }

    public function student_result(){
        return $this->hasMany('App\TestResult', 'student_id', 'id');
    }
    public function student_fee(){
        return $this->hasMany('App\StudentFee', 'student_id', 'id')->orderBy('id','asc')->limit(11);
    }
    public function student_fee_per_class(){
        return $this->hasMany('App\StudentFee', 'student_id', 'id')->where('class_id',$this->class_id)->orderBy('id','asc');
    }
    public function student_other_fee_per_class(){
        return $this->hasMany('App\OtherFee', 'student_id', 'id')->where('class_id',$this->class_id)->orderBy('id','asc');
    }
    public function student_other_fee(){
        return $this->hasMany('App\OtherFee', 'student_id', 'id')->orderBy('id','asc')->limit(11);
    }
    public function student_sold_books(){
        return $this->hasMany('App\SoldBooksReport', 'student_id', 'id')->orderBy('id','asc');
    }
    public function student_other_fee_month(){
        return $this->hasMany('App\OtherFee', 'student_id', 'id')->orderBy('id','asc')->limit(11);
    }

    public function get_student_attendance($student_id, $month, $year, $class_id,$day)
    {
        $attendance = Attendance::where('student_id',$student_id)->where('class_id',$class_id)->whereYear('attendance_date',$year)->whereMonth('attendance_date',$month)->whereDay('attendance_date',$day)->first();
        $att = '--';

        if($attendance == null)
        {
            $att = '--';
        }
        else if($attendance->status == 'present')
        {
            $att = 'P';
        }
        else if($attendance->status == 'absent')
        {
            $att = 'A';
        }
        else if($attendance->status == 'leave')
        {
            $att = 'L';
        }

        return $att;
    }

    public function get_student_attendance_by_month($student_id, $month, $year, $class_id,$status)
    {
        $attendance = Attendance::where('student_id',$student_id)->where('class_id',$class_id)->whereYear('attendance_date',$year)->whereMonth('attendance_date',$month)->where('status',$status)->count();
    
        return $attendance;
    }

    public function get_fee($student_id,$year,$month)
    {
        $value = str_pad($month,2,"0",STR_PAD_LEFT);
        $student_fees = StudentFee::where('student_id',$student_id)->whereYear('fee_month',$year)->where('month_id',$month)->first();

        return $student_fees;
    }

    public static function gradePosition($percentage){
        $grade = '';
        $position = '.';
        if($percentage >= 90 && $percentage <=100){
            $grade = 'A1';
            $position = '1st';
          }elseif($percentage >= 80 && $percentage <=89){
            $grade = 'A+';
            $position = '2nd';
          }elseif($percentage >= 70 && $percentage <=79){
            $grade = 'A';
            $position = '3rd';
          }elseif($percentage >= 60 && $percentage <=69){
            $grade = 'B+';
            $position = '.';
          }elseif($percentage >= 50 && $percentage <=59){
            $grade = 'B';
            $position = '.';
          }elseif($percentage < 50){
            $grade = 'X';
            $position = '.';
          }
        $data['grade'] = $grade;
        $data['position'] = $position;

        return $data;
    }
}
