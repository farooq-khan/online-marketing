<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    public function book_class(){
    	return $this->belongsTo('App\StudentClass', 'class_id', 'id');
    }
}
