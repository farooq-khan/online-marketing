<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudentFee extends Model
{
    protected $table = 'student_fees';

    public function student(){
    	return $this->belongsTo('App\Student', 'student_id', 'id');
    }

    public function user(){
    	return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function student_class(){
    	return $this->belongsTo('App\StudentClass', 'class_id', 'id');
    }

    public function student_result(){
        return $this->hasMany('App\TestResult', 'student_id', 'id');
    }

    public function history(){
        return $this->hasMany('App\Models\FeeUpdateHistory', 'fee_id', 'id');
    }


}
