<?php

namespace App\Exports;

use App\Result;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Events\AfterSheet;

class ExamResultExport implements FromQuery, ShouldAutoSize, WithHeadings, WithMapping, WithEvents
{
	protected $class_sub = null;
	protected $query = null;
	protected $std_classes = null;
	protected $exam = null;
	protected $request = null;

    public function __construct($class_sub,$query,$std_classes, $exam, $request)
    {
        $this->class_sub = $class_sub;
        $this->query = $query;
        $this->std_classes = $std_classes;
        $this->exam = $exam;
        $this->request = $request;
    }

    /**
    * @return query
    */
    public function query()
    {
        $query = $this->query;
        return $query;
    }

    public function map($item) : array {
    	$request = $this->request;
    	$data_array = [];
    	array_push($data_array, $item->roll_no);
    	array_push($data_array, $item->name);
    	array_push($data_array, $item->student_class != null ? $item->student_class->class_name : 'N.A');
    	array_push($data_array, $item->guardian !== null ? $item->guardian.' '.@$item->guardian_nickname : 'N.A');
    	array_push($data_array, $this->exam != null ? $this->exam->name : '--');
    	foreach ($this->std_classes as $class) {
    		$result = Result::where('student_id',$item->id)->where('class_id',$class->class_id)->where('subject_id',$class->id)->where('year',$request->year)->where('exam_id',$request->exam_id)->first();
    		array_push($data_array, $result != null ? $result->obtained_marks : null);
    	}
    	return $data_array;
    }

    public function headings(): array {
    	$headings = [];
    	array_push($headings,'Admission No');
    	array_push($headings,'Name');
    	array_push($headings,'Class');
    	array_push($headings,'Father/Guardian Name');
    	array_push($headings,'Exam');
    	foreach($this->class_sub as $sub)
    		array_push($headings, $sub->name);

    	return $headings;
    }
    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
               

            },
        ];
    }
}
