<?php

namespace App\Imports;

use App\Student;
use App\Subject;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Imports\HeadingRowFormatter;

//to read original names
HeadingRowFormatter::default('none');

class ExamResultImport implements ToCollection,WithStartRow, WithHeadingRow
{
	 protected $request;

	 public function __construct($request)
    {
        $this->request = $request;
    }
    /**
    * @param Collection $collection
    */
    public function collection(Collection $rows)
    {
        $row1 = $rows->toArray();
        $remove = array_shift($row1);
        $request = $this->request;
        // dd($row1[0]['Holi Quran']);
       	foreach ($row1 as $row) {
       		// dd($this->request->id);
       		$student = Student::where('roll_no',$row['Admission No'])->first();
       		// dd($student);
       		if(@$request->id && $student){
       			$std_classes = Subject::where('class_id',$request->id)->get();
       			// dd($std_classes[0]->name);
       			foreach ($std_classes as $sub) {
       				if(isset($row[@$sub->name])){
	       				$new_request = new \Illuminate\Http\Request();
	            		$new_request->replace(['id' => 200000,'obtained_marks' => $row[@$sub->name],'exam_id' => $request->exam_id, 'class_id' => $request->id,'subject_id' => $sub->id,'student_id' => @$student->id, 'year' => $request->year,'from_date' => $request->from_date, 'to_date' => $request->to_date]);
	            		app('App\Http\Controllers\ResultController')->updateStudentResult($new_request);
	            	}
       			}
       		}
       	}
       	return true;
    }
    public function startRow():int
    {
        return 1;
    }
}
