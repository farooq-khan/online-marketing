<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use App\Student;
use App\StudentClass;
use Auth;

class StudentBulkImport implements ToCollection
{
    /**
    * @param Collection $collection
    */
    public function collection(Collection $collection)
    {
        // dd($collection);
        $row1 = $collection->toArray();
        $remove = array_shift($row1);
        foreach ($row1 as $row) {
        	$student = new Student;

        	$student->name = $row[0];
        	$student->guardian = $row[1];
        	if(is_numeric($row[3])){
        		$student->guardian_phone = (float)$row[3];
        	}
        	$student->roll_no = $row[4];
        	if($row[5] == 'Female')
        	{
        		$gen = 'female';
        	}
        	else
        	{
        		$gen = 'male';
        	}
        	$student->gender = $gen;

        	$student_class = StudentClass::where('class_name',$row[9])->first();
        	if($student_class != null)
        	{
        		$student->class_id = $student_class->id;
        	}
        	$student->user_id = Auth::user()->id;
        	if($row[11] != 0)
        	{
        		$discount = ($row[11]/$row[10]) * 100;
        	}
        	else
        	{
        		$discount = 0;
        	}
        	$student->discount = $discount;
        	$student->fee = $row[12];
        	$student->status = 1;
        	$student->save();

        }

        return redirect()->back();
    }

    public function startRow():int
    {
        return 2;
    }
}
