<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use App\Student;
use App\StudentFee;
use Carbon\Carbon;
class StudentBulkImportForFees implements ToCollection
{
    /**
    * @param Collection $collection
    */
    public function collection(Collection $collection)
    {
        $row1 = $collection->toArray();
        $remove = array_shift($row1);
        // dd($row1);
        foreach ($row1 as $row) {
        	$student = Student::where('roll_no',$row[0])->first();
        	$start = 1;
        	if($student != null)
        	{
        		for ($i=1; $i < 6; $i++) { 
        			$fee = new StudentFee;
        			$fee->student_id = $student->id;
        			$fee->class_id 	 = $student->class_id;
        			$fee->discount 	 = $student->discount;
        			//date
        			date_default_timezone_set("Asia/Kolkata");
					$dDate = date_create('1900-01-01');
					if($dDate = date_modify($dDate, '+'.($row[$start]-2).' day'))
					    $dos = date_format($dDate,'Y-m-d');
					else $dos=$row[$start];
        			$fee->fee_month  = $dos;

        			$fee->month_id 	 = carbon::parse($dos)->format('m');
        			$fee->year 	 = carbon::parse($dos)->format('Y');
        			$row[$start++];
        			$fee->total_amount = $student->fee;
        			$fee->paid_amount  = (float)$row[$start];
        			$fee->unpaid_amount = ($student->fee - (float)$row[$start++]);

                    //date
                    if($row[$start]){
                        try{
                            $fee->submitted_date = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row[$start]);
                        }
                        catch(Exception $e){
                            dd($e,$row[$start]);
                        }
                    }
                    $row[$start++];
        			// $fee->submitted_date = $row[$start++];
        			$fee->remark		= $row[$start++];
        			$fee->user_id 		= 1;
        			$fee->save();

        			// $fee->submitted_date = carbon::now();
        			if($fee->total_amount == $fee->paid_amount)
        			{
        				$fee->status = 1;
        			}
        			else
        			{
        				$fee->status = 0;
        			}
        			$fee->save();
        		}
        	}
        }
    }

    public function startRow():int
    {
        return 2;
    }
}
