<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SchoolInformation extends Model
{
    protected $table = 'school_informations';
    protected $fillable = ['name', 'user_id', 'address', 'phone', 'status'];
}
