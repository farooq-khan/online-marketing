<?php

namespace App\Http\Controllers;

use App\ExpansesType;
use App\Expense;
use Auth;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class ExpansesController extends Controller
{
    public function expenseTypes()
    {
        return view('user.expenses.expenses-types');
    }

    public function addExpenseType(Request $request)
    {
        $expense_type = new ExpansesType;

        $expense_type->title = $request->title;
        $expense_type->type = $request->expense_type;
        $expense_type->user_id = Auth::user()->id;
        $expense_type->school_id = auth()->user()->school_id;
        $expense_type->status = 1;
        $expense_type->save();

        return response()->json(['success' => true]);
    }

    public function getExpenseTypes(Request $request)
    {
        $query = ExpansesType::where('school_id', auth()->user()->school_id)->orderBy('title','asc');
       
      // $query->latest();
        return Datatables::of($query)
        ->addColumn('checkbox', function ($item) {

            $html_string = ' <a href="javascript:void(0);" class=" selected-item-btn delete_fee_type actionicon deleteIcon" data-type="Delete" data-id="'.$item->id.'" title="Delete Expense Type" >
                            <i class="fas fa-trash-alt"></i></a>';
            return $html_string;
        })
         ->editColumn('title', function ($item){
            $title = $item->title !== null ? $item->title : 'N.A';

            $html_string = '
                 <span class="m-l-15 inputDoubleClick" id="title" style="text-transform: capitalize;"  data-fieldvalue="'.$item->title.'">'.$title.'</span>
                <input type="text" autocomplete="nope" name="title" class="fieldFocus d-none form-control" value="'.$item->title.'" data-id="'.$item->id.'">
            ';

            return $html_string;
        })

         ->editColumn('type', function ($item){
            $type = $item->type != null ? $item->type : '--';

             $html_string = '
                 <span class="m-l-15" id="title" style="text-transform: capitalize;""  data-fieldvalue="'.$item->type.'">'.$type.'</span>';

            return $html_string;
        })
       
        ->rawColumns(['checkbox','title','type'])
        ->make(true);

    }

    public function deleteExpenseType(Request $request)
    {
        $id = $request->id;

        $check = Expense::where('expense_id',$id)->get();

        if($check->count() > 0)
        {
            return response()->json(['success' => false]);
        }

        $fee_type = ExpansesType::where('id',$id)->first();
        $fee_type->delete();

        return response()->json(['success' => true]);
    }

    public function updateExpenseTypeData(Request $request)
    {
        // dd($request->all());
        $old_value = $request->new_select_value; 
   
        $type = ExpansesType::where('id',$request->id)->first();

        $show_title = $request->new_select_value;
        foreach($request->except('id','new_select_value') as $key => $value)
        {
            $type->$key = $value;
        }

        $type->save();

        return response()->json(['success' => true]);
    }

    public function allExpenses()
    {
    	$expenses = ExpansesType::where('status',1)->where('school_id', auth()->user()->school_id)->get();
        return view('user.expenses.all-expenses',compact('expenses'));
    }

    public function expense()
    {
        $expenses = ExpansesType::where('status',1)->where('school_id', auth()->user()->school_id)->get();
        return view('user.expenses.add-expense',compact('expenses'));
    }

    public function fetchExpenseCategory(Request $request)
    {
    	$expenses = ExpansesType::where('type',$request->id)->where('school_id', auth()->user()->school_id)->get();
    	$option = '<option disabled="true"> -- Please Select Expense Type --</option>';
    	foreach ($expenses as $exp) {
    		$option .= '<option value="'.$exp->id.'">'.$exp->title.'</option>';
    	}
    	return response()->json(['html' => $option, 'success' => true]);
    }

    public function addExpense(Request $request)
    {
        $expense = new Expense;

        $expense->expense_id = $request->expense_type;
        $expense->amount = $request->amount;
        $expense->paid_amount = $request->paid_amount;
        $expense->description = $request->description;
        $expense->expense_date = $request->expense_date;
        $expense->payment_date = $request->payment_date;
        $expense->user_id = Auth::user()->id;
        $expense->school_id = auth()->user()->school_id;
        if($request->amount == $request->paid_amount)
        {
            $expense->status = 1;
        }
        else
        {
            $expense->status = 0;
        }
        $expense->save();

        return response()->json(['success' => true]);
    }

    public function getExpense(Request $request)
    {
        $query = Expense::where('school_id', auth()->user()->school_id)->orderBy('id','desc');

        if($request->category_id !== null)
        {
        	$query->whereHas('expense',function($z)use($request){
        		$z->where('type',$request->category_id);
        	});
        }

        if($request->expense_type !== null)
        {
        	$query->where('expense_id',$request->expense_type);
        }

        if($request->from_date !== null)
        {
        	$query->whereDate('expense_date','>=',$request->from_date);
        }

        if($request->to_date !== null)
        {
        	$query->whereDate('expense_date','<=',$request->to_date);
        }
        if($request->from_payment_date !== null)
        {
            $query->whereDate('payment_date','>=',$request->from_payment_date);
        }

        if($request->to_payment_date !== null)
        {
            $query->whereDate('payment_date','<=',$request->to_payment_date);
        }

        $total_amount = (clone $query)->sum('amount');
        $paid_amount = (clone $query)->sum('paid_amount');
       
      // $query->latest();
        return Datatables::of($query)
        ->addColumn('checkbox', function ($item) {

            $html_string = ' <a href="javascript:void(0);" class="selected-item-btn delete_fee_type actionicon deleteIcon" data-type="Delete" data-id="'.$item->id.'" title="Delete Expense" >
                            <i class="fas fa-trash-alt"></i></a>';
            return $html_string;
        })
         ->addColumn('title', function ($item){
            $title = $item->expense !== null ? $item->expense->title : 'N.A';

            $html_string = '
                 <span class="m-l-15" id="title" style="text-transform: capitalize;"  data-fieldvalue="'.$item->title.'">'.$title.'</span>';

            return $html_string;
        })

         ->addColumn('type', function ($item){
            $type = $item->expense != null ? $item->expense->type : '--';

             $html_string = '
                 <span class="m-l-15" id="title" style="text-transform: capitalize;""  data-fieldvalue="'.$item->type.'">'.$type.'</span>';

            return $html_string;
        })

         ->editColumn('description', function ($item){
         	$des = $item->description != null ? $item->description : '--';

         	$html_string = '
                 <span class="m-l-15 inputDoubleClick" id="description" style="text-transform: capitalize;"  data-fieldvalue="'.$item->description.'">'.$des.'</span>
                <input type="text" autocomplete="nope" name="description" class="fieldFocus d-none form-control" value="'.$item->description.'" data-id="'.$item->id.'">
            ';
            return $html_string;
         })

         ->addColumn('amount', function ($item){
         	$amount = $item->amount != null ? $item->amount : '--';

         	$html_string = '
                 <span class="m-l-15 inputDoubleClick" id="amount" style="text-transform: capitalize;"  data-fieldvalue="'.$item->amount.'">'.$amount.'</span>
                <input type="text" autocomplete="nope" name="amount" class="fieldFocus d-none form-control" value="'.$item->amount.'" data-id="'.$item->id.'">
            ';
            return $html_string;
         })

         ->addColumn('paid_amount', function ($item){
            $paid_amount = $item->paid_amount != null ? $item->paid_amount : '--';

            $html_string = '
                 <span class="m-l-15 inputDoubleClick" id="paid_amount" style="text-transform: capitalize;"  data-fieldvalue="'.$item->paid_amount.'">'.$paid_amount.'</span>
                <input type="text" autocomplete="nope" name="paid_amount" class="fieldFocus d-none form-control" value="'.$item->paid_amount.'" data-id="'.$item->id.'">
            ';
            return $html_string;
         })

         ->addColumn('remaining_amount', function ($item){
            $paid_amount = $item->amount - $item->paid_amount;
            return $paid_amount != 0 ? $paid_amount : 0;
         })
         ->addColumn('empty_col', function ($item){
            return '';
         })

         ->addColumn('status', function ($item){
            $paid_amount = $item->status == 1 ? 'Paid' : 'Unpaid';
            return $paid_amount;
         })

          ->addColumn('expense_date', function ($item){
         	$dat = $item->expense_date != null ? $item->expense_date : '--';
         	$html_string = '
                 <span class="m-l-15 inputDoubleClick" id="expense_date" style="text-transform: capitalize;"  data-fieldvalue="'.$item->expense_date.'">'.$dat.'</span>
                <input type="date" autocomplete="nope" name="expense_date" class="fieldFocus d-none form-control" value="'.$item->expense_date.'" data-id="'.$item->id.'">
            ';
            return $html_string;
         })
          ->addColumn('payment_date', function ($item){
            $dat = $item->payment_date != null ? $item->payment_date : '--';
            $html_string = '
                 <span class="m-l-15 inputDoubleClick" id="payment_date" style="text-transform: capitalize;"  data-fieldvalue="'.$item->payment_date.'">'.$dat.'</span>
                <input type="date" autocomplete="nope" name="payment_date" class="fieldFocus d-none form-control" value="'.$item->payment_date.'" data-id="'.$item->id.'">
            ';
            return $html_string;
         })
       
        ->rawColumns(['checkbox','title','type','description','amount','expense_date','paid_amount','remaining_amount','status','payment_date','empty_col'])
        ->with(['total_amount' => $total_amount,'paid_amount' => $paid_amount])
        ->make(true);

    }

    public function deleteExpense(Request $request)
    {
        $id = $request->id;

        $fee_type = Expense::where('id',$id)->first();
        $fee_type->delete();

        return response()->json(['success' => true]);
    }

    public function updateExpenseData(Request $request)
    {
        // dd($request->all());
        $old_value = $request->new_select_value; 
   
        $type = Expense::where('id',$request->id)->first();

        $show_title = $request->new_select_value;
        foreach($request->except('id','new_select_value') as $key => $value)
        {
            $type->$key = $value;
        }

        $type->save();

        if($type->amount > $type->paid_amount)
        {
            $type->status = 0;
            $type->save();
        }
        else
        {
            $type->status = 1;
            $type->save();
        }

        return response()->json(['success' => true]);
    }
}
