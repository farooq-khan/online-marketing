<?php

namespace App\Http\Controllers\Common;

use App\Http\Controllers\Controller;
use App\Models\Common\Department;
use App\Teacher;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\Datatables\Datatables;

class DepartmentController extends Controller
{
    public function index()
    {
    	return view('common.departments');
    }

    public function getDepartments(Request $request)
    {
       $query = Department::where('school_id', auth()->user()->school_id);

        return Datatables::of($query)
        ->addColumn('action', function ($item) {
        	$html_string = '';
            if($item->status == 1)
            {
            $html_string .= ' <a href="javascript:void(0);" class="suspend_teacher actionicon suspendIcon fas fa-user-minus" data-type="Delete" data-id="'.$item->id.'" title="Suspend Department" >
                            </a>';
            }
            elseif($item->status == 2)
            {
                $html_string .= ' <a href="javascript:void(0);" class="activate_teacher actionicon activateIcon fas fa-user-plus" data-type="Delete" data-id="'.$item->id.'" title="Activate Department" >
                            </a>';
            }
            $html_string .= '
                 <a href="javascript:void(0)" class="actionicon deleteIcon delete_department" data-id="' . $item->id . '" title="Delete Department"><i class="fa fa-trash-alt"></i></a>
                 ';
            return $html_string;
        })
         ->editColumn('name', function ($item){
            return $item->name !== null ? $item->name : 'N.A';
        })

        ->addColumn('empty_col', function ($item){
            return '';
        })
        ->rawColumns(['action','name','empty_col'])
        ->make(true);

    }

    public function addDepartment(Request $request)
    {
        // dd($request->all());

        $department = new Department;
        $department->name = $request->name;
        $department->school_id = Auth::user()->school_id;
        $department->save();

        return response()->json(['success' => true]);
    }

    public function deleteDepartment(Request $request)
    {
        $department = Department::find($request->id);
        if($department)
        {
            $check_teacher = Teacher::where('department_id',$department->id)->first();

            if(!$check_teacher)
            {
                $department->delete();
                return response()->json(['success' => true]);
            }
            else
            {
                return response()->json(['success' => false]);
            }
        }
        else
        {
            return response()->json(['error' => true]);
        }
    }
}
