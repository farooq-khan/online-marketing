<?php

namespace App\Http\Controllers\Common;

use App\Http\Controllers\Controller;
use App\Models\Common\Payscale;
use App\Teacher;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\Datatables\Datatables;

class PayScaleController extends Controller
{
    public function index()
    {
    	return view('common.payscales');
    }

    public function getPayScales(Request $request)
    {
       $query = Payscale::where('school_id', auth()->user()->school_id);

        return Datatables::of($query)
        ->addColumn('action', function ($item) {
        	$html_string = '';
            if($item->status == 1)
            {
            $html_string .= ' <a href="javascript:void(0);" class="suspend_teacher actionicon suspendIcon fas fa-user-minus" data-type="Delete" data-id="'.$item->id.'" title="Suspend Designation" >
                            </a>';
            }
            elseif($item->status == 2)
            {
                $html_string .= ' <a href="javascript:void(0);" class="activate_teacher actionicon activateIcon fas fa-user-plus" data-type="Delete" data-id="'.$item->id.'" title="Activate Designation" >
                            </a>';
            }
            $html_string .= '
                 <a href="javascript:void(0)" class="actionicon deleteIcon delete_designation" data-id="' . $item->id . '" title="Delete Designation"><i class="fa fa-trash-alt"></i></a>
                 ';
            return $html_string;
        })
         ->editColumn('name', function ($item){
            return $item->name !== null ? $item->name : 'N.A';
        })

        ->addColumn('empty_col', function ($item){
            return '';
        })
        ->rawColumns(['action','name','empty_col'])
        ->make(true);

    }

    public function addPayScale(Request $request)
    {
        // dd($request->all());

        $payscale = new Payscale;
        $payscale->name = $request->name;
        $payscale->school_id = Auth::user()->school_id;
        $payscale->save();

        return response()->json(['success' => true]);
    }

    public function deletePayScale(Request $request)
    {
        $payscale = Payscale::find($request->id);
        if($payscale)
        {
            $check_teacher = Teacher::where('payscale_id',$payscale->id)->first();

            if(!$check_teacher)
            {
                $payscale->delete();
                return response()->json(['success' => true]);
            }
            else
            {
                return response()->json(['success' => false]);
            }
        }
        else
        {
            return response()->json(['error' => true]);
        }
    }
}
