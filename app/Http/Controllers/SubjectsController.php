<?php

namespace App\Http\Controllers;

use App\OfferCourse;
use App\OfferCourseDetail;
use App\StudentClass;
use App\Subject;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\OnlineApply;

class SubjectsController extends Controller
{
    public function allSubjects()
    {
        $classes = StudentClass::where('school_id', auth()->user()->school_id)->orderBy('class_name','asc')->get();
        return view('user.subjects.index',compact('classes'));
    }

     public function addSubject(Request $request)
    {
        // dd($request->all());
        $count = count($request->name);
        $invalid_subjects = 0;
        $already_exists_subjects = null;
        $valid_subjects = 0;
        for ($j=0; $j < $count ; $j++) {
             if($request->name[$j] == null || $request->exam_score[$j] == null)
             {
                $invalid_subjects++;
             }
        }

        if($invalid_subjects > 0)
        {
            return response()->json(['invalid_subjects' => $invalid_subjects]);
        }
        for ($i=0; $i < $count ; $i++) { 
        	$check_subject = Subject::where('name',$request->name[$i])->where('class_id',$request->class_id)->first();

            if($check_subject == null)
            {
            	$subject = new Subject;

    	        $subject->name 		= $request->name[$i];
    	        $subject->class_id    = $request->class_id;
    	        $subject->exam_score	= $request->exam_score[$i];
    	        $subject->status 		= 1;
    	        $subject->save();
                $valid_subjects++;
            }
            else
            {
                $already_exists_subjects .= $request->name[$i].' ';
            }
        }
       
        return response()->json(['success' => true,'already_exists_subjects' => $already_exists_subjects,'valid_subjects' => $valid_subjects]);
    }

    public function getSubjects(Request $request)
    {
        $query = StudentClass::select('subjects.*','student_classes.class_name')->join('subjects','subjects.class_id','student_classes.id')->orderBy('student_classes.id','asc');
       
      // $query->latest();
        return Datatables::of($query)
        ->addColumn('checkbox', function ($item) {

            $html_string = ' <a href="javascript:void(0);" class="btn selected-item-btn btn-sm delete_class deleteIcon" data-type="Delete" data-id="'.$item->id.'" title="Delete Class" >
                            <i class="zmdi zmdi-delete" style="font-size: 18px;"></i></a>';
            return $html_string;
        })
         ->addColumn('name', function ($item){
            return $item->name != null ? $item->name : 'N.A';
        })

        ->addColumn('class', function ($item){
            return $item != null ? $item->class_name : '--';
        })

         ->addColumn('exam_score', function ($item){
            return $item->exam_score != null ? $item->exam_score : 'N.A';
        })
       
        ->rawColumns(['checkbox','name','class','exam_score'])
        ->make(true);

    }

    public function updateSubject($id)
    {
        $class_subjects = Subject::where('class_id',$id)->get();

        return view('user.subjects.update',compact('class_subjects','id'));
    }

    public function updateSubjects(Request $request)
    {
        // dd($request->all());
        if($request->ids != null)
        {
            $new_subject = count(@$request->name) - count(@$request->ids);
        }
        else
        {
            $new_subject = count($request->name);
        }
        // dd($new_subject);
        $invalid_subjects = null;
        $last_subject = 0;
        if($request->ids != null)
        {
            foreach ($request->ids as $key => $value) {
            if($request->exam_score[$key] == null || $request->name[@$key] == null)
            {
                $invalid_subjects .= ' '.($key+1).' ';
            }
            else
            {
               $sub = Subject::find($value);
               if($sub != null)
               {
                   $sub->name = $request->name[$key];
                   $sub->exam_score = $request->exam_score[$key];

                   $sub->save();
                }
            }

            $last_subject++;
        }
        }

        // dd($last_subject);

        if($new_subject > 0)
        {
            for ($i=$new_subject; $i > 0 ; $i--) { 
            if($request->exam_score[$last_subject] == null || $request->name[@$last_subject] == null)
            {
                $invalid_subjects .= ' '.($i+1).' ';
            }
            else
            {
                $sub = new Subject;
                $sub->name = $request->name[@$last_subject];
                $sub->exam_score = $request->exam_score[@$last_subject];
                $sub->class_id = $request->class_id;
                $sub->save();
            }

                $last_subject++;
            }
        }

        if($invalid_subjects != null)
        {
            return redirect()->back()->with('msg','Subject(s) '.$invalid_subjects.' cannot be updated due to incomplete information');
        }

        return redirect()->route('all-subjects');
    }

    public function allOfferCourses()
    {
        return view('admin.subjects.all-offer-courses');
    }

    public function allApplies()
    {
        return view('admin.subjects.all-applies');
    }

    public function offerCourseDetail($id)
    {
        $course = OfferCourse::find($id);
        $course_detail = OfferCourseDetail::where('course_id',$id)->get();
        return view('admin.subjects.offer-course-detail',compact('course','course_detail'));
    }

    public function addOfferCourse(Request $request)
    {
        $course = new OfferCourse;
        $course->name = $request->name;
        $course->course_fee = $request->course_fee;
        $course->duration = $request->duration;
        if($request->hasFile('image') && $request->image->isValid())
        {
          $fileNameWithExt = $request->file('image')->getClientOriginalName();
          $fileName = pathinfo($fileNameWithExt,PATHINFO_FILENAME);
          $extension = $request->file('image')->getClientOriginalExtension();
          $fileNameToStore = $fileName.'_'.time().'.'.$extension;
          $path = $request->file('image')->move('public/uploads/school/courses/',$fileNameToStore);
          $course->image = $fileNameToStore;
        }
        $course->school_id = auth()->user()->school_id;
        $course->status = 1;
        $course->save();
        return response()->json(['success' => true]);
    }

    public function getOfferCourses(Request $request)
    {
        $query = OfferCourse::where('status',1)->where('school_id', auth()->user()->school_id)->orderBy('name','asc');
       
      // $query->latest();
        return Datatables::of($query)
        ->addColumn('checkbox', function ($item) {

            $html_string = '
                 <a href="'.url('admin/offer-course-detail/'.$item->id.'').'" class="actionicon viewIcon delete_order_transaction" data-id="' . $item->id . '" title="View Details"><i class="fa fa-eye"></i></a>
                 ';
            return $html_string;
        })
         ->editColumn('name', function ($item){
            $name = $item->name !== null ? $item->name : 'N.A';

             $html_string = '
                 <span class="m-l-15 inputDoubleClick" id="name" style="text-transform: capitalize;"  data-fieldvalue="'.$item->name.'">'.$name.'</span>
                <input type="text" autocomplete="nope" name="name" class="fieldFocus d-none form-control" value="'.$item->name.'" data-id="'.$item->id.'">
            ';

            return $html_string;
        })

         ->editColumn('course_fee', function ($item){
            $fee = $item->course_fee != null ? $item->course_fee : '--';

             $html_string = '
                 <span class="m-l-15 inputDoubleClick" id="course_fee" style="text-transform: capitalize;"  data-fieldvalue="'.$item->course_fee.'">'.$fee.'</span>
                <input type="number" autocomplete="nope" name="course_fee" class="fieldFocus d-none form-control" value="'.$item->course_fee.'" data-id="'.$item->id.'">
            ';

            return $html_string;
        })

         ->editColumn('duration', function ($item){
            $duration = $item->duration != null ? $item->duration : '--';

             $html_string = '
                 <span class="m-l-15 inputDoubleClick" id="duration" style="text-transform: capitalize;"  data-fieldvalue="'.$item->duration.'">'.$duration.'</span>
                <input type="text" autocomplete="nope" name="duration" class="fieldFocus d-none form-control" value="'.$item->duration.'" data-id="'.$item->id.'">
            ';

            return $html_string;
        })
       
        ->rawColumns(['checkbox','name','course_fee','duration'])
        ->make(true);

    }

    public function addOfferCourseDetail(Request $request)
    {
        $course_detail = new OfferCourseDetail;
        $course_detail->course_id = $request->course_id;
        $course_detail->header = $request->header;
        $course_detail->description = $request->description;
        $course_detail->save();
        return response()->json(['success' => true]);
    }

    public function getAllApplies(Request $request)
    {
        $query = OnlineApply::where('status',1)->where('school_id', auth()->user()->school_id)->orderBy('fullname','asc');
       
      // $query->latest();
        return Datatables::of($query)
        ->addColumn('checkbox', function ($item) {
            $html_string = '<div class="custom-control custom-checkbox custom-checkbox1 d-inline-block">
                            <input type="checkbox" class="custom-control-input check" value="'.$item->id.'" id="stone_check_'.$item->id.'">
                            <label class="custom-control-label" for="stone_check_'.$item->id.'"></label>
                        </div>';
            return $html_string;
        })
         ->addColumn('name', function ($item){
            return $item->fullname !== null ? $item->fullname : 'N.A';
        })

        ->addColumn('fathername', function ($item){
            return $item->father_name !== null ? $item->father_name : 'N.A';
        })

        ->addColumn('email', function ($item){
            return $item->email !== null ? $item->email : 'N.A';
        })

         ->addColumn('phone', function ($item){
            return $item->phone !== null ? $item->phone : 'N.A';
        })
        ->addColumn('course', function ($item){
            return $item->get_course !== null ? $item->get_course->name : 'N.A';
        })

        ->addColumn('date', function ($item){
            return carbon::parse($item->created_at)->format('d-m-Y');
        })
       
        ->rawColumns(['checkbox','name','fathername','email','phone','course','date'])
        ->make(true);

    }

    public function updateOfferCourseData(Request $request)
    {
        // dd($request->all());
        $old_value = $request->new_select_value; 
   
        $type = OfferCourse::where('id',$request->id)->first();

        $show_title = $request->new_select_value;
        foreach($request->except('id','new_select_value') as $key => $value)
        {
            $type->$key = $value;
        }

        $type->save();

        return response()->json(['success' => true]);
    }

    public function deleteOnlineApplies(Request $request)
    {
        // dd($request->all());
        $ids = explode(',', $request->ids);
        $applies = OnlineApply::whereIn('id',$ids)->get();

        foreach ($applies as $apply) {
            $apply->delete();
        }

        return response()->json(['success' => true]);
    }
}
