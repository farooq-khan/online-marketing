<?php

namespace App\Http\Controllers;

use App\Configuration\WebsiteGallery;
use App\Configuration\WebsiteTeacher;
use App\OfferCourse;
use App\OfferCourseDetail;
use App\OnlineApply;
use App\SchoolInformation;
use App\Student;
use App\User;
use App\WebsiteConfiguration;
use App\WebsiteConfigurationLabel;
use Illuminate\Http\Request;

class GuestController extends Controller
{
    
    public function signup()
    {
        return view('user.login.index');
    }

    
    public function add(Request $request){

        // dd($request->all());
        $validator = $request->validate([
            'name'   => 'required',
            'email'    => 'required|unique:users',
            'pass'    => 'required|not_in:0',
            're_pass' => 'required',
            'role' => 'required',
        ]);
        // dd($sponsor_id);

        $is_user_exist = User::where('email',$request->email)->first();
        if($is_user_exist !== null)
        {
            return response()->json(['user_exist' => true]);
        }
        else
        {
            $user                    = new User;
            $user->name              = $request->name;
            $user->email             = $request->email;
            $user->password          =  bcrypt($request->pass);
            $user->email_verified_at =  null;
            $user->role              = $request->role;  
            $user->status            = 1;
            $user->save();
        }

        // $template= EmailTemplate::where('type','create-purchasing')->first();
        // send email //

        // $my_helper =  new MyHelper;
        // $result_helper = $my_helper->getApiCredentials();

        // if($result_helper['email_notification'] == 1)
        // {
        //     Mail::to($users_role->email, $users_role->name)->send(new AddPurchasingEmail($users_role, $password,$template));
        // }

        return response()->json(['success' => true]);

    }

        public function addStudent(Request $request){

        // dd($request->all());
        $validator = $request->validate([
            'name'   => 'required',
            'email'    => 'required|unique:users',
            'pass'    => 'required|not_in:0',
            're_pass' => 'required',
            // 'role' => 'required',
        ]);
        // dd($request->all());
        $student = Student::find($request->name);
        if($student != null)
        {
            $is_user_exist = User::where('email',$request->email)->first();
            if($is_user_exist !== null)
            {
                return response()->json(['user_exist' => true]);
            }
            else
            {
                $user                    = new User;
                $user->name              = $student->name;
                $user->email             = $request->email;
                $user->password          =  bcrypt($request->pass);
                $user->email_verified_at =  null;
                $user->role              = 'student';  
                $user->status            = 1;
                $user->student_id        = $student->id;
                $user->school_id         = auth()->user()->school_id;
                $user->save();
            }

            return response()->json(['success' => true]);
        }
        else
        {
            return response()->json(['success' => false]);
        }

    }

    function generateBarcodeNumber() {
    $number = mt_rand(1000000000, 9999999999); // better than rand()

    // call the same function if the barcode exists already
    if ($this->barcodeNumberExists($number)) {
        return generateBarcodeNumber();
    }

        // otherwise, it's valid and can be used
        return $number;
    }

    function barcodeNumberExists($number) {
    // query the database and return a boolean
    // for instance, it might look like this in Laravel
        return User::where('sponsor_id',$number)->exists();
    }

    public function checkSponsor(Request $request)
    {
        // dd($request->all());

        $user = User::where('sponsor_id',$request->id)->first();
        if($user !== null)
        {
            return response()->json(['success' => true, 'user' => $user]);
        }
        else
        {
            return response()->json(['success' => false]);
        }
    }

    public function aboutUs()
    {
        return view('guest.about-us');
    }

    public function offerCourse($id)
    {
        $course = OfferCourse::find($id);
        $course_detail = OfferCourseDetail::where('course_id',$id)->get();
        return view('user.subjects.offer-course',compact('course','course_detail'));
    }

    public function contactUs()
    {
        return view('guest.contact-us');
    }

    public function applyOnline()
    {
        $courses = OfferCourse::where('status',1)->get();
        return view('guest.apply-online',compact('courses'));
    }

    public function sendEmailForContact(Request $request)
    {
         \Mail::send(array(), array(), function ($message) use ($request) {
          $message->to('zubairkhanlkl8338@gmail.com')
            ->subject('Email From '.$request->fullname.' ('.$request->email.')')
            ->from($request->email)
            ->setBody($request->message)
            ->setReplyTo($request->email);
          });

         return response()->json(['success' => true]);
    }

    public function applyOnlineForCourse(Request $request)
    {
        $apply = new OnlineApply;
        $apply->fullname = $request->fullname;
        $apply->father_name = $request->guardian;
        $apply->email = $request->email;
        $apply->phone = $request->phone;
        $apply->course_id = $request->course;
        $apply->status = 1;
        $apply->save();

         return response()->json(['success' => true]);
    }

    public function getStarted()
    {
        return view('guest.register');
    }

    public function registerSchool(Request $request){
        $validator = $request->validate([
            'school_name'   => 'required|string',
            'school_address'   => 'required|string',
            'phone_number'   => 'required',
            'name'   => 'required',
            'email'    => 'required|unique:users',
            'password'    => 'required|not_in:0',
            'confirm_password' => 'required',
            // 'role' => 'required',
        ]);

        //check already registered or not
        $user = User::where('email', $request->email)->first();
        if($user){
            return reponse()->json(['success' => false, 'msg' => 'Email already registered']);
        }

        //register school
        $school = SchoolInformation::create([
            'name' => $request->school_name,
            'address'   => $request->school_address,
            'phone'     => $request->phone_number,
            'status'    => 1
        ]);

        $user = User::create([
            'name'      => $request->name,
            'email'     => $request->email,
            'password'  => bcrypt($request->password),
            'role'      => 'admin',
            'status'    => 1
        ]);

        $school->user_id = $user->id;
        $user->school_id = $school->id;

        $school->save();
        $user->save();

        return response()->json(['success' => true]);
    }

    public function schoolWebsite($id){
        $configuration = WebsiteConfiguration::where('school_id', $id)->first();
        if(!$configuration){
            return redirect()->back();
        }
        $labels = WebsiteConfigurationLabel::where('website_configuration_id', $configuration->id)->get();
        $teachers = WebsiteTeacher::where('configuration_id', $configuration->id)->get();
        $galleries = WebsiteGallery::where('configuration_id', $configuration->id)->get();
        // dd($labels);
        return view('guest.website-detail', compact('configuration', 'labels', 'teachers', 'galleries'));
    }

}
