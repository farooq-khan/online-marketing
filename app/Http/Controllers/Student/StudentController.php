<?php

namespace App\Http\Controllers\Student;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Student;
use App\StudentClass;
use App\StudentFee;
use App\FeeType;
use Auth;

class StudentController extends Controller
{
    public function singleStudent($id)
    {
        if(Auth::user()->student_id == $id)
        {
            $student = Student::find($id);
            $students = Student::where('status',1)->where('school_id', auth()->user()->school_id)->where('id','!=',$id)->get();
                   $fee = StudentFee::where('student_id',$id)->pluck('class_id')->toArray();
            $classes = StudentClass::whereIn('id',$fee)->orderBy('class_name','asc')->get();
            $ids = explode(',', $student->promotion_history);
            $student_promotion = StudentClass::whereIn('id',$ids)->get();
            $std_cl = '';
            if($student_promotion->count() > 0)
            {
                foreach ($student_promotion as $cla) {
                    $std_cl .= $cla->class_name.' --> ';
                }
            }
            $fee_types = FeeType::where('status',1)->where('school_id', auth()->user()->school_id)->get();
            $std_cl .= $student->student_class->class_name;
            return view('student.single-student',compact('student','students','classes','std_cl','fee_types'));
        }
        else
        {
            return redirect()->back();
        }
    }

    public function getStudentByRollno(Request $request){
        // dd($request->all());
        $student = Student::where('roll_no', $request->roll_no)->first();
        if($student){
            $img = public_path('/uploads/students/images/'.$student->image);
            // dd(\File::exists($img));
            if(!file_exists($img)){
                $img = asset('public/assets/img/male.jpg');
                if($student->gender == 'female')
                    $img = asset('public/assets/img/female.jpg');
            }else{
                $img = asset('public/uploads/students/images/'.$student->image);
            }
            return response()->json(['success' => true, 'student' => $student, 'img' => $img]);
        }
        return response()->json(['success' => false, 'No Student Found !!!']);
    }
}
