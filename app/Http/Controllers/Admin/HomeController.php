<?php

namespace App\Http\Controllers\Admin;

use App\Configuration\WebsiteGallery;
use App\Configuration\WebsiteTeacher;
use App\ExpansesType;
use App\Expense;
use App\Http\Controllers\Controller;
use App\Imports\StudentBulkImport;
use App\Imports\StudentBulkImportForFees;
use App\OtherFee;
use App\SchoolInformation;
use App\Student;
use App\StudentClass;
use App\StudentFee;
use App\TeacherSalary;
use App\User;
use App\WebsiteConfiguration;
use App\WebsiteConfigurationLabel;
use Auth;
use Carbon\Carbon;
use Excel;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class HomeController extends Controller
{
    public function adminDashboard()
    {
      $year = carbon::now()->format('Y');
      $months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
      $jan = StudentFee::whereMonth('fee_month','01')->whereYear('fee_month',$year)->join('users', 'users.id', '=', 'student_fees.user_id')->where('users.school_id',auth()->user()->school_id)->sum('paid_amount');
      $feb = StudentFee::whereMonth('fee_month','02')->whereYear('fee_month',$year)->join('users', 'users.id', '=', 'student_fees.user_id')->where('users.school_id',auth()->user()->school_id)->sum('paid_amount');
      $mar = StudentFee::whereMonth('fee_month','03')->whereYear('fee_month',$year)->join('users', 'users.id', '=', 'student_fees.user_id')->where('users.school_id',auth()->user()->school_id)->sum('paid_amount');
      $apr = StudentFee::whereMonth('fee_month','04')->whereYear('fee_month',$year)->join('users', 'users.id', '=', 'student_fees.user_id')->where('users.school_id',auth()->user()->school_id)->sum('paid_amount');
      $may = StudentFee::whereMonth('fee_month','05')->whereYear('fee_month',$year)->join('users', 'users.id', '=', 'student_fees.user_id')->where('users.school_id',auth()->user()->school_id)->sum('paid_amount');
      $jun = StudentFee::whereMonth('fee_month','06')->whereYear('fee_month',$year)->join('users', 'users.id', '=', 'student_fees.user_id')->where('users.school_id',auth()->user()->school_id)->sum('paid_amount');
      $jul = StudentFee::whereMonth('fee_month','07')->whereYear('fee_month',$year)->join('users', 'users.id', '=', 'student_fees.user_id')->where('users.school_id',auth()->user()->school_id)->sum('paid_amount');
      $aug = StudentFee::whereMonth('fee_month','08')->whereYear('fee_month',$year)->join('users', 'users.id', '=', 'student_fees.user_id')->where('users.school_id',auth()->user()->school_id)->sum('paid_amount');
      $sep = StudentFee::whereMonth('fee_month','09')->whereYear('fee_month',$year)->join('users', 'users.id', '=', 'student_fees.user_id')->where('users.school_id',auth()->user()->school_id)->sum('paid_amount');
      $oct = StudentFee::whereMonth('fee_month','10')->whereYear('fee_month',$year)->join('users', 'users.id', '=', 'student_fees.user_id')->where('users.school_id',auth()->user()->school_id)->sum('paid_amount');
      $nov = StudentFee::whereMonth('fee_month','11')->whereYear('fee_month',$year)->join('users', 'users.id', '=', 'student_fees.user_id')->where('users.school_id',auth()->user()->school_id)->sum('paid_amount');
      $dec = StudentFee::whereMonth('fee_month','12')->whereYear('fee_month',$year)->join('users', 'users.id', '=', 'student_fees.user_id')->where('users.school_id',auth()->user()->school_id)->sum('paid_amount');

      $jan_income = $this->findIncome($year,'01');
      $feb_income = $this->findIncome($year,'02');
      $mar_income = $this->findIncome($year,'03');
      $apr_income = $this->findIncome($year,'04');
      $may_income = $this->findIncome($year,'05');
      $jun_income = $this->findIncome($year,'06');
      $jul_income = $this->findIncome($year,'07');
      $aug_income = $this->findIncome($year,'08');
      $sep_income = $this->findIncome($year,'09');
      $oct_income = $this->findIncome($year,'10');
      $nov_income = $this->findIncome($year,'11');
      $dec_income = $this->findIncome($year,'12');

      // dd($jan_income);

      $classes = StudentClass::select('class_name')->where('school_id', @auth()->user()->school_id)->pluck('class_name')->toArray();
      $ids = StudentClass::select('id')->where('school_id', @auth()->user()->school_id)->pluck('id')->toArray();
      // $cla = json_encode($classes);
      // dd($classes,$months);
      $students_array = array();
      foreach ($ids as $cls) {
        $std = Student::select('class_id')->where('class_id',$cls)->get()->count();
        array_push($students_array, $std);
      }

      $all_classes = StudentClass::select('class_name','id')->where('school_id', @auth()->user()->school_id)->get();
      $total_fee = Student::select('status','id','fee')->where('school_id', @auth()->user()->school_id)->where('status',1)->sum('fee');

      //Fee Detail
      $current_year = Carbon::now()->format('Y');
      $current_month = Carbon::now()->format('m');
      // dd($current_month);
      $student_pending_fee = StudentFee::select('unpaid_amount')->where('school_id', @auth()->user()->school_id)->sum('unpaid_amount');
      $suspended_student_pending_fee = StudentFee::with('student')->select('unpaid_amount','student_id')->where('school_id', @auth()->user()->school_id)->whereHas('student',function($q){
        $q->where('status',2);
      })->sum('unpaid_amount');
      $passout_student_pending_fee = StudentFee::with('student')->select('unpaid_amount','student_id')->whereHas('student',function($q){
        $q->where('status',10);
      })->where('school_id', @auth()->user()->school_id)->sum('unpaid_amount');
      $active_student_pending_fee = StudentFee::with('student')->select('unpaid_amount','student_id')->whereHas('student',function($q){
        $q->where('status',1);
      })->where('school_id', @auth()->user()->school_id)->sum('unpaid_amount');

      $total_dues_of_other_fee = OtherFee::select('unpaid_amount')->where('school_id', @auth()->user()->school_id)->sum('unpaid_amount');
      $fee_submitted_current_month = StudentFee::whereMonth('submitted_date',$current_month)->whereYear('submitted_date',$current_year)->where('school_id', @auth()->user()->school_id)->sum('paid_amount');
      // dd($fee_submitted_current_month);
      // for ($i = 1; $i <= 12; $i++) {
      //         $monthss[] = date("Y-m", strtotime( date( 'Y-m-01' )." -$i months"));
      //     }
      // dd($monthss);

      $start = new \DateTime;
      $start->setDate($start->format('Y'), $start->format('n'), 1); // Normalize the day to 1
      $start->setTime(0, 0, 0); // Normalize time to midnight
      $start->sub(new \DateInterval('P12M'));
      $interval = new \DateInterval('P1M');
      $recurrences = 12;

      foreach (new \DatePeriod($start, $interval, $recurrences, true) as $date) {
          // $monthss[] = $date->format('F, Y'); // attempting to make it more clear to read here
           $months_names[] = $date->format('F,Y'); // attempting to make it more clear to read here
           $years_names[] = $date->format('Y'); // attempting to make it more clear to read here
      }

      $second_last_month = date('Y-m-d', strtotime(date('Y-m')." -2 month"));
      $second_last_month_of_year = date('Y-m-d', strtotime(date('Y-m')." -2 month"));
      // dd(carbon::parse($second_last_month)->format('Y'));
      $second_last_month_dues = TeacherSalary::where('month',carbon::parse($second_last_month)->format('m'))->where('year',carbon::parse($second_last_month)->format('Y'))->where('school_id', @auth()->user()->school_id)->sum('unpaid_amount');
      $second_last_month_of_year = carbon::parse($second_last_month_of_year)->format('F');
      $total_salary_paid_in_current_month = TeacherSalary::whereMonth('submitted_date',carbon::now()->format('m'))->whereYear('submitted_date',carbon::now()->format('Y'))->where('school_id', @auth()->user()->school_id)->sum('paid_amount');

      //Personal Expenses
      $personal_expenses_types = ExpansesType::where('type','personal')->where('school_id', @auth()->user()->school_id)->get();
      $school_expenses_types = ExpansesType::where('type','school')->where('school_id', @auth()->user()->school_id)->get();
      $other_expenses_types = ExpansesType::where('type','other')->where('school_id', @auth()->user()->school_id)->get();

      $payble_amounts = Expense::where('status',0)->where('school_id', @auth()->user()->school_id)->get();
    	return view('admin.dashboard.index',compact('months','jan','feb','mar','apr','may','jun','jul','aug','sep','oct','nov','dec','jan_income','feb_income','mar_income','apr_income','may_income','jun_income','jul_income','aug_income','sep_income','oct_income','nov_income','dec_income','classes','students_array','all_classes','total_fee','student_pending_fee','suspended_student_pending_fee','active_student_pending_fee','fee_submitted_current_month','months_names','years_names','second_last_month_dues','second_last_month_of_year','total_salary_paid_in_current_month','personal_expenses_types','school_expenses_types','other_expenses_types','payble_amounts','passout_student_pending_fee','total_dues_of_other_fee'));
    }

    private function findIncome($year,$month)
    {
        //Income chart values
        $previous_month_fee = StudentFee::whereMonth('fee_month',$month)->where('school_id', @auth()->user()->school_id)->whereYear('fee_month',$year)->sum('paid_amount');
        $previous_month_other_fee = OtherFee::whereMonth('submitted_date',$month)->whereYear('submitted_date',$year)->where('school_id', @auth()->user()->school_id)->sum('amount');
        $personal_expenses = Expense::where('status',1)->whereMonth('expense_date',$month)->whereYear('expense_date',$year)->whereHas('expense',function($z){
          $z->where('type','personal');
        })->where('school_id', @auth()->user()->school_id)->sum('amount');
        $school_expenses = Expense::where('status',1)->whereMonth('expense_date',$month)->whereYear('expense_date',$year)->whereHas('expense',function($z){
            $z->where('type','school');
        })->where('school_id', @auth()->user()->school_id)->sum('amount');

        $other_expenses = Expense::where('status',1)->whereMonth('expense_date',$month)->whereYear('expense_date',$year)->whereHas('expense',function($z){
            $z->where('type','other');
        })->where('school_id', @auth()->user()->school_id)->sum('amount');
        $teachers_paid_salary = TeacherSalary::select('id','paid_amount')->whereMonth('salary_month',$month)->whereYear('salary_month',$year)->where('school_id', @auth()->user()->school_id)->sum('paid_amount');
        $out = number_format(($previous_month_fee + $previous_month_other_fee) - (($personal_expenses + $school_expenses + $other_expenses) + $teachers_paid_salary),2,'.','');

        return $out;
    }

    public function allSchools()
    {
        return view('admin.school.index');
    }

    public function addSchool(Request $request)
    {
    	$find = SchoolInformation::where('user_id',Auth::user()->id)->get();

    	if($find->count() > 0)
    	{
    		return response()->json(['success' => false]);
    	}
        // dd($request->all());
        $school = new SchoolInformation;

        $school->name = $request->name;
        $school->address = $request->address;
        $school->registration_no = $request->registration_no;
        $school->phone = $request->phone;
        $school->status = 1;
        $school->user_id = Auth::user()->id;

        if($request->hasFile('image') && $request->image->isValid())
          {
              $fileNameWithExt = $request->file('image')->getClientOriginalName();
              $fileName = pathinfo($fileNameWithExt,PATHINFO_FILENAME);
              $extension = $request->file('image')->getClientOriginalExtension();
              $fileNameToStore = $fileName.'_'.time().'.'.$extension;
              $path = $request->file('image')->move('public/uploads/school/images/',$fileNameToStore);
              $school->logo = $fileNameToStore;
          }

          if($request->hasFile('certificate_frame') && $request->certificate_frame->isValid())
          {
              $fileNameWithExt = $request->file('certificate_frame')->getClientOriginalName();
              $fileName = pathinfo($fileNameWithExt,PATHINFO_FILENAME);
              $extension = $request->file('certificate_frame')->getClientOriginalExtension();
              $fileNameToStore = $fileName.'_'.time().'.'.$extension;
              $path = $request->file('certificate_frame')->move('public/uploads/school/images/frames/',$fileNameToStore);
              $school->certificate_frame = $fileNameToStore;
          }

        $school->save();

          return response()->json(['success' => true]);
    }

    public function getSchool(Request $request)
    {
        $query = SchoolInformation::where('id', auth()->user()->school_id)->orderBy('name','asc');
       
      // $query->latest();
        return Datatables::of($query)
        ->addColumn('checkbox', function ($item) {

            $html_string = '
                 <a href="'.url('admin/school/'.$item->id.'').'" class="actionicon viewIcon delete_order_transaction" data-id="' . $item->id . '" title="Delete"><i class="fa fa-eye"></i></a>
                 ';
            return $html_string;
        })
         ->editColumn('name', function ($item){
            return $item->name !== null ? $item->name : 'N.A';
        })

         ->editColumn('registration_no', function ($item){
            return $item->registration_no != null ? $item->registration_no : '--';
        })

         ->editColumn('address', function ($item){
            return $item->address != null ? $item->address : '--';
        })

         ->editColumn('phone', function ($item){
            return $item->phone != null ? $item->phone : '--';
        })
       
        ->rawColumns(['checkbox','name','registration_no','address','phone'])
        ->make(true);

    }

    public function singleSchool($id)
    {
        $school = SchoolInformation::find($id);
        return view('admin.school.single-school',compact('school'));
    }

    public function addSchoolPhoto(Request $request)
    {
        // dd($request->all());

        $school = SchoolInformation::find($request->std_id);

        if($request->hasFile('image') && $request->image->isValid())
          {
              $fileNameWithExt = $request->file('image')->getClientOriginalName();
              $fileName = pathinfo($fileNameWithExt,PATHINFO_FILENAME);
              $extension = $request->file('image')->getClientOriginalExtension();
              $fileNameToStore = $fileName.'_'.time().'.'.$extension;
              $path = $request->file('image')->move('public/uploads/school/images/',$fileNameToStore);
              $school->logo = $fileNameToStore;
          }

        $school->save();
        return response()->json(['success' => true]);
    }

    public function updateSchoolData(Request $request)
    {
        $old_value = $request->new_select_value; 
   
        $school = SchoolInformation::where('id',$request->id)->first();

        $show_title = $request->new_select_value;
        foreach($request->except('id','new_select_value') as $key => $value)
        {
            $school->$key = $value;
        }

        $school->save();

        return response()->json(['success' => true]);
    }

    public function addCertificatePhoto(Request $request)
    {
        // dd($request->all());

        $school = SchoolInformation::find($request->std_id);

        if($request->hasFile('image') && $request->image->isValid())
          {
              $fileNameWithExt = $request->file('image')->getClientOriginalName();
              $fileName = pathinfo($fileNameWithExt,PATHINFO_FILENAME);
              $extension = $request->file('image')->getClientOriginalExtension();
              $fileNameToStore = $fileName.'_'.time().'.'.$extension;
              $path = $request->file('image')->move('public/uploads/school/images/frames/',$fileNameToStore);
              $school->certificate_frame = $fileNameToStore;
          }

        $school->save();
        return response()->json(['success' => true]);
    }

    public function allWebsiteConfiguration()
    {
        $conf = WebsiteConfiguration::where('school_id', auth()->user()->school_id)->first();
        $labels = WebsiteConfigurationLabel::where('website_configuration_id', @$conf->id)->get();
        return view('admin.website.index',compact('conf', 'labels'));
    }

    public function registerAccount()
    {
        $students = Student::where('status',1)->where('school_id', auth()->user()->school_id)->get();
        return view('admin.website.register-account',compact('students'));
    }

    public function registerStudentAccount()
    {
        $students = Student::where('status',1)->where('school_id', auth()->user()->school_id)->get();
        return view('admin.website.register-student-account',compact('students'));
    }

    public function getRegisterAccount($role = null)
    {
        return view('admin.website.all-register-account', compact('role'));
    }

    public function disableAccount(Request $request)
    {
        $user = User::find($request->id);

        if($user)
        {
          $user->status = 0;
          $user->save();

          return response()->json(['success' => true]);
        }
        else
        {
          return response()->json(['success' => false]);
        }
    }

    public function activateAccount(Request $request)
    {
        $user = User::find($request->id);

        if($user)
        {
          $user->status = 1;
          $user->save();

          return response()->json(['success' => true]);
        }
        else
        {
          return response()->json(['success' => false]);
        }
    }

    public function deleteAccount(Request $request)
    {
        $user = User::find($request->id);
        $user->delete();
        return response()->json(['success' => true]);

    }

    public function getRegisterAccountRecord(Request $request)
    {
        $query = User::where('school_id', auth()->user()->school_id)->orderBy('id','desc');
        if($request->status != null)
        {
          $query->where('status',$request->status);
        }
        if($request->role != null){
          $query->where('role', $request->role);
        }else{
          $query->where('role', '!=', 'student');
        }
      // $query->latest();
        return Datatables::of($query)
        ->addColumn('action', function ($item) {
          // if($item->status == 1)
          // {
          //   $html_string = ' <a href="javascript:void(0);" class="btn selected-item-btn btn-sm disable_account deleteIcon" data-type="Disable" data-id="'.$item->id.'" title="Disable Account" >
          //                   <i class="fa fa-ban" style="font-size: 18px;"></i></a>';
          // }
          // else if($item->status == 0)
          // {
          //   $html_string = ' <a href="javascript:void(0);" class="btn selected-item-btn btn-sm activate_account deleteIcon" data-type="Activate" data-id="'.$item->id.'" title="Activate Account" style="background-color:red;color:white;">
          //                   <i class="fa fa-ban" style="font-size: 18px;"></i></a>';
          // }

          // $html_string .= ' <a href="javascript:void(0);" class="btn selected-item-btn btn-sm delete_account deleteIcon" data-type="Disable" data-id="'.$item->id.'" title="Delete Account" >
          //                   <i class="fa fa-trash-alt"></i></a>';
                            // Add the dropdown menu
    $html_string = '
    <div class="dropdown action-dropdown">
        <button class="fa fa-ellipsis-v ellipsis-button" type="button" id="triggerId'.$item->id.'" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            
        </button>
        <div class="dropdown-menu" aria-labelledby="triggerId'.$item->id.'">';
        if($item->status == 1)
        {
          $html_string .= ' <a href="javascript:void(0);" class="dropdown-item disable_account deleteIcon" data-type="Disable" data-id="'.$item->id.'" title="Disable Account" >Disable Account</a>';
        }
        else if($item->status == 0)
        {
          $html_string .= ' <a href="javascript:void(0);" class="dropdown-item activate_account deleteIcon" data-type="Activate" data-id="'.$item->id.'" title="Activate Account" style="background-color:red;color:white;">Activate Account</a>';
        }
         $html_string .= ' <a href="javascript:void(0);" class="dropdown-item delete_account deleteIcon" data-type="Disable" data-id="'.$item->id.'" title="Delete Account" >
                            Delete Account</a>
        </div>
    </div>';
            return $html_string;
        })
         ->editColumn('name', function ($item){
            $name = $item->name !== null ? $item->name : 'N.A';
            $html_string = '
                 <span class="m-l-15 inputDoubleClick" id="name" style="text-transform: capitalize;"  data-fieldvalue="'.$item->name.'">'.$name.'</span>
                <input type="text" autocomplete="nope" name="name" class="fieldFocus d-none form-control" value="'.$item->name.'" data-id="'.$item->id.'">
            ';

            return $html_string;
        })

        ->editColumn('email', function ($item){
            $email = $item->email !== null ? $item->email : 'N.A';
            $html_string = '
                 <span class="m-l-15 inputDoubleClick" id="email" style="text-transform: capitalize;"  data-fieldvalue="'.$item->email.'">'.$email.'</span>
                <input type="text" autocomplete="nope" name="email" class="fieldFocus d-none form-control" value="'.$item->email.'" data-id="'.$item->id.'">
            ';
            return $html_string;
        })

        ->editColumn('role', function ($item){
            return '<span style="text-transform: capitalize">'.$item->role.'</span>';
        })

         ->addColumn('status', function ($item){
            return $item->status == 0 ? 'Disabled' : 'Active';
        })

        ->addColumn('created_at', function ($item){
            return $item->created_at != null ? carbon::parse($item->created_at)->format('D-M-Y') :'--';
        })

        ->rawColumns(['action','name','email','role','status'])
        ->make(true);

    }

    public function addWebsiteConfiguration(Request $request)
    {
      // dd($request->all());
      $find = WebsiteConfiguration::where('school_id', auth()->user()->school_id)->first();

      if($find != null)
      {
        // return response()->json(['success' => false]);
        $school = $find;
      }
      else
      {
        $school = new WebsiteConfiguration;
        $school->school_id = auth()->user()->school_id;
      }
        // dd($request->all());
        // $school = new WebsiteConfiguration;
        if($request->hasFile('image1') && $request->image1->isValid())
        {
            $fileNameWithExt = $request->file('image1')->getClientOriginalName();
            $fileName = pathinfo($fileNameWithExt,PATHINFO_FILENAME);
            $extension = $request->file('image1')->getClientOriginalExtension();
            $fileNameToStore = $fileName.'_'.time().'.'.$extension;
            $path = $request->file('image1')->move('public/uploads/website/banners/',$fileNameToStore);
            $school->banner1 = $fileNameToStore;
        }

        if($request->hasFile('image2') && $request->image2->isValid())
        {
            $fileNameWithExt = $request->file('image2')->getClientOriginalName();
            $fileName = pathinfo($fileNameWithExt,PATHINFO_FILENAME);
            $extension = $request->file('image2')->getClientOriginalExtension();
            $fileNameToStore = $fileName.'_'.time().'.'.$extension;
            $path = $request->file('image2')->move('public/uploads/website/banners/',$fileNameToStore);
            $school->banner2 = $fileNameToStore;
        }

        if($request->hasFile('image3') && $request->image3->isValid())
        {
            $fileNameWithExt = $request->file('image3')->getClientOriginalName();
            $fileName = pathinfo($fileNameWithExt,PATHINFO_FILENAME);
            $extension = $request->file('image3')->getClientOriginalExtension();
            $fileNameToStore = $fileName.'_'.time().'.'.$extension;
            $path = $request->file('image3')->move('public/uploads/website/banners/',$fileNameToStore);
            $school->banner3 = $fileNameToStore;
        }
        if($request->hasFile('principal_photo') && $request->principal_photo->isValid())
        {
            $fileNameWithExt = $request->file('principal_photo')->getClientOriginalName();
            $fileName = pathinfo($fileNameWithExt,PATHINFO_FILENAME);
            $extension = $request->file('principal_photo')->getClientOriginalExtension();
            $fileNameToStore = $fileName.'_'.time().'.'.$extension;
            $path = $request->file('principal_photo')->move('public/uploads/website/banners/',$fileNameToStore);
            $school->principal_photo = $fileNameToStore;
        }
        $school->quote = $request->quote;
        $school->about_us = $request->about_us;
        $school->principal_name = $request->principal_name;
        $school->principal_description = $request->principal_description;
        $school->why_choose_us = $request->why_choose_us;
        $school->fee_slip_notice = $request->fee_slip_notice;
        $school->books_and_library = $request->books_and_library;
        $school->quality_teachers = $request->quality_teachers;
        $school->great_certification = $request->great_certification;
        $school->teacher_section_description = $request->teacher_section_description;
        $school->image_gallery_description = $request->image_gallery_description;
        $school->save();

        WebsiteConfigurationLabel::where('website_configuration_id', $school->id)->delete();
        foreach ($request->labels as $label) {
            WebsiteConfigurationLabel::create([
                'website_configuration_id' => $school->id,
                'label' => $label['label'],
                'value' => $label['value'],
            ]);
        }

          return response()->json(['success' => true]);
    }

    public function getWebsiteConfiguration(Request $request)
    {
        $query = WebsiteConfiguration::whereNotNull('id')->where('school_id', auth()->user()->school_id);
       
      // $query->latest();
        return Datatables::of($query)
        ->addColumn('checkbox', function ($item) {

            $html_string = '
                 <a href="javascript:void(0)" class="actionicon viewIcon" data-toggle="modal" data-target="#addWebsiteConfiguration" data-id="' . $item->id . '" title="Delete"><i class="zmdi zmdi-edit"></i></a>
                 ';
            return $html_string;
        })
        ->addColumn('banner1', function ($item){
            return '<img src="'.asset('public/uploads/website/banners/'.$item->banner1).'" style="width:102px;height:52px;">';
        })

        ->addColumn('banner2', function ($item){
            return '<img src="'.asset('public/uploads/website/banners/'.$item->banner2).'" style="width:102px;height:52px;">';
        })

        ->addColumn('banner3', function ($item){
            return '<img src="'.asset('public/uploads/website/banners/'.$item->banner3).'" style="width:102px;height:52px;">';
        })
        ->addColumn('fee_slip_notice', function ($item){
            return $item->fee_slip_notice;
        })
       
        ->rawColumns(['checkbox','banner1','banner2','banner3','fee_slip_notice'])
        ->make(true);

    }

    public function uploadStudentExcelFile(Request $request)
    {
        // dd($request->all());
        $import = new StudentBulkImport();
        Excel::import($import ,$request->file('product_excel'));
        // dd($import);
        // if($import->result == "false")
        // {
        //     return response()->json(['success' => true, 'msg' => $import->response, 'errorMsg' => $import->error_msgs]);
        // }
        // if($import->result == "true")
        // {
        //     return response()->json(['success' => false, 'msg' => $import->response, 'errorMsg' => $import->error_msgs]);
        // }
        // if($import->result == "withissues")
        // {
        //     return response()->json(['success' => "withissues", 'msg' => $import->response, 'errorMsg' => $import->error_msgs]);
        // }
    }

    public function uploadStudentExcelFileForFees(Request $request)
    {
        // dd($request->all());
        $import = new StudentBulkImportForFees();
        Excel::import($import ,$request->file('product_excel'));
        // dd($import);
        // if($import->result == "false")
        // {
        //     return response()->json(['success' => true, 'msg' => $import->response, 'errorMsg' => $import->error_msgs]);
        // }
        // if($import->result == "true")
        // {
        //     return response()->json(['success' => false, 'msg' => $import->response, 'errorMsg' => $import->error_msgs]);
        // }
        // if($import->result == "withissues")
        // {
        //     return response()->json(['success' => "withissues", 'msg' => $import->response, 'errorMsg' => $import->error_msgs]);
        // }
    }

    public function getWebsiteTeachers(Request $request){
      $data = WebsiteTeacher::where('configuration_id', $request->configuration_id);
      return Datatables::of($data)
        ->addColumn('action', function ($item) {

            $html_string = '
                 <a href="javascript:void(0)" class="actionicon viewIcon delete-teacher" data-id="' . $item->id . '" title="Delete"><i class="zmdi zmdi-delete"></i></a>
                 ';
            return $html_string;
        })
        ->addColumn('photo', function ($item){
            return '<img src="'.asset('public/uploads/website/banners/'.$item->photo).'" style="width:102px;height:52px;">';
        })

        ->addColumn('name', function ($item){
            return $item->name;
        })

        ->addColumn('description', function ($item){
            return $item->description;
        })
       
        ->rawColumns(['action','name','description','photo'])
        ->make(true);
    }

    public function addWebsiteTeacher(Request $request){
      $check = WebsiteTeacher::where('configuration_id', $request->configuration_id)->count();
      if($check >= 6){
        return response()->json(['success' => false, 'errormsg' => 'Can add a maximum of six teachers']);
      }
      $rules = [
            'name'                => 'required|string',
            'teacher_description' => 'required|string',
            'teacher_image'       => 'required|mimes:doc,docx,pdf,xls,xlsx,jpg,jpeg,png|max:500',
        ];

        // Custom error messages
        $messages = [
            '*.string' => 'The :attribute must of type string.',
            '*.required' => 'The :attribute field is required.',
            '*.max' => 'The :attribute must be less or equal to 500kb.',
            '*.mime' => 'The :attribute extension must be in [doc,docx,pdf,xls,xlsx,jpg,jpeg].',
        ];

        // Validate the request
        $this->validate($request, $rules, $messages);

        if(!$request->has('configuration_id') || @$request->configuration_id == ''){
          return response()->json(['success' => false, 'errormsg' => 'First enter the above section data']);
        }
        
        $record = new WebsiteTeacher;
        $record->name = $request->name;
        $record->configuration_id = $request->configuration_id;
        $record->description = $request->teacher_description;
        if($request->hasFile('teacher_image') && $request->teacher_image->isValid())
        {
            $fileNameWithExt = $request->file('teacher_image')->getClientOriginalName();
            $fileName = pathinfo($fileNameWithExt,PATHINFO_FILENAME);
            $extension = $request->file('teacher_image')->getClientOriginalExtension();
            $fileNameToStore = $fileName.'_'.time().'.'.$extension;
            $path = $request->file('teacher_image')->move('public/uploads/website/banners/',$fileNameToStore);
            $record->photo = $fileNameToStore;
        }
        $record->save();

        return response()->json(['success' => true, 'errormsg' => 'Record added successfully']);
    }

    public function deleteWebsiteTeacher(Request $request){
      $record = WebsiteTeacher::find($request->id);
      if(!$record){
        return response()->json(['success' => false, 'msg' => 'Record not found!']);
      }
      if(file_exists(public_path('/uploads/website/banners/'.$record->photo)))
        unlink(public_path('/uploads/website/banners/'.$record->photo));

      $record->delete();
      return response()->json(['success' => true, 'msg' => 'Record deleted successfully!']);
    }

    public function getWebsiteGallery(Request $request){
      $data = WebsiteGallery::where('configuration_id', $request->configuration_id);
      return Datatables::of($data)
        ->addColumn('action', function ($item) {

            $html_string = '
                 <a href="javascript:void(0)" class="actionicon viewIcon delete-website-gallery" data-id="' . $item->id . '" title="Delete"><i class="zmdi zmdi-delete"></i></a>
                 ';
            return $html_string;
        })
        ->addColumn('photo', function ($item){
            return '<img src="'.asset('public/uploads/website/banners/'.$item->photo).'" style="width:102px;height:52px;">';
        })
        ->rawColumns(['action','photo'])
        ->make(true);
    }

    public function addWebsiteGallery(Request $request){
      $check = WebsiteGallery::where('configuration_id', $request->configuration_id)->count();
      if($check >= 6){
        return response()->json(['success' => false, 'errormsg' => 'Can add a maximum of six images']);
      }
      $rules = [
            'gallery_image'       => 'required|mimes:doc,docx,pdf,xls,xlsx,jpg,jpeg,png|max:500',
        ];

        // Custom error messages
        $messages = [
            '*.string' => 'The :attribute must of type string.',
            '*.required' => 'The :attribute field is required.',
            '*.max' => 'The :attribute must be less or equal to 500kb.',
            '*.mime' => 'The :attribute extension must be in [doc,docx,pdf,xls,xlsx,jpg,jpeg].',
        ];

        // Validate the request
        $this->validate($request, $rules, $messages);

        if(!$request->has('configuration_id') || @$request->configuration_id == ''){
          return response()->json(['success' => false, 'errormsg' => 'First enter the above section data']);
        }
        
        $record = new WebsiteGallery;
        $record->configuration_id = $request->configuration_id;
        if($request->hasFile('gallery_image') && $request->gallery_image->isValid())
        {
            $fileNameWithExt = $request->file('gallery_image')->getClientOriginalName();
            $fileName = pathinfo($fileNameWithExt,PATHINFO_FILENAME);
            $extension = $request->file('gallery_image')->getClientOriginalExtension();
            $fileNameToStore = $fileName.'_'.time().'.'.$extension;
            $path = $request->file('gallery_image')->move('public/uploads/website/banners/',$fileNameToStore);
            $record->photo = $fileNameToStore;
        }
        $record->save();

        return response()->json(['success' => true, 'errormsg' => 'Record added successfully']);
    }

    public function deleteWebsiteGallery(Request $request){
      $record = WebsiteGallery::find($request->id);
      if(!$record){
        return response()->json(['success' => false, 'msg' => 'Record not found!']);
      }
      if(file_exists(public_path('/uploads/website/banners/'.$record->photo)))
        unlink(public_path('/uploads/website/banners/'.$record->photo));

      $record->delete();
      return response()->json(['success' => true, 'msg' => 'Record deleted successfully!']);
    }

    public function deleteConfigurationImage(Request $request){
      if($request->type == 'main'){
        $record = WebsiteConfiguration::find($request->id);
        if(!$record){
          return response()->json(['success' => false, 'msg' => 'Record not found!']);
        }

        if(file_exists(public_path('/uploads/website/banners/'.$record->banner1)))
          unlink(public_path('/uploads/website/banners/'.$record->banner1));

        $record->banner1 = null;
        $record->save();
        return response()->json(['success' => true, 'msg' => 'Image deleted successfully!']);
      }

      if($request->type == 'principal'){
        $record = WebsiteConfiguration::find($request->id);
        if(!$record){
          return response()->json(['success' => false, 'msg' => 'Record not found!']);
        }
        if(file_exists(public_path('/uploads/website/banners/'.$record->principal_photo)))
          unlink(public_path('/uploads/website/banners/'.$record->principal_photo));

        $record->principal_photo = null;
        $record->save();
        return response()->json(['success' => true, 'msg' => 'Image deleted successfully!']);
      }

      return response()->json(['success' => false, 'msg' => 'Record not found!']);
    }

}
