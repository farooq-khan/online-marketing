<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\StudentClass;
use App\Subject;
use App\Teacher;
use App\TimeTable;
use Illuminate\Http\Request;

class TimetableController extends Controller
{
    public function index()
    {
        $timetables = TimeTable::where('school_id', auth()->user()->school_id)->get();
        return view('admin.timetable.index', compact('timetables'));
    }

    public function create()
    {
        $classes = StudentClass::where('school_id', auth()->user()->school_id)->get();
        $subjects = Subject::all();
        $teachers = Teacher::where('school_id', auth()->user()->school_id)->get();
        return view('admin.timetable.create', compact('classes', 'subjects', 'teachers'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'class_id' => 'required',
            'subject_id' => 'required',
            'teacher_id' => 'required',
            'date' => 'required|date',
            'start_time' => 'required',
            'end_time' => 'required',
            'school_id' => 'required',
        ]);

        TimeTable::create($request->all());

        return redirect()->route('timetables.index')->with('success', 'Timetable created successfully.');
    }

    public function edit(Timetable $timetable)
    {
        $classes = StudentClass::where('school_id', auth()->user()->school_id)->get();
        $subjects = Subject::where('class_id', $timetable->class_id)->get();
        $teachers = Teacher::where('school_id', auth()->user()->school_id)->get();
        return view('admin.timetable.edit', compact('timetable', 'classes', 'subjects', 'teachers'));
    }

    public function update(Request $request, Timetable $timetable)
    {
        $request->validate([
            'class_id' => 'required',
            'subject_id' => 'required',
            'teacher_id' => 'required',
            'date' => 'required|date',
            'start_time' => 'required',
            'end_time' => 'required',
        ]);

        $timetable->update($request->all());

        return redirect()->route('timetables.index')->with('success', 'Timetable updated successfully.');
    }

    public function destroy(Timetable $timetable)
    {
        $timetable->delete();
        return redirect()->route('timetables.index')->with('success', 'Timetable deleted successfully.');
    }

    public function getSubjects(Request $request)
	{
	    $subjects = Subject::where('class_id', $request->id)->get();
	    return response()->json($subjects);
	}
}
