<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Mail\ResetPasswordLink;
use App\User;
use Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    protected function authenticated(Request $request, $user)
    {
        // dd($user);
        if($user->role == 'user') {
            return redirect('user/dashboard');
        }

        if($user->role == 'admin') {
            return redirect('admin/dashboard');
        }

        if($user->role == 'teacher') {
            return redirect('user/all-attendance');
        }

        if($user->role == 'student') {
            return redirect('student/student/'.$user->student_id);
        }

        return redirect('login');
    }

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function postLogin(Request $request)
    {
            // dd($request);
            $this->validateLogin($request);

            if ($this->hasTooManyLoginAttempts($request)) {
                $this->fireLockoutEvent($request);

                return $this->sendLockoutResponse($request);
            }

            if (Auth::attempt(['email' => $request->email, 'password' => $request->password,'status' => 1], $request->remember)) {
                return $this->sendLoginResponse($request);
            }
            else
            {
                    return redirect()->back()->with('suspend', true);

            }

            $this->incrementLoginAttempts($request);

            return $this->sendFailedLoginResponse($request);

    }

    protected function validateLogin(Request $request)
    {
        $this->validate($request, [
          $this->username() => 'required|string',
          'password' => 'required|string'
          //,'g-recaptcha-response' => 'required|captcha'
        ]);
    }

    public function logout(Request $request) {
          Auth::logout();
          return redirect('/login');
    }

    public function sendResetLink(Request $request){
        try {
            $user = User::where('email', $request->email)->first();
            if(!$user){
                return redirect()->back()->with('error', 'Account does not exists!');
            }
            $randomToken = Str::random(25);
            $user->reset_token = $randomToken;
            $user->save();
            Mail::to($user->email)->send(new ResetPasswordLink($user, $randomToken));
           return redirect()->back()->with('status', 'A reset link is sent to your email. Please check your email.');
        } catch (\Exception $e) {
            \Log::info($e->getMessage());
             return redirect()->back()->with('error', 'Someting went wrong!');;
        }
    }
}
