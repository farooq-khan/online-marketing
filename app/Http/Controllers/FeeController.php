<?php

namespace App\Http\Controllers;

use App\FeeType;
use App\GetMonth;
use App\OtherFee;
use App\Student;
use App\StudentClass;
use App\StudentFee;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class FeeController extends Controller
{
    public function singleStudentFee($id,$class_id)
    {
        $student = Student::find($id);
        $students = Student::where('status',1)->where('id','!=',$id)->get();
        $fee = StudentFee::where('student_id',$id)->pluck('class_id')->toArray();
        array_push($fee, $student->class_id);
        $classes = StudentClass::whereIn('id',$fee)->orderBy('class_name','asc')->get();

        return view('user.students.single-student-fee',compact('student','students','classes','class_id'));
    }

    public function otherFeeHistory($id,$class_id = null)
    {
        $id = auth()->user()->role == 'student' ? auth()->user()->student_id : $id;
        return view('user.students.other-fee-history',compact('id'));
    }

    public function getStudentOtherFeeHistory(Request $request){
        $id = auth()->user()->role == 'student' ? auth()->user()->student_id : $request->student_id;
        $data = OtherFee::where('student_id', $id)->with('fee_type', 'student_class');

        return Datatables::of($data)
        ->addColumn('fee_type', function($item){
            return @$item->fee_type->title;
        })
        ->addColumn('cls', function($item){
            return @$item->student_class->class_name;
        })

        ->rawColumns(['fee_type', 'cls'])
        ->make(true);
    }

    public function getSingleStudentFee(Request $request)
    {
    	// dd($request->all());
        $id = auth()->user()->role == 'student' ? auth()->user()->student_id : $request->student_id;

        $query = StudentFee::with('student')->where('student_id',$id);

        if($request->student_classes_select != null)
        {
            $query->where('class_id',$request->student_classes_select);
        }

        if($request->discount_student != null)
        {
            if($request->discount_student == 'discount')
            {
                $query->where('discount','>',0);
            }

            elseif($request->discount_student == 'non-discount')
            {
                $query->whereNull('discount')->orWhere('discount','==',0);
            }
        }

        if($request->status_student != null)
        {
            $query->where('status',$request->status_student);
        }

        if($request->from_date != null)
        {
            $query->whereDate('fee_month','>=',$request->from_date);
        }

        if($request->to_date != null)
        {
            $query->whereDate('fee_month','<=',$request->to_date);
        }
       
      // $query->latest();
        return Datatables::of($query)
        ->addColumn('checkbox', function ($item) {
            $html_string = '<div class="custom-control custom-checkbox custom-checkbox1 d-inline-block">
                            <input type="checkbox" class="custom-control-input check" value="'.$item->id.'" id="stone_check_'.$item->id.'">
                            <label class="custom-control-label" for="stone_check_'.$item->id.'"></label>
                        </div>';
            return $html_string;
        })
         ->editColumn('name', function ($item){
            return $item->student->name !== null ? $item->student->name : 'N.A';
        })

         ->editColumn('guardian', function ($item){
            return $item->student->guardian !== null ? $item->student->guardian : 'N.A';
        })

         ->addColumn('id', function ($item){
            return $item->id !== null ? $item->id : 'N.A';
        })

         ->editColumn('roll_no', function ($item){
            return $item->student->roll_no !== null ? $item->student->roll_no : 'N.A';
        })

         ->addColumn('gender', function ($item){
            return $item->student->gender !== null ? $item->student->gender : 'N.A';
        })

         ->addColumn('fee', function ($item){
            $fee = $item->student->student_class != null ? $item->student->student_class->fee : 0;
            if($item->student->discount != null)
            {
                $fee = (100 - $item->student->discount)/100 * $item->student->student_class->fee;
            }

            return $fee;
        })
         ->addColumn('class', function ($item){
            return $item->student_class != null ? $item->student_class->class_name : 'N.A';
        })
    
        ->addColumn('discount', function ($item){
            return $item->student->discount !== null ? $item->student->discount.' %' : 0;
        })

        ->addColumn('invoice_no', function ($item){
            return $item->receipt != null ? $item->receipt : '--';
        })

        ->addColumn('paid_amount', function ($item){
            return $item->paid_amount;
        })

        ->addColumn('unpaid_amount', function ($item){
            return $item->unpaid_amount;
        })

        ->addColumn('fee_month', function ($item){
            return carbon::parse($item->fee_month)->format('d M Y');
        })

        ->addColumn('submitted_date', function ($item){
            return carbon::parse($item->submitted_date)->format('d M Y');
        })

        ->addColumn('status', function ($item){
            if($item->status == 1)
            {
                return '<span class="badge badge-pill d-block badge-outline-success">Paid</span>';
            }
            else
            {
                return '<span class="badge badge-pill d-block badge-outline-secondary">Partial Paid</span>';
            }
        })

       

        ->rawColumns(['checkbox','address','name','roll_no','gender','guardian_phone','fee','class','id','image','guardian','invoice_no','paid_amount','unpaid_amount','fee_month','submitted_date','status'])
        ->make(true);

    }

    public function getSingleStudentFeeMonth(Request $request)
    {
    	// dd($request->all());
    	$year = $request->year;
        // $query = StudentFee::with('student')->where('student_id',$request->student_id)->whereYear('fee_month',$year)->orderBy('fee_month','asc');
        // $query = StudentFee::query()->with('student','user')->select('student_fees.*','months.name')->rightJoin('months','months.id','=','student_fees.month_id');


        // dd($query->get());

        // $query = $query->where(function($q) use ($request){
        // 	$q->where('student_id',$request->student_id)->orWhereNull('student_id');
        // })->where(function($q) use ($year){
        // 	$q->whereYear('fee_month',$year)->orWhereNull('fee_month');
        // });

        // dd($query->toSql());
        // $query = $query->orderBy('months.id','asc');

        $query = GetMonth::whereNotNull('id');
        $id = auth()->user()->role == 'student' ? auth()->user()->student_id : $request->student_id;
        $student = Student::find($id);

        // if($request->student_classes_select != null)
        // {
        //     $query->whereHas('student',function($q) use ($request){
        //         $q->where('class_id',$request->student_classes_select);
        //     });
        // }

        // if($request->discount_student != null)
        // {
        //     if($request->discount_student == 'discount')
        //     {
        //         $query->where('discount','>',0);
        //     }

        //     elseif($request->discount_student == 'non-discount')
        //     {
        //         $query->whereNull('discount')->orWhere('discount','==',0);
        //     }
        // }

        // if($request->status_student != null)
        // {
        //     $query->where('status',$request->status_student);
        // }

        // if($request->from_date != null)
        // {
        //     $query->whereDate('fee_month','>=',$request->from_date);
        // }

        // if($request->to_date != null)
        // {
        //     $query->whereDate('fee_month','<=',$request->to_date);
        // }
       
      // $query->latest();
        return Datatables::of($query)
        ->addColumn('checkbox', function ($item) {

            // $html_string = '<div class="custom-control custom-checkbox custom-checkbox1 d-inline-block">
            //                 <input type="checkbox" class="custom-control-input check" value="'.$item->id.'" id="stone_check_'.$item->id.'">
            //                 <label class="custom-control-label" for="stone_check_'.$item->id.'"></label>
            //             </div>';
        	if($item->student != null)
        	{
             $html_string = '
                 <a href="'.url('user/student-fee/'.$student->id.'').'" class="actionicon viewIcon delete_order_transaction" data-id="' . $student->id . '" title="Download Invoice"><i class="zmdi zmdi-download"></i></a>
                 ';
        	}
        	else
        	{
        		$html_string = '--';
        	}
            return $html_string;
        })
         ->editColumn('month', function ($item){
            return $item->name !== null ? $item->name : 'N.A';
        })

        ->addColumn('total_amount', function ($item) use($year,$student){
            $total_amount = $item->get_fees()->where('month_id',$item->id)->where('student_id',$student->id)->whereYear('fee_month',$year)->pluck('total_amount')->first();
            return $total_amount != null ? $total_amount : '--';
        }) 

        ->addColumn('paid_amount', function ($item)use($year,$student){
            $paid_amount = $item->get_fees()->where('month_id',$item->id)->where('student_id',$student->id)->whereYear('fee_month',$year)->pluck('paid_amount')->first();
            return $paid_amount != null ? $paid_amount : '--';
        })

        ->addColumn('unpaid_amount', function ($item)use($year,$student){
            $unpaid_amount = $item->get_fees()->where('month_id',$item->id)->where('student_id',$student->id)->whereYear('fee_month',$year)->pluck('unpaid_amount')->first();
            return $unpaid_amount != null ? $unpaid_amount : '--';
        })

         ->addColumn('submitted_date', function ($item)use($year,$student){
            $date = $item->get_fees()->where('month_id',$item->id)->where('student_id',$student->id)->whereYear('fee_month',$year)->pluck('submitted_date')->first();
            return $date != null ? carbon::parse($date)->format('d-M-Y') : '--';
        })

        ->addColumn('submitted_to', function ($item)use($year,$student){
            $rec = $item->get_fees()->where('month_id',$item->id)->where('student_id',$student->id)->whereYear('fee_month',$year)->first();
            return $rec != null ? @$rec->user->name : '--';
        })

		->addColumn('remark', function ($item)use($year,$student){
            return $item->get_fees != null ? $item->get_fees()->where('month_id',$item->id)->where('student_id',$student->id)->whereYear('fee_month',$year)->pluck('remark')->first() : '--';
        })

        ->addColumn('status', function ($item)use($year,$student){
            $status = $item->get_fees()->where('month_id',$item->id)->where('student_id',$student->id)->whereYear('fee_month',$year)->pluck('status')->first();
            if($status == 1)
            {
                return '<span class="badge badge-pill d-block badge-outline-success">Paid</span';
            }
            else if($status == 2)
            {
                return '<span class="badge badge-pill d-block badge-outline-secondary">Partial Paid</span>';
            }
            else
            {
            	return '--';
            }
        })

       

        ->rawColumns(['checkbox','total_amount','paid_amount','unpaid_amount','month','status','submitted_date','submitted_to','remark'])
        ->make(true);

    }

    public function allFeeTypes()
    {
        return view('user.students.all-fee-types');
    }

    public function addFeeType(Request $request)
    {
        $fee_type = new FeeType;

        $fee_type->title = $request->title;
        $fee_type->description = $request->description;
        $fee_type->school_id = auth()->user()->school_id;
        $fee_type->status = 1;
        $fee_type->save();

        return response()->json(['success' => true]);
    }

    public function getFeeType(Request $request)
    {
        $query = FeeType::where('school_id', auth()->user()->school_id)->orderBy('title','asc');
       
      // $query->latest();
        return Datatables::of($query)
        ->addColumn('checkbox', function ($item) {

            $html_string = ' <a href="javascript:void(0);" class="selected-item-btn delete_fee_type actionicon deleteIcon" data-type="Delete" data-id="'.$item->id.'" title="Delete Fee Type" >
                            <i class="fa fa-trash-alt"></i></a>';
            return $html_string;
        })
         ->editColumn('title', function ($item){
            $title = $item->title !== null ? $item->title : 'N.A';

            $html_string = '
                 <span class="m-l-15 inputDoubleClick" id="title"  data-fieldvalue="'.$item->title.'">'.$title.'</span>
                <input type="text" autocomplete="nope" name="title" class="fieldFocus d-none form-control" value="'.$item->title.'" data-id="'.$item->id.'">
            ';

            return $html_string;
        })

         ->editColumn('description', function ($item){
            $description = $item->description != null ? $item->description : '--';

             $html_string = '
                 <span class="m-l-15 inputDoubleClick" id="title"  data-fieldvalue="'.$item->description.'">'.$description.'</span>
                <input type="text" autocomplete="nope" name="description" class="fieldFocus d-none form-control" value="'.$item->description.'" data-id="'.$item->id.'">
            ';

            return $html_string;
        })
       
        ->rawColumns(['checkbox','title','description'])
        ->make(true);

    }

    public function deleteFeeType(Request $request)
    {
        $id = $request->id;

        $find_fee = OtherFee::where('fee_type_id',$id)->get();

        if($find_fee->count() > 0)
        {
            return response()->json(['success' => false]);
        }

        $fee_type = FeeType::where('id',$id)->first();
        $fee_type->delete();

        return response()->json(['success' => true]);
    }

    public function submitOtherFee($page = null)
    {
        $students = Student::select('id','status','roll_no','name','class_id')->where('school_id', auth()->user()->school_id)->where('status',1)->get();
        $fee_types = FeeType::where('school_id', auth()->user()->school_id)->where('status',1)->get();
        $classes = StudentClass::where('school_id', auth()->user()->school_id)->get();
        return view('user.students.submit-other-fee',compact('students','fee_types','classes','page'));
    }

    public function submitOtherFeeRecord(Request $request)
    {
        // dd($request->all());
        $other_Fee = new OtherFee;
        $student = Student::find($request->student_admission_no);

        $other_Fee->student_id = $request->student_admission_no;
        $other_Fee->class_id = $student->class_id;
        $other_Fee->fee_type_id = $request->fee_type;
        $other_Fee->amount = $request->amount;
        $other_Fee->paid_amount = $request->paid_amount;
        $other_Fee->concession = $request->concession;
        $other_Fee->unpaid_amount = $request->amount - $request->concession - $request->paid_amount;
        $other_Fee->fee_month = $request->fee_month;
        $other_Fee->submitted_date = $request->submitted_date;
        $other_Fee->remark = $request->remark;
        $other_Fee->receipt = $request->receipt;
        $other_Fee->user_id = Auth::user()->id;
        $other_Fee->school_id = Auth::user()->school_id;
        $other_Fee->save();
        if($other_Fee->unpaid_amount == 0 || $other_Fee->unpaid_amount < 0)
        {
            $other_Fee->status = 1;
        }
        else
        {
            $other_Fee->status = 0;
        }
        $other_Fee->save();
        return response()->json(['success' => true]);
    }

    public function getOtherFee(Request $request)
    {
        $query = OtherFee::where('school_id', auth()->user()->school_id)->with('student')->orderBy('id','desc');

        if($request->student_classes_select != null)
        {
            $query->where('class_id',$request->student_classes_select);
        }

        if($request->fee_type_select != null)
        {
            $query->where('fee_type_id',$request->fee_type_select);
        }

        if($request->student_id != null)
        {
            $query->where('student_id',$request->student_id);
        }

        if($request->receipt != null)
        {
            $receipt_no = explode('-', $request->receipt);
            if(isset($receipt_no[0]))
            $query->where(\DB::raw('receipt+0'),'>=',$receipt_no[0]);

            if(isset($receipt_no[1]))
            $query->where(\DB::raw('receipt+0'),'<=',$receipt_no[1]);
            
        }

        if($request->status_student != null)
        {
            $query->where('status',$request->status_student);
        }
        if($request->from_date != null)
        {
            $query->whereDate('fee_month','>=',$request->from_date);
        }

        if($request->to_date != null)
        {
            $query->whereDate('fee_month','<=',$request->to_date);
        }

        if($request->sub_from_date != null)
        {
            $query->whereDate('submitted_date','>=',$request->sub_from_date);
        }

        if($request->sub_to_date != null)
        {
            $query->whereDate('submitted_date','<=',$request->sub_to_date);
        }
        if($request->father_name != null)
        {
          $input = explode('-', $request->father_name);
            $query->whereHas('student', function($q) use($input){
              $q->where('guardian','LIKE', "%".@$input[0]."%");
              if(isset($input[1]))
                $q->where('guardian_nickname','LIKE', "%".@$input[1]."%");
            });
        }

        if($request->student_search != null)
        {
          $query->whereHas('student', function($q) use($request){
              $q->where('name','LIKE', "%$request->student_search%");
          });
            
        }
        $total_amount = (clone $query)->sum('amount');
        $concession = (clone $query)->sum('concession');
        $paid_amount = (clone $query)->sum('paid_amount');
        $unpaid_amount = (clone $query)->sum('unpaid_amount');
       
      // $query->latest();
        return Datatables::of($query)
        ->addColumn('checkbox', function ($item) {

            $html_string = ' <a href="javascript:void(0);" class="btn selected-item-btn btn-sm delete_fee_type actionicon deleteIcon" data-type="Delete" data-id="'.$item->id.'" title="Delete Fee Type" >
                            <i class="fas-trash-alt"></i></a>';
            return $html_string;
        })
        ->addColumn('action', function ($item) {
             $html_string = '
                 <a href="javascript:void(0)" class="actionicon deleteIcon delete_student_permanant" data-id="' . $item->id . '" title="Delete Other Fee"><i class="fa fa-trash-alt"></i></a>
                 ';
            $html_string .= '
                 <a href="javascript:void(0)" class="actionicon viewIcon edit_student_fee" data-toggle="modal" data-target="#EditStudentFee'.$item->id.'" data-id="' . $item->id . '" title="Edit"><i class="fa fa-edit"></i></a>
                 ';

                 // $html_string .= '
                 // <a href="javascript:void(0)" class="actionicon viewIcon edit_student_fee download_single_fee" data-id="' . $item->id . '" title="Edit"><i class="zmdi zmdi-download"></i></a>
                 // ';

                 $html_string .= '<div class="modal fade" id="EditStudentFee'.$item->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                  <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Edit Other Fee</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <form id="edit-student-fee-form-'.$item->id.'" enctype="multipart/form-data">
                      <input type="hidden" name="id" value="'.$item->id.'" />
                      <div class="modal-body ">

                          <div class="form-row">
                          
                              <div class="form-group col-md-6">
                                <label for="total_amount_edit_'.$item->id.'"><i class="zmdi zmdi-money-off pr-2"></i>Total Amount</label>
                                <input type="text" name="amount" id="total_amount_edit_'.$item->id.'" class="form-control" value="'.$item->amount.'" placeholder="0.00" disabled="disabled" />
                            </div>

                              <div class="form-group col-md-6">
                                <label for="paid_amount_edit_'.$item->id.'"><i class="zmdi zmdi-money-off pr-2"></i>Amount Paid</label>
                                <input type="number" name="paid_amount" id="paid_amount_edit_'.$item->id.'" value="'.$item->paid_amount.'" min="'.$item->paid_amount.'" max="'.$item->amount.'" class="form-control" placeholder="Paid Amount" disabled="true" />
                            </div>

                            <div class="form-group col-md-6">
                                <label for="unpaid_amount_edit_'.$item->id.'"><i class="zmdi zmdi-money-off pr-2"></i>Amount Unpaid</label>
                                <input type="number" name="paid_amount" id="unpaid_amount_edit_'.$item->id.'" value="'.$item->unpaid_amount.'" min="'.$item->unpaid_amount.'" max="'.$item->unpaid_amount.'" class="form-control" placeholder="Unpaid Amount" disabled="true" />
                            </div>

                            <div class="form-group col-md-6">
                                <label for="new_payment_'.$item->id.'"><i class="zmdi zmdi-money-off pr-2"></i>New Payment</label>
                                <input type="number" name="new_payment" id="new_payment_'.$item->id.'" value="" class="form-control" placeholder="Please Enter New Payment" />
                            </div>

                             <div class="form-group col-md-6">
                                <label for="concession_'.$item->id.'"><i class="zmdi zmdi-money-off pr-2"></i>Concession</label>
                                <input type="number" name="concession" id="concession_'.$item->id.'" value="" class="form-control" placeholder="Enter Concession(Optional)" />
                            </div>

                            <div class="form-group col-md-6">
                                <label for="concession_reason_'.$item->id.'"><i class="zmdi zmdi-money-off pr-2"></i>Concession Reason</label>
                                <input type="text" name="reason" id="concession_reason_'.$item->id.'" value="" class="form-control" placeholder="Please Enter Concession Reason(Optional)" />
                            </div>
                            <div class="form-group col-md-6">
                                <label for="submitted_date_'.$item->id.'"><i class="fa fa-calendar pr-2"></i>Submitted Date</label>
                                <input type="date" name="submitted_date" id="submitted_date_'.$item->id.'" value="" class="form-control" />
                            </div>
                            <div class="form-group col-md-6">
                                <label for="receipt'.$item->id.'"><i class="fa fa-file pr-2"></i>Receipt No.</label>
                                <input type="text" name="receipt" id="receipt'.$item->id.'" value="" class="form-control" placeholder="Please Enter Receipt No.(Optional)" />
                            </div>

                            </div>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary edit_fee" data-id="'.$item->id.'">Update</button>
                      </div>
                      </form>
                    </div>
                  </div>
                </div>';
            return $html_string;
        })
         ->addColumn('roll_no', function ($item){
            return $item->student !== null ? $item->student->roll_no : 'N.A';
        })

          ->filterColumn('roll_no', function( $query, $keyword ) {
             $query->whereHas('student', function($q) use($keyword){
                    $q->where('roll_no','LIKE', "%$keyword%");                    
                });
            })

        ->addColumn('name', function ($item){
            return $item->student != null ? $item->student->name : '--';
        })

        ->filterColumn('name', function( $query, $keyword ) {
         $query->whereHas('student', function($q) use($keyword){
                $q->where('name','LIKE', "%$keyword%");                    
            });
        })
        ->addColumn('father_name', function ($item){
            return $item->student != null ? $item->student->guardian.' '.@$item->student->guardian_nickname : '--';
        })

        // ->filterColumn('father_name', function( $query, $keyword ) {
        //  $query->whereHas('student', function($q) use($keyword){
        //         $q->where('guardian','LIKE', "%$keyword%");                    
        //     });
        // })

        ->addColumn('class', function ($item){
            return $item->student_class !== null ? $item->student_class->class_name : 'N.A';
        })

         ->addColumn('fee_type', function ($item){
            return $item->fee_type != null ? $item->fee_type->title : '--';
        })

        ->addColumn('amount', function ($item){
            return $item->amount != null ? $item->amount : '0.00';
        })

         ->addColumn('remark', function ($item){
            // return $item->remark != null ? $item->remark : '--';
             $remark = $item->remark != null ? $item->remark : '--';
            $html_string = '<span class="m-l-15 inputDoubleClick font-weight-bold" id="roll_no"  data-fieldvalue="'.$item->remark.'">'.$remark.'</span>';
            $html_string .= '
                <input type="text" autocomplete="nope" name="remark" class="fieldFocus d-none form-control" value="'.$remark.'" data-id="'.$item->id.'">
            ';

            return $html_string;
        })

         ->addColumn('submitted_date', function ($item){
            $concession = $item->submitted_date != null ? $item->submitted_date : '--';
            $html_string = '<span class="m-l-15 inputDoubleClick font-weight-bold" id="roll_no"  data-fieldvalue="'.$item->submitted_date.'">'.$concession.'</span>';
            $html_string .= '
                <input type="date" autocomplete="nope" name="submitted_date" class="fieldFocus d-none form-control" value="'.$concession.'" data-id="'.$item->id.'">
            ';

            return $html_string;
        })

        ->addColumn('empty_col', function ($item){
            return '';
        })
        ->addColumn('concession', function ($item){
            $concession = $item->concession != null ? $item->concession : 0;
            $html_string = '<span class="m-l-15 inputDoubleClick font-weight-bold" id="roll_no"  data-fieldvalue="'.$item->concession.'">'.$concession.'</span>';
            $html_string .= '
                <input type="text" autocomplete="nope" name="concession" class="fieldFocus d-none form-control" value="'.$concession.'" data-id="'.$item->id.'">
            ';

            return $html_string;
        })
        ->addColumn('paid_amount', function ($item){
             
            $concession = $item->paid_amount != null ? $item->paid_amount : 0;
            $html_string = '<span class="m-l-15 inputDoubleClick font-weight-bold" id="roll_no"  data-fieldvalue="'.$item->paid_amount.'">'.$concession.'</span>';
            $html_string .= '
                <input type="text" autocomplete="nope" name="paid_amount" class="fieldFocus d-none form-control" value="'.$concession.'" data-id="'.$item->id.'">
            ';

            return $html_string;
        })
        ->addColumn('unpaid_amount', function ($item){
            return $item->unpaid_amount != null ? $item->unpaid_amount : '0.00';
        })
        ->addColumn('fee_month', function ($item){
            return $item->fee_month != null ? carbon::parse($item->fee_month)->format('d-m-Y') : '--';
        })
        ->addColumn('receipt', function ($item){
            $concession = $item->receipt != null ? $item->receipt : '--';
            $html_string = '<span class="m-l-15 inputDoubleClick font-weight-bold" id="roll_no"  data-fieldvalue="'.$item->receipt.'">'.$concession.'</span>';
            $html_string .= '
                <input type="text" autocomplete="nope" name="receipt" class="fieldFocus d-none form-control" value="'.$concession.'" data-id="'.$item->id.'">
            ';

            return $html_string;
        })
        ->addColumn('status', function ($item){
            return $item->status !== null ? ($item->status == '0' ? '<span class="badge badge-pill d-block badge-outline-secondary">Partial Paid</span>' : '<span class="badge badge-pill d-block badge-outline-success">Paid</span>') : '<span class="badge badge-pill d-block badge-outline-success">Paid</span>';
        })
       
        ->rawColumns(['checkbox','roll_no','name','class','fee_type','amount','remark','submitted_date','empty_col','concession','paid_amount','unpaid_amount','fee_month','receipt','status','action'])
        ->with(['total_amount' => $total_amount,'concession' => $concession,'paid_amount' => $paid_amount,'unpaid_amount' => $unpaid_amount])
        ->make(true);

    }

    public function updateOtherFee(Request $request)
    {
        // dd($request->all());
        $old_value = $request->new_select_value; 
   
        $type = FeeType::where('id',$request->id)->first();

        $show_title = $request->new_select_value;
        foreach($request->except('id','new_select_value') as $key => $value)
        {
            $type->$key = $value;
        }
        $type->save();

        return response()->json(['success' => true]);
    }

    public function deleteFeeRecords(Request $request)
    {
        $ids = explode(',', $request->fee_ids);

        foreach ($ids as $id) {
            $fee = StudentFee::find($id);
            $fee->delete();
        }

        return response()->json(['success' => true]);
    }

    public function updateOtherFeeData(Request $request)
    {
        $old_value = $request->new_select_value; 
   
        $student = OtherFee::where('id',$request->id)->first();

        $show_title = $request->new_select_value;
        foreach($request->except('id','new_select_value') as $key => $value)
        {
            $student->$key = $value;
            $student->save();
                
            $student->unpaid_amount = $student->amount - $student->paid_amount - $student->concession;
            $student->save();

            if($student->unpaid_amount == 0 || $student->unpaid_amount < 0)
            {
                $student->status = 1;
            }
            else
            {
                $student->status = 0;
            }
            // $student->submitted_date = carbon::now()->format('Y-m-d');
            $student->save();
        }
        return response()->json(['success' => true]);
    }

    public function deleteOtherFee(Request $request)
    {
        $find = OtherFee::find($request->id);
        if($find)
        {
            $find->delete();
        }

        return response()->json(['success' => true]);
    }
}
