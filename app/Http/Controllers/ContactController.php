<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\ContactUsMail;

class ContactController extends Controller
{
    public function submitContactForm(Request $request)
    {
        $request->validate([
            'txtName' => 'required|string|max:255',
            'txtEmail' => 'required|email',
            'txtPhone' => 'required|string|max:20',
            'txtMsg' => 'required|string',
        ]);

        $data = [
            'name' => $request->txtName,
            'email' => $request->txtEmail,
            'phone' => $request->txtPhone,
            'message' => $request->txtMsg,
        ];

        try {
            Mail::to('4schoolm@gmail.com')->send(new ContactUsMail($data));
            return response()->json(['success' => true, 'message' => 'Message sent successfully!']);
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'message' => 'Failed to send message. Please try again later.']);
        }
    }
}