<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Auth;

class HomeController extends Controller
{
    
    public function login()
    {
        if(Auth::user()->role == 'admin')
        {
            return redirect('/admin/dashboard');
        }
        if(Auth::user()->role == 'user')
        {
            return redirect('/user/dashboard');
        }
        if(Auth::user()->role == 'student')
        {
            return redirect('/student/student/'.Auth()->user()->id);
        }
        return view('auth.login');
    }

    public function userDashboard()
    {
    	return view('user.dashboard.index');
    }

    public function updateProfile()
    {
    	return view('user.dashboard.profile');
    }

    public function saveChangesProfile(Request $request){
        // dd($request->all());

        $user_id = Auth::user()->id;

        $user = User::where('id',$user_id)->first();
        $user->name = $request->name;
        if($request->password != null)
        {
            $user->password = bcrypt($request->password);
        }
        if($request->hasFile('logo') && $request->logo->isValid())
        {         
            $fileNameWithExt = $request->file('logo')->getClientOriginalName();
            $fileName = pathinfo($fileNameWithExt,PATHINFO_FILENAME);
            $extension = $request->file('logo')->getClientOriginalExtension();
            $fileNameToStore = $fileName.'_'.time().'.'.$extension;
            $path = $request->file('logo')->move('public/uploads/users/logos/',$fileNameToStore);
            $user->logo = $fileNameToStore;
        }
        $user->save();

        return response()->json([
            "error" => false,  
         ]);
    }

}
