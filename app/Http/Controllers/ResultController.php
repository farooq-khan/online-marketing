<?php

namespace App\Http\Controllers;

use App\Exam;
use App\ExportStatus;
use App\Exports\ExamResultExport;
use App\Exports\TestResultExport;
use App\GetMonth;
use App\Imports\ExamResultImport;
use App\Imports\TestResultImport;
use App\Jobs\ExportFeeInvoiceToPdfJob;
use App\OtherFee;
use App\Result;
use App\SoldBooksReport;
use App\Student;
use App\StudentClass;
use App\StudentFee;
use App\StudentTest;
use App\Subject;
use App\TestResult;
use App\WebsiteConfiguration;
use Auth;
use Carbon\Carbon;
use Excel;
use Illuminate\Http\Request;
use PDF;
use Yajra\Datatables\Datatables;

class ResultController extends Controller
{
    public function allResult()
    {
        $classes = StudentClass::where('school_id', auth()->user()->school_id)->orderBy('class_name','asc')->get();
        $exams = Exam::where('school_id', auth()->user()->school_id)->orderBy('id','asc')->get();
        return view('user.result.index',compact('classes','exams'));
    }

    public function submitResult($id,$exam_id, $year, $from_date, $to_date, $new_res = false)
    {
    	$class_sub = Subject::where('class_id',$id)->get();
        return view('user.result.submit-result',compact('class_sub','id','exam_id','year','from_date','to_date', 'new_res'));

    }

    public function exportExamResult(Request $request){
        // dd($request->all());
        $class_sub = Subject::where('class_id',$request->id)->get();
        $query = Student::with('student_class')->where('status',1)->where('class_id',$request->id);
        $std_classes = Subject::where('class_id',$request->id)->get();
        $ex = Exam::find($request->exam_id);
        return \Excel::download(new ExamResultExport($class_sub,$query,$std_classes, $ex, $request), 'Result Export.xlsx');
    }

    public function importExamResult(Request $request){
        // dd($request->all());
        $import = new ExamResultImport($request);
         try {
            $result = Excel::import($import, $request->file('product_excel'));
            return response()->json(['success' => true]);
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'errors' => 'Please Upload Valid File']);
        }
    }

    // test import and export
    public function exportTestResult(Request $request){
        // dd($request->all());
        $class_sub = Subject::where('class_id',$request->id)->get();
        $query = Student::with('student_class')->where('status',1)->where('class_id',$request->id);
        $std_classes = Subject::where('class_id',$request->id)->get();
        $ex = StudentTest::find($request->exam_id);
        return \Excel::download(new TestResultExport($class_sub,$query,$std_classes, $ex, $request), 'Result Export.xlsx');
    }
    public function importTestResult(Request $request){
        // dd($request->all());
        $import = new TestResultImport($request);
         try {
            $result = Excel::import($import, $request->file('product_excel'));
            return response()->json(['success' => true]);
        } catch (\Exception $e) {
            dd($e);
            return response()->json(['success' => false, 'errors' => 'Please Upload Valid File']);
        }
    }

    public function allExams()
    {
        return view('user.exams.index');
    }

    public function addExam(Request $request)
    {
        // dd($request->all());

        $exam = new Exam;

        $exam->name = $request->name;
        $exam->exam_date        = $request->exam_date;
        $exam->school_id        = auth()->user()->school_id;
        $exam->save();

        return response()->json(['success' => true]);
    }

    public function getExams(Request $request)
    {
        $query = Exam::where('school_id', auth()->user()->school_id)->orderBy('id','asc');
       
      // $query->latest();
        return Datatables::of($query)
        ->addColumn('checkbox', function ($item) {

            $html_string = ' <a href="javascript:void(0);" class="actionicon delete_class deleteIcon" data-type="Delete" data-id="'.$item->id.'" title="Delete Class" >
                            <i class="fa fa-trash-alt"></i></a>';
            return $html_string;
        })
         ->addColumn('name', function ($item){
            $title = $item->name !== null ? $item->name : 'N.A';
            $html_string = '
                 <span class="m-l-15 inputDoubleClick font-weight-bold" id="title"  data-fieldvalue="'.$item->name.'">'.$title.'</span>
                <input type="text" autocomplete="nope" name="name" class="fieldFocus d-none form-control" value="'.$item->name.'" data-id="'.$item->id.'">
            ';

            return $html_string;
        })

         ->addColumn('date', function ($item){
            return $item->exam_date != null ? carbon::parse($item->exam_date)->format('d/m/y') : '--';
        })
       
        ->rawColumns(['checkbox','name','date'])
        ->make(true);

    }

    public function getSpecificClassResult(Request $request)
    {
        if($request->new_res){
            $students = Student::where('class_id', $request->class_id)->where('status', 1)->get();
            foreach ($students as $student) {
                Result::updateOrCreate([
                    'student_id' => $student->id,
                    'exam_id'   => $request->exam_id,
                    'year'      => $request->year,
                    'class_id'  => $request->class_id
                ],[
                    'student_id'    => $student->id,
                    'exam_id'       => $request->exam_id,
                    'class_id'      => $request->class_id,
                    'year'          => $request->year,
                    'from_date'     => $request->from_date,
                    'to_date'       => $request->to_date,
                    'school_id'       => auth()->user()->school_id,
                ]);
            }
        }
        $ids = Result::where('exam_id', $request->exam_id)->where('year', $request->year)->where('class_id', $request->class_id)->pluck('student_id')->toArray();
            // $query = Student::with('student_class')->where('status',1)->where('class_id',$request->class_id);
        $query = Student::with('student_class')->whereIn('id', $ids);

        $std_classes = Subject::where('class_id',$request->class_id)->get();

        if($request->student_classes_select != null)
        {
            $query->whereHas('student_class',function($q) use ($request){
                $q->where('id',$request->student_classes_select);
            });
        }

        if($request->discount_student != null)
        {
            if($request->discount_student == 'discount')
            {
                $query->where('discount','>',0);
            }

            elseif($request->discount_student == 'non-discount')
            {
                $query->whereNull('discount')->orWhere('discount','==',0);
            }
        }
       $ex = Exam::find($request->exam_id);
      // $query->latest();
        $dt = Datatables::of($query);

        $dt->addColumn('checkbox', function ($item) use ($request){
          $result = Result::where('student_id',$item->id)->where('class_id',$item->class_id)->where('year',$request->year)->where('exam_id',$request->exam_id)->first();
          if($result){
            $html_string = '<div class="custom-control custom-checkbox custom-checkbox1 d-inline-block">
                            <input type="checkbox" class="custom-control-input check" value="'.@$result->id.'" id="stone_check_'.$item->id.'">
                            <label class="custom-control-label" for="stone_check_'.$item->id.'"></label>
                        </div>';
            return $html_string;
          }
          return '--';
        });

        $dt->addColumn('action', function ($item) use($request){
            $result = Result::where('student_id',$item->id)->where('class_id',$item->class_id)->where('year',$request->year)->where('exam_id',$request->exam_id)->first();

            // $html_string = '<div class="custom-control custom-checkbox custom-checkbox1 d-inline-block">
            //                 <input type="checkbox" class="custom-control-input check" value="'.$item->id.'" id="stone_check_'.$item->id.'">
            //                 <label class="custom-control-label" for="stone_check_'.$item->id.'"></label>
            //             </div>';
            $html_string = '';
             $html_string = '
                 <a href="'.url('user/student-exam-history/'.$item->id.'/'.$request->class_id.'').'" class="actionicon viewIcon delete_order_transaction" data-id="' . $item->id . '" title="View Details"><i class="fa fa-eye"></i></a>
                 ';
            if($result){
                $html_string .= '  <a href="javascript:void(0)" class="actionicon viewIcon download_invoice" data-id="'.@$result->id.'" title="Download Invoice"><i class="zmdi zmdi-download"></i></a>';
            }
            // $html_string .= ' <a href="javascript:void(0);" class="delete_student deleteIcon" data-type="Delete" data-id="'.$item->id.'" title="Delete Student" >
            //                 <i class="fa fa-trash-alt"></i></a>';
            return $html_string;
        });

         $dt->editColumn('name', function ($item){
            return $item->name !== null ? $item->name : 'N.A';
        });

         $dt->addColumn('guardian', function ($item){
            return $item->guardian !== null ? $item->guardian.' '.@$item->guardian_nickname : 'N.A';
        });
         $dt->filterColumn('guardian', function( $query, $keyword ) {
         $query->where(function($q) use ($keyword){
            $q->where('guardian','LIKE', "%$keyword%")->orWhere('guardian_nickname','LIKE', "%$keyword%");
         });
        });

         $dt->editColumn('roll_no', function ($item){
            return $item->roll_no !== null ? $item->roll_no : 'N.A';
        });
 
         $dt->addColumn('class', function ($item){
            return $item->student_class != null ? $item->student_class->class_name : 'N.A';
        });

         $dt->addColumn('exam', function ($item) use($request,$ex){
         	 
            return $ex->name != null ? $ex->name : '--';
        });

         if($std_classes->count() > 0)
         {
         	foreach ($std_classes as $class) {
         		$dt->addColumn($class->name, function($item) use ($class,$request){
         			$result = Result::where('student_id',$item->id)->where('class_id',$class->class_id)->where('subject_id',$class->id)->where('year',$request->year)->where('exam_id',$request->exam_id)->first();
         			$val = $result != null ? $result->obtained_marks : null;
         			$html_string = '<span class="m-l-15 inputDoubleClick" id="obtained_marks'.$item->id.'"  data-fieldvalue="'.$val.'">'.($val != null ? $val : '--').'</span><input type="text" autocomplete="nope" name="obtained_marks" class="fieldFocus d-none form-control" value="'.(@$val!=null?$val:'').'" data-id="'.$class->id.'" data-student="'.$item->id.'">';
         			return $html_string;
         		});
         	}
         }
        
         $dt->escapeColumns([]);
        $dt->rawColumns(['checkbox','action','address','name','roll_no','gender','guardian_phone','fee','class','id','image','guardian','exam']);
        return $dt->make(true);

    }

    public function updateStudentResult(Request $request)
    {
    	// dd($request->all());
     //    $student = Student::where('id',$request->id)->first();

        $result = Result::where('student_id',$request->student_id)->where('exam_id',$request->exam_id)->where('year',$request->year)->where('class_id',$request->class_id)->where('subject_id',$request->subject_id)->first();



        if($result == null)
        {
        	$result = new Result;

        	$result->student_id = $request->student_id;
        	$result->exam_id = $request->exam_id;
        	$result->class_id = $request->class_id;
        	$result->subject_id = $request->subject_id;
            $result->year = $request->year;
            $result->from_date = $request->from_date;
            $result->to_date = $request->to_date;
            $result->school_id = auth()->user()->school_id;

        	$result->save();
        }

        $show_title = $request->new_select_value;
        foreach($request->except('id','subject_id','exam_id','student_id','class_id','year','from_date','to_date') as $key => $value)
        {
            // dump($value > $result->student_subject->exam_score);
            if($value > $result->student_subject->exam_score)
            {
                // dd($value > $result->student_subject->exam_score);
                return response()->json(['score_greater' => true]);
            }
            $result->$key = $value;
            $result->total_marks = $result->student_subject->exam_score;
        }

        $result->save();

        $result->percentage = ($result->obtained_marks / $result->total_marks ) * 100;
        $result->save();

        if($result->percentage >= 80)
        {
        	$result->grade = 'A1';
        }
        else if($result->percentage < 80 and $result->percentage >= 70)
        {
        	$result->grade = 'A';
        }
        else if($result->percentage < 70 and $result->percentage >= 60)
        {
        	$result->grade = 'B';
        }
        else if($result->percentage < 60 and $result->percentage >= 50)
        {
        	$result->grade = 'C';
        }
        else if($result->percentage < 50 and $result->percentage >= 40)
        {
        	$result->grade = 'D';
        }
        else if($result->percentage < 40 and $result->percentage >= 33)
        {
        	$result->grade = 'E';
        }
        else if($result->percentage < 33)
        {
        	$result->grade = 'F';
        }

        $result->save();

        return response()->json(['success' => true]);
    }

    public function getAllResults(Request $request)
    {
        $query = Result::where('school_id', auth()->user()->school_id)->whereNotNull('id');

          $query = Result::where('school_id', auth()->user()->school_id)->select(\DB::Raw('results.*'))->groupBy('exam_id','class_id','year')->orderBy('id', 'DESC');
        // dd($query->get());
      // $query->latest();
        if($request->student_classes_select != null)
        {
            $query->where('class_id',$request->student_classes_select);
        }

        if($request->exam_id != null)
        {
            $query->where('exam_id',$request->exam_id);
        }
        if($request->from_date != null)
        {
            $query->whereDate('from_date','>=',$request->from_date);
        }

        if($request->to_date != null)
        {
            $query->whereDate('to_date','<=',$request->to_test_date);
        }
        // dd($query->toSql());
        $dt = Datatables::of($query);

        $dt->addColumn('empty_col', function ($item){
            return '';
        });

        $dt->addColumn('checkbox', function ($item) {
            $html_string = '<div class="custom-control custom-checkbox custom-checkbox1 d-inline-block">
                            <input type="checkbox" class="custom-control-input check" value="'.$item->student_id.'" id="stone_check_'.$item->id.'">
                            <label class="custom-control-label" for="stone_check_'.$item->id.'"></label>
                        </div>';
            return $html_string;
        });

        $dt->addColumn('action', function ($item) {
        	$html_string = '<div class="d-flex">';
             // $html_string .= '
             //     <a href="'.url('user/student-exam-history/'.$item->student_id.'/'.$item->class_id).'" class="actionicon viewIcon delete_order_transaction mr-2" data-id="' . $item->id . '" title="Delete"><i class="fa fa-eye"></i></a>
             //     ';
            $html_string .= '
                 <a href="'.url('user/submit-result/'.$item->class_id.'/'.$item->exam_id.'/'.$item->year.'/'.($item->from_date ?? $item->year).'/'.($item->to_date ?? $item->year)).'" class="actionicon viewIcon delete_order_transaction mr-2" data-id="' . $item->id . '" title="View Details"><i class="fa fa-eye"></i></a>
                 ';
            // $html_string .= '  <a href="javascript:void(0)" class="actionicon viewIcon download_invoice mr-2" data-id="'.$item->id.'" title="Download Invoice"><i class="zmdi zmdi-download"></i></a>';
            $html_string .= '
                 <a href="javascript:void(0)" class="actionicon deleteIcon delete_student_permanant" data-id="' . $item->id . '" title="Delete Result"><i class="fa fa-trash-alt"></i></a>
                 ';
            $html_string .= '</div>';
            
            return $html_string;
        });
 
         $dt->addColumn('class', function ($item){
            return $item->student_class != null ? $item->student_class->class_name : 'N.A';
        });

         $dt->addColumn('exam', function ($item){
            return $item->student_exam != null ? $item->student_exam->name : 'N.A';
        });

         $dt->addColumn('from_date', function ($item){
            return $item->from_date != null ? $item->from_date : 'N.A';
        });

         $dt->addColumn('to_date', function ($item){
            return $item->to_date != null ? $item->to_date : 'N.A';
        });
        
         $dt->escapeColumns([]);
        $dt->rawColumns(['action','checkbox','class','exam','from_date','to_date','empty_col']);
        return $dt->make(true);

    }

    public function studentExamHistory($id,$class_id = null)
    {
    	$student = Student::find($id);
        $students = Student::where('status',1)->where('id','!=',$id)->get();
        $classes = StudentClass::orderBy('class_name','asc')->get();
        $student_class = Result::where('student_id',$id)->pluck('class_id')->toArray();
    	$class_sub = Subject::where('class_id',$class_id)->get();
      array_push($student_class, $student->class_id);
        $student_classes = StudentClass::whereIn('id',$student_class)->orderBy('class_name','asc')->get();

        return view('user.result.exam-history',compact('student','students','classes','class_sub','id','student_classes','class_id'));
    }

    public function getSpecificStudentResult(Request $request)
    {
    	$query = Exam::where('school_id', auth()->user()->school_id);
    	$class = StudentClass::find($request->student_classes_select);
      // dd($class);
        // $student_class = Result::where('student_id',$request->std_id)->pluck('class_id')->toArray();
        // $query = Result::where('student_id',$request->std_id);
        
        // $total_marks = $query->sum('total_marks');
        // $obtained_marks = $query->sum('obtained_marks');
        // $std_classes = Subject::all();
        // $query = StudentClass::whereIn('id',$student_class);
    	// $std_classes = Subject::whereIn('class_id',$student_class)->get();
        $id = auth()->user()->role == 'student' ? auth()->user()->student_id : $request->std_id;
    	$student = Student::find($id);
       
      // $query->latest();
        $dt = Datatables::of($query);

        // $std_classes = Subject::where('class_id',$class->id)->get();
        $dt->addColumn('checkbox', function ($item)use($student,$class) {
        $result = Result::where('student_id',$student->id)->where('class_id',$class->id)->where('exam_id',$item->id)->orderBy('id','desc')->first();
        // dd($result->id);
        if($result != null)
        {
            $html_string = '<div class="d-flex">';
            $html_string .= '  <a href="javascript:void(0)" class="actionicon viewIcon download_invoice" data-id="'.@$result->id.'" title="Download Invoice"><i class="zmdi zmdi-download"></i></a>';
            $html_string .= '</div>';
            return $html_string;
        }
        else
        {
            return '--';
        }
        });

         $dt->editColumn('name', function ($item) use($student){
            return $student->name !== null ? $student->name : 'N.A';
        });

         $dt->editColumn('guardian', function ($item) use($student){
            return $student->guardian !== null ? $student->guardian : 'N.A';
        });

         $dt->editColumn('roll_no', function ($item) use($student){
            return $student->roll_no !== null ? $student->roll_no : 'N.A';
        });
 
         $dt->addColumn('class', function ($item) use($class){
            return $class != null ? $class->name : 'N.A';
        });

         $dt->addColumn('exam', function ($item){
            return $item->name != null ? $item->name : '--';
        });

    	$std_classes = Subject::where('class_id',$class->id)->get();
         if($std_classes->count() > 0)
         {

         	foreach ($std_classes as $classs) {
         		$dt->addColumn($classs->name, function($item) use ($classs,$student,$class){
         			$result = Result::where('student_id',$student->id)->where('class_id',$classs->class_id)->where('exam_id',$item->id)->where('subject_id',$classs->id)->first();
         			$val = $result != null ? $result->obtained_marks : null;
         			$html_string = '<span class="m-l-15" id="obtained_marks'.$item->id.'"  data-fieldvalue="'.$val.'">'.($val != null ? $val : '--').'</span>';
         			return $html_string;
         		});
         	}
         }

         $dt->addColumn('total_marks', function ($item) use($student,$class){
         	$total_marks = Result::where('student_id',$student->id)->where('class_id',$class->id)->where('exam_id',$item->id)->sum('total_marks');

            return $total_marks != null ? $total_marks : 0;
        });

         $dt->addColumn('obtained_marks', function ($item) use($student,$class){
         	$obtained_marks = Result::where('student_id',$student->id)->where('class_id',$class->id)->where('exam_id',$item->id)->sum('obtained_marks');

            return $obtained_marks != null ? $obtained_marks : 0;
        });

         $dt->addColumn('percentage', function ($item) use($student,$class){
         	$total_marks = Result::where('student_id',$student->id)->where('class_id',$class->id)->where('exam_id',$item->id)->sum('total_marks');
         	$obtained_marks = Result::where('student_id',$student->id)->where('class_id',$class->id)->where('exam_id',$item->id)->sum('obtained_marks');
         	if($total_marks != 0)
         	{
	         	$per = ($obtained_marks / $total_marks) * 100;
	            return number_format($per,2,'.',',').' %';
        	}
        	else
        	{
        		return 0;
        	}	
        });

         $dt->addColumn('grade', function ($item) use($student,$class){
         	$total_marks = Result::where('student_id',$student->id)->where('class_id',$class->id)->where('exam_id',$item->id)->sum('total_marks');
         	$obtained_marks = Result::where('student_id',$student->id)->where('class_id',$class->id)->where('exam_id',$item->id)->sum('obtained_marks');
         	if($total_marks != 0)
         	{
         		$per = ($obtained_marks / $total_marks) * 100;

         		$grade = $student->get_grade($per);
            	return $grade;
            }
            else 
            {
            	return 0;
            }
        });

          $dt->addColumn('position', function ($item) use($student,$class){
          	// $total_marks = Result::where('student_id',$student->id)->where('class_id',$class->id)->where('exam_id',$item->id)->sum('total_marks');
         	$obtained_marks = Result::where('student_id',$student->id)->where('class_id',$class->id)->where('exam_id',$item->id)->sum('obtained_marks');

         	// $per = ($obtained_marks / $total_marks) * 100;
            if($obtained_marks != null && $obtained_marks != 0)
            {
         	    $pos = $item->get_position($item->id,$class->id,$obtained_marks);
                return $pos;
            }
            else
            {
                return '--';
            }
        });
        
         $dt->escapeColumns([]);
        $dt->rawColumns(['checkbox','address','name','roll_no','gender','guardian_phone','fee','class','id','image','guardian','exam','total_marks','obtained_marks','percentage','grade']);
        return $dt->make(true);

    }

    public function deleteExam(Request $request)
    {
        $id = $request->id;

        $find_students = Result::where('exam_id',$id)->get();

        if($find_students->count() > 0)
        {
            return response()->json(['success' => false]);
        }

        $exam = Exam::find($id);

        $exam->delete();

        return response()->json(['success' => true]);
    }

    public function deleteSubject(Request $request)
    {
        $id = $request->id;

        $find_result = Result::where('subject_id',$id)->get();

        if($find_result->count() > 0)
        {
            return response()->json(['success' => false]);
        }

        $subject = Subject::find($id);

        $subject->delete();

        return response()->json(['success' => true]);
    }

    public function checkSameResult(Request $request)
    {
        // dd($request->all());

        $fees = explode(',',$request->fee_ids);
        $student_fee = StudentFee::whereIn('id',$fees)->distinct('student_id')->count('student_id');

        return response()->json(['same_student' => $student_fee]);


    }

    public function printInvoices(Request $request, $fees = null)
    {
        $fees = explode(',',$request->fees);
        $conf = WebsiteConfiguration::first();
        $student_fee = StudentFee::whereIn('id',$fees)->get();

        if($student_fee->count() == 1)
        {
            // dd($student);
          $single_student = StudentFee::whereIn('id',$fees)->first();
          // dd($single_student);
          $dues = StudentFee::where('student_id',$single_student->student_id)->where('fee_month','<',$single_student->fee_month)->orderBy('id','asc')->sum('unpaid_amount');

          $other_dues = OtherFee::where('student_id',$single_student->student_id)->where('fee_month','<',$single_student->fee_month)->orderBy('id','asc')->sum('unpaid_amount');
          $concession = StudentFee::where('student_id',$single_student->student_id)->where('fee_month','<',$single_student->fee_month)->orderBy('id','asc')->sum('concession');

          $other_concession = OtherFee::where('student_id',$single_student->student_id)->where('fee_month','<',$single_student->fee_month)->orderBy('id','asc')->sum('concession');
          $received = StudentFee::where('student_id',$single_student->student_id)->where('fee_month','<',$single_student->fee_month)->orderBy('id','asc')->sum('paid_amount');
          $other_received = OtherFee::where('student_id',$single_student->student_id)->where('fee_month','<',$single_student->fee_month)->orderBy('id','asc')->sum('paid_amount');
          $last_records = StudentFee::where('student_id',$single_student->student_id)->where('fee_month','<=',$single_student->fee_month)->orderBy('id','asc')->limit(12)->get();

          $other_fee_current_month = OtherFee::where('student_id',$single_student->student_id)->whereMonth('fee_month',carbon::parse($single_student->fee_month)->format('m'))->whereYear('fee_month',carbon::parse($single_student->fee_month)->format('Y'))->orderBy('id','asc')->sum('amount');
          $books_total = SoldBooksReport::where('student_id',$single_student->student_id)->orderBy('id','asc')->sum('total_amount');
          $books_paid = SoldBooksReport::where('student_id',$single_student->student_id)->orderBy('id','asc')->sum('paid_amount');

          $books_dues = number_format($books_total,2,'.',',');
          // dd($last_records);
            $pdf = PDF::loadView('user.invoices.single-fee-invoice',compact('student_fee','last_records','single_student','dues','received','concession','other_dues','books_total','books_paid','books_dues','other_fee_current_month','other_concession','other_received','conf'));
            $customPaper = array(0,0,320,480);
            $pdf->setPaper($customPaper);
            // $pdf->setPaper('A4', 'portrait');
            $pdf->getDomPDF()->set_option("enable_php", true);
              // making pdf name starts
            $makePdfName='invoice';
            return $pdf->stream(
               $makePdfName.'.pdf',
                array(
                  'Attachment' => 0
                )
              );
        }
        else
        {
            // dd($student);

            $pdf = PDF::loadView('user.invoices.fee-invoice',compact('student_fee'));
            // $customPaper = array(0,0,360,360);
            // $dompdf->set_paper($customPaper);
            $pdf->setPaper('A4', 'portrait');
            $pdf->getDomPDF()->set_option("enable_php", true);
              // making pdf name starts
            $makePdfName='invoice';
            return $pdf->stream(
               $makePdfName.'.pdf',
                array(
                  'Attachment' => 0
                )
              );
        }
    }

    public function printInvoicesStudentWise(Request $request, $fees = null)
    {
        ini_set('memory_limit', '-1');
        $fees = explode(',',$request->fees);
        $conf = WebsiteConfiguration::first();
        $student_fee = StudentFee::with('student.student_class')->whereIn('id',$fees)->get();
            // dd($student);
         
          // dd($last_records);
            $pdf = PDF::loadView('user.invoices.single-fee-invoice-student-wise',compact('student_fee','conf'));
            // $customPaper = array(0,0,320,450);
            // $pdf->setPaper($customPaper);
            // $pdf->setPaper('A4', 'portrait');
            $pdf->getDomPDF()->set_option("enable_php", true);
              // making pdf name starts
            $makePdfName='invoice';
            return $pdf->stream(
               $makePdfName.'.pdf',
                array(
                  'Attachment' => 0
                )
              );
        }
    public function printInvoicesAll(Request $request)
    {
        // $fees = explode(',',$request->ids);
        // dd($fees);
        $data = $request->all();
        $check=ExportStatus::where('user_id',Auth::user()->id)->where('type','all_fee_invoice')->first();
        if($check==null)
        {
             $pdfStatus=new ExportStatus();
             $pdfStatus->user_id=Auth::user()->id;
             $pdfStatus->type='all_fee_invoice';
             $pdfStatus->status=1;
             if($pdfStatus->save())
             {
                 $group_to_pdf_job = (new ExportFeeInvoiceToPdfJob($data,Auth::user()->id));
                 dispatch($group_to_pdf_job);
                 return response()->json(['success'=>true]);
     
             }
             return response()->json(['success'=>false]);
        }
        elseif($check->status==0)
        {
             ExportStatus::where('user_id',Auth::user()->id)->where('type','all_fee_invoice')->update(['status'=>1]);
             $group_to_pdf_job = (new ExportFeeInvoiceToPdfJob($data,Auth::user()->id));
             dispatch($group_to_pdf_job);
             return response()->json(['success'=>true]);
        }
    }
    public function getPdfStatus(Request $request)
    {   

        $status=ExportStatus::where('user_id',Auth::user()->id)->where('type','all_fee_invoice')->first();
        if($status!=null)
        {
            return response()->json(['status'=>$status->status,'exception' => $status->exception]);
        }
        else
        {
            return response()->json(['status'=>0,'exception' => $status->exception]);
        }
    }

    public function printInvoicesAdvance(Request $request, $fees = null,$fee_month = null)
    {
        $fees = explode(',',$request->fees);
        $students = Student::whereIn('id',$fees)->get();

            $pdf = PDF::loadView('user.invoices.single-fee-invoice-advance',compact('students','fee_month'));
            $customPaper = array(0,0,360,340);
            $pdf->setPaper($customPaper);
            // $pdf->setPaper('A4', 'portrait');
            $pdf->getDomPDF()->set_option("enable_php", true);
              // making pdf name starts
            $makePdfName='invoice';
            return $pdf->stream(
               $makePdfName.'.pdf',
                array(
                  'Attachment' => 0
                )
              );
    }

    public function printInvoicesAdvanceAll(Request $request, $fees = null,$fee_month = null)
    {
        $fees = explode(',',$request->fees);
        $students = Student::whereIn('id',$fees)->get();

            $pdf = PDF::loadView('user.invoices.single-fee-invoice-advance-all',compact('students','fee_month'));
            // $customPaper = array(0,0,360,340);
            // $pdf->setPaper($customPaper);
            // $pdf->setPaper('A4', 'portrait');
            $pdf->getDomPDF()->set_option("enable_php", true);
              // making pdf name starts
            $makePdfName='invoice';
            return $pdf->stream(
               $makePdfName.'.pdf',
                array(
                  'Attachment' => 0
                )
              );
    }

    public function printWholeInvoices(Request $request, $ids, $std_id, $class_id)
    {
      $ids = explode(',', $ids);
      $student = Student::find($std_id);
      $query = TestResult::where('student_id',$std_id)->where('class_id',$class_id)->whereIn('id',$ids)->groupBy('test_id','from_test_date')->get();
      // $min_date = (clone $query)->min('from_test_date');
      // $max_date = (clone $query)->max('to_test_date');
      $student_class = StudentClass::find($class_id);

      // $query = TestResult::where('student_id',$std_id)->where('class_id',$class_id)->whereDate('from_test_date','>=',$min_date)->whereDate('to_test_date','<=',$max_date)->get();
      $subjects = Subject::where('class_id',$class_id)->get();

      // dd($query);

      $pdf = PDF::loadView('user.invoices.multi-test-invoice',compact('query','student','subjects','class_id','student_class'));
      $pdf->setPaper('A4', 'landscape');
      $pdf->getDomPDF()->set_option("enable_php", true);
              // making pdf name starts
      $makePdfName='invoice';
      return $pdf->stream(
         $makePdfName.'.pdf',
          array(
            'Attachment' => 0
          )
        );
    }

    public function printResult(Request $request, $id)
    {
        $student_result = Result::find($id);
        $student = Student::find($student_result->student_id);
            // dd($student);
        $subjects_result = Result::where('student_id',$student_result->student_id)->where('class_id',$student_result->class_id)->where('exam_id',$student_result->exam_id)->whereNotNull('total_marks')->get();


        $total_marks = $subjects_result->sum('total_marks');
        $obtained_marks = $subjects_result->sum('obtained_marks');

        $subjects = Subject::where('class_id',$student_result->class_id)->with(['subject_exam_results' => function($sub) use ($student_result){
            $sub->where('student_id',$student_result->student_id);
        }])->get();

        $grand_total = $subjects->sum('exam_score');
        // dd($subjects);

            $pdf = PDF::loadView('user.invoices.result-invoice',compact('student_result','subjects_result','total_marks','obtained_marks','student','subjects', 'grand_total'));
            // $customPaper = array(0,0,360,360);
            // $dompdf->set_paper($customPaper);
            $pdf->setPaper('A4', 'portrait');
            $pdf->getDomPDF()->set_option("enable_php", true);
              // making pdf name starts
            $makePdfName='invoice';
            return $pdf->stream(
               $makePdfName.'.pdf',
                array(
                  'Attachment' => 0
                )
              );
    }

    public function getFeeYearInvoice(Request $request, $student_id, $year)
    {
        $student = Student::find($student_id);
        $months = GetMonth::all();

        $pdf = PDF::loadView('user.invoices.fee-year-invoice',compact('student','months','year'));
        // $customPaper = array(0,0,360,360);
        // $dompdf->set_paper($customPaper);
        $pdf->setPaper('A4', 'portrait');
        $pdf->getDomPDF()->set_option("enable_php", true);
          // making pdf name starts
        $makePdfName='invoice';
        return $pdf->stream(
           $makePdfName.'.pdf',
            array(
              'Attachment' => 0
            )
          );
    }

    public function allTests()
    {
        return view('user.tests.index');
    }

    public function addTest(Request $request)
    {
        // dd($request->all());

        $test = new StudentTest;

        $test->name = $request->name;
        $test->total_marks = $request->total_marks;
        $test->status = 1;
        $test->school_id = auth()->user()->school_id;
        $test->save();

        return response()->json(['success' => true]);
    }

    public function getTests(Request $request)
    {
        $query = StudentTest::where('school_id', auth()->user()->school_id)->orderBy('id','asc');
       
      // $query->latest();
        return Datatables::of($query)
        ->addColumn('checkbox', function ($item) {

            $html_string = ' <a href="javascript:void(0);" class="actionicon delete_class deleteIcon" data-type="Delete" data-id="'.$item->id.'" title="Delete Class" >
                            <i class="fa fa-trash-alt"></i></a>';
            return $html_string;
        })
         ->addColumn('name', function ($item){
             $title = $item->name !== null ? $item->name : 'N.A';
            $html_string = '
                 <span class="m-l-15 inputDoubleClick font-weight-bold" id="title"  data-fieldvalue="'.$item->name.'">'.$title.'</span>
                <input type="text" autocomplete="nope" name="name" class="fieldFocus d-none form-control" value="'.$item->name.'" data-id="'.$item->id.'">
            ';

            return $html_string;
        })

          ->addColumn('total_marks', function ($item){
            return $item->total_marks !== null ? $item->total_marks : 'N.A';
        })
       
        ->rawColumns(['checkbox','name','total_marks'])
        ->make(true);

    }

    public function deleteTest(Request $request)
    {
        $id = $request->id;

        $find_students = TestResult::where('test_id',$id)->get();

        if($find_students->count() > 0)
        {
            return response()->json(['success' => false]);
        }

        $exam = StudentTest::find($id);

        $exam->delete();

        return response()->json(['success' => true]);
    }

    public function allTestResult()
    {
        $classes = StudentClass::where('school_id', auth()->user()->school_id)->orderBy('class_name','asc')->get();
        $exams = StudentTest::where('school_id', auth()->user()->school_id)->orderBy('id','asc')->get();
        return view('user.test-results.index',compact('classes','exams'));
    }

    public function submitTestResult($id,$exam_id,$test_date,$to_test_date, $new_test = false)
    {
        $class_sub = Subject::where('class_id',$id)->get();
        $find_test = StudentTest::find($exam_id);
        return view('user.test-results.submit-result',compact('class_sub','id','exam_id','test_date','find_test','to_test_date','new_test'));

    }

    public function getSpecificClassTestResult(Request $request)
    {
        // $result = TestResult::where('class_id',$request->class_id)->where('test_id',$request->exam_id)->whereDate('to_test_date','=',$request->test_date)->first();
        if($request->new_test){
            $students = Student::where('class_id', $request->class_id)->where('status', 1)->get();
            foreach ($students as $student) {
                TestResult::updateOrCreate([
                    'student_id' => $student->id,
                    'test_id'   => $request->exam_id,
                    'from_test_date'      => $request->test_date,
                    'class_id'  => $request->class_id
                ],[
                    'student_id'    => $student->id,
                    'test_id'       => $request->exam_id,
                    'class_id'      => $request->class_id,
                    'from_test_date'     => $request->test_date,
                    'to_test_date'       => $request->to_test_date,
                    'school_id'       => auth()->user()->school_id,
                ]);
            }
        }
        $ids = TestResult::where('test_id', $request->exam_id)->where('from_test_date', $request->test_date)->where('class_id', $request->class_id)->pluck('student_id')->toArray();
        // dd($ids);
        if(count($ids) == 0)
        {
          $query = Student::where('class_id',$request->class_id)->where('status',1);
        }
        else
        {
          // $query = Student::with('student_result')->whereHas('student_result',function($z)use($request){
          //   $z->where('class_id',$request->class_id);
          // })->where('status',1);

          $query = Student::with('student_result')->whereIn('id', $ids);

        }
        

        $std_classes = Subject::where('class_id',$request->class_id)->get();

        if($request->student_classes_select != null)
        {
            $query->whereHas('student_class',function($q) use ($request){
                $q->where('id',$request->student_classes_select);
            });
        }

        if($request->discount_student != null)
        {
            if($request->discount_student == 'discount')
            {
                $query->where('discount','>',0);
            }

            elseif($request->discount_student == 'non-discount')
            {
                $query->whereNull('discount')->orWhere('discount','==',0);
            }
        }

        $class = StudentClass::find($request->class_id);
       
      // $query->latest();
        $dt = Datatables::of($query);

         $dt->addColumn('checkbox', function ($item){
             $html_string = '<div class="custom-control custom-checkbox custom-checkbox1 d-inline-block">
                            <input type="checkbox" class="custom-control-input check" value="'.$item->id.'" id="stone_check_'.$item->id.'">
                            <label class="custom-control-label" for="stone_check_'.$item->id.'"></label>
                        </div>';

            return $html_string;
        });

        $dt->addColumn('action', function ($item)use($class) {
             $html_string = '
                 <a href="'.url('user/student-test-history/'.$item->id.'/'.$class->id.'').'" class="actionicon viewIcon delete_order_transaction" data-id="' . $item->id . '" title="Delete"><i class="fa fa-eye"></i></a>
                 ';
            return $html_string;
        });

         $dt->editColumn('name', function ($item){
            return $item->name !== null ? $item->name : 'N.A';
        });

         $dt->addColumn('guardian', function ($item){
            return $item->guardian !== null ? $item->guardian.' '.@$item->guardian_nickname : 'N.A';
        });
         $dt->filterColumn('guardian', function( $query, $keyword ) {
         $query->where(function($q) use ($keyword){
            $q->where('guardian','LIKE', "%$keyword%")->orWhere('guardian_nickname','LIKE', "%$keyword%");
         });
        });

         $dt->editColumn('roll_no', function ($item){
            return $item->roll_no !== null ? $item->roll_no : 'N.A';
        });
 
         $dt->addColumn('class', function ($item) use($class){
            return $class != null ? $class->class_name : 'N.A';
        });

         $dt->addColumn('exam', function ($item) use($request){
            $ex = StudentTest::find($request->exam_id); 
            return $ex->name != null ? $ex->name : '--';
        });

         if($std_classes->count() > 0)
         {
            foreach ($std_classes as $class) {
                $dt->addColumn($class->name, function($item) use ($class,$request){
                    $result = TestResult::where('student_id',$item->id)->where('class_id',$class->class_id)->where('subject_id',$class->id)->where('test_id',$request->exam_id)->whereDate('to_test_date','>=',$request->test_date)->first();
                    $val = $result != null ? $result->obtained_marks : null;
                    $html_string = '<span class="m-l-15 inputDoubleClick" id="obtained_marks'.$item->id.'"  data-fieldvalue="'.$val.'">'.($val != null ? $val : '--').'</span><input type="text" autocomplete="nope" name="obtained_marks" class="fieldFocus d-none form-control" value="'.(@$val!=null?$val:'').'" data-id="'.$class->id.'" data-student="'.$item->id.'">';
                    return $html_string;
                });
            }
         }
        
         $dt->escapeColumns([]);
        $dt->rawColumns(['checkbox','address','name','roll_no','gender','guardian_phone','fee','class','id','image','guardian','exam','action']);
        return $dt->make(true);

    }

    public function updateStudentTestResult(Request $request)
    {
        // dd($request->all());
     //    $student = Student::where('id',$request->id)->first();
        $find_test = StudentTest::find($request->exam_id);
        $result = TestResult::where('student_id',$request->student_id)->where('test_id',$request->exam_id)->where('class_id',$request->class_id)->where('subject_id',$request->subject_id)->whereDate('from_test_date',$request->test_date)->whereDate('to_test_date',$request->to_test_date)->first();



        if($result == null)
        {
            // $result_dates = TestResult::where('student_id',$request->student_id)->where('test_id',$request->exam_id)->where('class_id',$request->class_id)->whereDate('to_test_date','>=',$request->test_date)->first();
            $result = new TestResult;

            $result->student_id = $request->student_id;
            $result->test_id = $request->exam_id;
            $result->class_id = $request->class_id;
            $result->subject_id = $request->subject_id;
            $result->from_test_date = $request->test_date;
            $result->to_test_date = $request->to_test_date;
            $result->school_id = auth()->user()->school_id;

            $result->save();
        }

        $show_title = $request->new_select_value;
        foreach($request->except('id','subject_id','exam_id','student_id','class_id','test_date','to_test_date') as $key => $value)
        {
            if($value > $find_test->total_marks || $value < 0)
            {
                return response()->json(['score_greater' => true]);
            }
            $result->$key = $value;
            $result->total_marks = $find_test->total_marks;
        }

        $result->save();

        $result->percentage = ($result->obtained_marks / $result->total_marks ) * 100;
        $result->save();

        return response()->json(['success' => true]);
    }

    public function getAllTestResults(Request $request)
    {
        $query = TestResult::whereNotNull('id');

          $query = TestResult::where('school_id', auth()->user()->school_id)->select(\DB::Raw('sum(total_marks) as total'),'test_results.*')->groupBy('from_test_date','class_id')->orderBy('id', 'DESC');

        // dd($query->get());
      // $query->latest();
        if($request->student_classes_select != null)
        {
            $query->where('class_id',$request->student_classes_select);
        }

        if($request->exam_id != null)
        {
            $query->where('test_id',$request->exam_id);
        }

        if($request->from_test_date != null)
        {
            $query->whereDate('from_test_date','>=',$request->from_test_date);
        }

        if($request->to_test_date != null)
        {
            $query->whereDate('to_test_date','<=',$request->to_test_date);
        }
        // dd($query->toSql());
        $dt = Datatables::of($query);

        $dt->addColumn('checkbox', function ($item)use($request) {
            // $html_string = '<div class="custom-control custom-checkbox custom-checkbox1 d-inline-block">
            //                 <input type="checkbox" class="custom-control-input check" value="'.$item->id.'" id="stone_check_'.$item->id.'">
            //                 <label class="custom-control-label" for="stone_check_'.$item->id.'"></label>
            //             </div>';
            $html_string = '<div class="d-flex">';
            if($item->student_test != null)
            {
             $html_string .= '
                 <a href="'.url('user/submit-test-result/'.@$item->student_class->id.'/'.$item->student_test->id.'/'.@$item->from_test_date.'/'.$item->to_test_date.'').'" class="actionicon viewIcon delete_order_transaction mr-2" data-id="' . $item->id . '" title="Delete"><i class="fa fa-eye"></i></a>
                 ';
            }
            $html_string .= '  <a href="javascript:void(0)" class="actionicon viewIcon download_invoice d-none" data-id="'.$item->id.'" title="Download Invoice"><i class="zmdi zmdi-download"></i></a>';
            $html_string .= '
                 <a href="javascript:void(0)" class="actionicon deleteIcon delete_student_permanant" data-id="' . $item->id . '" title="Delete Result"><i class="fa fa-trash-alt"></i></a>
                 ';
            $html_string .= '</div>';
            return $html_string;
        });
 

         $dt->addColumn('class', function ($item){
            return $item->student_class != null ? $item->student_class->class_name : 'N.A';
        });

         $dt->addColumn('test_name', function ($item){
            return $item->student_test != null ? $item->student_test->name : 'N.A';
        });

         $dt->addColumn('total_marks', function ($item){
            return $item->student_test != null ? $item->student_test->total_marks : 'N.A';
        });

        $dt->addColumn('test_date', function ($item){
            return $item->from_test_date != null ? $item->from_test_date : 'N.A';
        });

        $dt->addColumn('to_test_date', function ($item){
            return $item->to_test_date != null ? $item->to_test_date : 'N.A';
        });
        
         $dt->escapeColumns([]);
        $dt->rawColumns(['checkbox','class','test_name','total_marks','test_date']);
        return $dt->make(true);

    }

    public function studentTestHistory($id,$class_id = null)
    {
        $student = Student::find($id);
        $students = Student::where('status',1)->where('id','!=',$id)->get();
        $classes = StudentClass::orderBy('class_name','asc')->get();
        $student_class = TestResult::where('student_id',$id)->pluck('class_id')->toArray();
        array_push($student_class, $student->class_id);
        $class_sub = Subject::where('class_id',$class_id)->get();

        $student_classes = StudentClass::whereIn('id',$student_class)->orderBy('class_name','asc')->get();
        return view('user.test-results.test-history',compact('student','students','classes','class_sub','id','student_classes','class_id'));
    }

    public function getSpecificStudentTestResult(Request $request)
    {
        $id = auth()->user()->role == 'student' ? auth()->user()->student_id : $request->std_id;
        $student = Student::find($id);
        $class = StudentClass::find($request->student_classes_select);
        $query = TestResult::where('student_id',$id)->where('class_id',$request->student_classes_select)->groupBy('test_id','from_test_date');
       
      // $query->latest();
        $dt = Datatables::of($query);

        // $std_classes = Subject::where('class_id',$class->id)->get();
        $dt->addColumn('checkbox', function ($item) {

            $html_string = '<div class="custom-control custom-checkbox custom-checkbox1 d-inline-block">
                            <input type="checkbox" class="custom-control-input check" value="'.$item->id.'" id="stone_check_'.$item->id.'">
                            <label class="custom-control-label" for="stone_check_'.$item->id.'"></label>
                        </div>';
            return $html_string;
        });
        $dt->addColumn('action', function ($item){
       
            $html_string = '<div class="d-flex">';
            $html_string .= '  <a href="javascript:void(0)" class="actionicon viewIcon download_invoice2" data-id="'.@$item->id.'" title="Download Invoice"><i class="zmdi zmdi-download"></i></a>';
            $html_string .= '</div>';
            return $html_string;
        });

         $dt->addColumn('name', function ($item) use($student){
            return $student->name !== null ? $student->name : 'N.A';
        });

         $dt->addColumn('guardian', function ($item) use($student){
            return $student->guardian !== null ? $student->guardian : 'N.A';
        });

         $dt->addColumn('roll_no', function ($item) use($student){
            return $student->roll_no !== null ? $student->roll_no : 'N.A';
        });
 
         $dt->addColumn('class', function ($item) use($class){
            return $class != null ? $class->class_name : 'N.A';
        });

         $dt->addColumn('exam', function ($item){
            return $item->student_test != null ? $item->student_test->name : '--';
        });

        $std_classes = Subject::where('class_id',$class->id)->get();
         if($std_classes->count() > 0)
         {

            foreach ($std_classes as $classs) {
                $dt->addColumn($classs->name, function($item) use ($classs,$student,$class){
                    $result = TestResult::where('student_id',$student->id)->where('class_id',$classs->class_id)->where('test_id',$item->test_id)->where('subject_id',$classs->id)->whereDate('from_test_date',$item->from_test_date)->first();
                    $val = $result != null ? $result->obtained_marks : null;
                    $html_string = '<span class="m-l-15" id="obtained_marks'.$item->id.'"  data-fieldvalue="'.$val.'">'.($val != null ? $val : '--').'</span>';
                    return $html_string;
                });
            }
         }

         $dt->addColumn('total_marks', function ($item) use($student,$class){
            $total_marks = TestResult::where('student_id',$student->id)->where('class_id',$class->id)->where('test_id',$item->test_id)->whereDate('from_test_date',$item->from_test_date)->sum('total_marks');

            return $total_marks != null ? $total_marks : 0;
        });

         $dt->addColumn('obtained_marks', function ($item) use($student,$class){
            $obtained_marks = TestResult::where('student_id',$student->id)->where('class_id',$class->id)->where('test_id',$item->test_id)->whereDate('from_test_date',$item->from_test_date)->sum('obtained_marks');

            return $obtained_marks != null ? $obtained_marks : 0;
        }); 

        $dt->addColumn('percentage', function ($item) use($student,$class){
            $total_marks = TestResult::where('student_id',$student->id)->where('class_id',$class->id)->where('test_id',$item->test_id)->whereDate('from_test_date',$item->from_test_date)->sum('total_marks');
            $obtained_marks = TestResult::where('student_id',$student->id)->where('class_id',$class->id)->where('test_id',$item->test_id)->whereDate('from_test_date',$item->from_test_date)->sum('obtained_marks');
        $per = $total_marks != 0 ? ($obtained_marks / $total_marks) * 100 : 0;

            return round($per, 2);
        });

        $dt->addColumn('grade_and_position', function ($item) use($student,$class){
            $total_marks = TestResult::where('student_id',$student->id)->where('class_id',$class->id)->where('test_id',$item->test_id)->whereDate('from_test_date',$item->from_test_date)->sum('total_marks');
            $obtained_marks = TestResult::where('student_id',$student->id)->where('class_id',$class->id)->where('test_id',$item->test_id)->whereDate('from_test_date',$item->from_test_date)->sum('obtained_marks');
            $per = $total_marks != 0 ? ($obtained_marks / $total_marks) * 100 : 0;

            $grade = $student->get_grade($per);
            $post = $student->get_position($per);

            return $grade.' & '.($post != '' ? $post : '--');
        });

        $dt->addColumn('start_date', function ($item){
            return $item->from_test_date != null ? carbon::parse($item->from_test_date)->format('d-M-Y') : '--';
        });

        $dt->addColumn('end_date', function ($item){
            return $item->to_test_date != null ? carbon::parse($item->to_test_date)->format('d-M-Y') : '--';
        });   
        
         $dt->escapeColumns([]);
        $dt->rawColumns(['action','address','name','roll_no','gender','guardian_phone','fee','class','id','image','guardian','exam','total_marks','obtained_marks','percentage','grade','checkbox','position','grade_and_position']);
        return $dt->make(true);

    }

    public function printTestInvoices($std_ids,$class_id,$exam_id,$test_date)
    {
        $ids = explode(',',$std_ids);
        // dd($ids);
        $test = StudentTest::find($exam_id);
        $subjects = Subject::where('class_id',$class_id)->get();
        $student_result = TestResult::whereIn('student_id',$ids)->where('class_id',$class_id)->where('test_id',$exam_id)->whereDate('from_test_date',$test_date)->groupBy('student_id')->get();
        $pdf = PDF::loadView('user.invoices.test-invoice',compact('student_result','test','subjects'));
        // $customPaper = array(0,0,360,360);
        // $dompdf->set_paper($customPaper);
        $pdf->setPaper('A4', 'portrait');
        $pdf->getDomPDF()->set_option("enable_php", true);
          // making pdf name starts
        $makePdfName='invoice';
        return $pdf->stream(
           $makePdfName.'.pdf',
            array(
              'Attachment' => 0
            )
          );
    }

    public function printTestInvoicesFull($std_ids,$class_id,$exam_id,$test_date)
    {
        $ids = explode(',',$std_ids);
        // dd($ids);
        $test = StudentTest::find($exam_id);
        $subjects = Subject::where('class_id',$class_id)->get();
        $student_result = TestResult::whereIn('student_id',$ids)->where('class_id',$class_id)->where('test_id',$exam_id)->whereDate('from_test_date',$test_date)->groupBy('student_id')->get();
        $pdf = PDF::loadView('user.invoices.test-invoice-full',compact('student_result','test','subjects'));
        // $customPaper = array(0,0,360,360);
        // $dompdf->set_paper($customPaper);
        $pdf->setPaper('A4', 'landscape');
        $pdf->getDomPDF()->set_option("enable_php", true);
          // making pdf name starts
        $makePdfName='invoice';
        return $pdf->stream(
           $makePdfName.'.pdf',
            array(
              'Attachment' => 0
            )
          );
    }

    public function printResultAll(Request $request, $std_ids)
    {
        $ids = explode(',',$std_ids);

        // $all_results = Result::select(\DB::Raw('sum(total_marks) as total, sum(obtained_marks) as sub_total'),'results.*')->where('exam_id',$exam_id)->whereIn('student_id',$ids)->groupBy('student_id')->orderBy('id', 'DESC')->get();

        $student_results = Result::find($ids);
        $pdf = PDF::loadView('user.invoices.result-invoice-all',compact('student_results'));
        // $customPaper = array(0,0,360,360);
        // $dompdf->set_paper($customPaper);
        $pdf->setPaper('A4', 'portrait');
        $pdf->getDomPDF()->set_option("enable_php", true);
          // making pdf name starts
        $makePdfName='invoice';
        return $pdf->stream(
           $makePdfName.'.pdf',
            array(
              'Attachment' => 0
            )
          );
    }

    public function deleteStudentResultPermanent(Request $request)
    {
      $result = Result::find($request->id);

      $all_result = Result::where('class_id',$result->class_id)->where('exam_id',$result->exam_id)->where('year',$result->year)->get();

      if($all_result->count() > 0)
      {
        foreach ($all_result as $res) {
          $res->delete();
        }
      }

      return response()->json(['success' => true]);
    }

    public function deleteStudentTestResultPermanent(Request $request)
    {
      $result = TestResult::find($request->id);

      $all_result = TestResult::where('class_id',$result->class_id)->where('test_id',$result->test_id)->whereDate('from_test_date',$result->from_test_date)->whereDate('to_test_date',$result->to_test_date)->get();

      if($all_result->count() > 0)
      {
        foreach ($all_result as $res) {
          $res->delete();
        }
      }

      return response()->json(['success' => true]);
    }

    public function updateExamData(Request $request)
    {
        // dd($request->all());
        $old_value = $request->new_select_value; 
   
        $type = Exam::where('id',$request->id)->first();

        $show_title = $request->new_select_value;
        foreach($request->except('id','new_select_value') as $key => $value)
        {
            $type->$key = $value;
        }
        $type->save();

        return response()->json(['success' => true]);
    }
    public function updateTestData(Request $request)
    {
        // dd($request->all());
        $old_value = $request->new_select_value; 
   
        $type = StudentTest::where('id',$request->id)->first();

        $show_title = $request->new_select_value;
        foreach($request->except('id','new_select_value') as $key => $value)
        {
            $type->$key = $value;
        }
        $type->save();

        return response()->json(['success' => true]);
    }
}
