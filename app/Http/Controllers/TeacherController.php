<?php

namespace App\Http\Controllers;

use App\Models\Common\Department;
use App\Models\Common\Designation;
use App\Models\Common\Payscale;
use App\Student;
use App\StudentClass;
use App\Teacher;
use App\TeacherAttendance;
use App\TeacherSalary;
use Auth;
use Carbon\Carbon;
use File;
use Illuminate\Http\Request;
use PDF;
use Yajra\Datatables\Datatables;
use Validator;

class TeacherController extends Controller
{
    public function index()
    {
      $departments = Department::where('school_id', auth()->user()->school_id)->get();
      $designations = Designation::where('school_id', auth()->user()->school_id)->get();
    	return view('user.teachers.index', compact('departments', 'designations'));
    }

    public function addStaff()
    {
      $designations = Designation::where('school_id', auth()->user()->school_id)->get();
      $departments = Department::where('school_id', auth()->user()->school_id)->get();
      $payscales = Payscale::where('school_id', auth()->user()->school_id)->get();
      return view('user.teachers.add-staff', compact('designations', 'departments', 'payscales'));
    }

    public function addTeacher(Request $request)
    {
        // dd($request->all());
        // Validation rules
        $validator = Validator::make($request->all(), [
            'name'                => 'required|string|max:255',
            'guardian'            => 'required|string|max:255',
            'guardian_relation'   => 'required|string|max:255',
            'cnic'                => 'required|numeric|digits:13',
            'phone'               => 'required|numeric|digits_between:10,15',
            'address'             => 'required|string|max:500',
            'gender'              => 'required|in:male,female,other',
            'email'               => 'required|email|unique:teachers,email',
            'education'           => 'nullable|string|max:255',
            'experience'          => 'nullable|string|max:255',
            'department'          => 'required|exists:departments,id', // Ensure the department exists
            'designation'         => 'required|exists:designations,id', // Ensure the designation exists
            'payscale'            => 'required|exists:payscales,id', // Ensure the payscale exists
            'joining_date'        => 'required|date',
            'salary'              => 'required|numeric|min:0',
            'image'               => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048', // Optional, for image upload
        ]);

        // Check if validation fails
        if ($validator->fails()) {
            return response()->json(['success' => false, 'errors' => $validator->errors()]);
        }

        $teacher = new Teacher;

        $teacher->name = $request->name;
        $teacher->guardian = $request->guardian;
        $teacher->cnic = $request->cnic;
        $teacher->phone = $request->phone;
        $teacher->address = $request->address;
        $teacher->guardian_relation = $request->guardian_relation;
        $teacher->experience = $request->experience;
        $teacher->gender = $request->gender;
        $teacher->education = $request->education;
        $teacher->designation_id = $request->designation;
        $teacher->payscale_id = $request->payscale;
        $teacher->department_id = $request->department;
        $teacher->joining_date = $request->joining_date;
        $teacher->salary = $request->salary;
        $teacher->email = $request->email;
        // $teacher->user_id = Auth::user()->id;
        $teacher->school_id = Auth::user()->school_id;
        $teacher->status = 1;

        if($request->hasFile('image') && $request->image->isValid())
          {
              $fileNameWithExt = $request->file('image')->getClientOriginalName();
              $fileName = pathinfo($fileNameWithExt,PATHINFO_FILENAME);
              $extension = $request->file('image')->getClientOriginalExtension();
              $fileNameToStore = $fileName.'_'.time().'.'.$extension;
              $path = $request->file('image')->move('public/uploads/teachers/images/',$fileNameToStore);
              $teacher->image = $fileNameToStore;
          }

        $teacher->save();

          return response()->json(['success' => true]);
    }

    public function getTeachers(Request $request)
    {
        $query = Teacher::where('school_id', auth()->user()->school_id)->with('department', 'designation', 'payscale');

       if($request->teacher_status != null)
       {
        $query->where('status',$request->teacher_status);
       }

       if($request->department != ''){
        $query->where('department_id', $request->department);
       }
       if($request->designation != ''){
        $query->where('designation_id', $request->designation);
       }
       if($request->search != ''){
        $query->where('name', 'LIKE', '%'.$request->search.'%');
       }
      // $query->latest();
        return Datatables::of($query)
        ->addColumn('checkbox', function ($item) {
            $html_string = '<div class="custom-control custom-checkbox custom-checkbox1 d-inline-block">
                            <input type="checkbox" class="custom-control-input check" value="'.$item->id.'" id="stone_check_'.$item->id.'">
                            <label class="custom-control-label" for="stone_check_'.$item->id.'"></label>
                        </div>';
            return $html_string;
        })
        ->addColumn('action', function ($item) {

            // $html_string = '<div class="custom-control custom-checkbox custom-checkbox1 d-inline-block">
            //                 <input type="checkbox" class="custom-control-input check" value="'.$item->id.'" id="stone_check_'.$item->id.'">
            //                 <label class="custom-control-label" for="stone_check_'.$item->id.'"></label>
            //             </div>';

             $html_string = '
                 <a href="'.url('user/teacher/'.$item->id.'').'" class="actionicon viewIcon delete_order_transaction" data-id="' . $item->id . '" title="Delete"><i class="fa fa-eye"></i></a>
                 ';
            if($item->status == 1)
            {
            $html_string .= ' <a href="javascript:void(0);" class="suspend_teacher actionicon suspendIcon fas fa-user-minus" data-type="Delete" data-id="'.$item->id.'" title="Suspend Teacher" >
                            </a>';
            }
            elseif($item->status == 2)
            {
                $html_string .= ' <a href="javascript:void(0);" class="activate_teacher actionicon activateIcon fas fa-user-plus" data-type="Delete" data-id="'.$item->id.'" title="Activate Teacher" >
                            </a>';
            }
            $html_string .= '
                 <a href="javascript:void(0)" class="actionicon deleteIcon delete_student_permanant" data-id="' . $item->id . '" title="Delete Teacher"><i class="fa fa-trash-alt"></i></a>
                 ';
            return $html_string;
        })
         ->editColumn('name', function ($item){
            return $item->name !== null ? $item->name : 'N.A';
        })

         ->editColumn('guardian', function ($item){
            return $item->guardian !== null ? $item->guardian : 'N.A';
        })
         ->editColumn('guardian_relation', function ($item){
            return $item->guardian_relation !== null ? $item->guardian_relation : 'N.A';
        })

         ->editColumn('phone', function ($item){
            return $item->phone !== null ? $item->phone : 'N.A';
        })

         ->addColumn('education', function ($item){
            return $item->education !== null ? $item->education : 'N.A';
        }) 
        ->addColumn('experience', function ($item){
            return $item->experience !== null ? $item->experience : 'N.A';
        })
        ->addColumn('department', function ($item){
            return $item->department !== null ? $item->department->name : 'N.A';
        })
        ->addColumn('designation', function ($item){
            return $item->designation !== null ? $item->designation->name : 'N.A';
        })
        ->addColumn('payscale', function ($item){
            return $item->payscale !== null ? $item->payscale->name : 'N.A';
        })
         ->editColumn('joining_date', function ($item){
            return $item->joining_date !== null ? $item->joining_date : 'N.A';
        })
         ->editColumn('leaving_date', function ($item){
            return $item->leaving_date !== null ? $item->leaving_date : 'N.A';
        })

        ->addColumn('empty_col', function ($item){
            return '';
        })
        ->rawColumns(['action','name','guardian','phone','education','joining_date','designation','empty_col','checkbox', 'department', 'payscale'])
        ->make(true);

    }

    public function singleTeacher($id)
    {
        $teacher = Teacher::find($id);
        return view('user.teachers.single-teacher',compact('teacher'));
    }

    public function suspendTeacher(Request $request)
    {
        $id = $request->id;
        $find_teacher = Teacher::where('id',$id)->first();
        $find_teacher->status = 2;
        $find_teacher->save();

        return response()->json(['success' => true]);
    }

    public function suspendedTeachers()
    {
    	return view('user.teachers.suspended-teachers');
    }

    public function getSuspendedTeachers(Request $request)
    {
        $query = Teacher::where('school_id', auth()->user()->school_id)->where('status',2);

       
      // $query->latest();
        return Datatables::of($query)
        ->addColumn('action', function ($item) {

            // $html_string = '<div class="custom-control custom-checkbox custom-checkbox1 d-inline-block">
            //                 <input type="checkbox" class="custom-control-input check" value="'.$item->id.'" id="stone_check_'.$item->id.'">
            //                 <label class="custom-control-label" for="stone_check_'.$item->id.'"></label>
            //             </div>';

             $html_string = '
                 <a href="'.url('user/teacher/'.$item->id.'').'" class="actionicon viewIcon delete_order_transaction" data-id="' . $item->id . '" title="Delete"><i class="fa fa-eye"></i></a>
                 ';
            $html_string .= ' <a href="javascript:void(0);" class="activate_teacher viewIcon" data-type="Delete" data-id="'.$item->id.'" title="Activate Teacher" >
                            <i class="zmdi zmdi-check-circle" style="font-size: 18px;"></i></a>';
            return $html_string;
        })
         ->editColumn('name', function ($item){
            return $item->name !== null ? $item->name : 'N.A';
        })

         ->editColumn('guardian', function ($item){
            return $item->guardian !== null ? $item->guardian : 'N.A';
        })

         ->editColumn('phone', function ($item){
            return $item->phone !== null ? $item->phone : 'N.A';
        })

         ->addColumn('education', function ($item){
            return $item->education !== null ? $item->education : 'N.A';
        })
         ->editColumn('joining_date', function ($item){
            return $item->joining_date !== null ? $item->joining_date : 'N.A';
        })


        

        ->rawColumns(['action','name','guardian','phone','education','joining_date'])
        ->make(true);

    }

    public function activateTeacher(Request $request)
    {
        $id = $request->id;
        $find_teacher = Teacher::where('id',$id)->first();
        $find_teacher->status = 1;
        $find_teacher->save();

        return response()->json(['success' => true]);
    }

    public function teacherSalaries()
    {
      $teachers = Teacher::where('school_id', auth()->user()->school_id)->where('status',1)->get();
      return view('user.teachers.teacher-salaries',compact('teachers'));
    }

    public function fetchTeacherDetail(Request $request)
    {
        $teacher = teacher::where('id',$request->id)->first();

        return response()->json(['teacher' => $teacher , 'success' => true]);
    }

    public function addTeacherSalary(Request $request)
    {
        // dd($request->all());
        $teacher = Teacher::find($request->name);
        $total_amount_bonus = $request->bonus_amount;

        $month = carbon::parse($request->salary_month)->format('m');
        $year = carbon::parse($request->salary_month)->format('Y');
        $check_month = TeacherSalary::where('teacher_id',$teacher->id)->where('month',$month)->where('year',$year)->first();

        if($check_month != null)
        {
            return response()->json(['paid' => true]);
        }
        $teacher_salary = new TeacherSalary;

        $teacher_salary->teacher_id = $teacher->id;
        $teacher_salary->total_amount = $teacher->salary;
        $teacher_salary->paid_amount = $request->paid_amount;
        $teacher_salary->deduction = $request->deduction;
        $teacher_salary->reason = $request->reason;
        $teacher_salary->unpaid_amount = $teacher->salary - $request->paid_amount - $request->deduction + $total_amount_bonus;
        $teacher_salary->bonus = $request->bonus;
        $teacher_salary->bonus_description = $request->bonus_description;
        $teacher_salary->bonus_amount = $total_amount_bonus;
        $teacher_salary->salary_month = $request->salary_month;
        $teacher_salary->submitted_date = $request->submitted_date;
        $teacher_salary->remark = $request->remark;
        $teacher_salary->month = $month;
        $teacher_salary->year = $year;
       

        if($teacher->salary == ($request->paid_amount+$request->deduction))
        {
            $teacher_salary->status = 1;
        }
        else
        {
            $teacher_salary->status = 2;
        }
        $teacher_salary->school_id = auth()->user()->school_id;
        $teacher_salary->save();

        return response()->json(['success' => true]);

    }

    public function chargeTeacherSalaries(Request $request){
      // dd($request->all());
      $ids = explode(',',$request->ids);
      $teachers = Teacher::whereIn('id',$ids)->get();
      foreach ($teachers as $teacher) {
        $total_amount_bonus = null;

        $month = carbon::parse($request->salary_month)->format('m');
        $year = carbon::parse($request->salary_month)->format('Y');
        $check_month = TeacherSalary::where('teacher_id',$teacher->id)->where('month',$month)->where('year',$year)->first();

        if($check_month == null)
        {
          $teacher_salary = new TeacherSalary;
          $teacher_salary->teacher_id = $teacher->id;
          $teacher_salary->total_amount = $teacher->salary;
          $teacher_salary->paid_amount = 0;
          $teacher_salary->deduction = 0;
          $teacher_salary->reason = null;
          $teacher_salary->unpaid_amount = null;
          $teacher_salary->bonus = null;
          $teacher_salary->bonus_description = null;
          $teacher_salary->bonus_amount = null;
          $teacher_salary->salary_month = $request->salary_month;
          $teacher_salary->submitted_date = null;
          $teacher_salary->remark = null;
          $teacher_salary->month = $month;
          $teacher_salary->year = $year;
          $teacher_salary->status = 2;
          $teacher_salary->school_id = auth()->user()->school_id;
          $teacher_salary->save();
        }
      }

        return response()->json(['success' => true]);
    }

    public function getTeachersSalaries(Request $request)
    {
      if($request->teacher_id != null)
      {
        $query = TeacherSalary::where('teacher_id',$request->teacher_id);
      }
      else
      {
        $query = TeacherSalary::where('school_id', auth()->user()->school_id);
      }

      if($request->year != null)
      {
        $query = $query->where('year',$request->year);
      }

      if($request->from_date !== null)
      {
        $query->whereDate('salary_month','>=',$request->from_date);
      }

      if($request->to_date !== null)
      {
        $query->whereDate('salary_month','<=',$request->to_date);
      }
      if($request->from_payment_date !== null)
      {
          $query->whereDate('submitted_date','>=',$request->from_payment_date);
      }

      if($request->to_payment_date !== null)
      {
          $query->whereDate('submitted_date','<=',$request->to_payment_date);
      }

      if($request->status !== null && $request->status != ""){
        $query->where('status',$request->status);
      }
      // $query->latest();
      $total_amount = (clone $query)->sum('total_amount');
      $deduction = (clone $query)->sum('deduction');
      $bonus_amount = (clone $query)->sum('bonus_amount');
      $paid_amount = (clone $query)->sum('paid_amount');
      $unpaid_amount = (clone $query)->sum('unpaid_amount');
        return Datatables::of($query)
        ->addColumn('checkbox', function ($item) {
            $html_string = '<div class="custom-control custom-checkbox custom-checkbox1 d-inline-block">
                            <input type="checkbox" class="custom-control-input check" value="'.$item->id.'" id="stone_check_'.$item->id.'">
                            <label class="custom-control-label" for="stone_check_'.$item->id.'"></label>
                        </div>';
            return $html_string;
        })
        ->addColumn('action', function ($item) {

            // $html_string = '<div class="custom-control custom-checkbox custom-checkbox1 d-inline-block">
            //                 <input type="checkbox" class="custom-control-input check" value="'.$item->id.'" id="stone_check_'.$item->id.'">
            //                 <label class="custom-control-label" for="stone_check_'.$item->id.'"></label>
            //             </div>';

             $html_string = '
                 <a href="'.url('user/teacher/'.$item->teacher_id.'').'" class="actionicon viewIcon delete_order_transaction" data-id="' . $item->id . '" title="Delete"><i class="fa fa-eye"></i></a>
                 ';
            $html_string .= '
                 <a href="javascript:void(0)" class="actionicon deleteIcon delete_student_permanant" data-id="' . $item->id . '" title="Delete Salary"><i class="fa fa-trash-alt"></i></a>
                 ';
            return $html_string;
        })
         ->addColumn('name', function ($item){
            return $item->teacher !== null ? $item->teacher->name : 'N.A';
        })

        ->filterColumn('name', function( $query, $keyword ) {
            $query->whereHas('teacher', function($q) use($keyword){
            $q->where('name','LIKE', "%$keyword%");                    
        });
            })

         ->addColumn('phone', function ($item){
            return $item->teacher !== null ? $item->teacher->phone : 'N.A';
        })

         ->addColumn('salary', function ($item){
            return $item->total_amount !== null ? number_format(floatval($item->total_amount),2,'.',',') : 'N.A';
        })

          ->addColumn('total_salary', function ($item){
            $num = $item->total_amount + $item->bonus_amount;
            return $num != null ? number_format(floatval($num),2,'.',',') : 0;
        })

         ->addColumn('paid_amount', function ($item) use ($request){
            $title = $item->paid_amount !== null ? number_format(floatval($item->paid_amount),2,'.',',') : 'N.A';
            if($request->page == 'single-teacher')
              return $title;
            $html_string = '
                 <span class="m-l-15 inputDoubleClick font-weight-bold" id="title"  data-fieldvalue="'.$item->paid_amount.'">'.$title.'</span>
                <input type="text" autocomplete="nope" name="paid_amount" class="fieldFocus d-none form-control" value="'.$item->paid_amount.'" data-id="'.$item->id.'">
            ';

            return $html_string;
        })

          ->addColumn('deduction', function ($item) use ($request){
            $title = $item->deduction !== null ? number_format(floatval($item->deduction),2,'.',',') : 'N.A';
            if($request->page == 'single-teacher')
              return $title;
            $html_string = '
                 <span class="m-l-15 inputDoubleClick font-weight-bold" id="title"  data-fieldvalue="'.$item->deduction.'">'.$title.'</span>
                <input type="text" autocomplete="nope" name="deduction" class="fieldFocus d-none form-control" value="'.$item->deduction.'" data-id="'.$item->id.'">
            ';

            return $html_string;
        })

           ->addColumn('reason', function ($item) use ($request){
            $title = $item->reason !== null ? $item->reason : '--';
            if($request->page == 'single-teacher')
              return $title;
            $html_string = '
                 <span class="m-l-15 inputDoubleClick font-weight-bold" id="title"  data-fieldvalue="'.$item->reason.'">'.$title.'</span>
                <input type="text" autocomplete="nope" name="reason" class="fieldFocus d-none form-control" value="'.$item->reason.'" data-id="'.$item->id.'">
            ';

            return $html_string;
        })

        ->addColumn('unpaid_amount', function ($item){
            return $item->unpaid_amount !== null ? number_format(floatval($item->unpaid_amount),2,'.',',') : 'N.A';
        })
        ->addColumn('total_payable_amount', function ($item){
          $t = floatval($item->total_amount) - floatval($item->deduction) + floatval($item->bonus_amount);
            return $t;
        })

        ->addColumn('bonus', function ($item){
            return $item->bonus !== null ? $item->bonus.' %' : 'N.A';
        })
        ->addColumn('bonus_description', function ($item) use ($request){
            $title = $item->bonus_description !== null ? $item->bonus_description : '--';
            if($request->page == 'single-teacher')
              return $title;
            $html_string = '
                 <span class="m-l-15 inputDoubleClick font-weight-bold" id="title"  data-fieldvalue="'.$item->bonus_description.'">'.$title.'</span>
                <input type="text" autocomplete="nope" name="bonus_description" class="fieldFocus d-none form-control" value="'.$item->bonus_description.'" data-id="'.$item->id.'">
            ';

            return $html_string;
        })

        ->addColumn('bonus_amount', function ($item) use ($request){
            $title = $item->bonus_amount !== null ? number_format(floatval($item->bonus_amount),2,'.',',') : '--';
            if($request->page == 'single-teacher')
              return $title;
            $html_string = '
                 <span class="m-l-15 inputDoubleClick font-weight-bold" id="title"  data-fieldvalue="'.$item->bonus_amount.'">'.$title.'</span>
                <input type="text" autocomplete="nope" name="bonus_amount" class="fieldFocus d-none form-control" value="'.$item->bonus_amount.'" data-id="'.$item->id.'">
            ';

            return $html_string;
        })

        ->addColumn('salary_month', function ($item){
            return $item->salary_month !== null ? $item->salary_month : 'N.A';
        })

        ->addColumn('submitted_date', function ($item) use ($request){
            $title = $item->submitted_date !== null ? carbon::parse($item->submitted_date)->format('Y-m-d') : '--';
            if($request->page == 'single-teacher')
              return $title;
            $html_string = '
                 <span class="m-l-15 inputDoubleClick font-weight-bold" id="title"  data-fieldvalue="'.$item->submitted_date.'">'.$title.'</span>
                <input type="date" autocomplete="nope" name="submitted_date" class="fieldFocus d-none form-control" value="'.$item->submitted_date.'" data-id="'.$item->id.'">
            ';

            return $html_string;
        })

        ->addColumn('remarks', function ($item){
            return $item->remark != null ? $item->remark : '--';
        })

        ->addColumn('status', function ($item){
            return $item->status == 1 ? 'Paid' : 'Partial Paid';
        })

        ->rawColumns(['action','name','phone','salary','paid_amount','unpaid_amount','bonus','bonus_amount','salary_month','submitted_date','status','checkbox','remarks','deduction','reason','bonus_description','total_payable_amount'])
        ->with(['total_amount' => $total_amount,'paid_amount' => $paid_amount,'deduction' => $deduction,'bonus_amount' => $bonus_amount, 'unpaid_amount' => $unpaid_amount])
        ->make(true);

    }

    public function checkSameTeacher(Request $request)
    {
        // dd($request->all());

        $salary = explode(',',$request->selected_salary);
        $teacher_salary = TeacherSalary::whereIn('id',$salary)->distinct('teacher_id')->count('teacher_id');

        return response()->json(['teacher_salary' => $teacher_salary]);

    }

    public function printSalaryInvoices(Request $request, $salary)
    {
        $salary = explode(',',$salary);
        $salaries = TeacherSalary::whereIn('id',$salary)->get();

        if($salaries->count() == 1)
        {
            // dd($student);

            $pdf = PDF::loadView('user.invoices.single-salary-invoice',compact('salaries'));
            $customPaper = array(0,0,360,320);
            $pdf->setPaper($customPaper);
            // $pdf->setPaper('A4', 'portrait');
            $pdf->getDomPDF()->set_option("enable_php", true);
              // making pdf name starts
            $makePdfName='invoice';
            return $pdf->stream(
               $makePdfName.'.pdf',
                array(
                  'Attachment' => 0
                )
              );
        }
        else
        {
            // dd($student);

            $pdf = PDF::loadView('user.invoices.salary-invoice',compact('salaries'));
            // $customPaper = array(0,0,360,360);
            // $dompdf->set_paper($customPaper);
            $pdf->setPaper('A4', 'portrait');
            $pdf->getDomPDF()->set_option("enable_php", true);
              // making pdf name starts
            $makePdfName='invoice';
            return $pdf->stream(
               $makePdfName.'.pdf',
                array(
                  'Attachment' => 0
                )
              );
        }
    }

    public function teachersAllAttendance()
    {
        return view('user.attendance.teachers-all-attendance');
    }

    public function updateTeacherData(Request $request)
    {
        // dd($request->all());
        $old_value = $request->new_select_value; 
   
        $teacher = Teacher::where('id',$request->id)->first();

        $show_title = $request->new_select_value;
        foreach($request->except('id','new_select_value') as $key => $value)
        {
            if($value == 'null' || $value == null)
              $teacher->$key = null;
            else
              $teacher->$key = $value;
        }

        $teacher->save();

        return response()->json(['success' => true]);
    }

    public function deleteTeacher(Request $request)
    {
        $teacher = Teacher::find($request->id);
        if($teacher)
        {
            $check_attendance = TeacherAttendance::where('teacher_id',$teacher->id)->first();
            $check_salary = TeacherSalary::where('teacher_id',$teacher->id)->first();

            if($check_attendance == null && $check_salary == null)
            {
                $teacher->delete();
                return response()->json(['success' => true]);
            }
            else
            {
                return response()->json(['success' => false]);
            }
        }
        else
        {
            return response()->json(['error' => true]);
        }
    }

    public function deleteTeacherSalary(Request $request)
    {
        $salary = TeacherSalary::find($request->id);

        if($salary)
        {
            $salary->delete();
            return response()->json(['success' => true]);
        }
        else
        {
            return response()->json(['success' => false]);
        }
    }

    public function editTeacherData(Request $request)
    {
        $old_value = $request->new_select_value; 
   
        $student = TeacherSalary::where('id',$request->id)->first();

        $show_title = $request->new_select_value;
        foreach($request->except('id','new_select_value') as $key => $value)
        {
            $student->$key = $value;
            $student->save();
                
            $student->unpaid_amount = $student->total_amount - $student->paid_amount - $student->deduction + $student->bonus_amount;
            $student->save();

            if($student->unpaid_amount == 0 || $student->unpaid_amount < 0)
            {
                $student->status = 1;
            }
            else
            {
                $student->status = 0;
            }
            // $student->submitted_date = carbon::now()->format('Y-m-d');
            $student->save();
        }
        return response()->json(['success' => true]);
    }

    public function updateProfileImgTeacher(Request $request){
        // dd($request->all());
        $teacher = Teacher::find($request->teacher_id);
        if($teacher != null){
            if($teacher->image != null)
            {
                $img_path = public_path('uploads/teachers/images/'.$teacher->image);
                if(File::exists($img_path)) {
                    File::delete($img_path);
                }
            }

            if($request->hasFile('profileimg') && $request->profileimg->isValid())
            {
              $fileNameWithExt = $request->file('profileimg')->getClientOriginalName();
              $fileName = pathinfo($fileNameWithExt,PATHINFO_FILENAME);
              $extension = $request->file('profileimg')->getClientOriginalExtension();
              $fileNameToStore = $fileName.'_'.time().'.'.$extension;
              $path = $request->file('profileimg')->move('public/uploads/teachers/images/',$fileNameToStore);
              $teacher->image = $fileNameToStore;
            }
            $teacher->save();
            return response()->json(['success' => true]);
        }

        return response()->json(['success' => false]);
        
    }
    public function teacherPaySlip($fees){
      $ids = explode(',', $fees);
      $teachers = TeacherSalary::whereIn('id',$ids)->with('teacher')->get();

      ini_set('memory_limit', '-1');
      $pdf = PDF::loadView('teacher.pay-slip',compact('teachers'));
      $pdf->getDomPDF()->set_option("enable_php", true);
        // making pdf name starts
      $makePdfName='invoice';
      return $pdf->stream(
         $makePdfName.'.pdf',
          array(
            'Attachment' => 0
          )
        );
    }
}
