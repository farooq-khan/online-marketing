<?php

namespace App\Http\Controllers;
use App\Attendance;
use App\GetMonth;
use App\Student;
use App\StudentClass;
use App\Teacher;
use App\TeacherAttendance;
use Carbon\Carbon;
use Illuminate\Http\Request;
use PDF;
use Yajra\Datatables\Datatables;

class AttendanceController extends Controller
{
    public function allAttendance()
    {
    	$classes = StudentClass::where('school_id', auth()->user()->school_id)->orderBy('class_name','asc')->get();
        return view('user.attendance.all-attendance',compact('classes'));
    }

    public function studentAttendence()
    {
        $classes = StudentClass::where('school_id', auth()->user()->school_id)->orderBy('class_name','asc')->get();
        return view('user.attendance.student-attendence-form',compact('classes'));
    }

    public function underConstruction()
    {
        return view('common.under-construction');
    }

    public function submitAttendance($class_id, $attendance_date, $option = null)
    {
    	$students = Student::where('class_id',$class_id)->where('status',1)->get();

    	$class = StudentClass::find($class_id);

        $attendance = Attendance::where('class_id',$class_id)->whereDate('attendance_date',$attendance_date)->first();

        if($attendance != null && $option != null)
        {
            return redirect()->back()->with('attendance_exists','yes');
        }

    	return view('user.attendance.submit-attendance',compact('students','attendance_date','class_id','class'));
    }

    public function singleStudentAttendance($id,$class_id = null)
    {
        $student = Student::find($id);
        $students = Student::where('school_id', auth()->user()->school_id)->where('status',1)->where('id','!=',$id)->get();
        $classes = StudentClass::where('school_id', auth()->user()->school_id)->orderBy('class_name','asc')->get();

        return view('user.attendance.single-student-attendance',compact('student','students','classes','class_id'));
    }

    public function getStudentForAttendance(Request $request)
    {
        $att = Attendance::where('class_id',$request->class_id)->whereDate('attendance_date',$request->attendance_date)->get();
        if($att->count() > 0)
        {
            // $students = Student::with('student_attendance')->whereHas('student_attendance',function($z)use($request){
            //     $z->where('class_id',$request->class_id);
            // });
            $students = Student::with('student_attendance')->where('class_id',$request->class_id)->where('status',1);
        }
        else
        {
            $students = Student::with('student_attendance')->where('class_id',$request->class_id)->where('status',1);
        }
        $ids = (clone $students)->pluck('id')->toArray();
        $total_present = Attendance::where('class_id',$request->class_id)->whereIn('student_id',$ids)->whereDate('attendance_date',$request->attendance_date)->where('status','present')->count();

        $total_absent = Attendance::where('class_id',$request->class_id)->whereIn('student_id',$ids)->whereDate('attendance_date',$request->attendance_date)->where('status','absent')->count();

        $total_leave = Attendance::where('class_id',$request->class_id)->whereIn('student_id',$ids)->whereDate('attendance_date',$request->attendance_date)->where('status','leave')->count();
    	      // $query->latest();
        return Datatables::of($students)
        ->addColumn('action', function ($item) use($request) {
             $html_string = '
                 <a href="'.url('user/student-attendance/'.$item->id.'/'.$request->class_id.'').'" class="actionicon viewIcon delete_order_transaction" data-id="' . $item->id . '" title="Delete"><i class="fa fa-eye"></i></a>
                 ';
            return $html_string;
        })
         ->editColumn('name', function ($item){
            return $item->name !== null ? $item->name : 'N.A';
        })

         ->addColumn('guardian', function ($item){
            return $item->guardian !== null ? $item->guardian.' '.@$item->guardian_nickname : 'N.A';
        })
         ->filterColumn('guardian', function( $query, $keyword ) {
         $query->where(function($q) use ($keyword){
            $q->where('guardian','LIKE', "%$keyword%")->orWhere('guardian_nickname','LIKE', "%$keyword%");
         });
        })

        ->editColumn('roll_no', function ($item){
            return $item->roll_no !== null ? $item->roll_no : 'N.A';
        })

        ->addColumn('present',function($item) use($request){
        	$checked = '';
        	$check = $item->student_attendance()->where('class_id',$request->class_id)->whereDate('attendance_date',$request->attendance_date)->pluck('status')->first();
        	if($check == 'present')
        	{
        		$checked = 'checked';
        	}
        	$html = '
                <div class="custom-control custom-radio custom-control-inline pull-right">';
                $html .= '<input type="radio" class="condition custom-control-input" id="present_radio_'.@$item->id.'" name="attendance_radio_'.$item->id.'" data-id="'.$item->id.'" data-title="present" value="" '.$checked.'>';
                
                $html .='<label class="custom-control-label" for="present_radio_'.@$item->id.'"></label></div>';
        	return $html;
        })

        ->addColumn('absent',function($item) use($request){
        	$checked = '';
        	$check = $item->student_attendance()->where('class_id',$request->class_id)->whereDate('attendance_date',$request->attendance_date)->pluck('status')->first();
        	if($check == 'absent')
        	{
        		$checked = 'checked';
        	}
        	$html = '
                <div class="custom-control custom-radio custom-control-inline pull-right">';
                $html .= '<input type="radio" class="condition custom-control-input" id="absent_radio_'.@$item->id.'" name="attendance_radio_'.$item->id.'" data-id="'.$item->id.'" data-title="absent" '.$checked.' value="">';
                
                $html .='<label class="custom-control-label" for="absent_radio_'.@$item->id.'"></label></div>';
        	return $html;
        })

        ->addColumn('leave',function($item) use($request){
        	$checked = '';
        	$check = $item->student_attendance()->where('class_id',$request->class_id)->whereDate('attendance_date',$request->attendance_date)->pluck('status')->first();
        	if($check == 'leave')
        	{
        		$checked = 'checked';
        	}
        	$html = '
                <div class="custom-control custom-radio custom-control-inline pull-right">';
                $html .= '<input type="radio" class="condition custom-control-input" id="leave_radio_'.@$item->id.'" name="attendance_radio_'.$item->id.'" data-id="'.$item->id.'" data-title="leave" '.$checked.' value="">';
                
                $html .='<label class="custom-control-label" for="leave_radio_'.@$item->id.'"></label></div>';
        	return $html;
        })

        ->addColumn('timing',function($item) use($request){
            $checked = '';
            $check = $item->student_attendance()->where('class_id',$request->class_id)->whereDate('attendance_date',$request->attendance_date)->pluck('timing')->first();
            if($check == 'early')
            {
                $checked = 'checked';
            }
            $html = '
                <div class="custom-control custom-radio custom-control-inline pull-right">';
                $html .= '<input type="radio" class="condition custom-control-input" id="early_radio_'.@$item->id.'" name="attendance_timing_radio_'.$item->id.'" data-id="'.$item->id.'" data-title="early" value="" '.$checked.'>';
                
                $html .='<label class="custom-control-label" for="early_radio_'.@$item->id.'">Early</label></div>';

            $checked = '';
            $check = $item->student_attendance()->where('class_id',$request->class_id)->whereDate('attendance_date',$request->attendance_date)->pluck('timing')->first();
            if($check == 'late')
            {
                $checked = 'checked';
            }
            $html .= '
                <div class="custom-control custom-radio custom-control-inline pull-right">';
                $html .= '<input type="radio" class="condition custom-control-input" id="late_radio_'.@$item->id.'" name="attendance_timing_radio_'.$item->id.'" data-id="'.$item->id.'" data-title="late" value="" '.$checked.'>';
                
                $html .='<label class="custom-control-label" for="late_radio_'.@$item->id.'">Late</label></div>';
            return $html;
        })

        ->addColumn('uniform',function($item) use($request){
            $checked = '';
            $check = $item->student_attendance()->where('class_id',$request->class_id)->whereDate('attendance_date',$request->attendance_date)->pluck('uniform')->first();
            if($check == 'uniform')
            {
                $checked = 'checked';
            }
            $html = '
                <div class="custom-control custom-radio custom-control-inline pull-right">';
                $html .= '<input type="radio" class="condition custom-control-input" id="uniform_radio_'.@$item->id.'" name="attendance_uniform_radio_'.$item->id.'" data-id="'.$item->id.'" data-title="uniform" value="" '.$checked.'>';
                
                $html .='<label class="custom-control-label" for="uniform_radio_'.@$item->id.'">Uniform</label></div>';

            $checked = '';
            $check = $item->student_attendance()->where('class_id',$request->class_id)->whereDate('attendance_date',$request->attendance_date)->pluck('uniform')->first();
            if($check == 'without_uniform')
            {
                $checked = 'checked';
            }
            $html .= '
                <div class="custom-control custom-radio custom-control-inline pull-right">';
                $html .= '<input type="radio" class="condition custom-control-input" id="without_uniform_radio_'.@$item->id.'" name="attendance_uniform_radio_'.$item->id.'" data-id="'.$item->id.'" data-title="without_uniform" value="" '.$checked.'>';
                
                $html .='<label class="custom-control-label" for="without_uniform_radio_'.@$item->id.'">Without Uniform</label></div>';
            return $html;
        })
        
        ->rawColumns(['action','name','roll_no','guardian','present','absent','leave','timing','uniform'])
        ->with(['total_present' => $total_present,'total_absent' => $total_absent, 'total_leave' => $total_leave])
        ->make(true);
    }

    public function updateStudentsAttendance(Request $request)
    {

      // dd($request->all());
        if($request->title == 'late' || $request->title == 'early')
        {
            $key = 'timing';
        }
        else if($request->title == 'uniform' || $request->title == 'without_uniform')
        {
            $key = 'uniform';
        }
        else
        {
            $key = 'status';
        }
        // dd($key);
        $attendance = Attendance::where('student_id',$request->id)->where('class_id',$request->class_id)->whereDate('attendance_date',$request->attendance_date)->first();
        if($attendance == null)
        {
            $new_attendance = new Attendance;

            $new_attendance->student_id = $request->id;
            $new_attendance->class_id = $request->class_id;
            $new_attendance->school_id = auth()->user()->school_id;
            $new_attendance->$key = $request->title;
            $new_attendance->attendance_date = $request->attendance_date;

            $new_attendance->save();
        }
        else
        {
            $attendance->$key = $request->title;
            $attendance->save();
        }

      

      return response()->json(['success' => true]);
    }

    public function markAllStudentPresent(Request $request){
        $students = Student::where('class_id',$request->class_id)->where('status',1)->get();
        if($students){
            foreach ($students as $std) {
                $attendance = Attendance::where('student_id',$std->id)->where('class_id',$request->class_id)->whereDate('attendance_date',$request->date)->first();
                if($attendance){
                    if($request->type == 'present')
                        $attendance->status = 'present';
                    elseif($request->type == 'early')
                        $attendance->timing = 'early';
                    elseif($request->type == 'uniform')
                        $attendance->uniform = 'uniform';
                    $attendance->save();
                }else{
                    $new_attendance = new Attendance;

                    $new_attendance->student_id = $std->id;
                    $new_attendance->class_id = $request->class_id;
                    $new_attendance->school_id = auth()->user()->school_id;
                    if($request->type == 'present')
                        $new_attendance->status = 'present';
                    elseif($request->type == 'early')
                        $new_attendance->timing = 'early';
                    elseif($request->type == 'uniform')
                        $new_attendance->uniform = 'uniform';
                    $new_attendance->attendance_date = $request->date;
                    $new_attendance->save();
                }
            }

            return response()->json(['success' => true]);
        }
        return response()->json(['success' => false]);
    }

    public function getStudentAllAttendance(Request $request)
    {
    	$date = $request->attendance_date;
    	$class_id = $request->class_id;


    	$attendance = Attendance::where('school_id', auth()->user()->school_id)->orderBy('id','desc');

    	if($class_id != null)
    	{
    		$attendance = $attendance->where('class_id',$request->class_id);
    	}
        if($date != null)
        {
            $attendance = $attendance->whereDate('attendance_date',$date);
        }

    	$attendance = $attendance->groupBy('class_id','attendance_date');
    	      // $query->latest();
        // dd($attendance->toSql());
        return Datatables::of($attendance)
        ->addColumn('action', function ($item) {
             $html_string = '
                 <a href="'.url('user/submit-attendance/'.$item->class_id.'/'.$item->attendance_date.'').'" class="actionicon viewIcon delete_order_transaction" data-id="' . $item->id . '" title="Detail"><i class="fas fa-eye"></i></a>
                 ';
            $html_string .= '
                 <a href="javascript:void(0)" class="actionicon deleteIcon delete_student_permanant" data-id="' . $item->id . '" data-class="'.$item->class_id.'" data-att="'.$item->attendance_date.'" title="Delete Attendance"><i class="fas fa-trash-alt"></i></a>
                 ';
            return $html_string;
        })
         ->addColumn('class', function ($item){
            return $item->student_class !== null ? $item->student_class->class_name : 'N.A';
        })

        ->addColumn('present',function($item) use($date){
        	$present_count = Attendance::where('class_id',$item->class_id)->whereDate('attendance_date',$item->attendance_date)->where('status','present')->count();

        	return $present_count;
        })

        ->addColumn('absent',function($item) use($date){
        	$absent_count = Attendance::where('class_id',$item->class_id)->whereDate('attendance_date',$item->attendance_date)->where('status','absent')->count();

        	return $absent_count;
        })

        ->addColumn('leave',function($item) use($date){
        	$leave_count = Attendance::where('class_id',$item->class_id)->whereDate('attendance_date',$item->attendance_date)->where('status','leave')->count();

        	return $leave_count;
        })

        ->addColumn('late',function($item) use($date){
            $late = Attendance::where('class_id',$item->class_id)->whereDate('attendance_date',$item->attendance_date)->where('timing','late')->count();

            return $late;
        })

        ->addColumn('without_uniform',function($item) use($date){
            $without_uniform = Attendance::where('class_id',$item->class_id)->whereDate('attendance_date',$item->attendance_date)->where('uniform','without_uniform')->count();

            return $without_uniform;
        })

        ->addColumn('total',function($item) use($date){
        	$total_count = Attendance::where('class_id',$item->class_id)->whereDate('attendance_date',$item->attendance_date)->count();

        	return $total_count;
        })

         ->addColumn('date',function($item) use($date){
        	return carbon::parse($item->attendance_date)->format('d-m-Y');
        })

        ->rawColumns(['action','class','present','absent','leave','total','date','late','without_uniform'])
        ->make(true);
    }

    public function getSingleStudentAttendance(Request $request)
    {
    	$months = GetMonth::whereNotNull('id');
        $id = auth()->user()->role == 'student' ? auth()->user()->student_id : $request->student_id;
    	$student = Student::find($id);

    	$month_total = Attendance::where('student_id',$student->id)->whereYear('attendance_date',$request->year)->count();
    	$month_total_present = Attendance::where('student_id',$student->id)->whereYear('attendance_date',$request->year)->where('status','present')->count();
    	$month_total_absent = Attendance::where('student_id',$student->id)->whereYear('attendance_date',$request->year)->where('status','absent')->count();
    	$month_total_leave = Attendance::where('student_id',$student->id)->whereYear('attendance_date',$request->year)->where('status','leave')->count();

        $month_total_late = Attendance::where('student_id',$student->id)->whereYear('attendance_date',$request->year)->where('timing','late')->count();
        $month_total_without_uniform = Attendance::where('student_id',$student->id)->whereYear('attendance_date',$request->year)->where('uniform','without_uniform')->count();
    	// dd($month_total);
    	$dt = Datatables::of($months);

        $dt->addColumn('checkbox', function ($item) {
        	return '--';
        });

        $dt->addColumn('month', function ($item) {
        	return $item->name;
        });

        for ($i=1; $i < 32; $i++) { 
        	$dt->addColumn('month_'.$i, function ($item) use($i,$student,$request) {
        		$value = str_pad($i,2,"0",STR_PAD_LEFT);
        		$month = str_pad($item->id,2,"0",STR_PAD_LEFT);
        		$attendance = Attendance::where('student_id',$student->id)->whereDay('attendance_date',$value)->whereMonth('attendance_date',$month)->whereYear('attendance_date',$request->year)->pluck('status')->first();
        		$color = '';
        		if($attendance == 'leave')
        		{
        			$color = 'yellow';
        		}

        		if($attendance == 'present')
        		{
        			$color = 'green';
        		}

        		if($attendance == 'absent')
        		{
        			$color = 'red';
        		}
        		$html = '<div style="width:20px;height:20px;background-color: '.$color.';border-radius:50%;"></div>';
        		return $attendance != null ? $html : '--';
        	});
        }

        $dt->addColumn('present_total', function ($item) use($student,$request){
        	$month = str_pad($item->id,2,"0",STR_PAD_LEFT);
        		$attendance_count = Attendance::where('student_id',$student->id)->whereMonth('attendance_date',$month)->whereYear('attendance_date',$request->year)->where('status','present')->count();
        	return $attendance_count;
        });

        $dt->addColumn('absent_total', function ($item) use($student,$request){
        	$month = str_pad($item->id,2,"0",STR_PAD_LEFT);
        		$attendance_count = Attendance::where('student_id',$student->id)->whereMonth('attendance_date',$month)->whereYear('attendance_date',$request->year)->where('status','absent')->count();
        	return $attendance_count;
        });

        $dt->addColumn('leave_total', function ($item) use($student,$request){
        	$month = str_pad($item->id,2,"0",STR_PAD_LEFT);
        		$attendance_count = Attendance::where('student_id',$student->id)->whereMonth('attendance_date',$month)->whereYear('attendance_date',$request->year)->where('status','leave')->count();
        	return $attendance_count;
        });

        $dt->addColumn('late', function ($item) use($student,$request){
            $month = str_pad($item->id,2,"0",STR_PAD_LEFT);
                $attendance_count = Attendance::where('student_id',$student->id)->whereMonth('attendance_date',$month)->whereYear('attendance_date',$request->year)->where('timing','late')->count();
            return $attendance_count;
        });

         $dt->addColumn('without_uniform', function ($item) use($student,$request){
            $month = str_pad($item->id,2,"0",STR_PAD_LEFT);
                $attendance_count = Attendance::where('student_id',$student->id)->whereMonth('attendance_date',$month)->whereYear('attendance_date',$request->year)->where('uniform','without_uniform')->count();
            return $attendance_count;
        });

        $dt->addColumn('total', function ($item) use($student,$request){
        	$month = str_pad($item->id,2,"0",STR_PAD_LEFT);
        		$attendance_count = Attendance::where('student_id',$student->id)->whereMonth('attendance_date',$month)->whereYear('attendance_date',$request->year)->count();
        	return $attendance_count;
        });

        $dt->escapeColumns([]);
        $dt->rawColumns(['checkbox','month','present_total','absent_total','leave_total','total','late','without_uniform']);
        $dt->with(['month_total' => $month_total, 'month_total_present' => $month_total_present, 'month_total_absent' => $month_total_absent, 'month_total_leave' => $month_total_leave,'month_total_late' => $month_total_late, 'month_total_without_uniform' => $month_total_without_uniform]);
        return $dt->make(true);
    }

    public function getAttendanceInvoice(Request $request, $student_id, $year)
    {
        $student = Student::find($student_id);
        $months = GetMonth::all();

        $month_total = Attendance::where('student_id',$student->id)->where('class_id',$student->class_id)->whereYear('attendance_date',$year)->count();
        $month_total_present = Attendance::where('student_id',$student->id)->where('class_id',$student->class_id)->whereYear('attendance_date',$year)->where('status','present')->count();
        $month_total_absent = Attendance::where('student_id',$student->id)->where('class_id',$student->class_id)->whereYear('attendance_date',$year)->where('status','absent')->count();
        $month_total_leave = Attendance::where('student_id',$student->id)->where('class_id',$student->class_id)->whereYear('attendance_date',$year)->where('status','leave')->count();

        $pdf = PDF::loadView('user.invoices.attendance-invoice',compact('student','months','year','month_total','month_total_present','month_total_leave','month_total_absent'));
        // $customPaper = array(0,0,360,360);
        // $dompdf->set_paper($customPaper);
        $pdf->setPaper('A4', 'portrait');
        $pdf->getDomPDF()->set_option("enable_php", true);
          // making pdf name starts
        $makePdfName='invoice';
        return $pdf->stream(
           $makePdfName.'.pdf',
            array(
              'Attachment' => 0
            )
          );
    }

    public function submitTeacherAttendance($attendance_date, $option = null)
    {
        $teachers = Teacher::where('status',1)->get();
        $attendance = TeacherAttendance::whereDate('attendance_date',$attendance_date)->first();

        if($attendance != null && $option != null)
        {
            return redirect()->back()->with('attendance_exists','yes');
        }

        return view('user.attendance.submit-teachers-attendance',compact('teachers','attendance_date'));
    }
    public function markAllTeacherPresent(Request $request){
        // dd($request->all());
        $teachers = Teacher::where('status',1)->get();
        foreach ($teachers as $teacher) {
            $attendance = TeacherAttendance::whereDate('attendance_date',$request->date)->where('teacher_id',$teacher->id)->first();
            if($attendance){
                if($request->type == 'present')
                    $attendance->status = 'present';
                else
                    $attendance->timing = 'early';
                $attendance->save();
            }else{
                $new_attendance  = new TeacherAttendance;
                $new_attendance->teacher_id = $teacher->id;
                if($request->type == 'present')
                    $new_attendance->status = 'present';
                else
                    $new_attendance->timing = 'early';
                $new_attendance->attendance_date = $request->date;
                $new_attendance->school_id = auth()->user()->school_id;
                $new_attendance->save();
            }
        }
        return response()->json(['success' => true]);
    }

    public function getTeacherForAttendance(Request $request)
    {
        $students = Teacher::where('school_id', auth()->user()->school_id)->with('teacher_attendance')->where('status',1);

        $ids = (clone $students)->pluck('id')->toArray();

        $total_present = TeacherAttendance::whereIn('teacher_id',$ids)->whereDate('attendance_date',$request->attendance_date)->where('status','present')->count();

        $total_absent = TeacherAttendance::whereIn('teacher_id',$ids)->whereDate('attendance_date',$request->attendance_date)->where('status','absent')->count();

        $total_leave = TeacherAttendance::whereIn('teacher_id',$ids)->whereDate('attendance_date',$request->attendance_date)->where('status','leave')->count();

              // $query->latest();
        return Datatables::of($students)
        ->addColumn('action', function ($item) {
             $html_string = '
                 <a href="'.url('user/teacher-attendance/'.$item->id.'').'" class="actionicon viewIcon delete_order_transaction" data-id="' . $item->id . '" title="Delete"><i class="fa fa-eye"></i></a>
                 ';
            return $html_string;
        })
         ->addColumn('name', function ($item){
            return $item->name !== null ? $item->name : 'N.A';
        })
         ->filterColumn('guardian', function( $query, $keyword ) {
         $query->where(function($q) use ($keyword){
            $q->where('guardian','LIKE', "%$keyword%")->orWhere('guardian_nickname','LIKE', "%$keyword%");
         });
        })

         ->editColumn('guardian', function ($item){
            return $item->guardian !== null ? $item->guardian.' '.@$item->guardian_nickname : 'N.A';
        })


        ->addColumn('present',function($item) use($request){
            $checked = '';
            $check = $item->teacher_attendance()->whereDate('attendance_date',$request->attendance_date)->pluck('status')->first();
            if($check == 'present')
            {
                $checked = 'checked';
            }
            $html = '
                <div class="custom-control custom-radio custom-control-inline pull-right">';
                $html .= '<input type="radio" class="condition custom-control-input" id="present_radio_'.@$item->id.'" name="attendance_radio_'.$item->id.'" data-id="'.$item->id.'" data-title="present" value="" '.$checked.'>';
                
                $html .='<label class="custom-control-label" for="present_radio_'.@$item->id.'"></label></div>';
            return $html;
        })

        ->addColumn('absent',function($item) use($request){
            $checked = '';
            $check = $item->teacher_attendance()->whereDate('attendance_date',$request->attendance_date)->pluck('status')->first();
            if($check == 'absent')
            {
                $checked = 'checked';
            }
            $html = '
                <div class="custom-control custom-radio custom-control-inline pull-right">';
                $html .= '<input type="radio" class="condition custom-control-input" id="absent_radio_'.@$item->id.'" name="attendance_radio_'.$item->id.'" data-id="'.$item->id.'" data-title="absent" '.$checked.' value="">';
                
                $html .='<label class="custom-control-label" for="absent_radio_'.@$item->id.'"></label></div>';
            return $html;
        })

        ->addColumn('leave',function($item) use($request){
            $checked = '';
            $check = $item->teacher_attendance()->whereDate('attendance_date',$request->attendance_date)->pluck('status')->first();
            if($check == 'leave')
            {
                $checked = 'checked';
            }
            $html = '
                <div class="custom-control custom-radio custom-control-inline pull-right">';
                $html .= '<input type="radio" class="condition custom-control-input" id="leave_radio_'.@$item->id.'" name="attendance_radio_'.$item->id.'" data-id="'.$item->id.'" data-title="leave" '.$checked.' value="">';
                
                $html .='<label class="custom-control-label" for="leave_radio_'.@$item->id.'"></label></div>';
            return $html;
        })

        ->addColumn('timing',function($item) use($request){
            $checked = '';
            $check = $item->teacher_attendance()->whereDate('attendance_date',$request->attendance_date)->pluck('timing')->first();
            if($check == 'early')
            {
                $checked = 'checked';
            }
            $html = '
                <div class="custom-control custom-radio custom-control-inline pull-right">';
                $html .= '<input type="radio" class="condition custom-control-input" id="early_radio_'.@$item->id.'" name="attendance_timing_radio_'.$item->id.'" data-id="'.$item->id.'" data-title="early" value="" '.$checked.'>';
                
                $html .='<label class="custom-control-label" for="early_radio_'.@$item->id.'">Early</label></div>';

            $checked = '';
            $check = $item->teacher_attendance()->whereDate('attendance_date',$request->attendance_date)->pluck('timing')->first();
            if($check == 'late')
            {
                $checked = 'checked';
            }
            $html .= '
                <div class="custom-control custom-radio custom-control-inline pull-right">';
                $html .= '<input type="radio" class="condition custom-control-input" id="late_radio_'.@$item->id.'" name="attendance_timing_radio_'.$item->id.'" data-id="'.$item->id.'" data-title="late" value="" '.$checked.'>';
                
                $html .='<label class="custom-control-label" for="late_radio_'.@$item->id.'">Late</label></div>';
            return $html;
        })

        ->addColumn('uniform',function($item) use($request){
            $checked = '';
            $check = $item->teacher_attendance()->whereDate('attendance_date',$request->attendance_date)->pluck('timing')->first();
            if($check == 'uniform')
            {
                $checked = 'checked';
            }
            $html = '
                <div class="custom-control custom-radio custom-control-inline pull-right">';
                $html .= '<input type="radio" class="condition custom-control-input" id="uniform_radio_'.@$item->id.'" name="attendance_uniform_radio_'.$item->id.'" data-id="'.$item->id.'" data-title="uniform" value="" '.$checked.'>';
                
                $html .='<label class="custom-control-label" for="uniform_radio_'.@$item->id.'">Uniform</label></div>';

            $checked = '';
            $check = $item->teacher_attendance()->whereDate('attendance_date',$request->attendance_date)->pluck('timing')->first();
            if($check == 'without_uniform')
            {
                $checked = 'checked';
            }
            $html .= '
                <div class="custom-control custom-radio custom-control-inline pull-right">';
                $html .= '<input type="radio" class="condition custom-control-input" id="without_uniform_radio_'.@$item->id.'" name="attendance_uniform_radio_'.$item->id.'" data-id="'.$item->id.'" data-title="without_uniform" value="" '.$checked.'>';
                
                $html .='<label class="custom-control-label" for="without_uniform_radio_'.@$item->id.'">Without Uniform</label></div>';
            return $html;
        })

        

        ->rawColumns(['action','name','guardian','present','absent','leave','timing','uniform'])
        ->with(['total_present' => $total_present,'total_absent' => $total_absent, 'total_leave' => $total_leave])
        ->make(true);
    }

    public function updateTeachersAttendance(Request $request)
    {

      // dd($request->all());
        if($request->title == 'late' || $request->title == 'early')
        {
            $key = 'timing';
        }
        else if($request->title == 'uniform' || $request->title == 'without_uniform')
        {
            $key = 'uniform';
        }
        else
        {
            $key = 'status';
        }
        $attendance = TeacherAttendance::where('teacher_id',$request->id)->whereDate('attendance_date',$request->attendance_date)->first();
        if($attendance == null)
        {
            $new_attendance = new TeacherAttendance;

            $new_attendance->teacher_id = $request->id;
            $new_attendance->$key = $request->title;
            $new_attendance->attendance_date = $request->attendance_date;
            $new_attendance->school_id = auth()->user()->school_id;

            $new_attendance->save();
        }
        else
        {
            $attendance->$key = $request->title;
            $attendance->save();
        }

      

      return response()->json(['success' => true]);
    }

    public function getTeacherAllAttendance(Request $request)
    {
        $date = $request->attendance_date;
        $attendance = TeacherAttendance::where('school_id', auth()->user()->school_id)->orderBy('id','desc');
        if($date != null)
        {
            $attendance->whereDate('attendance_date',$date);
        }

        $attendance = $attendance->groupBy('attendance_date');

              // $query->latest();
        return Datatables::of($attendance)
        ->addColumn('action', function ($item) {
             $html_string = '
                 <a href="'.url('user/submit-teacher-attendance/'.$item->attendance_date.'').'" class="actionicon viewIcon delete_order_transaction" data-id="' . $item->id . '" title="Detail"><i class="fa fa-eye"></i></a>
                 ';
                $html_string .= '
                 <a href="javascript:void(0)" class="actionicon deleteIcon delete_student_permanant" data-id="' . $item->id . '" data-att="' . $item->attendance_date . '" title="Delete Attendance"><i class="fa fa-trash-alt"></i></a>
                 ';
            return $html_string;
        })

        ->addColumn('present',function($item){
            $present_count = TeacherAttendance::whereDate('attendance_date',$item->attendance_date)->where('status','present')->count();

            return $present_count;
        })

        ->addColumn('absent',function($item){
            $absent_count = TeacherAttendance::whereDate('attendance_date',$item->attendance_date)->where('status','absent')->count();

            return $absent_count;
        })

        ->addColumn('leave',function($item){
            $leave_count = TeacherAttendance::whereDate('attendance_date',$item->attendance_date)->where('status','leave')->count();

            return $leave_count;
        })

        ->addColumn('total',function($item){
            $total_count = TeacherAttendance::whereDate('attendance_date',$item->attendance_date)->count();

            return $total_count;
        })

         ->addColumn('date',function($item) use($date){
            return $item->attendance_date != null ? $item->attendance_date : '--';
        })
         ->addColumn('empty_col',function($item){
            return '';
        })

        ->rawColumns(['action','present','absent','leave','total','date','empty_col'])
        ->make(true);
    }

    public function singleTeacherAttendance($id)
    {
        $teacher = Teacher::find($id);
        $teachers = Teacher::where('status',1)->where('id','!=',$id)->get();

        return view('user.attendance.single-teacher-attendance',compact('teacher','teachers'));
    }

    public function getSingleTeacherAttendance(Request $request)
    {
        $months = GetMonth::whereNotNull('id');
        $teacher = Teacher::find($request->teacher_id);

        $month_total = TeacherAttendance::where('teacher_id',$teacher->id)->whereYear('attendance_date',$request->year)->count();
        $month_total_present = TeacherAttendance::where('teacher_id',$teacher->id)->whereYear('attendance_date',$request->year)->where('status','present')->count();
        $month_total_absent = TeacherAttendance::where('teacher_id',$teacher->id)->whereYear('attendance_date',$request->year)->where('status','absent')->count();
        $month_total_leave = TeacherAttendance::where('teacher_id',$teacher->id)->whereYear('attendance_date',$request->year)->where('status','leave')->count();
        $month_total_late = TeacherAttendance::where('teacher_id',$teacher->id)->whereYear('attendance_date',$request->year)->where('timing','late')->count();
        // dd($month_total);
        $dt = Datatables::of($months);

        $dt->addColumn('checkbox', function ($item) {
            return '--';
        });

        $dt->addColumn('month', function ($item) {
            return $item->name;
        });

        for ($i=1; $i < 32; $i++) { 
            $dt->addColumn('month_'.$i, function ($item) use($i,$teacher,$request) {
                $value = str_pad($i,2,"0",STR_PAD_LEFT);
                $month = str_pad($item->id,2,"0",STR_PAD_LEFT);
                $attendance = TeacherAttendance::where('teacher_id',$teacher->id)->whereDay('attendance_date',$value)->whereMonth('attendance_date',$month)->whereYear('attendance_date',$request->year)->pluck('status')->first();
                $color = '';
                if($attendance == 'leave')
                {
                    $color = 'yellow';
                }

                if($attendance == 'present')
                {
                    $color = 'green';
                }

                if($attendance == 'absent')
                {
                    $color = 'red';
                }
                $html = '<div style="width:20px;height:20px;background-color: '.$color.';border-radius:50%;"></div>';
                return $attendance != null ? $html : '--';
            });
        }

        $dt->addColumn('present_total', function ($item) use($teacher,$request){
            $month = str_pad($item->id,2,"0",STR_PAD_LEFT);
                $attendance_count = TeacherAttendance::where('teacher_id',$teacher->id)->whereMonth('attendance_date',$month)->whereYear('attendance_date',$request->year)->where('status','present')->count();
            return $attendance_count;
        });

        $dt->addColumn('absent_total', function ($item) use($teacher,$request){
            $month = str_pad($item->id,2,"0",STR_PAD_LEFT);
                $attendance_count = TeacherAttendance::where('teacher_id',$teacher->id)->whereMonth('attendance_date',$month)->whereYear('attendance_date',$request->year)->where('status','absent')->count();
            return $attendance_count;
        });

        $dt->addColumn('leave_total', function ($item) use($teacher,$request){
            $month = str_pad($item->id,2,"0",STR_PAD_LEFT);
                $attendance_count = TeacherAttendance::where('teacher_id',$teacher->id)->whereMonth('attendance_date',$month)->whereYear('attendance_date',$request->year)->where('status','leave')->count();
            return $attendance_count;
        });

        $dt->addColumn('late', function ($item) use($teacher,$request){
            $month = str_pad($item->id,2,"0",STR_PAD_LEFT);
                $attendance_count = TeacherAttendance::where('teacher_id',$teacher->id)->whereMonth('attendance_date',$month)->whereYear('attendance_date',$request->year)->where('timing','late')->count();
            return $attendance_count;
        });

        $dt->addColumn('total', function ($item) use($teacher,$request){
            $month = str_pad($item->id,2,"0",STR_PAD_LEFT);
                $attendance_count = TeacherAttendance::where('teacher_id',$teacher->id)->whereMonth('attendance_date',$month)->whereYear('attendance_date',$request->year)->count();
            return $attendance_count;
        });

        $dt->escapeColumns([]);
        $dt->rawColumns(['checkbox','month','present_total','absent_total','leave_total','total','late']);
        $dt->with(['month_total' => $month_total, 'month_total_present' => $month_total_present, 'month_total_absent' => $month_total_absent, 'month_total_leave' => $month_total_leave,'month_total_late' => $month_total_late]);
        return $dt->make(true);
    }

    public function getTeacherAttendanceInvoice(Request $request, $teacher_id, $year)
    {
        $teacher = Teacher::find($teacher_id);
        $months = GetMonth::all();

        $month_total = TeacherAttendance::where('teacher_id',$teacher->id)->whereYear('attendance_date',$year)->count();
        $month_total_present = TeacherAttendance::where('teacher_id',$teacher->id)->whereYear('attendance_date',$year)->where('status','present')->count();
        $month_total_absent = TeacherAttendance::where('teacher_id',$teacher->id)->whereYear('attendance_date',$year)->where('status','absent')->count();
        $month_total_leave = TeacherAttendance::where('teacher_id',$teacher->id)->whereYear('attendance_date',$year)->where('status','leave')->count();

        $pdf = PDF::loadView('user.invoices.teacher-attendance-invoice',compact('teacher','months','year','month_total','month_total_present','month_total_leave','month_total_absent'));
        // $customPaper = array(0,0,360,360);
        // $dompdf->set_paper($customPaper);
        $pdf->setPaper('A4', 'portrait');
        $pdf->getDomPDF()->set_option("enable_php", true);
          // making pdf name starts
        $makePdfName='invoice';
        return $pdf->stream(
           $makePdfName.'.pdf',
            array(
              'Attachment' => 0
            )
          );
    }

    public function deleteAttendance(Request $request)
    {
        $attendance_date = Attendance::where('class_id',$request->class_id)->whereDate('attendance_date',$request->attendance_date)->get();

        // dd($attendance_date);
        if($attendance_date->count() > 0)
        {
            foreach ($attendance_date as $att) {
                $att->delete();
            }
        }

        return response()->json(['success' => true]);
    }

    public function deleteTeacherAttendance(Request $request)
    {
        $attendance_date = TeacherAttendance::whereDate('attendance_date',$request->attendance_date)->get();

        // dd($attendance_date);
        if($attendance_date->count() > 0)
        {
            foreach ($attendance_date as $att) {
                $att->delete();
            }
        }

        return response()->json(['success' => true]);
    }


}
