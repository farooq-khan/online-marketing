<?php

namespace App\Http\Controllers;

use App\Attendance;
use App\Exam;
use App\FeeRenewHistory;
use App\FeeType;
use App\Models\FeeUpdateHistory;
use App\OtherFee;
use App\Result;
use App\SoldBooksReport;
use App\Student;
use App\StudentClass;
use App\StudentFamily;
use App\StudentFee;
use App\Subject;
use App\Teacher;
use App\TestResult;
use App\User;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class StudentsController extends Controller
{
    public function index()
    {
        $classes = StudentClass::where('school_id', auth()->user()->school_id)->orderBy('class_name','asc')->get();
        $exams = Exam::all();
        // dd($exams);
    	return view('user.students.index',compact('classes','exams'));
    }

    public function addNewStudent(){
      $classes = StudentClass::where('school_id', auth()->user()->school_id)->orderBy('class_name','asc')->get();
      $exams = Exam::all();
      return view('user.students.add-student', compact('classes', 'exams'));
    }

    public function passOutStudents()
    {
        $classes = StudentClass::where('school_id', auth()->user()->school_id)->orderBy('class_name','asc')->get();
        $graduate_dates = Student::where('school_id', auth()->user()->school_id)->select('graduate_date')->whereNotNull('graduate_date')->get();
        return view('user.students.passout-students',compact('classes','graduate_dates'));
    }

    public function getStudents(Request $request)
    {
        $query = Student::whereNotNull('id')->where('school_id', auth()->user()->school_id);

        if($request->student_classes_select != null)
        {
            $query->whereHas('student_class',function($q) use ($request){
                $q->where('id',$request->student_classes_select);
            });
        }

        if($request->discount_student != null)
        {
            if($request->discount_student == 'discount')
            {
                $query->where('discount','>',0);
            }

            elseif($request->discount_student == 'non-discount')
            {
                $query->whereNull('discount')->orWhere('discount',0);
            }
        }

        if($request->student_status != null)
        {
            $query->where('status',$request->student_status);
        }
        if($request->input_keyword != null)
        {
            $input = explode('-', $request->input_keyword);
          $query->where(function($q) use ($input){
            $q->where('guardian','LIKE',"%".$input[0]."%");
            if(@$input[1] != null){
                $q->where('guardian_nickname','LIKE',"%".@$input[1]."%");
            }
          });
            
        }
        if($request->input_keyword_std != null)
        {
          $query->where(function($q) use ($request){
            $q->where('name','LIKE',"%".$request->input_keyword_std."%");
          });
            
        }
        if($request->from_date != null)
        {
            $query->whereDate('suspended_date','>=',$request->from_date);
        }

        if($request->to_date != null)
        {
            $query->whereDate('suspended_date','<=',$request->to_date);
        }
        $total = (clone $query)->sum('fee');

        $ids_a = (clone $query)->pluck('id')->toArray();
        $c_ids_a = (clone $query)->pluck('class_id')->toArray();
        // dd($c_ids_a);
        $student_unpaid_fee = StudentFee::whereIn('student_id',$ids_a)->whereIn('class_id',$c_ids_a)->sum('unpaid_amount');
        $other_unpaid_fee = OtherFee::whereIn('student_id',$ids_a)->whereIn('class_id',$c_ids_a)->sum('unpaid_amount');
        $res = $student_unpaid_fee + $other_unpaid_fee;

      // $query->latest();
        return Datatables::of($query)
        ->addColumn('checkbox', function ($item) {

            $html_string = '<div class="custom-control custom-checkbox custom-checkbox1 d-inline-block">
                            <input type="checkbox" class="custom-control-input check" value="'.$item->id.'" id="stone_check_'.$item->id.'">
                            <label class="custom-control-label" for="stone_check_'.$item->id.'"></label>
                        </div>';
            return $html_string;
        })
        ->addColumn('action', function ($item) {

            // $html_string = '<div class="custom-control custom-checkbox custom-checkbox1 d-inline-block">
            //                 <input type="checkbox" class="custom-control-input check" value="'.$item->id.'" id="stone_check_'.$item->id.'">
            //                 <label class="custom-control-label" for="stone_check_'.$item->id.'"></label>
            //             </div>';

             $html_string = '
                 <a href="'.url('user/student/'.$item->id.'').'" class="actionicon viewIcon delete_order_transaction fas fa-eye" data-id="' . $item->id . '" title="View Detail">
                 </a>
                 ';
            if (Auth::user()->role == 'admin') {
                if($item->status == 1)
                {
                    // $html_string .= ' <a href="javascript:void(0);" class="delete_student deleteIcon" data-type="Delete" data-id="'.$item->id.'" title="Suspend Student" >
                    //             <i class="zmdi zmdi-minus-circle" style="font-size: 18px;"></i></a>';
                    $html_string .= '
                     <a href="javascript:void(0)" class="actionicon suspendIcon fas fa-user-minus" data-toggle="modal" data-target="#EditStudentFee'.$item->id.'" data-id="' . $item->id . '" title="Suspend Student">
                     </a>
                     ';

                    $html_string .= '<div class="modal text-left fade" id="EditStudentFee'.$item->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                      <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Suspend Student</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <form id="edit-student-fee-form-'.$item->id.'" enctype="multipart/form-data">
                          <input type="hidden" name="id" value="'.$item->id.'" />
                          <div class="modal-body ">

                              <div class="form-row">

                                  <div class="form-group col-md-6">
                                    <label for="suspended_date'.$item->id.'">Suspend Date</label>
                                    <input type="date" name="suspended_date" id="suspended_date'.$item->id.'" class="form-control" />
                                </div>
                                </div>
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-primary suspend_student" data-id="'.$item->id.'">confirm</button>
                          </div>
                          </form>
                        </div>
                      </div>
                    </div>';
                }

                if($item->status == 2)
                {
                    $html_string .= ' <a href="javascript:void(0);" class="actionicon activateIcon activate_student fas fa-user-plus" data-type="Delete" data-id="'.$item->id.'" title="Activate Student"></a>';
                }
                $html_string .= '
                     <a href="javascript:void(0)" class="actionicon deleteIcon delete_student_permanant fas fa-trash-alt" data-id="' . $item->id . '" title="Delete Student">
                     </a>
                     ';
            }
            return $html_string;
        })
         ->addColumn('name', function ($item){
            return $item->name !== null ? $item->name : 'N.A';
        })
        // ->filterColumn('name', function( $query, $keyword ) {
        //  $query->where(function($q) use ($keyword){
        //     $q->where('name','LIKE', "$keyword%");
        //  });
        // })

        ->addColumn('guardian', function ($item){
            return $item->guardian !== null ? $item->guardian.' '.@$item->guardian_nickname : 'N.A';
        })
        //  ->filterColumn('guardian', function( $query, $keyword ) {
        //  $query->where(function($q) use ($keyword){
        //     $q->where('guardian','LIKE', "%$keyword%");
        //  });
        // })

        ->addColumn('id', function ($item){
            return $item->id !== null ? $item->id : 'N.A';
        })

        ->addColumn('total_payable', function ($item){
          $student_unpaid_fee = StudentFee::where('student_id',$item->id)->sum('unpaid_amount');
          $other_unpaid_fee = OtherFee::where('student_id',$item->id)->sum('unpaid_amount');
          $res = number_format($student_unpaid_fee + $other_unpaid_fee,2,'.',',');
            return $res !== null ? $res : 'N.A';
        })

         ->editColumn('roll_no', function ($item){
            return $item->roll_no !== null ? $item->roll_no : 'N.A';
        })

         ->addColumn('gender', function ($item){
            return $item->gender !== null ? $item->gender : 'N.A';
        })
         ->editColumn('guardian_phone', function ($item){
            return $item->guardian_phone !== null ? $item->guardian_phone : 'N.A';
        })

         ->addColumn('fee', function ($item){
            $fee = $item->fee != null ? $item->fee : 0;
            // if($item->discount != null)
            // {
            //     $fee = (100 - $item->discount)/100 * $item->student_class->fee;
            // }

            return $fee;
        })
         ->addColumn('class', function ($item){
            return $item->student_class != null ? $item->student_class->class_name : 'N.A';
        })
        ->addColumn('address', function ($item){
            return $item->address !== null ? $item->address : 'N.A';
        })

        ->addColumn('discount', function ($item){
            return $item->discount !== null ? $item->discount.' %' : 0;
        })

        ->addColumn('image', function ($item){
            if($item->image != null)
            {
            	$html_string = 'Image';
            }
            else
            {
            	$html_string = 'Image';
            }

            return $html_string;
        })

        ->addColumn('empty_col', function ($item){
            return '';
        })


        ->rawColumns(['checkbox','action','address','name','roll_no','gender','guardian_phone','fee','class','id','image','guardian','empty_col','total_payable'])
        ->with(['total_amount' => $total,'res' => $res])
        ->make(true);

    }

        public function getPassoutStudents(Request $request)
    {
        $query = Student::where('school_id', auth()->user()->school_id)->where('status',10);
       if($request->graduate_date != null)
       {
        $query->whereDate('graduate_date',$request->graduate_date);
       }
       if($request->from_date != null)
       {
        $query->whereDate('graduate_date','>=',$request->from_date);
       }
       if($request->to_date != null)
       {
        $query->whereDate('graduate_date','<=',$request->to_date);
       }
      // $query->latest();
        return Datatables::of($query)
        ->addColumn('action', function ($item) {

            // $html_string = '<div class="custom-control custom-checkbox custom-checkbox1 d-inline-block">
            //                 <input type="checkbox" class="custom-control-input check" value="'.$item->id.'" id="stone_check_'.$item->id.'">
            //                 <label class="custom-control-label" for="stone_check_'.$item->id.'"></label>
            //             </div>';

             $html_string = '
                 <a href="'.url('user/student/'.$item->id.'').'" class="actionicon viewIcon delete_order_transaction" data-id="' . $item->id . '" title="View Detail"><i class="fas fa-eye"></i></a>
                 ';
            if($item->status == 1)
            {
                $html_string .= ' <a href="javascript:void(0);" class="delete_student deleteIcon" data-type="Delete" data-id="'.$item->id.'" title="Suspend Student" >
                            <i class="zmdi zmdi-minus-circle" style="font-size: 18px;"></i></a>';
            }

            if($item->status == 2)
            {
                $html_string .= ' <a href="javascript:void(0);" class="activate_student viewIcon" data-type="Delete" data-id="'.$item->id.'" title="Activate Student" >
                            <i class="zmdi zmdi-check" style="font-size: 18px;"></i></a>';
            }
            return $html_string;
        })
         ->editColumn('name', function ($item){
            return $item->name !== null ? $item->name : 'N.A';
        })

         ->addColumn('guardian', function ($item){
            return $item->guardian !== null ? $item->guardian.' '.@$item->guardian_nickname : 'N.A';
        })
         ->filterColumn('guardian', function( $query, $keyword ) {
         $query->where(function($q) use ($keyword){
            $q->where('guardian','LIKE', "%$keyword%")->orWhere('guardian_nickname','LIKE', "%$keyword%");
         });
        })

         ->addColumn('id', function ($item){
            return $item->id !== null ? $item->id : 'N.A';
        })

         ->editColumn('roll_no', function ($item){
            return $item->roll_no !== null ? $item->roll_no : 'N.A';
        })

         ->addColumn('gender', function ($item){
            return $item->gender !== null ? $item->gender : 'N.A';
        })
         ->editColumn('guardian_phone', function ($item){
            return $item->guardian_phone !== null ? $item->guardian_phone : 'N.A';
        })

         ->addColumn('fee', function ($item){
            $fee = $item->student_class != null ? $item->student_class->fee : 0;
            if($item->discount != null)
            {
                $fee = (100 - $item->discount)/100 * $item->student_class->fee;
            }

            return $fee;
        })
         ->addColumn('class', function ($item){
            return $item->student_class != null ? $item->student_class->class_name.' (Graduated)' : 'N.A';
        })
        ->addColumn('address', function ($item){
            return $item->address !== null ? $item->address : 'N.A';
        })

         ->addColumn('passout_date', function ($item){
            return $item->graduate_date !== null ? $item->graduate_date : 'N.A';
        })

        ->addColumn('discount', function ($item){
            return $item->discount !== null ? $item->discount.' %' : 0;
        })

        ->addColumn('image', function ($item){
            if($item->image != null)
            {
                $html_string = 'Image';
            }
            else
            {
                $html_string = 'Image';
            }

            return $html_string;
        })


        ->rawColumns(['action','address','name','roll_no','gender','guardian_phone','fee','class','id','image','guardian','passout_date'])
        ->make(true);

    }

    public function allClasses()
    {
        return view('user.students.grades');
    }

    public function getClasses(Request $request)
    {
        $query = StudentClass::where('school_id', auth()->user()->school_id)->orderBy('class_name','asc');
        $teachers = Teacher::where('status',1)->where('school_id', auth()->user()->school_id)->get();
      // $query->latest();
        return Datatables::of($query)
        ->addColumn('checkbox', function ($item) {

            $html_string = ' <a href="javascript:void(0);" class="actionicon deleteicon selected-item-btn btn-sm delete_class deleteIcon" data-type="Delete" data-id="'.$item->id.'" title="Delete Class" >
                            <i class="fa fa-trash-alt"></i></a>';
            return $html_string;
        })
         ->editColumn('class_name', function ($item){
            $name = $item->class_name !== null ? $item->class_name : 'N.A';
            $html_string = '
                 <span class="m-l-15 inputDoubleClick" id="name" style="text-transform: capitalize;"  data-fieldvalue="'.$item->class_name.'">'.$name.'</span>
                <input type="text" autocomplete="nope" name="class_name" class="fieldFocus d-none form-control" value="'.$item->class_name.'" data-id="'.$item->id.'">
            ';

            return $html_string;
        })
        ->addColumn('class_teacher', function ($item) use ($teachers){
            
            $html_string = '<span class="m-l-15 selectDoubleClick" id="teacher_id" data-fieldvalue="'.@$item->teacher_id.'">'.(@$item->teacher_id!=null ? @$item->class_teacher->name :'--').'</span>';

            $html_string .= '<select name="teacher_id" class="selectFocus form-control d-none" data-id='.$item->id.'>
            <option value="">Choose Teacher</option>';

            if($teachers->count() > 0)
            foreach($teachers as $teacher)
            {
                $html_string .= '<option '.(@$teacher->id == $item->teacher_id ? 'selected' : '' ).' value="'.$teacher->id.'">'.$teacher->name.'</option>';
            }
            $html_string .= '</select>';
            return $html_string;
        })

         ->editColumn('fee', function ($item){
            $fee = $item->fee != null ? $item->fee : '--';

            $html_string = '
                 <span class="m-l-15 inputDoubleClick" id="fee" style="text-transform: capitalize;"  data-fieldvalue="'.$item->fee.'">'.$fee.'</span>
                <input type="text" autocomplete="nope" name="fee" class="fieldFocus d-none form-control" value="'.$item->fee.'" data-id="'.$item->id.'">
            ';

            return $html_string;
        })
         ->editColumn('section', function ($item){
            $section = $item->section != null ? $item->section : '--';

            $html_string = '
                 <span class="m-l-15 inputDoubleClick" id="section" style="text-transform: capitalize;"  data-fieldvalue="'.$item->section.'">'.$section.'</span>
                <input type="text" autocomplete="nope" name="section" class="fieldFocus d-none form-control" value="'.$item->section.'" data-id="'.$item->id.'">
            ';

            return $html_string;
        })
         ->addColumn('empty_col', function ($item){
            return '';
        })

        ->rawColumns(['checkbox','class_name','fee','class_teacher','empty_col','section'])
        ->make(true);

    }

    public function addClass(Request $request)
    {
        // dd($request->all());

        $class = new StudentClass;

        $class->class_name = $request->name;
        $class->school_id = auth()->user()->school_id;
        $class->fee        = $request->fee;
        $class->save();

        return response()->json(['success' => true]);
    }

    public function deleteClass(Request $request)
    {
        $id = $request->id;

        $find_students = Student::where('class_id',$id)->get();
        $result = Result::where('class_id',$id)->get();

        if($find_students->count() > 0 || $result->count() > 0)
        {
            return response()->json(['success' => false]);
        }

        $class = StudentClass::find($id);

        $class->delete();

        return response()->json(['success' => true]);
    }

    public function fetchFee(Request $request)
    {
        $id = $request->id;
        $class = StudentClass::find($id);

        // dd($request->all());
        if($class != null)
        {
            $fee = $class->fee;
        }
        else
        {
            $fee = 0;
        }

        return response()->json(['success' => true, 'fee' => $fee]);
    }

    public function addStudent(Request $request)
    {
        // dd($request->all());

        $student_check = Student::where('roll_no',$request->roll_no)->where('school_id', auth()->user()->school_id)->first();
        if($student_check != null)
        {
            return response()->json(['roll_no_exists' => true]);
        }
        $StudentClass = StudentClass::find($request->student_class);
        $student = new Student;

        $student->name = $request->name;
        $student->guardian = $request->guardian;
        $student->guardian_nickname = $request->guardian_nickname;
        $student->guardian_cnic = $request->guardian_cnic;
        $student->guardian_phone = $request->guardian_phone;
        $student->roll_no = $request->roll_no;
        $student->gender = $request->gender;
        $student->date_of_admission = $request->admission_date;
        $student->dob = $request->dob;
        $student->std_phone = $request->std_phone;
        $student->address = $request->address;
        $student->class_id = $request->student_class;
        $student->discount = number_format(($request->discount_amount / $StudentClass->fee) * 100,2,'.',',');
        $student->discount_amount = $request->discount_amount;
        $student->fee = $StudentClass->fee - $request->discount_amount;
        $student->user_id = Auth::user()->id;
        $student->status = 1;
        $student->school_id = auth()->user()->school_id;

        if($request->hasFile('image') && $request->image->isValid())
          {
              $fileNameWithExt = $request->file('image')->getClientOriginalName();
              $fileName = pathinfo($fileNameWithExt,PATHINFO_FILENAME);
              $extension = $request->file('image')->getClientOriginalExtension();
              $fileNameToStore = $fileName.'_'.time().'.'.$extension;
              $path = $request->file('image')->move('public/uploads/students/images/',$fileNameToStore);
              $student->image = $fileNameToStore;
          }

        $student->save();

          return response()->json(['success' => true]);
    }

    public function deleteStudent(Request $request)
    {
        // dd($request->all());
        $id = $request->id;
        $find_students = Student::where('id',$id)->first();
        $find_students->status = 2;
        $find_students->suspended_date = $request->suspended_date;
        $find_students->save();

        return response()->json(['success' => true]);
    }

    public function deleteStudentPermanent(Request $request)
    {
        $check_fee = StudentFee::where('student_id',$request->id)->first();
        $check_other_fee = OtherFee::where('student_id',$request->id)->first();
        $check_attendance = Attendance::where('student_id',$request->id)->first();
        $check_result = Result::where('student_id',$request->id)->first();
        $check_test_result = TestResult::where('student_id',$request->id)->first();
        $check_sold_books = SoldBooksReport::where('student_id',$request->id)->first();

        if($check_fee != null || $check_other_fee != null || $check_attendance != null || $check_result != null || $check_test_result != null || $check_sold_books != null )
        {
            return response()->json(['success' => false]);
        }
        $student = Student::find($request->id);
        $student->delete();

        return response()->json(['success' => true]);
    }

    public function activateStudent(Request $request)
    {
        $id = $request->id;
        $find_students = Student::where('id',$id)->first();
        $find_students->status = 1;
        $find_students->suspended_date = null;
        $find_students->save();

        return response()->json(['success' => true]);
    }

    public function singleStudent($id)
    {
        $student = Student::find($id);
        $students = Student::where('status',1)->where('id','!=',$id)->get();
        $classes = StudentClass::where('school_id', auth()->user()->school_id)->orderBy('class_name','asc')->get();
        $ids = explode(',', $student->promotion_history);
        $student_promotion = StudentClass::whereIn('id',$ids)->get();
        $std_cl = '';
        if($student_promotion->count() > 0)
        {
            foreach ($student_promotion as $cla) {
                $std_cl .= $cla->class_name.' --> ';
            }
        }
        $std_cl .= $student->student_class->class_name;

        $student_total_fee = StudentFee::where('student_id',$student->id)->sum('total_amount');
        $student_paid_fee = StudentFee::where('student_id',$student->id)->sum('paid_amount');
        $tuition_fee_concession = StudentFee::where('student_id',$student->id)->sum('concession');
        $student_unpaid_fee = StudentFee::where('student_id',$student->id)->sum('unpaid_amount');

        $student_total_other_fee = OtherFee::where('student_id',$student->id)->sum('amount');
        $student_paid_other_fee = OtherFee::where('student_id',$student->id)->sum('paid_amount');
        $other_fee_concession = OtherFee::where('student_id',$student->id)->sum('concession');
        $other_unpaid_fee = OtherFee::where('student_id',$student->id)->sum('unpaid_amount');

        $book_totals = SoldBooksReport::where('student_id',$student->id)->sum('total_amount');
        $book_paid = SoldBooksReport::where('student_id',$student->id)->sum('paid_amount');
        $class_id = $student->class_id;
        $student_class = Result::where('student_id',$id)->pluck('class_id')->toArray();
        $student_classes = StudentClass::whereIn('id',$student_class)->orderBy('class_name','asc')->get();
        $class_sub = Subject::where('class_id',$student->class_id)->get();

        return view('user.students.single-student',compact('student','students','classes','std_cl','student_total_fee','student_paid_fee','student_unpaid_fee','book_totals','book_paid','tuition_fee_concession','student_total_other_fee','student_paid_other_fee','other_fee_concession','other_unpaid_fee', 'class_id', 'student_classes', 'class_sub', 'id'));
    }

    public function updateStudent(Request $request)
    {
        $old_value = $request->new_select_value;

        $student = Student::where('id',$request->id)->first();

        $show_title = $request->new_select_value;
        foreach($request->except('id','new_select_value') as $key => $value)
        {
            if($key == 'discount_amount')
            {
                $dis = $value;
                $class_find = StudentClass::find($student->class_id);
                $class_fee = $class_find->fee;
                // dd($class_fee);
                $final_fee = $class_fee - $dis;
                $per = ($dis / $class_fee) * 100;

                $student->discount = round($per);
                $student->fee = $final_fee;
                $student->$key = $dis;
            }
            else
            {
                $student->$key = $value;
            }
        }

        $student->save();

        return response()->json(['success' => true]);
    }

    public function getStudentFamily(Request $request)
    {
        $query = StudentFamily::where('student_id',$request->id);

      // $query->latest();
        return Datatables::of($query)
        ->addColumn('checkbox', function ($item) {

            // $html_string = '<div class="custom-control custom-checkbox custom-checkbox1 d-inline-block">
            //                 <input type="checkbox" class="custom-control-input check" value="'.$item->id.'" id="stone_check_'.$item->id.'">
            //                 <label class="custom-control-label" for="stone_check_'.$item->id.'"></label>
            //             </div>';
            if(Auth::user()->role !== 'student')
            {
             $html_string = '
                 <a href="'.url('user/student/'.$item->student_family_member->id.'').'" class="actionicon viewIcon delete_order_transaction" data-id="' . $item->student_family_member->id . '" title="View Detail"><i class="fa fa-eye"></i></a>
                 ';
            $html_string .= ' <a href="javascript:void(0);" class="delete_student_family actionicon deleteIcon" data-type="Delete" data-id="'.$item->id.'" title="Delete Student Family" >
                            <i class="fa fa-trash-alt"></i></a>';
            }
            else
            {
                $html_string = '--';
            }
            return $html_string;
        })
         ->editColumn('father', function ($item){
            return $item->student_family_member->guardian !== null ? $item->student_family_member->guardian : 'N.A';
        })

        ->addColumn('name', function ($item){
            return $item->student_family_member->name !== null ? $item->student_family_member->name : 'N.A';
        })

        ->addColumn('relation', function ($item){
            return $item->relation !== null ? $item->relation : 'N.A';
        })

        ->addColumn('std_class', function ($item){
            return $item->student_family_member->student_class !== null ? $item->student_family_member->student_class->class_name : 'N.A';
        })

        ->rawColumns(['checkbox','name','father','relation','std_class'])
        ->make(true);

    }

    public function addStudentFamily(Request $request)
    {
        // dd($request->all());

        $check_family = StudentFamily::where('student_id',$request->std_id)->where('member_id',$request->member)->get();

        if($check_family->count() > 0)
        {
            return response()->json(['already_exist' => true]);
        }

        $student_family = new StudentFamily;
        $student_family->student_id = $request->std_id;
        $student_family->member_id = $request->member;
        $student_family->relation = $request->relation;
        $student_family->save();

          return response()->json(['success' => true]);
    }

    public function deleteStudentFamily(Request $request)
    {
        $id = $request->id;
        $find_students = StudentFamily::where('id',$id)->first();
        $find_students->delete();

        return response()->json(['success' => true]);
    }

    public function fetchFamilyStudents(Request $request)
    {
        $id = $request->id;
        $students = Student::where('class_id',$id)->get();

        $html_string = '<option value="">--Select Student--</option>';

        foreach ($students as $value) {
           $html_string .= '<option value="'.$value->id.'">'.$value->name.'</option>';
        }

        $html_string2 = '<option value="">--Select Father/Guardian--</option>';
        foreach ($students as $value) {
           $html_string2 .= '<option value="'.$value->id.'">'.$value->guardian.'</option>';
        }

        return response()->json(['success' => true, 'html' => $html_string,'html2' => $html_string2]);
    }

    public function fetchFamilyStudentsByGuardian(Request $request)
    {
        $id = $request->id;
        $class_id = $request->class_id;

        $std = Student::find($id);
        $students = Student::where('class_id',$class_id)->where('guardian',$std->guardian)->get();

        $html_string = '<option value="">--Select Student--</option>';

        foreach ($students as $value) {
           $html_string .= '<option value="'.$value->id.'">'.$value->name.'</option>';
        }

        return response()->json(['success' => true, 'html' => $html_string]);
    }

    public function addStudentPhoto(Request $request)
    {
        // dd($request->all());

        $student = Student::find($request->std_id);

        if($request->hasFile('image') && $request->image->isValid())
          {
              $fileNameWithExt = $request->file('image')->getClientOriginalName();
              $fileName = pathinfo($fileNameWithExt,PATHINFO_FILENAME);
              $extension = $request->file('image')->getClientOriginalExtension();
              $fileNameToStore = $fileName.'_'.time().'.'.$extension;
              $path = $request->file('image')->move('public/uploads/students/images/',$fileNameToStore);
              $student->image = $fileNameToStore;
          }

        $student->save();
        return response()->json(['success' => true]);
    }

    public function allFee()
    {
        $year = Carbon::now()->format('Y');
        $classes = StudentClass::where('school_id', auth()->user()->school_id)->orderBy('class_name','asc')->get();
        $students = Student::where('school_id', auth()->user()->school_id)->where('status',1)->get();
        return view('user.students.all-fee',compact('classes','students','year'));
    }

    public function submittedFee($type = null)
    {
        $year = Carbon::now()->format('Y');
        $classes = StudentClass::where('school_id', auth()->user()->school_id)->orderBy('class_name','asc')->get();
        $students = Student::where('school_id', auth()->user()->school_id)->where('status',1)->get();
        return view('user.students.all-submitted-fee',compact('classes','students','year', 'type'));
    }

    public function assignFee()
    {
        $year = Carbon::now()->format('Y');
        $classes = StudentClass::where('school_id', auth()->user()->school_id)->orderBy('class_name','asc')->get();
        $students = Student::where('school_id', auth()->user()->school_id)->where('status',1)->get();
        return view('user.students.assign-fee',compact('classes','students','year'));
    }

    public function checkUnsubmittedFee()
    {
        $year = Carbon::now()->format('Y');
        $classes = StudentClass::where('school_id', auth()->user()->school_id)->orderBy('class_name','asc')->get();
        $students = Student::where('school_id', auth()->user()->school_id)->get();
        $fee_types = FeeType::where('school_id', auth()->user()->school_id)->where('status',1)->get();
        return view('user.students.check-unsubmitted-fee',compact('classes','students','year','fee_types'));
    }

    public function renewMonthlyFee()
    {
        $classes = StudentClass::orderBy('class_name','asc')->get();
        return view('user.students.renew-monthly-fee',compact('classes'));
    }

    public function getStudentsFee(Request $request)
    {
        $query = StudentFee::query()->where('school_id', auth()->user()->school_id)->with('student','history');

        if($request->student_classes_select != null)
        {
            $query->where('class_id',$request->student_classes_select);
        }

        if($request->discount_student != null)
        {
            if($request->discount_student == 'discount')
            {
                $query->where('discount','>',0);
            }

            elseif($request->discount_student == 'non-discount')
            {
                $query->where(function($q){
                    $q->whereNull('discount')->orWhere('discount',0);
                });
            }
        }

        if($request->status_student != null)
        {
            $query->where('status',$request->status_student);
        }

        if($request->from_date != null)
        {
            $query->whereDate('fee_month','>=',$request->from_date);
        }

        if($request->to_date != null)
        {
            $query->whereDate('fee_month','<=',$request->to_date);
        }

        if($request->sub_from_date != null)
        {
            $query->where(function($q) use ($request){
                $q->whereDate('submitted_date','>=',$request->sub_from_date)
                // ->orWhereHas('history',function($q) use ($request){
                //     $q->whereDate('submitted_date','>=',$request->sub_from_date);
                // });
                ->orWhere(function($z) use ($request){
                    $z->leftJoin('histories', 'student_fees.id', '=', 'histories.fee_id')->where('submitted_date', '>=', $request->sub_from_date);
                });
            });

        }

        if($request->sub_to_date != null)
        {
            // $query->whereDate('submitted_date','<=',$request->sub_to_date)
            // ->orWhereHas('history',function($q) use ($request){
            //     $q->whereDate('submitted_date','<=',$request->sub_to_date);
            // });

            $query->where(function($q) use ($request){
                $q->whereDate('submitted_date','<=',$request->sub_to_date)
                // ->orWhereHas('history',function($q) use ($request){
                //     $q->whereDate('submitted_date','<=',$request->sub_to_date);
                // });
                ->orWhere(function($z) use ($request){
                    $z->leftJoin('histories', 'student_fees.id', '=', 'histories.fee_id')->where('submitted_date', '<=', $request->sub_to_date);
                });
            });
        }

        if($request->father_name != null)
        {
            $input = explode('-', $request->father_name);
            $query->whereHas('student', function($q) use($input){
              $q->where('guardian','LIKE', "%".@$input[0]."%");
              if(isset($input[1]))
                $q->where('guardian_nickname','LIKE', "%".@$input[1]."%");
            });
        }

        if($request->student_search != null)
        {
          $query->whereHas('student', function($q) use($request){
              $q->where('name','LIKE', "%$request->student_search%");
          });
            
        }

        if($request->receipt != null)
        {
            $receipt_no = explode('-', $request->receipt);
            if(isset($receipt_no[0]))
            $query->where(\DB::raw('receipt+0'),'>=',$receipt_no[0]);

            if(isset($receipt_no[1]))
            $query->where(\DB::raw('receipt+0'),'<=',$receipt_no[1]);
            
        }

        $total_amount = (clone $query)->sum('total_amount');
        $concession = (clone $query)->sum('concession');
        $paid_amount = (clone $query)->sum('paid_amount');
        $unpaid_amount = (clone $query)->sum('unpaid_amount');
      // $query->latest();
        return Datatables::of($query)
        ->addColumn('checkbox', function ($item) {
            $html_string = '<div class="custom-control custom-checkbox custom-checkbox1 d-inline-block">
                            <input type="checkbox" class="custom-control-input check" value="'.$item->id.'" id="stone_check_'.$item->id.'">
                            <label class="custom-control-label" for="stone_check_'.$item->id.'"></label>
                        </div>';
            return $html_string;
        })
        ->addColumn('action', function ($item) {



             $html_string = '
                 <a href="'.url('user/student-fee/'.$item->student->id.'/'.$item->class_id.'').'" class="actionicon viewIcon delete_order_transaction" data-id="' . $item->student->id . '" title="View Details"><i class="fa fa-eye"></i></a>
                 ';
              $html_string .= '
                 <a href="javascript:void(0)" class="actionicon viewIcon edit_student_fee download_single_fee" data-id="' . $item->id . '" title="Download"><i class="fa fa-download"></i></a>
                 ';
            $html_string .= '
                 <a href="javascript:void(0)" class="actionicon viewIcon view_history" data-toggle="modal" data-target="#history_table" data-id="' . $item->id . '" title="History"><i class="fa fa-history"></i></a>
                 ';

            if(true)
            {
                $html_string .= '
                 <a href="javascript:void(0)" class="actionicon viewIcon edit_student_fee" data-toggle="modal" data-target="#EditStudentFee'.$item->id.'" data-id="' . $item->id . '" title="Edit"><i class="fa fa-edit"></i></a>
                 ';

                 // $html_string .= '
                 // <a href="javascript:void(0)" class="actionicon viewIcon edit_student_fee download_single_fee" data-id="' . $item->id . '" title="Edit"><i class="zmdi zmdi-download"></i></a>
                 // ';

                 $html_string .= '<div class="modal fade" id="EditStudentFee'.$item->id.'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                  <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Edit Fee</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <form id="edit-student-fee-form-'.$item->id.'" enctype="multipart/form-data">
                      <input type="hidden" name="id" value="'.$item->id.'" />
                      <div class="modal-body ">

                          <div class="form-row">

                              <div class="form-group col-md-6">
                                <label for="total_amount_edit_'.$item->id.'"><i class="zmdi zmdi-money-off pr-2"></i>Total Amount</label>
                                <input type="text" name="total_amount" id="total_amount_edit_'.$item->id.'" class="form-control" value="'.$item->total_amount.'" placeholder="0.00" disabled="disabled" />
                            </div>

                              <div class="form-group col-md-6">
                                <label for="paid_amount_edit_'.$item->id.'"><i class="zmdi zmdi-money-off pr-2"></i>Amount Paid</label>
                                <input type="number" name="paid_amount" id="paid_amount_edit_'.$item->id.'" value="'.$item->paid_amount.'" min="'.$item->paid_amount.'" max="'.$item->total_amount.'" class="form-control" placeholder="Paid Amount" disabled="true" />
                            </div>

                            <div class="form-group col-md-6">
                                <label for="unpaid_amount_edit_'.$item->id.'"><i class="zmdi zmdi-money-off pr-2"></i>Amount Unpaid</label>
                                <input type="number" name="paid_amount" id="unpaid_amount_edit_'.$item->id.'" value="'.$item->unpaid_amount.'" min="'.$item->unpaid_amount.'" max="'.$item->unpaid_amount.'" class="form-control" placeholder="Unpaid Amount" disabled="true" />
                            </div>

                            <div class="form-group col-md-6">
                                <label for="new_payment_'.$item->id.'"><i class="zmdi zmdi-money-off pr-2"></i>New Payment</label>
                                <input type="number" name="new_payment" id="new_payment_'.$item->id.'" value="" class="form-control" placeholder="Please Enter New Payment" />
                            </div>

                             <div class="form-group col-md-6">
                                <label for="concession_'.$item->id.'"><i class="zmdi zmdi-money-off pr-2"></i>Concession</label>
                                <input type="number" name="concession" id="concession_'.$item->id.'" value="" class="form-control" placeholder="Enter Concession(Optional)" />
                            </div>

                            <div class="form-group col-md-6">
                                <label for="concession_reason_'.$item->id.'"><i class="zmdi zmdi-money-off pr-2"></i>Concession Reason</label>
                                <input type="text" name="reason" id="concession_reason_'.$item->id.'" value="" class="form-control" placeholder="Please Enter Concession Reason(Optional)" />
                            </div>
                            <div class="form-group col-md-6">
                                <label for="submitted_date_'.$item->id.'"><i class="fa fa-calendar pr-2"></i>Submitted Date</label>
                                <input type="date" name="submitted_date" id="submitted_date_'.$item->id.'" value="" class="form-control" />
                            </div>
                            <div class="form-group col-md-6">
                                <label for="receipt'.$item->id.'"><i class="fa fa-file pr-2"></i>Receipt No.</label>
                                <input type="text" name="receipt" id="receipt'.$item->id.'" value="" class="form-control" placeholder="Please Enter Receipt No.(Optional)" />
                            </div>

                            </div>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary edit_fee" data-id="'.$item->id.'">Update</button>
                      </div>
                      </form>
                    </div>
                  </div>
                </div>';
            }
            return $html_string;
        })
         ->addColumn('name', function ($item){
            return $item->student->name !== null ? $item->student->name : 'N.A';
        })

         ->filterColumn('name', function( $query, $keyword ) {
             $query->whereHas('student', function($q) use($keyword){
                    $q->where('name','LIKE', "%$keyword%");
                });
            })

         ->addColumn('guardian', function ($item){
            return $item->student->guardian !== null ? $item->student->guardian.' '.@$item->student->guardian_nickname : 'N.A';
        })

         ->addColumn('concession', function ($item){
            return $item->concession !== null ? $item->concession : 'N.A';
        })

          ->addColumn('reason', function ($item){
            return $item->reason !== null ? $item->reason : 'N.A';
        })

         ->filterColumn('guardian', function( $query, $keyword ) {
             $query->whereHas('student', function($q) use($keyword){
                    $q->where('guardian','LIKE', "%$keyword%");
                });
            })

         ->addColumn('id', function ($item){
            return $item->id !== null ? $item->id : 'N.A';
        })

         ->addColumn('roll_no', function ($item){
            return $item->student->roll_no !== null ? $item->student->roll_no : 'N.A';
        })

          ->filterColumn('roll_no', function( $query, $keyword ) {
             $query->whereHas('student', function($q) use($keyword){
                    $q->where('roll_no','LIKE', "%$keyword%");
                });
            })

         ->addColumn('gender', function ($item){
            return $item->student->gender !== null ? $item->student->gender : 'N.A';
        })
         ->addColumn('suspended_date', function ($item){
            return $item->suspended_date !== NULL ? $item->suspended_date : '--';
        })

         ->addColumn('fee', function ($item){
            // $fee = $item->student->student_class != null ? $item->student->student_class->fee : 0;
            // if($item->student->discount != null)
            // {
            //     $fee = (100 - $item->student->discount)/100 * $item->student->student_class->fee;
            // }

            // return $fee;

            return $item->total_amount;
        })
         ->addColumn('class', function ($item){
            return $item->student_class != null ? $item->student_class->class_name : 'N.A';
        })

        ->addColumn('discount', function ($item){
            return $item->discount !== null ? number_format($item->discount,2,'.',',').' %' : 0;
        })

        ->addColumn('invoice_no', function ($item){
            return $item->receipt != null ? $item->receipt : '--';
        })

        ->addColumn('paid_amount', function ($item){
            return $item->paid_amount;
        })

        ->addColumn('unpaid_amount', function ($item){
            return $item->unpaid_amount;
        })

        ->addColumn('fee_month', function ($item){
            return carbon::parse($item->fee_month)->format('d M Y');
        })

        ->addColumn('submitted_date', function ($item){
            return $item->submitted_date != null ? carbon::parse($item->submitted_date)->format('d M Y') : '--';
        })

        ->addColumn('status', function ($item){
            if($item->status == 1)
            {
                return '<span class="badge badge-pill d-block badge-outline-success">Paid</span>';
            }
            else if($item->status == 0)
            {
                return '<span class="badge badge-pill d-block badge-outline-danger">Unpaid</span>';
            }
            else
            {
                return '<span class="badge badge-pill badge-outline-secondary">Partial Paid</span>';
            }
        })



        ->rawColumns(['checkbox','address','name','roll_no','gender','guardian_phone','fee','class','id','image','guardian','invoice_no','paid_amount','unpaid_amount','fee_month','submitted_date','status','action','suspended_date'])
        ->with(['total_amount' => $total_amount, 'paid_amount' => $paid_amount, 'unpaid_amount' => $unpaid_amount,'concession' => $concession])
        ->make(true);

    }

    public function checkUnsubmittedFeeTable(Request $request)
    {
        // dd($request->all());
        $query = StudentFee::where('school_id', auth()->user()->school_id)->with('student');

        if($request->student_classes_select != null)
        {
            if($request->student_classes_select == 'all')
            {

            }
            else
            {
                $query->where('class_id',$request->student_classes_select);
            }
        }

        if($request->from_date != null)
        {
            $query->whereDate('fee_month','>=',$request->from_date);
        }

        if($request->to_date != null)
        {
            $query->whereDate('fee_month','<=',$request->to_date);
        }
        if($request->student_classes_select != null && $request->from_date != null && $request->to_date != null)
        {
            $ids = $query->pluck('student_id')->toArray();
        }
        else
        {
            $ids = [];
        }
        if($request->student_classes_select == 'all')
        {
            $students = Student::with('student_class')->where('school_id', auth()->user()->school_id)->where('status',1)->whereNotIn('id',$ids);
        }
        else
        {
            $students = Student::with('student_class')->where('school_id', auth()->user()->school_id)->where('class_id',$request->student_classes_select)->whereNotIn('id',$ids)->where('status',1);
        }


        return Datatables::of($students)
        ->addColumn('checkbox', function ($item) {
            $html_string = '<div class="custom-control custom-checkbox custom-checkbox1 d-inline-block">
                            <input type="checkbox" class="custom-control-input check" value="'.$item->id.'" id="stone_check_'.$item->id.'">
                            <label class="custom-control-label" for="stone_check_'.$item->id.'"></label>
                        </div>';
            return $html_string;
        })
        ->addColumn('action', function ($item) {



             $html_string = '
                 <a href="'.url('user/student-fee/'.$item->id.'/'.$item->class_id.'').'" class="actionicon viewIcon delete_order_transaction" data-id="' . $item->id . '" title="View Detail"><i class="fa fa-eye"></i></a>
                 ';
            return $html_string;
        })
         ->editColumn('name', function ($item){
            return $item->name !== null ? $item->name : 'N.A';
        })

         ->addColumn('guardian', function ($item){
            return $item->guardian !== null ? $item->guardian.' '.@$item->guardian_nickname : 'N.A';
        })
         ->filterColumn('guardian', function( $query, $keyword ) {
         $query->where(function($q) use ($keyword){
            $q->where('guardian','LIKE', "%$keyword%")->orWhere('guardian_nickname','LIKE', "%$keyword%");
         });
        })

         ->addColumn('id', function ($item){
            return $item->id !== null ? $item->id : 'N.A';
        })

         ->editColumn('roll_no', function ($item){
            return $item->roll_no !== null ? $item->roll_no : 'N.A';
        })

         ->addColumn('gender', function ($item){
            return $item->gender !== null ? $item->gender : 'N.A';
        })

         ->addColumn('fee', function ($item){
            // $fee = $item->student->student_class != null ? $item->student->student_class->fee : 0;
            // if($item->student->discount != null)
            // {
            //     $fee = (100 - $item->student->discount)/100 * $item->student->student_class->fee;
            // }

            // return $fee;

            return $item->fee;
        })
         ->addColumn('class', function ($item){
            return $item->student_class != null ? $item->student_class->class_name : 'N.A';
        })

        ->addColumn('discount', function ($item){
            return $item->discount !== null ? $item->discount.' %' : 0;
        })

        ->rawColumns(['checkbox','address','name','roll_no','gender','guardian_phone','fee','class','id','image','guardian','invoice_no','paid_amount','unpaid_amount','fee_month','submitted_date','status','action'])
        ->make(true);

    }

    public function fetchStudentDetail(Request $request)
    {
        $student = Student::with('student_class')->where('id',$request->id)->first();

        return response()->json(['student' => $student , 'success' => true]);
    }

    public function addStudentFee(Request $request)
    {
        // dd($request->all());
        $student = Student::find($request->student_admission_no);
        $total_amount = $student->fee;
        $month = carbon::parse($request->fee_month)->format('m');
        $year = carbon::parse($request->fee_month)->format('Y');
        $check_month = StudentFee::where('student_id',$student->id)->whereMonth('fee_month',$month)->whereYear('fee_month',$year)->first();

        if($total_amount < $request->paid_amount)
        {
            return response()->json(['total_amount' => true]);
        }

        if($check_month != null)
        {
            return response()->json(['paid' => true]);
        }
        $fee = new StudentFee;

        $fee->student_id = $request->student_admission_no;
        $fee->class_id = $student->class_id;
        $fee->discount = $student->discount;
        $fee->total_amount = $total_amount;
        $fee->paid_amount = $request->paid_amount;
        $fee->concession = $request->concession;
        $fee->receipt = $request->receipt;
        $fee->reason = $request->concession_reason;
        $fee->unpaid_amount = $total_amount - $request->paid_amount - $request->concession;
        $fee->fee_month = $request->fee_month;
        $fee->month_id = $student->getMonth($month);
        $fee->submitted_date = $request->submitted_date;
        $fee->remark = $request->remark;
        $fee->user_id = Auth::user()->id;
        $fee->school_id = Auth::user()->school_id;

        if($total_amount == ($request->paid_amount+$request->concession))
        {
            $fee->status = 1;
        }
        else
        {
            $fee->status = 2;
        }

        $fee->save();

        return response()->json(['success' => true]);

    }

    public function editStudentFee(Request $request)
    {
        // dd($request->all());
        $fee = StudentFee::find($request->id);
        $fee->paid_amount = $fee->paid_amount + $request->new_payment;
        $fee->concession = $fee->concession + $request->concession;
        $fee->reason = $request->reason;
        $fee->receipt = $request->receipt;
        $fee->unpaid_amount = $fee->unpaid_amount - $request->new_payment - $request->concession;
        $fee->save();

        if($fee->unpaid_amount == 0 || $fee->unpaid_amount < 0)
        {
            $fee->status = 1;
            $fee->save();
        }
        if($request->submitted_date != null)
        {
            $fee->submitted_date = $request->submitted_date;
        }
        $fee->save();

        $new_history = new FeeUpdateHistory;
        $new_history->fee_id = $request->id;
        $new_history->paid_amount = $request->new_payment;
        $new_history->concession = $request->concession;
        $new_history->concession_reason = $request->reason;
        $new_history->receipt_no = $request->receipt;
        $new_history->unpaid_amount = $fee->unpaid_amount;
        $new_history->user_id = Auth::user()->id;
        $new_history->submitted_date = $request->submitted_date;
        $new_history->save();
        return response()->json(['success' => true]);

    }
    public function editStudentOtherFee(Request $request)
    {
        // dd($request->all());
        $fee = OtherFee::find($request->id);
        $fee->paid_amount = $fee->paid_amount + $request->new_payment;
        $fee->concession = $fee->concession + $request->concession;
        $fee->reason = $request->reason;
        $fee->receipt = $request->receipt;
        $fee->unpaid_amount = $fee->unpaid_amount - $request->new_payment - $request->concession;
        $fee->save();

        if($fee->unpaid_amount == 0 || $fee->unpaid_amount < 0)
        {
            $fee->status = 1;
            $fee->save();
        }
        if($request->submitted_date != null)
        {
            $fee->submitted_date = $request->submitted_date;
        }
        $fee->save();
        return response()->json(['success' => true]);

    }

    public function promoteStudents(Request $request)
    {
        // dd($request->all());
        $students_ids = explode(',', $request->selected_students_ids);
        $students = Student::whereIn('id',$students_ids)->get();
        $class = StudentClass::find($request->class_id);
        foreach ($students as $std) {
            $std->promotion_history = $std->promotion_history.','.$std->class_id;
            $std->class_id = $request->class_id;
            $std->fee = ((100 - $std->discount) / 100) * $class->fee;
            $std->save();
        }

        return response()->json(['success' => true]);
    }
    public function moveStudents(Request $request)
    {
        // dd($request->all());
        $students_ids = explode(',', $request->selected_students_ids);
        $students = Student::whereIn('id',$students_ids)->get();
        $class = StudentClass::find($request->class_id);
        foreach ($students as $std) {
            // $std->promotion_history = $std->promotion_history.','.$std->class_id;
            $std->move_history = $std->move_history.','.$std->class_id;
            $std->class_id = $request->class_id;
            $std->fee = ((100 - $std->discount) / 100) * $class->fee;
            $std->save();
        }

        return response()->json(['success' => true]);
    }

    public function graduateStudents(Request $request)
    {
        // dd($request->all());
        $students_ids = explode(',', $request->selected_students_ids2);
        $students = Student::whereIn('id',$students_ids)->get();
        foreach ($students as $std) {
            $std->graduate_date = $request->graduate_date;
            $std->status = 10;
            $std->save();
        }

        return response()->json(['success' => true]);
    }

    public function makeFeeEntry(Request $request)
    {
        $year = carbon::parse($request->m_date)->format('Y');
        $month = carbon::parse($request->m_date)->format('m');
        $class_id = $request->class_id;

        $ids = explode(',',$request->std_ids);
        // dd($year,$month,$class_id,$ids);

        foreach ($ids as $id) {
            $std = Student::find($id);
            $found = 0;
            if($class_id == 'all')
            {
                $check_fee = StudentFee::where('class_id',$std->class_id)->where('student_id',$id)->whereMonth('fee_month',$month)->whereYear('fee_month',$year)->first();
            }
            else
            {
                $check_fee = StudentFee::where('class_id',$class_id)->where('student_id',$id)->whereMonth('fee_month',$month)->whereYear('fee_month',$year)->first();
            }

           if($check_fee == null)
           {
                if($std->status == 1)
                {
                    $found = 1;
                    $new_fee = new StudentFee;
                    $new_fee->student_id = $id;
                    if($class_id == 'all')
                    {
                        $new_fee->class_id = $std->class_id;
                    }
                    else
                    {
                        $new_fee->class_id = $class_id;
                    }
                    $new_fee->discount = $std->discount;
                    $new_fee->total_amount = $std->fee;
                    $new_fee->paid_amount = 0;
                    $new_fee->unpaid_amount = $std->fee;
                    $new_fee->fee_month = $request->m_date;
                    $new_fee->year = $year;
                    $new_fee->month_id = $month;
                    $new_fee->user_id = Auth::user()->id;
                    $new_fee->school_id = auth()->user()->school_id;
                    $new_fee->status = 0;
                    $new_fee->save();
                }
           }
        }
        if($found > 0)
        {
            //to record history
            $new_history = new FeeRenewHistory;
            $new_history->user_id = Auth::user()->id;
            $new_history->fee_month = $request->m_date;
            $new_history->class_id = $class_id;
            $new_history->school_id = auth()->user()->school_id;
            $new_history->fee_type = 'Tuition Fee';
            $new_history->save();
        }


        return response()->json(['success' => true]);
    }

    public function makeOtherFeeEntry(Request $request)
    {
        // dd($request->all());
        $year = carbon::parse($request->fee_month)->format('Y');
        $month = carbon::parse($request->fee_month)->format('m');
        $class_id = $request->other_fee_class_id;

        $ids = explode(',',$request->other_fee_std_ids);
        // dd($year,$month,$class_id,$ids);

        foreach ($ids as $id) {
            $student = Student::find($id);
            $found = 0;
            if($class_id == 'all')
            {
                $check_fee = OtherFee::where('class_id',$student->class_id)->where('student_id',$id)->whereMonth('fee_month',$month)->whereYear('fee_month',$year)->first();
            }
            else
            {
                $check_fee = OtherFee::where('class_id',$class_id)->where('student_id',$id)->whereMonth('fee_month',$month)->whereYear('fee_month',$year)->first();
            }

           if($check_fee == null)
           {
                if($student->status == 1)
                {
                    $found = 1;
                    $other_Fee = new OtherFee;
                    $other_Fee->student_id = $id;
                    if($class_id == 'all')
                    {
                        $other_Fee->class_id = $student->class_id;
                    }
                    else
                    {
                        $other_Fee->class_id = $class_id;
                    }
                    $other_Fee->fee_type_id = $request->fee_type;
                    $other_Fee->amount = $request->amount;
                    $other_Fee->paid_amount = $request->paid_amount;
                    $other_Fee->concession = $request->concession;
                    $other_Fee->unpaid_amount = $request->amount - $request->concession - $request->paid_amount;
                    $other_Fee->fee_month = $request->fee_month;
                    $other_Fee->submitted_date = $request->submitted_date;
                    $other_Fee->remark = $request->remark;
                    $other_Fee->receipt = $request->receipt;
                    $other_Fee->user_id = Auth::user()->id;
                    $other_Fee->save();
                    if($other_Fee->unpaid_amount == 0 || $other_Fee->unpaid_amount < 0)
                    {
                        $other_Fee->status = 1;
                    }
                    else
                    {
                        $other_Fee->status = 0;
                    }
                    $other_Fee->save();
                }
           }
        }
        if($found > 0)
        {
            $fee_type = FeeType::find($request->fee_type);
            //to record history
            $new_history = new FeeRenewHistory;
            $new_history->user_id = Auth::user()->id;
            $new_history->fee_month = $request->fee_month;
            $new_history->class_id = $class_id;
            $new_history->school_id = auth()->user()->school_id;
            $new_history->fee_type = $fee_type->title;
            $new_history->save();
        }


        return response()->json(['success' => true]);
    }

    public function getFeeRenewHistory(Request $request)
    {
        $find_history = FeeRenewHistory::where('school_id', auth()->user()->school_id)->orderBy('id','desc');

        return Datatables::of($find_history)

        ->addColumn('id', function ($item){
            return $item->id !== null ? $item->id : 'N.A';
        })

        ->addColumn('class_name', function ($item){
            if($item->class_id == 'all')
            {
                return 'All';
            }
            else
            {
                return $item->student_class !== null ? $item->student_class->class_name : 'N.A';
            }
        })
        ->addColumn('created_by', function ($item){
            return $item->user != null ? $item->user->name : '--';
        })

        ->addColumn('fee_month', function ($item){
            return $item->fee_month != null ? carbon::parse($item->fee_month)->format('d-m-Y') : '--';
        })
        ->addColumn('fee_type', function ($item){
            return $item->fee_type != null ? $item->fee_type : '--';
        })
        ->addColumn('created_at', function ($item){
            return $item->created_at != null ? carbon::parse($item->created_at)->format('d-m-Y') : '--';
        })

        ->rawColumns(['id','class_name','created_by','fee_month','fee_type'])
        ->make(true);
    }

    public function viewFeeHistory(Request $request)
    {
        $history = FeeUpdateHistory::where('fee_id',$request->id);
        if($request->sub_from_date != null)
        {
            $history->whereDate('submitted_date','>=',$request->sub_from_date);
        }

        if($request->sub_to_date != null)
        {
            $history->whereDate('submitted_date','<=',$request->sub_to_date);
        }
        $history = $history->get();

        $html = '<table class="w-100 history-table-design">';
        $html .= '<thead>
                <tr>
                    <th>Action</th>
                    <th>Submitted To</th>
                    <th>Paid Amount</th>
                    <th>Unpaid Amount</th>
                    <th>Concession</th>
                    <th>Concession Reason</th>
                    <th>Receipt</th>
                    <th>Submitted Date</th>
                </tr>
        </thead>';
        if($history->count() > 0)
        {
            foreach($history as $his)
            {
                $html .= '<tr>
                    <td><i class="fa fa-trash delete_history__icon" data-id="'.$his->id.'" title="Delete Record"></i></td>
                    <td>'.($his->user != null ? $his->user->name : "--").'</td>
                    <td>'.$his->paid_amount.'</td>
                    <td>'.$his->unpaid_amount.'</td>
                    <td>'.($his->concession != null ? $his->concession : '--').'</td>
                    <td>'.($his->concession_reason != null ? $his->concession_reason : '--').'</td>
                    <td>'.($his->receipt_no != null ? $his->receipt_no : '--').'</td>
                    <td>'.carbon::parse($his->submitted_date)->format('d-m-Y').'</td>
                </tr>';
            }
        }
        else
        {
            $html .= '<tr>
                    <td colspan="7">No Data Found !!!</td>
                </tr>';
        }

        $html .= '</table>';

        return response()->json(['html' => $html]);
    }

    public function deleteUpdateFeeHistory(Request $request)
    {
        // dd($request->all());

        $history = FeeUpdateHistory::find($request->id);
        if($history != null)
        {
            $fee = StudentFee::find($history->fee_id);
            if($fee != null)
            {
                $fee->concession = $fee->concession - $history->concession;
                $fee->unpaid_amount = $fee->unpaid_amount + $history->paid_amount + $history->concession;
                $fee->paid_amount = $fee->paid_amount - $history->paid_amount;
                $fee->save();

                if($fee->unpaid_amount > 0 )
                {
                    $fee->status = 0;
                    $fee->save();
                }

                $history->delete();
                return response()->json(['success' => true,'id' => $fee->id]);
            }
        }
        return response()->json(['success' => false]);

    }
    public function studentExamRollNo($students, $exam_id){
      $std_ids = explode(',', $students);

      $students = Student::whereIn('id',$std_ids)->get();
      $exam = Exam::find($exam_id);

      ini_set('memory_limit', '-1');
      $pdf = PDF::loadView('student.invoice.exam-rollno',compact('students','exam'));
      $pdf->getDomPDF()->set_option("enable_php", true);
        // making pdf name starts
      $makePdfName='invoice';
      return $pdf->stream(
         $makePdfName.'.pdf',
          array(
            'Attachment' => 0
          )
        );
    }
}
