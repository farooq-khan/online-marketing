<?php

namespace App\Http\Controllers;

use App\Student;
use App\StudentClass;
use App\Subject;
use App\Teacher;
use Illuminate\Http\Request;
use PDF;
use Yajra\Datatables\Datatables;

class CertificatesController extends Controller
{
    public function index()
    {
    	$classes = StudentClass::orderBy('class_name','asc')->get();
    	return view('user.certificates.index',compact('classes'));
    }

    public function getStudents(Request $request)
    {
        $query = Student::where('status',1)->where('school_id', auth()->user()->school_id);

        if($request->student_classes_select != null)
        {
            $query->whereHas('student_class',function($q) use ($request){
                $q->where('id',$request->student_classes_select);
            });
        }

        if($request->discount_student != null)
        {
            if($request->discount_student == 'discount')
            {
                $query->where('discount','>',0);
            }

            elseif($request->discount_student == 'non-discount')
            {
                $query->whereNull('discount')->orWhere('discount','==',0);
            }
        }
       
      // $query->latest();
        return Datatables::of($query)
         ->addColumn('checkbox', function ($item){
            $html_string = '<div class="custom-control custom-checkbox custom-checkbox1 d-inline-block">
                            <input type="checkbox" class="custom-control-input check" value="'.$item->id.'" id="stone_check_'.$item->id.'">
                            <label class="custom-control-label" for="stone_check_'.$item->id.'"></label>
                        </div>';
            return $html_string;
        })
        ->addColumn('action', function ($item) {

            

             $html_string = '
                 <a href="'.url('user/student/'.$item->id.'').'" class="actionicon viewIcon delete_order_transaction" data-id="' . $item->id . '" title="Delete"><i class="fa fa-eye"></i></a>
                 ';
            return $html_string;
        })
         ->editColumn('name', function ($item){
            return $item->name !== null ? $item->name : 'N.A';
        })

         ->addColumn('guardian', function ($item){
            return $item->guardian !== null ? $item->guardian.' '.@$item->guardian_nickname : 'N.A';
        })
         ->filterColumn('guardian', function( $query, $keyword ) {
         $query->where(function($q) use ($keyword){
            $q->where('guardian','LIKE', "%$keyword%")->orWhere('guardian_nickname','LIKE', "%$keyword%");
         });
        })

         ->addColumn('id', function ($item){
            return $item->id !== null ? $item->id : 'N.A';
        })

         ->editColumn('roll_no', function ($item){
            return $item->roll_no !== null ? $item->roll_no : 'N.A';
        })

         ->addColumn('gender', function ($item){
            return $item->gender !== null ? $item->gender : 'N.A';
        })
         ->editColumn('guardian_phone', function ($item){
            return $item->guardian_phone !== null ? $item->guardian_phone : 'N.A';
        })

         ->addColumn('fee', function ($item){
            $fee = $item->student_class != null ? $item->student_class->fee : 0;
            if($item->discount != null)
            {
                $fee = (100 - $item->discount)/100 * $item->student_class->fee;
            }

            return $fee;
        })
         ->addColumn('class', function ($item){
            return $item->student_class != null ? $item->student_class->class_name : 'N.A';
        })
        ->addColumn('address', function ($item){
            return $item->address !== null ? $item->address : 'N.A';
        })

        ->addColumn('discount', function ($item){
            return $item->discount !== null ? $item->discount.' %' : 0;
        })

        ->addColumn('image', function ($item){
            if($item->image != null)
            {
            	$html_string = 'Image';
            }
            else
            {
            	$html_string = 'Image';
            }

            return $html_string;
        })
        

        ->rawColumns(['checkbox','address','name','roll_no','gender','guardian_phone','fee','class','id','image','guardian','action'])
        ->make(true);

    }

    public function printCertificateInvoices(Request $request, $salary,$id,$sport = null)
    {
        $salary = explode(',',$salary);
        $students = Student::whereIn('id',$salary)->get();
            // dd($students);
        if($id == 1)
        {
            $pdf = PDF::loadView('user.invoices.certificate-invoice',compact('students'));
            // $customPaper = array(0,0,360,360);
            // $dompdf->set_paper($customPaper);
            $pdf->setPaper('a4', 'landscape');
            // $pdf->getDomPDF()->set_option("enable_php", true);
              // making pdf name starts
            $makePdfName='invoice';
            return $pdf->stream(
               $makePdfName.'.pdf',
                array(
                  'Attachment' => 0
                )
              );
        }

        if($id == 2)
        {
            $pdf = PDF::loadView('user.invoices.character-certificate-invoice',compact('students'));
            // $customPaper = array(0,0,360,360);
            // $dompdf->set_paper($customPaper);
            $pdf->setPaper('a4', 'landscape');
            // $pdf->getDomPDF()->set_option("enable_php", true);
              // making pdf name starts
            $makePdfName='invoice';
            return $pdf->stream(
               $makePdfName.'.pdf',
                array(
                  'Attachment' => 0
                )
              );
        }

        if($id == 3)
        {
            $pdf = PDF::loadView('user.invoices.sport-certificate-invoice',compact('students','sport'));
            // $customPaper = array(0,0,360,360);
            // $dompdf->set_paper($customPaper);
            $pdf->setPaper('a4', 'landscape');
            // $pdf->getDomPDF()->set_option("enable_php", true);
              // making pdf name starts
            $makePdfName='invoice';
            return $pdf->stream(
               $makePdfName.'.pdf',
                array(
                  'Attachment' => 0
                )
              );
        }
    }

    public function customPrintCertificateInvoices(Request $request)
    {
            // dd($request->all());
            $data = $request;
            $pdf = PDF::loadView('user.invoices.custom-certificate-invoice',compact('data'));
            // $customPaper = array(0,0,360,360);
            // $dompdf->set_paper($customPaper);
            $pdf->setPaper('a4', 'landscape');
            // $pdf->getDomPDF()->set_option("enable_php", true);
              // making pdf name starts
            $makePdfName='invoice';
            return $pdf->stream(
               $makePdfName.'.pdf',
                array(
                  'Attachment' => 0
                )
              );
    }

    public function teacherCertificate()
    {
        $class_ids = StudentClass::where('school_id', auth()->user()->school_id)->pluck('id')->toArray();
        $subjects = Subject::whereIn('class_id', $class_ids)->get(); 
        return view('user.certificates.teacher-certificate',compact('subjects'));
    }

    public function customCertificate()
    {
        return view('user.certificates.custom-certificate');
    }

    public function getTeachersForCertificates(Request $request)
    {
        $query = Teacher::where('status',1)->where('school_id', auth()->user()->school_id);

       
      // $query->latest();
        return Datatables::of($query)
        ->addColumn('checkbox', function ($item){
            $html_string = '<div class="custom-control custom-checkbox custom-checkbox1 d-inline-block">
                            <input type="checkbox" class="custom-control-input check" value="'.$item->id.'" id="stone_check_'.$item->id.'">
                            <label class="custom-control-label" for="stone_check_'.$item->id.'"></label>
                        </div>';
            return $html_string;
        })
        ->addColumn('action', function ($item) {
             $html_string = '
                 <a href="'.url('user/teacher/'.$item->id.'').'" class="actionicon viewIcon delete_order_transaction" data-id="' . $item->id . '" title="Delete"><i class="fa fa-eye"></i></a>
                 ';
            return $html_string;
        })
         ->addColumn('name', function ($item){
            return $item->name !== null ? $item->name : 'N.A';
        })
         ->filterColumn('guardian', function( $query, $keyword ) {
         $query->where(function($q) use ($keyword){
            $q->where('guardian','LIKE', "%$keyword%")->orWhere('guardian_nickname','LIKE', "%$keyword%");
         });
        })

         ->editColumn('guardian', function ($item){
            return $item->guardian !== null ? $item->guardian.' '.@$item->guardian_nickname : 'N.A';
        })

         ->editColumn('phone', function ($item){
            return $item->phone !== null ? $item->phone : 'N.A';
        })

         ->addColumn('education', function ($item){
            return $item->education !== null ? $item->education : 'N.A';
        })
         ->editColumn('joining_date', function ($item){
            return $item->joining_date !== null ? $item->joining_date : 'N.A';
        })


        

        ->rawColumns(['action','name','guardian','phone','education','joining_date','checkbox'])
        ->make(true);

    }

    public function printTeacherCertificateInvoices(Request $request, $teachers,$subject)
    {
        $subje = explode(',', $subject);

        // dd($subje);
        $teachers = explode(',',$teachers);
        $teachers = Teacher::whereIn('id',$teachers)->get();
            // dd($students);

            $pdf = PDF::loadView('user.invoices.teacher-certificate-invoice',compact('teachers','subje'));
            // $customPaper = array(0,0,360,360);
            // $dompdf->set_paper($customPaper);
            $pdf->setPaper('a4', 'landscape');
            // $pdf->getDomPDF()->set_option("enable_php", true);
              // making pdf name starts
            $makePdfName='invoice';
            return $pdf->stream(
               $makePdfName.'.pdf',
                array(
                  'Attachment' => 0
                )
              );
    }
}
