<?php

namespace App\Http\Controllers;

use App\Book;
use App\Teacher;
use App\LibraryBook;
use App\IssuedBook;
use App\Student;
use App\Subject;
use App\StudentClass;
use App\SoldBooksReport;
use Auth;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use PDF;
use Carbon\Carbon;

class BooksController extends Controller
{
    public function allBooks()
    {
    	$classes = StudentClass::where('school_id', auth()->user()->school_id)->get();
        return view('user.books.all-books',compact('classes'));
    }

    public function libraryAllBooks()
    {
        // $classes = StudentClass::all();
        return view('user.books.library-all-books');
    }
    public function issuedBooks()
    {
        $books = LibraryBook::where('school_id', auth()->user()->school_id)->get();
        return view('user.books.issued-books',compact('books'));
    }

    public function soldBooks()
    {
        $classes = StudentClass::where('school_id', auth()->user()->school_id)->get();
        return view('user.books.sold-books',compact('classes'));
    }

    public function soldBooksHistory()
    {
        $classes = StudentClass::where('school_id', auth()->user()->school_id)->get();
        return view('user.books.sold-books-history',compact('classes'));
    }

    public function addBookStock(Request $request)
    {
    	$book = new Book;
    	$book->name = $request->name;
    	$book->author = $request->author;
    	$book->description = $request->description;
    	$book->class_id = $request->class_id;
    	$book->purchasing_price = $request->price;
    	$book->selling_price = $request->sale_price;
    	$book->stock = $request->stock;
    	$book->user_id = Auth::user()->id;
        $book->school_id = Auth::user()->school_id;
    	$book->status = 1;
    	$book->save();

    	return response()->json(['success' => true]);
    }

    public function getBooks(Request $request)
    {
        $query = Book::where('school_id', auth()->user()->school_id)->orderBy('id','desc');
       	if($request->student_classes_select != null)
       	{
       		$query->where('class_id',$request->student_classes_select);
       	}

       	if($request->stock != null)
       	{
       		if($request->stock == 0)
       		{
       			$query->where('stock',0);
       		}
       		else if($request->stock != 0)
       		{
       			$query->where('stock','>',0);
       		}
       	}
      // $query->latest();
        return Datatables::of($query)
        ->addColumn('checkbox', function ($item) {

            $html_string = ' <a href="javascript:void(0);" class="selected-item-btn delete_stock actionicon deleteIcon" data-type="Delete" data-id="'.$item->id.'" title="Delete Class" >
                            <i class="fa fa-trash-alt"></i></a>';
            return $html_string;
        })
         ->editColumn('name', function ($item){
            $name = $item->name !== null ? $item->name : 'N.A';
            $html_string = '
                 <span class="m-l-15 inputDoubleClick" id="name" style="text-transform: capitalize;"  data-fieldvalue="'.$item->name.'">'.$name.'</span>
                <input type="text" autocomplete="nope" name="name" class="fieldFocus d-none form-control" value="'.$item->name.'" data-id="'.$item->id.'">
            ';

            return $html_string;
        })

        ->editColumn('author', function ($item){
            $author = $item->author !== null ? $item->author : 'N.A';
            $html_string = '
                 <span class="m-l-15 inputDoubleClick" id="author" style="text-transform: capitalize;"  data-fieldvalue="'.$item->author.'">'.$author.'</span>
                <input type="text" autocomplete="nope" name="author" class="fieldFocus d-none form-control" value="'.$item->author.'" data-id="'.$item->id.'">
            ';
            return $html_string;
        })

        ->editColumn('description', function ($item){
            $description = $item->description !== null ? $item->description : 'N.A';
            $html_string = '
                 <span class="m-l-15 inputDoubleClick" id="description" style="text-transform: capitalize;"  data-fieldvalue="'.$item->description.'">'.$description.'</span>
                <input type="text" autocomplete="nope" name="description" class="fieldFocus d-none form-control" value="'.$item->description.'" data-id="'.$item->id.'">
            ';

            return $html_string;
        })
        ->addColumn('class_id', function ($item){
            return $item->book_class != null ? $item->book_class->class_name : 'N.A';
        })

        ->addColumn('purchasing_price', function ($item){
            $purchasing_price = $item->purchasing_price != null ? $item->purchasing_price : 'N.A';
            $html_string = '
                 <span class="m-l-15 inputDoubleClick" id="purchasing_price" style="text-transform: capitalize;"  data-fieldvalue="'.$item->purchasing_price.'">'.$purchasing_price.'</span>
                <input type="number" autocomplete="nope" name="purchasing_price" class="fieldFocus d-none form-control" value="'.$item->purchasing_price.'" data-id="'.$item->id.'">
            ';

            return $html_string;
        })

         ->addColumn('selling_price', function ($item){
            $selling_price = $item->selling_price != null ? $item->selling_price : 'N.A';

            $html_string = '
                 <span class="m-l-15 inputDoubleClick" id="selling_price" style="text-transform: capitalize;"  data-fieldvalue="'.$item->selling_price.'">'.$selling_price.'</span>
                <input type="number" autocomplete="nope" name="selling_price" class="fieldFocus d-none form-control" value="'.$item->selling_price.'" data-id="'.$item->id.'">
            ';

            return $html_string;
        })

        ->addColumn('stock', function ($item){
            $stock = $item->stock != null ? $item->stock : 'N.A';
            $html_string = '
                 <span class="m-l-15 inputDoubleClick" id="stock" style="text-transform: capitalize;"  data-fieldvalue="'.$item->stock.'">'.$stock.'</span>
                <input type="text" autocomplete="nope" name="stock" class="fieldFocus d-none form-control" value="'.$item->stock.'" data-id="'.$item->id.'">
            ';
            return $html_string;
        })

         ->addColumn('status', function ($item){
            return $item->stock != null ? ($item->stock == 0 ? 'Out of Stock' : 'Available') : 'Out of Stock';
        })

        ->rawColumns(['checkbox','name','author','description','class_id','purchasing_price','selling_price','stock','status'])
        ->make(true);

    }

    public function getLibraryBooks(Request $request)
    {
        $query = LibraryBook::where('school_id', auth()->user()->school_id)->orderBy('id','desc');
        return Datatables::of($query)
        ->addColumn('checkbox', function ($item) {

            $html_string = ' <a href="javascript:void(0);" class="selected-item-btn delete_stock actionicon deleteIcon" data-type="Delete" data-id="'.$item->id.'" title="Delete Class" >
                            <i class="fa fa-trash-alt"></i></a>';
            return $html_string;
        })
         ->editColumn('book_title', function ($item){
            $name = $item->title !== null ? $item->title : 'N.A';
            $html_string = '
                 <span class="m-l-15 inputDoubleClick" id="title" style="text-transform: capitalize;"  data-fieldvalue="'.$item->title.'">'.$name.'</span>
                <input type="text" autocomplete="nope" name="title" class="fieldFocus d-none form-control" value="'.$item->title.'" data-id="'.$item->id.'">
            ';

            return $html_string;
        })

        ->editColumn('book_author', function ($item){
            $author = $item->author !== null ? $item->author : 'N.A';
            $html_string = '
                 <span class="m-l-15 inputDoubleClick" id="author" style="text-transform: capitalize;"  data-fieldvalue="'.$item->author.'">'.$author.'</span>
                <input type="text" autocomplete="nope" name="author" class="fieldFocus d-none form-control" value="'.$item->author.'" data-id="'.$item->id.'">
            ';
            return $html_string;
        })

        ->editColumn('book_edition', function ($item){
            $description = $item->edition !== null ? $item->edition : 'N.A';
            $html_string = '
                 <span class="m-l-15 inputDoubleClick" id="description" style="text-transform: capitalize;"  data-fieldvalue="'.$item->edition.'">'.$description.'</span>
                <input type="text" autocomplete="nope" name="edition" class="fieldFocus d-none form-control" value="'.$item->edition.'" data-id="'.$item->id.'">
            ';

            return $html_string;
        })
        ->editColumn('book_price', function ($item){
            $description = $item->price !== null ? $item->price : 'N.A';
            $html_string = '
                 <span class="m-l-15 inputDoubleClick" id="price" style="text-transform: capitalize;"  data-fieldvalue="'.$item->price.'">'.$description.'</span>
                <input type="text" autocomplete="nope" name="price" class="fieldFocus d-none form-control" value="'.$item->price.'" data-id="'.$item->id.'">
            ';
            return $html_string;
        })

        ->editColumn('book_publisher', function ($item){
            $purchasing_price = $item->publisher != null ? $item->publisher : 'N.A';
            $html_string = '
                 <span class="m-l-15 inputDoubleClick" id="publisher" style="text-transform: capitalize;"  data-fieldvalue="'.$item->publisher.'">'.$purchasing_price.'</span>
                <input type="number" autocomplete="nope" name="publisher" class="fieldFocus d-none form-control" value="'.$item->publisher.'" data-id="'.$item->id.'">
            ';

            return $html_string;
        })

         ->editColumn('book_no', function ($item){
            $selling_price = $item->book_no != null ? $item->book_no : 'N.A';

            $html_string = '
                 <span class="m-l-15 inputDoubleClick" id="book_no" style="text-transform: capitalize;"  data-fieldvalue="'.$item->book_no.'">'.$selling_price.'</span>
                <input type="number" autocomplete="nope" name="book_no" class="fieldFocus d-none form-control" value="'.$item->book_no.'" data-id="'.$item->id.'">
            ';

            return $html_string;
        })

        ->addColumn('book_type', function ($item){
            $stock = $item->book_fund != null ? $item->book_fund : 'N.A';
            $html_string = '
                 <span class="m-l-15 inputDoubleClick" id="book_fund" style="text-transform: capitalize;"  data-fieldvalue="'.$item->book_fund.'">'.$stock.'</span>
                <input type="text" autocomplete="nope" name="book_fund" class="fieldFocus d-none form-control" value="'.$item->book_fund.'" data-id="'.$item->id.'">
            ';
            return $html_string;
        })

        ->addColumn('book_pages', function ($item){
            $stock = $item->pages != null ? $item->pages : 'N.A';
            $html_string = '
                 <span class="m-l-15 inputDoubleClick" id="pages" style="text-transform: capitalize;"  data-fieldvalue="'.$item->pages.'">'.$stock.'</span>
                <input type="text" autocomplete="nope" name="pages" class="fieldFocus d-none form-control" value="'.$item->pages.'" data-id="'.$item->id.'">
            ';
            return $html_string;
        })
        ->editColumn('book_subject', function ($item){
            $stock = $item->subject != null ? $item->subject : 'N.A';
            $html_string = '
                 <span class="m-l-15 inputDoubleClick" id="subject" style="text-transform: capitalize;"  data-fieldvalue="'.$item->subject.'">'.$stock.'</span>
                <input type="text" autocomplete="nope" name="subject" class="fieldFocus d-none form-control" value="'.$item->subject.'" data-id="'.$item->id.'">
            ';
            return $html_string;
        })
        ->addColumn('created_date', function ($item){
            return $item->created_at != null ? carbon::parse($item->created_at)->format('d-m-Y') : '--';
        })
        ->addColumn('total_quantity', function ($item){
            $total_quantity = $item->total_quantity != null ? $item->total_quantity : 'N.A';

            $html_string = '
                 <span class="m-l-15 inputDoubleClick" id="total_quantity" style="text-transform: capitalize;"  data-fieldvalue="'.$item->total_quantity.'">'.$total_quantity.'</span>
                <input type="number" autocomplete="nope" name="total_quantity" class="fieldFocus d-none form-control" value="'.$item->total_quantity.'" data-id="'.$item->id.'">
            ';

            return $html_string;
        })
        ->addColumn('remaining_quantity', function ($item){
            $remaining_quantity = $item->remaining_quantity != null ? $item->remaining_quantity : 'N.A';
            return $remaining_quantity;
        })

        ->rawColumns(['checkbox','book_title','book_author','book_edition','book_price','book_publisher','book_no','book_type','book_pages','book_subject','created_date','total_quantity','remaining_quantity'])
        ->make(true);

    }

    public function updateBookStock(Request $request)
    {
        // dd($request->all());
        $old_value = $request->new_select_value; 
   
        $type = Book::where('id',$request->id)->first();

        $show_title = $request->new_select_value;
        foreach($request->except('id','new_select_value') as $key => $value)
        {
            $type->$key = $value;
        }

        $type->save();

        return response()->json(['success' => true]);
    }

    public function updateBookLibrary(Request $request)
    {
        // dd($request->all());
        $old_value = $request->new_select_value; 
   
        $type = LibraryBook::where('id',$request->id)->first();
        $qty = $type->total_quantity;
        $diff = 0;

        $show_title = $request->new_select_value;
        foreach($request->except('id','new_select_value') as $key => $value)
        {
            $type->$key = $value;
            $diff = $value - $qty;
        }

        $type->save();
        if($key == 'total_quantity')
        {
            $type->remaining_quantity = $type->remaining_quantity + $diff;
            $type->save();
        }

        return response()->json(['success' => true]);
    }

    public function deleteBookStock(Request $request)
    {
        $id = $request->id;

        $fee_type = Book::where('id',$id)->first();
        $fee_type->delete();

        return response()->json(['success' => true]);
    }

    public function deleteBookLibrary(Request $request)
    {
        $id = $request->id;
        $check = IssuedBook::where('book_id',$id)->first();
        if($check == null)
        {
            $fee_type = LibraryBook::where('id',$id)->first();
            $fee_type->delete();
            return response()->json(['success' => true]);
        }
        else
        {
            return response()->json(['success' => false]);
        }
        

    }

    public function getBooksForSold(Request $request)
    {
        $query = Book::where('school_id', auth()->user()->school_id)->orderBy('id','desc');
       	if($request->student_classes_select != null)
       	{
       		$query->where('class_id',$request->student_classes_select);
       	}

       	if($request->stock != null)
       	{
       		if($request->stock == 0)
       		{
       			$query->where('stock',0);
       		}
       		else if($request->stock != 0)
       		{
       			$query->where('stock','>',0);
       		}
       	}
      // $query->latest();
        return Datatables::of($query)
        ->addColumn('checkbox', function ($item) {
            $html_string = '<div class="custom-control custom-checkbox custom-checkbox1 d-inline-block">
                            <input type="checkbox" class="custom-control-input check" value="'.$item->id.'" data-id="'.$item->id.'" id="stone_check_'.$item->id.'">
                            <label class="custom-control-label" for="stone_check_'.$item->id.'"></label>
                        </div>';
            return $html_string;
        })
        ->addColumn('action', function ($item) {

            $html_string = ' <a href="javascript:void(0);" class="selected-item-btn delete_stock actionicon deleteIcon" data-type="Delete" data-id="'.$item->id.'" title="Delete Class" >
                            <i class="fa fa-trash-alt"></i></a>';
            return $html_string;
        })
         ->editColumn('name', function ($item){
            $name = $item->name !== null ? $item->name : 'N.A';
            $html_string = '
                 <span class="m-l-15 inputDoubleClick" id="name" style="text-transform: capitalize;"  data-fieldvalue="'.$item->name.'">'.$name.'</span>
                <input type="text" autocomplete="nope" name="name" class="fieldFocus d-none form-control" value="'.$item->name.'" data-id="'.$item->id.'">
            ';

            return $html_string;
        })

        ->editColumn('author', function ($item){
            $author = $item->author !== null ? $item->author : 'N.A';
            $html_string = '
                 <span class="m-l-15 inputDoubleClick" id="author" style="text-transform: capitalize;"  data-fieldvalue="'.$item->author.'">'.$author.'</span>
                <input type="text" autocomplete="nope" name="author" class="fieldFocus d-none form-control" value="'.$item->author.'" data-id="'.$item->id.'">
            ';
            return $html_string;
        })

        ->editColumn('description', function ($item){
            $description = $item->description !== null ? $item->description : 'N.A';
            $html_string = '
                 <span class="m-l-15 inputDoubleClick" id="description" style="text-transform: capitalize;"  data-fieldvalue="'.$item->description.'">'.$description.'</span>
                <input type="text" autocomplete="nope" name="description" class="fieldFocus d-none form-control" value="'.$item->description.'" data-id="'.$item->id.'">
            ';

            return $html_string;
        })
        ->addColumn('class_id', function ($item){
            return $item->book_class != null ? $item->book_class->class_name : 'N.A';
        })

        ->addColumn('purchasing_price', function ($item){
            $purchasing_price = $item->purchasing_price != null ? $item->purchasing_price : 'N.A';
            $html_string = '
                 <span class="m-l-15 inputDoubleClick" id="purchasing_price" style="text-transform: capitalize;"  data-fieldvalue="'.$item->purchasing_price.'">'.$purchasing_price.'</span>
                <input type="number" autocomplete="nope" name="purchasing_price" class="fieldFocus d-none form-control" value="'.$item->purchasing_price.'" data-id="'.$item->id.'">
            ';

            return $html_string;
        })

         ->addColumn('selling_price', function ($item){
            $selling_price = $item->selling_price != null ? $item->selling_price : 'N.A';

            $html_string = '
                 <span class="m-l-15 inputDoubleClick" id="selling_price" style="text-transform: capitalize;"  data-fieldvalue="'.$item->selling_price.'">'.$selling_price.'</span>
                <input type="number" autocomplete="nope" name="selling_price" id="selling_price_'.$item->id.'" class="fieldFocus d-none form-control" value="'.$item->selling_price.'" data-id="'.$item->id.'">
            ';

            return $html_string;
        })

        ->addColumn('stock', function ($item){
            $stock = $item->stock != null ? $item->stock : 'N.A';
            $html_string = '
                 <span class="m-l-15 inputDoubleClick" id="stock" style="text-transform: capitalize;"  data-fieldvalue="'.$item->stock.'">'.$stock.'</span>
                <input type="text" autocomplete="nope" name="stock" class="fieldFocus d-none form-control" value="'.$item->stock.'" data-id="'.$item->id.'">
            ';
            return $html_string;
        })

         ->addColumn('status', function ($item){
            return $item->stock != null ? ($item->stock == 0 ? 'Out of Stock' : 'Available') : 'Out of Stock';
        })

        ->rawColumns(['checkbox','action','name','author','description','class_id','purchasing_price','selling_price','stock','status'])
        ->make(true);

    }

    public function fetchStudentForBooks(Request $request)
    {
        $student = Student::with('student_class')->where('class_id',$request->id)->get();
        $html_string = '<option>--Select Student Admission No.--</option>';
        foreach ($student as $std) {
            $html_string .= '<option value="'.$std->id.'">'.$std->roll_no.' ('.$std->name.')</option>';
        }
        return response()->json(['html' => $html_string , 'success' => true]);
    }

    public function updateClassesData(Request $request)
    {
        // dd($request->all());
        $old_value = $request->new_select_value; 
   
        $type = StudentClass::where('id',$request->id)->first();

        $show_title = $request->new_select_value;
        foreach($request->except('id','new_select_value') as $key => $value)
        {
            $type->$key = $value;
        }

        $type->save();

        return response()->json(['success' => true]);
    }

    public function updateClassTeacher(Request $request)
    {
        // dd($request->all());
        $old_value = $request->new_select_value; 
   
        $type = StudentClass::where('id',$request->id)->first();

        $show_title = $request->new_select_value;
        foreach($request->except('id','new_select_value') as $key => $value)
        {
            $type->$key = $value;
        }

        $type->save();

        return response()->json(['success' => true]);
    }

    public function soldBooksToStudents(Request $request)
    {
        // dd($request->all());
        $student = Student::find($request->student_admission_no);
        $book = new SoldBooksReport;
        $book->student_id = $request->student_admission_no;  
        $book->class_id = $student->class_id;  
        $book->book_ids = $request->selected_books_ids;  
        $book->total_amount = $request->total_amount;  
        $book->paid_amount = $request->received_amount;  
        $book->user_id = Auth::user()->id;
        $book->school_id = Auth::user()->school_id;

        if($request->total_amount == $request->received_amount){
            $book->status = 'paid';
        }else{
            $book->status = 'unpaid';
        }

        $book->save();
        return response()->json(['success' => true]);
    }

    public function getSoldBooksHistory(Request $request)
    {
        $query = SoldBooksReport::where('school_id', auth()->user()->school_id)->orderBy('id','desc');
        if($request->student_classes_select != null)
        {
            $query->where('class_id',$request->student_classes_select);
        }

        if($request->status_filter != null)
        {
            $query->where('status',$request->status_filter);
        }
      // $query->latest();
        return Datatables::of($query)
        ->addColumn('checkbox', function ($item) {
            $html_string = '<div class="d-flex">';
             // $html_string .= '
             //     <a href="javascript:void(0)" class="actionicon viewIcon delete_order_transaction mr-2" data-id="' . $item->id . '" title="Delete"><i class="fa fa-eye"></i></a>
             //     ';
            $html_string .= '  <a href="javascript:void(0)" class="actionicon viewIcon download_invoice" data-id="'.$item->id.'" title="Download Invoice"><i class="zmdi zmdi-download"></i></a>';
            $html_string .= '</div>';
            return $html_string;
        })
         ->addColumn('name', function ($item){
            $name = $item->student !== null ? $item->student->name : 'N.A';
            return $name;
        })

        ->filterColumn('name', function( $query, $keyword ) {
         $query->whereHas('student', function($q) use($keyword){
                $q->where('name','LIKE', "%$keyword%");                    
            });
        })

        ->addColumn('std_class', function ($item){
            $name = $item->student_class !== null ? $item->student_class->class_name : 'N.A';
            return $name;
        })

        ->addColumn('books', function ($item){
            $books_ids = explode(',', $item->book_ids);
            $book_names = '';
            $subjects = Book::whereIn('id',$books_ids)->get();
            foreach ($subjects as $sub) {
               $book_names .= $sub->name.' ';
            }
            return $book_names;
        })

        ->addColumn('total_amount', function ($item){
            $name = $item->total_amount !== null ? $item->total_amount : 'N.A';
            return $name;
        })

        ->addColumn('paid_amount', function ($item){
            $name = $item->paid_amount !== null ? $item->paid_amount : 'N.A';
            $html_string = '
                 <span class="m-l-15 inputDoubleClick" id="paid_amount" style="text-transform: capitalize;"  data-fieldvalue="'.$item->paid_amount.'">'.$name.'</span>
                <input type="text" autocomplete="nope" name="paid_amount" class="fieldFocus d-none form-control" value="'.$item->paid_amount.'" data-id="'.$item->id.'">
            ';

            return $html_string;
        })

        ->addColumn('unpaid_amount', function ($item){
            $name = $item->total_amount - $item->paid_amount;
            return $name;
        })

        ->editColumn('status', function ($item){
            return '<span>'.$item->status.'</span>';
        })

        ->rawColumns(['checkbox','std_class','books','total_amount','paid_amount','unpaid_amount','status'])
        ->make(true);

    }

    public function soldBooksPrint(Request $request, $id)
    {
        $history = SoldBooksReport::find($id);
        $student = Student::find($history->student_id);
        $books_ids = explode(',', $history->book_ids);
        $subjects = Book::whereIn('id',$books_ids)->get();
        $pdf = PDF::loadView('user.invoices.sold-books-invoice',compact('history','student','subjects'));
        $pdf->setPaper('A4', 'portrait');
        $customPaper = array(0,0,320,520);
            $pdf->setPaper($customPaper);
        $pdf->getDomPDF()->set_option("enable_php", true);
          // making pdf name starts
        $makePdfName='invoice';
        return $pdf->stream(
           $makePdfName.'.pdf',
            array(
              'Attachment' => 0
            )
          );
    }

    public function updateSoldBooksData(Request $request)
    {
        // dd($request->all());
        $old_value = $request->new_select_value; 
   
        $type = SoldBooksReport::where('id',$request->id)->first();

        $show_title = $request->new_select_value;
        foreach($request->except('id','new_select_value') as $key => $value)
        {
            $type->$key = $value;
        }

        $type->save();

        if($type->total_amount == $type->paid_amount)
        {
            $type->status = 'paid';
            $type->save();
        }
        else
        {
            $type->status = 'unpaid';
            $type->save();
        }
        return response()->json(['success' => true]);
    }

    public function addBookToLibrary(Request $request)
    {
        // dd($request->all());
        $book = new LibraryBook;
        $book->title = $request->title;
        $book->author = $request->author;
        $book->edition = $request->edition;
        $book->price = $request->price;
        $book->publisher = $request->publisher;
        $book->book_no = $request->book_no;
        $book->book_fund = $request->book_fund;
        $book->pages = $request->book_pages;
        $book->subject = $request->subject;
        $book->total_quantity = $request->total_quantity;
        $book->remaining_quantity = $request->total_quantity;
        $book->user_id = Auth::user()->id;
        $book->school_id = Auth::user()->school_id;
        $book->save();

        return response()->json(['success' => true]);
    }

    public function fetchUserToIssueBook(Request $request)
    {
        if($request->id != null)
        {
            if($request->id == 'student')
            {
                $records = Student::where('status',1)->where('school_id', auth()->user()->school_id)->get();
            }
            else if($request->id == 'teacher')
            {
                $records = Teacher::where('status',1)->where('school_id', auth()->user()->school_id)->get();
            }

            if($records->count() > 0)
            {
                $html_string = '<option value="">--Select one of the following--</option>';
                foreach ($records as $rec) {
                    if($request->id == 'student')
                    {
                        $html_string .= "<option value='".$rec->id."'>".$rec->name." (".$rec->guardian.")"." (".$rec->student_class->class_name.")</option>";
                    }
                    else
                    {
                        $html_string .= "<option value='".$rec->id."'>".$rec->name."</option>";
                    }
                }

                return response()->json(['success' => true,'options' => $html_string]);
            }
        }
        else
        {
            return response()->json(['success' => false]);
        }
    }

    public function issueBookToUser(Request $request)
    {
        $check_issue = IssuedBook::where('user_id',$request->user_id)->where('book_id',$request->book_id)->where('user_type',$request->user_type)->first();

        if($check_issue == null)
        {
            $issue = new IssuedBook;
            $issue->user_type = $request->user_type;
            $issue->user_id = $request->user_id;
            $issue->book_id = $request->book_id;
            $issue->issue_date = $request->issue_date;
            $issue->actual_return_date = $request->return_date;
            $issue->created_by = Auth::user()->id;
            $issue->school_id = Auth::user()->school_id;
            $issue->status = 'issued';
            $issue->save();

            return response()->json(['success' => true]);
        }
        else
        {
            return response()->json(['success' => false,'already_exist' => true]);
        }
    }

    public function getLibraryIssuedBooks(Request $request)
    {
        $query = IssuedBook::where('school_id', auth()->user()->school_id)->orderBy('id','desc');
        if($request->user_type != null)
        {
            $query->where('user_type',$request->user_type);
        }
        if($request->status != null)
        {
            $query->where('status',$request->status);
        }

        if($request->from_date != null)
        {
            $query->where('actual_return_date','>=',$request->from_date);
        }

        if($request->to_date != null)
        {
            $query->where('actual_return_date','<=',$request->to_date);
        }
        return Datatables::of($query)
        ->addColumn('checkbox', function ($item) {
            $html_string = '<div class="custom-control custom-checkbox custom-checkbox1 d-inline-block">
                            <input type="checkbox" class="custom-control-input check" value="'.$item->id.'" id="stone_check_'.$item->id.'">
                            <label class="custom-control-label" for="stone_check_'.$item->id.'"></label>
                        </div>';
            return $html_string;
        })

        ->addColumn('action', function ($item) {

            $html_string = ' <a href="javascript:void(0);" class="selected-item-btn delete_stock actionicon deleteIcon" data-type="Delete" data-id="'.$item->id.'" title="Delete Class" >
                            <i class="fa fa-trash-alt"></i></a>';
            return $html_string;
        })
        ->addColumn('book_title', function ($item){
            $name = $item->book !== null ? $item->book->title : 'N.A';
            $html_string = '
                 <span class="m-l-15 inputDoubleClick" id="title" style="text-transform: capitalize;"  data-fieldvalue="'.$item->title.'">'.$name.'</span>
                <input type="text" autocomplete="nope" name="title" class="fieldFocus d-none form-control" value="'.$item->title.'" data-id="'.$item->id.'">
            ';

            return $name;
        })
        ->addColumn('issued_by', function ($item){
            $name = $item->user !== null ? $item->user->name : 'N.A';
            return $name;
        })

        ->addColumn('user_type', function ($item){
            $name = '';
            if($item->user_type == 'student')
            {
                $name = 'Student';
            }
            else if($item->user_type == 'teacher')
            {
                $name = 'Teacher';
            }
            return $name;
        })

        ->addColumn('username', function ($item){
            $name = '';
            if($item->user_type == 'student')
            {
                $name = $item->student !== null ? $item->student->name : 'N.A';
            }
            else if($item->user_type == 'teacher')
            {
                $name = $item->teacher !== null ? $item->teacher->name : 'N.A';
            }
            return $name;
        })
        ->addColumn('issue_date', function ($item){
            $date = $item->issue_date != null ? carbon::parse($item->issue_date)->format('d-m-Y') : '--';
            return $date;
        })
        ->addColumn('actual_return_date', function ($item){
            $date = $item->actual_return_date != null ? carbon::parse($item->actual_return_date)->format('d-m-Y') : '--';
            return $date;
        })
        ->addColumn('std_return_date', function ($item){
            $date = $item->std_return_date != null ? carbon::parse($item->std_return_date)->format('d-m-Y') : '--';
            return $date;
        })
        ->addColumn('status', function ($item){
            $date = $item->status == 'issued' ? 'Issued' : 'Returned';
            return $date;
        })

        ->rawColumns(['checkbox','book_title','issued_by','user_type','username','issue_date','acutal_return_date','std_return_date','status','action'])
        ->make(true);

    }

    public function receiveLibraryBooks(Request $request)
    {
        // dd($request->all());
        $ids = explode(',', $request->fee_ids);
        $found = 0;

        foreach ($ids as $id) {
            $issue_book = IssuedBook::find($id);
            if($issue_book)
            {
                if($issue_book->status == 'issued')
                {
                    $issue_book->status = 'returned';
                    $issue_book->std_return_date = carbon::now();
                    $issue_book->save();
                }
                else
                {
                    $found = 1;
                }
            }
        }
        if($found == 0)
        {
            return response()->json(['success' => true]);
        }
        else
        {
            return response()->json(['success' => false]);
        }
    }

    public function deleteIssueBookLibrary(Request $request)
    {
        // dd($request->all());
        $id = $request->id;
        $fee_type = IssuedBook::where('id',$id)->first();
        $fee_type->delete();
        return response()->json(['success' => true]);
    }
}
