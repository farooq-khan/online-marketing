<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SoldBooksReport extends Model
{
    public function student(){
    	return $this->belongsTo('App\Student', 'student_id', 'id');
    }

    public function student_class(){
    	return $this->belongsTo('App\StudentClass', 'class_id', 'id');
    }
}
