<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Result extends Model
{
    protected $fillable =  ['student_id', 'exam_id', 'class_id', 'year', 'from_date', 'to_date', 'school_id'];
    public function student_subject(){
    	return $this->belongsTo('App\Subject', 'subject_id', 'id');
    }

    public function student(){
    	return $this->belongsTo('App\Student', 'student_id', 'id');
    }

    public function student_class(){
    	return $this->belongsTo('App\StudentClass', 'class_id', 'id');
    }

    public function student_exam(){
    	return $this->belongsTo('App\Exam', 'exam_id', 'id');
    }

    public function get_grade($score)
    {
        if($score >= 80)
        {
            return 'A1';
        }
        else if($score < 80 and $score >= 70)
        {
            return 'A';
        }
        else if($score < 70 and $score >= 60)
        {
            return 'B';
        }
        else if($score < 60 and $score >= 50)
        {
            return 'C';
        }
        else if($score < 50 and $score >= 40)
        {
            return 'D';
        }
        else if($score < 40 and $score >= 33)
        {
            return 'E';
        }
        else if($score < 33)
        {
            return 'F';
        }
    }

    public function get_position($exam_id,$class_id,$per)
    {
          $query = Result::select(\DB::Raw('sum(total_marks) as total, sum(obtained_marks) as sub_total'),'results.*')->where('class_id',$class_id)->where('exam_id',$exam_id)->groupBy('exam_id','student_id')->orderBy('id', 'DESC')->get();
          $pos = 1;
          foreach ($query as $record) {
              if($record->sub_total > $per)
              {
                $pos++;
              }
          }
            $locale = 'en_US';
            $nf = new \NumberFormatter($locale, \NumberFormatter::ORDINAL);
            return $nf->format($pos);
          return $pos;

    }


}
