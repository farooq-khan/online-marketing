<?php

namespace App\Providers;

use App\Expense;
use App\SchoolInformation;
use App\Student;
use App\StudentFee;
use App\Teacher;
use App\SoldBooksReport;
use App\Book;
use App\TeacherSalary;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Carbon\Carbon;
use App\OtherFee;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
         view()->composer('*', function ($view) {
            $students_count = Student::select('id','status')->where('school_id', @auth()->user()->school_id)->where('status',1)->count();
            $suspended_students = Student::select('id','status')->where('school_id', @auth()->user()->school_id)->where('status',2)->count();
            $graduated_students = Student::select('id','status')->where('school_id', @auth()->user()->school_id)->where('status',10)->count();
            $date = \Carbon\Carbon::now();
            $currentdate = \Carbon\Carbon::now();
            $currentYear = \Carbon\Carbon::now()->format('Y');

            $lastMonth =  $date->subMonth()->format('m');
            $lastMonth_in_words =  \Carbon\Carbon::now()->subMonth()->format('F');
            // dd($lastMonth_in_words);

            $currentMonth =  $currentdate->format('m');
            // dd($currentMonth);
            $start = new \Carbon\Carbon('first day of last month');
            $end = new \Carbon\Carbon('last day of last month');
            $start = $start->format('d M');
            $end = $end->format('d M');

            $current_start = new \Carbon\Carbon('first day of this month');
            $current_end = new \Carbon\Carbon('last day of this month');
            $current_start = $current_start->format('d M');
            $current_end = $current_end->format('d M');

            $chart_month = carbon::parse($start)->format('M');
            $lastDayofMonth =    \Carbon\Carbon::parse($start)->endOfMonth()->toDateString();

            $days = carbon::parse($lastDayofMonth)->format('d');
            
            $previous_month_fee = StudentFee::whereMonth('fee_month',$lastMonth)->where('school_id', @auth()->user()->school_id)->sum('paid_amount');
            $previous_month_other_fee = OtherFee::whereMonth('submitted_date',$lastMonth)->where('school_id', @auth()->user()->school_id)->sum('amount');
            $unpaid = StudentFee::whereMonth('fee_month',$lastMonth)->where('school_id', @auth()->user()->school_id)->sum('unpaid_amount');
            // $previous_month_fee_unpaid = StudentFee::whereMonth('fee_month',$lastMonth)->sum('unpaid_amount');

            $previous_month_fee_unpaid1 = Student::whereNotIn('id',StudentFee::whereMonth('fee_month',$lastMonth)->pluck('student_id')->toArray())->where('school_id', @auth()->user()->school_id)->sum('fee');

            $previous_month_fee_unpaid = $previous_month_fee_unpaid1 + $unpaid;
                // dd($lastMonth);

            $school = SchoolInformation::where('id', @auth()->user()->school_id)->orderBy('id','asc')->first();
            if($school == null)
            {
                $school = new SchoolInformation;
                $school->name = 'Configure School Information';
            }

            // $personal_expenses = Expense::whereMonth('expense_date',$currentMonth)->whereYear('expense_date',$currentYear)->whereHas('expense',function($z){
            //     $z->where('type','personal');
            // })->sum('paid_amount');
            // $school_expenses = Expense::whereMonth('expense_date',$currentMonth)->whereYear('expense_date',$currentYear)->whereHas('expense',function($z){
            //     $z->where('type','school');
            // })->sum('paid_amount');

            // $other_expenses = Expense::whereMonth('expense_date',$currentMonth)->whereYear('expense_date',$currentYear)->whereHas('expense',function($z){
            //     $z->where('type','other');
            // })->sum('paid_amount');

            $personal_expenses = Expense::whereHas('expense',function($z){
                $z->where('type','personal');
            })->where('school_id', @auth()->user()->school_id)->sum('paid_amount');
            $school_expenses = Expense::whereHas('expense',function($z){
                $z->where('type','school');
            })->where('school_id', @auth()->user()->school_id)->sum('paid_amount');

            $other_expenses = Expense::whereHas('expense',function($z){
                $z->where('type','other');
            })->where('school_id', @auth()->user()->school_id)->sum('paid_amount');

            $all_teachers_count = Teacher::select('id','status')->where('school_id', @auth()->user()->school_id)->where('status',1)->count();
            $teachers_total_salary = Teacher::select('id','status','salary')->where('school_id', @auth()->user()->school_id)->where('status',1)->sum('salary');
            $teachers_paid_salary = TeacherSalary::select('id','paid_amount')->where('school_id', @auth()->user()->school_id)->whereMonth('salary_month',$lastMonth)->sum('paid_amount');
            $total_salaries = TeacherSalary::select('id','total_amount')->where('school_id', @auth()->user()->school_id)->sum('total_amount');
            $teachers_deduction_salary = TeacherSalary::select('id','deduction')->where('school_id', @auth()->user()->school_id)->whereMonth('salary_month',$lastMonth)->sum('deduction');;
            $bonus_amount = TeacherSalary::select('id','bonus_amount')->whereMonth('salary_month',$lastMonth)->where('school_id', @auth()->user()->school_id)->sum('bonus_amount');
            $paid_amount_in_last_m = TeacherSalary::select('id','paid_amount')->whereMonth('salary_month',$lastMonth)->where('school_id', @auth()->user()->school_id)->sum('paid_amount');
            $unpaid_amount_in_last_m = TeacherSalary::select('id','paid_amount')->where('school_id', @auth()->user()->school_id)->whereMonth('salary_month',$lastMonth)->sum('paid_amount');
            // dd($school->name);
            $conf = \App\WebsiteConfiguration::where('school_id', @auth()->user()->school_id)->first();
            $all_books_stock = Book::select('stock')->where('school_id', @auth()->user()->school_id)->sum('stock');
            $all_books_total_amount = SoldBooksReport::select('total_amount')->where('school_id', @auth()->user()->school_id)->sum('total_amount');
            $all_books_paid_amount = SoldBooksReport::select('paid_amount')->where('school_id', @auth()->user()->school_id)->sum('paid_amount');
            $view->with(compact('students_count','previous_month_fee','start','end','previous_month_fee_unpaid','school','personal_expenses','school_expenses','other_expenses','all_teachers_count','teachers_total_salary','teachers_paid_salary','chart_month','days','all_books_stock','all_books_total_amount','all_books_paid_amount','conf','previous_month_other_fee','suspended_students','graduated_students','current_start','current_end','teachers_deduction_salary','total_salaries','bonus_amount','paid_amount_in_last_m','unpaid_amount_in_last_m','lastMonth_in_words'));
         });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
    }
}
