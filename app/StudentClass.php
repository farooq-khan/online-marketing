<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudentClass extends Model
{
    public function get_subjects(){
    	return $this->hasMany('App\Subject', 'class_id', 'id');
    }

    public function get_students(){
    	return $this->hasMany('App\Student', 'class_id', 'id');
    }
    public function teacher(){
    	return $this->belongsTo('App\User', 'teacher_id', 'id');
    }
    public function class_teacher(){
    	return $this->belongsTo('App\Teacher', 'teacher_id', 'id');
    }
}
