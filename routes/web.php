<?php

use App\Http\Controllers\Admin\TimetableController;
use App\Http\Controllers\ContactController;

use Spatie\Sitemap\Sitemap;
use Spatie\Sitemap\Tags\Url;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/generate-sitemap', function () {
    $sitemap = Sitemap::create();

    // Add static pages
    $sitemap->add(Url::create('/')->setPriority(1.0)->setChangeFrequency('daily'))
            ->add(Url::create('/frequestly-asked-questions')->setPriority(0.8)->setChangeFrequency('weekly'))
            ->add(Url::create('/features')->setPriority(0.8)->setChangeFrequency('weekly'))
            ->add(Url::create('/contact-us')->setPriority(0.8)->setChangeFrequency('weekly'))
            ->add(Url::create('/privacy-policy')->setPriority(0.8)->setChangeFrequency('weekly'))
            ->add(Url::create('/terms-and-conditions')->setPriority(0.8)->setChangeFrequency('weekly'))
            ->add(Url::create('/pricing-plans')->setPriority(0.8)->setChangeFrequency('weekly'))
            ->add(Url::create('/about')->setPriority(0.8)->setChangeFrequency('weekly'));

    // Add dynamic pages
    // $posts = App\Models\Post::all();
    // foreach ($posts as $post) {
    //     $sitemap->add(
    //         Url::create("/blog/{$post->slug}")
    //             ->setLastModificationDate($post->updated_at)
    //             ->setChangeFrequency('weekly')
    //             ->setPriority(0.9)
    //     );
    // }

    $sitemap->writeToFile(public_path('sitemap.xml'));

    return "Dynamic sitemap generated!";
});



Route::get('/', function () {
	$courses = \App\OfferCourse::where('status',1)->get();
	$conf = \App\WebsiteConfiguration::first();
    return view('welcome',compact('courses','conf'));
})->name('fronted-home');

Route::get('/frequestly-asked-questions', function () {
    return view('frontend/faqs');
})->name('frequestly-asked-questions');

Route::get('/features', function () {
    return view('frontend/features');
})->name('features');

Route::get('/contact-us', function () {
    return view('frontend/contactus');
})->name('contact-us');

Route::get('/privacy-policy', function () {
    return view('frontend/privacy-policy');
})->name('privacy-policy');

Route::get('/terms-and-conditions', function () {
    return view('frontend/terms-and-conditions');
})->name('terms-and-conditions');


Route::get('/pricing-plans', function () {
    return view('frontend/pricing');
})->name('pricing-plans');

Route::get('/get-student-by-rollno','\App\Http\Controllers\Student\StudentController@getStudentByRollno')->name('get-student-by-rollno');

Route::get('download-file', function(){
	$file    = storage_path('app/system_pdfs/Fee_Invoices.pdf');
  	$headers = array(
	    'Content-Type: application/xlsx',
	    'Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0'
  	);
  	if (ob_get_contents()) ob_end_clean();
  	if (ob_get_length()) ob_end_clean();
    return \Response::download($file, 'Invoice.pdf', $headers);
});

Route::get('/update_student',function(){
	$student = \App\Student::all();

	foreach ($student as $std) {
		$class = \App\StudentClass::find($std->class_id);

		if($std->discount_amount != null)
		{
			$fee = $class->fee - $std->discount_amount;
			$dis = ( $std->discount_amount / $class->fee ) * 100;
			$std->discount = round($dis,2);

		}
		else if($std->discount != null)
		{
			$fee = $class->fee * ((100 - $std->discount)/100);
		}
		else
		{
			$fee = $class->fee;
		}
		$std->fee = $fee;
		$std->save();
	}

	return 'done';
});
Route::get('/offer-course/{subject}','GuestController@offerCourse')->name('offer-course');
Route::get('/about-us','GuestController@aboutUs')->name('about-us');
// Route::get('/contact-us','GuestController@contactUs')->name('contact-us');
Route::get('/apply-online','GuestController@applyOnline')->name('apply-online');
Route::post('/send-email-for-contact','GuestController@sendEmailForContact')->name('send-email-for-contact');
Route::post('/apply-online-for-course','GuestController@applyOnlineForCourse')->name('apply-online-for-course');

Route::get('/signup','GuestController@signup')->name('signup');
Route::post('/add-users','GuestController@add')->name('add-users');
Route::post('/add-users-student','GuestController@addStudent')->name('add-users-student');
Route::post('/check-sponsor','GuestController@checkSponsor')->name('check-sponsor');
Route::get('/school/{id}','GuestController@schoolWebsite')->name('school-website');

Route::get('/get-started','GuestController@getStarted')->name('get-started');
Route::post('/get-started','GuestController@registerSchool')->name('get-started');

Route::post('/contact', [ContactController::class, 'submitContactForm'])->name('contact.submit');

Auth::routes();

Route::group(['namespace' => 'Auth'], function (){
    Route::post('login', 'LoginController@postLogin')->name('login');
    Route::post('send-reset-link', 'LoginController@sendResetLink')->name('send-reset-link');
});



Route::get('/home', 'HomeController@index')->name('home');

Route::group(['namespace' => 'User', 'prefix' => 'user', 'middleware' => 'user-authenticated'], function (){
	Route::get('/user-login','HomeController@login')->name('user-login');
	Route::get('/dashboard','HomeController@userDashboard')->name('dashboard');
	Route::get('/update-profile','HomeController@updateProfile')->name('update-profile');
	Route::post('update-profile-changes','HomeController@saveChangesProfile')->name('update-profile-changes');

	Route::get('/all-student','\App\Http\Controllers\StudentsController@index')->name('all-students');
	Route::get('/pass-out-students','\App\Http\Controllers\StudentsController@passOutStudents')->name('pass-out-students');
	Route::get('/get-student','\App\Http\Controllers\StudentsController@getStudents')->name('get-students');
	Route::get('/get-passout-student','\App\Http\Controllers\StudentsController@getPassoutStudents')->name('get-passout-students');
	Route::post('/promote-students','\App\Http\Controllers\StudentsController@promoteStudents')->name('promote-students');
	Route::post('/move-students','\App\Http\Controllers\StudentsController@moveStudents')->name('move-students');
	Route::post('/graduate-students','\App\Http\Controllers\StudentsController@graduateStudents')->name('graduate-students');

	Route::get('/all-classes','\App\Http\Controllers\StudentsController@allClasses')->name('all-classes');
	Route::get('/get-classes','\App\Http\Controllers\StudentsController@getClasses')->name('get-classes');
	Route::post('/add-student-class','\App\Http\Controllers\StudentsController@addClass')->name('add-student-class');
	Route::post('/delete-class','\App\Http\Controllers\StudentsController@deleteClass')->name('delete-class');
	Route::post('/fetch-fee','\App\Http\Controllers\StudentsController@fetchFee')->name('fetch-fee');

	Route::get('/add-new-student','\App\Http\Controllers\StudentsController@addNewStudent')->name('add-new-student');
	Route::post('/add-student','\App\Http\Controllers\StudentsController@addStudent')->name('add-student');
	Route::post('/delete-student','\App\Http\Controllers\StudentsController@deleteStudent')->name('delete-student');
	Route::post('/delete-student-permanent','\App\Http\Controllers\StudentsController@deleteStudentPermanent')->name('delete-student-permanent');
	Route::post('/activate-student','\App\Http\Controllers\StudentsController@activateStudent')->name('activate-student');
	Route::get('/student/{id}','\App\Http\Controllers\StudentsController@singleStudent')->name('student');
	Route::post('/update-student-data','\App\Http\Controllers\StudentsController@updateStudent')->name('update-student-data');
	Route::get('/get-studen-family','\App\Http\Controllers\StudentsController@getStudentFamily')->name('get-student-family');
	Route::post('/add-student-family','\App\Http\Controllers\StudentsController@addStudentFamily')->name('add-student-family');
	Route::post('/delete-student-family','\App\Http\Controllers\StudentsController@deleteStudentFamily')->name('delete-student-family');
	Route::post('/fetch-family-students','\App\Http\Controllers\StudentsController@fetchFamilyStudents')->name('fetch-family-students');
	Route::post('/fetch-family-students-by-guardian','\App\Http\Controllers\StudentsController@fetchFamilyStudentsByGuardian')->name('fetch-family-students-by-guardian');
	Route::post('/add-student-photo','\App\Http\Controllers\StudentsController@addStudentPhoto')->name('add-student-photo');
	//rollnos
	Route::get('/student-exam-rollno/{students}/{exam_id}','\App\Http\Controllers\StudentsController@studentExamRollNo')->name('student-exam-rollno');

	Route::get('/all-fee','\App\Http\Controllers\StudentsController@allFee')->name('all-fee');
	Route::get('/submitted-fee/{type?}','\App\Http\Controllers\StudentsController@submittedFee')->name('submitted-fee');
	Route::get('/assign-fee','\App\Http\Controllers\StudentsController@assignFee')->name('assign-fee');
	Route::post('/view_fee_history','\App\Http\Controllers\StudentsController@viewFeeHistory')->name('view_fee_history');
	Route::get('/get-students-fee','\App\Http\Controllers\StudentsController@getStudentsFee')->name('get-students-fee');

	Route::post('/delete-updated-fee-history','\App\Http\Controllers\StudentsController@deleteUpdateFeeHistory')->name('delete-updated-fee-history');
	Route::post('/fetch-student-detail','\App\Http\Controllers\StudentsController@fetchStudentDetail')->name('fetch-student-detail');
	Route::post('/add-student-fee','\App\Http\Controllers\StudentsController@addStudentFee')->name('add-student-fee');
	Route::post('/edit-student-fee','\App\Http\Controllers\StudentsController@editStudentFee')->name('edit-student-fee');
	Route::post('/edit-student-other-fee','\App\Http\Controllers\StudentsController@editStudentOtherFee')->name('edit-student-other-fee');
	Route::get('/student-fee/{id}/{class_id?}','\App\Http\Controllers\FeeController@singleStudentFee')->name('student-fee');
	Route::get('/other-fee-history/{id}/{class_id?}','\App\Http\Controllers\FeeController@otherFeeHistory')->name('other-fee-history');
	Route::get('/get-student-other-fee-history','\App\Http\Controllers\FeeController@getStudentOtherFeeHistory')->name('get-student-other-fee-history');
	Route::get('/get-single-student-fee','\App\Http\Controllers\FeeController@getSingleStudentFee')->name('get-single-student-fee');
	Route::get('/get-single-student-fee-month','\App\Http\Controllers\FeeController@getSingleStudentFeeMonth')->name('get-single-student-fee-month');
	Route::get('/check-unsubmited-fee','\App\Http\Controllers\StudentsController@checkUnsubmittedFee')->name('check-unsubmited-fee');
	Route::post('/make-fee-entry','\App\Http\Controllers\StudentsController@makeFeeEntry')->name('make-fee-entry');
	Route::post('/make-other-fee-entry','\App\Http\Controllers\StudentsController@makeOtherFeeEntry')->name('make-other-fee-entry');
	Route::get('/get-students-unsubmitted-fee','\App\Http\Controllers\StudentsController@checkUnsubmittedFeeTable')->name('get-students-unsubmitted-fee');
	Route::get('/get-fee-renew-history','\App\Http\Controllers\StudentsController@getFeeRenewHistory')->name('get-fee-renew-history');


	Route::get('/all-fee-types','\App\Http\Controllers\FeeController@allFeeTypes')->name('all-fee-types');
	Route::post('/add-fee-type','\App\Http\Controllers\FeeController@addFeeType')->name('add-fee-type');
	Route::get('/get-fee-types','\App\Http\Controllers\FeeController@getFeeType')->name('get-fee-types');
	Route::post('/delete-fee-type','\App\Http\Controllers\FeeController@deleteFeeType')->name('delete-fee-type');
	Route::post('/delete-fee-records','\App\Http\Controllers\FeeController@deleteFeeRecords')->name('delete-fee-records');

	//Other Fee Submit
	Route::get('/submit-other-fee/{page?}','\App\Http\Controllers\FeeController@submitOtherFee')->name('submit-other-fee');
	Route::post('/submit-other-fee-record','\App\Http\Controllers\FeeController@submitOtherFeeRecord')->name('submit-other-fee-record');
	Route::post('/update-fee-type-data','\App\Http\Controllers\FeeController@updateOtherFee')->name('update-fee-type-data');

	Route::get('/get-other-fee','\App\Http\Controllers\FeeController@getOtherFee')->name('get-other-fee');
	Route::post('/update-other-fee-data','\App\Http\Controllers\FeeController@updateOtherFeeData')->name('update-other-fee-data');
	Route::post('/delete-other-fee-permanent','\App\Http\Controllers\FeeController@deleteOtherFee')->name('delete-other-fee-permanent');

	//SMS Management
	Route::get('/sms-management','\App\Http\Controllers\SmsController@index')->name('sms-management');

	Route::get('/all-subjects','\App\Http\Controllers\SubjectsController@allSubjects')->name('all-subjects');
	Route::post('/add-student-subject','\App\Http\Controllers\SubjectsController@addSubject')->name('add-student-subject');
	Route::get('/get-subjects','\App\Http\Controllers\SubjectsController@getSubjects')->name('get-subjects');
	Route::get('/update-subject/{id}','\App\Http\Controllers\SubjectsController@updateSubject')->name('update-subject');
	Route::post('/update-subjects','\App\Http\Controllers\SubjectsController@updateSubjects')->name('update-subjects');

	Route::get('/all-result','\App\Http\Controllers\ResultController@allResult')->name('all-result');
	Route::get('/submit-result/{class_id}/{exam_id}/{year}/{from_date}/{to_date}/{new_res?}','\App\Http\Controllers\ResultController@submitResult')->name('submit-result');

	Route::post('export-complete-exam-result','\App\Http\Controllers\ResultController@exportExamResult')->name('export-complete-exam-result');
	Route::post('export-complete-test-result','\App\Http\Controllers\ResultController@exportTestResult')->name('export-complete-test-result');

	Route::get('/all-exams','\App\Http\Controllers\ResultController@allExams')->name('all-exams');
	Route::post('/add-exam','\App\Http\Controllers\ResultController@addExam')->name('add-exam');
	Route::get('/get-exams','\App\Http\Controllers\ResultController@getExams')->name('get-exams');
	Route::post('/update-exam-data','\App\Http\Controllers\ResultController@updateExamData')->name('update-exam-data');
	Route::post('/upload-exam-result','\App\Http\Controllers\ResultController@importExamResult')->name('upload-exam-result');
	Route::post('/upload-test-result','\App\Http\Controllers\ResultController@importTestResult')->name('upload-test-result');
	Route::post('/update-test-data','\App\Http\Controllers\ResultController@updateTestData')->name('update-test-data');
	Route::get('/get-specific-class-result','\App\Http\Controllers\ResultController@getSpecificClassResult')->name('get-specific-class-result');
	Route::post('/update-student-result','\App\Http\Controllers\ResultController@updateStudentResult')->name('update-student-result');
	Route::get('/get-all-results','\App\Http\Controllers\ResultController@getAllResults')->name('get-all-results');
	Route::post('/delete-student-result-permanent','\App\Http\Controllers\ResultController@deleteStudentResultPermanent')->name('delete-student-result-permanent');

	//Tests Routes
	Route::get('/all-tests','\App\Http\Controllers\ResultController@allTests')->name('all-tests');
	Route::post('/add-test','\App\Http\Controllers\ResultController@addTest')->name('add-test');
	Route::get('/get-tests','\App\Http\Controllers\ResultController@getTests')->name('get-tests');
	Route::post('/delete-test','\App\Http\Controllers\ResultController@deleteTest')->name('delete-test');

	//Test Results Routes
	Route::get('/all-test-result','\App\Http\Controllers\ResultController@allTestResult')->name('all-test-result');
	Route::get('/submit-test-result/{class_id}/{exam_id}/{test_date}/{to_test_date}/{new_test?}','\App\Http\Controllers\ResultController@submitTestResult')->name('submit-test-result');
	Route::get('/get-specific-class-test-result','\App\Http\Controllers\ResultController@getSpecificClassTestResult')->name('get-specific-class-test-result');
	Route::post('/update-student-test-result','\App\Http\Controllers\ResultController@updateStudentTestResult')->name('update-student-test-result');
	Route::get('/get-all-test-results','\App\Http\Controllers\ResultController@getAllTestResults')->name('get-all-test-results');
	Route::get('/student-test-history/{id}/{class_id?}','\App\Http\Controllers\ResultController@studentTestHistory')->name('student-test-history');
	Route::post('/delete-student-test-result-permanent','\App\Http\Controllers\ResultController@deleteStudentTestResultPermanent')->name('delete-student-test-result-permanent');

	Route::get('/get-specific-student-test-result','\App\Http\Controllers\ResultController@getSpecificStudentTestResult')->name('get-specific-student-test-result');
	Route::get('/test-invoice-print/{std_id}/{class_id}/{exam_id}/{test_date}','\App\Http\Controllers\ResultController@printTestInvoices')->name('test-invoice-print');

	Route::get('/test-invoice-print-full/{std_id}/{class_id}/{exam_id}/{test_date}','\App\Http\Controllers\ResultController@printTestInvoicesFull')->name('test-invoice-print-full');


	Route::get('/student-exam-history/{id}/{class_id?}','\App\Http\Controllers\ResultController@studentExamHistory')->name('student-exam-history');
	Route::get('/get-specific-student-result','\App\Http\Controllers\ResultController@getSpecificStudentResult')->name('get-specific-student-result');
	Route::post('/delete-exam','\App\Http\Controllers\ResultController@deleteExam')->name('delete-exam');
	Route::post('/delete-subject','\App\Http\Controllers\ResultController@deleteSubject')->name('delete-subject');
	Route::post('/check-same-student','\App\Http\Controllers\ResultController@checkSameResult')->name('check-same-student');
	Route::get('/fee-invoice-print/{fees?}','\App\Http\Controllers\ResultController@printInvoices')->name('fee-invoice-print');
	Route::get('/fee-invoice-print-student-wise/{fees?}','\App\Http\Controllers\ResultController@printInvoicesStudentWise')->name('fee-invoice-print-student-wise');
	Route::get('/export-student-invoices','\App\Http\Controllers\ResultController@printInvoicesAll')->name('export-student-invoices');
	Route::get('/get-pdf-status','\App\Http\Controllers\ResultController@getPdfStatus')->name('get-pdf-status');
	Route::get('/fee-invoice-print-advance/{fees?}/{fee_month?}','\App\Http\Controllers\ResultController@printInvoicesAdvance')->name('fee-invoice-print-advance');
	Route::get('/fee-invoice-print-advance-all/{fees?}/{fee_month?}','\App\Http\Controllers\ResultController@printInvoicesAdvanceAll')->name('fee-invoice-print-advance-all');
	Route::get('/test-whole-invoice-print/{ids}/{id}/{class_id}','\App\Http\Controllers\ResultController@printWholeInvoices')->name('test-whole-invoice-print');
	Route::get('/result-print/{id}','\App\Http\Controllers\ResultController@printResult')->name('result-print');
	Route::get('/result-print-all/{id}','\App\Http\Controllers\ResultController@printResultAll')->name('result-print-all');
	Route::get('/fee-year-invoice-print/{student_id}/{year}','\App\Http\Controllers\ResultController@getFeeYearInvoice')->name('fee-year-invoice-print');


	//Attandance routes
	Route::get('/all-attendance','\App\Http\Controllers\AttendanceController@allAttendance')->name('all-attendance');
	Route::get('/student-attendence','\App\Http\Controllers\AttendanceController@studentAttendence')->name('student-attendence');
	Route::get('/under-construction','\App\Http\Controllers\AttendanceController@underConstruction')->name('under-construction');
	Route::get('/submit-attendance/{class_id}/{attendance_date}/{option?}','\App\Http\Controllers\AttendanceController@submitAttendance')->name('submit-attendance');
	Route::get('/get-students-for-attendance','\App\Http\Controllers\AttendanceController@getStudentForAttendance')->name('get-students-for-attendance');
	Route::post('/update-students-attendacne','\App\Http\Controllers\AttendanceController@updateStudentsAttendance')->name('update-students-attendacne');
	Route::get('/mark-all-student-present','\App\Http\Controllers\AttendanceController@markAllStudentPresent')->name('mark-all-student-present');
	Route::get('/get-students-all-attendance','\App\Http\Controllers\AttendanceController@getStudentAllAttendance')->name('get-students-all-attendance');
	Route::post('/delete-attendance-permanent','\App\Http\Controllers\AttendanceController@deleteAttendance')->name('delete-attendance-permanent');
	Route::get('/student-attendance/{id}/{class_id?}','\App\Http\Controllers\AttendanceController@singleStudentAttendance')->name('student-attendance');
	Route::post('/get-single-student-attendance','\App\Http\Controllers\AttendanceController@getSingleStudentAttendance')->name('get-single-student-attendance');
	Route::get('/attendance-invoice-print/{student_id}/{year}','\App\Http\Controllers\AttendanceController@getAttendanceInvoice')->name('attendance-invoice-print');

	//DEPARTMENT ROUTES
	Route::get('/all-departments','\App\Http\Controllers\Common\DepartmentController@index')->name('all-departments');
	Route::get('/get-departments','\App\Http\Controllers\Common\DepartmentController@getDepartments')->name('get-departments');
	Route::post('/add-department','\App\Http\Controllers\Common\DepartmentController@addDepartment')->name('add-department');
	Route::post('/delete-department','\App\Http\Controllers\Common\DepartmentController@deleteDepartment')->name('delete-department');

	//DESIGNATION ROUTES
	Route::get('/all-designations','\App\Http\Controllers\Common\DesignationController@index')->name('all-designations');
	Route::get('/get-designations','\App\Http\Controllers\Common\DesignationController@getDesignations')->name('get-designations');
	Route::post('/add-designation','\App\Http\Controllers\Common\DesignationController@addDesignation')->name('add-designation');
	Route::post('/delete-designation','\App\Http\Controllers\Common\DesignationController@deleteDesignation')->name('delete-designation');

	//PAYSCALE ROUTES
	Route::get('/all-payscales','\App\Http\Controllers\Common\PayScaleController@index')->name('all-payscales');
	Route::get('/get-payscales','\App\Http\Controllers\Common\PayScaleController@getPayScales')->name('get-payscales');
	Route::post('/add-payscale','\App\Http\Controllers\Common\PayScaleController@addPayScale')->name('add-payscale');
	Route::post('/delete-payscale','\App\Http\Controllers\Common\PayScaleController@deletePayScale')->name('delete-payscale');

	// Teachers Attendance routes
	Route::get('/teachers-all-attendance','\App\Http\Controllers\TeacherController@teachersAllAttendance')->name('teachers-all-attendance');
	Route::get('/submit-teacher-attendance/{attendance_date}/{option?}','\App\Http\Controllers\AttendanceController@submitTeacherAttendance')->name('submit-teacher-attendance');
	Route::get('/get-teachers-for-attendance','\App\Http\Controllers\AttendanceController@getTeacherForAttendance')->name('get-teachers-for-attendance');
	Route::post('/update-teachers-attendacne','\App\Http\Controllers\AttendanceController@updateTeachersAttendance')->name('update-teachers-attendacne');
	Route::get('/mark-all-teacher-present','\App\Http\Controllers\AttendanceController@markAllTeacherPresent')->name('mark-all-teacher-present');
	Route::get('/get-teachers-all-attendance','\App\Http\Controllers\AttendanceController@getTeacherAllAttendance')->name('get-teachers-all-attendance');
	Route::post('/delete-teacher-attendance-permanent','\App\Http\Controllers\AttendanceController@deleteTeacherAttendance')->name('delete-teacher-attendance-permanent');
	Route::get('/teacher-attendance/{id}','\App\Http\Controllers\AttendanceController@singleTeacherAttendance')->name('teacher-attendance');
	Route::post('/get-single-teacher-attendance','\App\Http\Controllers\AttendanceController@getSingleTeacherAttendance')->name('get-single-teacher-attendance');
	Route::get('/teacher-attendance-invoice-print/{teacher_id}/{year}','\App\Http\Controllers\AttendanceController@getTeacherAttendanceInvoice')->name('teacher-attendance-invoice-print');

	//Teachers Routes
	Route::get('/all-teachers','\App\Http\Controllers\TeacherController@index')->name('all-teachers');
	Route::get('/add-staff','\App\Http\Controllers\TeacherController@addStaff')->name('add-staff');
	Route::post('/add-teacher','\App\Http\Controllers\TeacherController@addTeacher')->name('add-teacher');
	Route::get('/get-teachers','\App\Http\Controllers\TeacherController@getTeachers')->name('get-teachers');
	Route::post('/update-profile-img-teacher','\App\Http\Controllers\TeacherController@updateProfileImgTeacher')->name('update-profile-img-teacher');
	Route::get('/teacher/{id}','\App\Http\Controllers\TeacherController@singleTeacher')->name('teacher');
	Route::post('/suspend-teacher','\App\Http\Controllers\TeacherController@suspendTeacher')->name('suspend-teacher');
	Route::get('/suspended-teachers','\App\Http\Controllers\TeacherController@suspendedTeachers')->name('suspended-teachers');
	Route::get('/get-suspended-teachers','\App\Http\Controllers\TeacherController@getSuspendedTeachers')->name('get-suspended-teachers');
	Route::post('/activate-teacher','\App\Http\Controllers\TeacherController@activateTeacher')->name('activate-teacher');
	Route::post('/delete-teacher-permanent','\App\Http\Controllers\TeacherController@deleteTeacher')->name('delete-teacher-permanent');
	Route::get('/charge-teachers-salary','\App\Http\Controllers\TeacherController@chargeTeacherSalaries')->name('charge-teachers-salary');

	Route::get('/teacher-salaries','\App\Http\Controllers\TeacherController@teacherSalaries')->name('teacher-salaries');
	Route::post('/fetch-teacher-detail','\App\Http\Controllers\TeacherController@fetchTeacherDetail')->name('fetch-teacher-detail');
	Route::post('/add-teacher-salary','\App\Http\Controllers\TeacherController@addTeacherSalary')->name('add-teacher-salary');
	Route::get('/get-teachers-salaries','\App\Http\Controllers\TeacherController@getTeachersSalaries')->name('get-teachers-salaries');
	Route::post('/check-same-teacher','\App\Http\Controllers\TeacherController@checkSameTeacher')->name('check-same-teacher');
	Route::get('/salary-invoice-print/{salary}','\App\Http\Controllers\TeacherController@printSalaryInvoices')->name('salary-invoice-print');

	//pay slips teacher
	Route::get('/teacher-pay-slip/{fees?}','\App\Http\Controllers\TeacherController@teacherPaySlip')->name('teacher-pay-slip');

	Route::post('/update-teacher-data','\App\Http\Controllers\TeacherController@updateTeacherData')->name('update-teacher-data');
	Route::post('/delete-teacher-salary','\App\Http\Controllers\TeacherController@deleteTeacherSalary')->name('delete-teacher-salary');
	Route::post('/update-teacher-salary','\App\Http\Controllers\TeacherController@editTeacherData')->name('update-teacher-salary');


	//Certificates Routes
	Route::get('/school-leaving-certificate','\App\Http\Controllers\CertificatesController@index')->name('school-leaving-certificate');
	Route::get('/get-students-for-certificates','\App\Http\Controllers\CertificatesController@getStudents')->name('get-students-for-certificates');
	Route::get('/certificate-print/{salary}/{id}/{sport?}','\App\Http\Controllers\CertificatesController@printCertificateInvoices')->name('certificate-print');
	Route::post('/custom-certificate-print','\App\Http\Controllers\CertificatesController@customPrintCertificateInvoices')->name('custom-certificate-print');

	Route::get('/custom-certificate','\App\Http\Controllers\CertificatesController@customCertificate')->name('custom-certificate');

	Route::get('/teacher-certificate','\App\Http\Controllers\CertificatesController@teacherCertificate')->name('teacher-certificate');
	Route::get('/get-teachers-for-certificates','\App\Http\Controllers\CertificatesController@getTeachersForCertificates')->name('get-teachers-for-certificates');
	Route::get('/teacher-certificate-print/{teachers}/{subject}','\App\Http\Controllers\CertificatesController@printTeacherCertificateInvoices')->name('teacher-certificate-print');

	//Expenses Routes
	Route::get('/all-expenses-types','\App\Http\Controllers\ExpansesController@expenseTypes')->name('all-expenses-types');
	Route::post('/add-expense-type','\App\Http\Controllers\ExpansesController@addExpenseType')->name('add-expense-type');
	Route::get('/get-expense-types','\App\Http\Controllers\ExpansesController@getExpenseTypes')->name('get-expense-types');
	Route::post('/delete-expense-type','\App\Http\Controllers\ExpansesController@deleteExpenseType')->name('delete-expense-type');
	Route::post('/update-expense-type-data','\App\Http\Controllers\ExpansesController@updateExpenseTypeData')->name('update-expense-type-data');

	Route::get('/all-expenses','\App\Http\Controllers\ExpansesController@allExpenses')->name('all-expenses');
	Route::get('/add-expense','\App\Http\Controllers\ExpansesController@expense')->name('add-expense');
	Route::post('/fetch-expense-category','\App\Http\Controllers\ExpansesController@fetchExpenseCategory')->name('fetch-expense-category');
	Route::post('/add-expense','\App\Http\Controllers\ExpansesController@addExpense')->name('add-expense');
	Route::get('/get-expense','\App\Http\Controllers\ExpansesController@getExpense')->name('get-expense');
	Route::post('/delete-expense','\App\Http\Controllers\ExpansesController@deleteExpense')->name('delete-expense');
	Route::post('/update-expense-data','\App\Http\Controllers\ExpansesController@updateExpenseData')->name('update-expense-data');

	// Library Routes
	Route::get('/library-all-books','\App\Http\Controllers\BooksController@libraryAllBooks')->name('library-all-books');
	Route::get('/get-library-books','\App\Http\Controllers\BooksController@getLibraryBooks')->name('get-library-books');
	Route::post('/add-book-to-library','\App\Http\Controllers\BooksController@addBookToLibrary')->name('add-book-to-library');
	Route::post('/update-book-library','\App\Http\Controllers\BooksController@updateBookLibrary')->name('update-book-library');
	Route::post('/delete-book-library','\App\Http\Controllers\BooksController@deleteBookLibrary')->name('delete-book-library');

	//issued books routes
	Route::get('/issued-books','\App\Http\Controllers\BooksController@issuedBooks')->name('issued-books');
	Route::post('/fetch-users-to-issue-book','\App\Http\Controllers\BooksController@fetchUserToIssueBook')->name('fetch-users-to-issue-book');
	Route::post('/issue-book-to-user','\App\Http\Controllers\BooksController@issueBookToUser')->name('issue-book-to-user');
	Route::get('/get-library-issued-books','\App\Http\Controllers\BooksController@getLibraryIssuedBooks')->name('get-library-issued-books');
	Route::post('/receive-library-books','\App\Http\Controllers\BooksController@receiveLibraryBooks')->name('receive-library-books');
	Route::post('/delete-issue-book-library','\App\Http\Controllers\BooksController@deleteIssueBookLibrary')->name('delete-issue-book-library');

	//Books Routes
	Route::get('/all-books','\App\Http\Controllers\BooksController@allBooks')->name('all-books');
	Route::post('/add-book-stock','\App\Http\Controllers\BooksController@addBookStock')->name('add-book-stock');
	Route::get('/get-books','\App\Http\Controllers\BooksController@getBooks')->name('get-books');
	Route::post('/update-book-stock','\App\Http\Controllers\BooksController@updateBookStock')->name('update-book-stock');
	Route::post('/delete-book-stock','\App\Http\Controllers\BooksController@deleteBookStock')->name('delete-book-stock');

	Route::get('/sold-books','\App\Http\Controllers\BooksController@soldBooks')->name('sold-books');
	Route::get('/get-books-for-sold','\App\Http\Controllers\BooksController@getBooksForSold')->name('get-books-for-sold');
	Route::post('/fetch-students-for-books','\App\Http\Controllers\BooksController@fetchStudentForBooks')->name('fetch-students-for-books');
	Route::get('/sold-books-history','\App\Http\Controllers\BooksController@soldBooksHistory')->name('sold-books-history');
	Route::get('/get-sold-books-history','\App\Http\Controllers\BooksController@getSoldBooksHistory')->name('get-sold-books-history');
	Route::get('/sold-books-print/{id}','\App\Http\Controllers\BooksController@soldBooksPrint')->name('sold-books-print');
	Route::post('/update-sold-books-data','\App\Http\Controllers\BooksController@updateSoldBooksData')->name('update-sold-books-data');

	Route::post('/update-classes-data','\App\Http\Controllers\BooksController@updateClassesData')->name('update-classes-data');
	Route::post('/save-class-teacher','\App\Http\Controllers\BooksController@updateClassTeacher')->name('save-class-teacher');
	Route::post('/sold-books-to-students','\App\Http\Controllers\BooksController@soldBooksToStudents')->name('sold-books-to-students');

});

Route::group(['namespace' => 'Admin', 'prefix' => 'admin', 'middleware' => 'admin'], function (){
	Route::get('/dashboard','HomeController@adminDashboard')->name('dashboard');
	Route::get('/all-schools','\App\Http\Controllers\Admin\HomeController@allSchools')->name('all-schools');
	Route::post('/add-school','HomeController@addSchool')->name('add-school');
	Route::get('/get-school','HomeController@getSchool')->name('get-school');
	Route::post('upload-students-excel-file','HomeController@uploadStudentExcelFile')->name('upload-students-excel-file');
	Route::post('upload-students-excel-file-for-fees','HomeController@uploadStudentExcelFileForFees')->name('upload-students-excel-file-for-fees');

	Route::get('/school/{id}','HomeController@singleSchool')->name('school');
	Route::post('/add-school-photo','HomeController@addSchoolPhoto')->name('add-school-photo');
	Route::post('/add-certificate-photo','HomeController@addCertificatePhoto')->name('add-certificate-photo');
	Route::post('/update-school-data','HomeController@updateSchoolData')->name('update-school-data');

	// offer courses routes
	Route::get('/all-offer-courses','\App\Http\Controllers\SubjectsController@allOfferCourses')->name('all-offer-courses');
	Route::post('/add-offer-course','\App\Http\Controllers\SubjectsController@addOfferCourse')->name('add-offer-course');
	Route::get('/get-offer-courses','\App\Http\Controllers\SubjectsController@getOfferCourses')->name('get-offer-courses');
	Route::get('/offer-course-detail/{id}','\App\Http\Controllers\SubjectsController@offerCourseDetail')->name('offer-course-detail');
	Route::post('/add-offer-course-detail','\App\Http\Controllers\SubjectsController@addOfferCourseDetail')->name('add-offer-course-detail');
	Route::post('/update-offer-course-data','\App\Http\Controllers\SubjectsController@updateOfferCourseData')->name('update-offer-course-data');


	Route::get('/all-applies','\App\Http\Controllers\SubjectsController@allApplies')->name('all-applies');
	Route::get('/get-all-online-applies','\App\Http\Controllers\SubjectsController@getAllApplies')->name('get-all-online-applies');
	Route::post('/delete-online-applies','\App\Http\Controllers\SubjectsController@deleteOnlineApplies')->name('delete-online-applies');

	//Website configuration
	Route::get('/all-website-configuration','\App\Http\Controllers\Admin\HomeController@allWebsiteConfiguration')->name('all-website-configuration');
	Route::post('/add-website-configuration','HomeController@addWebsiteConfiguration')->name('add-website-configuration');
	Route::get('/get-website-configuration','HomeController@getWebsiteConfiguration')->name('get-website-configuration');
	Route::get('/get-website-teachers','HomeController@getWebsiteTeachers')->name('get-website-teachers');
	Route::post('/add-website-teacher','HomeController@addWebsiteTeacher')->name('add-website-teacher');
	Route::delete('/delete-website-teacher','HomeController@deleteWebsiteTeacher')->name('delete-website-teacher');
	Route::get('/get-website-gallery','HomeController@getWebsiteGallery')->name('get-website-gallery');
	Route::post('/add-website-gallery','HomeController@addWebsiteGallery')->name('add-website-gallery');
	Route::delete('/delete-website-gallery','HomeController@deleteWebsiteGallery')->name('delete-website-gallery');
	Route::delete('/delete-configuration-image','HomeController@deleteConfigurationImage')->name('delete-configuration-image');

	// Register Account
	Route::get('/register-account','\App\Http\Controllers\Admin\HomeController@registerAccount')->name('register-account');
	Route::get('/register-student-account','\App\Http\Controllers\Admin\HomeController@registerStudentAccount')->name('register-student-account');
	Route::get('/get-register-account/{role?}','\App\Http\Controllers\Admin\HomeController@getRegisterAccount')->name('get-register-account');
	Route::get('/get-register-account-record','\App\Http\Controllers\Admin\HomeController@getRegisterAccountRecord')->name('get-register-account-record');
	Route::post('/disable-account','\App\Http\Controllers\Admin\HomeController@disableAccount')->name('disable-account');
	Route::post('/activate-account','\App\Http\Controllers\Admin\HomeController@activateAccount')->name('activate-account');
	Route::post('/delete-account','\App\Http\Controllers\Admin\HomeController@deleteAccount')->name('delete-account');

	Route::resource('timetables', \TimetableController::class);
	Route::get('/get-class-subjects', [TimetableController::class, 'getSubjects'])->name('get-class-subjects');

});

Route::group(['namespace' => 'Student', 'prefix' => 'student', 'middleware' => 'student-authenticated'], function (){
	Route::get('/student/{id}','\App\Http\Controllers\
	@singleStudent')->name('student');
});
